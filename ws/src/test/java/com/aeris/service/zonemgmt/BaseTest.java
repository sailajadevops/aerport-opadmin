package com.aeris.service.zonemgmt;

import org.junit.Before;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.google.inject.Injector;

public class BaseTest {
    protected Injector injector;

    @Before
    public void init() {
        System.setProperty(DBConstant.APPL_CONFIG_ENV, "qa");
        OperatorAdminListener operatorAdminListener=new OperatorAdminListener();
        operatorAdminListener.contextInitialized(null);
        injector = operatorAdminListener.getInjector();
    }
}
