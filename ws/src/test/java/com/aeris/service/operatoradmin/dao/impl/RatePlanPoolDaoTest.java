package com.aeris.service.operatoradmin.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.aeris.service.aeraccountmgmt.helper.BaseTest;
import com.aeris.service.operatoradmin.dao.IZoneSetDAO;
import com.aeris.service.operatoradmin.dao.util.DatabaseUtils;
import com.aeris.service.operatoradmin.model.AccountRatePlanPool;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanPaymentType;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanPoolStatus;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.RatePlanType;
import com.aeris.service.operatoradmin.model.ZoneSet;

public class RatePlanPoolDaoTest extends BaseTest {
	private static final String TESTNAME = "RatePlanPoolDaoTest";
	@Rule public TestName name = new TestName();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
	private static RatePlan globalSimSharedRatePlan1 = null;
	private static RatePlan globalSimSharedRatePlan2 = null;
	private static final long ACCOUNT_ID = 1;
	private static final int OPERATOR_ID = 1;
	private static final int GLOBAL_SIM = 2;
	private static final Map<String, AccountRatePlanPool> TEST_POOLS = new HashMap<String, AccountRatePlanPool>();
	private static IZoneSetDAO zoneSetDAO = null;
	private static int zoneSetId ;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RatePlanDAO ratePlanDao = injector.getInstance(RatePlanDAO.class);
		
		ZoneSet zoneSet = zoneSetDAO.createNewZoneSet(OPERATOR_ID, 6, 1, "Test ZoneSet123", new Date(), "test@aeris.net",null);
		zoneSetId = zoneSet.getZoneSetId();
		
		RatePlanFeeDetails ratePlanFeeDetails = new RatePlanFeeDetails();
		ratePlanFeeDetails.setAccessFee(1);
		RatePlan ratePlan = ratePlanDao.createNewRatePlan(OPERATOR_ID, GLOBAL_SIM, TESTNAME + System.currentTimeMillis()
				, RatePlanType.NON_TIERED, RatePlanPaymentType.POST_PAID, RatePlanAccessType.SHARED, RatePlanPeriodType.MONTHLY
				, 1, true, RatePlanStatus.PENDING, false
				, 1, 485, "test", "test", 82
				, 0, new Date(), sdf.parse("12-31-2099"), "USD", 1
				, 1, 1, ratePlanFeeDetails
				, null, null, null
				, null, new Date(), "RatePlanPoolDaoTest", 500, 45,
				45, 1, 1,
				0, null, false, zoneSetId, "GSM", false, null, false);
		
		Assert.assertNotNull(ratePlan);
		Assert.assertTrue(ratePlan.getRatePlanId() > 0);
		
		System.out.println("RatePlan Name = " + ratePlan.getRatePlanName());
		Assert.assertTrue(ratePlan.getRatePlanName().contains("GLOBSIM"));
		
		globalSimSharedRatePlan1 = ratePlan;
		boolean assigned = ratePlanDao.assignRatePlanToAccount(OPERATOR_ID, ACCOUNT_ID, globalSimSharedRatePlan1.getRatePlanId(), 82, new Date(), sdf.parse("12-31-2099"), "test", new Date(), TESTNAME);
		Assert.assertTrue(assigned);

	
		
		ratePlan = ratePlanDao.createNewRatePlan(OPERATOR_ID, GLOBAL_SIM, TESTNAME + System.currentTimeMillis()
				, RatePlanType.NON_TIERED, RatePlanPaymentType.POST_PAID, RatePlanAccessType.SHARED, RatePlanPeriodType.MONTHLY
				, 1, true, RatePlanStatus.PENDING, false
				, 1, 485, "test", "test", 82
				, 0, new Date(), sdf.parse("12-31-2099"), "USD", 1
				, 1, 1, ratePlanFeeDetails
				, null, null, null
				, null, new Date(), "RatePlanPoolDaoTest", 500, 45,
				45, 1, 1,
				0, null, false, zoneSetId, "GSM", false, null, false);
		
		Assert.assertNotNull(ratePlan);
		Assert.assertTrue(ratePlan.getRatePlanId() > 0);
		
		System.out.println("RatePlan Name = " + ratePlan.getRatePlanName());
		Assert.assertTrue(ratePlan.getRatePlanName().contains("GLOBSIM"));
		
		globalSimSharedRatePlan2 = ratePlan;
		
		assigned = ratePlanDao.assignRatePlanToAccount(OPERATOR_ID, ACCOUNT_ID, globalSimSharedRatePlan2.getRatePlanId(), 82, new Date(), sdf.parse("12-31-2099"), "test", new Date(), TESTNAME);
		Assert.assertTrue(assigned);

	}
	
	@AfterClass
    public static void oneTimeTearDown() throws Exception {
		RatePlanDAO ratePlanDao = injector.getInstance(RatePlanDAO.class);
		long ratePlanId = globalSimSharedRatePlan1.getRatePlanId();
		boolean expired = ratePlanDao.updateRatePlanStatus(OPERATOR_ID, ratePlanId, RatePlanStatus.EXPIRED, "test", new Date(), TESTNAME, null);
		Assert.assertTrue(expired);
		DatabaseUtils.hardDeleteRatePlan(ratePlanId);
		
		ratePlanId = globalSimSharedRatePlan2.getRatePlanId();
		expired = ratePlanDao.updateRatePlanStatus(OPERATOR_ID, ratePlanId, RatePlanStatus.EXPIRED, "test", new Date(), TESTNAME, null);
		Assert.assertTrue(expired);
		DatabaseUtils.hardDeleteRatePlan(ratePlanId);
		
		zoneSetDAO.deleteZoneSet(OPERATOR_ID, zoneSetId, new Date(), "test@aeris.net");
    }

	@Before
	public void setUp() throws Exception {
		System.out.println("Setup Name = " + name.getMethodName());
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Teardown Name = " + name.getMethodName());
		AccountRatePlanPool pool = TEST_POOLS.get(name.getMethodName());
		DatabaseUtils.hardDeletePool(pool.getAccountId(), pool.getRatePlanPoolId(), pool.getRatePlanPoolName(), pool.getRatePlans());
	}
	
	@Test
	public void testCreateAccountRatePlanPool() throws Exception {
		RatePlanPoolDAO ratePlanPoolDao = injector.getInstance(RatePlanPoolDAO.class);
		List<String> ratePlans = new ArrayList<String>();
		ratePlans.add(globalSimSharedRatePlan1.getRatePlanName());
		ratePlans.add(globalSimSharedRatePlan2.getRatePlanName());
		
		AccountRatePlanPool pool = ratePlanPoolDao.createNewAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, GLOBAL_SIM, "RPPDao-Pool" + System.currentTimeMillis(), ratePlans,  new Date(), sdf.parse("12-31-2099"), RatePlanPoolStatus.PENDING, new Date(), TESTNAME, null);
		Assert.assertNotNull(pool);
		Assert.assertEquals(pool.getRatePlans().size(), ratePlans.size());
		TEST_POOLS.put(name.getMethodName(), pool);
		AccountRatePlanPool readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);
		
		Assert.assertTrue(DatabaseUtils.isPoolAssignedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
		
		List<Long> ratePlanIds = new ArrayList<Long>();
		ratePlanIds.add(globalSimSharedRatePlan1.getRatePlanId());
		ratePlanIds.add(globalSimSharedRatePlan2.getRatePlanId());
		
		Assert.assertTrue(DatabaseUtils.isRatePlanAssignedInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, ratePlanIds));
	}
	
	@Test
	public void testCreateAndApproveAccountRatePlanPool() throws Exception {
		RatePlanPoolDAO ratePlanPoolDao = injector.getInstance(RatePlanPoolDAO.class);
		List<String> ratePlans = new ArrayList<String>();
		ratePlans.add(globalSimSharedRatePlan1.getRatePlanName());
		ratePlans.add(globalSimSharedRatePlan2.getRatePlanName());
		
		AccountRatePlanPool pool = ratePlanPoolDao.createNewAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, GLOBAL_SIM, "RPPDao-Pool" + System.currentTimeMillis(), ratePlans,  new Date(), sdf.parse("12-31-2099"), RatePlanPoolStatus.PENDING, new Date(), TESTNAME, null);
		Assert.assertNotNull(pool);
		Assert.assertEquals(pool.getRatePlans().size(), ratePlans.size());
		TEST_POOLS.put(name.getMethodName(), pool);
		AccountRatePlanPool readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);
		
		Assert.assertTrue(DatabaseUtils.isPoolAssignedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
		
		List<Long> ratePlanIds = new ArrayList<Long>();
		ratePlanIds.add(globalSimSharedRatePlan1.getRatePlanId());
		ratePlanIds.add(globalSimSharedRatePlan2.getRatePlanId());
		
		Assert.assertTrue(DatabaseUtils.isRatePlanAssignedInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, ratePlanIds));
		
		boolean approved = ratePlanPoolDao.updateRatePlanPoolStatus(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId(), RatePlanPoolStatus.APPROVED, new Date(), TESTNAME);
		Assert.assertTrue(approved);
		
		readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);
		
		System.out.println("Pool Status " + readPool.getStatus().getValue());
		Assert.assertTrue(readPool.getStatus().getValue() == RatePlanPoolStatus.APPROVED.getValue());
		
		Assert.assertTrue(DatabaseUtils.isPoolApprovedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
	}
	
	@Test
	public void testCreateAndUpdateAccountRatePlanPool() throws Exception {
		RatePlanPoolDAO ratePlanPoolDao = injector.getInstance(RatePlanPoolDAO.class);
		List<String> ratePlans = new ArrayList<String>();
		ratePlans.add(globalSimSharedRatePlan1.getRatePlanName());
		ratePlans.add(globalSimSharedRatePlan2.getRatePlanName());
		
		AccountRatePlanPool pool = ratePlanPoolDao.createNewAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, GLOBAL_SIM, "RPPDao-Pool" + System.currentTimeMillis(), ratePlans,  new Date(), sdf.parse("12-31-2099"), RatePlanPoolStatus.PENDING, new Date(), TESTNAME, null);
		Assert.assertNotNull(pool);
		Assert.assertEquals(pool.getRatePlans().size(), ratePlans.size());
		TEST_POOLS.put(name.getMethodName(), pool);
		AccountRatePlanPool readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);
		
		Assert.assertTrue(DatabaseUtils.isPoolAssignedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
		
		List<Long> ratePlanIds = new ArrayList<Long>();
		ratePlanIds.add(globalSimSharedRatePlan1.getRatePlanId());
		ratePlanIds.add(globalSimSharedRatePlan2.getRatePlanId());
		
		Assert.assertTrue(DatabaseUtils.isRatePlanAssignedInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, ratePlanIds));
		
		//Update and keep only one rate plan
		List<String> updatedRatePlans1 = new ArrayList<String>();
		updatedRatePlans1.add(globalSimSharedRatePlan1.getRatePlanName());
		List<Long> updatedRatePlanIds1 = new ArrayList<Long>();
		updatedRatePlanIds1.add(globalSimSharedRatePlan1.getRatePlanId());
		pool = ratePlanPoolDao.updateAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId(), GLOBAL_SIM, pool.getRatePlanPoolName(), updatedRatePlans1, new Date(), sdf.parse("12-31-2099"), RatePlanPoolStatus.PENDING, new Date(), TESTNAME, null);
		Assert.assertNotNull(pool);
		Assert.assertEquals(pool.getRatePlans().size(), updatedRatePlans1.size());
		
		readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);

		Assert.assertTrue(DatabaseUtils.isPoolAssignedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
		Assert.assertTrue(DatabaseUtils.isRatePlanAssignedInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, updatedRatePlanIds1));
		
		//Update and put both rate plans back
		pool = ratePlanPoolDao.updateAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId(), GLOBAL_SIM, pool.getRatePlanPoolName(), ratePlans, new Date(), sdf.parse("12-31-2099"), RatePlanPoolStatus.PENDING, new Date(), TESTNAME, null);
		Assert.assertNotNull(pool);
		Assert.assertEquals(pool.getRatePlans().size(), ratePlans.size());
		
		readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);

		Assert.assertTrue(DatabaseUtils.isPoolAssignedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
		Assert.assertTrue(DatabaseUtils.isRatePlanAssignedInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, ratePlanIds));
	}
	
	@Test
	public void testCreateAndInvalidateAccountRatePlanPool() throws Exception {
		RatePlanPoolDAO ratePlanPoolDao = injector.getInstance(RatePlanPoolDAO.class);
		List<String> ratePlans = new ArrayList<String>();
		ratePlans.add(globalSimSharedRatePlan1.getRatePlanName());
		ratePlans.add(globalSimSharedRatePlan2.getRatePlanName());
		
		AccountRatePlanPool pool = ratePlanPoolDao.createNewAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, GLOBAL_SIM, "RPPDao-Pool" + System.currentTimeMillis(), ratePlans,  new Date(), sdf.parse("12-31-2099"), RatePlanPoolStatus.PENDING, new Date(), TESTNAME, null);
		Assert.assertNotNull(pool);
		Assert.assertEquals(pool.getRatePlans().size(), ratePlans.size());
		TEST_POOLS.put(name.getMethodName(), pool);
		AccountRatePlanPool readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);
		
		Assert.assertTrue(DatabaseUtils.isPoolAssignedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
		
		List<Long> ratePlanIds = new ArrayList<Long>();
		ratePlanIds.add(globalSimSharedRatePlan1.getRatePlanId());
		ratePlanIds.add(globalSimSharedRatePlan2.getRatePlanId());
		
		Assert.assertTrue(DatabaseUtils.isRatePlanAssignedInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, ratePlanIds));
		
		Date expireDate = new Date();
		String expireDateStr = sdf.format(expireDate);
		
		boolean invalidated = ratePlanPoolDao.invalidateRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId(), expireDate, TESTNAME);
		Assert.assertTrue(invalidated);
		
		readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNull(readPool);
		
		Assert.assertTrue(DatabaseUtils.isPoolExpiredInAP(pool.getRatePlanPoolId(), ACCOUNT_ID, expireDateStr));
		
		Assert.assertTrue(DatabaseUtils.isRatePlanExpiredInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, ratePlanIds, expireDateStr));
	}


	@Test
	public void testCreateApproveAndInvalidateAccountRatePlanPool() throws Exception {
		RatePlanPoolDAO ratePlanPoolDao = injector.getInstance(RatePlanPoolDAO.class);
		List<String> ratePlans = new ArrayList<String>();
		ratePlans.add(globalSimSharedRatePlan1.getRatePlanName());
		ratePlans.add(globalSimSharedRatePlan2.getRatePlanName());
		
		AccountRatePlanPool pool = ratePlanPoolDao.createNewAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, GLOBAL_SIM, "RPPDao-Pool" + System.currentTimeMillis(), ratePlans,  new Date(), sdf.parse("12-31-2099"), RatePlanPoolStatus.PENDING, new Date(), TESTNAME, null);
		Assert.assertNotNull(pool);
		Assert.assertEquals(pool.getRatePlans().size(), ratePlans.size());
		TEST_POOLS.put(name.getMethodName(), pool);
		AccountRatePlanPool readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);
		
		Assert.assertTrue(DatabaseUtils.isPoolAssignedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
		
		List<Long> ratePlanIds = new ArrayList<Long>();
		ratePlanIds.add(globalSimSharedRatePlan1.getRatePlanId());
		ratePlanIds.add(globalSimSharedRatePlan2.getRatePlanId());
		
		Assert.assertTrue(DatabaseUtils.isRatePlanAssignedInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, ratePlanIds));
		
		boolean approved = ratePlanPoolDao.updateRatePlanPoolStatus(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId(), RatePlanPoolStatus.APPROVED, new Date(), TESTNAME);
		Assert.assertTrue(approved);
		
		readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNotNull(readPool);
		
		System.out.println("Pool Status " + readPool.getStatus().getValue());
		Assert.assertTrue(readPool.getStatus().getValue() == RatePlanPoolStatus.APPROVED.getValue());
		
		Assert.assertTrue(DatabaseUtils.isPoolApprovedInAP(pool.getRatePlanPoolId(), ACCOUNT_ID));
		
		Date expireDate = new Date();
		String expireDateStr = sdf.format(expireDate);
		
		boolean invalidated = ratePlanPoolDao.invalidateRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId(), expireDate, TESTNAME);
		Assert.assertTrue(invalidated);
		
		readPool = ratePlanPoolDao.getAccountRatePlanPool(OPERATOR_ID, ACCOUNT_ID, pool.getRatePlanPoolId());
		Assert.assertNull(readPool);
		
		Assert.assertTrue(DatabaseUtils.isPoolExpiredInAP(pool.getRatePlanPoolId(), ACCOUNT_ID, expireDateStr));
		
		Assert.assertTrue(DatabaseUtils.isRatePlanExpiredInARPPM(pool.getRatePlanPoolId(), ACCOUNT_ID, ratePlanIds, expireDateStr));
	}

}
