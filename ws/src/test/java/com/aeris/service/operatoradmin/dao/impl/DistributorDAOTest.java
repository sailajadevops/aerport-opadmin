package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.dao.IDistributorDAO;
import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.DistributorAccountException;
import com.aeris.service.operatoradmin.exception.DistributorDBException;
import com.aeris.service.operatoradmin.exception.DistributorException;
import com.aeris.service.operatoradmin.exception.DistributorNotFoundException;
import com.aeris.service.operatoradmin.exception.SimpackDBException;
import com.aeris.service.operatoradmin.exception.SIMPackException;
import com.aeris.service.operatoradmin.model.Distributor;
import com.aeris.service.operatoradmin.payload.AssignSIMsFromInventoryRequest;
import com.aeris.service.operatoradmin.payload.AssignSIMsFromSIMPACKRequest;
import com.aeris.service.operatoradmin.payload.CreateDistributorCode;
import com.aeris.service.operatoradmin.payload.CreateDistributorRequest;
import com.aeris.service.operatoradmin.payload.CreateSimpackRequest;
import com.aeris.service.operatoradmin.payload.Simpack;
import com.aeris.service.operatoradmin.payload.UpdateDistributorRequest;
import com.aeris.service.operatoradmin.payload.UpdateEmptySIMPACKRequest;
import com.google.inject.Injector;

/**
 * Test class for DistributorDAO
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class DistributorDAOTest {
	
	protected static Injector injector;
	private static IDistributorDAO distributorDao;
	private static OperatorAdminListener operatorAdminListener = null ;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty(DBConstant.APPL_CONFIG_ENV,"local");
		operatorAdminListener = new OperatorAdminListener();
        operatorAdminListener.contextInitialized(null);
        injector = operatorAdminListener.getInjector();
        distributorDao = injector.getInstance(DistributorDAO.class);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Test // passed
	public void testGetAllDistributors() {
		try {
			List<Distributor> distributors = distributorDao.getAllDistributors(2l);
			Assert.assertNotNull(distributors);
			Assert.assertTrue(distributors.size() > 0);
		} catch (DistributorException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//@Test //passed
	public void testGetDistributor() {
		try {
			Distributor distributor = distributorDao.getDistributor(1l, 3l);
			Assert.assertNotNull(distributor);
		} catch (DistributorNotFoundException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		}
	}

	//@Test //passed
	public void testCreateNewDistributor() {
		try {
			Distributor d = distributorDao.createNewDistributor(1l, prepareDistributorRequest());
			Assert.assertNotNull(d);
			Assert.assertTrue(d.getDistributorId() > 0);
		} catch (DistributorException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		}
	}


	//@Test //passed
	public void testUpdateDistributor() {
		try {
			Distributor d = distributorDao.updateDistributor(2l, 3l, prepareUpdateDistributorRequest());
			d.setDistributorId(3l);
			Assert.assertNotNull(d);
		} catch (DistributorNotFoundException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		}
	}

	@Ignore //passed
	public void testDeleteDistributor() {
		try {
			distributorDao.deleteDistributor(1l, 1l, new Date(System.currentTimeMillis()), "saurabh.sharma@aeris.net");
		} catch (DistributorException e) {
			e.printStackTrace();
		} catch (DistributorNotFoundException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		}
	}


	//@Test //  passed
	public void testCreateEmptySimpacks4Distributor() {
		try {
			CreateDistributorCode request = new CreateDistributorCode();
			request.setAccountId(1l);
			request.setAssignedIccid(null);
			request.setIccidStart("");
			request.setIccidEnd("");
			request.setNumberOfCodes(2);
			request.setDistributorId(1l);
			request.setEmailRecipients(Arrays.asList("saurabh.sharma@aeris.net".split(",")));
			List<String> simpacks = distributorDao.createEmptySimpacks4Distributor(1l, 1l, request);
			Assert.assertNotNull(simpacks);
			Assert.assertTrue(simpacks.size() == request.getNumberOfCodes());
		} catch (DistributorException e) {
			e.printStackTrace();
		} catch (DistributorNotFoundException e) {
			e.printStackTrace();
		}
	}


	//@Test //passed
	public void testCreateSIMPacksForOperator() {
		CreateSimpackRequest request = new CreateSimpackRequest();
		request.setIccids("89185014121800300023,89185014121800300024,89185014121800300025");
		request.setIccidStart("891850141218003000201");
		request.setIccidEnd("891850141218003000251");
		request.setPackCount(1);
		request.setPackSize(5);
		request.setEmailRecipients(Arrays.asList("saurabh.sharma@aeris.net".split(",")));
		List<Simpack> simpacks = null;
		try {
			simpacks = distributorDao.createSIMPacksForOperator(1l, request);
		} catch (SIMPackException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		} catch (SimpackDBException e) {
			e.printStackTrace();
		}
		Assert.assertNotNull(simpacks);
		Assert.assertTrue(simpacks.size() == request.getPackCount());
	}

	//@Test //passed
	public void testAssignSIMsToAccount() {
		AssignSIMsFromInventoryRequest request = new AssignSIMsFromInventoryRequest();
		request.setIccidStart(null);
		request.setIccidEnd(null);
		request.setIccids("8901260762214912652");
		request.setOrderId("20101112");
		request.setUserId("saurabh.sharma@aeris.net");
		try {
			boolean assigned = distributorDao.assignSIMsToAccount(1l, "11633", request);
			Assert.assertTrue(assigned);
		} catch (AccountNotFoundException e) {
			e.printStackTrace();
		} catch (SIMPackException e) {
			e.printStackTrace();
		} catch (DistributorAccountException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		} 
	}


	//@Test // pending
	public void testAssignSIMsFromSIMPACKToAccount() {
		AssignSIMsFromSIMPACKRequest request = new AssignSIMsFromSIMPACKRequest();
		request.setOrderId("20101112");
		request.setUserId("saurabh.sharma@aeris.net");
		request.setSimpackId("735910");
		try {
			boolean assigned = distributorDao.assignSIMsFromSIMPACKToAccount(1l, "1", request);
			Assert.assertTrue(assigned);
		} catch (AccountNotFoundException e) {
			e.printStackTrace();
		} catch (DistributorException e) {
			e.printStackTrace();
		} catch (DistributorNotFoundException e) {
			e.printStackTrace();
		} catch (SIMPackException e) {
			e.printStackTrace();
		} catch (DistributorAccountException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		} 
	}


	//@Test //passed
	public void testUpdateEmptySimpacksOfDistributor() {
		try {
			UpdateEmptySIMPACKRequest request = new UpdateEmptySIMPACKRequest();
			request.setEmailRecipients(Arrays.asList("saurabh.sharma@aeris.net,test@aeris.net".split(",")));
			request.setUserId("saurbh.sharma@aeris.net");
			List<Simpack> simpacks = new ArrayList<Simpack>();
			Simpack simpack = new Simpack();
			simpack.setIccidStart("89185014121800300023");
			simpack.setIccidEnd("89185014121800300053");
			simpack.setSimType("2FF");
			simpacks.add(simpack);
			request.setSimpacks(simpacks);
			distributorDao.updateEmptySimpacksOfDistributor(1l, 1l, request);
		} catch (DistributorException e) {
			e.printStackTrace();
		} catch (AccountNotFoundException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		}
	}

	//@Test //passed
	public void testAddSimpacksToDistributor() {
		try {
			UpdateEmptySIMPACKRequest request = new UpdateEmptySIMPACKRequest();
			request.setEmailRecipients(Arrays.asList("saurabh.sharma@aeris.net,test@aeris.net".split(",")));
			request.setUserId("saurbh.sharma@aeris.net");
			List<Simpack> simpacks = new ArrayList<Simpack>();
			Simpack simpack = new Simpack();
			simpack.setIccidStart("89185014121800300023");
			simpack.setIccidEnd("89185014121800300053");
			simpack.setSimType("2FF");
			simpack.setSimpackId("735910");
			simpacks.add(simpack);
			request.setSimpacks(simpacks);
			distributorDao.addSimpacksToDistributor(1l, 3l, request );
		} catch (DistributorException e) {
			e.printStackTrace();
		} catch (DistributorNotFoundException e) {
			e.printStackTrace();
		} catch (DistributorDBException e) {
			e.printStackTrace();
		}
	}


	//@Test // passed
	public void testGetSimPackByUniqueId() {
		try {
			final String uniqueId = "735910";
			Simpack simpack = distributorDao.getSimPackByUniqueId(uniqueId);
			Assert.assertNotNull(simpack);
			Assert.assertTrue(simpack.getSimpackId().equals(uniqueId));
		} catch (DistributorException e) {
			e.printStackTrace();
		}
	}

	//@Test //passed
	public void testGetSimpacks() {
		try {
			List<Simpack> simpacks = distributorDao.getSimpacks(1l, 1l, "1");
			Assert.assertNotNull(simpacks);
			Assert.assertTrue(simpacks.size() > 0);
		} catch (SimpackDBException e) {
			e.printStackTrace();
		}
	}
	
	
	//@Test //passed
	public void testGetSimpacks1() {
		try {
			List<Simpack> simpacks = distributorDao.getSimpacks(1l, -1l, "1");
			Assert.assertNotNull(simpacks);
			Assert.assertTrue(simpacks.size() > 0);
		} catch (SimpackDBException e) {
			e.printStackTrace();
		}
	}
	
	private CreateDistributorRequest prepareDistributorRequest () {
		CreateDistributorRequest distributor = new CreateDistributorRequest();
		distributor.setDistributorId(1l);
		distributor.setAccountId(1l);
		distributor.setCreatedBy("saurabh.sharma@aeris.net");
		distributor.setLastModifiedBy("saurabh.sharma@aeris.net");
		distributor.setStatus(1);
		distributor.setCreatedDate(new Date(System.currentTimeMillis()));
		distributor.setLastModifiedDate(new Date(System.currentTimeMillis()));
		distributor.setRepTimestamp(new Timestamp(System.currentTimeMillis()));
		distributor.setLogoUrls("test.logo.url");
		distributor.setPrivacyPolicyUrls("privacy.policy.url");
		distributor.setDistributorName("test");
		distributor.setContactEmails(Arrays.asList("saurabh.sharma@aeris.net,test@aeris.net".split(",")));
		return distributor ;
	}
	
	
	private UpdateDistributorRequest prepareUpdateDistributorRequest () {
		UpdateDistributorRequest distributor = new UpdateDistributorRequest();
		distributor.setAccountId(1l);
		distributor.setCreatedBy("saurabh.sharma@aeris.net");
		distributor.setLastModifiedBy("saurabh.sharma@aeris.net");
		distributor.setStatus(1);
		distributor.setCreatedDate(new Date(System.currentTimeMillis()));
		distributor.setLastModifiedDate(new Date(System.currentTimeMillis()));
		distributor.setRepTimestamp(new Timestamp(System.currentTimeMillis()));
		distributor.setLogoUrls("test.logo.url");
		distributor.setPrivacyPolicyUrls("privacy.policy.url");
		distributor.setDistributorName("test");
		distributor.setContactEmails(Arrays.asList("saurabh.sharma@aeris.net,test@aeris.net".split(",")));
		return distributor ;
	}

}
