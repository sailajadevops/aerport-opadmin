package com.aeris.service.operatoradmin.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneMappingException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.exception.ZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetNotFoundException;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZonePreference;
import com.aeris.service.operatoradmin.model.ZoneSet;
import com.google.inject.Injector;

@FixMethodOrder (MethodSorters.NAME_ASCENDING)
public class ZoneDaoTest {
	protected static Injector injector;
	public static IZoneDAO zoneDao ;
	static int zoneId1,zoneId2,zoneId3,zoneId4;
	
	@BeforeClass
	public static void init(){
		System.setProperty(DBConstant.APPL_CONFIG_ENV,"local");
        OperatorAdminListener operatorAdminListener=new OperatorAdminListener();
        operatorAdminListener.contextInitialized(null);
        injector = operatorAdminListener.getInjector();
        
        zoneDao = injector.getInstance(ZoneDAO.class);
	}
	
	@Test
	public void test1_createZone(){
		EnrolledServices services = new EnrolledServices();
		services.setAllowMOSms(false);
		services.setAllowMOVoice(false);
		services.setAllowMTSms(false);
		services.setAllowMTVoice(false);
		services.setAllowPacket(false);
		
		IncludedPlanDetails includedPlan = new IncludedPlanDetails();
		includedPlan.setIncludedMOSms(0);
		includedPlan.setIncludedMOVoiceMins(0);
		includedPlan.setIncludedMTSms(0);
		includedPlan.setIncludedMTVoiceMins(0);
		includedPlan.setIncludedPacketKB(0);
		includedPlan.setPerKBPacketPrice(0);
		includedPlan.setPerMOSmsPrice(0);
		includedPlan.setPerMTSmsPrice(0);
		includedPlan.setPerMinMOVoicePrice(0);
		includedPlan.setPerMinMTVoicePrice(0);
		
		List<String> operators = new ArrayList<String>(Arrays.asList("1,2,3,4"));
		List<String> countries = new ArrayList<String>(Arrays.asList("4,5,6,7"));
		List<ZonePreference> zonePreferences = new ArrayList<ZonePreference>();
		zonePreferences.add(ZonePreference.HOME);
		Zone zone = null;
		try {
			zone = zoneDao.createNewZone(2, 1, 10, "TestZone1 by Deepali", operators, countries , services, includedPlan, zonePreferences , "CDMA", new Date(), "deepali.mulay@aeris.net");
			zoneId1 = zone.getZoneId();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (DuplicateZoneException e) {
			e.printStackTrace();
		}
		Assert.assertNotEquals(0,zone.getZoneId());
		Assert.assertEquals(1, zone.getZoneSetId());
		
		try {
			zone = zoneDao.createNewZone(2, 1, 10, "TestZone2 by Deepali", operators, countries , services, includedPlan, zonePreferences , "CDMA", new Date(), "deepali.mulay@aeris.net");
			zoneId2 = zone.getZoneId();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (DuplicateZoneException e) {
			e.printStackTrace();
		}
		Assert.assertNotEquals(0,zone.getZoneId());
		Assert.assertEquals(1, zone.getZoneSetId());
		
		try {
			zone = zoneDao.createNewZone(1, 3, 10, "TestZone3 by Deepali", operators, countries , services, includedPlan, zonePreferences , "CDMA", new Date(), "deepali.mulay@aeris.net");
			zoneId3 = zone.getZoneId();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (DuplicateZoneException e) {
			e.printStackTrace();
		}
		Assert.assertNotEquals(0,zone.getZoneId());
		Assert.assertEquals(3, zone.getZoneSetId());
		
		try {
			zone = zoneDao.createNewZone(2, 5, 6, "TestZone4 by Deepali", operators, countries , services, includedPlan, zonePreferences , "GSM", new Date(), "deepali.mulay@aeris.net");
			zoneId4 = zone.getZoneId();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (DuplicateZoneException e) {
			e.printStackTrace();
		}
		Assert.assertNotEquals(0,zone.getZoneId());
		Assert.assertEquals(5, zone.getZoneSetId());
	}
	
	
	@Test
	public void test2_getZone(){
		Zone zone = null;
		try {
			zone = zoneDao.getZone(1, 3, zoneId3);
		} catch (ZoneNotFoundException e) {
			e.printStackTrace();
		} catch (ZoneException e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals(zone.getZoneName(),"TestZone3 by Deepali");
		Assert.assertEquals(3, zone.getZoneSetId());
	}
	
	@Test
	public void test3_getAllZones(){
		List<Zone> zoneList = null;
		try {
			zoneList = zoneDao.getAllZones(2, 1);
		}catch (ZoneException e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals(zoneList.size(),2);
	}
	
	@Test
	public void test4_getZonesByZoneSet(){
		List<Zone> zoneList = null;
		try {
			zoneList = zoneDao.getZonesByZoneSet(2, 5);
		} catch (ZoneException e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals(zoneList.size(),1);
	}
	
	@Test
	public void test4_updateZoneSet(){
		EnrolledServices services = new EnrolledServices();
		services.setAllowMOSms(false);
		services.setAllowMOVoice(false);
		services.setAllowMTSms(false);
		services.setAllowMTVoice(false);
		services.setAllowPacket(false);
		
		IncludedPlanDetails includedPlan = new IncludedPlanDetails();
		includedPlan.setIncludedMOSms(0);
		includedPlan.setIncludedMOVoiceMins(0);
		includedPlan.setIncludedMTSms(0);
		includedPlan.setIncludedMTVoiceMins(0);
		includedPlan.setIncludedPacketKB(0);
		includedPlan.setPerKBPacketPrice(0);
		includedPlan.setPerMOSmsPrice(0);
		includedPlan.setPerMTSmsPrice(0);
		includedPlan.setPerMinMOVoicePrice(0);
		includedPlan.setPerMinMTVoicePrice(0);
		
		List<String> operators = new ArrayList<String>(Arrays.asList("1,2,3,4"));
		List<String> countries = new ArrayList<String>(Arrays.asList("4,5,6,7"));
		List<ZonePreference> zonePreferences = new ArrayList<ZonePreference>();
		zonePreferences.add(ZonePreference.HOME);
		Zone zone = null;
		
		try {
			zoneDao.updateZone(1, 3, zoneId3, 10, "New TestZone3 by Deepali", operators, countries, services, includedPlan, zonePreferences, "CDMA", new Date(), "deepali.mulay@aeris.net");
			zone = zoneDao.getZone(1, 3, zoneId3);
		} catch (ZoneNotFoundException e) {
			e.printStackTrace();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (DuplicateZoneException e) {
			e.printStackTrace();
		} catch (ZoneMappingException e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals("New TestZone3 by Deepali", zone.getZoneName());
		Assert.assertEquals(3,zone.getZoneSetId());
	}
	
	@Test
	public void test5_deleteZone(){
		try {
			zoneDao.deleteZone(2, 1, zoneId1, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneNotFoundException e) {
			e.printStackTrace();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (ZoneMappingException e) {
			e.printStackTrace();
		}
		
		try {
			zoneDao.deleteZone(2, 1, zoneId2, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneNotFoundException e) {
			e.printStackTrace();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (ZoneMappingException e) {
			e.printStackTrace();
		}
		
		try {
			zoneDao.deleteZone(1, 3, zoneId3, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneNotFoundException e) {
			e.printStackTrace();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (ZoneMappingException e) {
			e.printStackTrace();
		}
		
		try {
			zoneDao.deleteZone(2, 5, zoneId4, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneNotFoundException e) {
			e.printStackTrace();
		} catch (ZoneException e) {
			e.printStackTrace();
		} catch (ZoneMappingException e) {
			e.printStackTrace();
		}
		
	}
	
	@Test(expected=ZoneNotFoundException.class)
	public void test6_getZoneWhichDoesNotExists() throws ZoneNotFoundException, ZoneException {
		zoneDao.getZone(1, 3, zoneId3);
		
	}
}
