package com.aeris.service.operatoradmin.resources;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Test class for verifying various resource access role mapping of request api-keys 
 * 
 * @author saurabh.sharma@aeris.net
 * 
 * @see BaseTest
 *
 */
public class RolesAllowedFilterTest extends BaseTest {
	
	private static final Logger LOG = LoggerFactory.getLogger(RolesAllowedFilterTest.class);

	@Test
	public void unauthorizedOperator() throws Exception {
		HttpResponse mockResponse = getResponse("2/ping1", "1c1ed267-47a3-11e3-b32d-b594d0c6a6a");
		int responseCode = mockResponse.getStatusLine().getStatusCode();
		Assert.assertTrue(responseCode == 401);
	}
	

	@Test
	public void testPlatformAdminOnly() {
		try {
			//1. PlatformAdmin key
			LOG.debug("1. verify platform admin key");
			HttpResponse mockResponse = getResponse("2/accts/1/platformAdminOnly", "a5ef2f82-44d0-4377-a9eb-fadfdf578177");
			int responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//2. matching operator WR key
			LOG.debug("2. verify operator write-read key");
			mockResponse = getResponse("1/accts/1/platformAdminOnly", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			LOG.debug("3. verify mismatched operator key");
			//2.1 mismatching operator key, operator=2 and key is of operator=1
			mockResponse = getResponse("2/accts/1/platformAdminOnly", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			LOG.debug("4. verify operator read-only key");
			//3. Operator Read-only key
			mockResponse = getResponse("1/accts/1/platformAdminOnly", "4240449e-5a70-4be9-a953-48e0650cc137");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			LOG.debug("5. verify account key");
			//4. Account key
			mockResponse = getResponse("1/accts/1/platformAdminOnly", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			LOG.debug("6. verify mismathced account key");
			//4.1 mismatching Account key
			mockResponse = getResponse("2/accts/1/platformAdminOnly", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			LOG.debug("7. verify hierarchy account key");
			//5. Account hierarchy key
			mockResponse = getResponse("1/accts/10745/platformAdminOnly", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			LOG.debug("8. verify mismatched hierarchy account key");
			//5.1 mismatching Account hierarchy key
			mockResponse = getResponse("1/accts/10277/platformAdminOnly", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testOperatorReadWriteOnly() {
		try {
			//1. PlatformAdmin key
			HttpResponse mockResponse = getResponse("2/accts/1/operatorReadWriteOnly", "a5ef2f82-44d0-4377-a9eb-fadfdf578177");
			int responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//2. matching operator WR key
			mockResponse = getResponse("1/accts/1/operatorReadWriteOnly", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//2.1 mismatching operator key, operator=2 and key is of operator=1
			mockResponse = getResponse("2/accts/1/operatorReadWriteOnly", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//3. Operator Read-only key
			mockResponse = getResponse("1/accts/1/operatorReadWriteOnly", "4240449e-5a70-4be9-a953-48e0650cc137");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//4. Account key
			mockResponse = getResponse("1/accts/1/operatorReadWriteOnly", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//4.1 mismatching Account key
			mockResponse = getResponse("2/accts/1/operatorReadWriteOnly", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//5. Account hierarchy key
			mockResponse = getResponse("1/accts/10745/operatorReadWriteOnly", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//5.1 mismatching Account hierarchy key
			mockResponse = getResponse("1/accts/10277/operatorReadWriteOnly", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPAdminNOperatorWR() {
		try {
			//1. PlatformAdmin key
			HttpResponse mockResponse = getResponse("1/accts/1/pAdminNOperatorWR", "a5ef2f82-44d0-4377-a9eb-fadfdf578177");
			int responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//2. matching operator WR key
			mockResponse = getResponse("1/accts/1/pAdminNOperatorWR", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//2.1 mismatching operator key, operator=2 and key is of operator=1
			mockResponse = getResponse("2/accts/1/pAdminNOperatorWR", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//3. Operator Read-only key
			mockResponse = getResponse("1/accts/1/pAdminNOperatorWR", "4240449e-5a70-4be9-a953-48e0650cc137");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//4. Account key
			mockResponse = getResponse("1/accts/1/pAdminNOperatorWR", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//4.1 mismatching Account key
			mockResponse = getResponse("2/accts/1/pAdminNOperatorWR", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//5. Account hierarchy key
			mockResponse = getResponse("1/accts/10745/pAdminNOperatorWR", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//5.1 mismatching Account hierarchy key
			mockResponse = getResponse("1/accts/10277/pAdminNOperatorWR", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPAdminNOperators() {
		try {
			//1. PlatformAdmin key
			HttpResponse mockResponse = getResponse("2/accts/1/pAdminNOperators", "a5ef2f82-44d0-4377-a9eb-fadfdf578177");
			int responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//2. matching operator WR key
			mockResponse = getResponse("1/accts/1/pAdminNOperators", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//2.1 mismatching operator key, operator=2 and key is of operator=1
			mockResponse = getResponse("2/accts/1/pAdminNOperators", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//3. Operator Read-only key
			mockResponse = getResponse("1/accts/1/pAdminNOperators", "4240449e-5a70-4be9-a953-48e0650cc137");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//4. Account key
			mockResponse = getResponse("1/accts/1/pAdminNOperators", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//4.1 mismatching Account key
			mockResponse = getResponse("2/accts/1/pAdminNOperators", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//5. Account hierarchy key
			mockResponse = getResponse("1/accts/10745/pAdminNOperators", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//5.1 mismatching Account hierarchy key
			mockResponse = getResponse("1/accts/10277/pAdminNOperators", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testAdminsOperatorsNAccount() {
		try {
			//1. PlatformAdmin key
			HttpResponse mockResponse = getResponse("1/accts/1/adminsOperatorsNAccount", "a5ef2f82-44d0-4377-a9eb-fadfdf578177");
			int responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//2. matching operator WR key
			mockResponse = getResponse("1/accts/1/adminsOperatorsNAccount", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//2.1 mismatching operator key, operator=2 and key is of operator=1
			mockResponse = getResponse("2/accts/1/adminsOperatorsNAccount", "5601fdce-479f-11e3-ba4f-91669eef5b80");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//3. Operator Read-only key
			mockResponse = getResponse("1/accts/1/adminsOperatorsNAccount", "4240449e-5a70-4be9-a953-48e0650cc137");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//4. Account key
			mockResponse = getResponse("1/accts/1/adminsOperatorsNAccount", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//4.1 mismatching Account key
			mockResponse = getResponse("2/accts/1/adminsOperatorsNAccount", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
			//5. Account hierarchy key
			mockResponse = getResponse("1/accts/10745/adminsOperatorsNAccount", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 200);
			//5.1 mismatching operator and Account hierarchy key
			mockResponse = getResponse("1/accts/10277/adminsOperatorsNAccount", "3965e581-120d-11e2-8fb3-6362753ec2a5");
			responseCode = mockResponse.getStatusLine().getStatusCode();
			Assert.assertTrue(responseCode == 401);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
