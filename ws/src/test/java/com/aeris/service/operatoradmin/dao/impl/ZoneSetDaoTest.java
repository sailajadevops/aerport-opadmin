package com.aeris.service.operatoradmin.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.aeris.service.aeraccountmgmt.helper.BaseTest;
import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.dao.IZoneSetDAO;
import com.aeris.service.operatoradmin.exception.DuplicateZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetNotFoundException;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZonePreference;
import com.aeris.service.operatoradmin.model.ZoneSet;
import com.google.inject.Injector;
import org.junit.Ignore;

@FixMethodOrder (MethodSorters.NAME_ASCENDING)
@Ignore
public class ZoneSetDaoTest {
	protected static Injector injector;
	public static IZoneSetDAO zoneSetDao ;
	static int zoneSetId1,zoneSetId2,zoneSetId3,zoneSetId4,zoneSetId5;
	
	@BeforeClass
	public static void init(){
		System.setProperty(DBConstant.APPL_CONFIG_ENV,"local");
        OperatorAdminListener operatorAdminListener=new OperatorAdminListener();
        operatorAdminListener.contextInitialized(null);
        injector = operatorAdminListener.getInjector();
        
		zoneSetDao = injector.getInstance(ZoneSetDAO.class);
	}
	/*
	@Ignore
	//@Test
	public void test1_CreateZoneSet(){
		ZoneSet zoneSet = null;
		try {
			zoneSet = zoneSetDao.createNewZoneSet(2, 10, 1, "Test ZoneSet1 By Deepali", new Date(), "deepali.mulay@aeris.net",null);
			zoneSetId1 = zoneSet.getZoneSetId();
			System.out.println("zoneSetId1="+zoneSetId1);
		} catch (DuplicateZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetException e) {
			e.printStackTrace();
		}
		
		Assert.assertNotEquals(0, zoneSet.getZoneSetId());
		
		try {
			zoneSet = zoneSetDao.createNewZoneSet(2, 10, 0, "Test ZoneSet2 By Deepali", new Date(), "deepali.mulay@aeris.net",null);
			zoneSetId2 =  zoneSet.getZoneSetId();
			System.out.println("zoneSetId2="+zoneSetId2);
		} catch (DuplicateZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetException e) {
			e.printStackTrace();
		}
		
		Assert.assertNotEquals(0, zoneSet.getZoneSetId());
		
		
		try {
			zoneSet = zoneSetDao.createNewZoneSet(2, 6, 0, "Test ZoneSet3 By Deepali", new Date(), "deepali.mulay@aeris.net",null);
			zoneSetId3 =  zoneSet.getZoneSetId();
			System.out.println("zoneSetId3="+zoneSetId3);
		} catch (DuplicateZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetException e) {
			e.printStackTrace();
		}
		
		Assert.assertNotEquals(0, zoneSet.getZoneSetId());
		
		try {
			zoneSet = zoneSetDao.createNewZoneSet(1, 6, 0, "Test ZoneSet4 By Deepali", new Date(), "deepali.mulay@aeris.net",null);
			zoneSetId4 =  zoneSet.getZoneSetId();
			System.out.println("zoneSetId4="+zoneSetId4);
		} catch (DuplicateZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetException e) {
			e.printStackTrace();
		}
		
		Assert.assertNotEquals(0, zoneSet.getZoneSetId());
	}
	
	@Ignore
	//@Test
	public void test2_getZoneSet(){
		ZoneSet zoneSet = null;
		try {
			zoneSet = zoneSetDao.getZoneSet(2, zoneSetId2);
		} catch (ZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetNotFoundException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("Test ZoneSet2 By Deepali", zoneSet.getZoneSetName());
	}
	
	@Ignore
	//@Test
	public void test3_getZoneSetByAccountId(){
		List<ZoneSet> zoneSets = null;
		try {
			zoneSets = zoneSetDao.getZoneSets(2, 10, 1);
		} catch (ZoneSetException e) {
			e.printStackTrace();
		} 

		Assert.assertEquals("Test ZoneSet1 By Deepali", zoneSets.get(0).getZoneSetName());
	}
	
	@Ignore
	//@Test
	public void test4_UpdateZoneSet(){
		try {
			zoneSetDao.updateZoneSet(zoneSetId1, 2, 10, 1, "Test New ZoneSet1 By Deepali", new Date(), "deepali.mulay@aeris.net");
		} catch (DuplicateZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetNotFoundException e) {
			e.printStackTrace();
		} catch (ZoneSetException e) {
			e.printStackTrace();
		}
		
		ZoneSet zoneSet = null;
		try {
			zoneSet = zoneSetDao.getZoneSet(2, zoneSetId1);
		} catch (ZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetNotFoundException e) {
			e.printStackTrace();
		}
		Assert.assertEquals("Test New ZoneSet1 By Deepali", zoneSet.getZoneSetName());
	}
	
	
	@Ignore
	//@Test
	public void test5_getZoneSet(){
		List<ZoneSet> zoneSets = null;
		try {
			zoneSets = zoneSetDao.getZoneSets(2, 10);
		} catch (ZoneSetException e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals(2, zoneSets.size());
	}
	
	@Ignore
	//@Test
	public void test6_getAllZoneSet(){
		List<ZoneSet> zoneSets = null;
		try {
			zoneSets = zoneSetDao.getAllZoneSets(2);
		} catch (ZoneSetException e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals(3, zoneSets.size());
	}
	
	@Ignore
	//@Test
	public void test7_createZoneSetAndZones() throws Exception{
		List<Zone> zones = new ArrayList();
		
		EnrolledServices services = new EnrolledServices();
		services.setAllowMOSms(false);
		services.setAllowMOVoice(false);
		services.setAllowMTSms(false);
		services.setAllowMTVoice(false);
		services.setAllowPacket(false);
		
		IncludedPlanDetails includedPlan = new IncludedPlanDetails();
		includedPlan.setIncludedMOSms(0);
		includedPlan.setIncludedMOVoiceMins(0);
		includedPlan.setIncludedMTSms(0);
		includedPlan.setIncludedMTVoiceMins(0);
		includedPlan.setIncludedPacketKB(0);
		includedPlan.setPerKBPacketPrice(0);
		includedPlan.setPerMOSmsPrice(0);
		includedPlan.setPerMTSmsPrice(0);
		includedPlan.setPerMinMOVoicePrice(0);
		includedPlan.setPerMinMTVoicePrice(0);
		
		List<String> operators = new ArrayList<String>(Arrays.asList("1,2,3,4"));
		List<String> countries = new ArrayList<String>(Arrays.asList("4,5,6,7"));
		List<ZonePreference> zonePreferences = new ArrayList<ZonePreference>();
		zonePreferences.add(ZonePreference.HOME);
		Zone zone = new Zone();
		zone.setZoneName("TestZone1 by Deepali");
		zone.setOperators(operators);
		zone.setGeographies(countries);
		zone.setServices(services);
		zone.setIncludedPlan(includedPlan);
		zone.setZonePreferences(zonePreferences);
		zone.setTechnology("CDMA");

		zones.add(zone);
		
		zone = new Zone();
		zone.setZoneName("TestZone2 by Deepali");
		zone.setOperators(operators);
		zone.setGeographies(countries);
		zone.setServices(services);
		zone.setIncludedPlan(includedPlan);
		zone.setZonePreferences(zonePreferences);
		zone.setTechnology("CDMA");
		
		zones.add(zone);
		
		ZoneSet zoneSet = zoneSetDao.createNewZoneSet(2, 10, 1, "Test ZoneSet1 By Deepali", new Date(), "deepali.mulay@aeris.net", zones);
		zoneSetId5 = zoneSet.getZoneSetId();
	}
	
	@Ignore
	//@Test
	public void test8_deleteZoneSet(){
		try {
			zoneSetDao.deleteZoneSet(2, zoneSetId1, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			zoneSetDao.deleteZoneSet(2, zoneSetId2, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			zoneSetDao.deleteZoneSet(2, zoneSetId3, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			zoneSetDao.deleteZoneSet(1, zoneSetId4, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			zoneSetDao.deleteZoneSet(2, zoneSetId5, new Date(), "deepali.mulay@aeris.net");
		} catch (ZoneSetException e) {
			e.printStackTrace();
		} catch (ZoneSetNotFoundException e) {
			e.printStackTrace();
		}
	}
	@Ignore
	//@Test(expected=ZoneSetException.class)
	public void test8_getZoneSetWhichDoesNotExists() throws ZoneSetException, ZoneSetNotFoundException{
			ZoneSet zoneSet = zoneSetDao.getZoneSet(2, zoneSetId2);
	}*/
}
