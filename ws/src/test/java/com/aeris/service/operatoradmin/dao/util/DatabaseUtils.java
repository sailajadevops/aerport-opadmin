package com.aeris.service.operatoradmin.dao.util;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.model.RatePlanPoolStatus;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.utils.DBUtils;

public class DatabaseUtils {

	private static final String DELETE_ACCOUNT_RATE_PLAN_MAP = "DELETE FROM aerisgen.account_rate_plan_map WHERE rate_plan_id = ?";
	private static final String DELETE_ACCOUNT_RATE_PLAN_V1 = "DELETE from account_rate_plan_v1 WHERE csp_rate_plan_id = ?";
	private static final String DELETE_PLAN_ELEMENT = "DELETE from plan_element WHERE rate_plan_id = ?";
	private static final String DELETE_CSP_RATE_PLAN = "DELETE from csp_rate_plan where rate_plan_id = ?";
	private static final String DELETE_RATE_PLAN_TIER = "DELETE from aerisgen.rate_plan_tier where rate_plan_id = ?";
	private static final String DELETE_RATE_PLAN_FEE = "DELETE from aerisgen.rate_plan_fee where rate_plan_id = ?";
	private static final String DELETE_RATE_PLAN_TRIGGER = "DELETE from aerisgen.rate_plan_trigger where rate_plan_id = ?";
	private static final String DELETE_RATE_PLAN_ZONE_RATING_POLICY = "DELETE from aerisgen.rate_plan_zone_rating_policy where rate_plan_id = ?";
	private static final String DELETE_RATE_PLAN = "DELETE from aerisgen.rate_plan where rate_plan_id = ?";
	private static final String DELETE_CSP_RATE_PLAN_TO_ZONE_MAP = "DELETE from aerisgen.CSP_RATE_PLAN_TO_ZONE_MAP where rate_plan_id = ?";
	private static final String DELETE_H_ACCOUNT_RATE_PLAN_MAP = "DELETE from aerisgen.H_ACCOUNT_RATE_PLAN_MAP where rate_plan_id = ?";
	private static final String DELETE_H_RATE_PLAN_TRIGGER = "DELETE from aerisgen.H_RATE_PLAN_TRIGGER where rate_plan_id = ?";
	private static final String DELETE_H_RATE_PLAN_ZONE_RATING_POLICY = "DELETE from aerisgen.h_rate_plan_zone_rating_policy where rate_plan_id = ?";
	private static final String DELETE_H_RATE_PLAN_FEE = "DELETE from aerisgen.h_rate_plan_fee where rate_plan_id = ?";
	private static final String DELETE_H_RATE_PLAN = "DELETE from aerisgen.h_rate_plan where rate_plan_id = ?";
	
	
	public static void hardDeleteRatePlan(long ratePlanId) {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(DELETE_H_ACCOUNT_RATE_PLAN_MAP);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();
			
			ps = conn.prepareStatement(DELETE_H_RATE_PLAN_TRIGGER);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_H_RATE_PLAN_ZONE_RATING_POLICY);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_H_RATE_PLAN_FEE);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_H_RATE_PLAN);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();


			ps = conn.prepareStatement(DELETE_ACCOUNT_RATE_PLAN_MAP);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();
			
			ps = conn.prepareStatement(DELETE_ACCOUNT_RATE_PLAN_V1);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_PLAN_ELEMENT);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();
			
			ps = conn.prepareStatement(DELETE_CSP_RATE_PLAN_TO_ZONE_MAP);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_CSP_RATE_PLAN);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_RATE_PLAN_TIER);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();
			
			ps = conn.prepareStatement(DELETE_RATE_PLAN_FEE);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_RATE_PLAN_TRIGGER);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_RATE_PLAN_ZONE_RATING_POLICY);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();
			
			ps = conn.prepareStatement(DELETE_RATE_PLAN);
			ps.setLong(1, ratePlanId);
			ps.executeUpdate();
			
			conn.commit();
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ex.printStackTrace();
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DBUtils.cleanup(conn, ps, null);
		}
	}
	
	public static boolean isRatePlanAssignedInARPM(long ratePlanId, long accountId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int n = 0;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement("select count(*) cnt from account_rate_plan_map where account_id = ? and rate_plan_id = ? and status_id = " + RatePlanStatus.APPROVED.getValue() + " and trunc(end_date) = to_date('12-31-2099','MM-DD-YYYY')");
			ps.setLong(1, accountId);
			ps.setLong(2, ratePlanId);
			rs = ps.executeQuery();
			if (rs.next()) {
				n = rs.getInt("cnt");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			fail("SQLException in validating entry in DB");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return (n == 1);
	}

	public static boolean isRatePlanExpiredInARPM(long ratePlanId, long accountId, String expireDateStr) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int n = 0;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement("select count(*) cnt from account_rate_plan_map where account_id = ? and rate_plan_id = ? and status_id = " + RatePlanStatus.EXPIRED.getValue() + " and trunc(end_date) = to_date('" + expireDateStr + "','MM-DD-YYYY')");
			ps.setLong(1, accountId);
			ps.setLong(2, ratePlanId);
			rs = ps.executeQuery();
			if (rs.next()) {
				n = rs.getInt("cnt");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			fail("SQLException in validating entry in DB");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return (n == 1);
	}
	
	public static boolean isPoolAssignedInAP(long poolId, long accountId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int n = 0;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement("select count(*) cnt from aerisgen.account_pool where account_id = ? and pool_id = ? and status_id =  " + RatePlanPoolStatus.PENDING.getValue() + "  and trunc(end_date) = to_date('12-31-2099','MM-DD-YYYY')");
			ps.setLong(1, accountId);
			ps.setLong(2, poolId);
			rs = ps.executeQuery();
			if (rs.next()) {
				n = rs.getInt("cnt");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			fail("SQLException in validating entry in DB");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return (n == 1);
	}
	
	public static boolean isRatePlanAssignedInARPPM(long poolId, long accountId, List<Long> ratePlanIds) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int n = 0;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			String QUERY_RATE_PLAN_IDS = "select count(*) cnt from aerisgen.account_rate_plan_pool_map where pool_id = ? and rate_plan_id in (:ratePlanIds) and trunc(end_date) = to_date('12-31-2099','MM-DD-YYYY')";
			
			StringBuilder ratePlanIdsStrBuilder = new StringBuilder();
			for (Long ratePlanId : ratePlanIds) {
				ratePlanIdsStrBuilder.append(ratePlanId).append(",");
			}
			String ratePlanIdsStr = ratePlanIdsStrBuilder.substring(0, ratePlanIdsStrBuilder.length()-1);
			String query = QUERY_RATE_PLAN_IDS.replace(":ratePlanIds", ratePlanIdsStr);
			
			
			ps = conn.prepareStatement(query);
			ps.setLong(1, poolId);
			rs = ps.executeQuery();
			if (rs.next()) {
				n = rs.getInt("cnt");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			fail("SQLException in validating entry in DB");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return (n == ratePlanIds.size());
	}
	
	public static boolean isPoolApprovedInAP(long poolId, long accountId) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int n = 0;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement("select count(*) cnt from aerisgen.account_pool where account_id = ? and pool_id = ? and status_id =  " + RatePlanPoolStatus.APPROVED.getValue() + "  and trunc(end_date) = to_date('12-31-2099','MM-DD-YYYY')");
			ps.setLong(1, accountId);
			ps.setLong(2, poolId);
			rs = ps.executeQuery();
			if (rs.next()) {
				n = rs.getInt("cnt");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			fail("SQLException in validating entry in DB");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return (n == 1);
	}
	
	public static boolean isPoolExpiredInAP(long poolId, long accountId, String expireDateStr) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int n = 0;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement("select count(*) cnt from aerisgen.account_pool where account_id = ? and pool_id = ? and status_id = " + RatePlanPoolStatus.EXPIRED.getValue() + " and trunc(end_date) = to_date('" + expireDateStr + "','MM-DD-YYYY')");
			ps.setLong(1, accountId);
			ps.setLong(2, poolId);
			rs = ps.executeQuery();
			if (rs.next()) {
				n = rs.getInt("cnt");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			fail("SQLException in validating entry in DB");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return (n == 1);
	}
	
	public static boolean isRatePlanExpiredInARPPM(long poolId, long accountId, List<Long> ratePlanIds, String expireDateStr) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int n = 0;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			String QUERY_RATE_PLAN_IDS = "select count(*) cnt from aerisgen.account_rate_plan_pool_map where pool_id = ? and rate_plan_id in (:ratePlanIds) and trunc(end_date) = to_date('" + expireDateStr + "','MM-DD-YYYY')";
			
			StringBuilder ratePlanIdsStrBuilder = new StringBuilder();
			for (Long ratePlanId : ratePlanIds) {
				ratePlanIdsStrBuilder.append(ratePlanId).append(",");
			}
			String ratePlanIdsStr = ratePlanIdsStrBuilder.substring(0, ratePlanIdsStrBuilder.length()-1);
			String query = QUERY_RATE_PLAN_IDS.replace(":ratePlanIds", ratePlanIdsStr);
			
			
			ps = conn.prepareStatement(query);
			ps.setLong(1, poolId);
			rs = ps.executeQuery();
			if (rs.next()) {
				n = rs.getInt("cnt");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
			fail("SQLException in validating entry in DB");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return (n == ratePlanIds.size());
	}

	
	private static final String DELETE_T_ACCOUNT_RATE_POOL_MAP = "DELETE FROM aerbill_prov.t_account_rate_pool_map WHERE pool_name = ? AND account_id = ? AND rate_plan in (:rateplannames)";
	
	private static final String DELETE_T_POOL = "DELETE FROM aerbill_prov.t_pool WHERE pool_id = ?";
	
	private static final String DELETE_ACCOUNT_RATE_PLAN_POOL_MAP = "DELETE FROM aerisgen.account_rate_plan_pool_map WHERE pool_id = ?";
	
	private static final String DELETE_ACCOUNT_POOL = "DELETE FROM aerisgen.account_pool WHERE pool_id = ?";
	
	public static void hardDeletePool(long accountId, long poolId, String poolName, List<String> ratePlanNames) {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);
			
			StringBuilder ratePlanNamesStrBuilder = new StringBuilder();
			for (String ratePlanName : ratePlanNames) {
				ratePlanNamesStrBuilder.append("'").append(ratePlanName).append("',");
			}
			String ratePlanNamesStr = ratePlanNamesStrBuilder.substring(0, ratePlanNamesStrBuilder.length()-1);
			String query = DELETE_T_ACCOUNT_RATE_POOL_MAP.replace(":rateplannames", ratePlanNamesStr);
			System.out.println("Query = " + query);

			ps = conn.prepareStatement(query);
			ps.setString(1, poolName);
			ps.setLong(2, accountId);
			ps.executeUpdate();
			
			ps = conn.prepareStatement(DELETE_T_POOL);
			ps.setLong(1, poolId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_ACCOUNT_RATE_PLAN_POOL_MAP);
			ps.setLong(1, poolId);
			ps.executeUpdate();

			ps = conn.prepareStatement(DELETE_ACCOUNT_POOL);
			ps.setLong(1, poolId);
			ps.executeUpdate();

			
			conn.commit();
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ex.printStackTrace();
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DBUtils.cleanup(conn, ps, null);
		}

	}

}
