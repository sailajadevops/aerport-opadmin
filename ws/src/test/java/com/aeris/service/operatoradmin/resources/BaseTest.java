package com.aeris.service.operatoradmin.resources;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;

/**
 * Base test class which initializes embedded opadmin context for sub-classes to run junit tests 
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class BaseTest {
	 
    protected static Server server;
    protected static final String OPADMIN_BASE_PATH = "http://localhost:8087/operatoradmin_ws_2_0/operators/" ; 

    @BeforeClass
    public static void startServer() throws Exception {
    	server = new Server(8087);
    	System.setProperty("app.config.env", "qa");
    	server.setStopAtShutdown(true);
    	WebAppContext webAppContext = new WebAppContext();
    	webAppContext.setContextPath("/operatoradmin_ws_2_0");
    	webAppContext.setResourceBase("src/main/webapp");       
    	webAppContext.setClassLoader(BaseTest.class.getClassLoader());
    	server.addHandler(webAppContext);
    	server.start();
    }
    
    
    protected HttpResponse getResponse(String subResource, String apiKey) throws IOException, ClientProtocolException {
		HttpClient client = new DefaultHttpClient();
		StringBuilder reqPath = new StringBuilder(OPADMIN_BASE_PATH);
		reqPath.append(subResource).append("?apiKey=").append(apiKey);
		HttpGet mockRequest = new HttpGet( reqPath.toString());
		HttpResponse mockResponse = client.execute(mockRequest);
		return mockResponse;
	}
    

    @AfterClass
    public static void shutdownServer() throws Exception {
        server.stop();
    }
}