package com.aeris.service.operatoradmin.dao.impl;

import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import com.aeris.service.aeraccountmgmt.helper.BaseTest;
import com.aeris.service.operatoradmin.dao.IZoneSetDAO;
import com.aeris.service.operatoradmin.dao.util.DatabaseUtils;
import com.aeris.service.operatoradmin.exception.RatePlanNotFoundException;
import com.aeris.service.operatoradmin.model.AccountRatePlan;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanPaymentType;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.RatePlanType;
import com.aeris.service.operatoradmin.model.ZoneSet;

@Ignore
public class RatePlanDaoTest extends BaseTest {
	@Rule public TestName name = new TestName();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
	private static RatePlan globalSimSharedRatePlan = null;
	private static final long ACCOUNT_ID = 1;
	private static final int OPERATOR_ID = 1;
	private static final int GLOBAL_SIM = 2;
	private static IZoneSetDAO zoneSetDAO = null;
	private static int zoneSetId ;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		zoneSetDAO = injector.getInstance(ZoneSetDAO.class);
		RatePlanDAO ratePlanDao = injector.getInstance(RatePlanDAO.class);
		
		ZoneSet zoneSet = zoneSetDAO.createNewZoneSet(OPERATOR_ID, GLOBAL_SIM, 1, "Test ZoneSet123", new Date(), "test@aeris.net",null);
		zoneSetId = zoneSet.getZoneSetId();
		
		RatePlanFeeDetails ratePlanFeeDetails = new RatePlanFeeDetails();
		ratePlanFeeDetails.setAccessFee(1);
		RatePlan ratePlan = ratePlanDao.createNewRatePlan(OPERATOR_ID, GLOBAL_SIM, "RatePlanDaoTest" + System.currentTimeMillis()
				, RatePlanType.NON_TIERED, RatePlanPaymentType.POST_PAID, RatePlanAccessType.SHARED, RatePlanPeriodType.MONTHLY
				, 1, true, RatePlanStatus.APPROVED, false
				, 1, 485, "test", "test", 82
				, 0, new Date(), sdf.parse("12-31-2099"), "USD", 1
				, 1, 1, ratePlanFeeDetails
				, null, null, null
				, null, new Date(), "RatePlanDaoTest", 500, 45,
				45, 1, 1,
				0, null, false, zoneSetId, "GSM", false,null,false);
		
		Assert.assertNotNull(ratePlan);
		Assert.assertTrue(ratePlan.getRatePlanId() > 0);
		
		System.out.println("RatePlan Name = " + ratePlan.getRatePlanName());
		Assert.assertTrue(ratePlan.getRatePlanName().contains("GLOBSIM"));
		
		globalSimSharedRatePlan = ratePlan;
	}
	
	@AfterClass
    public static void oneTimeTearDown() throws Exception {
		RatePlanDAO ratePlanDao = injector.getInstance(RatePlanDAO.class);
		boolean expired = ratePlanDao.updateRatePlanStatus(OPERATOR_ID, globalSimSharedRatePlan.getRatePlanId(), RatePlanStatus.EXPIRED, "test", new Date(), "RatePlanDaoTest", null);
		Assert.assertTrue(expired);
		DatabaseUtils.hardDeleteRatePlan(globalSimSharedRatePlan.getRatePlanId());
		
		zoneSetDAO.deleteZoneSet(OPERATOR_ID, zoneSetId, new Date(), "test@aeris.net");
    }

	@Before
	public void setUp() throws Exception {
		System.out.println("Setup Name = " + name.getMethodName());
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Teardown Name = " + name.getMethodName());
	}
	
		
	private boolean testAssignAndExpireSharedGlobalSIMRatePlan() throws Exception {
		RatePlanDAO ratePlanDao = injector.getInstance(RatePlanDAO.class);
		long ratePlanId = globalSimSharedRatePlan.getRatePlanId();
		
		boolean assigned = ratePlanDao.assignRatePlanToAccount(OPERATOR_ID, ACCOUNT_ID, ratePlanId, 82, new Date(), sdf.parse("12-31-2099"), "test", new Date(), "RatePlanDaoTest");
		Assert.assertTrue(assigned);
		
		AccountRatePlan accountRatePlan = ratePlanDao.getAccountRatePlan(OPERATOR_ID, ACCOUNT_ID, ratePlanId);
		Assert.assertNotNull(accountRatePlan);
		
		Assert.assertTrue(DatabaseUtils.isRatePlanAssignedInARPM(ratePlanId, ACCOUNT_ID));
		
		Date expireDate = new Date();
		String expireDateStr = sdf.format(expireDate);
		
		boolean invalidated = ratePlanDao.invalidateAccountRatePlan(OPERATOR_ID, ACCOUNT_ID, ratePlanId, expireDate, "RatePlanDaoTest");
		Assert.assertTrue(invalidated);
		
		try {
			accountRatePlan = ratePlanDao.getAccountRatePlan(OPERATOR_ID, ACCOUNT_ID, ratePlanId);
			fail("Fail to expire account rate plan");
		} catch (RatePlanNotFoundException ex) {
			//Expected exception
		}
		
		Assert.assertTrue(DatabaseUtils.isRatePlanExpiredInARPM(ratePlanId, ACCOUNT_ID, expireDateStr));
		
		return true;
	}
	
	@Test
	public void testAssignAndExpireTwiceSharedGlobalSIMRatePlan() throws Exception {
		boolean ret = testAssignAndExpireSharedGlobalSIMRatePlan();
		Assert.assertTrue(ret);
		ret = testAssignAndExpireSharedGlobalSIMRatePlan();
		Assert.assertTrue(ret);
	}
	
	@Test
	public void testAssignAndExpireOnceSharedGlobalSIMRatePlan() throws Exception {
		boolean ret = testAssignAndExpireSharedGlobalSIMRatePlan();
		Assert.assertTrue(ret);
	}

}
