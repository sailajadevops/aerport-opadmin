package com.aeris.service.salesagentmgmt;

import java.io.BufferedReader;

import org.junit.BeforeClass;
import org.junit.Test;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.guice.ServiceModule;
import com.aeris.service.operatoradmin.s3.S3AccessManager;
import com.google.inject.Guice;
import com.google.inject.Injector;

public class S3FileReaderTest { 
	
	private static Injector injector;
	private static S3AccessManager s3AccessManager ;
	
	
	@BeforeClass
    public static void init() {
        System.setProperty(DBConstant.APPL_CONFIG_ENV,"test");
        injector = Guice.createInjector(new ServiceModule());
        s3AccessManager = injector.getInstance(S3AccessManager.class);
    }
	
	
	@Test
	public void testReadFileFromS3() throws Exception {
		BufferedReader reader =  null ;
		try {
			reader = s3AccessManager.readFromS3("WebServices/operatoradmin/distributors/1/CONTRACT_TERM_Complete.csv");
			String line;
			while((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			s3AccessManager.shutdownAmazonClient();
		}
	}

}
