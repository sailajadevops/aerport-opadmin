package com.aeris.service.salesagentmgmt;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.dao.ISalesAgentDAO;
import com.aeris.service.operatoradmin.dao.impl.SalesAgentDAO;
import com.aeris.service.operatoradmin.model.SalesAgent;
import com.aeris.service.operatoradmin.utils.Constants;
import com.google.inject.Injector;

//@Ignore
public class SalesAgentDAOTest {
	private static final Logger LOG = LoggerFactory
			.getLogger(SalesAgentDAOTest.class);
	private static ISalesAgentDAO dao = null;
	private static final String operatorId = "1";
	private static final String agentId = "TestSalesAgent_"
			+ System.currentTimeMillis();
	private static final String user = "test@aeris.net";
    private static Injector injector;
    @BeforeClass
    public static void init() {
        System.setProperty(DBConstant.APPL_CONFIG_ENV,"test");
        OperatorAdminListener operatorAdminListener=new OperatorAdminListener();
        operatorAdminListener.contextInitialized(null);
        injector = operatorAdminListener.getInjector();
        dao = injector
		.getInstance(SalesAgentDAO.class);
    }
	@Test
	public void testCreateSalesAgent() {
		SalesAgent agent = new SalesAgent();
		agent.setAgentId(agentId);
		agent.setAgentType("Group");
		agent.setCompany("Aeris");
		agent.setCreatedBy(user);
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT));
		agent.setStartDate(new Date(c.getTimeInMillis()));
		c.add(Calendar.DATE, 2);
		agent.setEndDate(new Date(c.getTimeInMillis()));
		agent.setFirstName("XYZ");
		agent.setLastName("ZYX");
		agent.setNotUsed(0);
		SalesAgent createdAgent = dao.createSalesAgent(agent, operatorId);
		Assert.assertNotNull(createdAgent);
		Assert.assertNotNull(createdAgent.getId());
		Assert.assertNotEquals(0, createdAgent.getId());
		Assert.assertEquals(agentId, createdAgent.getAgentId());
		LOG.info("Sales Agent successfully created: \n{}", createdAgent);
	}

	@Test
	public void testUpdateSalesAgent() {
		SalesAgent agent = new SalesAgent();
		agent.setAgentId(agentId);
		agent.setAgentType("Self");
		agent.setCompany("Aeris.com");
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT));
		Date startDate = new Date(c.getTimeInMillis());
		agent.setStartDate(startDate);
		c.add(Calendar.DATE, 2);
		Date endDate = new Date(c.getTimeInMillis());
		agent.setEndDate(endDate);
		agent.setFirstName("AgentFirstName");
		agent.setLastName("AgentLastName");
		agent.setNotUsed(0);
		agent.setLastModifiedBy(user);
		SalesAgent updatedAgent = dao.updateSalesAgent(agent, operatorId);
		Assert.assertNotNull(updatedAgent);
		Assert.assertNotNull(updatedAgent.getId());
		Assert.assertNotEquals(0, updatedAgent.getId());
		Assert.assertEquals(agentId, updatedAgent.getAgentId());
		Assert.assertEquals("Aeris.com", updatedAgent.getCompany());
		Assert.assertEquals("Self", updatedAgent.getAgentType());
		Assert.assertEquals(startDate.getTime(), updatedAgent.getStartDate().getTime());
		Assert.assertEquals(endDate.getTime(), updatedAgent.getEndDate().getTime());
		Assert.assertEquals("AgentFirstName", updatedAgent.getFirstName());
		Assert.assertEquals("AgentLastName", updatedAgent.getLastName());
		LOG.info("Sales Agent successfully updated: \n{}", updatedAgent);
	}

	@Test
	public void testGetSalesAgent() {
		SalesAgent agent = dao.getSalesAgent(agentId, operatorId);
		Assert.assertNotNull(agent);
		Assert.assertEquals(agentId, agent.getAgentId());
		LOG.info("Sales Agents found: \n{}", agent);
	}

	@Test
	public void testGetAllSalesAgents() {
		List<SalesAgent> agents = dao.getAllSalesAgents(operatorId, false);
		Assert.assertNotNull(agents);
		LOG.info("Total {} Sales Agents found for operatorId {} are: \n{}",
				agents.size(), operatorId, agents);
	}

	@Test
	public void testDeleteSalesAgent() {

		boolean result = dao.deleteSalesAgent(agentId, user, operatorId);
		Assert.assertTrue(result);
		LOG.info("Sales Agent with agentId: {} deleted successfully: \n{}",
				agentId);
	}

}
