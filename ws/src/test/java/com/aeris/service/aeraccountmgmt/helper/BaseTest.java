package com.aeris.service.aeraccountmgmt.helper;

import org.junit.BeforeClass;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.google.inject.Injector;

public class BaseTest {
    protected static Injector injector;
    @BeforeClass
    public static void init() {
        System.setProperty(DBConstant.APPL_CONFIG_ENV,"test");
        OperatorAdminListener operatorAdminListener=new OperatorAdminListener();
        operatorAdminListener.contextInitialized(null);
        injector = operatorAdminListener.getInjector();
    }
}
