package com.aeris.service.aeraccountmgmt.helper;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.impl.AccountDAO;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.AccountStatus;
import com.aeris.service.operatoradmin.model.AccountType;
import com.aeris.service.operatoradmin.model.ApprovalStatus;
import com.aeris.service.operatoradmin.model.BillableStatus;
import com.aeris.service.operatoradmin.model.BillingDetails;
import com.aeris.service.operatoradmin.model.Contact;
import com.aeris.service.operatoradmin.model.ContactType;
import com.aeris.service.operatoradmin.model.Distributor;
import com.aeris.service.operatoradmin.model.InvoiceAddress;
import com.aeris.service.operatoradmin.model.Region;
import com.aeris.service.operatoradmin.payload.CreateAccountRequest;



public class AccountDAOTest extends BaseTest {

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//@Test
	public void testGetAllAccounts() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAccount() {
		AccountDAO dao = new AccountDAO();
		Account acct = dao.getAccount("1", "1");
		Assert.assertNotNull(acct);
		Assert.assertEquals(acct.getAccountId(), 1);
		System.out.println("Account Name = " + acct.getAccountName());
	}

	//@Test
	public void testGetChildAccounts() {
		fail("Not yet implemented");
	}

	//@Test
	public void testGetContactsForAccount() {
		fail("Not yet implemented");
	}

	//@Test
	public void testGenerateNewAccountId() {
		List<Integer> operatorList = new ArrayList<Integer>();
		operatorList.add(1);operatorList.add(2);operatorList.add(3);
	    	for (int i = 0; i < operatorList.size() ; i++) {
				try {
					int operatorId = operatorList.get(i);
					AccountDAO dao = new AccountDAO();
					Connection conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
					long newAccountId = dao.generateNewAccountId(conn, operatorId);
					if(operatorId == 1) {
						Assert.assertTrue(newAccountId > 0 && newAccountId <= 39000);
					} else if (operatorId == 2) {
						Assert.assertTrue(newAccountId > 39000 && newAccountId <= 49000);
					} else if (operatorId == 3) {
						Assert.assertTrue(newAccountId > 49000 && newAccountId <= 59000);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	}
	
	@Test
	public void testGetSelectedAccounts() {
		fail("Not yet implemented");
	}

	//@Test
	public void testGenerateApiKey() {
		fail("Not yet implemented");
	}
	@Test
	public void testCreateNewAccountWholesale() {
		CreateAccountRequest request = new CreateAccountRequest();
		Contact primaryContact = new Contact();
		primaryContact.setEmail("shital.deshmukh@aeris.net");
		primaryContact.setFirstName("Shital");
		primaryContact.setFullName("");
		primaryContact.setJobTitle("");
		primaryContact.setLastName("deshmukh");
		primaryContact.setPhoneNumber("");
		primaryContact.setRole(ContactType.BILLING);
		request.setAccountName("WholesaleUTest"+System.currentTimeMillis());
		request.setBillableStatus(BillableStatus.NON_BILLABLE);
		request.setIncludedAlertProfiles(5);
		request.setMaxPortalAccounts(5);
		request.setMaxReportHistory(5);
		request.setMaxScheduledReports(5);
		request.setMyAlertsEmail(new String[]{"shital.deshmukh@aeris.net"});
		request.setNetSuiteId("1234");
		request.setPrimaryContact(primaryContact);
		request.setProductIds(Arrays.asList("1"));
		request.setRegion(Region.DOMESTIC);
		request.setScheduledReportsEmail(new String[]{"shital.deshmukh@aeris.net"});
		request.setServiceImpairmentEmail(new String[]{"shital.deshmukh@aeris.net"});
		request.setSpecialInstructions("");
		request.setStatus(AccountStatus.TRIAL);
		request.setTags("");
		request.setType(AccountType.APP_DEVELOPER);
		request.setUserId("shital.deshmukh@aeris.net");
		request.setVerticalId("4");
		request.setWebAddress("");		
		request.setAccountType("Wholesale");
		request.setEnableCreateWholesale(false); 
		Distributor distRequest = prepareDistributorRequest(); 
		String userId = request.getUserId();
		String accountName = request.getAccountName();
		//Contact primaryContact = request.getPrimaryContact();
		List<Contact> additionalContacts = request.getAdditionalContacts();
		BillingDetails billingDetails = request.getBillingDetails();
		List<String> productIds = request.getProductIds();
		AccountStatus status = request.getStatus();
		ApprovalStatus approvalStatus = request.getApprovalStatus();
		String parentAccountId = request.getParentAccountId();
		String carrierAccountId = request.getCarrierAccountId();
		String netSuiteId = request.getNetSuiteId();
		Region region = request.getRegion();
		AccountType accountType = request.getType();
		InvoiceAddress invoiceAddress = request.getInvoiceAddress();
		String parentLabel = request.getParentLabel();
		BillableStatus billableStatus = request.getBillableStatus();
		String specialInstructions = request.getSpecialInstructions();
		String tags = request.getTags();
		int includedAlertProfiles = request.getIncludedAlertProfiles();
		String verticalId = request.getVerticalId();
		int maxPortalAccounts = request.getMaxPortalAccounts();
		int maxReportHistory = request.getMaxReportHistory();
		int maxScheduledReports = request.getMaxScheduledReports();
		String[] serviceImpairmentEmail = request.getServiceImpairmentEmail();
		String[] myAlertsEmail = request.getMyAlertsEmail();
		String[] scheduledReportsEmail = request.getScheduledReportsEmail();
        String[] supportContactEmails = request.getSupportContactEmails();
		
		String webAddress = request.getWebAddress();

		Date requestedDate = new Date();
        
        String distributorId = request.getDistributorId();
        AccountDAO dao = new AccountDAO();
        try{
		Account account = dao.createNewAccount("1", accountName, region,
				accountType, status, approvalStatus, invoiceAddress,
				productIds, parentAccountId, parentLabel, carrierAccountId,
				primaryContact, additionalContacts, webAddress, billableStatus,
				billingDetails, netSuiteId, specialInstructions, tags,
				includedAlertProfiles, verticalId, maxPortalAccounts,
				maxScheduledReports, maxReportHistory, serviceImpairmentEmail,
				myAlertsEmail, scheduledReportsEmail, supportContactEmails,
				requestedDate, userId, request.getAccountOverageBucket(), 
				request.getAgentId(), distributorId,request.getAccountType(), request.isEnableCreateWholesale(), request.getWholesaleAccountId(),distRequest, 0);
		Assert.assertNotNull(account);
        }catch(Exception e){
        	e.fillInStackTrace();
        }
		
	}
	private Distributor prepareDistributorRequest () {
		Distributor distributor = new Distributor();
		//distributor.setDistributorId(3l);
		distributor.setAccountId(1l);
		distributor.setCreatedBy("shital.deshmukh@aeris.net");
		distributor.setLastModifiedBy("shital.deshmukh@aeris.net");
		distributor.setStatus(1);
		distributor.setCreatedDate(new java.sql.Date(System.currentTimeMillis()));
		distributor.setLastModifiedDate(new java.sql.Date(System.currentTimeMillis()));
		distributor.setRepTimestamp(new Timestamp(System.currentTimeMillis()));
		distributor.setLogoUrls("test.logo.url");
		distributor.setPrivacyPolicyUrls("privacy.policy.url");
		distributor.setDistributorName("test");
		distributor.setContactEmails(Arrays.asList("shital.deshmukh@aeris.net,test@aeris.net".split(",")));
		return distributor ;
	}
}
