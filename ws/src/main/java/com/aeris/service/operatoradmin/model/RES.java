package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class RES implements Serializable {
	private static final long serialVersionUID = 1L;

	private long RESId;
	private String RESName;
	private int productId;
	public long getRESId() {
		return RESId;
	}
	public void setRESId(long rESId) {
		RESId = rESId;
	}
	public String getRESName() {
		return RESName;
	}
	public void setRESName(String rESName) {
		RESName = rESName;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	
}
