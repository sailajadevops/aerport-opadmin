package com.aeris.service.operatoradmin.dao;

import com.aeris.service.operatoradmin.exception.ConnectionLimitException;
import com.aeris.service.operatoradmin.model.ConnectionLimit;

public interface ISMPPConnectionDAO {
	ConnectionLimit getConnectionLimit(long serviceCode) throws ConnectionLimitException;
	ConnectionLimit createConnectionLimit(long serviceCode, long connectionLimit) throws ConnectionLimitException;
	ConnectionLimit updateConnectionLimit(long serviceCode, long connectionLimit) throws ConnectionLimitException;
	boolean deleteConnectionLimit(long serviceCode) throws ConnectionLimitException;
}
