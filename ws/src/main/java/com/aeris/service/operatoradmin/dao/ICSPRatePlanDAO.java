package com.aeris.service.operatoradmin.dao;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.BadStartDateException;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.RatePlanException;
import com.aeris.service.operatoradmin.exception.RatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.SubRatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.ProvisionTriggerSettings;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanPaymentType;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.RatePlanTier;
import com.aeris.service.operatoradmin.model.RatePlanType;
import com.aeris.service.operatoradmin.model.SuspendTriggerSettings;
import com.aeris.service.operatoradmin.model.ZoneRatePlan;

public interface ICSPRatePlanDAO {
	long createNewRatePlan(int operatorId, int productId, String ratePlanName, RatePlanType ratePlanType, RatePlanPaymentType paymentType,
			RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded, RatePlanStatus status, int tierCriteria,
			int homeZoneId, String description, String specialNotes, int carrierRatePlanId, long accountId, Date startDate, Date endDate, String currencyName,
			int includedPeriodMonths, int devicePoolingPolicyId, int packetRoundingPolicyId, RatePlanFeeDetails ratePlanFeeDetails,
			ProvisionTriggerSettings provisionTriggerSettings, SuspendTriggerSettings suspendTriggerSettings, List<ZoneRatePlan> zoneRatePlanSettings,
			List<RatePlanTier> ratePlanTiers, boolean roamingIncluded, Date requestedDate, String requestedUser, int zoneSetId, String technology,  Connection conn) throws RatePlanException, DuplicateRatePlanException,
			ZoneNotFoundException, ZoneException;

	boolean updateRatePlan(int operatorId, long ratePlanId, int productId, String ratePlanName, RatePlanType ratePlanType, RatePlanPaymentType paymentType,
			RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded, RatePlanStatus status, int tierCriteria,
			int homeZoneId, String description, String specialNotes, int carrierRatePlanId, long accountId, Date startDate, Date endDate, String currencyName,
			int includedPeriodMonths, int devicePoolingPolicyId, int packetRoundingPolicyId, RatePlanFeeDetails ratePlanFeeDetails,
			ProvisionTriggerSettings provisionTriggerSettings, SuspendTriggerSettings suspendTriggerSettings, List<ZoneRatePlan> zoneRatePlanSettings,
			List<RatePlanTier> ratePlanTiers, boolean roamingIncluded, Date requestedDate, String requestedUser, int zoneSetId, String technology, Connection conn) throws RatePlanNotFoundException, RatePlanException,
			BadStartDateException, SubRatePlanNotFoundException, DuplicateRatePlanException, ZoneNotFoundException, DuplicateZoneException, ZoneException;

	String updateAndGetRatePlanNameUsingAlgo(int operatorId, int productId, long ratePlanId, Connection conn) throws RatePlanException;
}