package com.aeris.service.operatoradmin.s3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.model.Distributor;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 *
 * @author saurabh.sharma@aeris.net
 */
public class S3AccessManager implements IS3AccessManager {
    
	private static final Logger LOG = LoggerFactory.getLogger(S3AccessManager.class);
	
	@Inject
	@Named("access_key")
	private String accessKey;
	@Inject
	@Named("secret_key")
	private String secretKey;
	@Inject
	@Named("region")
	private String region;
	@Inject
	@Named("bucket_name")
	private String bucketName;
	@Inject
	@Named("s3_env_name")
	private String envName;
	@Inject
	@Named("s3.distributor.file.base.url")
	private String s3BaseUrl ;
	@Inject
	@Named ("distributor.file.appender")
	private String fileAppender;
	@Inject
	@Named("endpoint")
	private String endpoint;
	private AmazonS3 s3client;
	private TransferManager tx;
	
    @Inject
    public void init() {
		AWSCredentials awsCredentials = null;
		LOG.info("Loading AWS credentials");
		Preconditions.checkNotNull(accessKey,"AWS access key is NULL");
		Preconditions.checkNotNull(secretKey,"AWS secret key is NULL");
		awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
		this.s3client = new AmazonS3Client(awsCredentials);
        this.s3client.setEndpoint(endpoint);
        this.tx = new TransferManager(s3client);
	}
    
    
	public BufferedReader readFromS3(String bucketName, String key) throws IOException {
		S3Object s3object = s3client.getObject(new GetObjectRequest(bucketName, key));
		return new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
	}
	
	
	public BufferedReader readFromS3(String key) throws IOException {
		return readFromS3(bucketName, key);
	}
	
	
	public BufferedReader readFromS3(Distributor distributor, String baseUrl, String fileName) throws IOException {
		
		StringBuilder keyBuilder = new StringBuilder(StringUtils.isEmpty(baseUrl) ? baseUrl : s3BaseUrl);
		keyBuilder.append(distributor.getAccountId()).append("/").append(distributor.getDistributorId()).append("/").append(fileName);
		String extn = "." + fileAppender ;
		if(!fileName.endsWith(extn))
			keyBuilder.append(extn);
		return readFromS3(bucketName, keyBuilder.toString());
	}
	
	
	public List<String> getAllFilesForDistributor(long accountId, long distributorId) {
		StringBuilder keyBuilder = new StringBuilder();
		List<String> files = new ArrayList<String>();
		final ListObjectsRequest listObjectRequest = new ListObjectsRequest().
			    withBucketName(bucketName).
			    withPrefix(keyBuilder.toString());
			final ObjectListing objectListing = s3client.listObjects(listObjectRequest);
			for (S3ObjectSummary objectSummary: objectListing.getObjectSummaries()) {
		       files.add(objectSummary.getKey());
		    }
			return files ;
	}
	

	/**
     * Shutdown Amazon client
     */
    @Override
    public void shutdownAmazonClient(){
    	tx.shutdownNow();
    	if (s3client instanceof AmazonS3Client && null != s3client) {
            ((AmazonS3Client)s3client).shutdown();
        }
    }
    
}
