package com.aeris.service.operatoradmin.exception;

public class InvalidRatePlanPoolException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public InvalidRatePlanPoolException(String s) {
		super(s);
	}

	public InvalidRatePlanPoolException(String ex, Throwable t) {
		super(ex, t);
	}
}
