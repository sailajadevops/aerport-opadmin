package com.aeris.service.operatoradmin.resources;

import java.util.Date;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.ISprintCSADAO;
import com.aeris.service.operatoradmin.model.SprintCSA;
import com.aeris.service.operatoradmin.payload.SwitchCSARequest;
import com.aeris.service.operatoradmin.payload.UpdateSprintCSARequest;

/**
 * Rest Resource that talks to the sprint dao to fetch the sprint csa details from the database,
 * api method to switch the csa for a carrier in the database and
 * allows the operator to update the secondary csa for a carrier.
 * 
 * @author SP00125222 Srinivas Puranam
 */
@Path("/carriers")
@Singleton
//@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
@PermitAll
public class CarrierMgmtResource{
    private static final Logger LOG = LoggerFactory.getLogger(CarrierMgmtResource.class);
    
    @Inject
    private ISprintCSADAO sprintCSADAO;	
	
	/**
	 * test method
	 * 
	 * @param name the name to return back
	 * 
	 * @return the name sent in the request
	 */
    @Path("csatest")
    @GET
    public Response test(@QueryParam("name") final String name) {
    	ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
    	LOG.info("test method called");
        
    	builder.entity(name);
    	
    	Response response = builder.build();
    	LOG.info("test method called", response.getStatus(), response.getEntity().toString());
        
        return response;
    }
    
    
    /**
     * Fetches the CSA Information for a carrier.
     * Calling the CSADao to return the relevant data from the db.
     * 
     * @param carrierId the carrier identifier
     * 
     * @return the CSA Information for the input carrier
     */
    @Path("{carrierId}/csa")
    @GET
    public Response getSprintCSA(@PathParam("carrierId") final String carrierId) {
    	ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
    	LOG.info("getSprintCSA called");
        
        LOG.info("fetching csa for carrierId "+carrierId);
        
        try {        	
        	if(StringUtils.isBlank(carrierId))
        	{
        		LOG.error("Missing carrierId in request, value is: "+carrierId);
        		return Response.status(Status.BAD_REQUEST).entity("Missing carrierId in request").build();
        	}
        	
        	SprintCSA csa  = sprintCSADAO.getSprintCSA(carrierId);
        	
        	if(csa != null)
            {
            	LOG.info("fetched csa details successfully for carrierId "+carrierId);
            	
            	ObjectMapper mapper = new ObjectMapper();
                String csaJson = mapper.writeValueAsString(csa);
                
                builder.entity(csaJson);
            }
            else
            {
            	LOG.info("fetch csa failed for carrierId  "+carrierId);
                
            	return Response.status(Status.NOT_FOUND).entity("CSA details not found for carrier: "+carrierId).build();
            }
        } catch (Exception e) {
            LOG.error("getSprintCSA Exception occured :",e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting CSA information").build();
        }
        
        Response response = builder.build();
        LOG.info("getSprintCSA", response.getStatus(), response.getEntity().toString());
        
        return response;
    }
    
    /**
     * Switch the CSA Values in the data store for the specified carrierid.
     * Uses the CSADao to perform the switch operation in the database. 
     * 
     * @param carrierId the carrier identifier associated with the request
     * @param request the switch csa request sent by the user with the requestDate 
     * 
     * @return JSONResponse if the op is successful or HttpErrorResponse if the operation fails.
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{carrierId}/swapcsa")
    @POST
    public Response switchCSA(@PathParam("carrierId") String carrierId, SwitchCSARequest request) {
    	ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        LOG.info("switchCSA");
        
        Date requestedDate = request.getRequestedDate();
        String requestedUser = request.getRequestedUser();
        
        if(requestedDate == null)
        {
        	requestedDate = new Date();
        }
        
        if(requestedUser == null)
        {
        	requestedUser = "UNKNOWN";
        }
        
        LOG.info("switching csa for carrierId "+carrierId);
        
        try {        	
        	if(StringUtils.isBlank(carrierId))
        	{
        		LOG.error("Missing carrierId in request, value is: "+carrierId);
        		return Response.status(Status.BAD_REQUEST).entity("Missing carrierId in request").build();
        	}
        	
        	boolean success = sprintCSADAO.switchCSA(carrierId, requestedDate, requestedUser);
        	
        	if(!success)
        	{
        		LOG.info("csa switch failed for carrierId "+carrierId);
                
            	return Response.status(Status.NOT_FOUND).entity("CSA details not found for carrier: "+carrierId).build();
        	}
        	else
        	{
        		LOG.info("csa switched successfully for carrierId "+carrierId);

                SprintCSA csa = sprintCSADAO.getSprintCSA(carrierId);
                
                ObjectMapper mapper = new ObjectMapper();
                String csaJson = mapper.writeValueAsString(csa);
                
                builder.entity(csaJson);
        	}
        } catch (Exception e) {
            LOG.error("switchCSA Exception occured :",e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while switching CSA").build();
        }
        
        Response response = builder.build();
        LOG.info("switchCSA", response.getStatus(), response.getEntity().toString());
        
        return response;
    }
    
    

    /**
     * Update the Secondary CSA Value in the data store for the specified carrierid.
     * Uses the CSADao to perform the update secondary csa operation in the database for the carrier id. 
     * 
     * @param carrierId the carrier identifier associated with the request
     * @param request the update request sent by the user with the new secondary CSA value
     * and requestDate 
     * 
     * @return JSONResponse if the op is successful or HttpErrorResponse if the operation fails.
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{carrierId}/csa/secondary")
    @PUT
    public Response updateSprintCSA(@PathParam("carrierId") String carrierId, UpdateSprintCSARequest request) {
    	ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        LOG.info("updateSprintCSA");
        
        String newCSA = request.getNewCSA();
        Date requestedDate = request.getRequestedDate();
        String requestedUser = request.getRequestedUser();
        
        if(requestedDate == null)
        {
        	requestedDate = new Date();
        }
        
        if(requestedUser == null)
        {
        	requestedUser = "UNKNOWN";
        }

        LOG.info("updating the secondary csa value for carrierId "+carrierId);
       	
        try {        	
        	if(StringUtils.isBlank(carrierId))
        	{
        		LOG.error("Missing carrierId in request, value is: "+carrierId);
        		return Response.status(Status.BAD_REQUEST).entity("Missing carrierId in request").build();
        	}
        	
        	if(StringUtils.isBlank(newCSA))
        	{
        		LOG.error("Missing newCSA in request, value is: "+carrierId);
        		return Response.status(Status.BAD_REQUEST).entity("Missing newCSA value in request").build();
        	}
        	
        	boolean success = sprintCSADAO.updateSecondaryCSA(carrierId, newCSA, requestedDate, requestedUser);
        	
        	if(!success)
        	{
        		LOG.info("secondary csa update failed for carrierId "+carrierId);
                
            	return Response.status(Status.NOT_FOUND).entity("CSA Information not found for carrier: "+carrierId).build();
        	}
        	else
        	{
                LOG.info("secondary csa updated successfully for carrierId "+carrierId);

                SprintCSA csa = sprintCSADAO.getSprintCSA(carrierId);
                
                ObjectMapper mapper = new ObjectMapper();
                String csaJson = mapper.writeValueAsString(csa);
                
                builder.entity(csaJson);
        	}
        } catch (Exception e) {
            LOG.error("updateSprintCSA Exception occured :",e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while updating secondary CSA").build();
        }
        
        Response response = builder.build();
        LOG.info("updateSprintCSA", response.getStatus(), response.getEntity().toString());
        
        return response;
    }
}
