package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IGlobalSIMInventoryDAO;
import com.aeris.service.operatoradmin.exception.SIMInventoryException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.SIMInfo;
import com.aeris.service.operatoradmin.model.SIMInventoryItem;
import com.aeris.service.operatoradmin.model.SIMStatus;
import com.aeris.service.operatoradmin.model.SIMSummary;
import com.aeris.service.operatoradmin.model.SIMSupplier;
import com.aeris.service.operatoradmin.model.SIMWarehouse;
import com.aeris.service.operatoradmin.model.SupplierSIMOrder;
import com.aeris.service.operatoradmin.utils.DBUtils;

public class GlobalSIMInventoryDAO implements IGlobalSIMInventoryDAO {
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");
	
	private static final String SQL_CREATE_SIM_INVENTORY_ITEM = "insert into sim_inventory " +
																"(supp_order_id, imsi, iccid, delivery_date, supplier_id, " +
																"format_id, warehouse_id, order_date, created_by, " +
																"last_modified_date, last_modified_by) " +
																"values " +
																"(?, ?, ?, ?, ?, " +
																"?, ?, ?, ?, " +
																"?, ?)";

	private Logger LOG = LoggerFactory.getLogger(GlobalSIMInventoryDAO.class);

	
	@Override
	public SIMSummary<SIMWarehouse> getSIMSummaryGroupedByWarehouse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SIMSummary<SIMSupplier> getSIMSummaryGroupedBySupplier(SIMWarehouse warehouse) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SIMSummary<SIMStatus> getSIMSummaryGroupedByStatus(Account account) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SIMInventoryItem> addSIMsToInventory(SupplierSIMOrder supplierSIMOrder, List<SIMInfo> sims, String userId) throws SIMInventoryException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		Date deliveryDate = new Date();
		List<SIMInventoryItem> simInventoryItems = new ArrayList<SIMInventoryItem>();
		
		Date createdDate = new Date();
		
		try {
			createdDate = DATE_FORMAT.parse(supplierSIMOrder.getCreatedDate());
		} catch (ParseException e) {
			LOG.error("Error Parsing Order Creation Date, defaulting the order date to current time");
		}
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			ps = conn.prepareStatement(SQL_CREATE_SIM_INVENTORY_ITEM);
			
			for (SIMInfo simInfo : sims) {
				ps.clearParameters();
				
				ps.setLong(1, supplierSIMOrder.getOrderId());
				ps.setString(2, simInfo.getSimIsmi());
				ps.setString(3, simInfo.getSimIccId());
				ps.setDate(4, new java.sql.Date(deliveryDate.getTime()));
				ps.setInt(5, supplierSIMOrder.getSupplierId());
				ps.setInt(6, supplierSIMOrder.getSimFormatId());
				ps.setInt(7, supplierSIMOrder.getSimWarehouseId());
				ps.setDate(8, new java.sql.Date(createdDate.getTime()));
				ps.setString(9, userId);
				ps.setDate(10, new java.sql.Date(deliveryDate.getTime()));
				ps.setString(11, userId);
				
				ps.addBatch();
			}
			
			int[] counts = ps.executeBatch();
			
			int index = 0;
			
			for (SIMInfo simInfo : sims) {
				
				// If record is persisted successfully
				if(counts[index++] == 1)
				{
					SIMInventoryItem simInventoryItem = new SIMInventoryItem();
					
					simInventoryItem.setSupplierOrderId(supplierSIMOrder.getOrderId());
					
					simInventoryItem.setSimSupplierId(supplierSIMOrder.getSupplierId());
					simInventoryItem.setSimFormatId(supplierSIMOrder.getSimFormatId());
					simInventoryItem.setSimWarehouseId(supplierSIMOrder.getSimWarehouseId());
					
					simInventoryItem.setInfo(simInfo);
					simInventoryItem.setCreatedDate(new java.sql.Date(deliveryDate.getTime()));
					
					simInventoryItems.add(simInventoryItem);
				}
			}

			LOG.info("Created SIM Inventory Items = " + ReflectionToStringBuilder.toString(simInventoryItems));
		} catch (SQLException sqle) {
			LOG.error("Exception during creating supplier sim order in supplier_sim_order" + sqle);
			throw new SIMInventoryException("Exception in createSIMOrder - Exception during creating supplier sim order", sqle);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return simInventoryItems;
	}

	@Override
	public void removeSIMsFromInventory(List<SIMInventoryItem> items) {
		// TODO Auto-generated method stub

	}

	@Override
	public void allocateSIMstoAccount(Account account, List<SIMInventoryItem> items) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deallocateSIMsFromAccount(Account account, List<SIMInventoryItem> items) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<SIMInventoryItem> getSIMInventoryItemsPerAccount(Account account) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SIMInventoryItem getSIMInventoryItem(SIMInfo simInfo) {
		// TODO Auto-generated method stub
		return null;
	}
}
