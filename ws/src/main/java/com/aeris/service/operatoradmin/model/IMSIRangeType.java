package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum IMSIRangeType implements Serializable {
	NON_STEERED("Aeris (non-steered)"), STEERED("Aeris (steered)");
	String value;

	IMSIRangeType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static IMSIRangeType fromValue(String value) {
		IMSIRangeType[] statuses = values();

		for (IMSIRangeType accountStatus : statuses) {
			if (accountStatus.getValue().equalsIgnoreCase(value)) {
				return accountStatus;
			}
		}

		return null;
	}

	public static IMSIRangeType fromName(String value) {
		IMSIRangeType[] statuses = values();

		for (IMSIRangeType accountStatus : statuses) {
			if (accountStatus.name().equalsIgnoreCase(value)) {
				return accountStatus;
			}
		}

		return null;
	}
}
