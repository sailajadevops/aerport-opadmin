package com.aeris.service.operatoradmin.exception;

public class AccountException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public AccountException(String s) {
		super(s);
	}

	public AccountException(String ex, Throwable t) {
		super(ex, t);
	}
}
