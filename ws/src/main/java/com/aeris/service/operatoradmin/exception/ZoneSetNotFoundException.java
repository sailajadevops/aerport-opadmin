package com.aeris.service.operatoradmin.exception;

public class ZoneSetNotFoundException extends Exception {

	private static final long serialVersionUID = -8067557907421121426L;

	public ZoneSetNotFoundException(String s) {
		super(s);
	}

	public ZoneSetNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
