package com.aeris.service.operatoradmin.exception;

public class SupplierSIMOrderNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SupplierSIMOrderNotFoundException(String s) {
		super(s);
	}

	public SupplierSIMOrderNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
