package com.aeris.service.operatoradmin.utils;

import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.IRatePlanDAO;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.delegate.IEmailDelegate;
import com.aeris.service.operatoradmin.delegate.ITemplateProcessor;
import com.aeris.service.operatoradmin.delegate.TemplateData;
import com.aeris.service.operatoradmin.delegate.impl.SimpleTemplateProcessor;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.ExportException;
import com.aeris.service.operatoradmin.exception.RatePlanException;
import com.aeris.service.operatoradmin.exception.RatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.CarrierRatePlan;
import com.aeris.service.operatoradmin.model.EmailProperties;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.payload.NotificationRequest;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.Message;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class OperatorAdminEmailer implements IEventListener {

    private static Logger LOG = LoggerFactory.getLogger(OperatorAdminEmailer.class);
    @Inject
    private IEmailDelegate emailDelegate;
    @Inject
    private EmailProperties emailProperties;
    @Inject
    private IRatePlanDAO ratePlanDAO;
    @Inject
    private IAccountDAO accountDAO;
    @Inject
    private IZoneDAO zoneDAO;
    @Inject
    private IProductEnum productCache;
    private static final String RATE_PLAN_CREATION_EMAIL_TEMPLATE = "/tpl/rate_plan_created_email.tpl";
    private static final String RATE_PLAN_ASSIGNED_EMAIL_TEMPLATE = "/tpl/rate_plan_assigned_email.tpl";
    private static final String ACCOUNT_CREATION_EMAIL_TEMPLATE = "/tpl/account_created_email.tpl";
    private static final String UNIQUE_CODES_CREATION_EMAIL_TEMPLATE = "/tpl/uniquecodes_created_email.tpl";
    private static final String ACCOUNT_ADDRESS_CHANGE_EMAIL_TEMPLATE = "/tpl/account_address_change_email.tpl";
    private static final String ACCOUNT_EMAILADDRESS_CHANGE_EMAIL_TEMPLATE = "/tpl/account_emailaddress_change_email.tpl";
    private static final String ACCOUNT_APPROVED_EMAIL_TEMPLATE = "/tpl/account_approved_email.tpl";
    private static final String CREDIT_LIMIT_CHECK_EMAIL_TEMPLATE = "/tpl/credit_limit_check_email.tpl";
    private static final String CREDIT_LIMIT_UPDATE_EMAIL_TEMPLATE = "/tpl/credit_limit_update_email.tpl";
    
    @Override
    public void onEvent(Event event, Object... params) {
        String operatorId = (String) params[0];        
        NotificationRequest notificationRequest = (NotificationRequest) params[1];
        String accountId = notificationRequest.getAccountId();
        LOG.info("OperatorAdminEmailer :: Operator = " + operatorId + ", Account = " + accountId + ", Request information = " + notificationRequest.toString());
        switch (event) {
            case RATE_PLAN_CREATED:
                try {
                    RatePlan ratePlan = ratePlanDAO.getRatePlan(Integer.parseInt(operatorId), Integer.parseInt(notificationRequest.getEntityID()));
                    sendNewRatePlanEmail(notificationRequest, ratePlan);
                } catch (RatePlanNotFoundException rpnfe) {
                    LOG.error("Exception occurred while sending new rate plan created notification: ", rpnfe);
                    notificationRequest.setStatus(1);
                } catch (RatePlanException rpe) {
                    LOG.error("Exception occurred while sending new rate plan created notification: ", rpe);
                    notificationRequest.setStatus(1);
                }
                break;
            case RATE_PLAN_ASSIGNED:
                try {
                    Account account = accountDAO.getAccount(operatorId, accountId);
                    RatePlan ratePlan = ratePlanDAO.getRatePlan(Integer.parseInt(operatorId), Integer.parseInt(notificationRequest.getEntityID()));
                    sendAssignedRatePlanEmail(notificationRequest, ratePlan, account);
                } catch (RatePlanNotFoundException rpnfe) {
                    LOG.error("Exception occurred while sending new rate plan created notification: ", rpnfe);
                    notificationRequest.setStatus(1);
                } catch (RatePlanException rpe) {
                    LOG.error("Exception occurred while sending new rate plan created notification: ", rpe);
                    notificationRequest.setStatus(1);
                }
                break;
            case ACCOUNT_CREATED:
                try {
                    Account account = accountDAO.getAccount(operatorId, notificationRequest.getEntityID());
                    sendNewAccountEmail(notificationRequest, account);
                } catch (Exception exc) {
                    LOG.error("Exception occurred while sending new account created notification: ", exc);
                    notificationRequest.setStatus(1);
                }
                break;
            case UNIQUE_CODES_CREATED:
                try {
                    Account account = accountDAO.getAccount(operatorId, notificationRequest.getAccountId());
                    sendUniqueCodesCreatedEmail(notificationRequest, account);
                } catch (Exception exc) {
                    LOG.error("Exception occurred while sending new account created notification: ", exc);
                    notificationRequest.setStatus(1);
                }
                break;
            case ACCOUNT_ADDRESS_CHANGE:
                try {
                    Account account = accountDAO.getAccount(operatorId, notificationRequest.getEntityID());
                    sendAccountAddressChangeEmail(notificationRequest, account);
                } catch (Exception exc) {
                    LOG.error("Exception occurred while sending new account created notification: ", exc);
                    notificationRequest.setStatus(1);
                }
                break;
            case ACCOUNT_PRIMARY_EMAIL_CHANGE:
                try {
                    Account account = accountDAO.getAccount(operatorId, notificationRequest.getEntityID());
                    sendAccountEmailChangeEmail(notificationRequest, account);
                } catch (Exception exc) {
                    LOG.error("Exception occurred while sending new account created notification: ", exc);
                    notificationRequest.setStatus(1);
                }
                break;
            case ACCOUNT_APPROVED :
            	try{
            		Account account = accountDAO.getAccount(operatorId, notificationRequest.getEntityID());
            		sendAccountApprovalEmail(notificationRequest, account);
            	}catch (Exception exc) {
                    LOG.error("Exception occurred while sending account approval notification: ", exc);
                    notificationRequest.setStatus(1);
                }
                break;
            case CREDIT_LIMIT_CHECK :
            	try{
            		Account account = accountDAO.getAccount(operatorId, notificationRequest.getEntityID());
            		sendCreditLimitCheckEmail(notificationRequest, account);
            	}catch (Exception exc) {
                    LOG.error("Exception occurred while sending credit limit check notification: ", exc);
                    notificationRequest.setStatus(1);
                }
                break;
            case CREDIT_LIMIT_UPDATE :
            	try{
            		Account account = accountDAO.getAccount(operatorId, notificationRequest.getEntityID());
            		sendCreditLimitUpdateEmail(notificationRequest, account);
            	}catch (Exception exc) {
                    LOG.error("Exception occurred while sending credit limit update notification: ", exc);
                    notificationRequest.setStatus(1);
                }
                break;
            default:
        }
    }

    private void sendNewRatePlanEmail(NotificationRequest notificationRequest, RatePlan ratePlan) {
        String subject = "New rate plan '" + ratePlan.getRatePlanLabel().trim() + "' is created.";
        try {
            TemplateData data = buildRatePlanTemplateData(ratePlan, notificationRequest);
            ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
            byte[] content = processor.createByteStreamFromTemplate(RATE_PLAN_CREATION_EMAIL_TEMPLATE, data);
            if (content != null) {
                LOG.info("Content-Length: " + content.length);
                Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                String fromEmail = emailProperties.getSenderEmail();
                boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                if (emailSent) {
                    LOG.info("Request to send new rate plan created notification is completed successfully.");
                } else {
                    notificationRequest.setStatus(1);
                    LOG.info("Request to send new rate plan created notification is failed.");
                }                
            }
        } catch (ExportException ex) {
            LOG.error("Exception occurred while sending new rate plan created notification: ", ex);
            notificationRequest.setStatus(1);
        }
    }
    
    private void sendAssignedRatePlanEmail(NotificationRequest notificationRequest, RatePlan ratePlan, Account account) {
        String subject = "Rate plan '" + ratePlan.getRatePlanLabel().trim() + "' is assigned to account " + "'" + account.getAccountName().trim() + "'";
        try {
            TemplateData data = buildRatePlanTemplateData(ratePlan, notificationRequest);
            data.put("ACCOUNT_NAME", account.getAccountName().trim());
            ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
            byte[] content = processor.createByteStreamFromTemplate(RATE_PLAN_ASSIGNED_EMAIL_TEMPLATE, data);
            if (content != null) {
                LOG.info("Content-Length: " + content.length);
                Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                String fromEmail = emailProperties.getSenderEmail();
                boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                if (emailSent) {
                    LOG.info("Request to send assign rate plan notification is completed successfully.");
                } else {
                    notificationRequest.setStatus(1);
                    LOG.info("Request to send assign rate plan notification is failed.");
                }
            }
        } catch (ExportException ex) {
            LOG.error("Exception occurred while sending rate plan assigned notification: ", ex);
            notificationRequest.setStatus(1);
        }
    }

    private TemplateData buildRatePlanTemplateData(RatePlan ratePlan, NotificationRequest notificationRequest) {
        TemplateData data = new TemplateData();
        data.put("USER_NAME", notificationRequest.getUserId());
        data.put("VIEW_URL", notificationRequest.getViewUrl());
        data.put("LOGO_URL", getLogoUrl(notificationRequest.getOperatorId()));
        data.put("RATE_PLAN_ID", String.valueOf(ratePlan.getRatePlanId()));
        data.put("RATE_PLAN_LABEL", ratePlan.getRatePlanLabel());
        data.put("RATE_PLAN_NAME", ratePlan.getRatePlanName());
        Product product = productCache.getProductById(ratePlan.getProductId());
        data.put("PRODUCT", product.getProductName());
        try {
            Zone homeZone = zoneDAO.getZone(ratePlan.getOperatorId(), ratePlan.getZoneSetId(), ratePlan.getHomeZoneId());
            data.put("HOME_ZONE", homeZone.getZoneName());
        } catch (ZoneNotFoundException znfe) {
            LOG.error("Exception occurred while getting zone for building template data: ", znfe);
            data.put("HOME_ZONE", "");
        } catch (ZoneException ze) {
            LOG.error("Exception occurred while getting zone for building template data: ", ze);
            data.put("HOME_ZONE", "");
        }
        data.put("START_DATE", ratePlan.getStartDate().toString());
        data.put("END_DATE", ratePlan.getEndDate().toString());
        try {
            List<CarrierRatePlan> carrierRatePlans = ratePlanDAO.getCarrierRatePlans(ratePlan.getProductId());
            for (CarrierRatePlan carrierRatePlan : carrierRatePlans) {
                if (ratePlan.getCarrierRatePlanId() == carrierRatePlan.getRatePlanId()) {
                    data.put("CARRIER_RATE_PLAN", carrierRatePlan.getRatePlanName());
                    break;
                }
            }
        } catch (RatePlanException e) {
            LOG.error("Exception occurred while getting carrier rate plans for building template data: ", e);
            data.put("CARRIER_RATE_PLAN", "");
        }
        data.put("RATE_PLAN_PERIOD", StringUtils.replace(ratePlan.getPeriodType().name(), "_", " "));
        data.put("RATE_PLAN_PAYMENT_TYPE", StringUtils.replace(ratePlan.getPaymentType().name(), "_", " "));
        data.put("RATE_PLAN_ACCESS_TYPE", StringUtils.replace(ratePlan.getAccessType().name(), "_", " "));
        data.put("RATE_PLAN_MODEL_TYPE", StringUtils.replace(ratePlan.getRatePlanType().name(), "_", " "));
        return data;
    }

    private String getLogoUrl(String operatorId) {
        if ("1".equalsIgnoreCase(operatorId)) {
            return "https://aerport.aeris.com/aeris-theme/images/aeris_logo.png";
        } else if ("2".equalsIgnoreCase(operatorId)) {
            return "https://commandcenter.sprint.com/sprint-theme/images/company_logo.gif";
        } else if ("3".equalsIgnoreCase(operatorId)) { // TODO Put cisco url
            return "https://aerport.aeris.com/aeris-theme/images/aeris_logo.png";
        } else if ("4".equalsIgnoreCase(operatorId)) { // TODO Put aircel url
            return "https://aerport.aeris.com/aeris-theme/images/aeris_logo.png";
        }
        return "https://aerport.aeris.com/aeris-theme/images/aeris_logo.png";
    }
    
    private String getExternalIDLabel(String operatorId) {
        if ("2".equalsIgnoreCase(operatorId)) {
            return "BAN";
        }else if(operatorId.equalsIgnoreCase(Constants.USCC_OPEARTOR_ID)) {
        	return "TOPS A/C ID";
        }
        return "Netsuite ID";
    }
    
    private String getExternalIDvalue(String operatorId, Account account) {
        if ("2".equalsIgnoreCase(operatorId) || operatorId.equalsIgnoreCase(Constants.USCC_OPEARTOR_ID)) {
            return account.getCarrierAccountId();
        }
        return account.getNetSuiteId();
    }
    
    private void sendNewAccountEmail(NotificationRequest notificationRequest, Account account) {
        String subject = "New account '" + account.getAccountName().trim() + "' is created.";
        try {
            TemplateData data = buildAccountTemplateData(account, notificationRequest);
            ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
            byte[] content = processor.createByteStreamFromTemplate(ACCOUNT_CREATION_EMAIL_TEMPLATE, data);
            if (content != null) {
                LOG.info("Content-Length: " + content.length);
                Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                String fromEmail = emailProperties.getSenderEmail();
                boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                if (emailSent) {
                    LOG.info("Request to send new account notification is completed successfully.");
                } else {
                    notificationRequest.setStatus(1);
                    LOG.info("Request to send new account notification is failed.");
                }
            }
        } catch (ExportException ex) {
            LOG.error("Exception occurred while sending new account notification : ", ex);
            notificationRequest.setStatus(1);
        }
    }
    
    private void sendCreditLimitCheckEmail(NotificationRequest notificationRequest, Account account) {
    	String subject = "New account '" + account.getAccountName().trim() + "' is created.";
         try {
             TemplateData data = buildAccountTemplateData(account, notificationRequest);
             ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
             byte[] content = processor.createByteStreamFromTemplate(CREDIT_LIMIT_CHECK_EMAIL_TEMPLATE, data);
             if (content != null) {
                 LOG.info("Content-Length: " + content.length);
                 Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                 recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                 recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                 String fromEmail = emailProperties.getSenderEmail();
                 boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                 if (emailSent) {
                     LOG.info("Request to send credit limit check notification is completed successfully.");
                 } else {
                     notificationRequest.setStatus(1);
                     LOG.info("Request to send credit limit check notification is failed.");
                 }
             }
         } catch (ExportException ex) {
             LOG.error("Exception occurred while sending credit limit check notification: ", ex);
             notificationRequest.setStatus(1);
         }
    }
    
    private void sendCreditLimitUpdateEmail(NotificationRequest notificationRequest, Account account) {
   	 String subject = "Credit Limit for account '" + account.getAccountName().trim() + "' is updated.";
        try {
            TemplateData data = buildCreditLimitUpdateTemplateData(account, notificationRequest);
            ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
            byte[] content = processor.createByteStreamFromTemplate(CREDIT_LIMIT_UPDATE_EMAIL_TEMPLATE, data);
            if (content != null) {
                LOG.info("Content-Length: " + content.length);
                Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                String fromEmail = emailProperties.getSenderEmail();
                boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                if (emailSent) {
                    LOG.info("Request to send credit limit update notification is completed successfully.");
                } else {
                    notificationRequest.setStatus(1);
                    LOG.info("Request to send credit limit update notification is failed.");
                }
            }
        } catch (ExportException ex) {
            LOG.error("Exception occurred while sending credit limit update notification: ", ex);
            notificationRequest.setStatus(1);
        }
   }
    
    private void sendAccountApprovalEmail(NotificationRequest notificationRequest, Account account) {
   	 String subject = "Account '" + account.getAccountName().trim() + "' is approved.";
        try {
            TemplateData data = buildAccountTemplateData(account, notificationRequest);
            ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
            byte[] content = processor.createByteStreamFromTemplate(ACCOUNT_APPROVED_EMAIL_TEMPLATE, data);
            if (content != null) {
                LOG.info("Content-Length: " + content.length);
                Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                String fromEmail = emailProperties.getSenderEmail();
                boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                if (emailSent) {
                    LOG.info("Request to send account approval notification is completed successfully.");
                } else {
                    notificationRequest.setStatus(1);
                    LOG.info("Request to send account approval notification is failed.");
                }
            }
        } catch (ExportException ex) {
            LOG.error("Exception occurred while sending account approval notification: ", ex);
            notificationRequest.setStatus(1);
        }
   }
    
    public void sendUniqueCodesCreatedEmail(NotificationRequest notificationRequest, Account account) {
        String subject = "6 digit unique codes for distributor '" + account.getAccountName().trim() + "' are generated.";
        try {
            TemplateData data = buildUniqueCodesTemplateData(account, notificationRequest);
            ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
            byte[] content = processor.createByteStreamFromTemplate(UNIQUE_CODES_CREATION_EMAIL_TEMPLATE, data);
            if (content != null) {
                LOG.info("Content-Length: " + content.length);
                Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                String fromEmail = emailProperties.getSenderEmail();
                boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                if (emailSent) {
                    LOG.info("Request to send unique codes is completed successfully.");
                } else {
                    notificationRequest.setStatus(1);
                    LOG.info("Request to send unique codes notification is failed.");
                }
            }
        } catch (ExportException ex) {
            LOG.error("Exception occurred while sending unique codes notification: ", ex);
            notificationRequest.setStatus(1);
        }
    }
    
    private TemplateData buildAccountTemplateData(Account account, NotificationRequest notificationRequest) {
        TemplateData data = new TemplateData();
        data.put("USER_NAME", notificationRequest.getUserId());
        data.put("VIEW_URL", notificationRequest.getViewUrl());
        data.put("LOGO_URL", getLogoUrl(notificationRequest.getOperatorId()));
        data.put("ACCOUNT_NAME", account.getAccountName().trim());
        data.put("EXTERNAL_ID_LABEL", getExternalIDLabel(notificationRequest.getOperatorId()));
        data.put("EXTERNAL_ID_VALUE", getExternalIDvalue(notificationRequest.getOperatorId(), account));
        List<String> productIds = account.getProductIds();
        String[] products = new String[productIds.size()];
        int count = 0;
        for (String productId : productIds) {
            Product product = productCache.getProductById(Integer.parseInt(productId));
            products[count] = product.getProductName();
            count++;
        }
        data.put("PRODUCT", Arrays.toString(products));
        data.put("BILLABLE_STATUS", StringUtils.replace(account.getBillableStatus().name(), "_", " "));
        data.put("INVOICE_ADDRESS", account.getInvoiceAddress() == null ? "" : account.getInvoiceAddress().getFullAddress());  
        data.put("OLD_INVOICE_ADDRESS", notificationRequest.getAdditionalEmailBodyContents()); 
        return data;
    }
    
    private TemplateData buildCreditLimitUpdateTemplateData(Account account, NotificationRequest notificationRequest) {
        TemplateData data = new TemplateData();
        data.put("USER_NAME", notificationRequest.getUserId());
        data.put("VIEW_URL", notificationRequest.getViewUrl());
        data.put("LOGO_URL", getLogoUrl(notificationRequest.getOperatorId()));
        data.put("ACCOUNT_NAME", account.getAccountName().trim());
        data.put("CREDIT_LIMIT", String.valueOf(account.getCreditLimit()));
        data.put("EXTERNAL_ID_LABEL", getExternalIDLabel(notificationRequest.getOperatorId()));
        data.put("EXTERNAL_ID_VALUE", getExternalIDvalue(notificationRequest.getOperatorId(), account));
        List<String> productIds = account.getProductIds();
        String[] products = new String[productIds.size()];
        int count = 0;
        for (String productId : productIds) {
            Product product = productCache.getProductById(Integer.parseInt(productId));
            products[count] = product.getProductName();
            count++;
        }
        data.put("PRODUCT", Arrays.toString(products));
        data.put("BILLABLE_STATUS", StringUtils.replace(account.getBillableStatus().name(), "_", " "));
        data.put("INVOICE_ADDRESS", account.getInvoiceAddress() == null ? "" : account.getInvoiceAddress().getFullAddress());  
        data.put("OLD_INVOICE_ADDRESS", notificationRequest.getAdditionalEmailBodyContents()); 
        return data;
    }
    
    private TemplateData buildAccountChangeTemplateData(Account account, NotificationRequest notificationRequest) {
        TemplateData data = new TemplateData();
        data.put("USER_NAME", notificationRequest.getUserId());
        data.put("VIEW_URL", notificationRequest.getViewUrl());
        data.put("LOGO_URL", getLogoUrl(notificationRequest.getOperatorId()));
        data.put("ACCOUNT_NAME", account.getAccountName().trim());
        data.put("EXTERNAL_ID_LABEL", getExternalIDLabel(notificationRequest.getOperatorId()));
        data.put("EXTERNAL_ID_VALUE", getExternalIDvalue(notificationRequest.getOperatorId(), account));
        List<String> productIds = account.getProductIds();
        String[] products = new String[productIds.size()];
        int count = 0;
        for (String productId : productIds) {
            Product product = productCache.getProductById(Integer.parseInt(productId));
            products[count] = product.getProductName();
            count++;
        }
        data.put("PRODUCT", Arrays.toString(products));
        data.put("BILLABLE_STATUS", StringUtils.replace(account.getBillableStatus().name(), "_", " "));
        data.put("INVOICE_ADDRESS", account.getInvoiceAddress() == null ? "" : account.getInvoiceAddress().getFullAddress());  
        data.put("OLD_INVOICE_ADDRESS", notificationRequest.getAdditionalEmailBodyContents()); 
        data.put("PRIMARY_EMAIL", account.getPrimaryContact() == null ? "" : account.getPrimaryContact().getEmail());  
        data.put("OLD_PRIMARY_EMAIL", notificationRequest.getAdditionalEmailBodyContents()); 
        return data;
    }
    
    private TemplateData buildUniqueCodesTemplateData(Account account, NotificationRequest notificationRequest) {
        TemplateData data = new TemplateData();
        data.put("USER_NAME", notificationRequest.getUserId());
        data.put("LOGO_URL", getLogoUrl(notificationRequest.getOperatorId()));
        data.put("ACCOUNT_NAME", account.getAccountName().trim());
        data.put("UNIQUE_CODES", notificationRequest.getAdditionalEmailBodyContents());
        return data;
    }
    
    private void sendAccountAddressChangeEmail(NotificationRequest notificationRequest, Account account) {
        String subject = "Address of account '" + account.getAccountName().trim() + "' is changed.";
        try {
            TemplateData data = buildAccountChangeTemplateData(account, notificationRequest);
            ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
            byte[] content = processor.createByteStreamFromTemplate(ACCOUNT_ADDRESS_CHANGE_EMAIL_TEMPLATE, data);
            if (content != null) {
                LOG.info("Content-Length: " + content.length);
                Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                String fromEmail = emailProperties.getSenderEmail();
                boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                if (emailSent) {
                    LOG.info("Request to send address change notification is completed successfully.");
                } else {
                    notificationRequest.setStatus(1);
                    LOG.info("Request to send address change notification is failed.");
                }
            }
        } catch (ExportException ex) {
            LOG.error("Exception occurred while sending address change notification: ", ex);
            notificationRequest.setStatus(1);
        }
    }
    
    private void sendAccountEmailChangeEmail(NotificationRequest notificationRequest, Account account) {
        String subject = "Primary email of account '" + account.getAccountName().trim() + "' is changed.";
        try {
            TemplateData data = buildAccountChangeTemplateData(account, notificationRequest);
            ITemplateProcessor processor = new SimpleTemplateProcessor("\n");
            byte[] content = processor.createByteStreamFromTemplate(ACCOUNT_EMAILADDRESS_CHANGE_EMAIL_TEMPLATE, data);
            if (content != null) {
                LOG.info("Content-Length: " + content.length);
                Map<Message.RecipientType, String[]> recipientEmailAddresses = new HashMap<Message.RecipientType, String[]>();
                recipientEmailAddresses.put(Message.RecipientType.TO, notificationRequest.getToEmailRecipients());
                recipientEmailAddresses.put(Message.RecipientType.CC, notificationRequest.getCcEmailRecipients());                
                String fromEmail = emailProperties.getSenderEmail();
                boolean emailSent = emailDelegate.sendEmail(fromEmail, recipientEmailAddresses, subject, new String(content));
                if (emailSent) {
                    LOG.info("Request to send email change notification is completed successfully.");
                } else {
                    notificationRequest.setStatus(1);
                    LOG.info("Request to send email change notification is failed.");
                }
            }
        } catch (ExportException ex) {
            LOG.error("Exception occurred while sending email change notification: ", ex);
            notificationRequest.setStatus(1);
        }
    }
}
