package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.IccIdPrefix;

public interface IIccIdPrefixEnum  extends IBaseEnum{
	IccIdPrefix getIccIdPrefixByRange(int range);
	List<IccIdPrefix> getAllIccIdPrefixes();
}
