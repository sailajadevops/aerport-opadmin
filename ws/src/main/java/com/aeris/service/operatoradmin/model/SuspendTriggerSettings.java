package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@XmlRootElement
public class SuspendTriggerSettings implements Serializable {
	private static final long serialVersionUID = -3929097253561462725L;

	private int duration;

	@Enumeration(value = DurationUnit.class, message = "{durationUnit.enum}")
	private DurationUnit durationUnit = DurationUnit.MONTHS;

	private long packetThreshold;

	@Enumeration(value = PacketUnit.class, message = "{packetThresholdUnit.enum}")
	private PacketUnit packetThresholdUnit = PacketUnit.KB;

	private int mtSmsThreshold;

	private int moSmsThreshold;

	private int mtVoiceMinsThreshold;

	private int moVoiceMinsThreshold;
	
	private int trafficDays;
	
	@NotNull(message = "{perKBPacketPrice.notnull}")	
	private double perKBPacketPrice;
	
	@NotNull(message = "{perMTSmsPrice.notnull}")
	private double perMTSmsPrice;
	
	@NotNull(message = "{perMOSmsPrice.notnull}")
	private double perMOSmsPrice;
	
	@NotNull(message = "{perMinMTVoicePrice.notnull}")
	private double perMinMTVoicePrice;
	
	@NotNull(message = "{perMinMOVoicePrice.notnull}")
	private double perMinMOVoicePrice;	

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public DurationUnit getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(DurationUnit durationUnit) {
		this.durationUnit = durationUnit;
	}

	public long getPacketThreshold() {
		return packetThreshold;
	}

	public void setPacketThreshold(long packetThreshold) {
		this.packetThreshold = packetThreshold;
	}

	public PacketUnit getPacketThresholdUnit() {
		return packetThresholdUnit;
	}

	public void setPacketThresholdUnit(PacketUnit packetThresholdUnit) {
		this.packetThresholdUnit = packetThresholdUnit;
	}

	public int getMtSmsThreshold() {
		return mtSmsThreshold;
	}

	public void setMtSmsThreshold(int mtSmsThreshold) {
		this.mtSmsThreshold = mtSmsThreshold;
	}

	public int getMoSmsThreshold() {
		return moSmsThreshold;
	}

	public void setMoSmsThreshold(int moSmsThreshold) {
		this.moSmsThreshold = moSmsThreshold;
	}

	public int getMtVoiceMinsThreshold() {
		return mtVoiceMinsThreshold;
	}

	public void setMtVoiceMinsThreshold(int mtVoiceMinsThreshold) {
		this.mtVoiceMinsThreshold = mtVoiceMinsThreshold;
	}

	public int getMoVoiceMinsThreshold() {
		return moVoiceMinsThreshold;
	}

	public void setMoVoiceMinsThreshold(int moVoiceMinsThreshold) {
		this.moVoiceMinsThreshold = moVoiceMinsThreshold;
	}
	
	public int getTrafficDays() {
		return trafficDays;
	}
	
	public void setTrafficDays(int trafficDays) {
		this.trafficDays = trafficDays;
	}
	
	public double getPerKBPacketPrice() {
		return perKBPacketPrice;
	}

	public void setPerKBPacketPrice(double perKBPacketPrice) {
		this.perKBPacketPrice = perKBPacketPrice;
	}

	public double getPerMTSmsPrice() {
		return perMTSmsPrice;
	}

	public void setPerMTSmsPrice(double perMTSmsPrice) {
		this.perMTSmsPrice = perMTSmsPrice;
	}

	public double getPerMOSmsPrice() {
		return perMOSmsPrice;
	}

	public void setPerMOSmsPrice(double perMOSmsPrice) {
		this.perMOSmsPrice = perMOSmsPrice;
	}

	public double getPerMinMTVoicePrice() {
		return perMinMTVoicePrice;
	}

	public void setPerMinMTVoicePrice(double perMinMTVoicePrice) {
		this.perMinMTVoicePrice = perMinMTVoicePrice;
	}

	public double getPerMinMOVoicePrice() {
		return perMinMOVoicePrice;
	}

	public void setPerMinMOVoicePrice(double perMinMOVoicePrice) {
		this.perMinMOVoicePrice = perMinMOVoicePrice;
	}

	public int getDurationInMonths()
	{
		if(durationUnit == DurationUnit.YEARS)
		{
			return (duration * 12);
		}
		
		return duration;
	}
	
	public long getPacketThresholdInKB()
	{
		if(packetThresholdUnit == PacketUnit.MB)
		{
			return (packetThreshold * 1024);
		}
		
		if(packetThresholdUnit == PacketUnit.GB)
		{
			return (packetThreshold * 1048576);
		}
		
		return packetThreshold;
	}
}
