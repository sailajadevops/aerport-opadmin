package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum DurationUnit implements Serializable {
	DAYS(2), MONTHS(1), YEARS(3);
	int value;

	DurationUnit(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int value) {
		DurationUnit[] units = values();

		for (DurationUnit unit : units) {
			if (unit.getValue() == value) {
				return true;
			}
		}

		return false;
	}

	public static DurationUnit fromValue(int value) {
		DurationUnit[] units = values();

		for (DurationUnit unit : units) {
			if (unit.getValue() == value) {
				return unit;
			}
		}

		return null;
	}

	public static DurationUnit fromName(String value) {
		DurationUnit[] units = values();
		if(Integer.getInteger(value) != null) 
			return fromValue(Integer.getInteger(value));
		for (DurationUnit unit : units) {
			if (unit.name().equalsIgnoreCase(value)) {
				return unit;
			}
		}

		return null;
	}
}
