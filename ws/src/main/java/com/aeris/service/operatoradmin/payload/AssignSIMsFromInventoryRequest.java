package com.aeris.service.operatoradmin.payload;

/**
 *
 * @author SB00122138
 */
public class AssignSIMsFromInventoryRequest {

    private String userId;
    private String iccidStart;
    private String iccidEnd;
    private String orderId;
    private String iccids;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIccidStart() {
        return iccidStart;
    }

    public void setIccidStart(String iccidStart) {
        this.iccidStart = iccidStart;
    }

    public String getIccidEnd() {
        return iccidEnd;
    }

    public void setIccidEnd(String iccidEnd) {
        this.iccidEnd = iccidEnd;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIccids() {
        return iccids;
    }

    public void setIccids(String iccids) {
        this.iccids = iccids;
    }
}
