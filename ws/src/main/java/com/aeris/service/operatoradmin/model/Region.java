package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum Region implements Serializable {
	DOMESTIC(1), INTERNATIONAL(2);
	int value;

	Region(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static Region fromValue(int value) {
		Region[] regions = values();

		for (Region region : regions) {
			if (region.getValue() == value) {
				return region;
			}
		}

		return null;
	}

	public static Region fromName(String value) {
		Region[] regions = values();

		for (Region region : regions) {
			if (region.name().equalsIgnoreCase(value)) {
				return region;
			}
		}

		return null;
	}
}
