package com.aeris.service.operatoradmin.model;

import java.util.List;

public class DistributorICCIDRange {

	private long distributorId ;
	private long accountId ;
	private List<String> emailRecipients ;
	private String iccidStart;
	private String iccidEnd ;
	private String assignedIccid ;
	
	public long getDistributorId() {
		return distributorId;
	}
	public void setDistributorId(long distributorId) {
		this.distributorId = distributorId;
	}
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	public List<String> getEmailRecipients() {
		return emailRecipients;
	}
	public void setEmailRecipients(List<String> emailRecipients) {
		this.emailRecipients = emailRecipients;
	}
	public String getIccidStart() {
		return iccidStart;
	}
	public void setIccidStart(String iccidStart) {
		this.iccidStart = iccidStart;
	}
	public String getIccidEnd() {
		return iccidEnd;
	}
	public void setIccidEnd(String iccidEnd) {
		this.iccidEnd = iccidEnd;
	}
	public String getAssignedIccid() {
		return assignedIccid;
	}
	public void setAssignedIccid(String assignedIccid) {
		this.assignedIccid = assignedIccid;
	}

}
