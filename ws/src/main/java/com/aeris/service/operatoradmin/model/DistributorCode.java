package com.aeris.service.operatoradmin.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

public class DistributorCode {

	private long distributorId ;
	private long accountId ;
	private int numberOfCodes ;
	private long code ;
	private List<String> emailRecipients ;
	private String iccidStart;
	private String iccidEnd ;
	private String assignedIccid ;
	
	
	public long getDistributorId() {
		return distributorId;
	}
	public void setDistributorId(long distributorId) {
		this.distributorId = distributorId;
	}
	public int getNumberOfCodes() {
		return numberOfCodes;
	}
	public void setNumberOfCodes(int numberOfCodes) {
		this.numberOfCodes = numberOfCodes;
	}
	public List<String> getEmailRecipients() {
		return emailRecipients;
	}
	public void setEmailRecipients(List<String> emailRecipients) {
		this.emailRecipients = emailRecipients;
	}
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	public String getIccidStart() {
		return iccidStart;
	}
	public void setIccidStart(String iccidStart) {
		this.iccidStart = iccidStart;
	}
	public String getIccidEnd() {
		return iccidEnd;
	}
	public void setIccidEnd(String iccidEnd) {
		this.iccidEnd = iccidEnd;
	}
	public String getAssignedIccid() {
		return assignedIccid;
	}
	public void setAssignedIccid(String assignedIccid) {
		this.assignedIccid = assignedIccid;
	}
	public long getCode() {
		return code;
	}
	public void setCode(long code) {
		this.code = code;
	}
	
	
	public static void main(String[] args) throws Exception {
		DistributorCode distCode = new DistributorCode();
		distCode.setAccountId(1l);
		distCode.setNumberOfCodes(5);
		distCode.setAssignedIccid("8901260762214998727,8901260762214998728");
		distCode.setCode(123456l);
		distCode.setDistributorId(123l);
		distCode.setEmailRecipients(Arrays.asList("saurabh.sharma@aeris.net,ss@test.com".split(",")));
		DistributorCode distCode1 = new DistributorCode();
		distCode1.setAccountId(1l);
		distCode1.setAssignedIccid("8901260762214998727,8901260762214998728");
		distCode1.setCode(123456l);
		distCode1.setDistributorId(123l);
		distCode1.setNumberOfCodes(10);
		distCode1.setEmailRecipients(Arrays.asList("saurabh.sharma@aeris.net,ss@test.com".split(",")));
		List<DistributorCode> list = new ArrayList<DistributorCode>();
		list.add(distCode);
		list.add(distCode1);
		System.out.println(new ObjectMapper().writeValueAsString(list));
	}

}
