package com.aeris.service.operatoradmin.payload;

import java.util.List;

/**
 * The DTO class for distributorCode
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class UpdateSimpackRequest {

	private int packCount ;
	private int packSize ;
	private List<String> emailRecipients ;
	private String iccidStart;
	private String iccidEnd ;
	private String simType ;
	private String userId ;
	
	public List<String> getEmailRecipients() {
		return emailRecipients;
	}
	
	public void setEmailRecipients(List<String> emailRecipients) {
		this.emailRecipients = emailRecipients;
	}


	public String getIccidStart() {
		return iccidStart;
	}

	public void setIccidStart(String iccidStart) {
		this.iccidStart = iccidStart;
	}

	public String getIccidEnd() {
		return iccidEnd;
	}

	public void setIccidEnd(String iccidEnd) {
		this.iccidEnd = iccidEnd;
	}

	public int getPackCount() {
		return packCount;
	}

	public void setPackCount(int packCount) {
		this.packCount = packCount;
	}

	public int getPackSize() {
		return packSize;
	}

	public void setPackSize(int packSize) {
		this.packSize = packSize;
	}

	public String getSimType() {
		return simType;
	}

	public void setSimType(String simType) {
		this.simType = simType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
