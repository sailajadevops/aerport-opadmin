package com.aeris.service.operatoradmin.exception;

public class RatePlanNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public RatePlanNotFoundException(String s) {
		super(s);
	}

	public RatePlanNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
