package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class BillingAddress implements Serializable {
	private static final long serialVersionUID = -5265408983747538171L;
	private String contactFirstName;
	private String contactLastName;
	private String addressFirstLine;
	private String addressSecondLine;
	private String state;
	private String country;
	private String zipCode;
	private String phoneNumber;

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	public String getAddressFirstLine() {
		return addressFirstLine;
	}

	public void setAddressFirstLine(String addressFirstLine) {
		this.addressFirstLine = addressFirstLine;
	}

	public String getAddressSecondLine() {
		return addressSecondLine;
	}

	public void setAddressSecondLine(String addressSecondLine) {
		this.addressSecondLine = addressSecondLine;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String validate() {
		StringBuffer errors = new StringBuffer();

		if (StringUtils.isEmpty(contactFirstName)) {
			errors.append("contactFirstName\n");
		}

		if (StringUtils.isEmpty(contactLastName)) {
			errors.append("contactLastName\n");
		}

		if (StringUtils.isEmpty(addressFirstLine)) {
			errors.append("addressFirstLine\n");
		}

		if (StringUtils.isEmpty(addressSecondLine)) {
			errors.append("addressSecondLine\n");
		}

		if (StringUtils.isEmpty(state)) {
			errors.append("state\n");
		}

		if (StringUtils.isEmpty(country)) {
			errors.append("country\n");
		}

		if (StringUtils.isEmpty(zipCode)) {
			errors.append("zipCode\n");
		}

		if (StringUtils.isEmpty(phoneNumber)) {
			errors.append("phoneNumber\n");
		}

		return errors.toString();
	}

}
