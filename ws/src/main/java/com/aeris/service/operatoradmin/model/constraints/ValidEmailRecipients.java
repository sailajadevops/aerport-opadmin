package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

/**
 * Validator class for collection of email address values
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD, PARAMETER, TYPE})
@Documented
@Constraint(validatedBy = ValidEmailRecipients.Validator.class)
public @interface ValidEmailRecipients {
	String message() default "{email.recipients.not.valid}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class Validator implements ConstraintValidator<ValidEmailRecipients, List<String>> {

		@Override
		public void initialize(final ValidEmailRecipients enumClazz) {
			// do nothing
		}


		@Override
		public boolean isValid(List<String> value, ConstraintValidatorContext context) {
			for (String email : value) {
				if(!isValidEmailAddress(email))
					return false ;
			}
			return true ;
		}

		/**
		 * validates email string using java api
		 * 
		 * @param email
		 * @return
		 */
		public boolean isValidEmailAddress(String email) {
			boolean result = true;
			try {
				InternetAddress emailAddr = new InternetAddress(email);
				emailAddr.validate();
			} catch (AddressException ex) {
				result = false;
			}
			return result;
		}
		
		/**
		 * validate email using pattern matcher
		 * 
		 * @param email
		 * @return
		 */
		public boolean isValidEmail(String email) {
	           String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
	           java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
	           java.util.regex.Matcher m = p.matcher(email);
	           return m.matches();
	    }
	}

}
