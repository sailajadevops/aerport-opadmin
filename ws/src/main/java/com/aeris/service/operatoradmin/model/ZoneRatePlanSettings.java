package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class ZoneRatePlanSettings implements Serializable {
	private static final long serialVersionUID = 7582472101912421122L;
	private Map<Integer, IncludedPlanDetails> zoneRatePlanSettings;

	public void setIncludedPlanDetails(int zoneId, IncludedPlanDetails planDetails) {
		zoneRatePlanSettings.put(zoneId, planDetails);
	}

	public IncludedPlanDetails getIncludedPlanDetails(int zoneId) {
		return zoneRatePlanSettings.get(zoneId);
	}

	public Map<Integer, IncludedPlanDetails> getZoneRatePlanSettings() {
		return zoneRatePlanSettings;
	}
}
