package com.aeris.service.operatoradmin.payload;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Email;

@XmlRootElement
public class CreateNeoSIMPacksRequest {

    @Email(message = "{userEmail.email}")
    @NotNull(message = "{userEmail.notnull}")
    private String userEmail;

    @NotNull(message = "{numberOfSIMPacks.notnull}")
    private Integer numberOfSIMPacks;

    @NotNull(message = "{packSize.notnull}")
    private Integer packSize;

    @NotNull(message = "{iccidStart.notnull}")
    private String iccidStart;

    @NotNull(message = "{iccidEnd.notnull}")
    private String iccidEnd;

    private Long distributorAccountId;
    
    private boolean checkInSIMInventory = true;
    
    @NotNull(message = "{simType.notnull}")
    private String simType;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Integer getNumberOfSIMPacks() {
        return numberOfSIMPacks;
    }

    public void setNumberOfSIMPacks(Integer numberOfSIMPacks) {
        this.numberOfSIMPacks = numberOfSIMPacks;
    }

    public Integer getPackSize() {
        return packSize;
    }

    public void setPackSize(Integer packSize) {
        this.packSize = packSize;
    }

    public String getIccidStart() {
        return iccidStart;
    }

    public void setIccidStart(String iccidStart) {
        this.iccidStart = iccidStart;
    }

    public String getIccidEnd() {
        return iccidEnd;
    }

    public void setIccidEnd(String iccidEnd) {
        this.iccidEnd = iccidEnd;
    }

    public Long getDistributorAccountId() {
        return distributorAccountId;
    }

    public void setDistributorAccountId(Long distributorAccountId) {
        this.distributorAccountId = distributorAccountId;
    }

    public boolean isCheckInSIMInventory() {
        return checkInSIMInventory;
    }

    public void setCheckInSIMInventory(boolean checkInSIMInventory) {
        this.checkInSIMInventory = checkInSIMInventory;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

    @Override
    public String toString() {
        return "CreateNeoSIMPacksRequest{" + "userEmail=" + userEmail + ", numberOfSIMPacks=" + numberOfSIMPacks + ", packSize=" + packSize + ", iccidStart=" + iccidStart + ", iccidEnd=" + iccidEnd + ", distributorAccountId=" + distributorAccountId + ", checkInSIMInventory=" + checkInSIMInventory + ", simType=" + simType + '}';
    }
}
