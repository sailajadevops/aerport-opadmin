package com.aeris.service.operatoradmin.dao.impl;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IRESDAO;
import com.aeris.service.operatoradmin.enums.IRESEnum;
import com.aeris.service.operatoradmin.exception.RESException;
import com.aeris.service.operatoradmin.model.RES;

public class RESDAO implements IRESDAO {
	private static Logger LOG = LoggerFactory.getLogger(RESDAO.class);
	
	@Inject
	private IRESEnum resCache;
	
	@Override
	public List<RES> getRESList(int productId) throws RESException {
		LOG.info("getRES : processing getRES for productId = "+productId);
		
		return resCache.getRESList(productId);
	}

}
