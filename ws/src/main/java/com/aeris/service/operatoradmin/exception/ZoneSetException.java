package com.aeris.service.operatoradmin.exception;

public class ZoneSetException extends Exception{

	private static final long serialVersionUID = -4622495956895576183L;

	public ZoneSetException(String s){
		super(s);
	}
	public ZoneSetException(String ex, Throwable t) {
		super(ex, t);
	}
}
