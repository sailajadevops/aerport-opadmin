package com.aeris.service.operatoradmin.exception;

public class ZoneMappingException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public ZoneMappingException(String s) {
		super(s);
	}

	public ZoneMappingException(String ex, Throwable t) {
		super(ex, t);
	}
}
