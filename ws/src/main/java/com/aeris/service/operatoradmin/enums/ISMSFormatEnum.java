package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.SMSFormat;

public interface ISMSFormatEnum  extends IBaseEnum{
	SMSFormat getSMSFormatByName(String name);
	SMSFormat getSMSFormatById(int id);
	List<SMSFormat> getAllSMSFormats();
}
