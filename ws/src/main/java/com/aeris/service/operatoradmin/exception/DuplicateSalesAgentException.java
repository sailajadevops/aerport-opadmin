package com.aeris.service.operatoradmin.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class DuplicateSalesAgentException extends WebApplicationException {
	private static final long serialVersionUID = -6884633836582427897L;

	public DuplicateSalesAgentException(String s) {
		super(Response.status(Response.Status.BAD_REQUEST)
	             .entity(s).build());
	}
}
