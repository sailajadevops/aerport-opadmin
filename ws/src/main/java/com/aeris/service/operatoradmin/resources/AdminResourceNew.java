package com.aeris.service.operatoradmin.resources;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.constraints.Pattern;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IOperatorConfigDAO;
import com.aeris.service.operatoradmin.enums.IAccessPointNameEnum;
import com.aeris.service.operatoradmin.enums.ICountryEnum;
import com.aeris.service.operatoradmin.enums.ICurrencyEnum;
import com.aeris.service.operatoradmin.enums.IIccIdRangeEnum;
import com.aeris.service.operatoradmin.enums.IImsiRangeEnum;
import com.aeris.service.operatoradmin.enums.IOperatorEnum;
import com.aeris.service.operatoradmin.enums.IPacketRoundingPolicyEnum;
import com.aeris.service.operatoradmin.enums.IPoolingPolicyEnum;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.enums.ISIMFormatEnum;
import com.aeris.service.operatoradmin.enums.ISIMSupplierEnum;
import com.aeris.service.operatoradmin.enums.ISIMWarehouseEnum;
import com.aeris.service.operatoradmin.enums.ISMPPConnectionEnum;
import com.aeris.service.operatoradmin.enums.ISMSFormatEnum;
import com.aeris.service.operatoradmin.enums.ITierCriteriaEnum;
import com.aeris.service.operatoradmin.enums.IVerticalEnum;
import com.aeris.service.operatoradmin.exception.OperatorException;
import com.aeris.service.operatoradmin.model.AccessPointName;
import com.aeris.service.operatoradmin.model.AccessPointNameType;
import com.aeris.service.operatoradmin.model.Country;
import com.aeris.service.operatoradmin.model.Currency;
import com.aeris.service.operatoradmin.model.IccIdRange;
import com.aeris.service.operatoradmin.model.ImsiRange;
import com.aeris.service.operatoradmin.model.Operator;
import com.aeris.service.operatoradmin.model.PacketRoundingPolicy;
import com.aeris.service.operatoradmin.model.PoolingPolicy;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.RoamingOperator;
import com.aeris.service.operatoradmin.model.SIMFormat;
import com.aeris.service.operatoradmin.model.SIMSupplier;
import com.aeris.service.operatoradmin.model.SIMWarehouse;
import com.aeris.service.operatoradmin.model.SMPPConnection;
import com.aeris.service.operatoradmin.model.SMSFormat;
import com.aeris.service.operatoradmin.model.TierCriteria;
import com.aeris.service.operatoradmin.model.Vertical;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;

/**
 * Internal Rest Resource for fetching master data in portal. 
 * Includes methods for
 *  
 * <ul>
 * <li>
 * Fetch Products available in portal
 * </li>
 * <li>
 * Fetch list of operators in portal
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}/admin")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class AdminResourceNew{
	private static final Logger LOG = LoggerFactory.getLogger(AdminResourceNew.class);

    @Inject
	private IProductEnum productCache;
    
    @Inject
	private IOperatorEnum operatorCache;        

    @Inject
	private ISIMSupplierEnum simSupplierCache;        

    @Inject
	private ISIMWarehouseEnum simWarehouseCache;        

    @Inject
	private ISIMFormatEnum simFormatCache;        

    @Inject
	private IAccessPointNameEnum accessPointNameCache;    
    
    @Inject
	private IVerticalEnum verticalCache;        

    @Inject
	private ISMPPConnectionEnum smppConnectionCache;    
    
    @Inject
   	private ISMSFormatEnum smsFormatCache;    
    
    @Inject
   	private ICountryEnum countryCache;    
    
    @Inject
	private IOperatorConfigDAO operatorConfigDAO;

    @Inject
	private IIccIdRangeEnum iccidRangeCache;
    
    @Inject
	private IImsiRangeEnum imsiRangeCache;
    
    @Inject
	private ICurrencyEnum currencyCache;
    
    @Inject
	private IPoolingPolicyEnum poolingPolicyCache;
    
    @Inject
	private IPacketRoundingPolicyEnum packetRoundingPolicyCache;
    
    @Inject
	private ITierCriteriaEnum tierCriteriaCache;
    
    @GET
    @Path("/products")
    @RolesAllowed( { "OPERATOR_WR", "OPERATOR" } )
	public Response getAllProducts() {
    	LOG.info("Received request to fetch all Products");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<Product> products = null;
		
		try {
			// Fetch from database
			products = productCache.getAllProducts();
						
			if (products != null) {
				LOG.info("fetched products successfully, no of products: "+products.size());

				ObjectMapper mapper = new ObjectMapper();
				String productsJson = mapper.writeValueAsString(products);

				builder.entity(productsJson);
			} else {
				LOG.info("No products found in the db");
				return Response.status(Status.NOT_FOUND).entity("No products found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllProducts Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting products").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllProducts: " + response);
		return response;
	}

    @GET
    @Path("/operators")
    @RolesAllowed( { "PLATFORM_ADMIN" } )
	public Response getAllOperators() {
    	LOG.info("Received request to fetch all Operators");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<Operator> operators = null;
		
		try {
			// Fetch from database
			operators = operatorCache.getAllOperators();
						
			if (operators != null) {
				LOG.info("fetched operators successfully, no of operators: "+operators.size());

				ObjectMapper mapper = new ObjectMapper();
				String operatorsJson = mapper.writeValueAsString(operators);

				builder.entity(operatorsJson);
			} else {
				LOG.info("No operators found in the db");
				return Response.status(Status.NOT_FOUND).entity("No operators found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllOperators Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting operators").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllOperators: " + response);
		return response;
	}

    @GET
    @Path("/simsuppliers")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllSIMSuppliers() {
    	LOG.info("Received request to fetch all sim suppliers");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<SIMSupplier> simSuppliers = null;
		
		try {
			// Fetch from database
			simSuppliers = simSupplierCache.getAllSIMSuppliers();
						
			if (simSuppliers != null) {
				LOG.info("fetched sim suppliers successfully, no of sim suppliers: "+simSuppliers.size());

				ObjectMapper mapper = new ObjectMapper();
				String simSuppliersJson = mapper.writeValueAsString(simSuppliers);

				builder.entity(simSuppliersJson);
			} else {
				LOG.info("No sim suppliers found in the db");
				return Response.status(Status.NOT_FOUND).entity("No sim suppliers found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllSIMSuppliers Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting sim suppliers").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllSIMSuppliers: " + response);
		return response;
	}

    @GET
    @Path("/simwarehouses")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllSIMWarehouses() {
    	LOG.info("Received request to fetch all sim warehouses");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<SIMWarehouse> simWarehouses = null;
		
		try {
			// Fetch from database
			simWarehouses = simWarehouseCache.getAllSIMWarehouses();
						
			if (simWarehouses != null) {
				LOG.info("fetched sim warehouses successfully, no of sim warehouses: "+simWarehouses.size());

				ObjectMapper mapper = new ObjectMapper();
				String simWarehousesJson = mapper.writeValueAsString(simWarehouses);

				builder.entity(simWarehousesJson);
			} else {
				LOG.info("No sim warehouses found in the db");
				return Response.status(Status.NOT_FOUND).entity("No sim warehouses found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllSIMWarehouses Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting sim warehouses").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllSIMWarehouses: " + response);
		return response;
	}

    @GET
    @Path("/simformats")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllSIMFormats() {
    	LOG.info("Received request to fetch all sim formats");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<SIMFormat> simFormats = null;
		
		try {
			// Fetch from database
			simFormats = simFormatCache.getAllSIMFormats();
						
			if (simFormats != null) {
				LOG.info("fetched sim formats successfully, no of sim formats: "+simFormats.size());

				ObjectMapper mapper = new ObjectMapper();
				String simFormatsJson = mapper.writeValueAsString(simFormats);

				builder.entity(simFormatsJson);
			} else {
				LOG.info("No sim formats found in the db");
				return Response.status(Status.NOT_FOUND).entity("No sim formats found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllSIMFormats Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting sim formats").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllSIMFormats: " + response);
		return response;
	}

    @GET
    @Path("/accesspointnames")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllAccessPointNames(@QueryParam("type") String type) {
    	LOG.info("Received request to fetch all access point names");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<AccessPointName> accessPointNames = null;
		
		try {
			// Fetch from database
			if(AccessPointNameType.isValid(type))
			{
				accessPointNames = accessPointNameCache.getAccessPointNamesByByType(type);
			}
			else
			{
				accessPointNames = accessPointNameCache.getAllAccessPointNames();
			}
						
			if (accessPointNames != null) {
				LOG.info("fetched access point names successfully, no of access point names: "+accessPointNames.size());

				ObjectMapper mapper = new ObjectMapper();
				String accessPointNamesJson = mapper.writeValueAsString(accessPointNames);

				builder.entity(accessPointNamesJson);
			} else {
				LOG.info("No access point names found in the db");
				return Response.status(Status.NOT_FOUND).entity("No access point names found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllAccessPointNames Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting access point names").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllAccessPointNames: " + response);
		return response;
	}

    @GET
    @Path("/verticals")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllVerticals() {
    	LOG.info("Received request to fetch all sim formats");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<Vertical> verticals = null;
		
		try {
			// Fetch from database
			verticals = verticalCache.getAllVerticals();
						
			if (verticals != null) {
				LOG.info("fetched sim formats successfully, no of sim formats: "+verticals.size());

				ObjectMapper mapper = new ObjectMapper();
				String verticalsJson = mapper.writeValueAsString(verticals);

				builder.entity(verticalsJson);
			} else {
				LOG.info("No verticals found in the db");
				return Response.status(Status.NOT_FOUND).entity("No verticals found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllVerticals Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting verticals").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllVerticals: " + response);
		return response;
	}

    @GET
    @Path("/smppconnections")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllSMPPConnections() {
    	LOG.info("Received request to fetch all smpp connections");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<SMPPConnection> smppConnections = null;
		
		try {
			// Fetch from database
			smppConnections = smppConnectionCache.getAllSMPPConnections();
						
			if (smppConnections != null) {
				LOG.info("fetched smpp connections successfully, no of smpp connections: "+smppConnections.size());

				ObjectMapper mapper = new ObjectMapper();
				String smppConnectionsJson = mapper.writeValueAsString(smppConnections);

				builder.entity(smppConnectionsJson);
			} else {
				LOG.info("No smpp connections found in the db");
				return Response.status(Status.NOT_FOUND).entity("No smpp connections found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllSMPPConnections Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting smpp connections").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllSMPPConnections: " + response);
		return response;
	}

    @GET
    @Path("/smsformats")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllSMSFormats() {
    	LOG.info("Received request to fetch all sms formats");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<SMSFormat> smsFormats = null;
		
		try {
			// Fetch from database
			smsFormats = smsFormatCache.getAllSMSFormats();
						
			if (smsFormats != null) {
				LOG.info("fetched sms formats successfully, no of sms formats: "+smsFormats.size());

				ObjectMapper mapper = new ObjectMapper();
				String smsFormatsJson = mapper.writeValueAsString(smsFormats);

				builder.entity(smsFormatsJson);
			} else {
				LOG.info("No sms formats found in the db");
				return Response.status(Status.NOT_FOUND).entity("No sms formats found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllSMSFormats Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting sms formats").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllSMSFormats: " + response);
		return response;
	}

    @GET
    @Path("/countries")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllCountries() {
    	LOG.info("Received request to fetch all roaming countries");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<Country> countries = null;
		
		try {
			// Fetch from database
			countries = countryCache.getAllCountries();
						
			if (countries != null) {
				LOG.info("fetched roaming countries successfully, no of roaming countries: "+countries.size());

				ObjectMapper mapper = new ObjectMapper();
				String countriesJson = mapper.writeValueAsString(countries);

				builder.entity(countriesJson);
			} else {
				LOG.info("No roaming countries found in the db");
				return Response.status(Status.NOT_FOUND).entity("No roaming countries found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllRoamingCountries Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting roaming countries").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllRoamingCountries: " + response);
		return response;
	}

    @GET
    @Path("/roamingcountries")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getRoamingCountries(@QueryParam("operatorId") @ValidOperator final String operatorId,
			@QueryParam("filter") @Pattern(regexp = "[0-9]+", message = "{filter.number}") final String filterZoneId) {
    	LOG.info("Received request to fetch roaming countries for operator id: "+operatorId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<Country> countries = null;
		
		try {
			// Fetch from database
			countries = operatorConfigDAO.getRoamingCountries(Integer.parseInt(operatorId), NumberUtils.toInt(filterZoneId, 0));
						
			if (countries != null) {
				LOG.info("fetched roaming countries successfully, no of roaming countries: "+countries.size());

				ObjectMapper mapper = new ObjectMapper();
				String configurationJson = mapper.writeValueAsString(countries);

				builder.entity(configurationJson);	
			} else {
				LOG.info("No roaming countries found in the db associated to operator id: "+operatorId);
				return Response.status(Status.NOT_FOUND).entity("No roaming countries found in the db associated to operator id: "+operatorId).build();
			}
		} catch (OperatorException e) {
			LOG.error("OperatorException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getRoamingCountries Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting roaming countries").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getRoamingCountries: " + response);
		return response;
	} 

    @GET
    @Path("/roamingoperators")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getRoamingOperators(@QueryParam("operatorId") @ValidOperator final String operatorId,
			@QueryParam("filter") @Pattern(regexp = "[0-9]+", message = "{filter.number}") final String filterZoneId) {
    	LOG.info("Received request to fetch roaming operators for operator id: "+operatorId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<RoamingOperator> operators = null;
		
		try {
			// Fetch from database
			operators = operatorConfigDAO.getRoamingOperators(Integer.parseInt(operatorId), NumberUtils.toInt(filterZoneId, 0));
						
			if (operators != null) {
				LOG.info("fetched roaming operators successfully, no of roaming operators: "+operators.size());

				ObjectMapper mapper = new ObjectMapper();
				String configurationJson = mapper.writeValueAsString(operators);

				builder.entity(configurationJson);	
			} else {
				LOG.info("No roaming operators found in the db associated to operator id: "+operatorId);
				return Response.status(Status.NOT_FOUND).entity("No roaming operators found in the db associated to operator id: "+operatorId).build();
			}
		} catch (OperatorException e) {
			LOG.error("OperatorException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getRoamingOperators Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting roaming operators").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getRoamingOperators: " + response);
		return response;
	}
    
    @GET
    @Path("/iccidranges")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllIccIdRanges() {
    	LOG.info("Received request to fetch all Products");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<IccIdRange> iccidRanges = null;
		
		try {
			// Fetch from database
			iccidRanges = iccidRangeCache.getAllIccIdRanges();
						
			if (iccidRanges != null) {
				LOG.info("fetched iccid ranges successfully, no of iccid ranges: "+iccidRanges.size());

				ObjectMapper mapper = new ObjectMapper();
				String iccidRangesJson = mapper.writeValueAsString(iccidRanges);

				builder.entity(iccidRangesJson);
			} else {
				LOG.info("No iccid ranges found in the db");
				return Response.status(Status.NOT_FOUND).entity("No iccid ranges found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllIccIdRanges Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting iccid ranges").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllProducts: " + response);
		return response;
	}
    
    @GET
    @Path("/imsiranges")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllImsiRanges() {
    	LOG.info("Received request to fetch all Products");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<ImsiRange> imsiRanges = null;
		
		try {
			// Fetch from database
			imsiRanges = imsiRangeCache.getAllImsiRanges();
						
			if (imsiRanges != null) {
				LOG.info("fetched imsi ranges successfully, no of imsi ranges: "+imsiRanges.size());

				ObjectMapper mapper = new ObjectMapper();
				String imsiRangesJson = mapper.writeValueAsString(imsiRanges);

				builder.entity(imsiRangesJson);
			} else {
				LOG.info("No imsi ranges found in the db");
				return Response.status(Status.NOT_FOUND).entity("No imsi ranges found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllImsiRanges Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting imsi ranges").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllProducts: " + response);
		return response;
	}
    
    @GET
    @Path("/currencies")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllCurrencies() {
    	LOG.info("Received request to fetch all Currencies");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<Currency> currencies = null;
		
		try {
			// Fetch from database
			currencies = currencyCache.getAllCurrencies();
						
			if (currencies != null) {
				LOG.info("fetched currencies successfully, no of currencies: "+currencies.size());

				ObjectMapper mapper = new ObjectMapper();
				String currenciesJson = mapper.writeValueAsString(currencies);

				builder.entity(currenciesJson);
			} else {
				LOG.info("No currencies found in the db");
				return Response.status(Status.NOT_FOUND).entity("No currencies found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllCurrencies Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting currencies").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllCurrencies: " + response);
		return response;
	}
    
    @GET
    @Path("/poolingpolicies")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllPoolingPolicies() {
    	LOG.info("Received request to fetch all pooling policies");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<PoolingPolicy> poolingPolicies = null;
		
		try {
			// Fetch from database
			poolingPolicies = poolingPolicyCache.getAllPoolingPolicies();
						
			if (poolingPolicies != null) {
				LOG.info("fetched pooling policies successfully, no of pooling policies: "+poolingPolicies.size());

				ObjectMapper mapper = new ObjectMapper();
				String poolingPoliciesJson = mapper.writeValueAsString(poolingPolicies);

				builder.entity(poolingPoliciesJson);
			} else {
				LOG.info("No pooling policies found in the db");
				return Response.status(Status.NOT_FOUND).entity("No pooling policies found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllPoolingPolicies Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting pooling policies").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllPoolingPolicies: " + response);
		return response;
	}
    
    @GET
    @Path("/packetroundingpolicies")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllPacketRoundingPolicies() {
    	LOG.info("Received request to fetch all packet rounding policies");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<PacketRoundingPolicy> packetRoundingPolicies = null;
		
		try {
			// Fetch from database
			packetRoundingPolicies = packetRoundingPolicyCache.getAllPacketRoundingPolicies();
						
			if (packetRoundingPolicies != null) {
				LOG.info("fetched packet rounding policies successfully, no of packet rounding policies: "+packetRoundingPolicies.size());

				ObjectMapper mapper = new ObjectMapper();
				String packetRoundingPoliciesJson = mapper.writeValueAsString(packetRoundingPolicies);

				builder.entity(packetRoundingPoliciesJson);
			} else {
				LOG.info("No packet rounding policies found in the db");
				return Response.status(Status.NOT_FOUND).entity("No packet rounding policies found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllPacketRoundingPolicies Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting packet rounding policies").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllPacketRoundingPolicies: " + response);
		return response;
	}
    
    @GET
    @Path("/tiercriterion")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllTierCriteria() {
    	LOG.info("Received request to fetch all tier criteria");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<TierCriteria> tierCriterion = null;
		
		try {
			// Fetch from database
			tierCriterion = tierCriteriaCache.getAllTierCriteria();
						
			if (tierCriterion != null) {
				LOG.info("fetched tier criteria successfully, no of tier criteria: "+tierCriterion.size());

				ObjectMapper mapper = new ObjectMapper();
				String tierCriterionJson = mapper.writeValueAsString(tierCriterion);

				builder.entity(tierCriterionJson);
			} else {
				LOG.info("No tier criteria found in the db");
				return Response.status(Status.NOT_FOUND).entity("No tier criteria found in the db").build();
			}
		} catch (Exception e) {
			LOG.error("getAllTierCriteria Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting tier criteria").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllTierCriteria: " + response);
		return response;
	}
    
}
