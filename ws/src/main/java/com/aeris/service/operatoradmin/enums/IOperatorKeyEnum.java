package com.aeris.service.operatoradmin.enums;

import com.aeris.service.operatoradmin.model.ApiKey;

/**
 * Operator enum 
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public interface IOperatorKeyEnum extends IBaseEnum {

	boolean isValid(String apiKey);

	ApiKey get(String apiKey);

}
