package com.aeris.service.operatoradmin.resources;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.IGlobalSIMInventoryDAO;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.SIMStatus;
import com.aeris.service.operatoradmin.model.SIMSummary;
import com.aeris.service.operatoradmin.model.SIMSupplier;
import com.aeris.service.operatoradmin.model.SIMWarehouse;

/**
 * Rest Resource for SIM Inventory management. Includes methods for 
 * <ul>
 * <li>
 * Fetch SIM Summary grouped by warehouse.
 * </li>
 * <li>
 * Fetch SIM Summary for an account
 * </li>
 * <li>
 * Fetch SIM Summary Per Warehouse grouped by Supplier.
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operator/{operatorId}/simInventory")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class SIMInventoryResource{
	private static final Logger LOG = LoggerFactory.getLogger(SIMInventoryResource.class);

	@Inject
	private IGlobalSIMInventoryDAO inventoryDAO;
    
    @Inject
    private IAccountDAO accountDAO;
    
    @GET
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getSIMSummaryGroupedByWarehouseOrStatus(@PathParam("operatorId") final String operatorId, @QueryParam("accountId") String accountId) {
		LOG.info("Received request to fetch SIM Inventory grouped by warehouse for operator: " + operatorId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			if (!NumberUtils.isNumber(operatorId)) {
				LOG.error("The format of operatorId in the request is not valid, value is: '" + operatorId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of operatorId in the request is not valid").build();
			}
			
			// If Account ID is not specified, fetch full SIM Summary
			if(StringUtils.isEmpty(accountId))
			{
				SIMSummary<SIMWarehouse> summary = inventoryDAO.getSIMSummaryGroupedByWarehouse();
				
				if (summary != null) {
					LOG.info("fetched SIM Summary successfully, summary details: "+summary.getSummary());

					ObjectMapper mapper = new ObjectMapper();
					String apiKeysJson = mapper.writeValueAsString(summary);

					builder.entity(apiKeysJson);
				} else {
					LOG.info("Full SIM Summary is not available");
					return Response.status(Status.NOT_FOUND).entity("Full SIM Summary is not available").build();
				}
			}
			// Fetch SIM Summary for the account only
			else
			{
				// Fetch Account Details
				Account accountInfo = accountDAO.getAccount(operatorId, accountId);
				
				// Fetch SIM Summary for the account grouped by status USED or NOT_USED or ALLOCATED
				SIMSummary<SIMStatus> summary = inventoryDAO.getSIMSummaryGroupedByStatus(accountInfo);
				
				if (summary != null) {
					LOG.info("fetched SIM Summary successfully, summary details: "+summary.getSummary());

					ObjectMapper mapper = new ObjectMapper();
					String apiKeysJson = mapper.writeValueAsString(summary);

					builder.entity(apiKeysJson);
				} else {
					LOG.info("Full SIM Summary is not available");
					return Response.status(Status.NOT_FOUND).entity("Full SIM Summary is not available").build();
				}
			}
		} catch (Exception e) {
			LOG.error("getAllSIMOrders Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting SIM Summary").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getSIMSummaryBySupplier: " + response);
		return response;
	}
    
    @GET
    @Path("/warehouse/{warehouseId}")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getSIMSummaryGroupedBySupplier(@PathParam("operatorId") final String operatorId, @PathParam("warehouseId") final String warehouseId) {
		LOG.info("Received request to fetch SIM Inventory for warehouse: " + warehouseId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			if (!NumberUtils.isNumber(operatorId)) {
				LOG.error("The format of operatorId in the request is not valid, value is: '" + operatorId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of operatorId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(warehouseId)) {
				LOG.error("The format of warehouseId in the request is not valid, value is: '" + warehouseId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of warehouseId in the request is not valid").build();
			}
			
			// TODO - Replace with the DAO Call or Fetch from Warehouse Cache
			SIMWarehouse warehouse = new SIMWarehouse();
			warehouse.setWarehouseId(Integer.parseInt(warehouseId));			
			
			SIMSummary<SIMSupplier> summary = inventoryDAO.getSIMSummaryGroupedBySupplier(warehouse);
			
			if (summary != null) {
				LOG.info("fetched SIM Summary successfully, summary details: "+summary.getSummary());

				ObjectMapper mapper = new ObjectMapper();
				String apiKeysJson = mapper.writeValueAsString(summary);

				builder.entity(apiKeysJson);
			} else {
				LOG.info("No SIM Summary found for warehouse: "+warehouseId);
				return Response.status(Status.NOT_FOUND).entity("No SIM Summary found for warehouse: "+warehouseId).build();
			}
		} catch (Exception e) {
			LOG.error("getAllSIMOrders Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting SIM Summary").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getSIMSummaryBySupplier: " + response);
		return response;
	}
}
