package com.aeris.service.operatoradmin.utils;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.IRatePlanDAO;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.dao.IZoneSetDAO;
import com.aeris.service.operatoradmin.delegate.TemplateData;
import com.aeris.service.operatoradmin.delegate.impl.SimpleTemplateProcessor;
import com.aeris.service.operatoradmin.enums.ICurrencyEnum;
import com.aeris.service.operatoradmin.enums.IPacketRoundingPolicyEnum;
import com.aeris.service.operatoradmin.enums.IPoolingPolicyEnum;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.exception.ExportException;
import com.aeris.service.operatoradmin.exception.RatePlanException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.exception.ZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetNotFoundException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.CarrierRatePlan;
import com.aeris.service.operatoradmin.model.Currency;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.PacketRoundingPolicy;
import com.aeris.service.operatoradmin.model.PoolingPolicy;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.ProvisionTriggerSettings;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanOverageBucket;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.SuspendTriggerSettings;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZoneRatePlan;
import com.aeris.service.operatoradmin.model.ZoneSet;
import com.google.inject.Inject;

public class NonTieredRatePlanExporter{
	private static Logger LOG = LoggerFactory.getLogger(NonTieredRatePlanExporter.class);
	
	private static final String NON_TIERED_RATE_PLAN_TEMPLATE = "/tpl/export_nontiered_rate_plan.tpl";
	private static final String NON_TIERED_RATE_PLAN_TEMPLATE_CC = "/tpl/export_nontiered_rate_plan_cc.tpl";
	
	@Inject
	private IProductEnum productCache;	
	
	@Inject
	private IZoneDAO zoneDAO;	
	
	@Inject
	private IZoneSetDAO zoneSetDAO;

	@Inject
	private IRatePlanDAO ratePlanDAO;	

	@Inject
	private IAccountDAO accountDAO;	
	
	@Inject
	private IPoolingPolicyEnum poolingPolicyCache;	
	
	@Inject
	private IPacketRoundingPolicyEnum packetRoundingPolicyCache;	
	
	@Inject
	private ICurrencyEnum currencyCache;	
	
	@Inject
	private OAUtils oaUtils;	
	
	public byte[] exportRatePlanToTextFile(RatePlan ratePlan, String newline) throws ExportException {
		TemplateData data = buildTemplateData(ratePlan);
		
		// Process the rate plan template with data and return the content
		SimpleTemplateProcessor delegate = new SimpleTemplateProcessor(newline);
		String templateToUse = NON_TIERED_RATE_PLAN_TEMPLATE ;
		if(ratePlan.getOperatorId() == 2) {
			templateToUse = NON_TIERED_RATE_PLAN_TEMPLATE_CC ;
		}
		byte[] bytes = delegate.createByteStreamFromTemplate(templateToUse, data);
		
		return bytes;
	}

	private TemplateData buildTemplateData(RatePlan ratePlan) {
		TemplateData data = new TemplateData();
		
		if(ratePlan != null)
		{
			String fileName = oaUtils.generateExportedFileName(ratePlan);
					
			// Set File Name as header in the template
			setFileName(fileName, data);
			
			// Set Item Dimensions
			setItemDimensions(data);
			
			// Set Basic Information in the template
			setRatePlanBasicInfo(ratePlan, data);
			
			// Set Category Information in the template
			setRatePlanCategoryInfo(ratePlan, data);
			
			// Set Fees Information in the template
			setRatePlanFeesInfo(ratePlan, data);
			
			// Set Included Policy Information in the template
			setIncludedPolicyInfo(ratePlan, data);			

			// Set Provision triggers and fees Information in the template
			setProvisionTriggerSettingsInfo(ratePlan, data);

			// Set Suspend triggers and fees Information in the template
			setSuspendTriggerSettingsInfo(ratePlan, data);
			
			//set overage bucket info
			setOverageBucketInfo(ratePlan, data);
			
			//set cancellation fee info
			setCancellationFeeInfo(ratePlan, data);
			
			// Set Zone Rate Plan Information in the template
			setZoneRatePlanSettingsInfo(ratePlan, data);
		}
		
		return data;
	}

	private void setItemDimensions(TemplateData data) {
		data.setItemWidth("*", 35);
		data.setItemWidth("*ZONE*", 32);
		data.setItemWidth("*INCL_PKT*", 14);
		data.setItemWidth("*PKT_COST*", 14);
		data.setItemWidth("*INCL_MTS*", 14);
		data.setItemWidth("*MTS_COST*", 14);
		data.setItemWidth("*INCL_MOS*", 14);
		data.setItemWidth("*MOS_COST*", 14);
		data.setItemWidth("*INCL_MTV*", 14);
		data.setItemWidth("*MTV_COST*", 14);
		data.setItemWidth("*INCL_MOV*", 14);
		data.setItemWidth("*MOV_COST*", 14);
	}

	private void setSuspendTriggerSettingsInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting suspend triggers and fees info in rate plan template");
		
		SuspendTriggerSettings suspendTriggerSettings = ratePlan.getSuspendTriggerSettings();
		
		if(suspendTriggerSettings != null)
		{
			data.put("SUSP_DUR_WITH_UNIT", StringUtils.join(new Object[]{suspendTriggerSettings.getDuration(), " ", suspendTriggerSettings.getDurationUnit().name()}));
			data.put("SUSP_PKT_WITH_UNIT", StringUtils.join(new Object[]{suspendTriggerSettings.getPacketThreshold(), " ", suspendTriggerSettings.getPacketThresholdUnit().name()}));
			data.put("SUSP_MT_SMS_THRESHOLD", String.valueOf(suspendTriggerSettings.getMtSmsThreshold()));
			data.put("SUSP_MO_SMS_THRESHOLD", String.valueOf(suspendTriggerSettings.getMoSmsThreshold()));
			data.put("SUSP_MT_VOICE_THRESHOLD", String.valueOf(suspendTriggerSettings.getMtVoiceMinsThreshold()));
			data.put("SUSP_MO_VOICE_THRESHOLD", String.valueOf(suspendTriggerSettings.getMoVoiceMinsThreshold()));
			data.put("SUSP_TRAFFIC_DAYS", String.valueOf(suspendTriggerSettings.getTrafficDays()));
			
			data.put("SUSP_PACKET_COST", String.valueOf(suspendTriggerSettings.getPerKBPacketPrice()));
			data.put("SUSP_MT_SMS_COST", String.valueOf(suspendTriggerSettings.getPerMTSmsPrice()));
			data.put("SUSP_MO_SMS_COST", String.valueOf(suspendTriggerSettings.getPerMOSmsPrice()));
			data.put("SUSP_MT_VOICE_COST", String.valueOf(suspendTriggerSettings.getPerMinMTVoicePrice()));
			data.put("SUSP_MO_VOICE_COST", String.valueOf(suspendTriggerSettings.getPerMinMOVoicePrice()));
		}
	}
	
	private void setZoneRatePlanSettingsInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting zone rate plan settings in rate plan template");
		
		int zoneLength = 0;
		
		List<ZoneRatePlan> zoneProfiles = ratePlan.getZoneRatePlanSettings();
		
		if(zoneProfiles != null)
		{
			int i = 1;
			int count = 0;
			
			for (ZoneRatePlan zoneRatePlan : zoneProfiles) {
				int zoneId = zoneRatePlan.getZoneId();
				Zone zone = null; 
				
				try {
					zone = zoneDAO.getZone(ratePlan.getOperatorId(),ratePlan.getZoneSetId(), zoneId);
				} catch (ZoneNotFoundException e) {
					LOG.warn("Zone id: "+zoneId+" not found while setting zone customization info in the template");
				} catch (ZoneException e) {
					LOG.warn("Fetching Zone failed while setting zone customization info in the template");
				}
				
				if(zone != null)
				{	
					// Put Default Zone Rating Policy Info			
					data.put("DZONE"+i, zone.getZoneName());
					
					IncludedPlanDetails planDetails = zone.getIncludedPlan();
					
					if(planDetails != null)
					{
						data.put("DINCL_PKT"+i, String.valueOf(planDetails.getIncludedPacketKB()));
						data.put("DPKT_COST"+i, String.valueOf(planDetails.getPerKBPacketPrice()));
						data.put("DINCL_MTS"+i, String.valueOf(planDetails.getIncludedMTSms()));
						data.put("DMTS_COST"+i, String.valueOf(planDetails.getPerMTSmsPrice()));
						data.put("DINCL_MOS"+i, String.valueOf(planDetails.getIncludedMOSms()));
						data.put("DMOS_COST"+i, String.valueOf(planDetails.getPerMOSmsPrice()));		
						data.put("DINCL_MTV"+i, String.valueOf(planDetails.getIncludedMTVoiceMins()));
						data.put("DMTV_COST"+i, String.valueOf(planDetails.getPerMinMTVoicePrice()));
						data.put("DINCL_MOV"+i, String.valueOf(planDetails.getIncludedMOVoiceMins()));
						data.put("DMOV_COST"+i, String.valueOf(planDetails.getPerMinMOVoicePrice()));	
												
					}	
					
					// Put Customized Zone Rating Policy Info for this rate plan
					data.put("ZONE"+i, zone.getZoneName());
					
					planDetails = zoneRatePlan.getIncludedPlanDetails();
					
					if(planDetails != null)
					{
						data.put("INCL_PKT"+i, String.valueOf(planDetails.getIncludedPacketKB()));
						data.put("PKT_COST"+i, String.valueOf(planDetails.getPerKBPacketPrice()));
						data.put("INCL_MTS"+i, String.valueOf(planDetails.getIncludedMTSms()));
						data.put("MTS_COST"+i, String.valueOf(planDetails.getPerMTSmsPrice()));
						data.put("INCL_MOS"+i, String.valueOf(planDetails.getIncludedMOSms()));
						data.put("MOS_COST"+i, String.valueOf(planDetails.getPerMOSmsPrice()));		
						data.put("INCL_MTV"+i, String.valueOf(planDetails.getIncludedMTVoiceMins()));
						data.put("MTV_COST"+i, String.valueOf(planDetails.getPerMinMTVoicePrice()));
						data.put("INCL_MOV"+i, String.valueOf(planDetails.getIncludedMOVoiceMins()));
						data.put("MOV_COST"+i, String.valueOf(planDetails.getPerMinMOVoicePrice()));	
												
					}	
										
					i++;
					count++;
				}	
				
				zoneLength = count;
			}
		}
		
		data.put("ZONE_LEN", String.valueOf(zoneLength));
	}
	
	
	private void setOverageBucketInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting Overage Bucket settings in rate plan template");
		int bktLength = 0;
		List<RatePlanOverageBucket> overageBktList = ratePlan.getRatePlanOverageBucket();
		if(overageBktList != null) {
			int i = 1;
			int count = 0;
			for (RatePlanOverageBucket overageBucket : overageBktList) {
				if(overageBucket != null) {	
					data.put("BKT_STRT_DT" +i, overageBucket.getStartDate().toString());
					data.put("BKT_END_DT" +i, overageBucket.getEndDate().toString());
					data.put("BKT_SIZE" +i, String.valueOf(overageBucket.getBucketSize()).trim());
					data.put("BKT_PRICE" +i, String.valueOf(overageBucket.getBucketPrice()).trim());
					if(ratePlan.getOperatorId() == 2) {
						data.put("BKT_ACCFEE_DVCE_CNT" +i, "N/A");
					} else {
						data.put("BKT_ACCFEE_DVCE_CNT" +i, String.valueOf(overageBucket.getNoAccessFeeDeviceCount()).trim());
					}
					data.put("BKT_AUTO_ADD" +i, BooleanUtils.toStringYesNo(overageBucket.getAutoAdd() == 1).toUpperCase());
					i++;
					count++;
				}	
				bktLength = count;
			}
		}
		data.put("BKT_LEN", String.valueOf(bktLength));
	}
	
	
	private void setCancellationFeeInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting Overage Bucket settings in rate plan template");
		data.put("CANFEE_PRO_RT_CANFEE", "" + BooleanUtils.toStringYesNo(ratePlan.getProRateCancellationFee() == 1).toUpperCase());
		if(ratePlan.getContractTerm() < 0) {
			ratePlan.setContractTerm(0);
		}
		data.put("CANFEE_CNTRT_TRM", "" +  ratePlan.getContractTerm());
		data.put("CANFEE_AUTO_CNCL", BooleanUtils.toStringYesNo(ratePlan.getAutoCancelAtTermEnd() == 1).toUpperCase());
		data.put("CANFEE_ACC_PROV_STAT" , BooleanUtils.toStringYesNo(ratePlan.getAccountForDaysInProvisionState() == 1).toUpperCase());
		data.put("CANFEE_ACC_SPND_STAT" , BooleanUtils.toStringYesNo(ratePlan.getAccountForDaysInSuspendState() == 1).toUpperCase());
		if(ratePlan.getCancellationFeeReductionInterval() <= 0) {
			data.put("CANFEE_RDN_INTL" , "-");
		} else {
			data.put("CANFEE_RDN_INTL" , String.valueOf(ratePlan.getCancellationFeeReductionInterval()));
		}
	}
	

	private void setProvisionTriggerSettingsInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting provision triggers and fees info in rate plan template");
		
		ProvisionTriggerSettings provisionTriggerSettings = ratePlan.getProvisionTriggerSettings();
		
		if(provisionTriggerSettings != null)
		{
			data.put("PROV_DUR_WITH_UNIT", StringUtils.join(new Object[]{provisionTriggerSettings.getDuration(), " ", provisionTriggerSettings.getDurationUnit().name()}));
			data.put("PROV_PKT_WITH_UNIT", StringUtils.join(new Object[]{provisionTriggerSettings.getPacketThreshold(), " ", provisionTriggerSettings.getPacketThresholdUnit().name()}));
			data.put("PROV_MT_SMS_THRESHOLD", String.valueOf(provisionTriggerSettings.getMtSmsThreshold()));
			data.put("PROV_MO_SMS_THRESHOLD", String.valueOf(provisionTriggerSettings.getMoSmsThreshold()));
			data.put("PROV_MT_VOICE_THRESHOLD", String.valueOf(provisionTriggerSettings.getMtVoiceMinsThreshold()));
			data.put("PROV_MO_VOICE_THRESHOLD", String.valueOf(provisionTriggerSettings.getMoVoiceMinsThreshold()));
			data.put("PROV_TRAFFIC_DAYS", String.valueOf(provisionTriggerSettings.getTrafficDays()));
			
			data.put("PROV_PACKET_COST", String.valueOf(provisionTriggerSettings.getPerKBPacketPrice()));
			data.put("PROV_MT_SMS_COST", String.valueOf(provisionTriggerSettings.getPerMTSmsPrice()));
			data.put("PROV_MO_SMS_COST", String.valueOf(provisionTriggerSettings.getPerMOSmsPrice()));
			data.put("PROV_MT_VOICE_COST", String.valueOf(provisionTriggerSettings.getPerMinMTVoicePrice()));
			data.put("PROV_MO_VOICE_COST", String.valueOf(provisionTriggerSettings.getPerMinMOVoicePrice()));
		}		
	}

	private void setIncludedPolicyInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting included policy info in rate plan template");
		
		data.put("ROLL_OVER_INCLUDED_USAGE", ratePlan.isExpireIncluded() ? "ROLLOVER" : "NO ROLLOVER");
		data.put("INCLUDED_PERIOD_MONTHS", String.valueOf(ratePlan.getIncludedPeriodMonths()));
	}

	private void setRatePlanFeesInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting rate plan fees info in rate plan template");
		
		RatePlanFeeDetails feeDetails = ratePlan.getRatePlanFeeDetails();
		
		if(feeDetails != null)
		{
			data.put("ACCESS_FEE", String.valueOf(feeDetails.getAccessFee()));
			data.put("ACCESS_FEE_DUR_MONTHS", String.valueOf(ratePlan.getAccessFeeMonths()));
			data.put("ACTIVATION_FEE", String.valueOf(feeDetails.getActivationFee()));
			data.put("SUSPEND_FEE", String.valueOf(feeDetails.getSuspendFee()));
			data.put("UNSUSPEND_FEE", String.valueOf(feeDetails.getUnsuspendFee()));
			data.put("DEACTIVATION_FEE", String.valueOf(feeDetails.getDeactivationFee()));
			data.put("REACTIVATION_FEE", String.valueOf(feeDetails.getReactivationFee()));
		}
	}

	private void setRatePlanCategoryInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting rate plan category info in rate plan template");

		StringBuffer periodType = new StringBuffer(StringUtils.replace(ratePlan.getPeriodType().name(), "_", " "));
		
		if(ratePlan.getPeriodType() == RatePlanPeriodType.VARIABLE)
		{
			periodType.append(" (")
			.append(ratePlan.getAccessFeeMonths())
			.append(" ")
			.append("MONTHS")
			.append(")");
		}
		
		data.put("RATE_PLAN_PERIOD", periodType.toString());  
		data.put("RATE_PLAN_PAYMENT_TYPE", StringUtils.replace(ratePlan.getPaymentType().name(), "_", " "));
		
		StringBuffer accessType = new StringBuffer(StringUtils.replace(ratePlan.getAccessType().name(), "_", " "));
		
		if(ratePlan.getAccessType() == RatePlanAccessType.EXCLUSIVE)
		{
			List<Account> accountList = accountDAO.getSelectedAccounts(String.valueOf(ratePlan.getOperatorId()), String.valueOf(ratePlan.getAccountId()));
			
			if(accountList != null && accountList.size() > 0)
			{
				Account account = accountList.get(0);
				
				accessType.append(" (")
				.append(account.getAccountName())
				.append("[")
				.append(ratePlan.getAccountId())
				.append("]")
				.append(")");
			}
		}		
		
		data.put("RATE_PLAN_ACCESS_TYPE", accessType.toString());
		data.put("RATE_PLAN_MODEL_TYPE", StringUtils.replace(ratePlan.getRatePlanType().name(), "_", " "));
	}

	private void setRatePlanBasicInfo(RatePlan ratePlan, TemplateData data) {
		LOG.info("Setting basic info in rate plan template");
		
		setRatePlanName(ratePlan, data);
		
		setRatePlanLabel(ratePlan, data);
					
		Product product = productCache.getProductById(ratePlan.getProductId());
		data.put("PRODUCT", product.getProductName());
		
		try {
			Zone homeZone = zoneDAO.getZone(ratePlan.getOperatorId(),ratePlan.getZoneSetId(), ratePlan.getHomeZoneId());
			data.put("HOME_ZONE", homeZone.getZoneName());			
		} catch (ZoneNotFoundException e) {
			LOG.warn("Home Zone id: "+ratePlan.getHomeZoneId()+" not found");
		} catch (ZoneException e) {
			LOG.warn("Fetching Home Zone failed");
		}
		
		if(ratePlan.getDevicePoolingPolicyId() > 0)
		{
			PoolingPolicy poolingPolicy =  poolingPolicyCache.getPoolingPolicyById(ratePlan.getDevicePoolingPolicyId());
			data.put("DEVICE_POOLING_POLICY", poolingPolicy.getPolicyName());			
		}
		
		if(ratePlan.getPacketDataRoundingPolicyId() > 0)
		{
			PacketRoundingPolicy packetRoundingPolicy = packetRoundingPolicyCache.getPacketRoundingPolicyById(ratePlan.getPacketDataRoundingPolicyId());
			data.put("PACKET_ROUNDING_POLICY", packetRoundingPolicy.getPolicyName());
		}
		
		data.put("START_DATE", ratePlan.getStartDate().toString());
		data.put("END_DATE", ratePlan.getEndDate().toString());
		
		try {
			List<CarrierRatePlan> carrierRatePlans = ratePlanDAO.getCarrierRatePlans(ratePlan.getProductId());
			
			for (CarrierRatePlan carrierRatePlan : carrierRatePlans) {
				if(ratePlan.getCarrierRatePlanId() == carrierRatePlan.getRatePlanId())
				{
					data.put("CARRIER_RATE_PLAN", carrierRatePlan.getRatePlanName());
					break;
				}
			}
		} catch (RatePlanException e) {
			LOG.warn("Fetching carrier rate plan failed");
		}
		
		Currency currency = currencyCache.getCurrencyByCode(ratePlan.getCurrencyName());
		
		if(currency != null)
		{
			StringBuffer sb = new StringBuffer(currency.getCurrencyName());
			
			sb.append(" ")
			.append("(")
			.append(currency.getCurrencyCode())
			.append(")");

			data.put("CURRENCY_CODE", sb.toString());			
		}
		
		RatePlanStatus status = ratePlan.getStatus();

		data.put("STATUS", status != null ? status.name() : RatePlanStatus.PENDING.name());
		
		int zoneSetId = ratePlan.getZoneSetId();
		String zoneSetName = null;
		if(zoneSetId > 0){
			ZoneSet zoneSet = null;
			try {
				zoneSet = zoneSetDAO.getZoneSet(ratePlan.getOperatorId(), zoneSetId);
			} catch (ZoneSetException e) {
				LOG.warn("Fetching ZoneSetName failed for RatePlan "+ratePlan.getRatePlanName());
			} catch (ZoneSetNotFoundException e) {
				LOG.warn("Fetching ZoneSetName failed for RatePlan "+ratePlan.getRatePlanName());
			}
			
			if(zoneSet != null){
				zoneSetName = zoneSet.getZoneSetName();
			}
		}
		//Set Zone Set Name
		data.put("ZSET_NAME", zoneSetName != null ? zoneSetName : "");
	}

	private void setRatePlanLabel(RatePlan ratePlan, TemplateData data) {
		if(StringUtils.length(ratePlan.getRatePlanLabel()) > data.getDefaultItemWidth())
		{
			data.put("RATE_PLAN_LABEL", StringUtils.substring(ratePlan.getRatePlanLabel(), 0, data.getDefaultItemWidth()));
			data.put("RATE_PLAN_LABEL_2", StringUtils.substring(ratePlan.getRatePlanLabel(), data.getDefaultItemWidth()), "");
		}
		else
		{
			data.put("RATE_PLAN_LABEL", ratePlan.getRatePlanLabel());
			data.put("RATE_PLAN_LABEL_2", "");
		}
	}

	private void setRatePlanName(RatePlan ratePlan, TemplateData data) {
		if(StringUtils.length(ratePlan.getRatePlanName()) > data.getDefaultItemWidth())
		{
			data.put("RATE_PLAN_NAME", StringUtils.substring(ratePlan.getRatePlanName(), 0, data.getDefaultItemWidth()));
			data.put("RATE_PLAN_NAME_2", StringUtils.substring(ratePlan.getRatePlanName(), data.getDefaultItemWidth()), "");
		}
		else
		{
			data.put("RATE_PLAN_NAME", ratePlan.getRatePlanName());
			data.put("RATE_PLAN_NAME_2", "");
		}
	}
	
	private void setFileName(String fileName, TemplateData data) {
		data.put("FILE_NAME", fileName);
	}
}
