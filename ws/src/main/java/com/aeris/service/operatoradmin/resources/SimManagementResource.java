package com.aeris.service.operatoradmin.resources;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IDistributorDAO;
import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.DistributorException;
import com.aeris.service.operatoradmin.exception.SIMPackException;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.AssignSIMsFromInventoryRequest;
import com.aeris.service.operatoradmin.payload.AssignSIMsFromSIMPACKRequest;
import com.aeris.service.operatoradmin.payload.CreateSimpackRequest;
import com.aeris.service.operatoradmin.payload.Simpack;

/**
 * Rest Resource for Distributor sim management
 * 
 * @author saurabh.sharma@aeris.net
 */
@Path("/operators/{operatorId}")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class SimManagementResource {
	private static final Logger LOG = LoggerFactory.getLogger(SimManagementResource.class);

	@Inject
	private IDistributorDAO distributorDAO;
	private ObjectMapper mapper = new ObjectMapper();
	
	
	@GET
	@Path("/simpacks")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response getOperatorSimpacks(@PathParam("operatorId") @ValidOperator final String operatorId, @QueryParam("used") String used) {

		LOG.info("Received request to create simpacks under operator:{} ", operatorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			List<Simpack> simpacks = distributorDAO.getSimpacks(Long.valueOf(operatorId), -1, used);
			LOG.debug("Successfully created simpacks under operator:{}, simpacks {}", operatorId, mapper.writeValueAsString(simpacks));
			String simpackJson = mapper.writeValueAsString(simpacks);
			builder.entity(simpackJson);
			LOG.info("GET unique-codes Response: " + simpackJson);
		} catch (Exception e) {
			LOG.error("CreateNewDistributor Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewDistributor: " + response);
		return response;
	}
	
	
	@GET
	@Path("/distributors/{distributorId}/simpacks")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response getDistributorSimpacks(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId, @QueryParam("used") String used) {

		LOG.info("Received request to get all simpacks for distributor: " + distributorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			List<Simpack> simpacks = distributorDAO.getSimpacks(Long.valueOf(operatorId), Long.valueOf(distributorId), used);
			String simpack = mapper.writeValueAsString(simpacks);
			LOG.info("GET unique-codes Response: " + simpack);
			return builder.entity(simpack).build();
		} catch (Exception e) {
			LOG.error("GetDistributorSimpacks Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
	}
	
	
	@GET
	@Path("/simpacks/{simpackId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response getSimpack(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("simpackId") final String simpackId) {

		LOG.info("Received request to get all simpacks for with simpackId: " + simpackId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			Simpack simpack = distributorDAO.getSimPackByUniqueId(simpackId);
			String distributorJson = mapper.writeValueAsString(simpack);
			LOG.info("GET unique-codes Response: " + distributorJson);
			return builder.entity(distributorJson).build();
		} catch (DistributorException e) {
			LOG.error("Simpack not found with simpackId {}", simpackId, e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("GetSimpack Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
	}
	
	
    /**
     * Create SIMPACK for operator API. Purpose is to replace tool that Finance use to create SIMPACK for Neo usecase. 
     * @param operatorId
     * @param request
     * @return List of created SIM packs.
     */
    @POST
    @Path("/simpacks")
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
    public Response createSIMPacksForOperator(@PathParam("operatorId") @ValidOperator final String operatorId,
            @Valid CreateSimpackRequest request) {
        LOG.info("Received request to create simpacks for operator {}", operatorId);
        ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            List<Simpack> distributorCode = distributorDAO.createSIMPacksForOperator(Long.valueOf(operatorId), request);
            LOG.info("Successfully created simpacks for operatorId {}", operatorId);
            String distributorJson = mapper.writeValueAsString(distributorCode);
            builder.entity(distributorJson);
            LOG.debug("Create Operator simpack Response: {}", distributorJson);
        } catch (SIMPackException e) {
            LOG.error("Inconsistent simpack request", e);
            return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
        } catch (Exception e) {
            LOG.error("createSIMPacksForOperator Exception occured :", e);
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
        }
        Response response = builder.build();
        LOG.debug("Sent response from createSIMPacksForOperator: " + response);
        return response;
    }
    
	/**
     * Assign SIM to an account. This will be used to block and assign SIMs for distributor.
     * @param operatorId
     * @param accountId
     * @param request
     * @return 
     */
    @POST
	@Path("/accounts/{accountId}/sims")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response assignSIMsToAccount(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String accountId,
			@Valid AssignSIMsFromInventoryRequest request) {
		LOG.info("Received request to assign sims to account : " + accountId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			LOG.debug("Calling DistributorDAO.assignSIMsToAccount");
			boolean success = distributorDAO.assignSIMsToAccount(Long.valueOf(operatorId), accountId, request);
			String assignSIMsToAccountJson = mapper.writeValueAsString(success);
			builder.entity(assignSIMsToAccountJson);
			LOG.info("Create assignSimsToAccount Response: " + assignSIMsToAccountJson);
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("AssignSIMsToAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
		Response response = builder.build();
		LOG.debug("Sent response from assignSIMsToAccount: " + response);
		return response;
	}
	

    /**
     * Assign SIMs in SIMPACK to an account.
     * @param operatorId
     * @param accountId
     * @param request
     * @return 
     */
    @PUT
	@Path("/accounts/{accountId}/sims")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response assignSIMsFromSIMPACKToAccount(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String accountId,
			@Valid AssignSIMsFromSIMPACKRequest request) {
		LOG.info("Received request to assign sims to account : " + accountId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			boolean success = distributorDAO.assignSIMsFromSIMPACKToAccount(Long.valueOf(operatorId), accountId, request);
			String assignSIMsToAccountJson = mapper.writeValueAsString(success);
			builder.entity(assignSIMsToAccountJson);
			LOG.debug("Sent response from assignSIMsFromSIMPACKToAccount: " + assignSIMsToAccountJson);
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (DistributorException e) {
			LOG.error("DistributorException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("AssignSIMsFromSIMPACKToAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
		Response response = builder.build();
		LOG.debug("Sent response from assignSIMsFromSIMPACKToAccount: " + response);
		return response;
	}

}
