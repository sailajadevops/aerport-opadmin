package com.aeris.service.operatoradmin;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public class ApplicationVersion {

    private String buildPlan;
    private String buildNumber;
    private String appVersion;

    @Inject
    public ApplicationVersion(@Named("buildPlan") String buildPlan,
            @Named("buildNumber") String buildNumber,
            @Named("appVersion") String appVersion) {
        this.buildNumber = buildNumber;
        this.buildPlan = buildPlan;
        this.appVersion = appVersion;
    }

    public String getVersion() {
        StringBuilder sb = new StringBuilder("[Version =");
        sb.append(appVersion).append(", buildPlan=").append(buildPlan).append(", buildNumber=").append(buildNumber).append("]");
        return sb.toString();
    }
}
