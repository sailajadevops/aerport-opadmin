package com.aeris.service.operatoradmin;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.auth.RolesAllowedValidator;
import com.aeris.service.operatoradmin.filter.OperatorAdminFilter;

public class OperatorAdminApplication extends ResourceConfig {
	
	private static Logger log = LoggerFactory.getLogger(OperatorAdminApplication.class);

	@Inject
	public OperatorAdminApplication(ServiceLocator serviceLocator) {
		// Send Error to Client
		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
		
		Map<String, MediaType> mediaTypes = new HashMap<String, MediaType>();        
		mediaTypes.put("json", MediaType.valueOf("application/json"));      
		mediaTypes.put("txt", MediaType.valueOf("text/plain"));
		
		property(ServerProperties.MEDIA_TYPE_MAPPINGS, mediaTypes);
		
		// Scan for resources
		packages("com.aeris.service.aeraccountmgmt.resource", "com.aeris.service.simmgmt.resource", "com.aeris.service.zonemgmt.resource", "com.aeris.service.operatoradmin");
		register(JacksonConfig.class);
		register(JacksonFeature.class);
		//register(SecurityInterceptor.class);
		// Enable below line if need operatorApiKey Authentication
		register(OperatorAdminFilter.class);
		register(OperatorAdminResponseFilter.class);
		register(RolesAllowedValidator.class);
        
		// Load Guice Modules from listener
		log.info("Registering injectables..."); 

		GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);

		GuiceIntoHK2Bridge guiceBridge = serviceLocator.getService(GuiceIntoHK2Bridge.class);

		guiceBridge.bridgeGuiceInjector(OperatorAdminListener.sInjector);
		
		log.info("Guice Initialized.");
	}
}
