package com.aeris.service.operatoradmin.enums.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.IOperatorConfigDAO;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.IOperatorEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Operator;
import com.google.inject.Singleton;

/**
 * Operator Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class OperatorEnum implements IOperatorEnum, IEventListener{
	private Logger LOG = LoggerFactory.getLogger(OperatorEnum.class);
	
	private Cache<String, Operator> cache;
	private Cache<String, Operator> cacheById;
	private Cache<String, Operator> operatorKeyCache;
	
	@Inject
	private IStoredProcedure<List<Operator>> getOperatorsStoredProcedure;
	
	@Inject IOperatorConfigDAO operatorConfigDAO ;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "OperatorCache/name") Cache<String, Operator> cache, 
					@Hazelcast(cache = "OperatorCache/id") Cache<String, Operator> cacheById,
					@Hazelcast(cache = "OperatorCache/key") Cache<String, Operator> operatorKeyCache) {
		this.cache = cache;
		this.cacheById = cacheById;
		this.operatorKeyCache = operatorKeyCache ;
		loadResources();
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		try {
			// Call Get_Product Stored Procedure
			List<Operator> operators = getOperatorsStoredProcedure.execute();	
			Map<Long, String> operatorKeyMap = operatorConfigDAO.getOperatorKeys();
			for(Operator operator: operators) 
			{
				cache.put(operator.getOperatorName(), operator);
				cacheById.put(String.valueOf(operator.getOperatorId()), operator);
				if(operatorKeyMap.get(operator.getOperatorId()) != null) {
					operatorKeyCache.put(operatorKeyMap.get(operator.getOperatorId()), operator);
				}
			}
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during getOperatorsStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		}
	}
	
	@Override
	public Operator getOperatorByName(String name) {
		return cache.get(name);
	}

	@Override
	public Operator getOperatorById(int id) {
		return cacheById.get(String.valueOf(id)); 
	}
	
	@Override
	public Operator getOperatorByKey(String operatorKey) {
		return operatorKeyCache.get(operatorKey); 
	}

	@Override
	public List<Operator> getAllOperators() {
		return new ArrayList<Operator>(cacheById.getValues());
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
