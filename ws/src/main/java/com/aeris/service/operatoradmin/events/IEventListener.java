package com.aeris.service.operatoradmin.events;

public interface IEventListener {
	void onEvent(Event eventName, Object... params);
}
