package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Product;
import com.google.inject.Inject;

public class GetProductsStoredProcedure extends AbstractProcedureCall<List<Product>> {

	private static final String SQL_GET_PRODUCTS = "common_util_pkg.get_product";

	@Inject
	public GetProductsStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_PRODUCTS, true);
		
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Product> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<Product> products = createProductList(cursor);

		return products;
	}

	private List<Product> createProductList(List<Map<String, Object>> cursor) {
		List<Product> products = new ArrayList<Product>();

		for (Map<String, Object> row : cursor) {
			Product product = new Product();

			String productId = (String) row.get("product_id");

			if (NumberUtils.isNumber(productId)) {
				product.setProductId(Integer.parseInt(productId));
			}

			String name = (String) row.get("product_name");

			if (StringUtils.isNotEmpty(name)) {
				product.setProductName(name);
			}

			String technology = (String) row.get("technology");

			if (StringUtils.isNotEmpty(technology)) {
				product.setTechnology(technology);
			}

			String carrierId = (String) row.get("carrier_id");

			if (NumberUtils.isNumber(carrierId)) {
				product.setCarrierId(Integer.parseInt(carrierId));
			}
			
			String msisdnRange = (String) row.get("msisdn_range");

			if (StringUtils.isNotEmpty(msisdnRange)) {
				product.setMsisdnRange(msisdnRange);
			}
			
			String staticIpRange = (String) row.get("static_ip_range");

			if (StringUtils.isNotEmpty(staticIpRange)) {
				product.setStaticIpRange(staticIpRange);
			}

			products.add(product);
		}

		return products;
	}
}
