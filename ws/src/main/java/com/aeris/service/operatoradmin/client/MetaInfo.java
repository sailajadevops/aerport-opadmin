package com.aeris.service.operatoradmin.client;

import java.io.Serializable;

public class MetaInfo implements Serializable{
    private static final long serialVersionUID = 1L;
	private String uom;

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

    
}