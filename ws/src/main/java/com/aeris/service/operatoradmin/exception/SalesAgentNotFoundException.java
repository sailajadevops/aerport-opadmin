package com.aeris.service.operatoradmin.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class SalesAgentNotFoundException extends WebApplicationException  {
	private static final long serialVersionUID = -7699947374174087324L;

	public SalesAgentNotFoundException(String s) {
		super(Response.status(Response.Status.NOT_FOUND)
	             .entity(s).build());
	}
}
