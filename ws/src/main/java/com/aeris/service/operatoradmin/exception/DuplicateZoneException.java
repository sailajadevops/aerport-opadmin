package com.aeris.service.operatoradmin.exception;

public class DuplicateZoneException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DuplicateZoneException(String s) {
		super(s);
	}

	public DuplicateZoneException(String ex, Throwable t) {
		super(ex, t);
	}
}
