package com.aeris.service.operatoradmin.client;

import java.io.Serializable;

public class CreateAerCloudAccountRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String accountId;
	private String accessRightID;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccessRightID() {
		return accessRightID;
	}

	public void setAccessRightID(String accessRightID) {
		this.accessRightID = accessRightID;
	}
}
