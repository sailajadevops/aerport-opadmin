package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Map;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class SIMSummary<T> implements Serializable {
	private static final long serialVersionUID = 8612720853976610314L;
	private Map<T, Long> summary;

	public void addSummaryItem(T item, long summaryCount) {
		summary.put(item, summaryCount);
	}

	public Map<T, Long> getSummary() {
		return summary;
	}
}
