package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.SIMWarehouse;

public interface ISIMWarehouseEnum  extends IBaseEnum{
	SIMWarehouse getSIMWarehouseByName(String name);
	SIMWarehouse getSIMWarehouseById(int id);
	List<SIMWarehouse> getAllSIMWarehouses();
}
