package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum SubscriptionStatus implements Serializable {
	ENABLED(1, "Enabled"), DISABLED(0, "Disabled");

	int code;
	String value;

	SubscriptionStatus(int code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public int getCode() {
		return code;
	}

	public static boolean isValid(String value) {
		SubscriptionStatus[] statuses = values();

		for (SubscriptionStatus subscriptionStatus : statuses) {
			if (subscriptionStatus.getValue().equalsIgnoreCase(value)) {
				return true;
			}
		}

		return false;
	}

	public static SubscriptionStatus fromValue(String value) {
		SubscriptionStatus[] statuses = values();

		for (SubscriptionStatus subscriptionStatus : statuses) {
			if (subscriptionStatus.getValue().equalsIgnoreCase(value)) {
				return subscriptionStatus;
			}
		}

		return null;
	}
}
