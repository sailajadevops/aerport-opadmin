package com.aeris.service.operatoradmin.exception;

public class AccountRatePlanMappingException extends Exception{
	private static final long serialVersionUID = 3994535142860286763L;

	public AccountRatePlanMappingException(String s) {
		super(s);
	}

	public AccountRatePlanMappingException(String ex, Throwable t) {
		super(ex, t);
	}
}
