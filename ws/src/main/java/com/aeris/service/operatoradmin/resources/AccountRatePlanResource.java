package com.aeris.service.operatoradmin.resources;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IRatePlanDAO;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanException;
import com.aeris.service.operatoradmin.exception.RatePlanException;
import com.aeris.service.operatoradmin.exception.RatePlanNotFoundException;
import com.aeris.service.operatoradmin.model.AccountRatePlan;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.AssignRatePlanRequest;

/**
 * Rest Resource for Rate Plan management. Includes methods for
 * 
 * <ul>
 * <li>
 * Create a new Rate Plan
 * </li>
 * <li>
 * Get Rate Plans assigned to an account
 * </li>
 * <li>
 * Assign Rate Plan to an account
 * </li>
 * <li>
 * Expire Rate Plan assigned to an account
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}/accounts")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class AccountRatePlanResource {
	private static final Logger LOG = LoggerFactory.getLogger(AccountRatePlanResource.class);

	@Inject
	private IRatePlanDAO ratePlanDAO;
	
	/**
	 * Fetch a specific account identified by account id
	 * 
	 * @param operatorId the operator id
	 * @param accountId the account id
	 * 
	 * @return The Account details
	 */
	@GET
	@Path("/{accountId}/rateplans/{ratePlanId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "ACCOUNT", "OPERATOR"})
	public Response getAccountRatePlan(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("ratePlanId") @Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") final String ratePlanId) {
		LOG.info("Received request to fetch account rate plan for rate plan id: " + ratePlanId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			// Fetch from database
			AccountRatePlan accountRatePlan = ratePlanDAO.getAccountRatePlan(Integer.parseInt(operatorId), Integer.parseInt(accountId), Integer.parseInt(ratePlanId));

			if (accountRatePlan != null) {
				LOG.info("fetched account rate plan successfully for ratePlanId: " + ratePlanId);

				ObjectMapper mapper = new ObjectMapper();
				String accountRatePlanJson = mapper.writeValueAsString(accountRatePlan);

				builder.entity(accountRatePlanJson);
			} else {
				LOG.info("No account rate plan found for ratePlanId: " + ratePlanId);
				return Response.status(Status.NOT_FOUND).entity("No account rate plan found for ratePlanId: " + ratePlanId).build();
			}
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (RatePlanException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getAccountRatePlan Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting account rate plan").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAccountRatePlan: " + response);
		return response;
	}
	
	/**
	 * <p>
	 * Fetches List of Account details per operator.
	 * Allows to filter the Accounts based of status or list of specific account ids.
	 * </p>
	 * 
	 * @param operatorId The Operator Id
	 * @param status The Status
	 * @param start The start index for pagination
	 * @param count The record count for pagination
	 * @param accountIds The list of specific account ids to be fetched 
	 * @param extendAttributes true if the api sends extended account information to the client
	 * 
	 * @return The List of Accounts
	 */
	@GET
	@Path("/{accountId}/rateplans")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "ACCOUNT", "OPERATOR"})
	public Response getAccountRatePlans(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@QueryParam("status") @DefaultValue("all") final String status, @QueryParam("start") final String start,
			@QueryParam("count") final String count,
			@QueryParam("extendAttributes") final boolean extendAttributes) {
		LOG.info("Received request to fetch all rate plans for operatorId: " + operatorId+" assigned to account "+accountId);   
	
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
	
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);
	
		List<AccountRatePlan> ratePlans = null;
	
		try {
			if (paginated) {
				LOG.info("Returning a max record size of " + count + " all assigned rate plans starting from " + start + " for accountId: " + accountId);
			} else {
				LOG.info("Query is not paginated, returning all assigned rate plans for accountId: " + accountId);
			}
	
			// Fetch orders with any status
			if (!RatePlanStatus.isValid(status)) {
				// Fetch from database
				ratePlans = ratePlanDAO.getAccountRatePlans(Integer.parseInt(operatorId), Integer.parseInt(accountId), start, count);
			}
			// Fetch orders with specific status like Open, In Progress,
			// Completed etc.
			else {
				// Fetch from database
				ratePlans = ratePlanDAO.getAccountRatePlans(Integer.parseInt(operatorId), Integer.parseInt(accountId), start, count, RatePlanStatus.fromName(status));
			}
	
			if (ratePlans != null) {
				LOG.info("fetched rate plans successfully, no of assigned rate plans retreived : " + ratePlans.size()); 
	
				ObjectMapper mapper = new ObjectMapper();
				String ratePlansJson = mapper.writeValueAsString(ratePlans);
	
				builder.entity(ratePlansJson);
			} else {
				LOG.info("No rate plans for account : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No rate plans found for the account").build();
			}
		} catch (RatePlanException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getAssignedRatePlans Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting assigned rate plans").build();
		}
	
		Response response = builder.build();
	
		LOG.debug("Sent response from getAssignedRatePlans: " + response);
		return response;
	}
	
	/**
	 * Delete an existing account identified by account id and request authorized user
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param userId The user id of the user who has sent the request
	 * 
	 * @return true if the delete is success otherwise false
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{accountId}/rateplans/{ratePlanId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response assignRatePlanToAccount(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("ratePlanId") @Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") final String ratePlanId,
			@Valid AssignRatePlanRequest request) {
		LOG.info("Received request to assign rate plan: " + ratePlanId + " to account: " + accountId);
	
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Date requestedDate = new Date();
	
		String description = request.getDescription();
		Date startDate = request.getStartDate();
		Date endDate = request.getEndDate();
		int carrierRateplanId = request.getCarrierRatePlanId();
		String userId = request.getUserId();
		if (StringUtils.isBlank(userId)) {
			return Response.status(Status.BAD_REQUEST).entity("userId must be provided").build();
		}
		
		try {
			// Fetch from database
			boolean success = ratePlanDAO.assignRatePlanToAccount(Integer.parseInt(operatorId), Integer.parseInt(accountId),
					Integer.parseInt(ratePlanId), carrierRateplanId, startDate, endDate, description, requestedDate, userId);
	
			ObjectMapper mapper = new ObjectMapper();
	
			String ratePlanJson = mapper.writeValueAsString(success);
			builder.entity(ratePlanJson);
	
			LOG.info("Assign Rate Plan Response: " + ratePlanJson);
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (DuplicateRatePlanException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (RatePlanException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("assignRatePlan Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while assigning rate plan").build();
		}
	
		Response response = builder.build();
	
		LOG.debug("Sent response from assignRatePlan: " + response);
		return response;
	}

	/**
	 * Delete an existing account identified by account id and request authorized user
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param userId The user id of the user who has sent the request
	 * 
	 * @return true if the delete is success otherwise false
	 */
	@DELETE
	@Path("/{accountId}/rateplans/{ratePlanId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response invalidateAccountRatePlan(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("ratePlanId") @Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") final String ratePlanId,
			@QueryParam("userId") @Email(message = "{userId.email}") final String userId) {
		LOG.info("Received request to invalidate rate plan with ratePlanId: " + ratePlanId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Date requestedDate = new Date();
		if (StringUtils.isBlank(userId)) {
			return Response.status(Status.BAD_REQUEST).entity("userId must be provided").build();
		}

		try {
			// Fetch from database
			boolean success = ratePlanDAO.invalidateAccountRatePlan(Integer.parseInt(operatorId), Integer.parseInt(accountId), Integer.parseInt(ratePlanId), 
					requestedDate, userId);

			if (!success) {
				LOG.info("invalidateAccountRatePlan failed for rate plan id: " + ratePlanId);
				return Response.status(Status.NOT_FOUND).entity("invalidateAccountRatePlan failed for rate plan id: " + ratePlanId).build();
			} else {
				LOG.info("Rate plan with rate plan id: " + ratePlanId + " invalidated successfully for account: "+accountId);

				ObjectMapper mapper = new ObjectMapper();
				String invalidateJson = mapper.writeValueAsString(success);

				builder.entity(invalidateJson);
			}
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("invalidateAccountRatePlan Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while invalidating account rate plan").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from deleteRatePlan: " + response);
		return response;
	}
	
	/**
	 * Delete an existing account identified by account id and request authorized user
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param userId The user id of the user who has sent the request
	 * 
	 * @return true if the delete is success otherwise false
	 */
	@GET
	@Path("/{accountId}/unpooledrateplans")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "ACCOUNT", "OPERATOR"})
	public Response getUnpooledAccountRatePlans(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@QueryParam("productId") @Pattern(regexp = "[0-9]+", message = "{productId.number}") final String productId,
			@QueryParam("filter") @Pattern(regexp = "[0-9]+", message = "{filter.number}") final String filterRatePlanPoolId) {
		LOG.info("Received request to fetch unpooled account rate plans for account: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			// Fetch from database
			List<String> unpooledAccountRatePlans = ratePlanDAO.getUnpooledAccountRatePlans(Integer.parseInt(operatorId), Integer.parseInt(accountId), NumberUtils.toInt(filterRatePlanPoolId, 0), NumberUtils.toInt(productId, 0));

			if (unpooledAccountRatePlans == null) {
				LOG.info("getUnpooledAccountRatePlans failed for accountId: " + accountId);
				return Response.status(Status.NOT_FOUND).entity("getUnpooledAccountRatePlans failed for accountId: " + accountId).build();
			} else {
				LOG.info("Fetched unpooled account rate plans for account: " + accountId + " successfully");

				ObjectMapper mapper = new ObjectMapper();
				String unpooledAccountRatePlansJson = mapper.writeValueAsString(unpooledAccountRatePlans);

				builder.entity(unpooledAccountRatePlansJson);
			}
		}catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getUnpooledAccountRatePlans Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while invalidating account rate plan").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getUnpooledAccountRatePlans: " + response);
		return response;
	}
}
