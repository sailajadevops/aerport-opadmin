package com.aeris.service.operatoradmin.exception;

public class RESException extends Exception {

	private static final long serialVersionUID = 1L;

	public RESException(String s){
		super(s);
	}
	public RESException(String ex, Throwable t) {
		super(ex, t);
	}
}
