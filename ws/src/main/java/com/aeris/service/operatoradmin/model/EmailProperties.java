package com.aeris.service.operatoradmin.model;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public class EmailProperties {
	private static Logger LOG = LoggerFactory.getLogger(EmailProperties.class);
	
	private String mailSmtpHost;
	private String mailSmtpPort;
	private String mailSmtpAuth;
	private String mailSmtpStarttlsEnable;
	private String mailUser;
	private String mailPassword;
	private String mailSmtpSendpartial;
	private String mailDebug;
	private String senderEmail;
	private String ratePlanAdminEmail;
	
	@Inject
	public EmailProperties(@Named("mail.smtp.host") String mailSmtpHost, 
			@Named("mail.smtp.port") String mailSmtpPort,
			@Named("mail.smtp.auth") String mailSmtpAuth,
			@Named("mail.smtp.starttls.enable") String mailSmtpStarttlsEnable,
			@Named("mail.user") String mailUser,
			@Named("mail.password") String mailPassword,
			@Named("mail.smtp.sendpartial") String mailSmtpSendpartial,
			@Named("mail.debug") String mailDebug,
			@Named("sender.email") String senderEmail,
			@Named("rateplan.admin.email") String ratePlanAdminEmail) {
		this.mailSmtpHost = mailSmtpHost;
		this.mailSmtpPort = mailSmtpPort;
		this.mailSmtpAuth = mailSmtpAuth;
		this.mailSmtpStarttlsEnable = mailSmtpStarttlsEnable;
		this.mailUser = mailUser;
		this.mailPassword = mailPassword;
		this.mailSmtpSendpartial = mailSmtpSendpartial;
		this.mailDebug = mailDebug;
		this.senderEmail = senderEmail;
		this.ratePlanAdminEmail = ratePlanAdminEmail;
		
		LOG.info("Email properties: "+ReflectionToStringBuilder.toString(this));
	}

	public String getMailSmtpHost() {
		return mailSmtpHost;
	}

	public void setMailSmtpHost(String mailSmtpHost) {
		this.mailSmtpHost = mailSmtpHost;
	}

	public String getMailSmtpPort() {
		return mailSmtpPort;
	}

	public void setMailSmtpPort(String mailSmtpPort) {
		this.mailSmtpPort = mailSmtpPort;
	}

	public String getMailSmtpAuth() {
		return mailSmtpAuth;
	}

	public void setMailSmtpAuth(String mailSmtpAuth) {
		this.mailSmtpAuth = mailSmtpAuth;
	}

	public String getMailSmtpStarttlsEnable() {
		return mailSmtpStarttlsEnable;
	}

	public void setMailSmtpStarttlsEnable(String mailSmtpStarttlsEnable) {
		this.mailSmtpStarttlsEnable = mailSmtpStarttlsEnable;
	}

	public String getMailUser() {
		return mailUser;
	}

	public void setMailUser(String mailUser) {
		this.mailUser = mailUser;
	}

	public String getMailPassword() {
		return mailPassword;
	}

	public void setMailPassword(String mailPassword) {
		this.mailPassword = mailPassword;
	}

	public String getMailSmtpSendpartial() {
		return mailSmtpSendpartial;
	}

	public void setMailSmtpSendpartial(String mailSmtpSendpartial) {
		this.mailSmtpSendpartial = mailSmtpSendpartial;
	}

	public String getMailDebug() {
		return mailDebug;
	}

	public void setMailDebug(String mailDebug) {
		this.mailDebug = mailDebug;
	}
	
	public String getSenderEmail() {
		return senderEmail;
	}
	
	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}
	
	public String getRatePlanAdminEmail() {
		return ratePlanAdminEmail;
	}
	
	public void setRatePlanAdminEmail(String ratePlanAdminEmail) {
		this.ratePlanAdminEmail = ratePlanAdminEmail;
	}	
}
