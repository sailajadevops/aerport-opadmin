package com.aeris.service.operatoradmin.jmx;

public interface CacheManagerMBean {
	void refreshAll();
}
