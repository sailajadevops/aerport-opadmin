package com.aeris.service.operatoradmin.payload;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.IMSIRangeType;
import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@XmlRootElement
public class UpdateSupplierSIMOrderRequest {
	@NotNull(message = "{quantity.notnull}")
	@Min(value = 1, message = "{quantity.minimum}")
	@Max(value = 10000, message = "{quantity.maximum}")
	private long quantity;

	@Enumeration(value = OrderStatus.class, message = "{status.enum}")
	private OrderStatus status;

	@NotNull(message = "{iccIdRange.notnull}")
	private String iccIdRange;

	@NotNull(message = "{imsiRange.notnull}")
	private String imsiRange;

	@Enumeration(value=IMSIRangeType.class, message="{imsiRangeType.enum}")
	private IMSIRangeType imsiRangeType;

	private String iccIdStart;

	private String imsiStart;

	@NotNull(message = "{userId.notnull}")
	@Email(message = "{userId.email.pattern}")
	private String userId;

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public String getIccIdRange() {
		return iccIdRange;
	}

	public void setIccIdRange(String iccIdRange) {
		this.iccIdRange = iccIdRange;
	}

	public String getImsiRange() {
		return imsiRange;
	}

	public void setImsiRange(String imsiRange) {
		this.imsiRange = imsiRange;
	}

	public IMSIRangeType getImsiRangeType() {
		return imsiRangeType;
	}

	public void setImsiRangeType(IMSIRangeType imsiRangeType) {
		this.imsiRangeType = imsiRangeType;
	}

	public String getIccIdStart() {
		return iccIdStart;
	}

	public void setIccIdStart(String iccIdStart) {
		this.iccIdStart = iccIdStart;
	}

	public String getImsiStart() {
		return imsiStart;
	}

	public void setImsiStart(String imsiStart) {
		this.imsiStart = imsiStart;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
