package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Vertical;
import com.google.inject.Inject;

public class GetVerticalsStoredProcedure extends AbstractProcedureCall<List<Vertical>> {

	private static final String SQL_GET_VERTICALS = "common_util_pkg.get_ws_vertical";

	@Inject
	public GetVerticalsStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_VERTICALS, true);
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Vertical> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<Vertical> verticals = createVerticalList(cursor);

		return verticals;
	}

	private List<Vertical> createVerticalList(List<Map<String, Object>> cursor) {
		List<Vertical> verticals = new ArrayList<Vertical>();

		for (Map<String, Object> row : cursor) {
			Vertical vertical = new Vertical();

			String verticalId = (String) row.get("vertical_id");

			if (NumberUtils.isNumber(verticalId)) {
				vertical.setVerticalId(Integer.parseInt(verticalId));
			}

			String name = (String) row.get("vertical_name");

			if (StringUtils.isNotEmpty(name)) {
				vertical.setVerticalName(name);
			}
			
			verticals.add(vertical);
		}

		return verticals;
	}
}
