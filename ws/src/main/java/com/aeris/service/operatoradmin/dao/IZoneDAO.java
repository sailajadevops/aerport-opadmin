package com.aeris.service.operatoradmin.dao;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneMappingException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZonePreference;

public interface IZoneDAO {
	List<Zone> getAllZones(int operatorId, int zoneSetId) throws ZoneException;

	List<Zone> getZonesByZoneSet(int operatorId, int zoneSetId) throws ZoneException;
	
	Zone[] getOverageZonesByZoneSet(int operatorId, int zoneSetId, int homeZoneId) throws ZoneNotFoundException, ZoneException;

	Zone getZone(int operatorId, int zoneSetId, int zoneId) throws ZoneNotFoundException, ZoneException;
	
	List<Zone> createZones(Connection conn, List<Zone> zones, int zoneSetId, int operatorId, int productId, Date requestedDate, String requestedUser) throws ZoneException, DuplicateZoneException;
	
	Zone createNewZone(int operatorId, int zoneSetId, int productId, String zoneName, List<String> operators, List<String> geographies, EnrolledServices services, IncludedPlanDetails includedPlan,
			List<ZonePreference> zonePreferences, String technology, Date requestedDate, String requestedUser) throws ZoneException, DuplicateZoneException;

	Zone updateZone(int operatorId, int zoneSetId, int zoneId, int productId, String zoneName, List<String> operators, List<String> geographies, EnrolledServices services,
			IncludedPlanDetails includedPlan, List<ZonePreference> zonePreferences, String technology, Date requestedDate, String requestedUser)
			throws ZoneNotFoundException, ZoneException, DuplicateZoneException, ZoneMappingException;

	boolean deleteZone(int operatorId, int zoneSetId, int zoneId, Date requestedDate, String requestedUser) throws ZoneNotFoundException, ZoneException, ZoneMappingException;

	List<Zone> getAllZones();
	
	public void putInCache(Zone... zones) ;
}
