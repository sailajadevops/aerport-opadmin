package com.aeris.service.operatoradmin.exception;

public class ServiceProfileException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public ServiceProfileException(String s) {
		super(s);
	}

	public ServiceProfileException(String ex, Throwable t) {
		super(ex, t);
	}
}
