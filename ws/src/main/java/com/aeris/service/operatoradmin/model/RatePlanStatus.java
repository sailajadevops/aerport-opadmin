package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum RatePlanStatus implements Serializable {
	PENDING(2), APPROVED(1), EXPIRED(3);
	int value;

	RatePlanStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static RatePlanStatus fromValue(int value) {
		RatePlanStatus[] statuses = values();

		for (RatePlanStatus accountStatus : statuses) {
			if (accountStatus.getValue() == value) {
				return accountStatus;
			}
		}

		return null;
	}

	public static RatePlanStatus fromName(String value) {
		RatePlanStatus[] statuses = values();

		for (RatePlanStatus accountStatus : statuses) {
			if (accountStatus.name().equalsIgnoreCase(value)) {
				return accountStatus;
			}
		}

		return null;
	}
}
