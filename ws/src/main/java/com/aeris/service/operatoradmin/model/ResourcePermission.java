package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class ResourcePermission implements Serializable {
	private static final long serialVersionUID = 6777880994016133408L;

	private Logger LOG = LoggerFactory.getLogger(ResourcePermission.class);

	private String resource;
	private String[] permissions = { PermissionValue.NONE.name() };

	public ResourcePermission() {

	}

	public ResourcePermission(String resource, String permissions) {
		this.resource = Preconditions.checkNotNull(resource);
		setPermissions(permissions);
	}

	public boolean hasPermission(PermissionValue permission) {
		return ArrayUtils.contains(permissions, permission.name());
	}

	public void setPermissions(String permissions) {
		if (StringUtils.isNotBlank(permissions)) {
			this.permissions = permissions.split(",");
		}

		LOG.info("Permission specified in request: \"" + StringUtils.join(this.permissions, ", ") + "\"");

		List<String> validPermissions = new ArrayList<String>();

		for (String permission : this.permissions) {
			PermissionValue[] values = PermissionValue.values();

			for (PermissionValue permissionValue : values) {
				if (permissionValue.name().equalsIgnoreCase(permission)) {
					validPermissions.add(permissionValue.name());
				}
			}
		}

		this.permissions = new String[validPermissions.size()];
		validPermissions.toArray(this.permissions);

		LOG.info("Valid Permissions will only be assigned: \"" + StringUtils.join(this.permissions, ", ") + "\"");
	}

	public String[] getPermissions() {
		return permissions;
	}

	public void setResource(String resource) {
		this.resource = Preconditions.checkNotNull(resource);
	}

	public String getResource() {
		return resource;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("Resource URL: " + resource);
		sb.append(", permissions = \"" + StringUtils.join(permissions, ", ") + "\"");

		return sb.toString();
	}

	public String getDBPermission() {
		String permissionValue;

		if (this.hasPermission(PermissionValue.ALL)) {
			permissionValue = "1111";
		} else {
			StringBuilder permissionBuilder = new StringBuilder("0000");

			if (this.hasPermission(PermissionValue.READ)) {
				permissionBuilder.setCharAt(0, '1');
			}

			if (this.hasPermission(PermissionValue.WRITE)) {
				permissionBuilder.setCharAt(1, '1');
			}

			if (this.hasPermission(PermissionValue.UPDATE)) {
				permissionBuilder.setCharAt(2, '1');
			}

			if (this.hasPermission(PermissionValue.DELETE)) {
				permissionBuilder.setCharAt(3, '1');
			}

			permissionValue = permissionBuilder.toString();
		}

		LOG.info("Requested Permissions: " + this + " Required Permission in DB: " + permissionValue);
		return permissionValue;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ResourcePermission)) {
			return false;
		}

		return resource.equals(((ResourcePermission) obj).resource);
	}
}
