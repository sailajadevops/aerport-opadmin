package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IPacketRoundingPolicyEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.PacketRoundingPolicy;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * PacketRoundingPolicy Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class PacketRoundingPolicyEnum implements IPacketRoundingPolicyEnum, IEventListener  {
	private Logger LOG = LoggerFactory.getLogger(PacketRoundingPolicyEnum.class);
	
	static final String SQL_GET_PACKET_ROUNDING_POLICIES = "select policy_id, policy_name from packet_data_rounding_policy order by policy_id";
	
	private Cache<String, PacketRoundingPolicy> cache;
	private Cache<String, PacketRoundingPolicy> cacheById;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "PacketRoundingPolicyCache/name") Cache<String, PacketRoundingPolicy> cache, 
			@Hazelcast(cache = "PacketRoundingPolicyCache/id") Cache<String, PacketRoundingPolicy> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_PACKET_ROUNDING_POLICIES);
			
			while (rs.next()) {
				PacketRoundingPolicy packetRoundingPolicy = new PacketRoundingPolicy();
				
				packetRoundingPolicy.setPolicyId(rs.getInt("policy_id"));
				packetRoundingPolicy.setPolicyName(rs.getString("policy_name"));
				
				cache.put(packetRoundingPolicy.getPolicyName(), packetRoundingPolicy);
				cacheById.put(String.valueOf(packetRoundingPolicy.getPolicyId()), packetRoundingPolicy);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}	

	@Override
	public PacketRoundingPolicy getPacketRoundingPolicyByName(String name) {
		return cache.get(name);
	}

	@Override
	public PacketRoundingPolicy getPacketRoundingPolicyById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<PacketRoundingPolicy> getAllPacketRoundingPolicies() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
