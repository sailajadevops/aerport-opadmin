package com.aeris.service.operatoradmin.payload;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.ZonePreference;

@XmlRootElement
public class CreateZoneRequest {
	@NotNull(message = "{zoneName.notnull}")
	@Size(min = 1, max = 30, message = "{zoneName.size}")
	private String zoneName;

	@NotNull(message = "{productId.notnull}")
	@Pattern(regexp = "[0-9]+", message = "{productId.number}")
	private String productId;

	@Size(min = 0, message = "{geographies.size}")
	private List<String> geographies;

	@Size(min = 0, message = "{operators.size}")
	private List<String> operators;

	@Valid
	@NotNull(message = "{enrolledServices.notnull}")
	private EnrolledServices services;

	@Valid
	@NotNull(message = "{includedPlan.notnull}")
	private IncludedPlanDetails includedPlan;

	@Valid
	private List<ZonePreference> zonePreferences;

	@Email(message = "{userId.email}")
	@NotNull(message = "{userId.notnull}")
	private String userId;
	
	private String technology;

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public List<String> getGeographies() {
		return geographies;
	}

	public void setGeographies(List<String> geographies) {
		this.geographies = geographies;
	}

	public List<String> getOperators() {
		return operators;
	}

	public void setOperators(List<String> operators) {
		this.operators = operators;
	}

	public EnrolledServices getServices() {
		return services;
	}

	public void setServices(EnrolledServices services) {
		this.services = services;
	}

	public IncludedPlanDetails getIncludedPlan() {
		return includedPlan;
	}

	public void setIncludedPlan(IncludedPlanDetails includedPlan) {
		this.includedPlan = includedPlan;
	}

	public List<ZonePreference> getZonePreferences() {
		return zonePreferences;
	}

	public void setZonePreferences(List<ZonePreference> zonePreferences) {
		this.zonePreferences = zonePreferences;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
