package com.aeris.service.operatoradmin.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.delegate.impl.SimpleTemplateProcessor;
import com.aeris.service.operatoradmin.enums.IOperatorEnum;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.model.Operator;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.google.inject.Singleton;

@Singleton
public class OAUtils {
	private static final Logger LOG = LoggerFactory.getLogger(OAUtils.class);

	@Inject
	private IProductEnum productCache;

	@Inject
	private IOperatorEnum operatorCache;

	private static final String AER_CLOUD = "Aercloud";

	public boolean hasAerCloud(List<String> productIds) {
		boolean available = false; 
		Product aerCloudProduct = productCache.getProductByName(AER_CLOUD);

		LOG.info("Validating aerCloud product selection in order to create aercloud account...");

		if(aerCloudProduct != null)
		{
			available = productIds.contains(String.valueOf(aerCloudProduct.getProductId()));
		}

		LOG.info("Aercloud product selected: "+available);
		return available;
	}

	public boolean isFutureDate(Date date)
	{
		Date currDate = ApplicationUtils.getTodaysStartTimeStamp(Constants.TIMEZONE_GMT);

		return date.equals(currDate) || date.after(currDate);
	}

	public String generateRatePlanName(int operatorId, long ratePlanId, int productId)
	{
		StringBuffer sb = new StringBuffer(); 

		// Append Operator Name
		Operator operator = operatorCache.getOperatorById(operatorId);

		if(operator != null)
		{
			// AER for Aeris operator 
			// SPR for Sprint operator
			String operatorPrefix = operator.getOperatorName().substring(0, 3).toUpperCase();
			sb.append(operatorPrefix);
		}

		// Append Product Name
		Product product = productCache.getProductById(productId);

		if(product != null)
		{
			// CITY for CITY Sim product - to be removed
			// COVE for COVERAGE Sim product
			// GLOB for GLOBAL Sim product
			// CDMA for CDMA product
			// CDMA for CDMA-CC product
			String productSuffix = product.getProductName().substring(0, 4).toUpperCase();

			if(product.getProductName().toUpperCase().contains("COVERAGE"))
			{
				productSuffix = "COVSIM";
			}
			else if(product.getProductName().toUpperCase().contains("GLOBAL"))
			{
				productSuffix = "GLOBSIM";
			}
			else if(product.getProductName().toUpperCase().contains("CITY"))
			{
				productSuffix = "CITYSIM";
			}
			else if(product.getProductName().toUpperCase().contains("CDMA"))
			{
				productSuffix = "CDMA";
			}
			else if(product.getProductName().toUpperCase().contains("AERCLOUD"))
			{
				productSuffix = "AERCLD";
			}

			sb.append("_");
			sb.append(productSuffix);
		}

		// Append Rate Plan Id
		sb.append("_");
		sb.append(ratePlanId);

		return sb.toString();
	}

	public String generateExportedFileName(RatePlan ratePlan) {
		StringBuffer exportedFileName = new StringBuffer("EXPORT_")
		.append(ratePlan.getRatePlanName())
		.append(".txt");

		return exportedFileName.toString();
	}

	public String formatContent(String content, String userAgent) {
		String newLine = "\n";// Default for unix

		// If windows client
		if(StringUtils.containsIgnoreCase(userAgent, "windows"))
		{
			newLine = "\r\n";
		}	

		String newContent = content.replaceAll("\n", newLine);

		return newContent;
	}

	public String generateRatePlanName(int operatorId, String suffix, int productId)
	{
		StringBuffer sb = new StringBuffer(); 

		// Append Operator Name
		Operator operator = operatorCache.getOperatorById(operatorId);

		if(operator != null)
		{
			// AER for Aeris operator 
			// SPR for Sprint operator
			String operatorPrefix = operator.getOperatorName().substring(0, 3).toUpperCase();
			sb.append(operatorPrefix);
		}

		// Append Product Name
		Product product = productCache.getProductById(productId);

		if(product != null)
		{
			// CITY for CITY Sim product - to be removed
			// COVE for COVERAGE Sim product
			// GLOB for GLOBAL Sim product
			// CDMA for CDMA product
			// CDMA for CDMA-CC product
			String productSuffix = product.getProductName().substring(0, 4).toUpperCase();

			if(product.getProductName().toUpperCase().contains("COVERAGE"))
			{
				productSuffix = "COVSIM";
			}
			else if(product.getProductName().toUpperCase().contains("GLOBAL"))
			{
				productSuffix = "GLOBSIM";
			}
			else if(product.getProductName().toUpperCase().contains("CITY"))
			{
				productSuffix = "CITYSIM";
			}
			else if(product.getProductName().toUpperCase().contains("CDMA"))
			{
				productSuffix = "CDMA";
			}
			else if(product.getProductName().toUpperCase().contains("AERCLOUD"))
			{
				productSuffix = "AERCLD";
			}

			sb.append("_");
			sb.append(productSuffix);
		}

		// Append Rate Plan Id
		sb.append("_");
		sb.append(suffix);

		return sb.toString();
	}

	public File getTemplate(String templatePath) throws IOException {
		// Fetch the Template
		URL exportTemplateURL = SimpleTemplateProcessor.class.getResource(templatePath);
		String fileName;

		if (exportTemplateURL != null && exportTemplateURL.getProtocol().equals("file")) {
			fileName = exportTemplateURL.getFile();       
			LOG.debug("Template File Path: "+fileName);
		} else {
			LOG.error("Export Template "+templatePath+" cannot be found");
			throw new IOException("Export Template "+templatePath+" cannot be found");
		}

		// Read the template content
		File templateFile = new  File(fileName);

		return templateFile;
	}


	// Verify Luhn Algorithm
	public static boolean isValidNumber(String s) {
		return doLuhn(s, false) % 10 == 0;
	}

	/*
	 * Methos to create 5 digit unique code witht the help of Luhn Algorithm.
	 */
	public static String generateDigit(String s) {
		int digit;
		int value = doLuhn(s, true) % 10;
		if (value != 0) {
			digit = 10 - value;
		} else {
			digit = value;
		}
		return "" + digit;
	}

	public static int doLuhn(String s, boolean evenPosition) {
		int sum = 0;
		for (int i = s.length() - 1; i >= 0; i--) {
			int n = Integer.parseInt(s.substring(i, i + 1));
			if (evenPosition) {
				n *= 2;
				if (n > 9) {
					n = (n % 10) + 1;
				}
			}
			sum += n;
			evenPosition = !evenPosition;
		}

		return sum;
	}
	
	public static void main(String[] args) {
		String randomDigit = RandomStringUtils.randomNumeric(5);
		System.out.println(randomDigit);
		String uniqueId = randomDigit + OAUtils.generateDigit(randomDigit);
		System.out.println(uniqueId);
	}
	
	public static String getOracleINString(List<String> toJoinList) {
		StringBuilder sb = new StringBuilder("'");
		sb.append(StringUtils.join(toJoinList, "','")) ;
		sb.append("'");
		return sb.toString();
	}
}
