package com.aeris.service.operatoradmin.payload;

import com.aeris.service.operatoradmin.model.SIMPack;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Email;

@XmlRootElement
public class CreateSIMPacksRequest {

    @Email(message = "{userEmail.email}")
    @NotNull(message = "{userEmail.notnull}")
    private String userEmail;

    @NotNull(message = "{simPacks.notnull}")
    private List<SIMPack> simPacks;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public List<SIMPack> getSimPacks() {
        return simPacks;
    }

    public void setSimPacks(List<SIMPack> simPacks) {
        this.simPacks = simPacks;
    }

    @Override
    public String toString() {
        return "CreateSIMPacksRequest{" + "userEmail=" + userEmail + ", simPacks=" + simPacks + '}';
    }
}
