package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IRESEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.RESException;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.RES;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;
import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;

@Singleton
public class RESEnum implements IRESEnum, IEventListener {
	private Logger LOG = LoggerFactory.getLogger(RESEnum.class);
	
	private Cache<Integer, List<RES>> resCache;
	private static final String SQL_GET_ALL_RES = "select RES_ID, RES_NAME, PRODUCT_ID from RES_METADATA";

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}

	
	
	public void init(@Hazelcast(cache = "RESCache/name") Cache<Integer, List<RES>> resCache){
		this.resCache = resCache;
		loadResources();
	}
	
	void loadResources() {
		LOG.info("loadResources : loading all RES from database ..");

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_GET_ALL_RES :"+SQL_GET_ALL_RES);
			}

			ps = conn.prepareStatement(SQL_GET_ALL_RES);
			rs = ps.executeQuery();

			while(rs.next()){
				RES res = new RES();
				res.setRESId(Integer.parseInt(rs.getString("RES_ID")));
				res.setRESName(rs.getString("RES_NAME"));
				int productId = rs.getInt("PRODUCT_ID");
				
				putInCache(productId, res);
			}

		}catch (Exception e) {
			LOG.error("Exception during getAllRES " + e);
			throw new GenericServiceException("Exception during getAllRES - ", e);
		}  finally {
			DBUtils.cleanup(conn, ps, rs);
		}

	}

	private void putInCache(int productId, RES resObj){
		List<RES> resList = resCache.get(productId);
		if(resList != null){
			resList.add(resObj);
		}else{
			resList = new ArrayList<RES>();
			resList.add(resObj);
		}
		resCache.put(productId, resList);
	}
	
	@Override
	public List<RES> getRESList(int productId) {
		return resCache.get(productId);
	}

	@Override
	public boolean isValidRES(int productId, long resId) {
		List<RES> resList = resCache.get(productId);
		for(RES res : resList){
			if(res.getRESId() == resId){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean isValid(String id) {
		boolean isValid = false;
		if(id != null){
			isValid = resCache.get(Integer.valueOf(id)) != null;
		}
		return isValid;
	}
}
