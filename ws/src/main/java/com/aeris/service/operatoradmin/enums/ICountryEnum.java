package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.Country;

public interface ICountryEnum  extends IBaseEnum{
	Country getCountryByName(String name);
	Country getCountryById(int id);
	List<Country> getAllCountries();
}
