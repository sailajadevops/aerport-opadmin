package com.aeris.service.operatoradmin.events;

public enum Event {
	REFRESH_CACHE("refresh"),
	RATE_PLAN_CREATED("rate plan created"),
	RATE_PLAN_APPROVED("rate plan approved"),
    RATE_PLAN_ASSIGNED("Rate plan assigned"),
    ACCOUNT_CREATED("Account created"),
    UNIQUE_CODES_CREATED("Codes created"),
    ACCOUNT_ADDRESS_CHANGE("address changed"),
    ACCOUNT_PRIMARY_EMAIL_CHANGE("primary email changed"),
    ACCOUNT_APPROVED("Account approved"),
    CREDIT_LIMIT_CHECK("credit limit check"), 
	CREDIT_LIMIT_UPDATE("credit limit update");
	
	private String name;
	
	Event(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
