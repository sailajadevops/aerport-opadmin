package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

@XmlRootElement
public class InvoiceAddress implements Serializable {
	private static final long serialVersionUID = 1170404856678900526L;

	@Size(min = 1, max = 256, message = "{invoiceAddress.fullAddress.size}")
	private String fullAddress;

	@Size(min = 1, max = 60, message = "{invoiceAddress.addressFirstLine.size}")
	private String addressFirstLine;

	@Size(min = 1, max = 60, message = "{invoiceAddress.addressSecondLine.size}")
	private String addressSecondLine;

	private String state;

	private String country;

	private String zipCode;

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getAddressFirstLine() {
		return addressFirstLine;
	}

	public void setAddressFirstLine(String addressFirstLine) {
		this.addressFirstLine = addressFirstLine;
	}

	public String getAddressSecondLine() {
		return addressSecondLine;
	}

	public void setAddressSecondLine(String addressSecondLine) {
		this.addressSecondLine = addressSecondLine;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
