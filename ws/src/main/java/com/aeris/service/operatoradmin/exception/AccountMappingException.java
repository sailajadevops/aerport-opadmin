package com.aeris.service.operatoradmin.exception;

public class AccountMappingException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public AccountMappingException(String s) {
		super(s);
	}

	public AccountMappingException(String ex, Throwable t) {
		super(ex, t);
	}
}
