package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import com.aeris.service.operatoradmin.payload.CreateRatePlanPoolRequest;
import com.aeris.service.operatoradmin.payload.UpdateRatePlanPoolRequest;

@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD, PARAMETER, TYPE})
@Documented
@Constraint(validatedBy = {ValidateRatePlanPoolDate.Validator.class, ValidateRatePlanPoolDate.UpdateValidator.class})
public @interface ValidateRatePlanPoolDate {
	String message() default "{com.aeris.service.operatoradmin.model.constraints.ValidateRatePlanDate.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class Validator implements ConstraintValidator<ValidateRatePlanPoolDate, CreateRatePlanPoolRequest> {

		@Override
		public void initialize(final ValidateRatePlanPoolDate enumClazz) {
			// do nothing
		}

		public boolean isValid(final CreateRatePlanPoolRequest request, final ConstraintValidatorContext constraintValidatorContext) {
			if(request.getStartDate() == null || request.getEndDate() == null )
			{
				return true;
			}
			
			if(request.getEndDate().after(request.getStartDate()))
			{
				return true;
			}
			
			return false;
		}
	}
	
	public class UpdateValidator implements ConstraintValidator<ValidateRatePlanPoolDate, UpdateRatePlanPoolRequest> {

		@Override
		public void initialize(final ValidateRatePlanPoolDate enumClazz) {
			// do nothing
		}

		public boolean isValid(final UpdateRatePlanPoolRequest request, final ConstraintValidatorContext constraintValidatorContext) {
			if(request.getStartDate() == null || request.getEndDate() == null )
			{
				return true;
			}
			
			if(request.getEndDate().after(request.getStartDate()))
			{
				return true;
			}
			
			return false;
		}
	}
}
