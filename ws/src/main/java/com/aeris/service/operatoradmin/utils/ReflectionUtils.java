package com.aeris.service.operatoradmin.utils;

import java.util.Set;

import org.reflections.Reflections;
import org.reflections.util.ClasspathHelper;

public class ReflectionUtils {
	public static final String DEFAULT_PACKAGE = "com.aeris.service.operatoradmin";

	public static <I> Set<Class<? extends I>> getSubTypesOf(String inPackage, Class<I> type)
	{
		Reflections reflections = new Reflections(ClasspathHelper.forPackage(inPackage));
		Set<Class<? extends I>> subTypes = reflections.getSubTypesOf(type);
		
		return subTypes;
	}
	
	public static <I> Set<Class<? extends I>> getSubTypesOf(Class<I> type)
	{
		Reflections reflections = new Reflections(ClasspathHelper.forPackage(DEFAULT_PACKAGE));
		Set<Class<? extends I>> subTypes = reflections.getSubTypesOf(type);
		
		return subTypes;
	}
}
