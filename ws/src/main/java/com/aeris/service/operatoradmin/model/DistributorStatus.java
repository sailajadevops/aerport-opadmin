package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum DistributorStatus implements Serializable {
	COMMERCIAL(1), ON_BOARDING(2), TRIAL(4), SUSPENDED(0);
	int value;

	DistributorStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static DistributorStatus fromValue(int value) {
		DistributorStatus[] statuses = values();

		for (DistributorStatus accountStatus : statuses) {
			if (accountStatus.getValue() == value) {
				return accountStatus;
			}
		}

		return null;
	}

	public static DistributorStatus fromName(String value) {
		DistributorStatus[] statuses = values();

		for (DistributorStatus accountStatus : statuses) {
			if (accountStatus.name().equalsIgnoreCase(value)) {
				return accountStatus;
			}
		}

		return null;
	}
}
