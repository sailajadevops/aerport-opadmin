package com.aeris.service.operatoradmin.payload;

import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * The DTO class for simpack
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class CreateSimpackRequest {

	private int packCount ;
	private int packSize ;
	private List<String> emailRecipients ;
	@NotNull
	private String iccidStart;
	@NotNull
	private String iccidEnd ;
	private String simType ;
	private String userId ;
    private String iccids;
    private String accountId;
    private String distributorId;
	
	public List<String> getEmailRecipients() {
		return emailRecipients;
	}
	
	public void setEmailRecipients(List<String> emailRecipients) {
		this.emailRecipients = emailRecipients;
	}


	public String getIccidStart() {
		return iccidStart;
	}

	public void setIccidStart(String iccidStart) {
		this.iccidStart = iccidStart;
	}

	public String getIccidEnd() {
		return iccidEnd;
	}

	public void setIccidEnd(String iccidEnd) {
		this.iccidEnd = iccidEnd;
	}

	public int getPackCount() {
		return packCount;
	}

	public void setPackCount(int packCount) {
		this.packCount = packCount;
	}

	public int getPackSize() {
		return packSize;
	}

	public void setPackSize(int packSize) {
		this.packSize = packSize;
	}

	public String getSimType() {
		return simType;
	}

	public void setSimType(String simType) {
		this.simType = simType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

    public String getIccids() {
        return iccids;
    }

    public void setIccids(String iccids) {
        this.iccids = iccids;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }
    
}
