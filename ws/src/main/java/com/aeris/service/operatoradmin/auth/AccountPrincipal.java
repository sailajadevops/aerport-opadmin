package com.aeris.service.operatoradmin.auth;

import java.security.Principal;

import com.aeris.service.operatoradmin.model.Role;

/**
 * AccountPrincipal role mapping security context
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class AccountPrincipal implements Principal {

	private Role role;
	private String name;
	

	public AccountPrincipal(final Role role) {
		this.role = role;
		this.name = role.toString();
	}
	
	public void setName(final String lName) {
		this.name = lName;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	public Role getRole() {
		return role;
	}
}
