package com.aeris.service.operatoradmin.client;

public class CreateAerCloudDataModelRequest {
	private String accountId;
	private String name;
	private String id;
	private String description;
	private SclDataSchema sclDataSchema;
	private String apiKey;

	public String getAccountId() {
		return accountId;
	}
	
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SclDataSchema getSclDataSchema() {
		return sclDataSchema;
	}

	public void setSclDataSchema(SclDataSchema sclDataSchema) {
		this.sclDataSchema = sclDataSchema;
	}
	
	public String getApiKey() {
		return apiKey;
	}
	
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
}
