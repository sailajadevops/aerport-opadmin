package com.aeris.service.operatoradmin.dao;

import java.util.List;

import com.aeris.service.operatoradmin.model.Carrier;

public interface IProductCarrierDAO {
	List<Carrier> getCarriersByProduct(final int productId);
}
