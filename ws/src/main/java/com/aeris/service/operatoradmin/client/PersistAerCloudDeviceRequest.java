package com.aeris.service.operatoradmin.client;

public class PersistAerCloudDeviceRequest {
	private String accountID;
	private String sclId;
	private DeviceID deviceID;
	private String deviceName;
	private String dataModelId;
	private String technology = "GENERIC";
	private String apiKey;

	public String getAccountID() {
		return accountID;
	}

	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}

	public String getSclId() {
		return sclId;
	}

	public void setSclId(String sclId) {
		this.sclId = sclId;
	}

	public DeviceID getDeviceID() {
		return deviceID;
	}

	public void setDeviceID(DeviceID deviceID) {
		this.deviceID = deviceID;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDataModelId() {
		return dataModelId;
	}

	public void setDataModelId(String dataModelId) {
		this.dataModelId = dataModelId;
	}
	
	public String getApiKey() {
		return apiKey;
	}
	
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}
}
