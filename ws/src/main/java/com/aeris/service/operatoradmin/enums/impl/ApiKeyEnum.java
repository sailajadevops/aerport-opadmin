package com.aeris.service.operatoradmin.enums.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.IAccountApplicationDAO;
import com.aeris.service.operatoradmin.enums.IApiKeyEnum;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.ApiKey;
import com.google.inject.Singleton;

/**
 * API Key Cache used for lookup purposes
 * 
 * @author Saurabh.sharma@aeris.net
 */
@Singleton
public class ApiKeyEnum implements IApiKeyEnum {
	private Logger LOG = LoggerFactory.getLogger(ApiKeyEnum.class);
	
	private Cache<String, ApiKey> cache;
	private Cache<Long, List<ApiKey>> accountApiKeyCache ;
	@Inject private IAccountApplicationDAO accountApplicationDAO ;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "ApiKeyCache/name") Cache<String, ApiKey> cache,
			@Hazelcast(cache = "ApiKeyCache/name") Cache<Long, List<ApiKey>> accountApiKeyCache) {
		this.cache = cache;	
		this.accountApiKeyCache = accountApiKeyCache ;
		loadApiKeys();
	}
	
	/**
	 * Loads the ApiKeys object into the memory for future lookup.
	 */
	void loadApiKeys() {
		LOG.info("loading apiKeys from db");
		
		try {
			List<ApiKey> apiKeys = accountApplicationDAO.getAllApiKeys();
			for (ApiKey apiKey : apiKeys) {
				cache.put(apiKey.getApiKey(), apiKey);
				List<ApiKey> apiKeyList = accountApiKeyCache.get(apiKey.getAccountId());
				if(apiKeyList != null && apiKeyList.size() > 0) {
					apiKeyList.add(apiKey);
				} else {
					apiKeyList = new ArrayList<ApiKey>();
					apiKeyList.add(apiKey);
				}
				accountApiKeyCache.put(apiKey.getAccountId(), apiKeyList);
			}
			LOG.info("apiKeys loaded successfully");
		} catch (Exception e) {
			LOG.error("Exception during apiKeys", e);
			throw new GenericServiceException("Unable to loadPermissions", e);
		} 
	}
	
	
	@Override
	public boolean isValid(String apiKey) {
		return cache.get(apiKey) != null; 
	}
	
	
	/**
	 * Fetches the ApiKey Definition matching apiKey string value
	 * 
	 * @param apiKey string
	 * @return the ApiKey VO
	 */
	@Override
	public ApiKey get(String apiKey) {
		return cache.get(apiKey);
	}
	
	
	@Override
	public List<ApiKey> get(Long accountId) {
		return accountApiKeyCache.get(accountId);
	}
	
	
	@Override
	public void updateAccountKeyCache(Long accountId, ApiKey key) {
		List<ApiKey> apiKeys =  accountApiKeyCache.get(accountId);
		if(apiKeys == null ) {
			apiKeys = new ArrayList<ApiKey>();
		}
		apiKeys.add(key);
		accountApiKeyCache.put(accountId, apiKeys);
	}
}
