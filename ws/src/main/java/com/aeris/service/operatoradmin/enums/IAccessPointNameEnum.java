package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.AccessPointName;

public interface IAccessPointNameEnum  extends IBaseEnum{
	List<AccessPointName> getAccessPointNamesByByType(String type);
	AccessPointName getAccessPointNameById(int id);
	List<AccessPointName> getAllAccessPointNames();
	List<AccessPointName> getAccessPointNamesByContract(String contractId,String type);
}
