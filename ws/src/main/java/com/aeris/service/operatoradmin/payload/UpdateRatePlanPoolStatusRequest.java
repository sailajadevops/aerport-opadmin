package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.RatePlanPoolStatus;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@XmlRootElement
public class UpdateRatePlanPoolStatusRequest implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;
	
	@Email(message="{userId.email}")
	private String userId;
	
	@Enumeration(value = RatePlanPoolStatus.class, message = "{status.enum}")
	private RatePlanPoolStatus status;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public RatePlanPoolStatus getStatus() {
		return status;
	}

	public void setStatus(RatePlanPoolStatus status) {
		this.status = status;
	}
}
