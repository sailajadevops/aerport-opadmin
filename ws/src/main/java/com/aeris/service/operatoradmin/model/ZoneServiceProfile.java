package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@XmlRootElement
public class ZoneServiceProfile implements Serializable {
	private static final long serialVersionUID = -6082497606403240919L;
	private int zoneId;
	private EnrolledServices services;

	@Valid
	private List<ZonePreference> zonePreferences;
	
	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public EnrolledServices getServices() {
		return services;
	}

	public void setServices(EnrolledServices services) {
		this.services = services;
	}
	
	public List<ZonePreference> getZonePreferences() {
		return zonePreferences;
	}
	
	public void setZonePreferences(List<ZonePreference> zonePreferences) {
		this.zonePreferences = zonePreferences;
	}
}