package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SIMFormat;
import com.google.inject.Inject;

public class GetSIMFormatsStoredProcedure extends AbstractProcedureCall<List<SIMFormat>> {

	private static final String SQL_GET_SIM_FORMATS = "common_util_pkg.get_sim_format";

	@Inject
	public GetSIMFormatsStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_SIM_FORMATS, true);
		
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SIMFormat> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<SIMFormat> formats = createSIMFormatList(cursor);

		return formats;
	}

	private List<SIMFormat> createSIMFormatList(List<Map<String, Object>> cursor) {
		List<SIMFormat> formats = new ArrayList<SIMFormat>();

		for (Map<String, Object> row : cursor) {
			SIMFormat format = new SIMFormat();

			String formatId = (String) row.get("format_id");

			if (NumberUtils.isNumber(formatId)) {
				format.setId(Integer.parseInt(formatId));
			}

			String formatName = (String) row.get("format_name");

			if (StringUtils.isNotEmpty(formatName)) {
				format.setName(formatName);
			}
			
			formats.add(format);
		}

		return formats;
	}
}
