package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SIMSupplier;
import com.google.inject.Inject;

public class GetSuppliersStoredProcedure extends AbstractProcedureCall<List<SIMSupplier>> {

	private static final String SQL_GET_SUPPLIERS = "common_util_pkg.get_sim_supplier";

	@Inject
	public GetSuppliersStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_SUPPLIERS, true);
		
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SIMSupplier> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<SIMSupplier> suppliers = createSupplierList(cursor);

		return suppliers;
	}

	private List<SIMSupplier> createSupplierList(List<Map<String, Object>> cursor) {
		List<SIMSupplier> suppliers = new ArrayList<SIMSupplier>();

		for (Map<String, Object> row : cursor) {
			SIMSupplier supplier = new SIMSupplier();

			String supplierId = (String) row.get("supplier_id");

			if (NumberUtils.isNumber(supplierId)) {
				supplier.setSupplierId(Integer.parseInt(supplierId));
			}

			String supplierName = (String) row.get("supplier_name");

			if (StringUtils.isNotEmpty(supplierName)) {
				supplier.setName(supplierName);
			}
			
			String country = (String) row.get("country");

			if (StringUtils.isNotEmpty(country)) {
				supplier.setCountry(country);
			}
			
			suppliers.add(supplier);
		}

		return suppliers;
	}
}
