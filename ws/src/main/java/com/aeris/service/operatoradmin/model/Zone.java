package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@XmlRootElement 
public class Zone implements Serializable {
	private static final long serialVersionUID = 4136284250645045495L;
	private int zoneId;
	private String zoneName;
	private int productId;
	private List<String> operators;
	private List<String> geographies;
	private EnrolledServices services;
	private IncludedPlanDetails includedPlan;
	private List<ZonePreference> zonePreferences;
	private int operatorId;
	private int zoneSetId;
	private String technology;
	
	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public List<String> getOperators() {
		return operators;
	}

	public void setOperators(List<String> operators) {
		this.operators = operators;
	}

	public List<String> getGeographies() {
		return geographies;
	}

	public void setGeographies(List<String> geographies) {
		this.geographies = geographies;
	}

	public EnrolledServices getServices() {
		return services;
	}

	public void setServices(EnrolledServices services) {
		this.services = services;
	}

	public IncludedPlanDetails getIncludedPlan() {
		return includedPlan;
	}

	public void setIncludedPlan(IncludedPlanDetails includedPlan) {
		this.includedPlan = includedPlan;
	}

	public List<ZonePreference> getZonePreferences() {
		return zonePreferences;
	}

	public void setZonePreferences(List<ZonePreference> zonePreferences) {
		this.zonePreferences = zonePreferences;
	}

	public int getOperatorId() {
		return operatorId;
	}
	
	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}
	
	public int getZoneSetId() {
		return zoneSetId;
	}

	public void setZoneSetId(int zoneSetId) {
		this.zoneSetId = zoneSetId;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
