package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IOperatorConfigDAO;
import com.aeris.service.operatoradmin.exception.OperatorException;
import com.aeris.service.operatoradmin.model.Country;
import com.aeris.service.operatoradmin.model.Operator;
import com.aeris.service.operatoradmin.model.RoamingOperator;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.utils.Constants;
import com.aeris.service.operatoradmin.utils.DBUtils;

public class OperatorConfigDAO implements IOperatorConfigDAO{

	private static final String SQL_GET_OPERATOR_CONFIGURATION = "select a.property_name, " +   
																	"a.property_value " +
																	"from " +
																	"ws_operator_config a " +
																	"where a.operator_id = ?";
	
	private static final String SQL_GET_ROAMING_COUNTRIES_FOR_EXCLUSION = "select geo "+
																"from "+
																"(select distinct nvl(lower(extract (value (d), '//row/text()').getstringval ()), 0) geo "+
																"from "+
																"(select xmltype ( '<rows><row>' "+
																"|| replace (geographies, ',', '</row><row>') "+
																"|| '</row></rows>' ) as xmlval "+
																"from roaming_zones "+
																"where operator_id = ? "+
																"and zone_set_id = ? and geographies is not null "+
																") x, "+
																"table (xmlsequence (extract (x.xmlval, '/rows/row'))) d "+
																") "+
																"where geo not in "+
																"(select distinct nvl(lower(extract (value (e), '//row/text()').getstringval ()), 0) "+
																"from "+
																"(select xmltype ( '<rows><row>' "+
																"|| replace (geographies, ',', '</row><row>') "+
																"|| '</row></rows>' ) as xmlval "+
																"from roaming_zones "+
																"where operator_id = ? "+
																"and zone_id = ? and geographies is not null "+
																") y, "+
																"table (xmlsequence (extract (y.xmlval, '/rows/row'))) e "+
																")";
																

	
	private static final String SQL_GET_ROAMING_OPERATORS_FOR_EXCLUSION = "select ops "+
															 "from "+
															 "(select distinct nvl(lower(extract (value (d), '//row/text()').getstringval ()), ' ') ops "+
															 "from "+
															 "(select xmltype ( '<rows><row>'"+
															 "|| replace (operators, ',', '</row><row>')"+
															 "|| '</row></rows>' ) as xmlval "+
															 "from roaming_zones "+
															 "where operator_id = ? "+
															 "and zone_set_id = ? and operators is not null"+
															 ") x, "+
															 "table (xmlsequence (extract (x.xmlval, '/rows/row'))) d "+
															 ")"+
															 "where ops not in "+
															 "(select distinct nvl(lower(extract (value (e), '//row/text()').getstringval ()), ' ') "+
															 "from "+
															 "(select xmltype ( '<rows><row>'"+
															 "|| replace (operators, ',', '</row><row>')"+
															 "|| '</row></rows>' ) as xmlval "+
															 "from roaming_zones "+
															 "where operator_id = ? "+
															 "and zone_id = ? and operators is not null"+
															 ") y, "+
															 "table (xmlsequence (extract (y.xmlval, '/rows/row'))) e "+
															 ")";
															 
	
	private static final String SQL_GET_ALL_COUNTRIES = "select country_id, "+
														"country_name "+
														"from NETWORK_STORE.COUNTRY ";
	
	private static final String SQL_GET_ALL_OPERATORS = "select CO.COUNTRY_OPERATOR_ID as operator_id,(O.OPERATOR_NAME || ' (' || C.COUNTRY_NAME || ')') as operator_name "+
														 "from NETWORK_STORE.COUNTRY_OPERATOR CO, NETWORK_STORE.COUNTRY C, NETWORK_STORE.OPERATOR O "+
														 "where CO.COUNTRY_ID = C.COUNTRY_ID AND CO.OPERATOR_ID = O.OPERATOR_ID";

	private Cache<Integer, Map<String, String>> cache;
	private Cache<Integer, Zone> zoneCache;
	private Cache<Integer, Country> countryCache;
	private Cache<Long, RoamingOperator> countryOperatorCache;

	private static Logger LOG = LoggerFactory.getLogger(OperatorProductDAO.class);
	
	public void init(@Hazelcast(cache = "OperatorConfigCache/id") Cache<Integer, Map<String, String>> cache, @Hazelcast(cache = "ZoneCache/id") Cache<Integer, Zone> zoneCache, 
			@Hazelcast(cache = "CountryCache/id") Cache<Integer, Country> countryCache, @Hazelcast(cache = "CountryOperatorCache/id") Cache<Long, RoamingOperator> countryOperatorCache) {
		this.zoneCache = zoneCache;
		this.cache = cache;
		this.countryCache = countryCache;
		this.countryOperatorCache = countryOperatorCache;
		
		List<Country> countriesList = getAllCountries();
		Country[] arrCountries = new Country[countriesList.size()];
		countriesList.toArray(arrCountries);
		putInCountryCache(arrCountries);
		
		List<RoamingOperator> operatorList = getAllOperators();
		RoamingOperator[] arrOperators = new RoamingOperator[operatorList.size()];
		operatorList.toArray(arrOperators);
		putInCountryOperatorCache(arrOperators);
	}
	
	private void putInCountryCache(Country... countries) {
		for (Country country : countries) {
			countryCache.put(country.getCountryId(), country);
		}
	}
	
	private void putInCountryOperatorCache(RoamingOperator... operators) {
		for (RoamingOperator operator : operators) {
			countryOperatorCache.put(operator.getOperatorId(), operator);
		}
	}
	
	@Override
	public List<RoamingOperator> getAllOperators() {

		LOG.debug("getAllOperators: Loading All operators...");		
		List<RoamingOperator> operators = new ArrayList<RoamingOperator>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("getAllOperators: Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch all Operators");
			
			ps = conn.prepareStatement(SQL_GET_ALL_OPERATORS);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				RoamingOperator operator = new RoamingOperator();
				operator.setOperatorId(rs.getInt("operator_id"));
				operator.setOperatorName(rs.getString("operator_name"));
				operators.add(operator);
			}
		} catch (SQLException e) {
			LOG.error("DB Exception during getAllOperators", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAllOperators: Returned " + operators.size() + " operators.");

		return operators;
	}
	
	@Override
	public List<Country> getAllCountries() {
		LOG.debug("getAllCountries: Loading All countries...");		
		List<Country> countries = new ArrayList<Country>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("getAllCountries: Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch getAllCountries");
			
			ps = conn.prepareStatement(SQL_GET_ALL_COUNTRIES);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Country country = new Country();
				country.setCountryId(rs.getInt("country_id"));
				country.setCountryName(rs.getString("country_name"));
				countries.add(country);
			}
		} catch (SQLException e) {
			LOG.error("DB Exception during getAllCountries", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAllCountries: Returned " + countries.size() + " countries.");

		return countries;
	
	
	}

	@Override
	public Map<String, String> getOperatorConfiguration(final int operatorId) throws OperatorException {
		LOG.debug("getProductsByOperator: processing get products by operator");
		
		// If already loaded, fetch from cache
		if(cache.get(operatorId) != null)
		{
			return cache.get(operatorId);
		}
		
		Map<String, String> configuration = new HashMap<String, String>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("getOperatorConfiguration: Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch operator configuration by operator from ws_operator_config table");
			
			// Build query to fetch the operator configuration 
			// for the specified operator id
			ps = buildGetOperatorConfigurationQuery(conn, operatorId);

			LOG.debug("getOperatorConfiguration: executed query: " + ps);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				String propertyName = rs.getString("property_name");
				String propertyValue = rs.getString("property_value");
				
				configuration.put(propertyName, propertyValue);
			}
			// USCC-141 : Read CREDIT_LIMIT_SETTING_ENABLED property for the given operator from WS_OPERATOR table
			ps = conn.prepareStatement("select CREDIT_LIMIT_SETTING_ENABLED from WS_OPERATOR where operator_id = ?");
			ps.setInt(1, operatorId);
			
			LOG.debug("executed query: " + ps);
			
			rs = ps.executeQuery();
			if(rs.next()){
				configuration.put(Constants.PROPERTY_CREDIT_LIMIT_SETTING_ENABLED, rs.getInt("CREDIT_LIMIT_SETTING_ENABLED") == 1 ? "true" : "false");
			}
			cache.put(operatorId, configuration);

		} catch (SQLException e) {
			LOG.error("Exception during getOperatorConfiguration", e);
			throw new OperatorException("Unable to getOperatorConfiguration", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getOperatorConfiguration: Returned " + ReflectionToStringBuilder.toString(configuration) + " for operator id: "+operatorId);

		return configuration;
	}

	private PreparedStatement buildGetOperatorConfigurationQuery(Connection conn, int operatorId) throws SQLException {
		PreparedStatement ps;
		
		// Get Products by operator
		StringBuilder query = new StringBuilder(SQL_GET_OPERATOR_CONFIGURATION);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());
		ps.setInt(1, operatorId);

		return ps;
	}
	
	@Override
	public List<Country> getRoamingCountries(int operatorId, int filteredZoneId, int zoneSetId) throws OperatorException {
		/* First find out the countries to be excluded from the resultset. 
		 * then remove those countries from All Countries stored in cache & return that result
		 */
		LOG.debug("getRoamingCountries: processing get countries by operator");		
		List<Country> countries = new ArrayList<Country>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Integer> countryIdsToExclude ;
		try {
			LOG.debug("getRoamingCountries: Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch roaming countries by operator from ws_country table");
			
			ps = buildGetRoamingCountriesForExclusionQuery(conn, operatorId, filteredZoneId, zoneSetId );

			LOG.debug("getRoamingCountries: executed query: {" + SQL_GET_ROAMING_COUNTRIES_FOR_EXCLUSION +"}, operatorId="+operatorId+", filteredZoneId="+filteredZoneId+", zoneSetId="+zoneSetId);
			rs = ps.executeQuery();
			
			countryIdsToExclude = new ArrayList<Integer>();
			while (rs.next()) {
				countryIdsToExclude.add(rs.getInt("geo"));
			}
		} catch (SQLException e) {
			LOG.error("DB Exception during getRoamingCountries", e);
			throw new OperatorException("DB Exception while getting roaming countries", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		//Copy countryCache in hashmap
		Map<Integer, Country> countryMap = new HashMap<Integer, Country>();
		for(Country country : countryCache.getValues()){
			countryMap.put(country.getCountryId(), country);
		}
		
		// Exclude the countries from cache
		for(Integer id : countryIdsToExclude){
			countryMap.remove(id);
		}

		LOG.info("getRoamingOperators: Returned " + countryMap.size() + " roaming operators for operator id: "+operatorId);

		return new ArrayList<Country>(countryMap.values());
	
	}
	
	@Override
	public List<Country> getRoamingCountries(final int operatorId, final int filteredZoneId) throws OperatorException {
		return getRoamingCountries(operatorId, filteredZoneId, 0);
	}

	private PreparedStatement buildGetRoamingCountriesForExclusionQuery(Connection conn, int operatorId, int filteredZoneId, int zoneSetId) throws SQLException {
		PreparedStatement ps;
		
		StringBuilder query = new StringBuilder(SQL_GET_ROAMING_COUNTRIES_FOR_EXCLUSION);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());
		ps.setInt(1, operatorId);	
		ps.setInt(2, zoneSetId);	
		ps.setInt(3, operatorId);	
		ps.setInt(4, filteredZoneId);
		
		return ps;	
	}
	
	@Override
	public List<RoamingOperator> getRoamingOperators(int operatorId, int filteredZoneId, int zoneSetId) throws OperatorException {
		/* First find out the operators to be excluded from the resultset. 
		 * then remove those operators from All operators stored in cache & return that result
		 */
		
		LOG.debug("getRoamingOperators: processing get roaming operators by operator id");		

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Long> operatorIdsToExclude ;
		try {
			LOG.debug("getRoamingOperators: Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch roaming operators by operator id from NETWORK_STORE.COUNTRY_OPERATOR table");
			
			ps = buildGetRoamingOperatorsForExclusionQuery(conn, operatorId, filteredZoneId, zoneSetId);

			LOG.debug("getRoamingOperators: executed query: {" + SQL_GET_ROAMING_OPERATORS_FOR_EXCLUSION + "} operatorId="+operatorId +", filteredZoneId="+filteredZoneId+", zoneSetId="+zoneSetId);
			rs = ps.executeQuery();
			
			operatorIdsToExclude = new ArrayList<Long>();
			while (rs.next()) {
				operatorIdsToExclude.add(rs.getLong("ops"));
			}
		} catch (SQLException e) {
			LOG.error("DB Exception during getRoamingOperators", e);
			throw new OperatorException("DB Exception while getting roaming operators", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		
		//Copy countryOperatorCache in hashmap
		Map<Long, RoamingOperator> operatorMap = new HashMap<Long, RoamingOperator>();
		for(RoamingOperator operator : countryOperatorCache.getValues()){
			operatorMap.put(operator.getOperatorId(), operator);
		}
		
		// Exclude the operators from cache
		for(Long id : operatorIdsToExclude){
			operatorMap.remove(id);
		}

		LOG.info("getRoamingOperators: Returned " + operatorMap.size() + " roaming operators for operator id: "+operatorId);

		return new ArrayList<RoamingOperator>(operatorMap.values());
	
	}
	
	@Override
	public List<RoamingOperator> getRoamingOperators(final int operatorId, final int filteredZoneId) throws OperatorException {
		return getRoamingOperators(operatorId, filteredZoneId, 0);
	}

	private PreparedStatement buildGetRoamingOperatorsForExclusionQuery(Connection conn, int operatorId, int filteredZoneId, int zoneSetId) throws SQLException {
		PreparedStatement ps;
		
		// Get Products by operator
		StringBuilder query = new StringBuilder(SQL_GET_ROAMING_OPERATORS_FOR_EXCLUSION);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());
		ps.setInt(1, operatorId);	
		ps.setInt(2, zoneSetId);	
		ps.setInt(3, operatorId);	
		ps.setInt(4, filteredZoneId);
		
		return ps;
	}
	
	public Map<Long, String> getOperatorKeys() {
		String operatorKeyQuery = "select ws_app.api_key, ws_op_app.operator_id from aerisgen.ws_application ws_app inner join "
				+ "aerisgen.ws_operator_applications ws_op_app on ws_app.app_id = ws_op_app.app_id";

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Long, String> operatorKeyMap = new HashMap<Long, String>();
		try {
			LOG.debug("getOperatorKeys: Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch operatorKey");
			ps = conn.prepareStatement(operatorKeyQuery);
			LOG.debug("getOperatorKeys: executed query: " + ps);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				operatorKeyMap.put(rs.getLong("operator_id"), rs.getString("api_key"));
			}
		} catch (SQLException e) {
			LOG.error("DB Exception during getOperatorKeys", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		
		return operatorKeyMap ;
	}
}
