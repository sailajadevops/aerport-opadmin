package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class SIMInfo implements Serializable {
	private static final long serialVersionUID = -8854088970523902724L;
	private String simIccId;
	private String simIsmi;
	private SIMFormat format;
	private String country;

	public String getSimIccId() {
		return simIccId;
	}

	public void setSimIccId(String simIccId) {
		this.simIccId = simIccId;
	}

	public String getSimIsmi() {
		return simIsmi;
	}

	public void setSimIsmi(String simIsmi) {
		this.simIsmi = simIsmi;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public SIMFormat getFormat() {
		return format;
	}

	public void setFormat(SIMFormat format) {
		this.format = format;
	}
}
