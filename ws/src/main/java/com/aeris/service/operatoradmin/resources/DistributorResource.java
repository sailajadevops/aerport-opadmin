package com.aeris.service.operatoradmin.resources;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aeris.service.operatoradmin.dao.IDistributorDAO;
import com.aeris.service.operatoradmin.exception.DistributorDBException;
import com.aeris.service.operatoradmin.exception.DistributorException;
import com.aeris.service.operatoradmin.exception.DistributorNotFoundException;
import com.aeris.service.operatoradmin.model.Distributor;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateDistributorCode;
import com.aeris.service.operatoradmin.payload.CreateDistributorRequest;
import com.aeris.service.operatoradmin.payload.Simpack;
import com.aeris.service.operatoradmin.payload.UpdateDistributorRequest;
import com.aeris.service.operatoradmin.payload.UpdateEmptySIMPACKRequest;

/**
 * Rest Resource for Distributor management. Includes methods for CRUD operations on distributor
 * 
 * @author saurabh.sharma@aeris.net
 */
@Path("/operators/{operatorId}/distributors")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class DistributorResource {
	private static final Logger LOG = LoggerFactory.getLogger(DistributorResource.class);

	@Inject
	private IDistributorDAO distributorDAO;

	private ObjectMapper mapper = new ObjectMapper();


	/**
	 * This api returns list of distributors by operator id.
	 * @param operatorId ID of operator.
	 * @return list of distributors by operator id.
	 */
	@GET
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllDistributors(@PathParam("operatorId") @ValidOperator final String operatorId) {
		LOG.info("Received request to fetch all distributors for operator {}", operatorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			List<Distributor> distributors = distributorDAO.getAllDistributors(Long.valueOf(operatorId));
			LOG.debug("Distributors fetched :", distributors);
			LOG.info("Distributors fetched successfully. No of distributors: {}", distributors.size());
			String distributorJson = mapper.writeValueAsString(distributors);
			builder.entity(distributorJson);
		} catch (DistributorException e) {
			LOG.error("DistributorException occured in getAllDistributors():", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		} catch (Exception ex) {
			LOG.error("IOException occured in getAllDistributors():", ex);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
		Response response = builder.build();
		LOG.debug("Sent response from getAllDistributors: " + response);
		return response;
	}

	/**
	 * Get a distributor by distributor id.
	 * @param operatorId ID of operator.
	 * @param distributorId ID of distributor.
	 * @return 
	 */
	@GET
	@Path("/{distributorId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getDistributor (
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId) {
		LOG.info("Received request to fetch distributor with id {}", distributorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			Distributor distributor = distributorDAO.getDistributor(Long.valueOf(operatorId), Long.valueOf(distributorId));
			LOG.debug("Fetched distributor successfully for distributorId {}", mapper.writeValueAsString(distributor));
			String distributorJson = mapper.writeValueAsString(distributor);
			builder.entity(distributorJson);
		} catch (DistributorDBException e) {
			LOG.error("DistributorException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		} catch (DistributorNotFoundException e) {
			LOG.error("DistributorNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity("No distributor exists with distributorId " + distributorId).build();
		} catch (Exception e) {
			LOG.error("Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		} 
		Response response = builder.build();
		return response;
	}

	/**
	 * Create new distributor api.
	 * @param operatorId ID of Operator
	 * @param request CreateDistributorRequest
	 * @return 
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response createNewDistributor(@PathParam("operatorId") @ValidOperator final String operatorId, @Valid CreateDistributorRequest request) {
		LOG.info("Received request to create distributor under operator {}", operatorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			Distributor distributor = distributorDAO.createNewDistributor(Long.valueOf(operatorId), request);
			LOG.debug("Distributor created {} with details {}", mapper.writeValueAsString(distributor), operatorId);
			LOG.info("Distributor created successfully for operator {} with distributorId {}", operatorId, distributor.getDistributorId());
			String distributorJson = mapper.writeValueAsString(distributor);
			builder.entity(distributorJson);
		} catch (DistributorException e) {
			LOG.error("DistributorException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while creating distributor").build();
		} catch (IOException e) {
			LOG.error("IOException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Distributor created but failed to read it. "
					+ "Please check list of distributors before creating new").build();
		} catch (DistributorDBException e) {
			LOG.error("Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Exception while saving distributor. DB error.").build();
		} catch (Exception e) {
			LOG.error("Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
		Response response = builder.build();
		return response;
	}


	/**
	 * Update distributor api.
	 * @param operatorId
	 * @param distributorId
	 * @param request
	 * @return 
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{distributorId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateDistributor (@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId,
			@Valid UpdateDistributorRequest request) {
		LOG.info("Received request to update distributor with distributorId {} under operator {}", distributorId, operatorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			Distributor distributor = distributorDAO.updateDistributor(Long.valueOf(operatorId),Long.valueOf(distributorId),  request);
			LOG.info("Successfully updated distributor with distributorId {} under operator {}", distributorId, operatorId);
			String distributorJson = mapper.writeValueAsString(distributor);
			builder.entity(distributorJson);
		} catch (DistributorNotFoundException e) {
			LOG.error("DistributorNotFoundException occured in updateDistributor:", e);
			return Response.status(Status.NOT_FOUND).entity("Unable to service your request. Distributor with id " + distributorId + " does not exists.").build();
		} catch (IOException e) {
			LOG.error("IOException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Distributor updated but failed to read it. "
					+ "Please check list of distributors before creating new").build();
		} catch (Exception e) {
			LOG.error("Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
		Response response = builder.build();
		return response;
	}


	/**
	 * Delete distributor api.
	 * @param operatorId
	 * @param distributorId
	 * @param userId
	 * @return 
	 */
	@DELETE
	@Path("/{distributorId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response deleteDistributor(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId,
			@QueryParam("userId") @Email(message = "{userId.email}") final String userId) {
		LOG.debug("Received request to delete distributor with distributorId: " + distributorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Date requestedDate = new Date();
		try {
			boolean success = distributorDAO.deleteDistributor( Long.valueOf(operatorId), Long.valueOf(distributorId), requestedDate, userId);
			LOG.debug("Distributor with distributorId: " + distributorId + " deleted successfully");
			String deleteJson = mapper.writeValueAsString(success);
			builder.entity(deleteJson);
		} catch (DistributorNotFoundException e) {
			LOG.error("DistributorNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("DeleteDistributor Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
		Response response = builder.build();
		return response;
	}


	@GET
	@Path("/{distributorId}/simpacks")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response getDistributorSimpacks(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId,
			@QueryParam ("used") String used) {

		LOG.info("Received request to get all simpacks for distributor {} ", distributorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			List<Simpack> simpacks = distributorDAO.getSimpacks(Long.valueOf(operatorId), Long.valueOf(distributorId), used);
			String simpack = mapper.writeValueAsString(simpacks);
			LOG.debug("simpacks fetched {} for distributorId {}",simpack, distributorId);
			return builder.entity(simpack).build();
		} catch (Exception e) {
			LOG.error("GetDistributorSimpacks Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}
	}


	/**
    /**
	 * Assign SIM to an account. This will be used to block and assign SIMs for distributor.
	 * @param operatorId
	 * @param accountId
	 * @param request
	 * @return 
	 */
	@PUT
	@Path("/distributors/{distributorId}/simpacks")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response assignIccidsToDistributor(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId,
			UpdateEmptySIMPACKRequest request) {
		LOG.info("Received request to assign sims to distributorId : " + distributorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			boolean success = distributorDAO.addSimpacksToDistributor(Long.valueOf(operatorId),Long.valueOf(distributorId), request);
			ObjectMapper mapper = new ObjectMapper();
			String assignSIMsToAccountJson = mapper.writeValueAsString(success);
			builder.entity(assignSIMsToAccountJson);
			LOG.debug("Update assignIccidsToDistributor Response: " + assignSIMsToAccountJson);
		} catch (DistributorNotFoundException e) {
			LOG.error("DistributorNotFoundException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (DistributorException e) {
			LOG.error("DistributorException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("assignSIMsToAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error occurred.").build();
		}
		Response response = builder.build();
		LOG.debug("Sent response from assignIccidsToDistributor: " + response);
		return response;
	}


	/** Create Empty SIMPacks for distributor
	 * @param operatorId
	 * @param distributorId
	 * @param request
	 * @return 
	 */
	@POST
	@Path("/{distributorId}/simpacks")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response createSimpackForDistributor(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId,
			@Valid CreateDistributorCode request) {

		LOG.info("Received request to create simpack under operator {} with distributorId {} ", operatorId, distributorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		try {
			List<String> distributorCode = distributorDAO.createEmptySimpacks4Distributor(Long.valueOf(operatorId), Long.valueOf(distributorId), request);
			String distributorJson = mapper.writeValueAsString(distributorCode);
			builder.entity(distributorJson);
			LOG.debug("Sent response from createNewDistributor: " + distributorJson);
		} catch (DistributorNotFoundException e) {
			LOG.error("DistributorNotFoundException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("CreateNewDistributor Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewDistributor: " + response);
		return response;
	}

	@PUT
	@Path("/{distributorId}/simpacks")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateEmptySimpacksOfDistributor(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId,
			@Valid UpdateEmptySIMPACKRequest request) {
		LOG.info("Received request to update empty simpack of distributor : " + distributorId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Response response = builder.build();
		// TODO
		LOG.debug("Sent response from createNewDistributor: " + response);
		return response;
	}

}
