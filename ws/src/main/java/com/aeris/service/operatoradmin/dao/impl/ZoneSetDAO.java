package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.dao.IZoneSetDAO;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneSetException;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetNotFoundException;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZoneSet;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.inject.Inject;

public class ZoneSetDAO implements IZoneSetDAO{
	private static Logger LOG = LoggerFactory.getLogger(ZoneSetDAO.class);
	
	@Inject
	IZoneDAO zoneDao ;
	
	private Cache<Integer, ZoneSet> cache;
	
	private static final String SQL_GET_ZONE_SET_COUNT = "select count(*) as cnt " +
																	"from " +
																	"ZONE_SET where " +
																	"OPERATOR_ID = ? and " +
																	"PRODUCT_ID = ? and " +
																	"ZONE_SET_NAME = ? and " +
																	"ZONE_SET_ID != ?";
	
	private static final String SQL_CREATE_NEW_ZONE_SET = 	"INSERT INTO ZONE_SET " ;
	
	private static final String COLUMNS_FOR_INSERT_ZONE_SET = "ZONE_SET_ID, ZONE_SET_NAME, OPERATOR_ID, PRODUCT_ID,CREATED_BY, CREATED_DATE, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, REP_TIMESTAMP, RES  ";
	
	private static final String VALUES_FOR_INSERT_ZONE_SET	= "?,?,?,?,?,?,?,?,?,?";
	
	private static final String SQL_UPDATE_ZONE_SET = 	"UPDATE ZONE_SET SET "+
														"ZONE_SET_NAME = ?, OPERATOR_ID = ?, PRODUCT_ID = ?, ACCOUNT_ID = ?, LAST_MODIFIED_BY = ?, LAST_MODIFIED_DATE =? "+
														"WHERE OPERATOR_ID = ? AND ZONE_SET_ID = ?";
	
	private static final String SQL_DELETE_ZONE_SET = "DELETE FROM ZONE_SET WHERE OPERATOR_ID = ? AND ZONE_SET_ID = ?";
	
	private static final String SQL_GET_ZONE_SET = 	"SELECT zs.ZONE_SET_ID, zs.ZONE_SET_NAME, zs.OPERATOR_ID, zs.PRODUCT_ID, zs.ACCOUNT_ID, zs.RES, rm.RES_NAME "+
													"FROM ZONE_SET zs left join RES_METADATA rm on zs.RES = rm.RES_ID WHERE zs.OPERATOR_ID = ? AND zs.ZONE_SET_ID = ?";
	
	private static final String SQL_GET_ZONE_SETS = "SELECT zs.ZONE_SET_ID, zs.ZONE_SET_NAME, zs.OPERATOR_ID, zs.PRODUCT_ID, zs.ACCOUNT_ID, zs.RES, rm.RES_NAME "+
													"FROM ZONE_SET zs left join RES_METADATA rm on zs.RES = rm.RES_ID WHERE zs.OPERATOR_ID = ? AND zs.PRODUCT_ID = ?";
	
	private static final String SQL_GET_ZONE_SETS_BY_ACCOUNT_ID = "SELECT zs.ZONE_SET_ID, zs.ZONE_SET_NAME, zs.OPERATOR_ID, zs.PRODUCT_ID, zs.ACCOUNT_ID, zs.RES , rm.RES_NAME "+
																  " from ZONE_SET zs left join RES_METADATA rm on zs.RES = rm.RES_ID WHERE OPERATOR_ID = ? AND PRODUCT_ID = ? AND ACCOUNT_ID = ?";

	private static final String SQL_GET_RETAIL_ZONE_SETS = "SELECT zs.ZONE_SET_ID, zs.ZONE_SET_NAME, zs.OPERATOR_ID, zs.PRODUCT_ID, zs.ACCOUNT_ID, zs.RES , rm.RES_NAME "+
			  " from ZONE_SET zs left join RES_METADATA rm on zs.RES = rm.RES_ID WHERE OPERATOR_ID = ? AND PRODUCT_ID IN (?) AND (ACCOUNT_ID IS NULL OR ACCOUNT_ID IN (?))";

	private static final String  SQL_GET_ALL_ZONE_SETS_BY_OPEARTOR_ID = "SELECT ZONE_SET_ID, ZONE_SET_NAME, OPERATOR_ID, PRODUCT_ID, ACCOUNT_ID "+
			  															"FROM ZONE_SET WHERE OPERATOR_ID = ? ";
	
	private static final String  SQL_GET_ALL_ZONE_SETS = "SELECT zs.ZONE_SET_ID, zs.ZONE_SET_NAME, zs.OPERATOR_ID, zs.PRODUCT_ID, zs.ACCOUNT_ID, zs.RES , rm.RES_NAME "+
																		"FROM ZONE_SET zs left join RES_METADATA rm on zs.RES = rm.RES_ID";
	
	private static final String SQL_GET_NEXT_ZONE_SET_ID = "select ZONE_SET_SEQ.NEXTVAL as ID from dual";
	
	private static final String SQL_DELETE_ZONES_OF_ZONESET = "delete from roaming_zones where zone_set_id = ?";
	
	public void init(@Hazelcast(cache = "ZoneSetCache/id") Cache<Integer, ZoneSet> cache) {
		this.cache = cache;
		List<ZoneSet> zoneSetList = null;
		try {
			zoneSetList = getAllZoneSets(0);
		} catch (ZoneSetException e) {
			LOG.error("Exception occured during initialization of ZoneSetCache :" + e);
		}
		ZoneSet[] zoneSets = new ZoneSet[zoneSetList.size()];
		zoneSetList.toArray(zoneSets);
		
		putInCache(zoneSets);
	} 
	 
	
	@Override
	public ZoneSet createNewZoneSet(int operatorId, int productId,int accountId, String zoneSetName, Date requestedDate,String requestedUser, List<Zone> zones, int RESId)throws DuplicateZoneSetException, ZoneSetException {
		LOG.info("createNewZoneSet: processing create zoneSet...");
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ZoneSet zoneSet = new ZoneSet();
		int newZoneSetID = 0;
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Auto Commit off
			conn.setAutoCommit(false);
			
			// Check if ZoneSet Name already Exists
			boolean zoneSetExists = validateZoneSetName(operatorId, productId, zoneSetName, 0, conn);
			
			if(zoneSetExists){
				throw new DuplicateZoneSetException("ZoneSet Name already Exists: "+zoneSetName);
			}
			
			StringBuilder sql = new StringBuilder();
			if(accountId > 0){
				sql.append(SQL_CREATE_NEW_ZONE_SET);
				sql.append("(");
				sql.append(COLUMNS_FOR_INSERT_ZONE_SET);
				sql.append(", ACCOUNT_ID) ");
				sql.append("VALUES(");
				sql.append(VALUES_FOR_INSERT_ZONE_SET);
				sql.append(",?)");
			}else{
				sql.append(SQL_CREATE_NEW_ZONE_SET);
				sql.append("(");
				sql.append(COLUMNS_FOR_INSERT_ZONE_SET);
				sql.append(") ");
				sql.append("VALUES(");
				sql.append(VALUES_FOR_INSERT_ZONE_SET);
				sql.append(")");
			}
			
			newZoneSetID = getNextZoneSetId(conn);
			
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_CREATE_NEW_ZONE_SET :"+sql.toString()+" (ZoneSetID="+newZoneSetID+",zoneSetName="+zoneSetName+", operatorId="+operatorId+", productId="+productId+", accountId="+accountId+
						", createdBy="+requestedUser+", createdOn="+requestedDate+")");
			}
			
			ps = conn.prepareStatement(sql.toString());
			ps.setInt(1, newZoneSetID);
			ps.setString(2, zoneSetName);
			ps.setInt(3, operatorId);
			ps.setInt(4, productId);
			ps.setString(5, requestedUser);
			ps.setDate(6, new java.sql.Date(requestedDate.getTime()));
			ps.setString(7, requestedUser);
			ps.setDate(8, new java.sql.Date(requestedDate.getTime()));
			ps.setTimestamp(9, new Timestamp(requestedDate.getTime()));
			if(RESId > 0){
				ps.setString(10, String.valueOf(RESId));
			}else{
				ps.setNull(10, Types.VARCHAR);
			}
			
            if (accountId > 0) {
                ps.setInt(11, accountId);
            } 
			ps.executeUpdate();
			
			zoneSet.setZoneSetId(newZoneSetID);
			zoneSet.setZoneSetName(zoneSetName);
			zoneSet.setProductId(productId);
			if (accountId > 0) {
                zoneSet.setAccountId(accountId);
            }
			zoneSet.setOperatorId(operatorId);
			
			//IF zoneList is not null, create zones
			List<Zone> createdZones = null;
			if(zones != null && zones.size() > 0){
				createdZones = zoneDao.createZones(conn, zones, newZoneSetID, operatorId, productId, requestedDate, requestedUser);
			}
			
			
			if(conn!=null){
				conn.commit();
			}
			// Put the created Zones in the ZoneCache
			if(createdZones != null){
				for(Zone zone : createdZones){
					zoneDao.putInCache(zone);
				}
			}
			
		}catch (DuplicateZoneSetException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		}catch (ZoneException e){
			LOG.error("Exception during creating zones inside zoneSet:" + e);
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			throw new ZoneSetException("Exception during creating zones inside zoneSet: "+e);
		}catch (DuplicateZoneException e){
			LOG.error("Exception during creating zones inside zoneSet:" + e);
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			throw new ZoneSetException("Exception during creating zones inside zoneSet: "+e);
		}catch (SQLException sqle) {
			LOG.error("Exception during creating zoneSet :" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneSetException("Exception in createNewZoneSet - DB Error during creating zoneSet", sqle);
		}catch (Exception e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneSetException("Create ZoneSet failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		putInCache(zoneSet);
		
		return zoneSet;
	}
	
	@Override
	public ZoneSet createNewZoneSet(int operatorId, int productId, int accountId, String zoneSetName, Date requestedDate, String requestedUser, List<Zone> zones) throws DuplicateZoneSetException, ZoneSetException {
		return createNewZoneSet(operatorId, productId, accountId, zoneSetName, requestedDate, requestedUser, zones, 0);
	}
	
	@Override
	public ZoneSet updateZoneSet(int zoneSetId, int operatorId, int productId, int accountId, String zoneSetName,Date requestedDate,String requestedUser) throws DuplicateZoneSetException,ZoneSetNotFoundException,ZoneSetException {
		LOG.info("updateZoneSet: processing update zoneSet (id:"+zoneSetId+")...");
		Connection conn = null;
		PreparedStatement ps = null;
		ZoneSet zoneSet = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Auto Commit off
			conn.setAutoCommit(false);
			
			// Check if ZoneSet Name already Exists for some other ZoneSet
			boolean zoneSetExists = validateZoneSetName(operatorId, productId, zoneSetName, zoneSetId, conn);
			
			if(zoneSetExists){
				throw new DuplicateZoneSetException("ZoneSet Name already Exists: "+zoneSetName);
			}
			
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_UPDATE_ZONE_SET :"+SQL_UPDATE_ZONE_SET+" (zoneSetName="+zoneSetName+", operatorId="+operatorId+", productId="+productId+", accountId="+accountId+
						", lastModifiedBy="+requestedUser+", lastModifiedOn="+requestedDate+", zoneSetId"+zoneSetId+")");
			}
			ps = conn.prepareStatement(SQL_UPDATE_ZONE_SET);
			ps.setString(1, zoneSetName);
			ps.setInt(2, operatorId);
			ps.setInt(3, productId);
            if (accountId > 0) {
                ps.setInt(4, accountId);
            } else {
                ps.setNull(4, java.sql.Types.INTEGER);
            }
			ps.setString(5, requestedUser);
			ps.setDate(6, new java.sql.Date(requestedDate.getTime()));
			ps.setInt(7, operatorId);
			ps.setInt(8, zoneSetId);
			
			int updatedCount = ps.executeUpdate();
			if(updatedCount == 0){
				LOG.error("ZoneSet does not exist for zoneSet id: " + zoneSetId);
				throw new ZoneSetNotFoundException("ZoneSet does not exist for zoneSet id: " + zoneSetId);
			}
			
			// Commit the changes
			if(conn != null){
				conn.commit();
			}	
			
		} catch (ZoneSetNotFoundException e) {
			LOG.error("ZoneSetNotFoundException occured while updating zoneSet" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (DuplicateZoneSetException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (SQLException sqle) {
			LOG.error("Exception during updating zoneSet - db error" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneSetException("Exception during updateZoneSet - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during updating zoneSet - fatal error" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneSetException("Updating ZoneSet failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, null);
		}
		
		zoneSet = new ZoneSet();
		zoneSet.setZoneSetId(zoneSetId);
		zoneSet.setZoneSetName(zoneSetName);
		zoneSet.setProductId(productId);
        if (accountId > 0) {
            zoneSet.setAccountId(accountId);
        }
		zoneSet.setOperatorId(operatorId);
		zoneSet.setUserId(requestedUser);
		
		putInCache(zoneSet);
		return zoneSet;
	}
	
	
	private boolean validateZoneSetName(int operatorId, int productId, String zoneSetName, int zoneSetId, Connection conn){		
		LOG.info("Checking if ZoneSetName "+zoneSetName+" already exists...");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		boolean zoneSetExists = false;
		
		try
		{	
			ps = conn.prepareStatement(SQL_GET_ZONE_SET_COUNT);
			ps.setInt(1, operatorId);
			ps.setInt(2, productId);
			ps.setString(3, zoneSetName);
			ps.setLong(4, zoneSetId);
			
			if(LOG.isDebugEnabled())
				LOG.debug("SQL_GET_ZONE_SET_COUNT : "+SQL_GET_ZONE_SET_COUNT+"  (operatorId="+operatorId+", productId="+productId+", zoneSetName="+zoneSetName+", zoneSetId="+zoneSetId+")");
			
			rs = ps.executeQuery();

			if (rs.next()) {
				zoneSetExists = (rs.getInt("cnt") > 0);
			}
		} catch (SQLException e) {
			LOG.error("Exception during validateZoneSetName", e);
			throw new GenericServiceException("Exception during validateZoneSetName", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		
		LOG.info("ZoneSet Name already exists ? : "+zoneSetExists);		
		
		return zoneSetExists;
	}
	
	@Override
	public boolean deleteZoneSet(int operatorId, int zoneSetId,
			Date requestedDate, String requestedUser) throws ZoneSetException, ZoneSetNotFoundException {
		Connection conn = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;

		boolean success = false;

		LOG.info("deleteZoneSet: processing delete zoneSet (id:" + zoneSetId+ ")...");

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			// Auto commit off
			conn.setAutoCommit(false);
			
			// First delete Zones associated with the given zoneSet
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_DELETE_ZONES_OF_ZONESET :"+SQL_DELETE_ZONES_OF_ZONESET+"(zoneSetId="+zoneSetId+")");
			}
			ps1 = conn.prepareStatement(SQL_DELETE_ZONES_OF_ZONESET);
			ps1.setInt(1, zoneSetId);
			int deleteZonesCount = ps1.executeUpdate();
			
			LOG.info(deleteZonesCount+" zones deleted under zoneSetId "+zoneSetId);
			
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_DELETE_ZONE_SET :"+SQL_DELETE_ZONE_SET+" (operatorId="+operatorId+", zoneSetId="+zoneSetId+")");
			}
			
			// Now delete given zoneSet
			ps = conn.prepareStatement(SQL_DELETE_ZONE_SET);
			ps.setInt(1, operatorId);
			ps.setInt(2, zoneSetId);
			
			int deleteCount = ps.executeUpdate();
			if (deleteCount == 0) {
				LOG.error("ZoneSet does not exist for zoneSet id: " + zoneSetId);
				throw new ZoneSetNotFoundException("ZoneSet does not exist for zoneSet id: " + zoneSetId);
			}
			
			// Commit the changes
			if(conn != null){
				conn.commit();
			}
			
			// Mark delete as successful
			success = true;
			
		} catch (ZoneSetNotFoundException e) {
			LOG.error("ZoneSetNotFoundException occured while deleting zoneSet"	+ e);
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception "
							+ e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting zoneSet - db error" + sqle);
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception "
							+ sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new ZoneSetException("Exception during deleting zoneSet - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during deleting zoneSet  - fatal error" + e);
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception "
							+ e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			throw new ZoneSetException("Deleting ZoneSet failed due to fatal error:" + e);
		} finally {
			DBUtils.cleanup(null, ps1);
			DBUtils.cleanup(conn, ps);
			
		}
		cache.evict(zoneSetId);
		
		return success;
	}
	
	@Override
	public ZoneSet getZoneSet(int operatorId, int zoneSetId) throws ZoneSetException,ZoneSetNotFoundException {
		LOG.info("getZoneSet : processing getZoneSet (id:"+zoneSetId+") ...");
		
		ZoneSet zoneSet = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_GET_ZONE_SET :"+SQL_GET_ZONE_SET+" (operatorId="+operatorId+", zoneSetId="+zoneSetId+")");
			}
			
			ps = conn.prepareStatement(SQL_GET_ZONE_SET);
			ps.setInt(1, operatorId);
			ps.setInt(2, zoneSetId);
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				zoneSet = new ZoneSet();
				zoneSet.setZoneSetId(rs.getInt("ZONE_SET_ID"));
				zoneSet.setZoneSetName(rs.getString("ZONE_SET_NAME"));
				zoneSet.setOperatorId(rs.getInt("OPERATOR_ID"));
				zoneSet.setProductId(rs.getInt("PRODUCT_ID"));
				zoneSet.setAccountId(rs.getInt("ACCOUNT_ID"));
				zoneSet.setResName(rs.getString("RES_NAME"));
				String strRESId = rs.getString("RES");
				if(strRESId != null && !strRESId.isEmpty()){
					zoneSet.setResId(Integer.parseInt(strRESId));
				}
				
			}else{
				LOG.error("ZoneSet does not exist for zoneSet id: " + zoneSetId);
				throw new ZoneSetNotFoundException("ZoneSet does not exist for zoneSet id: " + zoneSetId);
			}
			
		}catch (SQLException sqle) {
			LOG.error("Exception during getting zoneSet - db error" + sqle);

			throw new ZoneSetException("Exception during getZone - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during getting zoneSet - fatal error" + e);
			
			throw new ZoneSetException("Get ZoneSet failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("Returned zoneSet to the caller: "+zoneSet);
		
		return zoneSet;
	}
	
	@Override
	public List<ZoneSet> getZoneSets(final int operatorId, final int productId)	throws ZoneSetException {
		LOG.info("getZoneSets: processing getZoneSets (operatorId="+operatorId+", productId="+productId);
		
		
		// If already loaded, fetch from cache
		if(!cache.getValues().isEmpty())
		{
			return new ArrayList<ZoneSet>(Collections2.filter(cache.getValues(), new Predicate<ZoneSet>() {
				@Override
				public boolean apply(ZoneSet input) {
					boolean matching = ((operatorId == input.getOperatorId()) && (productId == input.getProductId()));
					if(matching)
						LOG.info("Returning ZoneSet (id:"+input.getZoneSetId()+" )from Cache");
					return matching;
				}
				
			}));
		}
		
		List<ZoneSet> zoneSets = new ArrayList<ZoneSet>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_GET_ZONE_SETS :"+SQL_GET_ZONE_SETS+" (operatorId="+operatorId+", productId="+productId+")");
			}
			
			ps = conn.prepareStatement(SQL_GET_ZONE_SETS);
			ps.setInt(1, operatorId);
			ps.setInt(2, productId);
			
			rs = ps.executeQuery();
			
			ZoneSet zoneSet = null;
			while(rs.next()){
				zoneSet = new ZoneSet();
				zoneSet.setZoneSetId(rs.getInt("ZONE_SET_ID"));
				zoneSet.setZoneSetName(rs.getString("ZONE_SET_NAME"));
				zoneSet.setOperatorId(rs.getInt("OPERATOR_ID"));
				zoneSet.setProductId(rs.getInt("PRODUCT_ID"));
				zoneSet.setAccountId(rs.getInt("ACCOUNT_ID"));
				zoneSet.setResName(rs.getString("RES_NAME"));
				String strRESId = rs.getString("RES");
				if(strRESId != null && !strRESId.isEmpty()){
					zoneSet.setResId(Integer.parseInt(strRESId));
				}
				
				zoneSets.add(zoneSet);
			}
			
		}catch (SQLException sqle) {
			LOG.error("Exception during getZoneSets - db error" + sqle);
			
			throw new ZoneSetException("Exception during getZoneSets - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during getZoneSets  - fatal error" + e);
			
			throw new ZoneSetException("getZoneSets failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getZoneSets: Returned " + zoneSets.size() + " zoneSets to the caller");
		
		return zoneSets;
	}
	
	@Override
	public List<ZoneSet> getZoneSets(final int operatorId, final int productId, final int accountId) throws ZoneSetException {
		LOG.info("getZoneSets: processing getZoneSets (operatorId="+operatorId+", productId="+productId+", accountId="+accountId);
		
		// If already loaded, fetch from cache
		if(!cache.getValues().isEmpty())
		{
			return new ArrayList<ZoneSet>(Collections2.filter(cache.getValues(), new Predicate<ZoneSet>() {
				@Override
				public boolean apply(ZoneSet input) {
					if(accountId > 0){
						boolean matching = ((operatorId == input.getOperatorId()) && (productId == input.getProductId()) && (accountId == input.getAccountId()));
						if(matching)
							LOG.info("Returning ZoneSet (id:"+input.getZoneSetId()+" ) from Cache");
						return matching;
					}else{
						boolean matching = ((operatorId == input.getOperatorId()) && (productId == input.getProductId()));
						if(matching)
							LOG.info("Returning ZoneSet (id:"+input.getZoneSetId()+" ) from Cache");
						return matching;
					}
					
				}
				
			}));
		}
		
		
		List<ZoneSet> zoneSets = new ArrayList<ZoneSet>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_GET_ZONE_SETS_BY_ACCOUNT_ID :"+SQL_GET_ZONE_SETS_BY_ACCOUNT_ID+" (operatorId="+operatorId+", productId="+productId+", accountId="+accountId+")");
			}
			
			ps = conn.prepareStatement(SQL_GET_ZONE_SETS_BY_ACCOUNT_ID);
			ps.setInt(1, operatorId);
			ps.setInt(2, productId);
			ps.setInt(3, accountId);
			
			rs = ps.executeQuery();
			
			ZoneSet zoneSet = null;
			while(rs.next()){
				zoneSet = new ZoneSet();
				zoneSet.setZoneSetId(rs.getInt("ZONE_SET_ID"));
				zoneSet.setZoneSetName(rs.getString("ZONE_SET_NAME"));
				zoneSet.setOperatorId(rs.getInt("OPERATOR_ID"));
				zoneSet.setProductId(rs.getInt("PRODUCT_ID"));
				zoneSet.setAccountId(rs.getInt("ACCOUNT_ID"));
				zoneSet.setResName(rs.getString("RES_NAME"));
				String strRESId = rs.getString("RES");
				if(strRESId != null && !strRESId.isEmpty()){
					zoneSet.setResId(Integer.parseInt(strRESId));
				}
				
				zoneSets.add(zoneSet);
			}
			
		}catch (SQLException sqle) {
			LOG.error("Exception during getZoneSets - db error" + sqle);
			
			throw new ZoneSetException("Exception during getZoneSets - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during getZoneSets  - fatal error" + e);
			
			throw new ZoneSetException("getZoneSets failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getZoneSets: Returned " + zoneSets.size() + " zoneSets to the caller");
		
		return zoneSets;
	}
	@Override
	public List<ZoneSet> getZoneSets(final int operatorId, final List<String> productIdList, final List<String>  accountIdList) throws ZoneSetException {
		LOG.info("getZoneSets: processing getZoneSets (operatorId="+operatorId+", productId="+productIdList+", accountId="+accountIdList);
		
		// If already loaded, fetch from cache
		if(!cache.getValues().isEmpty())
		{
			return new ArrayList<ZoneSet>(Collections2.filter(cache.getValues(), new Predicate<ZoneSet>() {
				@Override
				public boolean apply(ZoneSet input) {
					if(accountIdList.size() > 0 && productIdList.size() > 0){
						boolean matching = ((operatorId == input.getOperatorId()) && (productIdList.contains(String.valueOf(input.getProductId()))) && (input.getAccountId() == 0 || accountIdList.contains(String.valueOf(input.getAccountId()))));
						if(matching)
							LOG.info("Returning ZoneSet (id:"+input.getZoneSetId()+" ) from Cache");
						return matching;
					}else if(productIdList.size() > 0){
						boolean matching = ((operatorId == input.getOperatorId()) && (productIdList.contains(String.valueOf(input.getProductId()))) && input.getAccountId() == 0);
						if(matching)
							LOG.info("Returning ZoneSet (id:"+input.getZoneSetId()+" ) from Cache");
						return matching;
					}else{
						return false;
					}
					
				}
				
			}));
		}
		
		
		List<ZoneSet> zoneSets = new ArrayList<ZoneSet>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_GET_ZONE_SETS_BY_ACCOUNT_ID :"+SQL_GET_RETAIL_ZONE_SETS+" (operatorId="+operatorId+", productId="+productIdList.toString()+", accountId="+accountIdList.toString()+")");
			}
			
			ps = conn.prepareStatement(SQL_GET_RETAIL_ZONE_SETS);
			ps.setInt(1, operatorId);
			ps.setString(2, StringUtils.join(productIdList, ','));
			ps.setString(3, StringUtils.join(accountIdList, ','));
			
			rs = ps.executeQuery();
			
			ZoneSet zoneSet = null;
			while(rs.next()){
				zoneSet = new ZoneSet();
				zoneSet.setZoneSetId(rs.getInt("ZONE_SET_ID"));
				zoneSet.setZoneSetName(rs.getString("ZONE_SET_NAME"));
				zoneSet.setOperatorId(rs.getInt("OPERATOR_ID"));
				zoneSet.setProductId(rs.getInt("PRODUCT_ID"));
				zoneSet.setAccountId(rs.getInt("ACCOUNT_ID"));
				zoneSet.setResName(rs.getString("RES_NAME"));
				String strRESId = rs.getString("RES");
				if(strRESId != null && !strRESId.isEmpty()){
					zoneSet.setResId(Integer.parseInt(strRESId));
				}
				
				zoneSets.add(zoneSet);
			}
			
		}catch (SQLException sqle) {
			LOG.error("Exception during getZoneSets - db error" + sqle);
			
			throw new ZoneSetException("Exception during getZoneSets - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during getZoneSets  - fatal error" + e);
			
			throw new ZoneSetException("getZoneSets failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getZoneSets: Returned " + zoneSets.size() + " zoneSets to the caller");
		
		return zoneSets;
	}
	
	@Override
	public List<ZoneSet> getAllZoneSets(int operatorId) throws ZoneSetException {
		LOG.info("getAllZoneSets: processing getAllZoneSets (operatorId="+operatorId);
		
		List<ZoneSet> zoneSets = new ArrayList<ZoneSet>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_GET_ALL_ZONE_SETS_BY_OPEARTOR_ID :"+SQL_GET_ALL_ZONE_SETS_BY_OPEARTOR_ID+" (operatorId="+operatorId+")");
			}
			
			if(operatorId > 0){
				ps = conn.prepareStatement(SQL_GET_ALL_ZONE_SETS_BY_OPEARTOR_ID);
				ps.setInt(1, operatorId);
			}else{
				ps = conn.prepareStatement(SQL_GET_ALL_ZONE_SETS);
			}
			
			rs = ps.executeQuery();
			
			ZoneSet zoneSet = null;
			while(rs.next()){
				zoneSet = new ZoneSet();
				zoneSet.setZoneSetId(rs.getInt("ZONE_SET_ID"));
				zoneSet.setZoneSetName(rs.getString("ZONE_SET_NAME"));
				zoneSet.setOperatorId(rs.getInt("OPERATOR_ID"));
				zoneSet.setProductId(rs.getInt("PRODUCT_ID"));
				zoneSet.setAccountId(rs.getInt("ACCOUNT_ID"));
				
				zoneSets.add(zoneSet);
			}
			
		}catch (SQLException sqle) {
			LOG.error("Exception during getAllZoneSets - db error" + sqle);
			
			throw new ZoneSetException("Exception during getAllZoneSets - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during getAllZoneSets  - fatal error" + e);
			
			throw new ZoneSetException("getAllZoneSets failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAllZoneSets: Returned " + zoneSets.size() + " zoneSets to the caller");
		
		return zoneSets;
	}
	
	private int getNextZoneSetId(Connection conn) throws ZoneSetException{
		int zoneSetId = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(SQL_GET_NEXT_ZONE_SET_ID);
			rs = ps.executeQuery();
			if(rs.next()){
				zoneSetId = rs.getInt("ID");
			}
		}catch(SQLException e){
			LOG.error("SQLException occured during getNextZoneSetId :"+e);
			throw new ZoneSetException("SQLException occured during getNextZoneSetId :"+e);
		}finally{
			DBUtils.cleanup(null, ps, rs);
		}
		
		return zoneSetId;
	}
	
	private void putInCache(ZoneSet... zoneSets) {
		for (ZoneSet zoneSet : zoneSets) {
			cache.put(zoneSet.getZoneSetId(), zoneSet);
		}
	}
}
