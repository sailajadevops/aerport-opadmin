package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SMPPConnection;
import com.google.inject.Inject;

public class GetSMPPConnectionsStoredProcedure extends AbstractProcedureCall<List<SMPPConnection>> {

	private static final String SQL_GET_SMPP_CONNECTIONS = "common_util_pkg.get_smpp_connection";

	@Inject
	public GetSMPPConnectionsStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_SMPP_CONNECTIONS, true);
		
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SMPPConnection> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<SMPPConnection> smppConnections = createSMPPConnectionList(cursor);

		return smppConnections;
	}

	private List<SMPPConnection> createSMPPConnectionList(List<Map<String, Object>> cursor) {
		List<SMPPConnection> smppConnections = new ArrayList<SMPPConnection>();

		for (Map<String, Object> row : cursor) {
			SMPPConnection smppConnection = new SMPPConnection();  

			String connectionId = (String) row.get("connection_id");

//			if (NumberUtils.isNumber(connectionId)) {
//				smppConnection.setSmppConnectionId(Integer.parseInt(connectionId));
//			}
//
//			String messageProtocol = (String) row.get("message_protocol");
//
//			if (StringUtils.isNotEmpty(messageProtocol)) {
//				smppConnection.setMessageProtocol(messageProtocol);
//			}
//			
//			String ipVersion = (String) row.get("ip_version");
//
//			if (NumberUtils.isNumber(ipVersion)) {
//				smppConnection.setIpVersion(Integer.parseInt(ipVersion));
//			}
//
//			String ipAddress = (String) row.get("ip_address");
//
//			if (StringUtils.isNotEmpty(ipAddress)) {
//				smppConnection.setIpAddress(ipAddress);
//			}
//			
//			String ipPort = (String) row.get("ip_port");
//
//			if (NumberUtils.isNumber(ipPort)) {
//				smppConnection.setIpPort(Integer.parseInt(ipPort));
//			}
//			
//			String systemId = (String) row.get("system_id");
//
//			if (StringUtils.isNotEmpty(systemId)) {
//				smppConnection.setSystemId(systemId);
//			}
//			
//			String password = (String) row.get("password");
//
//			if (StringUtils.isNotEmpty(password)) {
//				smppConnection.setPassword(password);
//			}
//			
//			String systemType = (String) row.get("system_type");
//
//			if (StringUtils.isNotEmpty(systemType)) {
//				smppConnection.setSystemType(systemType);
//			}
//			
//			String shortCode = (String) row.get("short_code");
//
//			if (NumberUtils.isNumber(shortCode)) {
//				smppConnection.setShortCode(Integer.parseInt(shortCode));
//			}
			
			smppConnections.add(smppConnection);
		}

		return smppConnections;
	}
}
