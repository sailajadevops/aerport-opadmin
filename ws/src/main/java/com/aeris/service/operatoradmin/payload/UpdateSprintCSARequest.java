package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;
import java.util.Date;

public class UpdateSprintCSARequest implements Serializable  {
	private static final long serialVersionUID = -4659589854090199702L;
	
	private String newCSA;
	
	private String requestedUser;
	
	private Date requestedDate;
	
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	
	public Date getRequestedDate() {
		return requestedDate;
	}
	
	public void setRequestedUser(String requestedUser) {
		this.requestedUser = requestedUser;
	}
	
	public String getRequestedUser() {
		return requestedUser;
	}
	
	public void setNewCSA(String newCSA) {
		this.newCSA = newCSA;
	}
	
	public String getNewCSA() {
		return newCSA;
	}
}
