package com.aeris.service.operatoradmin.dao.storedproc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.internal.OracleTypes;

import org.apache.commons.lang.StringUtils;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.utils.DBUtils;

/**
 * Abstract jdbc stored procedure calls to the oracle db.
 * 
 * Registers IN and OUT Parameters with the jdbc callable statement.
 * Dynamically constructs the query and sets the values to the stored proc
 * and returns the output to the caller in a prepopulated Object
 * 
 * The Stored proc classes will override this by setting the names and types of the stored proc 
 * params and invoking the execute method
 * 
 * @author Srinivas Puranam
 */
public abstract class AbstractProcedureCall<T extends Object> implements IStoredProcedure<T> {
	private String procedureName;
	private String dataSource;
	private boolean function;

	private List<AbstractParameter> parameters = new LinkedList<AbstractParameter>();

	public AbstractProcedureCall(String dataSource, String procedureName) {
		this(dataSource, procedureName, false);
	}
	
	public AbstractProcedureCall(String dataSource, String procedureName, boolean function) {
		this.dataSource = dataSource;
		this.procedureName = procedureName;
		setFunction(function);
	}

	protected final void registerParameter(AbstractParameter parameter) {
		if (parameter != null) {
			parameters.add(parameter);
		}
	}

	/* (non-Javadoc)
	 * @see com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure#execute(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T execute(Connection conn, Object... input) throws StoredProcedureException {
		CallableStatement statement = null;

		try {
			if (conn == null) {
				throw new StoredProcedureException("closed or no database connection");
			}

			statement = conn.prepareCall(!isFunction() ? buildProcedureName() : buildFunctionName());

			int count = 0;
			
			if(isFunction())
			{
				for (AbstractParameter param : parameters) {
					ParameterType type = param.getType();
					int sqlType = param.getSqlType();
					
					if(type == ParameterType.OUT_PARAM)
					{
						statement.registerOutParameter(1, sqlType);
					}
					else if (type == ParameterType.IN_PARAM)
					{
						statement.setObject(count+2, input[count], sqlType);
						count++;
					}
				}	
			}		
			else
			{
				for (AbstractParameter param : parameters) {
					ParameterType type = param.getType();
					String paramName = param.getName();
					int sqlType = param.getSqlType();

					if (type == ParameterType.IN_PARAM) {
						statement.setObject(paramName, input[count], sqlType);
						count++;
					}
					else
					{
						statement.registerOutParameter(paramName, sqlType);
					}
				}
			}

			statement.execute();

			Map<String, Object> outParams = new HashMap<String, Object>();
			
			if(isFunction())
			{
				for (AbstractParameter param : parameters) {
					ParameterType type = param.getType();
					int sqlType = param.getSqlType();

					if (type == ParameterType.OUT_PARAM) {
						String paramName = param.getName();
						
						Object output = statement.getObject(1);
						
						if(sqlType == OracleTypes.CURSOR || output instanceof ResultSet)
						{
							ResultSet rs = (ResultSet) output;
							List<Map<String, Object>> list = resultSetToArrayList(rs);
							outParams.put(paramName.toLowerCase(), list);
						}
						else
						{
							Object paramValue = statement.getString(paramName);
							
							if(paramValue != null)
							{
								outParams.put(paramName.toLowerCase(), paramValue);							
							}
						}
					}
				}
			}
			else
			{
				for (AbstractParameter param : parameters) {
					ParameterType type = param.getType();
					int sqlType = param.getSqlType();

					if (type == ParameterType.OUT_PARAM) {
						String paramName = param.getName();

						Object output = statement.getObject(paramName);
						
						if(sqlType == OracleTypes.CURSOR || output instanceof ResultSet)
						{
							ResultSet rs = (ResultSet) output;
							List<Map<String, Object>> list = resultSetToArrayList(rs);
							outParams.put(paramName.toLowerCase(), list);
						}
						else
						{
							Object paramValue = statement.getString(paramName);
							
							if(paramValue != null)
							{
								outParams.put(paramName.toLowerCase(), paramValue);							
							}
						}
					}
				}
			}
			
			return (T) outParams;

		} catch (SQLException e) {
			throw new StoredProcedureException("Exception calling function/procedure - "+statement+": "+e.getMessage(), e);
		} catch (Exception e) {
			throw new StoredProcedureException("Exception Occured: "+e.getMessage(), e);
		} 
	}

	/* (non-Javadoc)
	 * @see com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure#execute(java.lang.Object)
	 */
	@Override
	public T execute(Object... input) throws StoredProcedureException {
		CallableStatement statement = null;
		Connection conn = null;
		
		try {
			if(dataSource != null)
			{
				conn = DBConnectionManager.getInstance().getConnection(dataSource);
			}
			
			// Execute on connection
			T outParams = execute(conn, input);
			
			return outParams;
		} 
		finally {
			if(dataSource != null)
			{
				DBUtils.cleanup(conn, statement);
			}
		}
	}

	private String buildFunctionName() throws StoredProcedureException {
		StringBuffer buffer = new StringBuffer();

		if (procedureName == null) {
			throw new StoredProcedureException("function name cannot be null");
		}

		buffer.append("{ ? = call ");
		buffer.append(procedureName);

		List<String> inParams = new ArrayList<String>();
		
		if (parameters.size() > 0) {
			for (AbstractParameter parameter : parameters){
				if(parameter.getType() == ParameterType.IN_PARAM)
				{
					inParams.add("?");
				}
			}
		}
		
		if(inParams.size() > 0)
		{
			buffer.append("(");
			buffer.append(StringUtils.join(inParams, ", "));
			buffer.append(") ");
		}		

		buffer.append("}");
		
		return buffer.toString();
	}

	public List<Map<String, Object>> resultSetToArrayList(ResultSet rs) throws SQLException {
		ResultSetMetaData md = rs.getMetaData();
		int columns = md.getColumnCount();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		while (rs.next()) {
			Map<String, Object> row = new HashMap<String, Object>(columns);
			for (int i = 1; i <= columns; ++i) {
				int sqlType = md.getColumnType(i);
				
				Object val = null;
				
				if(sqlType == OracleTypes.CURSOR)
				{
					ResultSet resultSet = (ResultSet) rs.getObject(i);
					val = resultSetToArrayList(resultSet);
				}
				else
				{
					val = rs.getString(i);
				}
				
				if(val != null)
				{
					row.put(md.getColumnName(i).toLowerCase(), val);
				}
			}
			list.add(row);
		}

		try
		{
			rs.close();
		}
		catch (Exception e) {
			//Ignore
		}
		
		return list;
	}

	private String buildProcedureName() throws StoredProcedureException {
		StringBuffer buffer = new StringBuffer();

		if (procedureName == null) {
			throw new StoredProcedureException("procedureName cannot be null");
		}

		buffer.append("BEGIN ");
		buffer.append(procedureName);

		if (parameters.size() > 0) {
			buffer.append("(");

			for (int count = 0; count < parameters.size(); count++) {
				buffer.append("?");

				if (count < parameters.size() - 1) {
					buffer.append(",");
				}
			}

			buffer.append(");");
			buffer.append("END;");
		}

		return buffer.toString();
	}
	
	public boolean isFunction() {
		return function;
	}
	
	public void setFunction(boolean function) {
		this.function = function;
	}

	protected class InParameter extends AbstractParameter {
		public InParameter(String name, int sqlType) {
			setName(name);
			setSqlType(sqlType);
			setType(ParameterType.IN_PARAM);
		}
	}

	protected class OutParameter extends AbstractParameter {
		public OutParameter(String name, int sqlType) {
			setName(name);
			setSqlType(sqlType);
			setType(ParameterType.OUT_PARAM);
		}
	}

	protected abstract class AbstractParameter {
		private String name;
		private int sqlType;
		private ParameterType type;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getSqlType() {
			return sqlType;
		}

		public void setSqlType(int sqlType) {
			this.sqlType = sqlType;
		}

		public ParameterType getType() {
			return type;
		}

		public void setType(ParameterType type) {
			this.type = type;
		}
	}

	protected enum ParameterType {
		IN_PARAM, OUT_PARAM;
	}
}