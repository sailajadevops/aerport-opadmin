package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

/**
 * Distributor request mapping class with request validations
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
@XmlRootElement
public class CreateDistributorRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4736383041400399719L;
	@NotNull(message = "{accountId.notnull}")
	private long accountId;
	@NotNull(message = "{distributorId.notnull}")
	private long distributorId;
	@NotNull(message = "{distributorName.notnull}")
	private String distributorName;
	private List<String> contactEmails;
	private String logoUrls;
	private String privacyPolicyUrls;
	private int status;
    private Date lastModifiedDate;
    private Date createdDate;
    private String createdBy ;
    private String lastModifiedBy ;
    private Timestamp repTimestamp ;
    @NotNull(message = "{accessLink.notnull}")
    private String accessLink;
    @Pattern(regexp = "[0-9]+", message = "{liferayCompanyId.number}")
    private long liferayCompanyId;
    @NotNull(message = "{storeUrl.notnull}")
    private String storeUrl;
    private boolean isWholesale;
    @NotNull(message = "{brand.notnull}")
    private String brand;
    private String planPricingUrl;
    private String realm;
   
    @Email(message = "{userId.email}")
	private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(long distributorId) {
		this.distributorId = distributorId;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public List<String> getContactEmails() {
		return contactEmails;
	}

	public void setContactEmails(List<String> contactEmails) {
		this.contactEmails = contactEmails;
	}

	public String getLogoUrls() {
		return logoUrls;
	}

	public void setLogoUrls(String logoUrls) {
		this.logoUrls = logoUrls;
	}

	public String getPrivacyPolicyUrls() {
		return privacyPolicyUrls;
	}

	public void setPrivacyPolicyUrls(String privacyPolicyUrls) {
		this.privacyPolicyUrls = privacyPolicyUrls;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Timestamp getRepTimestamp() {
		return repTimestamp;
	}

	public void setRepTimestamp(Timestamp repTimestamp) {
		this.repTimestamp = repTimestamp;
	}

    public String getAccessLink() {
        return accessLink;
    }

    public void setAccessLink(String accessLink) {
        this.accessLink = accessLink;
    }

    public long getLiferayCompanyId() {
        return liferayCompanyId;
    }

    public void setLiferayCompanyId(long liferayCompanyId) {
        this.liferayCompanyId = liferayCompanyId;
    }

    public String getStoreUrl() {
        return storeUrl;
    }

    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

	public boolean isWholesale() {
		return isWholesale;
	}

	public void setWholesale(boolean isWholesale) {
		this.isWholesale = isWholesale;
	}
	 
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getPlanPricingUrl() {
		return planPricingUrl;
	}

	public void setPlanPricingUrl(String planPricingUrl) {
		this.planPricingUrl = planPricingUrl;
	}
	    

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public CreateDistributorRequest(long accountId, long distributorId,
			String distributorName, List<String> contactEmails,
			String logoUrls, String privacyPolicyUrls, int status,
			Date lastModifiedDate, Date createdDate, String createdBy,
			String lastModifiedBy, Timestamp repTimestamp, String accessLink,
			long liferayCompanyId, String storeUrl, boolean isWholesale,
			String brand, String planPricingUrl, String realm, String userId) {
		super();
		this.accountId = accountId;
		this.distributorId = distributorId;
		this.distributorName = distributorName;
		this.contactEmails = contactEmails;
		this.logoUrls = logoUrls;
		this.privacyPolicyUrls = privacyPolicyUrls;
		this.status = status;
		this.lastModifiedDate = lastModifiedDate;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.lastModifiedBy = lastModifiedBy;
		this.repTimestamp = repTimestamp;
		this.accessLink = accessLink;
		this.liferayCompanyId = liferayCompanyId;
		this.storeUrl = storeUrl;
		this.isWholesale = isWholesale;
		this.brand = brand;
		this.planPricingUrl = planPricingUrl;
		this.realm = realm;
		this.userId = userId;
	}
	
	public CreateDistributorRequest(){
		super();
	}
}
