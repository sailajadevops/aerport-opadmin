package com.aeris.service.operatoradmin.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.auth.AuthContext;
import com.aeris.service.operatoradmin.auth.Authenticator;
import com.aeris.service.operatoradmin.model.ApplicationProperties;
import com.aeris.service.operatoradmin.model.Role;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class OperatorAdminFilter implements ContainerRequestFilter {
	private static final Logger LOG = LoggerFactory.getLogger(OperatorAdminFilter.class);
	
	 @Inject
	 private ApplicationProperties applicationProperties;
	 
	 @Inject
	 private Authenticator authenticator;
	 
	@Override
	public void filter(ContainerRequestContext rc) throws IOException { 
		
		MultivaluedMap<String, String> headers = rc.getHeaders();
		
		// Put JSON as default response type
		headers.putSingle("Accept", MediaType.APPLICATION_JSON);
		
		
		MultivaluedMap<String, String> requestParams = rc.getUriInfo().getQueryParameters();
		MultivaluedMap<String, String> pathParams = rc.getUriInfo().getPathParameters();
		
		Long operatorId = pathParams.getFirst("operatorId") != null ? Long.valueOf(pathParams.getFirst("operatorId")) : -1;  
		String apiKey = requestParams.getFirst("apiKey");
		String resourceName = rc.getUriInfo().getPath();
		boolean authenticateRequested = false ;
		if(BooleanUtils.toBoolean(requestParams.getFirst("authenticate"))) {
			authenticateRequested = true ;
		}
		//If the request has request for apiKey auth filter, then ignore the auth-config settings and proceed with apiKey authentication
		if(!authenticateRequested) {
			if(!BooleanUtils.toBoolean(applicationProperties.getEnableAuth())) {
				LOG.info("Operator-Admin authorization filter disabled, Request authorized, Invoking web-service");
				rc.setSecurityContext(new AuthContext(Role.PLATFORM_ADMIN, true));
				return ;
			}
		}
		
		Long accountId = pathParams.getFirst("accountId") != null ? Long.valueOf(pathParams.getFirst("accountId")) : -1;
		
		if(StringUtils.isBlank(apiKey)) {
			apiKey = headers.getFirst("apiKey");
			//if apiKey still not found, reject the request
			if(StringUtils.isBlank(apiKey)) {
				LOG.error("ApiKey NOT found. Unauthorized.");
				rc.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
				return ;
			}
		}
		Role role = authenticator.getRole(apiKey, operatorId, accountId);
		rc.setSecurityContext(new AuthContext(role));
		LOG.info("Requested resource={}  with role={}" , resourceName, role.toString());
	}
	
	
}