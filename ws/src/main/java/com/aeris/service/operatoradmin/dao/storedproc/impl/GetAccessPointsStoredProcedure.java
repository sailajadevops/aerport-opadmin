package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.AccessPointName;
import com.aeris.service.operatoradmin.model.AccessPointNameType;
import com.google.inject.Inject;

public class GetAccessPointsStoredProcedure extends AbstractProcedureCall<List<AccessPointName>> {

	private static final String SQL_GET_ACCESS_POINT_NAMES = "common_util_pkg.get_access_point_name";

	@Inject
	public GetAccessPointsStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_ACCESS_POINT_NAMES, true);
		
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<AccessPointName> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<AccessPointName> formats = createAccessPointNamesList(cursor);

		return formats;
	}

	private List<AccessPointName> createAccessPointNamesList(List<Map<String, Object>> cursor) {
		Set<AccessPointName> apns = new HashSet<AccessPointName>();

		for (Map<String, Object> row : cursor) {
			AccessPointName apn = new AccessPointName();

			String apnId = (String) row.get("apn_id");

			if (NumberUtils.isNumber(apnId)) {
				apn.setApnId(Integer.parseInt(apnId));
			}

			String apnName = (String) row.get("apn_value");

			if (StringUtils.isNotEmpty(apnName)) {
				apn.setApnName(apnName);
			}
			
			String apnType = (String) row.get("service");

			if (StringUtils.isNotEmpty(apnType)) {
				apn.setApnType(AccessPointNameType.fromName(apnType));
			}
			
			String carrierId = (String) row.get("carrier_id");

			if (NumberUtils.isNumber(carrierId)) {
				apn.setCarrierId(Integer.parseInt(carrierId));
			}
			
			apns.add(apn);
		}

		return new ArrayList<AccessPointName>(apns);
	}
}
