package com.aeris.service.operatoradmin.dao;

import java.util.List;

import com.aeris.service.operatoradmin.exception.SalesAgentException;
import com.aeris.service.operatoradmin.exception.SalesAgentNotFoundException;
import com.aeris.service.operatoradmin.model.SalesAgent;

public interface ISalesAgentDAO {
	SalesAgent createSalesAgent(SalesAgent salesAgent, String operatorId) throws SalesAgentException;

	SalesAgent updateSalesAgent(SalesAgent salesAgent, String operatorId) throws SalesAgentException,
			SalesAgentNotFoundException;

	boolean deleteSalesAgent(String agentId, String userLogin, String operatorId)
			throws SalesAgentException, SalesAgentNotFoundException;

	List<SalesAgent> getAllSalesAgents(String operatorId, boolean activeAgents) throws SalesAgentException;

	SalesAgent getSalesAgent(String agentId, String operatorId) throws SalesAgentException;
}
