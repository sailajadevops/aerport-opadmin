package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.ISupplierSIMOrderDAO;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.SupplierSIMOrderException;
import com.aeris.service.operatoradmin.exception.SupplierSIMOrderNotFoundException;
import com.aeris.service.operatoradmin.model.IMSIRangeType;
import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.SupplierSIMOrder;
import com.aeris.service.operatoradmin.utils.DBUtils;

public class SupplierSIMOrderDAO implements ISupplierSIMOrderDAO {

	private Logger LOG = LoggerFactory.getLogger(CustomerSIMOrderDAO.class);

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");

	private static final String SQL_GET_ALL_SUPPLIER_SIM_ORDERS = "select * " 
																	+ "from " 
																	+ "(select a.order_id, " 
																	+ "a.quantity, " 
																	+ "a.sim_format_id, "
																	+ "a.sim_warehouse_id, "
																	+ "a.sim_supplier_id, "
																	+ "a.product_id, "
																	+ "a.status_id, " 
																	+ "a.iccid_range, "
																	+ "a.imsi_range, "
																	+ "a.imsi_range_type, "
																	+ "a.iccid_start, "
																	+ "a.imsi_start, " 
																	+ "a.created_by, " 
																	+ "a.order_date, " 
																	+ "a.last_modified_by, "
																	+ "a.last_modified_date, "
																	+ "ROW_NUMBER() OVER (ORDER BY a.order_id) AS rnum "
																	+ "from supplier_sim_order a " 
																	+ "where a.operator_id = ?)";
	
	private static final String SQL_GET_ALL_SUPPLIER_SIM_ORDERS_BY_STATUS = "select * " 
																			+ "from " 
																			+ "(select a.order_id, " 
																			+ "a.quantity, " 
																			+ "a.sim_format_id, "
																			+ "a.sim_warehouse_id, "
																			+ "a.sim_supplier_id, "
																			+ "a.product_id, "
																			+ "a.status_id, " 
																			+ "a.iccid_range, "
																			+ "a.imsi_range, "
																			+ "a.imsi_range_type, "
																			+ "a.iccid_start, "
																			+ "a.imsi_start, " 
																			+ "a.created_by, " 
																			+ "a.order_date, " 
																			+ "a.last_modified_by, "
																			+ "a.last_modified_date, "
																			+ "ROW_NUMBER() OVER (ORDER BY a.order_id) AS rnum "
																			+ "from supplier_sim_order a " 
																			+ "where a.operator_id = ? and " 
																			+ "status in :status_list)";
	
	private static final String SQL_GET_SUPPLIER_SIM_ORDERS_BY_SUPPLIER_ID = "select a.order_id, " 
																				+ "a.quantity, " 
																				+ "a.sim_format_id, "
																				+ "a.sim_warehouse_id, "
																				+ "a.sim_supplier_id, "
																				+ "a.product_id, "
																				+ "a.status_id, " 
																				+ "a.iccid_range, "
																				+ "a.imsi_range, "
																				+ "a.imsi_range_type, "
																				+ "a.iccid_start, "
																				+ "a.imsi_start, " 
																				+ "a.created_by, " 
																				+ "a.order_date, " 
																				+ "a.last_modified_by, "
																				+ "a.last_modified_date "
																				+ "from supplier_sim_order a " 
																				+ "where a.operator_id = ? and "
																				+ "a.sim_supplier_id = ?";
	
	private static final String SQL_GET_SUPPLIER_SIM_ORDER_BY_ORDER_ID = "select a.order_id, " 
																			+ "a.quantity, " 
																			+ "a.sim_format_id, "
																			+ "a.sim_warehouse_id, "
																			+ "a.sim_supplier_id, "
																			+ "a.product_id, "
																			+ "a.status_id, " 
																			+ "a.iccid_range, "
																			+ "a.imsi_range, "
																			+ "a.imsi_range_type, "
																			+ "a.iccid_start, "
																			+ "a.imsi_start, " 
																			+ "a.created_by, " 
																			+ "a.order_date, " 
																			+ "a.last_modified_by, "
																			+ "a.last_modified_date "
																			+ "from supplier_sim_order a " 
																			+ "where a.operator_id = ? and "
																			+ "a.order_id = ?";
	
	private static final String SQL_GET_SUPPLIER_SIM_ORDERS_PAGINATION_CLAUSE = " where rnum between ? and ?";
	
	private static final String SQL_CREATE_SUPPLIER_SIM_ORDER = "insert into supplier_sim_order (order_id, operator_id, sim_supplier_id, sim_format_id, sim_warehouse_id, product_id, quantity, status_id, iccid_range, imsi_range, imsi_range_type, iccid_start, imsi_start, order_date, created_by, last_modified_date, last_modified_by, rep_timestamp) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String SQL_UPDATE_SUPPLIER_SIM_ORDER = "update supplier_sim_order set quantity = ?, status_id = ?, iccid_range = ?, imsi_range = ?, imsi_range_type = ?, iccid_start = ?, imsi_start = ?, last_modified_date = ?, last_modified_by = ? where operator_id = ? and order_id = ?";

	private static final String SQL_DELETE_SUPPLIER_SIM_ORDER = "delete from supplier_sim_order where operator_id = ? and order_id = ?";

	private static final String SQL_GET_NEXT_ORDER_ID = "select supplier_sim_order_id_seq.NEXTVAL from dual";
	
	@Override
	public List<SupplierSIMOrder> getAllSIMOrders(String operatorId, int supplierId, String start, String count, OrderStatus... orderStatusList) {
		List<SupplierSIMOrder> simOrders = new ArrayList<SupplierSIMOrder>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Fetch the SIM Orders for the operator with pagination support
			ps = buildGetSupplierSIMOrdersByOperatorIdQuery(conn, start, count, operatorId, orderStatusList);

			LOG.debug("getAllSIMOrders: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				SupplierSIMOrder simOrder = new SupplierSIMOrder();

				simOrder.setOrderId(rs.getLong("order_id"));
				simOrder.setQuantity(rs.getLong("quantity"));

				simOrder.setSimWarehouseId(rs.getInt("sim_warehouse_id"));
				simOrder.setSupplierId(rs.getInt("sim_supplier_id"));
				simOrder.setSimFormatId(rs.getInt("sim_format_id"));

				simOrder.setProductId(rs.getInt("product_id"));
				
				simOrder.setStatus(OrderStatus.fromValue(rs.getInt("status_id")));

				simOrder.setIccIdRange(rs.getString("iccid_range"));
				simOrder.setImsiRange(rs.getString("imsi_range"));
				simOrder.setImsiRangeType(IMSIRangeType.fromValue(rs.getString("imsi_range_type")));
				simOrder.setIccIdStart(rs.getString("iccid_start"));
				simOrder.setImsiStart(rs.getString("imsi_start"));
				
				simOrder.setCreatedBy(rs.getString("created_by"));
				simOrder.setLastModifiedBy(rs.getString("last_modified_by"));

				if (rs.getDate("order_date") != null) {
					simOrder.setCreatedDate(DATE_FORMAT.format(rs.getDate("order_date")));
				}

				if (rs.getDate("last_modified_date") != null) {
					simOrder.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
				}

				simOrders.add(simOrder);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getAllSIMOrders", e);
			throw new GenericServiceException("Unable to getAllSIMOrders", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getAllSIMOrders" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return simOrders;
	}

	private PreparedStatement buildGetSupplierSIMOrdersByOperatorIdQuery(Connection conn, String start, String count, String operatorId,
			OrderStatus... orderStatusList) throws SQLException {
		PreparedStatement ps;

		// Check if pagination parameters are valid
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		// Check if status is a search field
		boolean queryByStatus = ArrayUtils.isNotEmpty(orderStatusList);

		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_ALL_SUPPLIER_SIM_ORDERS);

		// Query if the status is used in the order search criteria
		if (queryByStatus) {
			// Build getOrders by Status query
			StringBuffer inClause = new StringBuffer("(");

			for (int i = 0; i < orderStatusList.length; i++) {
				inClause.append("?");

				if (i != orderStatusList.length - 1) {
					inClause.append(",");
				}
			}

			inClause.append(")");

			String getOrdersWithStatusQuery = SQL_GET_ALL_SUPPLIER_SIM_ORDERS_BY_STATUS.replace(":status_list", inClause.toString());

			query = new StringBuilder(getOrdersWithStatusQuery);
		}

		// Append Pagination parameters
		if (paginated) {
			query.append(SQL_GET_SUPPLIER_SIM_ORDERS_PAGINATION_CLAUSE);
		}

		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;

		// Set Operator Id
		ps.setString(++i, operatorId);

		// Set Status values
		if (queryByStatus) {
			int j = 0;

			// In clause for account ids
			while (j < orderStatusList.length) {
				int status = orderStatusList[j++].getValue();
				ps.setInt(++i, status);
			}
		}

		// paginate the results if required
		if (paginated) {
			long rownumStart = Long.parseLong(start);
			long rownumEnd = Long.parseLong(start) + Long.parseLong(count);

			ps.setLong(++i, rownumStart);
			ps.setLong(++i, rownumEnd);
		}

		return ps;
	}

	@Override
	public List<SupplierSIMOrder> getSIMOrdersBySupplierId(String operatorId, int supplierId) {
		List<SupplierSIMOrder> simOrders = new ArrayList<SupplierSIMOrder>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Fetch the SIM Orders for the supplier id
			ps = conn.prepareStatement(SQL_GET_SUPPLIER_SIM_ORDERS_BY_SUPPLIER_ID);

			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, supplierId);

			LOG.debug("getSIMOrdersBySupplierId: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				SupplierSIMOrder simOrder = new SupplierSIMOrder();

				simOrder.setOrderId(rs.getLong("order_id"));
				simOrder.setQuantity(rs.getLong("quantity"));

				simOrder.setSimWarehouseId(rs.getInt("sim_warehouse_id"));
				simOrder.setSupplierId(rs.getInt("sim_supplier_id"));
				simOrder.setSimFormatId(rs.getInt("sim_format_id"));

				simOrder.setProductId(rs.getInt("product_id"));
				
				simOrder.setStatus(OrderStatus.fromValue(rs.getInt("status_id")));

				simOrder.setIccIdRange(rs.getString("iccid_range"));
				simOrder.setImsiRange(rs.getString("imsi_range"));
				simOrder.setImsiRangeType(IMSIRangeType.fromValue(rs.getString("imsi_range_type")));
				simOrder.setIccIdStart(rs.getString("iccid_start"));
				simOrder.setImsiStart(rs.getString("imsi_start"));

				simOrder.setCreatedBy(rs.getString("created_by"));
				simOrder.setLastModifiedBy(rs.getString("last_modified_by"));

				if (rs.getDate("order_date") != null) {
					simOrder.setCreatedDate(DATE_FORMAT.format(rs.getDate("order_date")));
				}

				if (rs.getDate("last_modified_date") != null) {
					simOrder.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
				}

				simOrders.add(simOrder);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getSIMOrdersBySupplierId", e);
			throw new GenericServiceException("Unable to getSIMOrdersBySupplierId", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getSIMOrdersBySupplierId" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return simOrders;
	}

	@Override
	public SupplierSIMOrder getSIMOrderByOrderId(String operatorId, String orderId) {
		SupplierSIMOrder simOrder = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Fetch the SIM Orders for the supplier id
			ps = conn.prepareStatement(SQL_GET_SUPPLIER_SIM_ORDER_BY_ORDER_ID);

			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, Long.parseLong(orderId));

			LOG.debug("getSIMOrdersBySupplierId: executed query: " + ps);
			rs = ps.executeQuery();

			if (rs.next()) {
				simOrder = new SupplierSIMOrder();

				simOrder.setOrderId(rs.getLong("order_id"));
				simOrder.setQuantity(rs.getLong("quantity"));

				simOrder.setSimWarehouseId(rs.getInt("sim_warehouse_id"));
				simOrder.setSupplierId(rs.getInt("sim_supplier_id"));
				simOrder.setSimFormatId(rs.getInt("sim_format_id"));

				simOrder.setProductId(rs.getInt("product_id"));
				
				simOrder.setStatus(OrderStatus.fromValue(rs.getInt("status_id")));

				simOrder.setIccIdRange(rs.getString("iccid_range"));
				simOrder.setImsiRange(rs.getString("imsi_range"));
				simOrder.setImsiRangeType(IMSIRangeType.fromValue(rs.getString("imsi_range_type")));
				simOrder.setIccIdStart(rs.getString("iccid_start"));
				simOrder.setImsiStart(rs.getString("imsi_start"));

				simOrder.setCreatedBy(rs.getString("created_by"));
				simOrder.setLastModifiedBy(rs.getString("last_modified_by"));

				if (rs.getDate("order_date") != null) {
					simOrder.setCreatedDate(DATE_FORMAT.format(rs.getDate("order_date")));
				}

				if (rs.getDate("last_modified_date") != null) {
					simOrder.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception during getSIMOrderByOrderId", e);
			throw new GenericServiceException("Unable to getSIMOrderByOrderId", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getSIMOrderByOrderId" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return simOrder;
	}

	@Override
	public SupplierSIMOrder createSIMOrder(String operatorId, int supplierId, long quantity, int simWarehouseId, int simFormatId, int productId, OrderStatus status,
			String iccidRange, String imsiRange, IMSIRangeType imsiRangeType, String iccidStart, String imsiStart, Date requestedDate, String requestedUser) 
			throws SupplierSIMOrderException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		SupplierSIMOrder simOrder = null;
		long supplierOrderId = -1;
		int index = 0;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Generate the new order id
			supplierOrderId = getNextOrderId(conn);
			
			ps = conn.prepareStatement(SQL_CREATE_SUPPLIER_SIM_ORDER);

			ps.setLong(++index, supplierOrderId);
			ps.setLong(++index, Long.parseLong(operatorId));
			ps.setLong(++index, supplierId);
			ps.setLong(++index, simFormatId);
			ps.setLong(++index, simWarehouseId);
			ps.setLong(++index, productId);
			ps.setLong(++index, quantity);
			ps.setInt(++index, status.getValue());
			ps.setString(++index, iccidRange);
			ps.setString(++index, imsiRange);
			ps.setString(++index, imsiRangeType.getValue());
			ps.setString(++index, iccidStart);
			ps.setString(++index, imsiStart);
			ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));
			ps.setString(++index, requestedUser);
			ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));
			ps.setString(++index, requestedUser);
			ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));

			int count = ps.executeUpdate();

			// If the order is not created
			if (count <= 0) {
				throw new SupplierSIMOrderException("Supplier Order Creation failed for operatorId: " + operatorId + " ,supplierId: " + supplierId + " ,simFormatId: "
						+ simFormatId + " ,simWarehouseId: " + simWarehouseId + " ,quantity: " + quantity);
			}

			// Success Response
			simOrder = new SupplierSIMOrder();			
			simOrder.setOrderId(supplierOrderId);			
			simOrder.setQuantity(quantity);			
			simOrder.setSupplierId(supplierId);
			simOrder.setSimFormatId(simFormatId);
			simOrder.setSimWarehouseId(simWarehouseId);
			simOrder.setProductId(productId);			
			simOrder.setStatus(status);
			simOrder.setIccIdRange(iccidRange);
			simOrder.setImsiRange(imsiRange);
			simOrder.setImsiRangeType(imsiRangeType);
			simOrder.setIccIdStart(iccidStart);
			simOrder.setImsiStart(imsiStart);
			simOrder.setCreatedBy(requestedUser);
			simOrder.setLastModifiedBy(requestedUser);
			simOrder.setCreatedDate(DATE_FORMAT.format(requestedDate));
			simOrder.setLastModifiedDate(DATE_FORMAT.format(requestedDate));

			LOG.info("Created Supplier SIM Order = " + ReflectionToStringBuilder.toString(simOrder));
		} catch (SQLException sqle) {
			LOG.error("Exception during creating supplier sim order in supplier_sim_order" + sqle);
			throw new SupplierSIMOrderException("Exception in createSIMOrder - Exception during creating supplier sim order", sqle);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return simOrder;
	}

	@Override
	public boolean updateSIMOrder(String operatorId, long orderId, OrderStatus status, long quantity, String iccidRange, String imsiRange, IMSIRangeType imsiRangeType, 
			String iccidStart, String imsiStart, Date requestedDate, String requestedUser) 
			throws SupplierSIMOrderException, SupplierSIMOrderNotFoundException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		int index = 0;
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			ps = conn.prepareStatement(SQL_UPDATE_SUPPLIER_SIM_ORDER);

			// Set Parameters
			ps.setLong(++index, quantity);
			ps.setInt(++index, status.getValue());
			ps.setString(++index, iccidRange);
			ps.setString(++index, imsiRange);
			ps.setString(++index, imsiRangeType.getValue());
			ps.setString(++index, iccidStart);
			ps.setString(++index, imsiStart);
			ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));
			ps.setString(++index, requestedUser);
			
			ps.setLong(++index, Long.parseLong(operatorId));
			ps.setLong(++index, orderId);
			
			int count = ps.executeUpdate();

			// If no rows are updated
			if(count <= 0)
			{
				throw new SupplierSIMOrderNotFoundException("Order with orderId: "+orderId+" for operatorId: "+operatorId+" is not found");
			}

			LOG.info("Updated Supplier SIM Order = " + orderId + " successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during updating supplier sim order in supplier_sim_order" + sqle);
			throw new SupplierSIMOrderException("Exception in updateSIMOrder - Exception during updating supplier sim order", sqle);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		
		return true;
	}

	@Override
	public boolean deleteSIMOrder(String operatorId, long orderId, Date requestedDate, String requestedUser) 
			throws SupplierSIMOrderException, SupplierSIMOrderNotFoundException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			ps = conn.prepareStatement(SQL_DELETE_SUPPLIER_SIM_ORDER);

			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, orderId);
			
			int count = ps.executeUpdate();

			// If no rows are updated
			if(count <= 0)
			{
				throw new SupplierSIMOrderNotFoundException("Order with orderId: "+orderId+" for operatorId: "+operatorId+" is not found");
			}

			LOG.info("Updated Supplier SIM Order = " + orderId + " successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting supplier sim order in supplier_sim_order" + sqle);
			throw new SupplierSIMOrderException("Exception in deleteSIMOrder - Exception during deleting supplier sim order", sqle);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		
		return true;
	}
	
	private long getNextOrderId(Connection conn) throws SQLException, SupplierSIMOrderException
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		long orderId = -1;
		
		try
		{
			ps = conn.prepareStatement(SQL_GET_NEXT_ORDER_ID);	
			
			LOG.info("Creating sql statement: " + ps +" to get next order id");
	
			rs = ps.executeQuery();
			
			if(rs.next())
			{
				orderId = rs.getLong(1);
			}
			
			LOG.info("Next supplier order Id fetched: "+orderId);
		}
		finally
		{
			DBUtils.cleanup(null, ps, rs);
		}
		
		if(orderId == -1)
		{
			throw new SupplierSIMOrderException("Could not fetch the next order id while creating new supplier sim order");
		}
		
		return orderId;
	}

}
