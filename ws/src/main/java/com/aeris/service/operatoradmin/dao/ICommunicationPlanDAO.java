package com.aeris.service.operatoradmin.dao;

import java.sql.SQLException;

import com.aeris.service.operatoradmin.model.APNConfiguration;
import com.aeris.service.operatoradmin.model.EnrolledServices;

public interface ICommunicationPlanDAO {
	
	public Integer getCommPlanId(EnrolledServices enrolledServices, APNConfiguration apnConfig, int RES, long accountId) throws SQLException;

}
