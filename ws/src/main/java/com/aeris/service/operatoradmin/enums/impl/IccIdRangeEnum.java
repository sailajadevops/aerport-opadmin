package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IIccIdRangeEnum;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.IccIdRange;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * Resource Definition Cache used for lookup purposes 
 * 
 * @author SP00125222
 */
@Singleton
public class IccIdRangeEnum implements IIccIdRangeEnum {
	private Logger LOG = LoggerFactory.getLogger(IccIdRangeEnum.class);
	static final String SQL_GET_ICCID_RANGE = "select range, range_name, order_date, tail from iccid_range";
	
	private Cache<String, IccIdRange> cache;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "IccIdRangeCache/name") Cache<String, IccIdRange> cache) {
		this.cache = cache;	
		loadResources();
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_ICCID_RANGE);
			
			while (rs.next()) {
				IccIdRange iccIdRange = new IccIdRange();

				iccIdRange.setRange(rs.getString("range"));
				iccIdRange.setRangeName(rs.getString("range_name"));
				iccIdRange.setOrderDate(rs.getDate("order_date"));
				iccIdRange.setTail(rs.getLong("tail"));
				
				cache.put(iccIdRange.getRange(), iccIdRange);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}

	@Override
	public IccIdRange getIccIdRange(String range) {
		return cache.get(range);
	}

	@Override
	public List<IccIdRange> getAllIccIdRanges() {
		return new ArrayList<IccIdRange>(cache.getValues());
	}	
	
	@Override
	public boolean isValid(String id) {
		return true; 
	}
}
