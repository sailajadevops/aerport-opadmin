package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.security.auth.login.AccountNotFoundException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.ICustomerSIMOrderDAO;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.enums.ISIMFormatEnum;
import com.aeris.service.operatoradmin.enums.ISIMWarehouseEnum;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.SIMConfigNotFoundException;
import com.aeris.service.operatoradmin.exception.SIMOrderException;
import com.aeris.service.operatoradmin.exception.SIMOrderNotFoundException;
import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.SIMConfig;
import com.aeris.service.operatoradmin.model.SIMOrder;
import com.aeris.service.operatoradmin.utils.DBUtils;

public class CustomerSIMOrderDAO implements ICustomerSIMOrderDAO{
	private static final String AERIS_US = "Aeris US";

	private Logger LOG = LoggerFactory.getLogger(CustomerSIMOrderDAO.class);
	
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");
	
	static final String SQL_GET_CUST_ORDERS = "select * " 
												+ "from " 
												+ "(select a.order_id, " 
												+ "a.account_id," 
												+ "a.quantity, " 
												+ "a.product_id, " 
												+ "a.sim_format_id, "
												+ "a.sim_warehouse_id, "
												+ "a.status_id, " 
												+ "a.alloc_quantity, " 
												+ "a.description, " 
												+ "a.mailing_addr, "
												+ "a.created_by, " 
												+ "a.order_date, " 
												+ "a.last_modified_by, "
												+ "a.last_modified_date, "
												+ "ROW_NUMBER() OVER (ORDER BY a.order_id) AS rnum "
												+ "from customer_sim_order a, aersys_accounts b, ws_accts_additional_info c " 
												+ "where a.account_id = b.account_id "
												+ "and b.account_id = c.account_id "
												+ "and a.account_id = ? "
												+ "and c.operator_id = ?)";
	
	static final String SQL_GET_CUST_ORDERS_BY_ORDER_ID = "select a.order_id, " 
															+ "a.account_id," 
															+ "a.quantity, " 
															+ "a.product_id, " 
															+ "a.sim_format_id, "
															+ "a.sim_warehouse_id, "
															+ "a.status_id, " 
															+ "a.alloc_quantity, " 
															+ "a.description, " 
															+ "a.mailing_addr, "
															+ "a.created_by, " 
															+ "a.order_date, " 
															+ "a.last_modified_by, "
															+ "a.last_modified_date "
															+ "from customer_sim_order a, aersys_accounts b, ws_accts_additional_info c " 
															+ "where a.account_id = b.account_id "
															+ "and a.account_id = ? " 
															+ "and c.operator_id = ? "
															+ "and a.order_id = ?";
	
	static final String SQL_CREATE_CUST_ORDER = "insert into customer_sim_order (account_id, product_id, sim_format_id, sim_warehouse_id, quantity, alloc_quantity, status_id, description, mailing_addr, order_date, created_by, last_modified_date, last_modified_by) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	static final String SQL_UPDATE_CUST_ORDER = "update customer_sim_order set quantity = ?, description = ?, mailing_addr = ?, status_id = ?, last_modified_by = ?, last_modified_date = ? where order_id = ? and account_id = ?";
	
	static final String SQL_UPDATE_CUST_ORDER_WITHOUT_QUANTITY = "update customer_sim_order set description = ?, mailing_addr = ?, status_id = ?, last_modified_by = ?, last_modified_date = ? where order_id = ? and account_id = ?";
	
	static final String SQL_DELETE_CUST_ORDER = "delete from customer_sim_order where order_id = ? and account_id = ?";
	
	static final String SQL_GET_CUST_ORDERS_BY_STATUS = "select * " 
														+ "from " 
														+ "(select a.order_id, " 
														+ "a.account_id," 
														+ "a.product_id, " 
														+ "a.sim_format_id, "
														+ "a.sim_warehouse_id, "
														+ "a.quantity, " 
														+ "a.alloc_quantity, " 
														+ "a.status_id, " 
														+ "a.description, " 
														+ "a.mailing_addr, " 
														+ "a.order_date, " 
														+ "a.created_by, " 
														+ "a.last_modified_by, "
														+ "a.last_modified_date, "
														+ "ROW_NUMBER() OVER (ORDER BY a.order_id) AS rnum "
														+ "from customer_sim_order a, aersys_accounts b, ws_accts_additional_info c " 
														+ "where a.account_id = b.account_id "
														+ "and b.account_id = c.account_id "
														+ "and a.account_id = ? " 
														+ "and c.operator_id = ? "
														+ "and status_id in :status_list)";
	
	static final String SQL_GET_CUST_SIM_CONFIG_BY_TYPE_AND_FORMAT = "select " 
																		+ "a.account_id," 
																		+ "a.product_id, " 
																		+ "a.sim_format_id, "
																		+ "a.sim_cost " 
																		+ "from acct_sim_config a " 
																		+ "where a.account_id = ? and a.product_id = ? and a.sim_format_id = ?";
	
	static final String SQL_GET_CUST_SIM_CONFIG = "select " 
													+ "a.account_id," 
													+ "a.product_id, " 
													+ "a.sim_format_id, "
													+ "a.sim_cost " 
													+ "from acct_sim_config a " 
													+ "where a.account_id = ?";

	static final String SQL_GET_CUST_ORDERS_PAGINATION_CLAUSE = " where rnum between ? and ?";
	
	static final String SQL_GET_OPERATOR_ID_FOR_ACCOUNT = "select " 
															+ "b.operator_id "
															+ "from aersys_accounts a, ws_accts_additional_info b " 
															+ "where a.account_id = b.account_id " 
															+ "and a.account_id = ?";
	
	@Inject
	private IProductEnum productCache;
    
    @Inject
	private ISIMFormatEnum simFormatCache;    

    @Inject
	private ISIMWarehouseEnum simWarehouseCache;
	
	@Override
	public List<SIMOrder> getAllSIMOrders(String operatorId, String accountId, String start, String count, OrderStatus... orderStatusList) {
		List<SIMOrder> simOrders = new ArrayList<SIMOrder>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Fetch the SIM Orders for the 
			ps = buildGetSIMOrdersByAccountIdsQuery(conn, start, count, operatorId, accountId, orderStatusList);

			LOG.debug("getAllSIMOrders: executed query: " + ps); 
			rs = ps.executeQuery();
			
			while (rs.next()) {
				SIMOrder simOrder = new SIMOrder();
				
				simOrder.setOrderId(rs.getLong("order_id"));
				simOrder.setAccountId(rs.getInt("account_id"));
				
				simOrder.setAllocQuantity(rs.getLong("alloc_quantity"));
				simOrder.setQuantity(rs.getLong("quantity"));
				
				simOrder.setSimFormatId(simFormatCache.getSIMFormatById(rs.getInt("sim_format_id")).getId());
				simOrder.setProductId(productCache.getProductById(rs.getInt("product_id")).getProductId());
				
				simOrder.setStatus(OrderStatus.fromValue(rs.getInt("status_id")));
				simOrder.setDescription(rs.getString("description"));
				simOrder.setShippingAddress(rs.getString("mailing_addr"));
				
				simOrder.setCreatedBy(rs.getString("created_by"));
				simOrder.setLastModifiedBy(rs.getString("last_modified_by"));
				
				if (rs.getDate("order_date") != null) {
					simOrder.setCreatedDate(DATE_FORMAT.format(rs.getDate("order_date")));
				}

				if (rs.getDate("last_modified_date") != null) {
					simOrder.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
				}
				
				simOrders.add(simOrder);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getAllSIMOrders", e);
			throw new GenericServiceException("Unable to getAllSIMOrders", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getAllSIMOrders" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return simOrders;
	}
	
	private PreparedStatement buildGetSIMOrdersByAccountIdsQuery(Connection conn, String start, String count, String operatorId, String accountId, OrderStatus... orderStatusList)
			throws SQLException {
		PreparedStatement ps;

		// Check if pagination parameters are valid
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);
		
		// Check if status is a search field
		boolean queryByStatus = ArrayUtils.isNotEmpty(orderStatusList);
		
		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_CUST_ORDERS);
				
		// Query if the status is used in the order search criteria
		if(queryByStatus)
		{
			// Build getOrders by Status query
			StringBuffer inClause = new StringBuffer("(");
			
			for (int i = 0; i < orderStatusList.length; i++) {
				inClause.append("?");

				if (i != orderStatusList.length - 1) {
					inClause.append(",");
				}
			}

			inClause.append(")");
			
			String getOrdersWithStatusQuery = SQL_GET_CUST_ORDERS_BY_STATUS.replace(":status_list", inClause.toString());
			
			query = new StringBuilder(getOrdersWithStatusQuery);
		}
		
		// Append Pagination parameters
		if (paginated) {
			query.append(SQL_GET_CUST_ORDERS_PAGINATION_CLAUSE);
		}

		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		
		// Set Account Id
		ps.setString(++i, accountId);
		
		// Set Operator Id
		ps.setString(++i, operatorId);
			
		// Set Status values
		if(queryByStatus)
		{
			int j = 0;
			
			// In clause for account ids
			while (j < orderStatusList.length) {
				int status = orderStatusList[j++].getValue();
				ps.setInt(++i, status);
			}
		}

		// paginate the results if required
		if (paginated) {
			long rownumStart = Long.parseLong(start);
			long rownumEnd = Long.parseLong(start) + Long.parseLong(count);

			ps.setLong(++i, rownumStart);
			ps.setLong(++i, rownumEnd);
		}

		return ps;
	}

	@Override
	public SIMOrder getSIMOrderByOrderId(String operatorId, String accountId, String orderId) {
		SIMOrder simOrder = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Fetch the SIM Orders for the 
			ps = conn.prepareStatement(SQL_GET_CUST_ORDERS_BY_ORDER_ID);

			ps.setLong(1, Long.parseLong(accountId));
			ps.setLong(2, Long.parseLong(operatorId));
			ps.setLong(3, Long.parseLong(orderId));
			
			LOG.debug("getSIMOrderByOrderId: executed query: " + ps);
			rs = ps.executeQuery();

			if (rs.next()) {
				simOrder = new SIMOrder();
				
				simOrder.setOrderId(rs.getLong("order_id"));
				simOrder.setAccountId(rs.getInt("account_id"));
				
				simOrder.setAllocQuantity(rs.getLong("alloc_quantity"));
				simOrder.setQuantity(rs.getLong("quantity"));
				
				simOrder.setSimFormatId(simFormatCache.getSIMFormatById(rs.getInt("sim_format_id")).getId());
				simOrder.setProductId(productCache.getProductById(rs.getInt("product_id")).getProductId());
				
				simOrder.setStatus(OrderStatus.fromValue(rs.getInt("status_id")));
				simOrder.setDescription(rs.getString("description"));
				simOrder.setShippingAddress(rs.getString("mailing_addr"));
				
				simOrder.setCreatedBy(rs.getString("created_by"));
				simOrder.setLastModifiedBy(rs.getString("last_modified_by"));
				
				if (rs.getDate("order_date") != null) {
					simOrder.setCreatedDate(DATE_FORMAT.format(rs.getDate("order_date")));
				}

				if (rs.getDate("last_modified_date") != null) {
					simOrder.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
				} 
			}
		} catch (SQLException e) {
			LOG.error("Exception during getSIMOrderByOrderId", e);
			throw new GenericServiceException("Unable to getSIMOrderByOrderId", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getSIMOrderByOrderId" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return simOrder;
	}

	@Override
	public List<SIMConfig> getSIMConfig(String operatorId, String accountId) throws SIMOrderException {
		List<SIMConfig> simConfig = new ArrayList<SIMConfig>();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_CUST_SIM_CONFIG);
			ps.setLong(1, Long.parseLong(accountId));
			
			LOG.debug("getSIMConfig: getting sim config for accountId: "+accountId);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				SIMConfig config = new SIMConfig();
				config.setProduct(productCache.getProductById(rs.getInt("product_id")));
				
				if(simConfig.contains(config))
				{
					int index = simConfig.indexOf(config);
					simConfig.get(index).addSIMFormat(simFormatCache.getSIMFormatById(rs.getInt("sim_format_id")), rs.getDouble("sim_cost"));
				}
				else
				{
					config.addSIMFormat(simFormatCache.getSIMFormatById(rs.getInt("sim_format_id")), rs.getDouble("sim_cost"));
					simConfig.add(config);
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception during getSIMConfig", e);
			throw new SIMOrderException("Exception during getSIMConfig", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getSIMConfig " + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		
		return simConfig;
	}
	
	@Override
	public SIMOrder createSIMOrder(String operatorId, String accountId, int productId, int simFormatId, long quantity, String description, String shippingAddress, 
			String trackingNumber, String comments, Date requestedDate, String requestedUser) 
			throws SIMConfigNotFoundException, SIMOrderException, AccountNotFoundException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		int simWarehouseId = simWarehouseCache.getSIMWarehouseByName(AERIS_US).getWarehouseId();// Fetch from cache
		
		// Validate Operator
		validateOperator(operatorId, accountId);
		
		// Validate SIM configuration for the account
		validateSIMConfiguration(accountId, productId, simFormatId);
		
		SIMOrder simOrder = null;
		long orderId = -1;
		long allocatedQuantity = 0;
		int index = 0;
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement(SQL_CREATE_CUST_ORDER, new String[] {"order_id"});

			ps.setLong(++index, Long.parseLong(accountId));
			ps.setInt(++index, productId);
			ps.setInt(++index, simFormatId);
			ps.setInt(++index, simWarehouseId);
			ps.setLong(++index, quantity);
			ps.setLong(++index, allocatedQuantity);
			ps.setInt(++index, OrderStatus.OPEN.getValue());
			ps.setString(++index, description);
			ps.setString(++index, shippingAddress);
			ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));
			ps.setString(++index, requestedUser);
			ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));
			ps.setString(++index, requestedUser);
			
			ps.executeUpdate();

			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				orderId = rs.getLong(1);
			}

			if(orderId == -1)
			{
				throw new SIMOrderException("Order Creation failed for accountId: "+accountId+" ,simType: " + productId+" ,simFormat: "+ simFormatId+" ,Quantity: "+ quantity);
			}
			
			simOrder = new SIMOrder();
			simOrder.setOrderId(orderId);
			simOrder.setAccountId(Integer.parseInt(accountId));
			simOrder.setQuantity(quantity);
			simOrder.setAllocQuantity(allocatedQuantity);
			
			simOrder.setSimFormatId(simFormatId);
			simOrder.setProductId(productId);
			
			simOrder.setStatus(OrderStatus.OPEN);
			simOrder.setDescription(description);
			simOrder.setShippingAddress(shippingAddress);
			simOrder.setCreatedBy(requestedUser);
			simOrder.setLastModifiedBy(requestedUser);
			simOrder.setCreatedDate(DATE_FORMAT.format(requestedDate));
			simOrder.setLastModifiedDate(DATE_FORMAT.format(requestedDate));
			
			LOG.info("Created SIM Order = " + ReflectionToStringBuilder.toString(simOrder));
		} catch (SQLException sqle) {
			LOG.error("DB Exception during creating sim order in customer_sim_order" + sqle);
			throw new SIMOrderException("DB Exception while creating SIMOrder", sqle);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return simOrder;
	}
	
	@Override
	public boolean updateSIMOrder(String operatorId, String accountId, String orderId, long quantity, String description, String shippingAddress, OrderStatus status, 
			String trackingNumber, String comments, Date requestedDate, String requestedUser) 
			throws SIMOrderException, AccountNotFoundException, SIMOrderNotFoundException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SIMOrder simOrder = null;
				
		// Validate Operator
		validateOperator(operatorId, accountId); 
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			int index = 0;
			
			// If quantity is given
			if(quantity > 0)
			{
				ps = conn.prepareStatement(SQL_UPDATE_CUST_ORDER);
				ps.setLong(++index, quantity); // set quantity
			}
			// Update other fields except quantity
			else
			{
				ps = conn.prepareStatement(SQL_UPDATE_CUST_ORDER_WITHOUT_QUANTITY);
			}
			
			ps.setString(++index, description); // set description
			ps.setString(++index, shippingAddress); // set mailing_addr
			ps.setInt(++index, status.getValue());// set status
			ps.setString(++index, requestedUser);// set last_modified_by
			ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));// set last_modified_date
			ps.setString(++index, orderId);// set order_id to update
			ps.setLong(++index, Long.parseLong(accountId));// set account_id
			
			int count = ps.executeUpdate();

			// If no rows are updated
			if(count <= 0)
			{
				throw new SIMOrderNotFoundException("Order with orderId: "+orderId+" for accountId: "+accountId+" is not found");
			}
			
			LOG.info("Updated SIM Order = " + ReflectionToStringBuilder.toString(simOrder));
		} catch (SQLException sqle) {
			LOG.error("DB Exception during updating sim order in customer_sim_order" + sqle);
			throw new SIMOrderException("DB Exception while updating SIMOrder", sqle);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		
		return true;
	}
	
	private void validateOperator(String operatorId, String accountId) throws SIMOrderException, AccountNotFoundException
	{
		LOG.info("validateOperator: paranmeters: accountId: "+accountId+" operatorId: "+operatorId);
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_OPERATOR_ID_FOR_ACCOUNT);
			ps.setLong(1, Long.parseLong(accountId));
			
			LOG.info("validateOperator: validating operator for accountId: "+accountId);
			rs = ps.executeQuery();

			long opId = -1;
			
			while (rs.next()) {
				opId = rs.getLong("operator_id");
			}

			long inOperatorId = Long.parseLong(operatorId);

			if (opId != inOperatorId) {
				throw new AccountNotFoundException("Account: "+accountId+" does not belong to the Operator: " +inOperatorId);
			}
		} catch (SQLException e) {
			LOG.error("DB Exception during validateOperator", e);
			throw new SIMOrderException("DB Exception during validateOperator", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
	}
	
	private void validateSIMConfiguration(String accountId, int productId, int simFormat) throws SIMOrderException, SIMConfigNotFoundException
	{
		LOG.info("validateSIMConfiguration: paranmeters: accountId: "+accountId+", productId: "+productId+", simFormat: "+simFormat);
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		long cost = -1;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_CUST_SIM_CONFIG_BY_TYPE_AND_FORMAT);
			ps.setInt(1, Integer.parseInt(accountId));
			ps.setInt(2, productId);
			ps.setInt(3, simFormat);
			
			LOG.debug("validateSIMConfiguration: validating sim config for accountId: "+accountId+", productId: "+productId+" and simFormat: " + simFormat);
			rs = ps.executeQuery();

			while (rs.next()) {
				cost = rs.getLong("sim_cost");
			}

			if (cost == -1) {
				throw new SIMConfigNotFoundException("Account not configured for productId: " + productId+" and simFormat: "+ simFormat);
			}
		} catch (SQLException e) {
			LOG.error("DB Exception during validateSIMConfiguration", e);
			throw new SIMOrderException("DB Exception while validating SIMConfiguration values in order request", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
	}

	@Override
	public boolean deleteSIMOrder(String operatorId, String accountId, String orderId, Date requestedDate, String requestedUser) 
			throws SIMOrderException, AccountNotFoundException, SIMOrderNotFoundException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		SIMOrder simOrder = null;
				
		// Validate Operator
		validateOperator(operatorId, accountId);
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			int index = 0;
			
			ps = conn.prepareStatement(SQL_DELETE_CUST_ORDER);  
			
			ps.setString(++index, orderId);// set order_id to delete
			ps.setLong(++index, Long.parseLong(accountId));// set account_id
			
			int count = ps.executeUpdate();

			// If no rows are updated
			if(count <= 0)
			{
				throw new SIMOrderNotFoundException("Order with orderId: "+orderId+" for accountId: "+accountId+" is not found");
			}
			
			LOG.info("Deleted SIM Order = " + ReflectionToStringBuilder.toString(simOrder));
		} catch (SQLException sqle) {
			LOG.error("DB Exception during deleting sim order in customer_sim_order" + sqle);
			throw new SIMOrderException("DB Exception while deleting SIMOrder", sqle);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return true;
	}
}
