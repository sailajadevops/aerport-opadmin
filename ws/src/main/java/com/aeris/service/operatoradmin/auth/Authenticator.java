package com.aeris.service.operatoradmin.auth;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IAccountApplicationDAO;
import com.aeris.service.operatoradmin.enums.impl.ApiKeyEnum;
import com.aeris.service.operatoradmin.enums.impl.OperatorKeyEnum;
import com.aeris.service.operatoradmin.model.ApiKey;
import com.aeris.service.operatoradmin.model.ApplicationProperties;
import com.aeris.service.operatoradmin.model.PermissionValue;
import com.aeris.service.operatoradmin.model.Role;
import com.google.inject.Inject;

/**
 * Authenticator class for apiKey authentication and role mapping
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class Authenticator {

	@Inject private ApplicationProperties applicationProperties ;
	@Inject private OperatorKeyEnum operatorKeyEnum ;
	@Inject private ApiKeyEnum apiKeyEnum ;
	@Inject private IAccountApplicationDAO accountApplicationDAO;
	private static final Logger LOG = LoggerFactory.getLogger(Authenticator.class);

	public Role getRole(String key, long operatorId, long accountId) {

		Role role = Role.PING_ONLY;

		if (StringUtils.isBlank(key)) {
			return role ;
		}
		// check weather the key passed is an operator-key or not
		ApiKey operatorKey = operatorKeyEnum.get(key);
		if(operatorKey != null && operatorKey.getOperatorId() > 0) {
			if((Long.valueOf(operatorId).equals(operatorKey.getOperatorId()))){  
				if(operatorKey.getPermissions() == null || operatorKey.getPermissions().size() == 0) {
					return role ;
				} else if(operatorKey.getPermissions().get(0).getPermissions().contains(PermissionValue.WRITE.name().toLowerCase())) {
					role = Role.OPERATOR_WR ;
				} else {
					role = Role.OPERATOR ;
				}
			}
		} else if(key.equalsIgnoreCase(applicationProperties.getPlatformAdminKey())) {
			role = Role.PLATFORM_ADMIN ;
		} else {
			//If its not an operator-key, check whether its an accountKey or not
			if(accountId > 0) {
				try {
					List<ApiKey> apiKeys = apiKeyEnum.get(Long.valueOf(accountId));
					//apiKey for accountId not found return default role 
					boolean keyFound = false ;
					boolean isAccountMappedToOperator = false ;
					if(apiKeys == null || apiKeys.isEmpty()) {
						LOG.info("Account not found::" + accountId);
						return role ;
					} else {
						for (ApiKey accKey : apiKeys) {
							if(accKey.getOperatorId() == operatorId){
								isAccountMappedToOperator = true ;
							}
							if(accKey.getOperatorId() == operatorId && accKey.getApiKey().equalsIgnoreCase(key)) {
								role = Role.ACCOUNT ;
								keyFound = true ;
								break ;
							}
						}
					}
					if(!keyFound && isAccountMappedToOperator) {
						//check if master is trying to access resources on-behalf-of child account
						List<Long> childAccounts = accountApplicationDAO.getAccountHierarchy( accountApplicationDAO.getAccountIdForApiKey(key)) ;
						if(childAccounts.contains(Long.valueOf(accountId))) {
							role = Role.ACCOUNT ;
						}
					}
				} catch (Exception e) {
					return role;
				}
			}
		}
		return role;
	}


}
