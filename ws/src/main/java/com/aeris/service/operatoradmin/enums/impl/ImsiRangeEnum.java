package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IImsiRangeEnum;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.IMSIRangeType;
import com.aeris.service.operatoradmin.model.ImsiRange;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * Resource Definition Cache used for lookup purposes 
 * 
 * @author SP00125222
 */
@Singleton
public class ImsiRangeEnum implements IImsiRangeEnum {
	private Logger LOG = LoggerFactory.getLogger(ImsiRangeEnum.class);
	static final String SQL_GET_IMSI_RANGE = "select range, range_type, curr_imsi, min_value, max_value, product_id, operator_id from imsi_range";
	
	private Cache<String, ImsiRange> cache;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "ImsiRangeCache/name") Cache<String, ImsiRange> cache) {
		this.cache = cache;	
		loadResources();
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_IMSI_RANGE);
			
			while (rs.next()) {
				ImsiRange imsiRange = new ImsiRange();

				imsiRange.setRange(rs.getString("range"));
				imsiRange.setRangeType(IMSIRangeType.fromValue(rs.getString("range_type")));
				imsiRange.setCurrentImsi(rs.getLong("curr_imsi"));
				imsiRange.setMinImsi(rs.getLong("min_value"));
				imsiRange.setMaxImsi(rs.getLong("max_value"));
				imsiRange.setProductId(rs.getInt("product_id"));
				imsiRange.setOperatorId(rs.getInt("operator_id"));
				
				cache.put(imsiRange.getRange(), imsiRange);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}

	@Override
	public ImsiRange getImsiRange(String range) {
		return cache.get(range);
	}

	@Override
	public List<ImsiRange> getAllImsiRanges() {
		return new ArrayList<ImsiRange>(cache.getValues());
	}	
	
	@Override
	public boolean isValid(String id) {
		return true; 
	}
}
