package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.ImsiRange;

public interface IImsiRangeEnum  extends IBaseEnum{
	ImsiRange getImsiRange(String range);
	List<ImsiRange> getAllImsiRanges();
}
