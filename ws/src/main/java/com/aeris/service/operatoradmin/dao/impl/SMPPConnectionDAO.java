package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.ISMPPConnectionDAO;
import com.aeris.service.operatoradmin.exception.ConnectionLimitException;
import com.aeris.service.operatoradmin.exception.DAOException;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.ConnectionLimit;

public class SMPPConnectionDAO implements ISMPPConnectionDAO {
	private static Logger LOG = LoggerFactory.getLogger(SMPPConnectionDAO.class);
	
	private static final String SQL_GET_CONNECTION_LIMIT ="select service_code, limit from smpp_connection_limit where service_code = ?";
	private static final String SQL_INSERT_CONNECTION_LIMIT = "insert into smpp_connection_limit(rep_timestamp,service_code,limit) values(?,?,?) ";
	private static final String SQL_UPDATE_CONNECTION_LIMIT = "update smpp_connection_limit set limit = ? where service_code = ?" ;
	private static final String SQL_DELETE_CONNECTION_LIMIT = "delete from smpp_connection_limit where service_code = ?";
	
	@Override
	public ConnectionLimit getConnectionLimit(long serviceCode)	throws ConnectionLimitException {
		LOG.info("getConnectionLimit: getting connection limit for service code: "+serviceCode);
		ConnectionLimit connectionLimit = null;
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement(SQL_GET_CONNECTION_LIMIT);

			ps.setLong(1, serviceCode);
			
			LOG.info("getConnectionLimit: executed query: " + SQL_GET_CONNECTION_LIMIT +" service_code :"+serviceCode);
			rs = ps.executeQuery();
			
			if(rs.next()){
				connectionLimit = new ConnectionLimit();
				connectionLimit.setServiceCode(rs.getLong("service_code"));
				connectionLimit.setConnectionLimit(rs.getLong("limit"));
				
				LOG.info("got connectionLimit object :"+connectionLimit);
			}
		}catch (SQLException e) {
			LOG.error("Exception during getConnectionLimit", e);
			throw new ConnectionLimitException("Fatal error while getting connection limit :", e);
		} finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}
		
		return connectionLimit;
	}

	@Override
	public ConnectionLimit createConnectionLimit(long serviceCode, long conLimit) throws ConnectionLimitException {
		LOG.info("createConnectionLimit: creating connection limit for service code: "+serviceCode+", connectionLimit : "+conLimit);
		
		ConnectionLimit connectionLimit = null;
		
		Connection conn = null;
		PreparedStatement ps = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);
			
			ps = conn.prepareStatement(SQL_INSERT_CONNECTION_LIMIT);

			ps.setTimestamp(1, new java.sql.Timestamp(new Date().getTime()));
			ps.setLong(2, serviceCode);
			ps.setLong(3, conLimit);
			
			LOG.info("createConnectionLimit: executed query: " + SQL_INSERT_CONNECTION_LIMIT +" service_code :"+serviceCode+",connectionLimit : "+conLimit);
			ps.executeUpdate();
			
			connectionLimit = new ConnectionLimit();
			connectionLimit.setServiceCode(serviceCode);
			connectionLimit.setConnectionLimit(conLimit);
				
		}catch (SQLException e) {
			LOG.error("Exception during createConnectionLimit", e);
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			throw new ConnectionLimitException("Fatal error while creating connection limit :", e);
		} finally {
			if (conn != null) {
				try {
					conn.commit();
				} catch (SQLException e) {
					LOG.error("Exception during commit " + e);
				}
			}
			
			DBConnectionManager.getInstance().cleanup(conn, ps, null);
		}
		
		return connectionLimit;
	}

	@Override
	public ConnectionLimit updateConnectionLimit(long serviceCode, long conLimit) throws ConnectionLimitException {
		LOG.info("updateConnectionLimit: updating connection limit for service code: "+serviceCode+", connectionLimit : "+conLimit);
		
		ConnectionLimit connectionLimit = null;
		
		Connection conn = null;
		PreparedStatement ps = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);
			
			ps = conn.prepareStatement(SQL_UPDATE_CONNECTION_LIMIT);

			ps.setLong(1, conLimit);
			ps.setLong(2, serviceCode);
			
			LOG.info("updateConnectionLimit: executed query: " + SQL_UPDATE_CONNECTION_LIMIT +" service_code :"+serviceCode+",connectionLimit : "+conLimit);
			int updatedCount = ps.executeUpdate();
			if(updatedCount == 0){
				LOG.error("no connection limit found for service code "+serviceCode);
				throw new ConnectionLimitException("no connection limit found for service code "+serviceCode);
			}
			
			connectionLimit = new ConnectionLimit();
			connectionLimit.setServiceCode(serviceCode);
			connectionLimit.setConnectionLimit(conLimit);
			
		}catch (SQLException e) {
			LOG.error("Exception during updateConnectionLimit", e);
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			throw new ConnectionLimitException("Fatal error while updating connection limit :", e);
		} finally {
			if (conn != null) {
				try {
					conn.commit();
				} catch (SQLException e) {
					LOG.error("Exception during commit " + e);
				}
			}

			DBConnectionManager.getInstance().cleanup(conn, ps, null);
		}
		
		return connectionLimit;
	}

	@Override
	public boolean deleteConnectionLimit(long serviceCode) throws ConnectionLimitException {
		LOG.info("deleteConnectionLimit: deleting connection limit for service code: "+serviceCode);
		
		boolean deleted = false;
		Connection conn = null;
		PreparedStatement ps = null;
		
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);
			
			ps = conn.prepareStatement(SQL_DELETE_CONNECTION_LIMIT);

			ps.setLong(1, serviceCode);
			
			LOG.info("deleteConnectionLimit: executed query: " + SQL_DELETE_CONNECTION_LIMIT +" service_code :"+serviceCode);
			int deletedCount = ps.executeUpdate();
			
			if(deletedCount == 0){
				LOG.error("No connection limit found for service code :"+serviceCode);
				throw new ConnectionLimitException("No connection limit found for service code :"+serviceCode);
			}else{
				deleted = true;
			}
			
		}catch (SQLException e) {
			LOG.error("Exception during updateConnectionLimit", e);
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ConnectionLimitException("Fatal error while updating connection limit :", e);
		} finally {
			if (conn != null) {
				try {
					conn.commit();
				} catch (SQLException e) {
					LOG.error("Exception during commit " + e);
				}
			}
			DBConnectionManager.getInstance().cleanup(conn, ps, null);
		}
		
		return deleted;
	}

}
