package com.aeris.service.operatoradmin.exception;

public class OperatorNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public OperatorNotFoundException(String s) {
		super(s);
	}

	public OperatorNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
