package com.aeris.service.operatoradmin.exception;

public class GenericServiceException extends RuntimeException {
	private static final long serialVersionUID = 3856961344904116103L;

	public GenericServiceException(String s) {
		super(s);
	}

	public GenericServiceException(String ex, Throwable t) {
		super(ex, t);
	}
}
