package com.aeris.service.operatoradmin.exception;

public class SIMOrderNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SIMOrderNotFoundException(String s) {
		super(s);
	}

	public SIMOrderNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
