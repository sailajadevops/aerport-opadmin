package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.ISMPPConnectionEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.SMPPConnection;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * SMPPConnection Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class SMPPConnectionEnum implements ISMPPConnectionEnum, IEventListener {
	private Logger LOG = LoggerFactory.getLogger(SMPPConnectionEnum.class);
	
	static final String SQL_GET_SMPP_CONNECTIONS = "select connection_id, connection_name, product_id, carrier_id, smpp_id from smpp_connection";
	
	private Cache<String, SMPPConnection> cache;
	private Cache<String, SMPPConnection> cacheById;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "SMPPConnectionCache/name") Cache<String, SMPPConnection> cache, 
			@Hazelcast(cache = "SMPPConnectionCache/id") Cache<String, SMPPConnection> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_SMPP_CONNECTIONS);
			
			while (rs.next()) {
				SMPPConnection smppConnection = new SMPPConnection();
				
				smppConnection.setSmppConnectionId(rs.getInt("connection_id"));
				smppConnection.setSmppConnectionName(rs.getString("connection_name"));
				smppConnection.setProductId(rs.getInt("product_id"));
				smppConnection.setCarrierId(rs.getInt("carrier_id"));
				smppConnection.setSmppId(rs.getInt("smpp_id"));
				
				cache.put(smppConnection.getSmppConnectionName(), smppConnection);
				cacheById.put(String.valueOf(smppConnection.getSmppConnectionId()), smppConnection);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}
	
	@Override
	public SMPPConnection getSMPPConnectionByName(String name) {
		return cache.get(name);
	}

	@Override
	public SMPPConnection getSMPPConnectionById(int id) {
		return cacheById.get(String.valueOf(id));
	}
	
	@Override
	public SMPPConnection getSMPPConnectionBySmppId(int productId, int carrierId, int smppId) {
		List<SMPPConnection> smppConnections = getAllSMPPConnections();
		SMPPConnection smppConnection = null;
		
		for (SMPPConnection smppConn : smppConnections) {
			if(smppConn.getProductId() == productId && smppConn.getCarrierId() == carrierId && smppConn.getSmppId() == smppId)
			{
				smppConnection = smppConn;
				break;
			}
		}
		
		return smppConnection;		
	}

	@Override
	public List<SMPPConnection> getAllSMPPConnections() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
