package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum CCAccountType implements Serializable {
	PERSONAL, COMPANY
}
