package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.RES;

public interface IRESEnum extends IBaseEnum {
	List<RES> getRESList(int productId);
	boolean isValidRES(int productId, long resId);
	boolean isValid(String id);
	
}
