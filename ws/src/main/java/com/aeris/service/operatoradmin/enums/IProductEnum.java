package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.Product;

public interface IProductEnum  extends IBaseEnum{
	Product getProductByName(String name);
	Product getProductById(int id);
	List<Product> getAllProducts();
	boolean isValid(String id);
	void putToCache(String id, Product product);
}
