package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Date;

public class SprintCSA implements Serializable {

	private static final long serialVersionUID = -832768189648346914L;
	private String primaryCSA;
	private String secondaryCSA;
	private Date lastModifiedDate;
	private String lastModifiedBy;

	public void setPrimaryCSA(String primaryCSA) {
		this.primaryCSA = primaryCSA;
	}

	public String getPrimaryCSA() {
		return primaryCSA;
	}

	public void setSecondaryCSA(String secondaryCSA) {
		this.secondaryCSA = secondaryCSA;
	}

	public String getSecondaryCSA() {
		return secondaryCSA;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

}
