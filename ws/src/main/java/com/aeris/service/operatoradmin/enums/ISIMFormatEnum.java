package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.SIMFormat;

public interface ISIMFormatEnum  extends IBaseEnum{
	SIMFormat getSIMFormatByName(String name);
	SIMFormat getSIMFormatById(int id);
	List<SIMFormat> getAllSIMFormats();
}
