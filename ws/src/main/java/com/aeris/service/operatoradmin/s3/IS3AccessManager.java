package com.aeris.service.operatoradmin.s3;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import com.google.inject.ImplementedBy;

/**
 *
 * @author saurabh.sharma@aeris.net
 */
@ImplementedBy(S3AccessManager.class)
public interface IS3AccessManager {
    void shutdownAmazonClient();
    BufferedReader readFromS3(String key) throws IOException ;
    BufferedReader readFromS3(String bucketName, String key) throws IOException ;
    List<String> getAllFilesForDistributor(long accountId, long distributorId) ;
}	
