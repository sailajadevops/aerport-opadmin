package com.aeris.service.operatoradmin.events;

public interface IEventDispatcher {
	public abstract void propogateEvent(final Event event, Object... params);
}