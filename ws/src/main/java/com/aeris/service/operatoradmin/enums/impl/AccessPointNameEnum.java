package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.IAccessPointNameEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.AccessPointName;
import com.aeris.service.operatoradmin.model.AccessPointNameType;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * Resource Definition Cache used for lookup purposes 
 * 
 * @author SP00125222
 */
@Singleton
public class AccessPointNameEnum implements IAccessPointNameEnum, IEventListener {
	private Logger LOG = LoggerFactory.getLogger(AccessPointNameEnum.class);
	private Cache<String, Map<String, AccessPointName>> cache;
	private Cache<String, AccessPointName> cacheById;
	private Cache<String, List<AccessPointName>> cacheByContractId;
	
	@Inject
	private IStoredProcedure<List<AccessPointName>> getAccessPointNamesStoredProcedure;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "AccessPointNameCache/type") Cache<String, Map<String, AccessPointName>> cache, 
			@Hazelcast(cache = "AccessPointNameCache/id") Cache<String, AccessPointName> cacheById,
			@Hazelcast(cache = "AccessPointNameCache/contractId") Cache<String, List<AccessPointName>> cacheByContractId) {
		this.cache = cache;
		this.cacheById = cacheById;	
		this.cacheByContractId = cacheByContractId;
		loadResources();
		loadContractSpecificAPNs();
	}
	
	void loadContractSpecificAPNs(){
		LOG.info("loading Contract Specific APNs...");
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			String query = 	"select cam.APN_ID,cam.CONTRACT_ID, apn.APN_VALUE, apn.CARRIER_ID, asv.service from CONTRACT_APN_MAPPING cam " +
							"left join ACCESS_POINT_NAME apn on cam.APN_ID = apn.APN_ID "+
							"left join apn_service asv on apn.apn_service_id = asv.service_id ";
			ps = conn.prepareStatement(query);
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				int contractId = rs.getInt("CONTRACT_ID");
				int apnId = rs.getInt("APN_ID");
				String apnName = rs.getString("APN_VALUE");
				int carrierId = rs.getInt("CARRIER_ID");
				String apnType = rs.getString("service");
				
				AccessPointName apn = new AccessPointName();
				apn.setApnId(apnId);
				apn.setApnName(apnName);
				apn.setCarrierId(carrierId);
				apn.setApnType(AccessPointNameType.fromName(apnType));
				
				putContractAPNInCache(contractId, apn);
			}
			
			LOG.info("loaded Contract Specific APNs successfully..");
		} catch (SQLException e) {
			LOG.error("Exception during loadContractSpecificAPNs call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		}finally{
			DBUtils.cleanup(conn, ps, rs);
		}
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		try {
			
			// Call Get_Product Stored Procedure
			List<AccessPointName> accessPointNames = getAccessPointNamesStoredProcedure.execute();	
			
			for(AccessPointName accessPointName: accessPointNames) 
			{
				AccessPointNameType type = accessPointName.getApnType();
				
				if(type!= null)
				{
					Map<String, AccessPointName> apns = cache.get(type.name());
					
					if(apns == null)
					{
						apns = new HashMap<String, AccessPointName>(); 
					}
					
					apns.put(String.valueOf(accessPointName.getApnId()), accessPointName);					
					cache.put(type.name(), apns);
				}
				
				cacheById.put(String.valueOf(accessPointName.getApnId()), accessPointName);
			}
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during getAccessPointNamesStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		}
	}
	
	@Override
	public List<AccessPointName> getAccessPointNamesByByType(String type) {
		return new ArrayList<AccessPointName>(cache.get(type).values());
	}

	@Override
	public AccessPointName getAccessPointNameById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<AccessPointName> getAllAccessPointNames() {
		return new ArrayList<AccessPointName>(cacheById.getValues());
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}
	
	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
	
	private void putContractAPNInCache(int contractId, AccessPointName apn){
		String strContractId = String.valueOf(contractId);
		List<AccessPointName> apns = cacheByContractId.get(strContractId);
		if(apns == null){
			apns = new ArrayList<AccessPointName>();
			apns.add(apn);
			cacheByContractId.put(strContractId,apns);
		}else{
			apns.add(apn);
			cacheByContractId.put(strContractId,apns);
		}
		
	}
	
	@Override
	public List<AccessPointName> getAccessPointNamesByContract(String contractId, String type) {
		List<AccessPointName> apns = cacheByContractId.get(contractId);
		List<AccessPointName> response = new ArrayList<AccessPointName>();
		
		for(AccessPointName apn : apns){
			if(apn.getApnType().name().equalsIgnoreCase(type)){
				response.add(apn);
			}
		}
		
		return response;
	}
}
