package com.aeris.service.operatoradmin.dao;

import java.util.Date;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import com.aeris.service.operatoradmin.exception.SIMConfigNotFoundException;
import com.aeris.service.operatoradmin.exception.SIMOrderException;
import com.aeris.service.operatoradmin.exception.SIMOrderNotFoundException;
import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.SIMConfig;
import com.aeris.service.operatoradmin.model.SIMOrder;

public interface ICustomerSIMOrderDAO {
	List<SIMOrder> getAllSIMOrders(String operatorId, String accountId, String start, String count, OrderStatus... orderStatusList);

	SIMOrder getSIMOrderByOrderId(String operatorId, String accountId, String orderId);

	SIMOrder createSIMOrder(String operatorId, String accountId, int productId, int simFormat, long quantity, String description, String shippingAddress,
			String trackingNumber, String comments, Date requestedDate, String requestedUser) throws SIMConfigNotFoundException, SIMOrderException,
			AccountNotFoundException;

	List<SIMConfig> getSIMConfig(String operatorId, String accountId) throws SIMOrderException;

	boolean updateSIMOrder(String operatorId, String accountId, String orderId, long quantity, String description, String shippingAddress, OrderStatus status,
			String trackingNumber, String comments, Date requestedDate, String requestedUser) throws SIMOrderException, AccountNotFoundException,
			SIMOrderNotFoundException;

	boolean deleteSIMOrder(String operatorId, String accountId, String orderId, Date requestedDate, String requestedUser) throws SIMOrderException,
			AccountNotFoundException, SIMOrderNotFoundException;
}
