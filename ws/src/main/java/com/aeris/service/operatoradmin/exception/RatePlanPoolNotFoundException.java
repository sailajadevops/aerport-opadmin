package com.aeris.service.operatoradmin.exception;

public class RatePlanPoolNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public RatePlanPoolNotFoundException(String s) {
		super(s);
	}

	public RatePlanPoolNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
