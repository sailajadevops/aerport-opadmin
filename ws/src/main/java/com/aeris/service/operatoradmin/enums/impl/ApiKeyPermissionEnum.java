package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IApiKeyPermissionEnum;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.ApiKeyPermission;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * API Key Permissions Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class ApiKeyPermissionEnum implements IApiKeyPermissionEnum{
	private Logger LOG = LoggerFactory.getLogger(ApiKeyPermissionEnum.class);
	static final String SQL_GET_PERMISSIONS = "select permission_id, description from ws_permission";
	
	private Cache<String, ApiKeyPermission> cache;
	private Cache<String, ApiKeyPermission> cacheById;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "ApiKeyPermissionCache/name") Cache<String, ApiKeyPermission> cache, 
			@Hazelcast(cache = "ApiKeyPermissionCache/id") Cache<String, ApiKeyPermission> cacheById) {
		this.cache = cache;	
		this.cacheById = cacheById;
		loadPermissions();
	}
	
	/**
	 * Loads the Permissions object into the memory for future lookup.
	 */
	void loadPermissions() {
		LOG.info("loading permissions from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_PERMISSIONS);
			
			while (rs.next()) {
				ApiKeyPermission permission = new ApiKeyPermission();
				
				permission.setPermissionId(rs.getInt("permission_id"));
				permission.setPermissionValue(rs.getString("description"));
				
				cache.put(permission.getPermissionValue(), permission);
				cacheById.put(String.valueOf(permission.getPermissionId()), permission);
			}
			
			LOG.info("permissions loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadPermissions", e);
			throw new GenericServiceException("Unable to loadPermissions", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}
	
	/**
	 * Fetches the Permission Definition matching permission id
	 * 
	 * @param permissionId the permission id
	 * @return the Permission Definition
	 */
	@Override
	public ApiKeyPermission getPermissionById(int permissionId)
	{
		return cacheById.get(String.valueOf(permissionId));
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}
	
	/**
	 * Fetches the Permission Definition matching permission value
	 * 
	 * @param permissionValue the permission value
	 * @return the Permission Definition
	 */
	@Override
	public ApiKeyPermission getPermissionByValue(String permissionValue)
	{
		return cache.get(permissionValue);
	}
}
