package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.CspZone;

public interface ICspZoneEnum  extends IBaseEnum{
	CspZone getCspZoneByName(String name);
	CspZone getCspZoneById(int id);
	List<CspZone> getAllCspZones();
}
