package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class SIMConfig implements Serializable {
	private static final long serialVersionUID = 9195563621151320040L;
	private Product product;
	private List<SIMFormatConfig> simFormatConfig = new ArrayList<SIMConfig.SIMFormatConfig>();

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<SIMFormatConfig> getSIMFormatConfig() {
		return simFormatConfig;
	}

	public void addSIMFormat(SIMFormat simFormat, double cost) {
		SIMFormatConfig config = new SIMFormatConfig();
		config.simFormat = simFormat;
		config.cost = cost;

		simFormatConfig.add(config);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SIMConfig)) {
			return super.equals(obj);
		}

		SIMConfig config = (SIMConfig) obj;

		if (product != null) {
			return product.equals(config.getProduct());
		}

		return false;
	}

	class SIMFormatConfig {
		SIMFormat simFormat;
		double cost;

		public SIMFormat getSimFormat() {
			return simFormat;
		}

		public void setSimFormat(SIMFormat simFormat) {
			this.simFormat = simFormat;
		}

		public double getCost() {
			return cost;
		}

		public void setCost(double cost) {
			this.cost = cost;
		}
	}

}
