package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.SMPPConnection;

public interface ISMPPConnectionEnum  extends IBaseEnum{
	SMPPConnection getSMPPConnectionByName(String name);
	SMPPConnection getSMPPConnectionById(int id);
	List<SMPPConnection> getAllSMPPConnections();
	SMPPConnection getSMPPConnectionBySmppId(int productId, int carrierId, int smppId);
}
