package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public class ImsiRange implements Serializable {
	private static final long serialVersionUID = -2108681718789026951L;
	private String range;
	private IMSIRangeType rangeType;
	private long currentImsi;
	private long minImsi;
	private long maxImsi;
	private int productId;
	private int operatorId;

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	} 

	public IMSIRangeType getRangeType() {
		return rangeType;
	}

	public void setRangeType(IMSIRangeType rangeType) {
		this.rangeType = rangeType;
	}

	public long getCurrentImsi() {
		return currentImsi;
	}

	public void setCurrentImsi(long currentImsi) {
		this.currentImsi = currentImsi;
	}

	public long getMinImsi() {
		return minImsi;
	}

	public void setMinImsi(long minImsi) {
		this.minImsi = minImsi;
	}

	public long getMaxImsi() {
		return maxImsi;
	}

	public void setMaxImsi(long maxImsi) {
		this.maxImsi = maxImsi;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	public int getOperatorId() {
		return operatorId;
	}
	
	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}
}