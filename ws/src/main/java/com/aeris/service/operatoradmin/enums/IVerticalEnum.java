package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.Vertical;

public interface IVerticalEnum  extends IBaseEnum{
	Vertical getVerticalByName(String name);
	Vertical getVerticalById(int id);
	List<Vertical> getAllVerticals();
}
