package com.aeris.service.operatoradmin.exception;

public class SIMOrderException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SIMOrderException(String s) {
		super(s);
	}

	public SIMOrderException(String ex, Throwable t) {
		super(ex, t);
	}
}
