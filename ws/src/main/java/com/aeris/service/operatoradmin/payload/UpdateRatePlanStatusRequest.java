package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;
import java.util.Date;

@XmlRootElement
public class UpdateRatePlanStatusRequest implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;

	@Email(message = "{userId.email}")
	@NotNull(message = "{userId.notnull}")
	private String userId;

	@Enumeration(value = RatePlanStatus.class, message = "{status.enum}")
	private RatePlanStatus status;

	private String specialNotes;
    
    private Date newEndDate;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public RatePlanStatus getStatus() {
		return status;
	}

	public void setStatus(RatePlanStatus status) {
		this.status = status;
	}
	
	public String getSpecialNotes() {
		return specialNotes;
	}
	
	public void setSpecialNotes(String specialNotes) {
		this.specialNotes = specialNotes;
	}

    public Date getNewEndDate() {
        return newEndDate;
    }

    public void setNewEndDate(Date newEndDate) {
        this.newEndDate = newEndDate;
    }
    
}
