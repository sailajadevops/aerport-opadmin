package com.aeris.service.operatoradmin.exception;

public class DuplicateServiceProfileException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DuplicateServiceProfileException(String s) {
		super(s);
	}

	public DuplicateServiceProfileException(String ex, Throwable t) {
		super(ex, t);
	}
}
