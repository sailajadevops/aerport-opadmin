package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class AccountRatePlanPool implements Serializable {
	private static final long serialVersionUID = -2520433767678088508L;
	private long ratePlanPoolId;
	private String ratePlanPoolName;
	private List<String> ratePlans;
	private long accountId;
	private RatePlanPoolStatus status;
	private Date startDate;
	private Date endDate;
	private int productId;
	private int operatorId;
	private List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBucket ;
	
	public long getRatePlanPoolId() {
		return ratePlanPoolId;
	}

	public void setRatePlanPoolId(long ratePlanPoolId) {
		this.ratePlanPoolId = ratePlanPoolId;
	}

	public String getRatePlanPoolName() {
		return ratePlanPoolName;
	}

	public void setRatePlanPoolName(String ratePlanPoolName) {
		this.ratePlanPoolName = ratePlanPoolName;
	}

	public List<String> getRatePlans() {
		return ratePlans;
	}

	public void setRatePlans(List<String> ratePlans) {
		this.ratePlans = ratePlans;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public RatePlanPoolStatus getStatus() {
		return status;
	}

	public void setStatus(RatePlanPoolStatus status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getProductId() {
		return productId;
	}
	
	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	public int getOperatorId() {
		return operatorId;
	}
	
	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof AccountRatePlanPool)){
			return false;
		}
		
		AccountRatePlanPool other = (AccountRatePlanPool) obj;
	
		return new EqualsBuilder().append(other.getAccountId(), this.getAccountId()).
			append(other.getRatePlanPoolId(), this.getRatePlanPoolId()).
			isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().
					append(accountId).
					append(getRatePlanPoolId()).
					toHashCode();
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public List<AccountRatePlanPoolOverageBucket> getRatePlanPoolOverageBucket() {
		return ratePlanPoolOverageBucket;
	}

	public void setRatePlanPoolOverageBucket(
			List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBucket) {
		this.ratePlanPoolOverageBucket = ratePlanPoolOverageBucket;
	}
}
