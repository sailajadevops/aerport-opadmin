package com.aeris.service.operatoradmin.payload;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Email;

@XmlRootElement
public class CreateUniqueCodesRequest {

    @Email(message = "{userEmail.email}")
    @NotNull(message = "{userEmail.notnull}")
    private String userEmail;
    @NotNull(message = "{numberOfUniqueCodes.notnull}")
    private int numberOfUniqueCodes;
    @NotNull(message = "{distributorAccountId.notnull}")
    private long distributorAccountId;
    @NotNull(message = "{emailRecipients.notnull}")
    private String[] emailRecipients;

    private long operatorId;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public int getNumberOfUniqueCodes() {
        return numberOfUniqueCodes;
    }

    public void setNumberOfUniqueCodes(int numberOfUniqueCodes) {
        this.numberOfUniqueCodes = numberOfUniqueCodes;
    }

    public long getDistributorAccountId() {
        return distributorAccountId;
    }

    public void setDistributorAccountId(long distributorAccountId) {
        this.distributorAccountId = distributorAccountId;
    }

    public String[] getEmailRecipients() {
        return emailRecipients;
    }

    public void setEmailRecipients(String[] emailRecipients) {
        this.emailRecipients = emailRecipients;
    }

    public long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(long operatorId) {
        this.operatorId = operatorId;
    }

    @Override
    public String toString() {
        return "CreateUniqueCodesRequest{" + "userEmail=" + userEmail + ", numberOfUniqueCodes=" + numberOfUniqueCodes + ", distributorAccountId=" + distributorAccountId + ", emailRecipients=" + emailRecipients + ", operatorId=" + operatorId + '}';
    }

}
