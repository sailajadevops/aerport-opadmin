package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * service_profile_technology mapping DTO for service profile creation
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@XmlRootElement
public class TechnologyServiceProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2978852539839348191L;
	
	private String technology;
	private String msisdnRange ;
	private String staticIPRange;
	private int serviceProviderId;
	private Integer commPlanId;
	
	public String getTechnology() {
		return technology;
	}
	public void setTechnology(String technology) {
		this.technology = technology;
	}
	public String getMsisdnRange() {
		return msisdnRange;
	}
	public void setMsisdnRange(String msisdnRange) {
		this.msisdnRange = msisdnRange;
	}
	public String getStaticIPRange() {
		return staticIPRange;
	}
	public void setStaticIPRange(String staticIPRange) {
		this.staticIPRange = staticIPRange;
	}
	public int getServiceProviderId() {
		return serviceProviderId;
	}
	public void setServiceProviderId(int serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Integer getCommPlanId() {
		return commPlanId;
	}
	public void setCommPlanId(Integer commPlanId) {
		this.commPlanId = commPlanId;
	}
	public static void main(String[] args) throws Exception {
		TechnologyServiceProfile temp = new TechnologyServiceProfile();
		temp.setMsisdnRange("123434343");
		temp.setServiceProviderId(7);
		temp.setStaticIPRange("434343-34423");
		temp.setTechnology("GSM");
		List<TechnologyServiceProfile> list = new ArrayList<TechnologyServiceProfile>();
		list.add(temp);
		System.out.println(new ObjectMapper().writeValueAsString(list));;
		
	}
}