package com.aeris.service.operatoradmin.client;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.model.ApplicationProperties;
import com.aeris.service.operatoradmin.model.HssApnConfig;
import com.aeris.service.operatoradmin.model.HssPSConfig;
import com.aeris.service.operatoradmin.model.HssProfile;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

@Singleton
public class AS4gClient {

    private static final Logger LOG = LoggerFactory.getLogger(AS4gClient.class);
    @Inject
    private ApplicationProperties appProperties;

    private WebTarget getWebTarget(String path) {
        ClientConfig cc = new ClientConfig().register(new JacksonFeature());
        Client client = ClientBuilder.newClient(cc);
        WebTarget target = client.target(appProperties.getAs4gUrl() + path);
        return target;
    }

    public List<HssProfile> getHssProfiles() {
        LOG.info("getHssProfiles(): starts");

	//This is specifically needed for aircel since aircel will not use LTE implementation
	if("aircel".equalsIgnoreCase(System.getProperty("operator"))) {
		return new ArrayList<HssProfile>();
	}

        WebTarget target = getWebTarget("/hss/serviceprofiles");
        GenericType<GetHssProfilesResponse> getHssProfilesResponse = new GenericType<GetHssProfilesResponse>() {
        };
        try {
            GetHssProfilesResponse result = target.request(MediaType.APPLICATION_JSON_TYPE).get(getHssProfilesResponse);
            List<HssProfile> serviceProfileList = result.getServiceProfileList();
            if (serviceProfileList != null) {
                LOG.info("getHssProfiles(): Number of HSS profiles : " + serviceProfileList.size());
                return serviceProfileList;
            } else {
                LOG.info("getHssProfiles(): Number of HSS profiles : 0");
            }
        } catch (Exception ex) {
            LOG.error("Error occurred while getting HSS profiles from As4g api /hss/serviceprofiles : ", ex);
        }
        LOG.info("getHssProfiles(): ends");
        return new ArrayList<HssProfile>();
    }

    public List<HssPSConfig> getHssPSConfigs() {
        LOG.info("getHssPSConfigs(): starts");
        WebTarget target = getWebTarget("/hss/psconfigs");
        GenericType<GetHssPSConfigsResponse> getHssPSConfigsResponse = new GenericType<GetHssPSConfigsResponse>() {
        };
        try {
            GetHssPSConfigsResponse result = target.request(MediaType.APPLICATION_JSON_TYPE).get(getHssPSConfigsResponse);
            List<HssPSConfig> configList = result.getConfigList();
            if (configList != null) {
                LOG.info("getHssPSConfigs(): Number of HSS PS Configs : " + configList.size());
                return configList;
            } else {
                LOG.info("getHssPSConfigs(): Number of HSS PS Configs : 0");
            }
        } catch (Exception ex) {
            LOG.error("Error occurred while getting HSS PS Configs from As4g api /hss/serviceprofiles : ", ex);
        }
        LOG.info("getHssPSConfigs(): ends");
        return new ArrayList<HssPSConfig>();
    }

    public List<HssApnConfig> getHssApnConfigs() {
        LOG.info("getHssApnConfigs(): starts");
        WebTarget target = getWebTarget("/hss/apnconfigs");
        GenericType<GetHssApnConfigsResponse> getHssApnConfigsResponse = new GenericType<GetHssApnConfigsResponse>() {
        };
        try {
            GetHssApnConfigsResponse result = target.request(MediaType.APPLICATION_JSON_TYPE).get(getHssApnConfigsResponse);
            List<HssApnConfig> configList = result.getConfigList();
            if (configList != null) {
                LOG.info("getHssApnConfigs(): Number of HSS APN Configs : " + configList.size());
                return configList;
            } else {
                LOG.info("getHssApnConfigs(): Number of HSS APN Configs : 0");
            }
        } catch (Exception ex) {
            LOG.error("Error occurred while getting HSS APN Configs from As4g api /hss/apnconfigs : ", ex);
        }
        LOG.info("getHssPSConfigs(): ends");
        return new ArrayList<HssApnConfig>();
    }
}

class GetHssProfilesResponse {

    private List<HssProfile> serviceProfileList;

    public List<HssProfile> getServiceProfileList() {
        return serviceProfileList;
    }

    public void setServiceProfileList(List<HssProfile> serviceProfileList) {
        this.serviceProfileList = serviceProfileList;
    }
}

class GetHssPSConfigsResponse {

    private List<HssPSConfig> configList;

    public List<HssPSConfig> getConfigList() {
        return configList;
    }

    public void setConfigList(List<HssPSConfig> configList) {
        this.configList = configList;
    }
}

class GetHssApnConfigsResponse {

    private List<HssApnConfig> configList;

    public List<HssApnConfig> getConfigList() {
        return configList;
    }

    public void setConfigList(List<HssApnConfig> configList) {
        this.configList = configList;
    }
}
