package com.aeris.service.operatoradmin.exception;

public class DuplicateZoneSetException  extends Exception {

	private static final long serialVersionUID = 5263347150936560600L;

	public DuplicateZoneSetException(String s) {
		super(s);
	}

	public DuplicateZoneSetException(String ex, Throwable t) {
		super(ex, t);
	}
}
