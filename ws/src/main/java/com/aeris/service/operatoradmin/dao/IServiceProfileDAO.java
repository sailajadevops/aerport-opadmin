package com.aeris.service.operatoradmin.dao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.DuplicateServiceProfileException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.ServiceProfileException;
import com.aeris.service.operatoradmin.exception.ServiceProfileNotFoundException;
import com.aeris.service.operatoradmin.exception.ServiceProfileValidationException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.APNConfiguration;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.SMPPConfiguration;
import com.aeris.service.operatoradmin.model.ServiceProfile;
import com.aeris.service.operatoradmin.model.TechnologyServiceProfile;
import com.aeris.service.operatoradmin.model.ZoneServiceProfile;

public interface IServiceProfileDAO {
	List<ServiceProfile> getAllServiceProfiles(String operatorId, String accountId, boolean extendAttributes);

	ServiceProfile getServiceProfile(String operatorId, String accountId, long serviceCode, boolean extendAttributes);

	ServiceProfile createNewServiceProfile(String operatorId, String accountId, String serviceProfileName, String serviceUsername, String servicePassword,
			int productId, int serviceProviderId, List<ZoneServiceProfile> zoneProfiles, List<TechnologyServiceProfile> technologyProfiles, String regexMOVoice, EnrolledServices enrolledServices, 
			SMPPConfiguration smppConfig, boolean smsNonAerisDevice, APNConfiguration apnConfig, String hssProfileId, String pccProfileId, int zoneSetId, boolean notUsed, String technology, Date requestedDate, String requestedUser)
			throws ServiceProfileException, DuplicateServiceProfileException, ZoneNotFoundException, DuplicateZoneException, ServiceProfileValidationException;
	
	ServiceProfile createNewServiceProfile(String operatorId, String accountId, String serviceProfileName, String serviceUsername, String servicePassword,
			int productId, int serviceProviderId, List<ZoneServiceProfile> zoneProfiles, List<TechnologyServiceProfile> technologyProfiles, String regexMOVoice, EnrolledServices enrolledServices, 
			SMPPConfiguration smppConfig, boolean smsNonAerisDevice, APNConfiguration apnConfig, String hssProfileId, String pccProfileId, int zoneSetId, boolean notUsed, String technology, Date requestedDate, String requestedUser, boolean validateCommPLan)
			throws ServiceProfileException, DuplicateServiceProfileException, ZoneNotFoundException, DuplicateZoneException, ServiceProfileValidationException;

	ServiceProfile updateServiceProfile(String operatorId, String accountId, long serviceCode, String serviceProfileName, String serviceUsername,
			String servicePassword, int productId, int serviceProviderId, List<ZoneServiceProfile> zoneProfiles, List<TechnologyServiceProfile> technologyProfiles, String regexMOVoice, 
			EnrolledServices enrolledServices, SMPPConfiguration smppConfig, boolean smsNonAerisDevice, APNConfiguration apnConfig, String hssProfileId, String pccProfileId, int zoneSetId, boolean notUsed, String technology, Date requestedDate, String requestedUser)
			throws ServiceProfileNotFoundException, DuplicateServiceProfileException, ZoneNotFoundException, ServiceProfileException, DuplicateZoneException, ServiceProfileValidationException;
	
	ServiceProfile updateServiceProfile(String operatorId, String accountId, long serviceCode, String serviceProfileName, String serviceUsername,
			String servicePassword, int productId, int serviceProviderId, List<ZoneServiceProfile> zoneProfiles, List<TechnologyServiceProfile> technologyProfiles, String regexMOVoice, 
			EnrolledServices enrolledServices, SMPPConfiguration smppConfig, boolean smsNonAerisDevice, APNConfiguration apnConfig, String hssProfileId, String pccProfileId, int zoneSetId, boolean notUsed, String technology, Date requestedDate, String requestedUser, boolean validateCommPlan)
			throws ServiceProfileNotFoundException, DuplicateServiceProfileException, ZoneNotFoundException, ServiceProfileException, DuplicateZoneException, ServiceProfileValidationException;

	boolean deleteServiceProfile(String operatorId, String accountId, long serviceCode, Date requestedDate, String requestedUser)
			throws ServiceProfileNotFoundException, ServiceProfileException, ServiceProfileValidationException;
	
	public String getCarrierId(String accountId, String productId) throws SQLException;
	
	public List<ServiceProfile> getServiceProfiles(String accountId, long serviceCode, String serviceName, String serviceLoginName) throws ServiceProfileException;
}
