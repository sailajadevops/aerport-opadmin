package com.aeris.service.operatoradmin.exception;

public class DuplicateRatePlanException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DuplicateRatePlanException(String s) {
		super(s);
	}

	public DuplicateRatePlanException(String ex, Throwable t) {
		super(ex, t);
	}
}
