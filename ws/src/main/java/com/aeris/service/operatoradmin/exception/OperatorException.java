package com.aeris.service.operatoradmin.exception;

public class OperatorException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public OperatorException(String s) {
		super(s);
	}

	public OperatorException(String ex, Throwable t) {
		super(ex, t);
	}
}
