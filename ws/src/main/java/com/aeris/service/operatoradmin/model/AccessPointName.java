package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AccessPointName implements Serializable{
	private static final long serialVersionUID = 7478576606994364514L;
	private int apnId;
	private String apnName;
	private AccessPointNameType apnType;
	private int carrierId;
	
	public int getApnId() {
		return apnId;
	}

	public void setApnId(int apnId) {
		this.apnId = apnId;
	}

	public String getApnName() {
		return apnName;
	}

	public void setApnName(String apnName) {
		this.apnName = apnName;
	}

	public AccessPointNameType getApnType() {
		return apnType;
	}

	public void setApnType(AccessPointNameType apnType) {
		this.apnType = apnType;
	}
	
	public int getCarrierId() {
		return carrierId;
	}
	
	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof AccessPointName))
		{
			return false;
		}
		
		AccessPointName other = (AccessPointName) obj;
		
		return new EqualsBuilder().
				append(other.getApnId(), this.apnId).
				append(other.getApnName(), this.apnName).
				append(other.getCarrierId(), this.carrierId).
				isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(apnId).append(apnName).append(carrierId).hashCode();
	}
}
