package com.aeris.service.operatoradmin.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.model.ApplicationProperties;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class AerCloudClient{
	private static final Logger LOG = LoggerFactory.getLogger(AerCloudClient.class);
	
	@Inject
	private ApplicationProperties appProperties;
	
	public AerCloudResponse createAerCloudAccount(long accountId, String apiKey)
	{
		LOG.info("Creating aer cloud account for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
	    Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAercloudAccountUrl()); 
		
		// Payload
		CreateAerCloudAccountRequest payload = new CreateAerCloudAccountRequest();
		payload.setAccountId(String.valueOf(accountId));
		payload.setAccessRightID(apiKey);
		
		Response result = target.
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
				
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
	
	public AerCloudResponse createAerCloudDefaultDataModel(long accountId, String apiKey)
	{
		LOG.info("Creating aer cloud default data model for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAercloudDataModelUrl());
		
		// Payload
		CreateAerCloudDataModelRequest payload = createAerCloudDefaultDataModelRequest(accountId, apiKey);
		
		Response result = target.
				resolveTemplate("accountId", payload.getAccountId()).
				queryParam("apiKey", payload.getApiKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
			
	public AerCloudResponse createAerCloudAlertDataModel(long accountId, String apiKey)
	{
		LOG.info("Creating aer cloud alerts data model for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAercloudDataModelUrl());
		
		// Payload
		CreateAerCloudDataModelRequest payload = createAerCloudAlertDataModelRequest(accountId, apiKey);
		
		Response result = target.
				resolveTemplate("accountId", payload.getAccountId()).
				queryParam("apiKey", payload.getApiKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
	
	public AerCloudResponse createAerCloudEventStreamDataModel(long accountId, String apiKey)
	{
		LOG.info("Creating aer cloud aer event stream data model for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAercloudDataModelUrl());
		
		// Payload
		CreateAerCloudDataModelRequest payload = createAerCloudEventStreamDataModelRequest(accountId, apiKey);
		
		Response result = target.
				resolveTemplate("accountId", payload.getAccountId()).
				queryParam("apiKey", payload.getApiKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
		
	public AerCloudResponse createAerCloudDefaultContainer(long accountId, String apiKey)
	{
		LOG.info("Creating aer cloud default container for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAercloudContainerUrl()); 

		// Payload
		CreateAerCloudContainerRequest payload = createAerCloudDefaultContainerRequest(accountId, apiKey);
				
		Response result = target.
				resolveTemplate("accountId", payload.getAccountId()).
				queryParam("apiKey", payload.getApiKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}

	public AerCloudResponse createAerCloudAlertContainer(long accountId, String apiKey)
	{
		LOG.info("Creating aer cloud account alerts container for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAercloudContainerUrl()); 

		// Payload
		CreateAerCloudContainerRequest payload = createAerCloudAlertContainerRequest(accountId, apiKey);
				
		Response result = target.
				resolveTemplate("accountId", payload.getAccountId()).
				queryParam("apiKey", payload.getApiKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;	
	}

	public AerCloudResponse createAerCloudEventStreamContainer(long accountId, String apiKey)
	{
		LOG.info("Creating aer cloud aer event stream container for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAercloudContainerUrl()); 

		// Payload
		CreateAerCloudContainerRequest payload = createAerCloudEventStreamContainerRequest(accountId, apiKey);
				
		Response result = target.
				resolveTemplate("accountId", payload.getAccountId()).
				queryParam("apiKey", payload.getApiKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;	
	}
	
	public AerCloudResponse persistAerCloudDevice(long accountId, String apiKey)
	{
		LOG.info("Persisting aer cloud my alerts app device for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();

		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAercloudDevicePersistUrl()); 
		
		// Payload
		PersistAerCloudDeviceRequest payload = createAerCloudPersistDeviceRequest(accountId, apiKey);				
		
		Response result = target.
				resolveTemplate("accountId", payload.getAccountID()).
				queryParam("apiKey", payload.getApiKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;	
	}
	
	public AerCloudResponse provisionAerCloudDevice(long accountId, String apiKey)
	{
		LOG.info("Persisting aer cloud my alerts app device for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();

		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getAeradminDeviceProvisionUrl()); 
		
		// Payload
		ProvisionAerCloudDeviceRequest payload = createProvisionDeviceRequest(accountId, apiKey);				
		
		Response result = target.
				queryParam("apiKey", payload.getApiKey()).
				request().post(Entity.entity(payload, MediaType.APPLICATION_JSON));
		
		LOG.info("POSTing to Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;	
	}
		
	public AerCloudResponse fetchAerCloudAccount(long accountId)
	{
		LOG.info("Fetching aer cloud account for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
	    Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getFetchAercloudAccountUrl()); 
		
		Response result = target.
				resolveTemplate("accountId", accountId).
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().get();
		
		LOG.info("DELETEd Aercloud URL: "+result.getLocation());		
				
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
	
	public AerCloudResponse deleteAerCloudAccount(long accountId)
	{
		LOG.info("deleting aer cloud account for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
	    Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getDeleteAercloudAccountUrl()); 
		
		Response result = target.
				resolveTemplate("accountId", accountId).
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().delete();
		
		LOG.info("DELETEd Aercloud URL: "+result.getLocation());		
				
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
		
	public AerCloudResponse deleteAerCloudDefaultDataModel(long accountId)
	{
		LOG.info("Deleting aer cloud default data model for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getDeleteAercloudDataModelUrl());
		
		Response result = target.
				resolveTemplate("accountId", accountId).
				resolveTemplate("dataModelId", appProperties.getDefaultDataModelId()).
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().delete();
		
		LOG.info("DELETEd Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
		
	public AerCloudResponse deleteAerCloudDefaultContainer(long accountId)
	{
		LOG.info("Deleting aer cloud default data model for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getDeleteAercloudContainerUrl());
		
		Response result = target.
				resolveTemplate("accountId", accountId).
				resolveTemplate("containerId", appProperties.getDefaultContainerId()).
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().delete();
		
		LOG.info("DELETEd Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
	
	public AerCloudResponse deleteAerCloudAlertsDataModel(long accountId)
	{
		LOG.info("Deleting aer cloud alerts data model for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getDeleteAercloudDataModelUrl());
		
		Response result = target.
				resolveTemplate("accountId", accountId).
				resolveTemplate("dataModelId", appProperties.getMyAlertsDataModelId()).
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().delete();
		
		LOG.info("DELETEd Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
		
	public AerCloudResponse deleteAerCloudAlertsContainer(long accountId)
	{
		LOG.info("Deleting aer cloud alerts container for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getDeleteAercloudContainerUrl());
		
		Response result = target.
				resolveTemplate("accountId", accountId).
				resolveTemplate("containerId", appProperties.getMyAlertsContainerId()).
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().delete();
		
		LOG.info("DELETEd Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}	
	
	public AerCloudResponse deleteAerCloudEventStreamDataModel(long accountId)
	{
		LOG.info("Deleting aer cloud event stream data model for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getDeleteAercloudDataModelUrl());
		
		Response result = target.
				resolveTemplate("accountId", accountId).
				resolveTemplate("dataModelId", appProperties.getAerEventStreamDataModelId()).
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().delete();
		
		LOG.info("DELETEd Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}
		
	public AerCloudResponse deleteAerCloudEventStreamContainer(long accountId)
	{
		LOG.info("Deleting aer cloud event stream container for account id: "+accountId);
		
		AerCloudResponse response = new AerCloudResponse();
		
		ClientConfig cc = new ClientConfig().register(new JacksonFeature());
		Client client = ClientBuilder.newClient(cc); 
		WebTarget target = client.target(appProperties.getDeleteAercloudContainerUrl());
		
		Response result = target.
				resolveTemplate("accountId", accountId).
				resolveTemplate("containerId", appProperties.getAerEventStreamContainerId()).
				queryParam("apiKey", appProperties.getAercloudAdminKey()).
				request().delete();
		
		LOG.info("DELETEd Aercloud URL: "+result.getLocation());		
		
		response.setResponseCode(result.getStatus());
		
		if(result.hasEntity())
		{
			response.setResponseMessage(result.readEntity(String.class));
		}
		
		if(response.isOK())
		{
			LOG.info("Aercloud Response: OK");	
		}
		else
		{
			LOG.info("Aercloud Response: Failed with Status: "+response.getResponseCode()+", Message: "+response.getResponseMessage());
		}		
		
		return response;
	}	
	
	private CreateAerCloudDataModelRequest createAerCloudDefaultDataModelRequest(long accountId, String apiKey)
	{
        CreateAerCloudDataModelRequest dataModel = new CreateAerCloudDataModelRequest();
        dataModel.setAccountId(String.valueOf(accountId));
        dataModel.setApiKey(apiKey);
        dataModel.setName(appProperties.getDefaultDataModelName());
        dataModel.setId(appProperties.getDefaultDataModelId());
        dataModel.setDescription("This is default data model created for your account. Please don't delete.");
        SclDataSchema sclDataSchema = new SclDataSchema();
        sclDataSchema.setEncoding("JSON");
        Parameter[] parameters = new Parameter[4];
        Parameter paramLocationTimeStamp = new Parameter();
        paramLocationTimeStamp.setName("LocationTimeStamp");
        paramLocationTimeStamp.setType("INT");
        paramLocationTimeStamp.setIsIndexed("true");
        MetaInfo metaInfoLocationTimeStamp = new MetaInfo();
        metaInfoLocationTimeStamp.setUom("millisecond");
        paramLocationTimeStamp.setMetainfo(metaInfoLocationTimeStamp);
        parameters[0] = paramLocationTimeStamp;
        Parameter paramLatitude = new Parameter();
        paramLatitude.setName("Latitude");
        paramLatitude.setType("FLOAT");
        paramLatitude.setIsIndexed("true");
        MetaInfo metaInfoLatitude = new MetaInfo();
        metaInfoLatitude.setUom("second");
        paramLatitude.setMetainfo(metaInfoLatitude);
        parameters[1] = paramLatitude;
        Parameter paramLongitude = new Parameter();
        paramLongitude.setName("Longitude");
        paramLongitude.setType("FLOAT");
        paramLongitude.setIsIndexed("true");
        MetaInfo metaInfoLongitude = new MetaInfo();
        metaInfoLongitude.setUom("second");
        paramLongitude.setMetainfo(metaInfoLongitude);
        parameters[2] = paramLongitude;
        Parameter paramAccuracy = new Parameter();
        paramAccuracy.setName("Accuracy");
        paramAccuracy.setType("FLOAT");
        paramAccuracy.setIsIndexed("false");
        MetaInfo metaInfoAccuracy = new MetaInfo();
        metaInfoAccuracy.setUom("meter");
        paramAccuracy.setMetainfo(metaInfoAccuracy);
        parameters[3] = paramAccuracy;
        sclDataSchema.setParameters(parameters);
        dataModel.setSclDataSchema(sclDataSchema);
        
        return dataModel;
	}
		
	private CreateAerCloudContainerRequest createAerCloudDefaultContainerRequest(long accountId, String apiKey)
	{
        CreateAerCloudContainerRequest container = new CreateAerCloudContainerRequest();
        container.setAccountId(String.valueOf(accountId));
        container.setApiKey(apiKey);
        container.setId(appProperties.getDefaultContainerId());
        container.setSclDataModelId(appProperties.getDefaultDataModelId());
        
        return container;
	}
	
	private CreateAerCloudDataModelRequest createAerCloudAlertDataModelRequest(long accountId, String apiKey)
	{
        CreateAerCloudDataModelRequest dataModel = new CreateAerCloudDataModelRequest();
        dataModel.setAccountId(String.valueOf(accountId));
        dataModel.setApiKey(apiKey);
        dataModel.setName(appProperties.getMyAlertsDataModelName());
        dataModel.setId(appProperties.getMyAlertsDataModelId());
        dataModel.setDescription("This is default My Alerts data model created for your account. Please don't delete.");

        SclDataSchema sclDataSchema = new SclDataSchema();
        sclDataSchema.setEncoding("JSON");

        Parameter[] parameters = new Parameter[17];

        Parameter paramActualValue = new Parameter();
        paramActualValue.setName("actualValue");
        paramActualValue.setType("FLOAT");
        paramActualValue.setIsIndexed("false");
        MetaInfo metaInfoActualValue = getMetaInfo();
        paramActualValue.setMetainfo(metaInfoActualValue);
        parameters[0] = paramActualValue;

        Parameter paramAlertDate_UTC = new Parameter();
        paramAlertDate_UTC.setName("alertDate");
        paramAlertDate_UTC.setType("STRING");
        paramAlertDate_UTC.setIsIndexed("false");
        MetaInfo metaInfoAlertDate_UTC = getMetaInfo();
        paramAlertDate_UTC.setMetainfo(metaInfoAlertDate_UTC);
        parameters[1] = paramAlertDate_UTC;

        Parameter paramAlertName = new Parameter();
        paramAlertName.setName("alertName");
        paramAlertName.setType("STRING");
        paramAlertName.setIsIndexed("false");
        MetaInfo metaInfoAlertName = getMetaInfo();
        paramAlertName.setMetainfo(metaInfoAlertName);
        parameters[2] = paramAlertName;

        Parameter paramAlertSubType = new Parameter();
        paramAlertSubType.setName("alertSubType");
        paramAlertSubType.setType("STRING");
        paramAlertSubType.setIsIndexed("false");
        MetaInfo metaInfoAlertSubType = getMetaInfo();
        paramAlertSubType.setMetainfo(metaInfoAlertSubType);
        parameters[3] = paramAlertSubType;

        Parameter paramAlertType = new Parameter();
        paramAlertType.setName("alertType");
        paramAlertType.setType("STRING");
        paramAlertType.setIsIndexed("false");
        MetaInfo metaInfoAlertType = getMetaInfo();
        paramAlertType.setMetainfo(metaInfoAlertType);
        parameters[4] = paramAlertType;

        Parameter paramDeviceStatus = new Parameter();
        paramDeviceStatus.setName("deviceStatus");
        paramDeviceStatus.setType("STRING");
        paramDeviceStatus.setIsIndexed("false");
        MetaInfo metaDeviceStatus = getMetaInfo();
        paramDeviceStatus.setMetainfo(metaDeviceStatus);
        parameters[5] = paramDeviceStatus;

        Parameter paramESN = new Parameter();
        paramESN.setName("esn");
        paramESN.setType("STRING");
        paramESN.setIsIndexed("false");
        MetaInfo metaInfoESN = getMetaInfo();
        paramESN.setMetainfo(metaInfoESN);
        parameters[6] = paramESN;

        Parameter paramGroupType = new Parameter();
        paramGroupType.setName("groupType");
        paramGroupType.setType("STRING");
        paramGroupType.setIsIndexed("false");
        MetaInfo metaInfoGroupType = getMetaInfo();
        paramGroupType.setMetainfo(metaInfoGroupType);
        parameters[7] = paramGroupType;

        Parameter paramICCID = new Parameter();
        paramICCID.setName("iccid");
        paramICCID.setType("STRING");
        paramICCID.setIsIndexed("false");
        MetaInfo metaInfoICCID = getMetaInfo();
        paramICCID.setMetainfo(metaInfoICCID);
        parameters[8] = paramICCID;

        Parameter paramIMSI = new Parameter();
        paramIMSI.setName("imsi");
        paramIMSI.setType("STRING");
        paramIMSI.setIsIndexed("false");
        MetaInfo metaInfoIMSI = getMetaInfo();
        paramIMSI.setMetainfo(metaInfoIMSI);
        parameters[9] = paramIMSI;

        Parameter paramMEID = new Parameter();
        paramMEID.setName("meid");
        paramMEID.setType("STRING");
        paramMEID.setIsIndexed("false");
        MetaInfo metaInfoMEID = getMetaInfo();
        paramMEID.setMetainfo(metaInfoMEID);
        parameters[10] = paramMEID;

        Parameter paramMSISDN = new Parameter();
        paramMSISDN.setName("msisdn");
        paramMSISDN.setType("STRING");
        paramMSISDN.setIsIndexed("false");
        MetaInfo metaInfoMSISDN = getMetaInfo();
        paramMSISDN.setMetainfo(metaInfoMSISDN);
        parameters[11] = paramMSISDN;

        Parameter paramPrimaryMin = new Parameter();
        paramPrimaryMin.setName("primaryMin");
        paramPrimaryMin.setType("STRING");
        paramPrimaryMin.setIsIndexed("false");
        MetaInfo metaInfoPrimaryMin = getMetaInfo();
        paramPrimaryMin.setMetainfo(metaInfoPrimaryMin);
        parameters[12] = paramPrimaryMin;

        Parameter paramRatePlan = new Parameter();
        paramRatePlan.setName("ratePlan");
        paramRatePlan.setType("STRING");
        paramRatePlan.setIsIndexed("false");
        MetaInfo metaInfoRatePlan = getMetaInfo();
        paramRatePlan.setMetainfo(metaInfoRatePlan);
        parameters[13] = paramRatePlan;

        Parameter paramReportGroup = new Parameter();
        paramReportGroup.setName("reportGroup");
        paramReportGroup.setType("INT");
        paramReportGroup.setIsIndexed("false");
        MetaInfo metaInfoReportGroup = getMetaInfo();
        paramReportGroup.setMetainfo(metaInfoReportGroup);
        parameters[14] = paramReportGroup;
        
        Parameter paramUnitofMeasure = new Parameter();
        paramUnitofMeasure.setName("unitOfMeasurement");
        paramUnitofMeasure.setType("STRING");
        paramUnitofMeasure.setIsIndexed("false");
        MetaInfo metaInfoUnitofMeasure = getMetaInfo();
        paramUnitofMeasure.setMetainfo(metaInfoUnitofMeasure);
        parameters[15] = paramUnitofMeasure;

        Parameter paramThresholdValue = new Parameter();
        paramThresholdValue.setName("thresholdValue");
        paramThresholdValue.setType("FLOAT");
        paramThresholdValue.setIsIndexed("false");
        MetaInfo metaInfoThresholdValue = getMetaInfo();
        paramThresholdValue.setMetainfo(metaInfoThresholdValue);
        parameters[16] = paramThresholdValue;

        sclDataSchema.setParameters(parameters);
        dataModel.setSclDataSchema(sclDataSchema);
        
        return dataModel;
	}
	
	private CreateAerCloudDataModelRequest createAerCloudEventStreamDataModelRequest(long accountId, String apiKey)
	{
        CreateAerCloudDataModelRequest dataModel = new CreateAerCloudDataModelRequest();
        dataModel.setAccountId(String.valueOf(accountId));
        dataModel.setApiKey(apiKey);
        dataModel.setName(appProperties.getAerEventStreamDataModelName());
        dataModel.setId(appProperties.getAerEventStreamDataModelId());
        dataModel.setDescription("This is Aer Event Stream data model created for your account. Please don't delete.");

        SclDataSchema sclDataSchema = new SclDataSchema();
        sclDataSchema.setEncoding("JSON");

        Parameter[] parameters = new Parameter[19];

        Parameter paramEventNotificationId = new Parameter();
        paramEventNotificationId.setName("eventNotificationID");
        paramEventNotificationId.setType("INT");
        paramEventNotificationId.setIsIndexed("false");
        paramEventNotificationId.setMetainfo(getMetaInfo());
        parameters[0] = paramEventNotificationId;

        Parameter paramName = new Parameter();
        paramName.setName("name");
        paramName.setType("STRING");
        paramName.setIsIndexed("false");
        paramName.setMetainfo(getMetaInfo());
        parameters[1] = paramName;

        Parameter paramLogStream = new Parameter();
        paramLogStream.setName("logStream");
        paramLogStream.setType("STRING");
        paramLogStream.setIsIndexed("false");
        paramLogStream.setMetainfo(getMetaInfo());
        parameters[2] = paramLogStream;

        Parameter paramEventType = new Parameter();
        paramEventType.setName("eventType");
        paramEventType.setType("STRING");
        paramEventType.setIsIndexed("false");
        paramEventType.setMetainfo(getMetaInfo());
        parameters[3] = paramEventType;

        Parameter paramDeviceID = new Parameter();
        paramDeviceID.setName("deviceID");
        paramDeviceID.setType("LONG");
        paramDeviceID.setIsIndexed("false");
        paramDeviceID.setMetainfo(getMetaInfo());
        parameters[4] = paramDeviceID;

        Parameter paramAccountID = new Parameter();
        paramAccountID.setName("accountID");
        paramAccountID.setType("LONG");
        paramAccountID.setIsIndexed("false");
        paramAccountID.setMetainfo(getMetaInfo());
        parameters[5] = paramAccountID;

        Parameter paramMsisdn = new Parameter();
        paramMsisdn.setName("msisdn");
        paramMsisdn.setType("STRING");
        paramMsisdn.setIsIndexed("false");
        paramMsisdn.setMetainfo(getMetaInfo());
        parameters[6] = paramMsisdn;

        Parameter paramPrimaryMin = new Parameter();
        paramPrimaryMin.setName("primaryMin");
        paramPrimaryMin.setType("STRING");
        paramPrimaryMin.setIsIndexed("false");
        paramPrimaryMin.setMetainfo(getMetaInfo());
        parameters[7] = paramPrimaryMin;

        Parameter paramIMSI = new Parameter();
        paramIMSI.setName("imsi");
        paramIMSI.setType("STRING");
        paramIMSI.setIsIndexed("false");
        paramIMSI.setMetainfo(getMetaInfo());
        parameters[8] = paramIMSI;

        Parameter paramMEID = new Parameter();
        paramMEID.setName("meid");
        paramMEID.setType("STRING");
        paramMEID.setIsIndexed("false");
        paramMEID.setMetainfo(getMetaInfo());
        parameters[9] = paramMEID;

        Parameter paramESN = new Parameter();
        paramESN.setName("esn");
        paramESN.setType("STRING");
        paramESN.setIsIndexed("false");
        paramESN.setMetainfo(getMetaInfo());
        parameters[10] = paramESN;
        
        Parameter paramICCID = new Parameter();
        paramICCID.setName("iccid");
        paramICCID.setType("STRING");
        paramICCID.setIsIndexed("false");
        paramICCID.setMetainfo(getMetaInfo());
        parameters[11] = paramICCID;

        Parameter paramEventTimestampMillis = new Parameter();
        paramEventTimestampMillis.setName("eventTimestampMillis");
        paramEventTimestampMillis.setType("LONG");
        paramEventTimestampMillis.setIsIndexed("false");
        paramEventTimestampMillis.setMetainfo(getMetaInfo());
        parameters[12] = paramEventTimestampMillis;

        Parameter paramDSSN = new Parameter();
        paramDSSN.setName("DSSN");
        paramDSSN.setType("INT");
        paramDSSN.setIsIndexed("false");
        paramDSSN.setMetainfo(getMetaInfo());
        parameters[13] = paramDSSN;
        
        Parameter paramOPC = new Parameter();
        paramOPC.setName("OPC");
        paramOPC.setType("STRING");
        paramOPC.setIsIndexed("false");
        paramOPC.setMetainfo(getMetaInfo());
        parameters[14] = paramOPC;

        Parameter paramDPC = new Parameter();
        paramDPC.setName("DPC");
        paramDPC.setType("STRING");
        paramDPC.setIsIndexed("false");
        paramDPC.setMetainfo(getMetaInfo());
        parameters[15] = paramDPC;

        Parameter paramOSSN = new Parameter();
        paramOSSN.setName("OSSN");
        paramOSSN.setType("INT");
        paramOSSN.setIsIndexed("false");
        paramOSSN.setMetainfo(getMetaInfo());
        parameters[16] = paramOSSN;
        
        Parameter paramRemotePC = new Parameter();
        paramRemotePC.setName("remote_pc");
        paramRemotePC.setType("STRING");
        paramRemotePC.setIsIndexed("false");
        paramRemotePC.setMetainfo(getMetaInfo());
        parameters[17] = paramRemotePC;

        Parameter paramRemoteMSC = new Parameter();
        paramRemoteMSC.setName("remote_msc");
        paramRemoteMSC.setType("STRING");
        paramRemoteMSC.setIsIndexed("false");
        paramRemoteMSC.setMetainfo(getMetaInfo());
        parameters[18] = paramRemoteMSC;

        sclDataSchema.setParameters(parameters);
        dataModel.setSclDataSchema(sclDataSchema);
        
        return dataModel;
	}

	private MetaInfo getMetaInfo() {
		MetaInfo metaInfo = new MetaInfo();
		metaInfo.setUom("");
        
		return metaInfo;
	}
		
	private CreateAerCloudContainerRequest createAerCloudAlertContainerRequest(long accountId, String apiKey)
	{
        CreateAerCloudContainerRequest container = new CreateAerCloudContainerRequest();
        container.setAccountId(String.valueOf(accountId));
        container.setApiKey(apiKey);
        container.setId(appProperties.getMyAlertsContainerId());
        container.setSclDataModelId(appProperties.getMyAlertsDataModelId());
        
        return container;
	}
	
	private CreateAerCloudContainerRequest createAerCloudEventStreamContainerRequest(long accountId, String apiKey)
	{
	    CreateAerCloudContainerRequest container = new CreateAerCloudContainerRequest();
	    container.setAccountId(String.valueOf(accountId));
	    container.setApiKey(apiKey);
	    container.setId(appProperties.getAerEventStreamContainerId());
	    container.setSclDataModelId(appProperties.getAerEventStreamDataModelId());
	    
	    return container;
	}
	
	private PersistAerCloudDeviceRequest createAerCloudPersistDeviceRequest(long accountId, String apiKey)
	{
	    PersistAerCloudDeviceRequest request = new PersistAerCloudDeviceRequest();
	    request.setAccountID(String.valueOf(accountId));
	    request.setApiKey(apiKey);
	    request.setSclId(appProperties.getMyAlertsAppSclId());
	    request.setDeviceName(appProperties.getMyAlertsAppSclId());
	    request.setDataModelId(appProperties.getMyAlertsDataModelId());
	    
	    return request;
	}
		
	private ProvisionAerCloudDeviceRequest createProvisionDeviceRequest(long accountId, String apiKey)
	{
		ProvisionAerCloudDeviceRequest request = new ProvisionAerCloudDeviceRequest();
		
	    request.setAccountID(String.valueOf(accountId));
	    request.setApiKey(apiKey);
	    request.setSclId(appProperties.getMyAlertsTestDeviceSclId());
	    request.setDeviceID(new DeviceID(appProperties.getMyAlertsTestDeviceSclId()));
	    request.setDeviceName(appProperties.getMyAlertsTestDeviceSclId());
	    request.setDataModelId(appProperties.getMyAlertsDataModelId());
	    
	    return request;
	}
}
