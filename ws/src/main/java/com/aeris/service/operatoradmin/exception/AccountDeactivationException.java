package com.aeris.service.operatoradmin.exception;

public class AccountDeactivationException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public AccountDeactivationException(String s) {
		super(s);
	}

	public AccountDeactivationException(String ex, Throwable t) {
		super(ex, t);
	}
}
