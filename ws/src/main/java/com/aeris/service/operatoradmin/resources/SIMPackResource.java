package com.aeris.service.operatoradmin.resources;

import com.aeris.service.operatoradmin.dao.ISIMPackDAO;
import com.aeris.service.operatoradmin.exception.DistributorDBException;
import com.aeris.service.operatoradmin.exception.DistributorException;
import com.aeris.service.operatoradmin.exception.DistributorNotFoundException;
import com.aeris.service.operatoradmin.exception.InvalidSIMPackException;
import com.aeris.service.operatoradmin.exception.SIMPackException;
import com.aeris.service.operatoradmin.model.SIMPack;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateNeoSIMPacksRequest;
import com.aeris.service.operatoradmin.payload.CreateUniqueCodesRequest;
import com.aeris.service.operatoradmin.payload.CreateSIMPacksRequest;
import com.aeris.service.operatoradmin.payload.UpdateSIMPacksRequest;
import com.opencsv.CSVReader;
import com.sun.jersey.multipart.FormDataParam;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/operators/{operatorId}/simpacks")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class SIMPackResource {

    private static final Logger LOG = LoggerFactory.getLogger(SIMPackResource.class);
    private final ObjectMapper mapper = new ObjectMapper();

    @Inject
    private ISIMPackDAO simPackDAO;

    @POST
    @Path("/uniquecodes")
    public Response createUniqueCodes(@PathParam("operatorId") @ValidOperator final String operatorId,
            @Valid CreateUniqueCodesRequest request) {
        request.setOperatorId(Long.parseLong(operatorId));
        LOG.info("Request received to create unique codes: " + request.toString());
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            List<String> uniqueCodes = simPackDAO.createUniqueCodes(request);
            String uniqueCodesString = mapper.writeValueAsString(uniqueCodes);
            builder.entity(uniqueCodesString);
            LOG.info("Request to create empty SIMPacks is completed. Response: " + uniqueCodesString);
        } catch (DistributorNotFoundException ex) {
            LOG.error("Distributor with account id {} does not exists.", request.getDistributorAccountId());
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("Distributor with account id " + request.getDistributorAccountId() + " does not exists.")
                    .build();
        } catch (SIMPackException ex) {
            LOG.error("SIMPackException occurred while creating unique codes for distributor {} due to {}", request.getDistributorAccountId(), ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (DistributorDBException ex) {
            LOG.error("DistributorDBException occurred while ccreating unique codes for distributor {} due to {}", request.getDistributorAccountId(), ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while creating creating unique codes distributor {} due to {}", request.getDistributorAccountId(), ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        }
        LOG.info("Request received to create unique codes is completed successfully");
        Response response = builder.build();
        return response;
    }

    @POST
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
    public Response createSIMPacks(@PathParam("operatorId") @ValidOperator final String operatorId, @Valid CreateSIMPacksRequest request) {
        LOG.info("Request received to create SIMPacks: " + request.toString());
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            List<SIMPack> simPacks = simPackDAO.createSIMPacks(request);
            String simPacksString = mapper.writeValueAsString(simPacks);
            builder.entity(simPacksString);
            LOG.info("Request to create SIMPacks is completed. Response: " + simPacksString);
        } catch (SIMPackException ex) {
            LOG.error("SIMPackException occurred while creating SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (DistributorException ex) {
            LOG.error("DistributorException occurred while creating SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while creating SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (DistributorDBException ex) {
            LOG.error("DistributorDBException occurred while creating sim packs: " + ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        }
        LOG.info("Request received to create SIMPacks is completed successfully");
        Response response = builder.build();
        return response;
    }

    @PUT
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
    public Response updateSIMPacks(@PathParam("operatorId") @ValidOperator final String operatorId,
            @Valid UpdateSIMPacksRequest request) {
        LOG.info("Request received to update SIMPacks: " + request.toString());
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            boolean status = simPackDAO.updateSIMPacks(request);
            String statusString = mapper.writeValueAsString(status);
            builder.entity(statusString);
        } catch (SIMPackException ex) {
            LOG.error("SIMPackException occurred while updating SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while updating SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (DistributorDBException ex) {
            LOG.error("DistributorDBException occurred while updating sim packs: " + ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        }
        LOG.info("Request received to update SIMPacks is completed successfully");
        Response response = builder.build();
        return response;
    }

    @GET
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
    public Response getAllSIMPacks(@PathParam("operatorId") @ValidOperator final String operatorId) {
        LOG.info("Request received to get all SIMPacks");
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            List<SIMPack> simPacks = simPackDAO.getAllSIMPacks(null);
            String simPacksString = mapper.writeValueAsString(simPacks);
            builder.entity(simPacksString);
            LOG.info("Request to get SIMPacks is completed. Response: " + simPacksString);
        } catch (SIMPackException ex) {
            LOG.error("SIMPackException occurred while getting SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while getting SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        }
        Response response = builder.build();
        return response;
    }

    @GET
    @Path("/distributor/{distributorAccountId}")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
    public Response getDistributorSIMPacks(@PathParam("operatorId") @ValidOperator final String operatorId, @PathParam("distributorAccountId") final String distributorAccountId) {
        LOG.info("Request received to get all SIMPacks for distributor with account id " + distributorAccountId);
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            List<SIMPack> simPacks = simPackDAO.getAllSIMPacks(distributorAccountId);
            String simPacksString = mapper.writeValueAsString(simPacks);
            builder.entity(simPacksString);
            LOG.info("Request to get SIMPacks is completed. Response: " + simPacksString);
        } catch (SIMPackException ex) {
            LOG.error("SIMPackException occurred while getting SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while getting SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        }
        Response response = builder.build();
        return response;
    }

    @GET
    @Path("/{simPackId}")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
    public Response getSIMPack(@PathParam("operatorId") @ValidOperator final String operatorId,
            @PathParam("simPackId") final String simPackId) {
        LOG.info("Request received to get SIMPack having unique code {}", simPackId);
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            SIMPack simPack = simPackDAO.getSIMPack(simPackId);
            String simPackString = mapper.writeValueAsString(simPack);
            builder.entity(simPackString);
            LOG.info("Request to get SIMPack is completed. Response: " + simPackString);
        } catch (SIMPackException ex) {
            LOG.error("SIMPackException occurred while getting SIM pack due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while getting SIM pack due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        }
        Response response = builder.build();
        return response;
    }

    @DELETE
    @Path("/{simPackId}/distributor/{distributorAccountId}")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
    public Response deleteSIMPack(@PathParam("operatorId") @ValidOperator final String operatorId,
            @PathParam("simPackId") final String simPackId, @PathParam("distributorAccountId") final String distributorAccountId) {
        LOG.info("Request received to delete SIMPack having unique code {}", simPackId);
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        boolean isDeleted = false ;
        try {
        	isDeleted = simPackDAO.deleteSIMPack(Long.valueOf(distributorAccountId), Long.valueOf(simPackId));
		} catch (SIMPackException e) {
			LOG.error("SIMPackException occurred while deleting SIM packs due to {}", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (InvalidSIMPackException e) {
			LOG.error("Invalid simpackException occurred while deleting ..", e.getMessage());
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("Exception occurred while deleting SIM pack due to {}", e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
        Response response = builder.entity(isDeleted).build();
        return response;
    }
    
    @POST
    @Path("/neo")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
    public Response createNeoSIMPacks(@PathParam("operatorId") @ValidOperator final String operatorId, @Valid CreateNeoSIMPacksRequest request) {
        LOG.info("Request received to create Neo SIMPacks: " + request.toString());
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);       
        try {
            List<SIMPack> simPacks = simPackDAO.createNeoSIMPacks(request);
            String simPacksString = mapper.writeValueAsString(simPacks);
            builder.entity(simPacksString);
            LOG.info("Request to create SIMPacks is completed. Response: " + simPacksString);
        } catch (SIMPackException ex) {
            LOG.error("SIMPackException occurred while creating SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while creating SIM packs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (DistributorDBException ex) {
            LOG.error("DistributorDBException occurred while creating SIM packs: " + ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        }        
        Response response = builder.build();
        return response;
    }
    
    
    @POST
	@Path("/distributor/{distributorAccountId}/upload")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
	@Consumes(MediaType.WILDCARD)
	public Response updateSimpacksFromCSV(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("distributorAccountId") @Pattern(regexp = "[0-9]+", message = "{distributorId.number}") final String distributorId, 
			@FormDataParam("file") InputStream csv, @QueryParam("email") String email) {
		CSVReader csvReader = new CSVReader(new InputStreamReader(csv));
	    List<String[]> rows = null;
		try {
			rows = csvReader.readAll();
			simPackDAO.updateSIMPacksFromCSV(operatorId, distributorId, rows, email);
		} catch (IOException e) {
			LOG.error("CSV file read Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		} catch (SIMPackException e) {
			LOG.error("Exception while updating simpacks :", e);
			return Response.status(Status.BAD_REQUEST).entity("Invalid data file format").build();
		} catch (DistributorDBException e) {
			LOG.error("DB exception :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Database error").build();
		} catch (Exception e) {
			LOG.error("General service exception :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
		} finally {
			if(csvReader != null) {
				try {
					csvReader.close();
				} catch (IOException e) {
					LOG.error("Exception while closing CSV Reader stream :", e);
					return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unknown exception occurred.").build();
				}
			}
		}
	    return Response.status(Status.ACCEPTED).entity("OK").build();
	}

}
