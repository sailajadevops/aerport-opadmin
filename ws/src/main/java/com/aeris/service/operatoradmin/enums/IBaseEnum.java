package com.aeris.service.operatoradmin.enums;


public interface IBaseEnum {
	boolean isValid(String id);
}
