package com.aeris.service.operatoradmin.exception;

public class ServiceProfileValidationException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public ServiceProfileValidationException(String s) {
		super(s);
	}

	public ServiceProfileValidationException(String ex, Throwable t) {
		super(ex, t);
	}
}
