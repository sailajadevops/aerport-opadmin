package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.sql.Types;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.dao.storedproc.ResultStatus;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.google.inject.Inject;

public class AssignRatePlanToAccountStoredProcedure extends AbstractProcedureCall<ResultStatus> {

	private static final String SQL_ASSIGN_RATE_PLAN_TO_ACCOUNT = "account_rate_plan_insert";

	@Inject
	public AssignRatePlanToAccountStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_ASSIGN_RATE_PLAN_TO_ACCOUNT);
		
		registerParameter(new InParameter("in_plan_id", Types.INTEGER));  
		registerParameter(new InParameter("in_account_id", Types.INTEGER));
		registerParameter(new InParameter("in_carrier_plan_id", Types.INTEGER));
		registerParameter(new InParameter("in_start_date", Types.DATE));
		registerParameter(new InParameter("in_created_by", Types.VARCHAR));
		
		registerParameter(new OutParameter("ret_status", Types.INTEGER));
	}

	@Override
	@SuppressWarnings("unchecked")
	public ResultStatus execute(Object... input) throws StoredProcedureException {
		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		ResultStatus status = new ResultStatus();
		
		// Read the status
		if (results != null) {
			String retStatus = (String) results.get("ret_status");
			
			if(NumberUtils.isNumber(retStatus))
			{
				int statusCode = Integer.parseInt(retStatus);
				status.setResultCode(statusCode); // success_code == 0
			}
		}

		return status;
	}
}
