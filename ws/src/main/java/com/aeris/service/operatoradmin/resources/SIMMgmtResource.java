package com.aeris.service.operatoradmin.resources;

import com.aeris.service.operatoradmin.dao.ISIMMgmtDAO;
import com.aeris.service.operatoradmin.exception.DAOException;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.AssignSIMsToAccountRequest;
import java.io.IOException;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/operators/{operatorId}/accounts/{accountId}/sim-mgmt")
@Singleton
public class SIMMgmtResource {

    private static final Logger LOG = LoggerFactory.getLogger(SIMMgmtResource.class);
    private final ObjectMapper mapper = new ObjectMapper();

    @Inject
    private ISIMMgmtDAO simMgmtDAO;

    @PUT
    @Path("/assign/sims")
    public Response assignSIMsToAccount(
            @PathParam("operatorId") @ValidOperator final String operatorId,
            @PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId, 
            @Valid AssignSIMsToAccountRequest request) {
        LOG.info("Request received to assign SIMs to account : " + request.toString());
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            simMgmtDAO.assignSIMsToAccount(operatorId, accountId, request);
            String result = mapper.writeValueAsString("true");
            builder.entity(result);
            LOG.info("Request to assign SIMs to {} is completed. Response: true", accountId);
        } catch (DAOException ex) {
            LOG.error("DAOException occurred while assigning SIMs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while assigning SIMs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        }        
        Response response = builder.build();
        return response;
    }

    @PUT
    @Path("/assign/simpack-sims/{simPackId}")
    public Response assignSIMsToAccountFromSIMPack(@PathParam("operatorId") @ValidOperator final String operatorId,
            @PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId, 
            @PathParam("simPackId") final String simPackId,
            @QueryParam("orderId") final long orderId,
            @QueryParam("userId") @Email(message = "{userId.email}") final String userId) {
        LOG.info("Request received to assign sims from simpack {} to account {}", simPackId, accountId);
        Response.ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        try {
            simMgmtDAO.assignSIMsToAccountFromSIMPack(operatorId, accountId, simPackId, userId, orderId);
            String result = mapper.writeValueAsString("true");
            builder.entity(result);
            LOG.info("Request to assign SIMs to {} from SIMPack {} is completed. Response: true", accountId, simPackId);
        } catch (DAOException ex) {
            LOG.error("DAOException occurred while assigning SIMs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } catch (IOException ex) {
            LOG.error("IOException occurred while assigning SIMs due to {}", ex.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .build();
        } 
        Response response = builder.build();
        return response;
    }

}
