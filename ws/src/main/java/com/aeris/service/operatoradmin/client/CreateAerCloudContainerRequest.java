package com.aeris.service.operatoradmin.client;

import java.io.Serializable;

public class CreateAerCloudContainerRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String accountId;
    private String id;
    private String sclDataModelId;
    private String apiKey;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSclDataModelId() {
		return sclDataModelId;
	}

	public void setSclDataModelId(String sclDataModelId) {
		this.sclDataModelId = sclDataModelId;
	}	
	
	public String getApiKey() {
		return apiKey;
	}
	
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
}
