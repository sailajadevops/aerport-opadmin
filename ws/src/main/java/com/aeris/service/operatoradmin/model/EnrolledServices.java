package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

@XmlRootElement
public class EnrolledServices implements Serializable {
	private static final long serialVersionUID = 8468363183066668111L;

	@NotNull(message = "{allowMTVoice.notnull}")
	private boolean allowMTVoice;

	@NotNull(message = "{allowMOVoice.notnull}")
	private boolean allowMOVoice;

	@NotNull(message = "{allowMTSms.notnull}")
	private boolean allowMTSms;

	@NotNull(message = "{allowMOSms.notnull}")
	private boolean allowMOSms;

	@NotNull(message = "{allowPacket.notnull}")
	private boolean allowPacket;

	private boolean allowStaticIP;

	private boolean allowRoaming;

	public boolean isAllowMTVoice() {
		return allowMTVoice;
	}

	public void setAllowMTVoice(boolean allowMTVoice) {
		this.allowMTVoice = allowMTVoice;
	}

	public boolean isAllowMOVoice() {
		return allowMOVoice;
	}

	public void setAllowMOVoice(boolean allowMOVoice) {
		this.allowMOVoice = allowMOVoice;
	}

	public boolean isAllowMTSms() {
		return allowMTSms;
	}

	public void setAllowMTSms(boolean allowMTSms) {
		this.allowMTSms = allowMTSms;
	}

	public boolean isAllowMOSms() {
		return allowMOSms;
	}

	public void setAllowMOSms(boolean allowMOSms) {
		this.allowMOSms = allowMOSms;
	}

	public boolean isAllowPacket() {
		return allowPacket;
	}

	public void setAllowPacket(boolean allowPacket) {
		this.allowPacket = allowPacket;
	}

	public boolean isAllowStaticIP() {
		return allowStaticIP;
	}

	public void setAllowStaticIP(boolean allowStaticIP) {
		this.allowStaticIP = allowStaticIP;
	}

	public void setAllowRoaming(boolean allowRoaming) {
		this.allowRoaming = allowRoaming;
	}

	public boolean isAllowRoaming() {
		return allowRoaming;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
