package com.aeris.service.operatoradmin.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Utility class for string and other basic operations 
 * 
 * @author saurabh.sharma@aeris.net	
 *
 */
public class ApplicationUtils {
	
	private ApplicationUtils () {}
	
	private static Logger LOG = LoggerFactory.getLogger(ApplicationUtils.class);
	public static final String DATE_FORMAT_YYYYMMDD = "yyyy-MM-dd" ;

	/**
	 * Converts string value to int, default value to 0
	 * 
	 * @param value
	 * @return
	 */
	public static int convertStringToInt(String value) {
		if(value != null) {
			value = value.trim() ;
			try {
				return Integer.valueOf(value);
			} catch (Exception e) {
				return 0 ;
			}
		} else 
			return 0 ;
	}


	/**
	 * converts the string value to double, default value is 0.0
	 * 
	 * @param value
	 * @return
	 */
	public static double convertStringToDouble(String value) {
		try {
			return Double.valueOf(value);
		} catch (Exception e) {
			return 0.0 ;
		}
	}
	
	
	public static Timestamp convertStringToTS(String timestampStr) {
		try{
			if(isValidDate(timestampStr)){
				timestampStr += " 00:00:00.000" ;
			}
		    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		    dateFormat.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE_GMT));
		    Date parsedDate = dateFormat.parse(timestampStr);
		    Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
		    return timestamp ;
		}catch(Exception e){
		 	LOG.error("Exception while trying to parse timestamp string:" + timestampStr, e); 
		}
		return null ;

	}
	
	public static boolean isValidDate(String inDate) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setLenient(false);
            try {
                dateFormat.parse(inDate.trim());
            } catch (ParseException pe) {
                return false;
            }
            return true;
        }
        
	public static Date getDate(String dateStr) {
		return getDate(dateStr, DATE_FORMAT_YYYYMMDD);
	}


	/**
	 * Gets the date object for date-string and dateFormat.
	 * @param dateStr
	 * @param dateFormat
	 * @return
	 */
	public static Date getDate(String dateStr, String dateFormat) {
		Date dt = null ;
		if(dateStr != null){
			try {
				SimpleDateFormat sf = new SimpleDateFormat(dateFormat);
				sf.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE_GMT));
				dt = sf.parse(dateStr);
			} catch (ParseException e) {
				LOG.error("Unable to parse date string :: " + dateStr, e);
			}
		}
		return dt ;
	}
	
	
	/**
	 * Returns the String representation of the date object in specified format
	 * @param dateStr
	 * @param dateFormat
	 * @return
	 */
	public static String getGMTDateStr(Date date, String dateFormat) {
		String gmtDateStr = null ;
		if(date != null){
			try {
				SimpleDateFormat sf = new SimpleDateFormat(dateFormat);
				sf.setTimeZone(TimeZone.getTimeZone(Constants.TIMEZONE_GMT));
				gmtDateStr = sf.format(date);
			} catch (Exception e) {
				LOG.error("Unable to Format date object :: " + date, e);
			}
		}
		return gmtDateStr ;
	}
	
	/**
	 * Returns the String representation of the date object in yyyy-MM-dd format
	 * @param dateStr
	 * @return
	 */
	public static String getGMTDateStr(Date date) {
		return getGMTDateStr(date, DATE_FORMAT_YYYYMMDD);
	}
	
	
	public static void main(String[] args) {
		System.out.println(convertStringToInt("saruabh"));
	}
	
	/**Returns todays date with 00 hr 00 min 00 sec and 000 miliSec time as per TimeZone value provided 
	 * @param timeZone
	 * @return
	 */
	public static Date getTodaysStartTimeStamp(String timeZone){
		
		Calendar currCal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
		currCal.set(Calendar.HOUR_OF_DAY, 0);
		currCal.set(Calendar.MINUTE, 0);
		currCal.set(Calendar.SECOND, 0);
		currCal.set(Calendar.MILLISECOND, 0);
		
		return currCal.getTime();
	}

}
