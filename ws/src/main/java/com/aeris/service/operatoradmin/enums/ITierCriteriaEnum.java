package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.TierCriteria;

public interface ITierCriteriaEnum  extends IBaseEnum{
	TierCriteria getTierCriteriaByName(String name);
	TierCriteria getTierCriteriaById(int id);
	List<TierCriteria> getAllTierCriteria();
}
