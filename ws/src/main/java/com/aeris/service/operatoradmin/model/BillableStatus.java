package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum BillableStatus implements Serializable {
	BILLABLE(1), NON_BILLABLE(0);
	int value;

	BillableStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int value) {
		BillableStatus[] statuses = values();

		for (BillableStatus billableStatus : statuses) {
			if (billableStatus.getValue() == value) {
				return true;
			}
		}

		return false;
	}

	public static BillableStatus fromValue(int value) {
		BillableStatus[] statuses = values();

		for (BillableStatus billableStatus : statuses) {
			if (billableStatus.getValue() == value) {
				return billableStatus;
			}
		}

		return null;
	}

	public static BillableStatus fromName(String value) {
		BillableStatus[] statuses = values();

		for (BillableStatus billableStatus : statuses) {
			if (billableStatus.name().equalsIgnoreCase(value)) {
				return billableStatus;
			}
		}

		return null;
	}
}
