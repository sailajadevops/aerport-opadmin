package com.aeris.service.operatoradmin.exception;

public class BadStartDateException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public BadStartDateException(String s) {
		super(s);
	}

	public BadStartDateException(String ex, Throwable t) {
		super(ex, t);
	}
}
