package com.aeris.service.operatoradmin.payload;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.Zone;

@XmlRootElement
public class CreateZoneSetRequest {
	@NotNull(message = "{zoneSetName.notnull}")
	@Size(min = 1, max = 30, message = "{zoneSetName.size}")
	private String zoneSetName;
	
	@NotNull(message = "{productId.notnull}")
	@Pattern(regexp = "[0-9]+", message = "{productId.number}")
	private String productId;
	
	@Pattern(regexp = "[0-9]+", message = "{accountId.number}")
	private String accountId;
	
	@Email(message = "{userId.email}")
	@NotNull(message = "{userId.notnull}")
	private String userId;
	
	private int resId;
	
	List<Zone> zones;
	
	public List<Zone> getZones() {
		return zones;
	}

	public void setZones(List<Zone> zones) {
		this.zones = zones;
	}

	public String getZoneSetName() {
		return zoneSetName;
	}

	public void setZoneSetName(String zoneSetName) {
		this.zoneSetName = zoneSetName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getResId() {
		return resId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

}
