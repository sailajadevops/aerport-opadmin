package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import com.aeris.service.operatoradmin.payload.AssignRatePlanRequest;
import com.aeris.service.operatoradmin.payload.CreateRatePlanRequest;
import com.aeris.service.operatoradmin.payload.UpdateRatePlanRequest;

@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD, PARAMETER, TYPE})
@Documented
@Constraint(validatedBy = {ValidateRatePlanDate.Validator.class, ValidateRatePlanDate.UpdateValidator.class, ValidateRatePlanDate.AssignValidator.class})
public @interface ValidateRatePlanDate {
	String message() default "{com.aeris.service.operatoradmin.model.constraints.ValidateRatePlanDate.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class Validator implements ConstraintValidator<ValidateRatePlanDate, CreateRatePlanRequest> {

		@Override
		public void initialize(final ValidateRatePlanDate enumClazz) {
			// do nothing
		}

		public boolean isValid(final CreateRatePlanRequest request, final ConstraintValidatorContext constraintValidatorContext) {
			if(request.getEndDate().after(request.getStartDate()))
			{
				return true;
			}
			
			return false;
		}
	}

	public class UpdateValidator implements ConstraintValidator<ValidateRatePlanDate, UpdateRatePlanRequest> {

		@Override
		public void initialize(final ValidateRatePlanDate enumClazz) {
			// do nothing
		}

		public boolean isValid(final UpdateRatePlanRequest request, final ConstraintValidatorContext constraintValidatorContext) {
			if(request.getEndDate().after(request.getStartDate()))
			{
				return true;
			}
			
			return false;
		}
	}

	public class AssignValidator implements ConstraintValidator<ValidateRatePlanDate, AssignRatePlanRequest> {

		@Override
		public void initialize(final ValidateRatePlanDate enumClazz) {
			// do nothing
		}

		public boolean isValid(final AssignRatePlanRequest request, final ConstraintValidatorContext constraintValidatorContext) {
			if(request.getEndDate().after(request.getStartDate()))
			{
				return true;
			}
			
			return false;
		}
	}
}
