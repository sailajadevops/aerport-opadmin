package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum RatePlanPaymentType implements Serializable {
	POST_PAID(1), PRE_PAID(2);
	int value;

	RatePlanPaymentType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int value) {
		RatePlanPaymentType[] statuses = values();

		for (RatePlanPaymentType paymentType : statuses) {
			if (paymentType.getValue() == value) {
				return true;
			}
		}

		return false;
	}

	public static RatePlanPaymentType fromValue(int value) {
		RatePlanPaymentType[] statuses = values();

		for (RatePlanPaymentType paymentType : statuses) {
			if (paymentType.getValue() == value) {
				return paymentType;
			}
		}

		return null;
	}

	public static RatePlanPaymentType fromName(String value) {
		RatePlanPaymentType[] statuses = values();

		for (RatePlanPaymentType paymentType : statuses) {
			if (paymentType.name().equalsIgnoreCase(value)) {
				return paymentType;
			}
		}

		return null;
	}
}
