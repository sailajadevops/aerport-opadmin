package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IncludedPlanDetails implements Serializable {
	private static final long serialVersionUID = 999977294234257540L;
	private long includedPacketKB;
	private double perKBPacketPrice;
	private int includedMTSms;
	private double perMTSmsPrice;
	private int includedMOSms;
	private double perMOSmsPrice;
	private int includedMTVoiceMins;
	private double perMinMTVoicePrice;
	private int includedMOVoiceMins;
	private double perMinMOVoicePrice;

	public long getIncludedPacketKB() {
		return includedPacketKB;
	}

	public void setIncludedPacketKB(long includedPacketKB) {
		this.includedPacketKB = includedPacketKB;
	}

	public double getPerKBPacketPrice() {
		return perKBPacketPrice;
	}

	public void setPerKBPacketPrice(double perKBPacketPrice) {
		this.perKBPacketPrice = perKBPacketPrice;
	}

	public int getIncludedMTSms() {
		return includedMTSms;
	}

	public void setIncludedMTSms(int includedMTSms) {
		this.includedMTSms = includedMTSms;
	}

	public double getPerMTSmsPrice() {
		return perMTSmsPrice;
	}

	public void setPerMTSmsPrice(double perMTSmsPrice) {
		this.perMTSmsPrice = perMTSmsPrice;
	}

	public int getIncludedMOSms() {
		return includedMOSms;
	}

	public void setIncludedMOSms(int includedMOSms) {
		this.includedMOSms = includedMOSms;
	}

	public double getPerMOSmsPrice() {
		return perMOSmsPrice;
	}

	public void setPerMOSmsPrice(double perMOSmsPrice) {
		this.perMOSmsPrice = perMOSmsPrice;
	}

	public int getIncludedMTVoiceMins() {
		return includedMTVoiceMins;
	}

	public void setIncludedMTVoiceMins(int includedMTVoiceMins) {
		this.includedMTVoiceMins = includedMTVoiceMins;
	}

	public double getPerMinMTVoicePrice() {
		return perMinMTVoicePrice;
	}

	public void setPerMinMTVoicePrice(double perMinMTVoicePrice) {
		this.perMinMTVoicePrice = perMinMTVoicePrice;
	}

	public int getIncludedMOVoiceMins() {
		return includedMOVoiceMins;
	}

	public void setIncludedMOVoiceMins(int includedMOVoiceMins) {
		this.includedMOVoiceMins = includedMOVoiceMins;
	}

	public double getPerMinMOVoicePrice() {
		return perMinMOVoicePrice;
	}

	public void setPerMinMOVoicePrice(double perMinMOVoicePrice) {
		this.perMinMOVoicePrice = perMinMOVoicePrice;
	}
}
