package com.aeris.service.operatoradmin.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IAccountApplicationDAO;
import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.IDistributorDAO;
import com.aeris.service.operatoradmin.dao.IOperatorProductDAO;
import com.aeris.service.operatoradmin.dao.IServiceProfileDAO;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.IContractEnum;
import com.aeris.service.operatoradmin.exception.AccountDeactivationException;
import com.aeris.service.operatoradmin.exception.AccountException;
import com.aeris.service.operatoradmin.exception.AccountMappingException;
import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.ApiKeyException;
import com.aeris.service.operatoradmin.exception.DistributorDBException;
import com.aeris.service.operatoradmin.exception.DistributorNotFoundException;
import com.aeris.service.operatoradmin.exception.DuplicateAccountException;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.exception.SubAccountException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.AccountOverageBucket;
import com.aeris.service.operatoradmin.model.AccountStatus;
import com.aeris.service.operatoradmin.model.AccountType;
import com.aeris.service.operatoradmin.model.ApiKey;
import com.aeris.service.operatoradmin.model.ApprovalStatus;
import com.aeris.service.operatoradmin.model.BillableStatus;
import com.aeris.service.operatoradmin.model.BillingDetails;
import com.aeris.service.operatoradmin.model.Contact;
import com.aeris.service.operatoradmin.model.ContactType;
import com.aeris.service.operatoradmin.model.Contract;
import com.aeris.service.operatoradmin.model.Distributor;
import com.aeris.service.operatoradmin.model.InvoiceAddress;
import com.aeris.service.operatoradmin.model.Region;
import com.aeris.service.operatoradmin.model.ResourcePermission;
import com.aeris.service.operatoradmin.model.ServiceProfile;
import com.aeris.service.operatoradmin.model.SubscriptionStatus;
import com.aeris.service.operatoradmin.utils.ApplicationUtils;
import com.aeris.service.operatoradmin.utils.Constants;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedGenerator;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.inject.Inject;

public class AccountDAO implements IAccountDAO {
	@Inject
	private IDistributorDAO distributorDAO;
	private static final String EMAIL_SEPERATOR = ";";
	private static final String SUB_ACCOUNT = "SUB-ACCOUNT";
	private static final String MOBO = "MOBO";
	private static final String SEPARATOR = ":";
	private static final int NOT_USED = 1;
	private static final int USED = 0;
	private static final int OPERATOR_AERIS = 1;
	private static final int OPERATOR_SPRINT = 2;
	private static final int OPERATOR_CISCO = 3;
	private static Logger LOG = LoggerFactory.getLogger(AccountDAO.class);
	private static final String SQL_GET_MAX_ACCOUNT_ID = "select max(account_id) as max_account_id from aersys_accounts where account_id  between ? and ?";
	private static final String SQL_CREATE_ACCOUNT_INFO = "INSERT INTO aersys_accounts " +
															"(account_type, " +
															"account_id, " +
															"name, " +
															"region_code," +
															"rep_time," +
															"not_used, " +
															"www_addr) " +
															"values " +
															"(?, ?, ?, ?, ?, ?, ?)";

	private static final String SQL_CREATE_ACCT_ADDITIONAL_INFO = "INSERT INTO ws_accts_additional_info " +
																	"(account_id, " +
																	"account_status, " +
																	"invoice_address, " +
																	"special_instruction, " +
																	"included_alert_profiles, " +
																	"tags, " +
																	"approval_status, " +
																	"billable_status, " +
																	"operator_id, " +
																	"netsuite_id, " +
																	"parent_account_id, " +
																	"carrier_account_id, " +
																	"vertical_id, " +
																	"max_portal_accounts, " + 
																	"max_sheduled_reports, " + 
																	"max_report_history, " + 
																	"service_impairment_email, " + 
																	"my_alerts_email, " + 
																	"scheduled_report_recipients, " + 
																	"created_by, " +
																	"date_created, " +
																	"last_modified_by, " +
																	"last_modified_date, " +
                                                                    "support_contact_emails," +
                                                                    "agent_id, " +
                                                                    "account_type, " +
                                                                    "enable_create_wholesale, " +
                                                                    "wholesale_account_id, "+
                                                                    "credit_limit, " +
                                                                    "BILL_MRC_IN_ADVANCE ) " +
																	"values " +
																	"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_UPDATE_ACCOUNT_INFO = "UPDATE aersys_accounts set " +
															"account_type = ?, " +
															"name = ?, " +
															"region_code = ?, " +
															"rep_time = ?, " +
															"not_used = ?, " +
															"www_addr = ? " +
															"where account_id = ? ";
	
	private static final String SQL_UPDATE_ACCOUNT_STATUS = "UPDATE aersys_accounts set " +
															"not_used = ?, " +
															"rep_time = ? " +
															"where account_id = ? ";
	
	private static final String SQL_UPDATE_ACCT_ADDITIONAL_INFO = "UPDATE ws_accts_additional_info set " +
																	"account_status = ?, " +
																	"invoice_address = ?, " +
																	"special_instruction = ?, " +
																	"included_alert_profiles = ?, " +
																	"tags = ?, " +
																	"approval_status = ?, " +
																	"billable_status = ?, " +
																	"netsuite_id = ?, " +
																	"parent_account_id = ?, " +
																	"carrier_account_id = ?, " +
																	"vertical_id = ?, " +
																	"max_portal_accounts = ?, " + 
																	"max_sheduled_reports = ?, " + 
																	"max_report_history = ?, " + 
																	"service_impairment_email = ?, " + 
																	"my_alerts_email = ?, " + 
																	"scheduled_report_recipients = ?, " + 
																	"last_modified_by = ?, " +
																	"last_modified_date = ?, " +
                                                                    "support_contact_emails = ?, " +
                                                                    "agent_id = ?, " +
                                                                    "account_type = ?, "+
                                                                    "enable_create_wholesale = ?, "+
                                                                    "wholesale_account_id = ?, " +
                                                                    "credit_limit = ? " +
																	"where operator_id = ? and " +
																	"account_id = ? ";
	
	private static final String SQL_UPDATE_ACCT_STATUS = "UPDATE ws_accts_additional_info set " +
																	"account_status = ?, " +
																	"approval_status = ?, " +
																	"last_modified_by = ?, " +
																	"last_modified_date = ? " +
																	"where operator_id = ? and " +
																	"account_id = ? ";
	
	
	private static final String SQL_CREATE_CONTACT_INFO = "INSERT INTO ws_account_contacts " +
															"(account_id, " +
															"contact_type, " +
															"contact_name ," +
															"contact_role, " +
															"job_title, " +
															"email, " +
															"phone, " +
															"created_by, " +
															"date_created, " +
															"last_modified_by, " +
															"last_modified_date) " +
															"values " +
															"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_GET_SUB_ACCOUNT = "SELECT sa_id " +
														"FROM sub_accounts2 WHERE " +
														"account_id = ?";
	
	private static final String SQL_CREATE_SUB_ACCOUNT = "INSERT INTO sub_accounts2"+
															"(account_id, "+
															"parent_sa_id, "+
															"hierarchy_name, "+
															"group_type, "+
															"created_by, "+
															"modified_by, "+
															"modified_date) "+
															"values "+
															"(?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_CREATE_ACCOUNT_PRODUCT_SUBSCRIPTION = "INSERT INTO account_product_subscription " +
																			"(account_id," +
																			"product_id, " +
																			"status) " +
																			"values " +
																			"(?, ?, ?)";
	private static final String SQL_GET_RETAIL_ACCOUNTS_BY_WHOLESALE_ACCOUNT ="SELECT * " 
																			+ "from " 
																			+ "(select a.account_id, " 
																			+ "a.account_type as account_type_a, "
																			+ "a.name, " 
																			+ "a.not_used, " 
																			+ "b.account_status, "
																			+ "b.approval_status, "
																			+ "b.billable_status, " 
																			+ "b.operator_id, " 
																			+ "b.netsuite_id, " 
																			+ "b.credit_limit, " 
																			+ "sa2.account_id as parent_account_id, " 
																			+ "b.carrier_account_id, "
																			+ "b.last_modified_date, "
																            + "b.account_type as account_type_b, "
																            + "b.wholesale_account_id, "
																			+ "RTRIM (XMLAGG (XMLELEMENT (e, c.product_id || ',')).EXTRACT ('//text()'), ',') as product_ids, "
																			+ "ROW_NUMBER() OVER (ORDER BY a.account_id) AS rnum "
																			+ "from aersys_accounts a inner join ws_accts_additional_info b " 
																			+ "on a.account_id = b.account_id "
																			+ "and b.operator_id = ? "
																			+ "and b.wholesale_account_id = ? "
																			+ "and b.account_type = 'Retail' "
																			+ "and a.account_id < 80000 "
																			+ "left join AERISGEN.sub_accounts2 sa "
																			+ "on sa.account_id = a.account_id "
																			+ "left join sub_accounts2 sa2 "
																			+ "on sa2.sa_id = sa.parent_sa_id "
																			+ "left join account_product_subscription c " 
																			+ "on b.account_id = c.account_id "
																			+ "group by :group_by_clause "
																			+ "order by b.last_modified_date)";
	private static final String SQL_GET_ACCOUNTS_BY_OPERATOR = "SELECT * " 
																+ "from " 
																+ "(select a.account_id, " 
																+ "a.account_type as account_type_a, "
																+ "a.name, " 
																+ "ac.contract_id, "
																+ "a.not_used, " 
																+ "b.account_status, "
																+ "b.approval_status, "
																+ "b.billable_status, " 
																+ "b.operator_id, " 
																+ "b.netsuite_id, " 
																+ "b.credit_limit, "
																+ "sa2.account_id as parent_account_id, " 
																+ "b.carrier_account_id, "
																+ "b.last_modified_date, "
                                                                + "b.account_type as account_type_b, "
                                                                + "b.wholesale_account_id, "
																+ "RTRIM (replace(replace(XMLAGG (XMLELEMENT (e, c.product_id || ',')),'<E>'),'</E>'), ',') as product_ids, "
																+ "ROW_NUMBER() OVER (ORDER BY a.account_id) AS rnum "
																+ "from aersys_accounts a inner join ws_accts_additional_info b " 
																+ "on a.account_id = b.account_id "
																+ "and b.operator_id = ? "																
																+ "and a.account_id < 80000 "
																+ "left join AERISGEN.sub_accounts2 sa "
																+ "on sa.account_id = a.account_id "
																+ "left join sub_accounts2 sa2 "
																+ "on sa2.sa_id = sa.parent_sa_id "
																+ "left join account_product_subscription c " 
																+ "on b.account_id = c.account_id "
																+ "left join account_contract ac on a.account_id = ac.account_id "
																+ "group by :group_by_clause "
																+ "order by b.last_modified_date)";
	
	private static final String SQL_GET_ACCOUNTS = "SELECT * " 
													+ "from " 
													+ "(select a.account_id, " 
													+ "a.account_type as account_type_a, " 
													+ "a.name, " 
													+ "ac.contract_id, "
													+ "a.not_used, " 
													+ "b.account_status, "
													+ "b.approval_status, "
													+ "b.billable_status, " 
													+ "b.operator_id, " 
													+ "b.netsuite_id, " 
													+ "b.credit_limit, "
													+ "sa2.account_id as parent_account_id, " 
													+ "b.carrier_account_id, "
													+ "b.last_modified_date, "
                                                    + "b.account_type as account_type_b, "
                                                    + "b.wholesale_account_id, "
													+ "RTRIM (replace(replace(XMLAGG (XMLELEMENT (e, c.product_id || ',')),'<E>'),'</E>'), ',') as product_ids, "
													+ "ROW_NUMBER() OVER (ORDER BY a.account_id) AS rnum "
													+ "from aersys_accounts a inner join ws_accts_additional_info b " 
													+ "on a.account_id = b.account_id "
													+ "and (a.account_id < 80000) "
													+ "left join AERISGEN.sub_accounts2 sa "
													+ "on sa.account_id = a.account_id "
													+ "left join sub_accounts2 sa2 "
													+ "on sa2.sa_id = sa.parent_sa_id "
													+ "left join account_product_subscription c " 
													+ "on b.account_id = c.account_id "
													+ "inner join ws_account_applications d "
													+ "on d.account_id = b.account_id "
													+ "inner join ws_application e "
													+ "on e.app_id = d.app_id "
													+ "inner join ws_app_res_permissions f "
													+ "on f.app_id = d.app_id "
													+ "inner join ws_resource g "
													+ "on g.resource_id = f.resource_id "
													+ "and g.resource_path = '/*' "
													+ "inner join ws_permission h "  
													+ "on h.permission_id = f.permission_id "
													+ "and h.description = '1111' "
													+ "left join account_contract ac on a.account_id = ac.account_id "
													+ "group by :group_by_clause "
													+ "order by b.last_modified_date)";	
	
	private static final String SQL_GET_ACCOUNT_BY_ID = "SELECT * " 
														+ "from " 
														+ "(select a.account_id, " 
														+ "a.account_type," 
														+ "a.name, " 
														+ "ac.contract_id, "
														+ "a.not_used, " 
														+ "a.www_addr, "
														+ "a.region_code, "
														+ "b.account_status, "
														+ "b.invoice_address, " 
														+ "b.special_instruction, " 
														+ "b.included_alert_profiles, "
														+ "b.tags, "  
														+ "b.approval_status, "
														+ "b.billable_status, " 
														+ "b.operator_id, " 
														+ "b.netsuite_id, " 
														+ "b.credit_limit, "
														+ "sa2.account_id as parent_account_id, " 
														+ "b.carrier_account_id, "
														+ "b.vertical_id, "
														+ "b.max_portal_accounts, "
														+ "b.max_sheduled_reports, "
														+ "b.max_report_history, "
														+ "b.service_impairment_email, "
														+ "b.my_alerts_email, "
														+ "b.scheduled_report_recipients, "
                                                        + "b.support_contact_emails, "
                                                        + "b.agent_id, "
                                                        + "b.account_type as acct_type, "
                                                        + "b.enable_create_wholesale, "
                                                        + "b.wholesale_account_id, "
                                                        + "b.distributor_id, "
														+ "e.api_key, "
														+ "RTRIM (replace(replace(XMLAGG (XMLELEMENT (e, c.product_id || ',')),'<E>'),'</E>'), ',') as product_ids, "
														+ "ROW_NUMBER() OVER (ORDER BY a.account_id) AS rnum "
														+ "from aersys_accounts a inner join ws_accts_additional_info b " 
														+ "on a.account_id = b.account_id "
														+ "and a.account_id = ? "
														+ "and b.operator_id = ? "
														+ "left join sub_accounts2 sa "
														+ "on sa.account_id = a.account_id "
														+ "left join sub_accounts2 sa2 "
														+ "on sa2.sa_id = sa.parent_sa_id "
														+ "left join account_product_subscription c " 
														+ "on b.account_id = c.account_id "
														+ "inner join ws_account_applications d "
														+ "on d.account_id = b.account_id "
														+ "inner join ws_application e "
														+ "on e.app_id = d.app_id "
														+ "inner join ws_app_res_permissions f "
														+ "on f.app_id = d.app_id "
														+ "inner join ws_resource g "
														+ "on g.resource_id = f.resource_id "
														+ "and g.resource_path = '/*' "
														+ "inner join ws_permission h "  
														+ "on h.permission_id = f.permission_id "
														+ "and h.description = '1111' "
														+ "left join account_contract ac on a.account_id = ac.account_id "
														+ "group by :group_by_clause)";
	
	private static final String SQL_GET_ACCOUNTS_BY_STATUS = "SELECT * " 
																+ "from " 
																+ "(select a.account_id, " 
																+ "a.account_type," 
																+ "a.name, " 
																+ "a.not_used, " 
																+ "b.account_status, "
																+ "b.approval_status, "
																+ "b.billable_status, " 
																+ "b.operator_id, " 
																+ "b.netsuite_id, " 
																+ "b.credit_limit, " 
																+ "sa2.account_id as parent_account_id, " 
																+ "b.carrier_account_id, "
																+ "b.last_modified_date, "
																+ "RTRIM (replace(replace(XMLAGG (XMLELEMENT (e, c.product_id || ',')),'<E>'),'</E>'), ',') as product_ids, "
																+ "ROW_NUMBER() OVER (ORDER BY a.account_id) AS rnum "
																+ "from aersys_accounts a inner join ws_accts_additional_info b " 
																+ "on a.account_id = b.account_id "
																+ "and b.operator_id = ? "
																+ "and a.account_id < 80000 "
																+ "and b.account_status in :status_list "
																+ "left join sub_accounts2 sa "
																+ "on sa.account_id = a.account_id "
																+ "left join sub_accounts2 sa2 "
																+ "on sa2.sa_id = sa.parent_sa_id "
																+ "left join account_product_subscription c " 
																+ "on b.account_id = c.account_id "
																+ "inner join ws_account_applications d "
																+ "on d.account_id = b.account_id "
																+ "inner join ws_application e "
																+ "on e.app_id = d.app_id "
																+ "inner join ws_app_res_permissions f "
																+ "on f.app_id = d.app_id "
																+ "inner join ws_resource g "
																+ "on g.resource_id = f.resource_id "
																+ "and g.resource_path = '/*' "
																+ "inner join ws_permission h "  
																+ "on h.permission_id = f.permission_id "
																+ "and h.description = '1111' "
																+ "group by :group_by_clause "
																+ "order by b.last_modified_date)"; 
	
	private static final String SQL_GET_MULTIPLE_ACCOUNTS_BY_STATUS = "SELECT * " 
																		+ "from " 
																		+ "(select a.account_id, " 
																		+ "a.account_type," 
																		+ "a.name, " 
																		+ "a.not_used, " 
																		+ "b.account_status, "
																		+ "b.approval_status, "
																		+ "b.billable_status, " 
																		+ "b.operator_id, " 
																		+ "b.netsuite_id, " 
																		+ "b.credit_limit, " 
																		+ "sa2.account_id as parent_account_id, " 
																		+ "b.carrier_account_id, "
																		+ "b.last_modified_date, "
																		+ "RTRIM (replace(replace(XMLAGG (XMLELEMENT (e, c.product_id || ',')),'<E>'),'</E>'), ',') as product_ids, "
																		+ "ROW_NUMBER() OVER (ORDER BY a.account_id) AS rnum "
																		+ "from aersys_accounts a inner join ws_accts_additional_info b " 
																		+ "on a.account_id = b.account_id "
																		+ "and b.operator_id = ? "
																		+ "and a.account_id < 80000 "
																		+ "and b.account_status in :status_list "
																		+ "and a.account_id in :account_id_list "
																		+ "left join sub_accounts2 sa "
																		+ "on sa.account_id = a.account_id "
																		+ "left join sub_accounts2 sa2 "
																		+ "on sa2.sa_id = sa.parent_sa_id "
																		+ "left join account_product_subscription c " 
																		+ "on b.account_id = c.account_id "
																		+ "inner join ws_account_applications d "
																		+ "on d.account_id = b.account_id "
																		+ "inner join ws_application e "
																		+ "on e.app_id = d.app_id "
																		+ "inner join ws_app_res_permissions f "
																		+ "on f.app_id = d.app_id "
																		+ "inner join ws_resource g "
																		+ "on g.resource_id = f.resource_id "
																		+ "and g.resource_path = '/*' "
																		+ "inner join ws_permission h "  
																		+ "on h.permission_id = f.permission_id "
																		+ "and h.description = '1111' "
																		+ "group by :group_by_clause "
																		+ "order by b.last_modified_date)";
	
	private static final String SQL_GET_MULTIPLE_ACCOUNTS = "SELECT * " 
															+ "from " 
															+ "(select a.account_id, " 
															+ "a.account_type," 
															+ "a.name, " 
															+ "a.not_used, " 
															+ "b.account_status, "
															+ "b.approval_status, "
															+ "b.billable_status, " 
															+ "b.operator_id, " 
															+ "b.netsuite_id, "
															+ "b.credit_limit, " 
															+ "sa2.account_id as parent_account_id, " 
															+ "b.carrier_account_id, "
															+ "b.last_modified_date, "
															+ "RTRIM (replace(replace(XMLAGG (XMLELEMENT (e, c.product_id || ',')),'<E>'),'</E>'), ',') as product_ids, "
															+ "ROW_NUMBER() OVER (ORDER BY a.account_id) AS rnum "
															+ "from aersys_accounts a inner join ws_accts_additional_info b " 
															+ "on a.account_id = b.account_id "
															+ "and b.operator_id = ? "
															+ "and a.account_id < 80000 "
															+ "and a.account_id in :account_id_list "
															+ "left join sub_accounts2 sa "
															+ "on sa.account_id = a.account_id "
															+ "left join sub_accounts2 sa2 "
															+ "on sa2.sa_id = sa.parent_sa_id "
															+ "left join account_product_subscription c " 
															+ "on b.account_id = c.account_id "
															+ "inner join ws_account_applications d "
															+ "on d.account_id = b.account_id "
															+ "inner join ws_application e "
															+ "on e.app_id = d.app_id "
															+ "inner join ws_app_res_permissions f "
															+ "on f.app_id = d.app_id "
															+ "inner join ws_resource g "
															+ "on g.resource_id = f.resource_id "
															+ "and g.resource_path = '/*' "
															+ "inner join ws_permission h "  
															+ "on h.permission_id = f.permission_id "
															+ "and h.description = '1111' "
															+ "group by :group_by_clause "
															+ "order by b.last_modified_date)";
	
	private static final String SQL_GET_ACCOUNT_CONTACTS = "SELECT a.contact_type, "
															+ "a.contact_name, "
															+ "a.contact_role, "
															+ "a.job_title, "
															+ "a.email, "
															+ "a.phone, "
															+ "a.created_by, "
															+ "a.date_created, "
															+ "a.last_modified_by, " 
															+ "a.last_modified_date " 
															+ "from ws_account_contacts a, ws_accts_additional_info b " 
															+ "where a.account_id = b.account_id "
															+ "and b.operator_id = ? "
															+ "and a.account_id = ? "
															+ "order by a.ws_account_contacts_id";
	
	private static final String SQL_GET_ACCOUNT_COUNT = "select count(*) as account_cnt " 
														+ "from " 
														+ "ws_accts_additional_info a " 
														+ "where a.operator_id = ? and a.account_id = ?";
		
	private static final String SQL_GET_ACCOUNTS_PAGINATION_CLAUSE = " where rnum between ? and ?";
	
	private static final String GROUP_BY_CLAUSE = "(a.account_id, " 
													+ "a.account_type," 
													+ "a.name, " 
													+ "a.not_used, " 
													+ "b.account_status, "
													+ "b.approval_status, "
													+ "b.billable_status, " 
													+ "b.operator_id, " 
													+ "b.netsuite_id, " 
													+ "b.credit_limit, "
													+ "sa2.account_id, " 
													+ "b.carrier_account_id, "
													+ "b.last_modified_date, "
                                                    + "b.account_type, "
                                                    + "b.wholesale_account_id )";
	
	private static final String GROUP_BY_CLAUSE_WITH_CONTRACT = "(a.account_id, " 
													+ "a.account_type," 
													+ "a.name, " 
													+ "ac.contract_id, "
													+ "a.not_used, " 
													+ "b.account_status, "
													+ "b.approval_status, "
													+ "b.billable_status, " 
													+ "b.operator_id, " 
													+ "b.netsuite_id, " 
													+ "b.credit_limit, " 
													+ "sa2.account_id, " 
													+ "b.carrier_account_id, "
													+ "b.last_modified_date, "
										            + "b.account_type, "
										            + "b.wholesale_account_id )";
			
			
	private static final String GROUP_BY_CLAUSE_2 = "(a.account_id, " 
													+ "a.account_type," 
													+ "a.name, " 
													+ "a.not_used, " 
													+ "a.www_addr, "
													+ "a.region_code, "
													+ "b.account_status, "
													+ "b.invoice_address, " 
													+ "b.special_instruction, " 
													+ "b.included_alert_profiles, "
													+ "b.tags, "  
													+ "b.approval_status, "
													+ "b.billable_status, " 
													+ "b.operator_id, " 
													+ "b.netsuite_id, " 
													+ "b.credit_limit, "
													+ "sa2.account_id, " 
													+ "b.carrier_account_id, "
													+ "b.vertical_id, "
													+ "b.max_portal_accounts, "
													+ "b.max_sheduled_reports, "
													+ "b.max_report_history, "
													+ "b.service_impairment_email, "
													+ "b.my_alerts_email, "
													+ "b.scheduled_report_recipients, "
                                                    + "b.support_contact_emails, "
                                                    + "b.agent_id, "
                                                    + "e.api_key, "
                                                    + "b.account_type, "
                                                    + "b.enable_create_wholesale, "
                                                    + "b.wholesale_account_id, "
                                                    + "b.distributor_id "
                                                    + ")";
	
	private static final String GROUP_BY_CLAUSE_2_WITH_CONTRACT = "(a.account_id, " 
			+ "a.account_type," 
			+ "a.name, " 
			+ "ac.contract_id, "
			+ "a.not_used, " 
			+ "a.www_addr, "
			+ "a.region_code, "
			+ "b.account_status, "
			+ "b.invoice_address, " 
			+ "b.special_instruction, " 
			+ "b.included_alert_profiles, "
			+ "b.tags, "  
			+ "b.approval_status, "
			+ "b.billable_status, " 
			+ "b.operator_id, " 
			+ "b.netsuite_id, " 
			+ "b.credit_limit, " 
			+ "sa2.account_id, " 
			+ "b.carrier_account_id, "
			+ "b.vertical_id, "
			+ "b.max_portal_accounts, "
			+ "b.max_sheduled_reports, "
			+ "b.max_report_history, "
			+ "b.service_impairment_email, "
			+ "b.my_alerts_email, "
			+ "b.scheduled_report_recipients, "
            + "b.support_contact_emails, "
            + "b.agent_id, "
            + "e.api_key, "
            + "b.account_type, "
            + "b.enable_create_wholesale, "
            + "b.wholesale_account_id, "
            + "b.distributor_id "
            + ")";
	
	private static final String SQL_GET_ACTIVE_CHILD_ACCOUNT_COUNT = "SELECT COUNT(*) "+
													"FROM aersys_accounts "+
													"WHERE account_id IN "+
													"(SELECT account_id "+
													"FROM sub_accounts2 "+
													"WHERE parent_sa_id = "+
													"(SELECT sa_id FROM sub_accounts2 WHERE account_id = ?) "+
													") AND not_used = 0";
	
	private static final String SQL_GET_ACCOUNT_NAME_COUNT = 	"SELECT COUNT(*) as cnt "+
																"FROM aersys_accounts a, ws_accts_additional_info b "+
																"WHERE a.account_id = b.account_id "+ 
																"and b.operator_id = ? "+
																"and a.name = ? "+
																"and a.account_id != ?";
    
    private static final String SQL_GET_ACCOUNT_NAME_COUNT_DISTRIBUTOR = 	"SELECT COUNT(*) as cnt "+
																"FROM aersys_accounts a, ws_accts_additional_info b "+
																"WHERE a.account_id = b.account_id "+ 
																"and b.operator_id = ? "+
																"and a.name = ? "+
																"and a.account_id != ? " +
                                                                "and b.distributor_id = ?";
    private static final String SQL_GET_ACCESS_LINK_COUNT = 	"SELECT COUNT(*) as cnt "+
			"FROM aerisgen.distributor a, aerisgen.ws_accts_additional_info b "+
			"WHERE a.aeris_account_id = b.account_id "+ 
			"and b.operator_id = ? "+
			"and a.access_link = ? "+
			"and a.aeris_account_id != ?";
	private static final String SQL_GET_AUX_SERVICE_FEATURE_DESC = "select ao.account_id, "
																	+ "xsf.service_id, " 
																	+ "af.feature_code "
																	+ "from " 
																	+ "aersys_accounts ao, "
																	+ "(select a.account_id, "
																	+ "RTRIM (replace(replace(XMLAGG (XMLELEMENT (xs, SERVICE_ID || ',')),'<XS>'),'</XS>'), ',') as service_id "
																	+ "from "
																	+ "aersys_accounts a "
																	+ "left outer join AERISGEN.ACCOUNT_AUXSERVICE xs "
																	+ "on xs.ACCOUNT_ID = a.ACCOUNT_ID "
																	+ "where a.ACCOUNT_ID = ? "
																	+ "group by a.account_id) xsf, "
																	+ "WS_ACCT_FEATURES af	"
																	+ "where ao.account_id = xsf.account_id	"
																	+ "and af.ACCOUNT_ID = ao.ACCOUNT_ID "
																	+ "and xsf.account_id = ao.account_id ";
		
	private static final String SQL_DELETE_ACCOUNT_INFO = "delete from aersys_accounts where account_id = :account_id";
	
	private static final String SQL_DELETE_ACCT_ADDITIONAL_INFO = "delete from ws_accts_additional_info where account_id = :account_id and operator_id = :operator_id";
	
	private static final String SQL_DELETE_ACCOUNT_CONTACT_INFO = "delete from ws_account_contacts where account_id = :account_id";
	
	private static final String SQL_DELETE_ACCOUNT_HEIRARCHY = "delete from sub_accounts2 where account_id = :account_id";
	
	private static final String SQL_UPDATE_ACCOUNT_HEIRARCHY = "update sub_accounts2 set parent_sa_id = ?, modified_by = ?, modified_date = ? where sa_id = ?";
	
	private static final String SQL_DELETE_ACCOUNT_PRODUCT_SUBSCRIPTION = "delete from account_product_subscription where account_id = :account_id";

	private static final String SQL_INSERT_FEATURE_CODE = "insert into aerisgen.ws_acct_features (account_id, feature_code) values (?, ?)";
	
	private static final String SQL_DELETE_FEATURE_CODE = "delete from aerisgen.ws_acct_features where account_id = ? and feature_code = ?";
	
	private static final String SQL_UPDATE_DISTRIBUTOR_ID = "update aerisgen.ws_accts_additional_info set distributor_id = ? where account_id = ?";
	
	private static final String SQL_CREATE_DISTRIBUTOR = "INSERT INTO aerisgen.distributor (" +
			"DISTRIBUTOR_ID, " +
			"OPERATOR_ID, " +
			"DISTRIBUTOR_NAME, " +
			"AERIS_ACCOUNT_ID, " +
			"CONTACT_EMAILS, " +
            "ACCESS_LINK, " +
			"PRIVACY_POLICY_URL, " +
			"STATUS, " +
            "LIFERAY_COMPANY_ID, " +
            "STORE_URL, " +
            "IS_WHOLESALE, " +
            "BRAND, " +
            "PLAN_PRICING_URL, " +
            "REALM, " +
			"CREATED_BY, " +
			"CREATED_DATE, " +
			"LAST_MODIFIED_BY, " +
			"LAST_MODIFIED_DATE, " +
			"REP_TIMESTAMP )" +
			"values " +
			"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, sysgmtdate, ?, sysgmtdate, systimestamp)";
	private static final String SQL_UPDATE_DISTRIBUTOR = "UPDATE aerisgen.distributor set " +
			"DISTRIBUTOR_NAME = ?, " +
			"AERIS_ACCOUNT_ID = ?, " +
			"CONTACT_EMAILS = ?, " +
            "ACCESS_LINK = ?, " +
			"PRIVACY_POLICY_URL = ?, " +
			"STATUS = ?, " +
            "LIFERAY_COMPANY_ID = ?, "+
            "STORE_URL = ?, "+
            "IS_WHOLESALE = ?, "+
            "PLAN_PRICING_URL = ?, " +
			"LAST_MODIFIED_BY = ?, " +
			"LAST_MODIFIED_DATE = sysgmtdate, " +
			"REP_TIMESTAMP = systimestamp " +			
			"where distributor_id = ? and operator_id = ?";
	
	private static final String SQL_INSERT_ACCOUNT_CONTRACT = 	"INSERT INTO AERBILL_PROV.ACCOUNT_CONTRACT (ACCOUNT_ID, CONTRACT_ID, CREATED_DATE, CREATED_BY, LAST_MODIFIED_DATE, LAST_MODIFIED_BY) "+
																" values (?,?,?,?,?,?)";

	private static final String SQL_UPDATE_ACCOUNT_CONTRACT = 	"update AERBILL_PROV.ACCOUNT_CONTRACT set CONTRACT_ID = ?, CREATED_DATE = ?, CREATED_BY = ?, LAST_MODIFIED_DATE = ?, LAST_MODIFIED_BY = ? " +
                                                                "where ACCOUNT_ID = ? ";

	private static final String SQL_DELETE_ACCOUNT_CONTRACT = 	"DELETE FROM AERBILL_PROV.ACCOUNT_CONTRACT where account_id in (?) ";

	private static final String SQL_GET_ACCOUNT_CONTRACT_COUNT = "Select count(*) as contractCount from AERBILL_PROV.ACCOUNT_CONTRACT where account_id = ?";

    private static final int FEATURE_CODE_CONNECTIVITY = 1;
	
	private static final int FEATURE_CODE_AERCLOUD = 2;
	
	@Inject
	private IAccountApplicationDAO accountApplicationDAO;
	
	@Inject
	private IServiceProfileDAO serviceProfileDAO;

	@Inject
	private IStoredProcedure<List<Account>> childAccountStoredProc;
	
	@Inject
	private OAUtils oaUtils;
    
    @Inject
	private IOperatorProductDAO operatorProductDAO;
	
	private Cache<Long, Account> cache;
	private Cache<Long, Map<Long, Account>> childCache;
	private Map<Long, Long> parentChildMap;
	
	@Inject 
	IContractEnum contractEnum;
	
	
	public void init(@Hazelcast(cache = "AccountCache/id") Cache<Long, Account> cache, @Hazelcast(cache = "AccountCache/children") Cache<Long, Map<Long, Account>> childCache) {
		this.cache = cache;
		this.childCache = childCache;
		this.parentChildMap = new HashMap<Long, Long>();
		
		List<Account> accountsList = getAllAccounts();
		Account[] accounts = new Account[accountsList.size()];
		accountsList.toArray(accounts);
		
		putInCache(accounts);
	}
	
	@Override
	public List<Account> getRetailAccounts(final String operatorId, String start, String count, String wholesaleAccountId) {
		List<Account> accounts = new ArrayList<Account>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			LOG.debug("getRetialAccounts: Fetched db connection");
		 
			// provides pagination support
			ps = buildGetRetailAccountsByWholesaleAccountQuery(conn, start, count, operatorId, wholesaleAccountId);
			
			LOG.debug("getRetialAccounts: executed query: " + ps);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Account account = new Account();
				account.setAccountId(rs.getLong("account_id"));
				account.setAccountName(rs.getString("name"));
				account.setStatus(AccountStatus.fromValue(rs.getInt("account_status")));
				account.setApprovalStatus(ApprovalStatus.fromValue(rs.getInt("approval_status")));
				
				// set Products
				List<String> productIds = getProductIdsForAccount(rs.getString("product_ids"));
				account.setProductIds(productIds);
				
				account.setBillableStatus(BillableStatus.fromValue(rs.getInt("billable_status")));
				account.setCarrierAccountId(rs.getString("carrier_account_id"));
				account.setNetSuiteId(rs.getString("netsuite_id"));
				account.setOperatorId(rs.getInt("operator_id"));
				account.setParentAccountId(rs.getString("parent_account_id"));
                account.setLastModifiedDate(rs.getDate("last_modified_date"));
                                    account.setAccountType(rs.getString("account_type_b"));
                account.setWholesaleAccountId(rs.getString("wholesale_account_id"));
				
				accounts.add(account);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getRetialAccounts", e);
			throw new GenericServiceException("Unable to getRetialAccounts", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getRetialAccounts" + e.getMessage());
			throw e;
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			DBUtils.cleanup(conn, ps, rs);
		}
		//put the newly fetched accounts in cache.
		if(accounts.size() > 0) {
			Account[] accountsArr = new Account[accounts.size()];
			accounts.toArray(accountsArr);
			putInCache(accountsArr);
		}

		LOG.info("getRetialAccounts: Returned " + accounts.size() + " accounts to the caller");

		return accounts;
	}
	@Override
	public List<Account> getAllAccounts(final String operatorId, String start, String count, List<String> accountIds, boolean wholesaleManagementEnabled,
			final AccountStatus... accountStatusList) {
		List<Account> accounts = new ArrayList<Account>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		// If already loaded, fetch from cache, if the cache returned accounts list
		 List<Account> cachedAccounts = getCachedAccounts(Integer.parseInt(operatorId), start, count, accountIds, accounts, wholesaleManagementEnabled, accountStatusList) ;
		if(!cache.getValues().isEmpty() && cachedAccounts.size() > 0) {
			return cachedAccounts;
		} else {
			//fetch the accounts from DB and update the cache as well.
			try {
				DBConnectionManager connectionManager = DBConnectionManager.getInstance();
				conn = connectionManager.getProvisionDatabaseConnection();
				
				LOG.debug("getAllAccounts: Fetched db connection");
				
				// Fetch the Accounts for the specified account ids and status or
				// get all accounts
				// provides pagination support
				ps = buildGetAccountsByOperatorQuery(conn, start, count, operatorId, accountIds, wholesaleManagementEnabled, accountStatusList);
				
				LOG.debug("getAllAccounts: executed query: " + ps);
				rs = ps.executeQuery();
				
				while (rs.next()) {
					Account account = new Account();
					account.setAccountId(rs.getLong("account_id"));
					account.setAccountName(rs.getString("name"));
					account.setContractId(rs.getInt("contract_id"));
					account.setStatus(AccountStatus.fromValue(rs.getInt("account_status")));
					account.setApprovalStatus(ApprovalStatus.fromValue(rs.getInt("approval_status")));
					
					// set Products
					List<String> productIds = getProductIdsForAccount(rs.getString("product_ids"));
					account.setProductIds(productIds);
					
					account.setBillableStatus(BillableStatus.fromValue(rs.getInt("billable_status")));
					account.setCarrierAccountId(rs.getString("carrier_account_id"));
					account.setNetSuiteId(rs.getString("netsuite_id"));
					account.setOperatorId(rs.getInt("operator_id"));
					account.setParentAccountId(rs.getString("parent_account_id"));
                    account.setLastModifiedDate(rs.getDate("last_modified_date"));
                                        account.setAccountType(rs.getString("account_type_b"));
                    account.setWholesaleAccountId(rs.getString("wholesale_account_id"));
					account.setCreditLimit(rs.getLong("credit_limit"));
					accounts.add(account);
				}
			} catch (SQLException e) {
				LOG.error("Exception during getAllAccounts", e);
				throw new GenericServiceException("Unable to getAllAccounts", e);
			} catch (GenericServiceException e) {
				LOG.error("ServiceException in getAllAccounts" + e.getMessage());
				throw e;
			} finally {
				try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				DBUtils.cleanup(conn, ps, rs);
			}
			//put the newly fetched accounts in cache.
			if(accounts.size() > 0) {
				Account[] accountsArr = new Account[accounts.size()];
				accounts.toArray(accountsArr);
				putInCache(accountsArr);
			}
		}

		LOG.info("getAllAccounts: Returned " + accounts.size() + " accounts to the caller");

		return accounts;
	}

	private List<Account> getCachedAccounts(final int operatorId, String start, String count, List<String> accountIds, List<Account> accounts, boolean wholesaleManagementEnabled, final AccountStatus... accountStatusList) {
		List<Account> cachedAccts = new ArrayList<Account>();
		long rownumStart = 0;
		long rownumEnd = 0;
		

		if (accountIds != null && !accountIds.isEmpty()) {
			for (int i = 0; i < accountIds.size(); i++) {
				String accountId = accountIds.get(i);

				if (NumberUtils.isNumber(accountId)) {
					Account account = cache.get(Long.parseLong(accountId));

					if (account != null && operatorId == account.getOperatorId()) {
						cachedAccts.add(account);
					}
				}
			}
		} else {
			return new ArrayList<Account>(Collections2.filter(cache.getValues(), new Predicate<Account>() {
				@Override
				public boolean apply(Account input) {
					return (operatorId == input.getOperatorId() &&
							(null == input.getWholesaleAccountId() || input.getWholesaleAccountId().isEmpty()));
				}
				
			}));
		}

		if (NumberUtils.isNumber(start) && NumberUtils.isNumber(count)) {
			rownumStart = Long.parseLong(start);
			rownumEnd = Long.parseLong(start) + Long.parseLong(count);
		}
		
		for (int i = 0; i < cachedAccts.size(); i++) {
			boolean success = true;
			Account account = cachedAccts.get(i);

			for (AccountStatus accountStatus : accountStatusList) {
				success = success && (accountStatus == account.getStatus());

				if (success) {
					break;
				}
			}

			if (rownumStart > 0 && rownumEnd > rownumStart) {
				success = success && rownumStart <= i && i <= rownumEnd;
			}

			if (!success) {
				cachedAccts.remove(account);
			}
		}

		return cachedAccts;
	}

	private List<String> getProductIdsForAccount(String productId) {
		Set<String> productIds = new HashSet<String>();

		if (productId != null) {
			String[] productIdArr = productId.split(",");
			Collections.addAll(productIds, productIdArr);
		}

		return new ArrayList<String>(productIds);
	}

	private PreparedStatement buildGetAccountsByOperatorQuery(Connection conn, String start, String count, String operatorId,
			List<String> accountIds, boolean wholesaleManagementEnabled, AccountStatus... accountStatusList) throws SQLException {
		PreparedStatement ps;

		// Check if pagination parameters are valid
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		// Check if status is a search field
		boolean queryByStatus = ArrayUtils.isNotEmpty(accountStatusList);

		// Check if status is a search field
		boolean filterAccountIds = accountIds != null && !accountIds.isEmpty();

		// Use this Query if the account id and status are not in account search
		// criteria
		StringBuilder query = new StringBuilder(SQL_GET_ACCOUNTS_BY_OPERATOR);

		// Use this Query if the account id and status is used in the account
		// search criteria
		if (queryByStatus && filterAccountIds) {
			LOG.info("Creating query to fetch accounts filtered by accountIds: " + accountIds + " and statuses: " + accountStatusList);
			query = buildGetAccountsWithAccountIdsAndStatusQuery(accountIds, accountStatusList);
		}
		// Use this Query if the status is only used in the account search
		// criteria
		else if (queryByStatus) {
			LOG.info("Creating query to fetch accounts filtered by statuses: " + accountStatusList);
			query = buildGetAccountsWithStatusQuery(accountStatusList);
		}
		// Use this Query if the account ids is only used in the account search
		// criteria
		else if (filterAccountIds) {
			LOG.info("Creating query to fetch accounts filtered by accountIds: " + accountIds);
			query = buildGetAccountsWithAccountIdsQuery(accountIds);
		}

		// Append Pagination parameters
		if (paginated) {
			LOG.info("Pagination Parameters are appended to the query");
			query.append(SQL_GET_ACCOUNTS_PAGINATION_CLAUSE);
		}

		// Add Group By Clause
		String sqlQuery = query.toString().replace(":group_by_clause", GROUP_BY_CLAUSE_WITH_CONTRACT);

		// Create Prepared Statement
		ps = conn.prepareStatement(sqlQuery);

		int i = 0;

		// Set Operator Id
		ps.setString(++i, operatorId);

//		// Set Account Range for operator
//		ps.setInt(++i, getStartRange(operatorId));
//		ps.setInt(++i, getEndRange(operatorId));

		// Set Status and Account Id values
		if (queryByStatus && filterAccountIds) {
			i = setStatus(ps, i, accountStatusList);
			i = setAccountIds(accountIds, ps, i);
		} else if (queryByStatus) {
			i = setStatus(ps, i, accountStatusList);
		} else if (filterAccountIds) {
			i = setAccountIds(accountIds, ps, i);
		}

		// paginate the results if required
		if (paginated) {
			long rownumStart = Long.parseLong(start);
			long rownumEnd = Long.parseLong(start) + Long.parseLong(count);

			ps.setLong(++i, rownumStart);
			ps.setLong(++i, rownumEnd);
		}

		LOG.debug("SQL Query is: " + sqlQuery);

		return ps;
	}
	private PreparedStatement buildGetRetailAccountsByWholesaleAccountQuery(Connection conn, String start, String count, String operatorId,
			String wholesaleAccountId) throws SQLException {
		PreparedStatement ps;

		// Check if pagination parameters are valid
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);
  
		// Use this Query if the account id and status are not in account search
		// criteria
		StringBuilder query = new StringBuilder(SQL_GET_RETAIL_ACCOUNTS_BY_WHOLESALE_ACCOUNT);
		// Append Pagination parameters
		if (paginated) {
			LOG.info("Pagination Parameters are appended to the query");
			query.append(SQL_GET_ACCOUNTS_PAGINATION_CLAUSE);
		}

		// Add Group By Clause
		String sqlQuery = query.toString().replace(":group_by_clause", GROUP_BY_CLAUSE);

		// Create Prepared Statement
		ps = conn.prepareStatement(sqlQuery);

		int i = 0;

		// Set Operator Id
		ps.setString(++i, operatorId);
		ps.setString(++i, wholesaleAccountId);

//		// Set Account Range for operator
//		ps.setInt(++i, getStartRange(operatorId));
//		ps.setInt(++i, getEndRange(operatorId));

		// paginate the results if required
		if (paginated) {
			long rownumStart = Long.parseLong(start);
			long rownumEnd = Long.parseLong(start) + Long.parseLong(count);

			ps.setLong(++i, rownumStart);
			ps.setLong(++i, rownumEnd);
		}

		LOG.debug("SQL Query is: " + sqlQuery);

		return ps;
	}

	private int setAccountIds(List<String> accountIds, PreparedStatement ps, int i) throws SQLException {
		LOG.debug("Setting accountIds in the Query: " + accountIds);

		int j = 0;

		// In clause for account ids
		while (j < accountIds.size()) {
			String accountId = accountIds.get(j++);

			if (NumberUtils.isNumber(accountId)) {
				ps.setLong(++i, Long.parseLong(accountId));
			}
		}

		return i;
	}

	private int setStatus(PreparedStatement ps, int i, AccountStatus... accountStatusList) throws SQLException {
		LOG.debug("Setting statuses in the Query: " + accountStatusList);

		int j = 0;

		// In clause for status
		while (j < accountStatusList.length) {
			AccountStatus accountStatus = accountStatusList[j++];

			if (accountStatus != null) {
				int status = accountStatus.getValue();
				ps.setInt(++i, status);
			}
		}

		return i;
	}

	private StringBuilder buildGetAccountsWithAccountIdsQuery(List<String> accountIds) {
		StringBuilder query;
		// Build getAccounts by account ids query
		StringBuffer inClause = new StringBuffer("(");

		for (int i = 0; i < accountIds.size(); i++) {
			inClause.append("?");

			if (i != accountIds.size() - 1) {

				// Append if account id is valid
				if (NumberUtils.isNumber(accountIds.get(i))) {
					inClause.append(",");
				}
			}
		}

		inClause.append(")");

		String getAccountsWithStatusQuery = SQL_GET_MULTIPLE_ACCOUNTS.replace(":account_id_list", inClause.toString());

		query = new StringBuilder(getAccountsWithStatusQuery);
		return query;
	}

	private StringBuilder buildGetAccountsWithStatusQuery(AccountStatus... accountStatusList) {
		StringBuilder query;

		// Build getAccounts by Status query
		StringBuffer inClause = new StringBuffer();

		for (int i = 0; i < accountStatusList.length; i++) {
			if (accountStatusList[i] != null) {
				inClause.append("?");

				if (i != accountStatusList.length - 1) {
					inClause.append(",");
				}
			}
		}

		if (StringUtils.isNotEmpty(inClause.toString())) {
			inClause = new StringBuffer("(").append(inClause.toString()).append(")");
		}

		String getAccountsWithStatusQuery = SQL_GET_ACCOUNTS_BY_STATUS.replace(":status_list", inClause.toString());

		query = new StringBuilder(getAccountsWithStatusQuery);
		return query;
	}

	private StringBuilder buildGetAccountsWithAccountIdsAndStatusQuery(List<String> accountIds, AccountStatus... accountStatusList) {
		StringBuilder query;

		// Build getAccounts by Status query
		StringBuffer inClause = new StringBuffer();

		for (int i = 0; i < accountStatusList.length; i++) {
			if (accountStatusList[i] != null) {
				inClause.append("?");

				if (i != accountStatusList.length - 1) {
					inClause.append(",");
				}
			}
		}

		if (StringUtils.isNotEmpty(inClause.toString())) {
			inClause = new StringBuffer("(").append(inClause.toString()).append(")");
		}

		String getAccountsWithStatusQuery = SQL_GET_MULTIPLE_ACCOUNTS_BY_STATUS.replace(":status_list", inClause.toString());

		// Add account ids
		inClause = new StringBuffer("(");

		for (int i = 0; i < accountIds.size(); i++) {
			inClause.append("?");

			if (i != accountIds.size() - 1) {

				// Append if account id is valid
				if (NumberUtils.isNumber(accountIds.get(i))) {
					inClause.append(",");
				}
			}
		}

		inClause.append(")");

		getAccountsWithStatusQuery = getAccountsWithStatusQuery.replace(":account_id_list", inClause.toString());

		query = new StringBuilder(getAccountsWithStatusQuery);
		return query;
	}

	@Override
	public Account getAccount(String operatorId, String accountId) {
		Account account = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAccount: Fetched db connection");

			// Fetch a Single Account
			ps = conn.prepareStatement(SQL_GET_ACCOUNT_BY_ID.replace(":group_by_clause", GROUP_BY_CLAUSE_2_WITH_CONTRACT));

			ps.setLong(1, Long.parseLong(accountId));
			ps.setLong(2, Long.parseLong(operatorId));

			LOG.debug("getAccountById: executed query: " + ps);
			rs = ps.executeQuery();

			if (rs.next()) {
				account = new Account();

				// Set Basic Information
				account.setAccountId(rs.getLong("account_id"));
				account.setAccountName(rs.getString("name"));
				account.setContractId(rs.getInt("contract_id"));
				account.setWebAddress(rs.getString("www_addr"));
				account.setStatus(AccountStatus.fromValue(rs.getInt("account_status")));
				account.setApprovalStatus(ApprovalStatus.fromValue(rs.getInt("approval_status")));
				account.setIncludedAlertProfiles(rs.getInt("included_alert_profiles"));
				account.setRegion(Region.fromValue(rs.getInt("region_code")));
				
				// Set Invoice Address
				if (rs.getString("invoice_address") != null) {
					InvoiceAddress address = new InvoiceAddress();
					address.setFullAddress(rs.getString("invoice_address"));
					account.setInvoiceAddress(address);
				}

				// Set Products
				List<String> productIds = getProductIdsForAccount(rs.getString("product_ids"));
				account.setProductIds(productIds);

				account.setSpecialInstructions(rs.getString("special_instruction"));
				account.setTags(rs.getString("tags"));
				account.setBillableStatus(BillableStatus.fromValue(rs.getInt("billable_status")));
				account.setCarrierAccountId(rs.getString("carrier_account_id"));
				account.setNetSuiteId(rs.getString("netsuite_id"));
				account.setOperatorId(rs.getInt("operator_id"));
				account.setParentAccountId(rs.getString("parent_account_id"));
				account.setApiKey(rs.getString("api_key"));
				account.setVerticalId(rs.getString("vertical_id"));				
				account.setMaxPortalAccounts(rs.getInt("max_portal_accounts"));
				account.setMaxScheduledReports(rs.getInt("max_sheduled_reports"));
				account.setMaxReportHistory(rs.getInt("max_report_history"));
				account.setServiceImpairmentEmail(StringUtils.split(rs.getString("service_impairment_email"), EMAIL_SEPERATOR));
				account.setMyAlertsEmail(StringUtils.split(rs.getString("my_alerts_email"), EMAIL_SEPERATOR));
				account.setScheduledReportsEmail(StringUtils.split(rs.getString("scheduled_report_recipients"), EMAIL_SEPERATOR));
                account.setSupportContactEmails(StringUtils.split(rs.getString("support_contact_emails"), EMAIL_SEPERATOR));
                account.setAgentId(rs.getString("agent_id"));
                account.setAccountType(rs.getString("acct_type"));
                account.setEnableCreateWholesale(rs.getBoolean("enable_create_wholesale"));
                account.setWholesaleAccountId(rs.getString("wholesale_account_id"));
                account.setDistributorId(rs.getString("distributor_id"));
                account.setCreditLimit(rs.getLong("credit_limit"));
				// Set Contacts
				setContacts(account, rs, conn);
				// Set Overage bucket
				setAccountOverageBucket(account, conn);
				//set aux service and feature code desc
				setAuxServicesFeatureCodes(account, conn);
				if (account.getAccountType().equalsIgnoreCase("wholesale") || account.getAccountType().equalsIgnoreCase("distributor")) {
				try {
						account.setDistributor(distributorDAO.getDistributorByAerisAccountId(
								Long.parseLong(operatorId),
								account.getAccountId()));
				} catch (NumberFormatException e) {
					LOG.error("Exception during getDistributor", e);
						/*throw new GenericServiceException(
								"Unable to getAccount", e);*/
				} catch (DistributorNotFoundException e) {
					LOG.error("Exception during getDistributor", e);
						/*throw new GenericServiceException(
								"Unable to getAccount", e);*/
				} catch (DistributorDBException e) {
					LOG.error("Exception during getDistributor", e);
						/*throw new GenericServiceException(
								"Unable to getAccount", e);*/
					}
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception during getAccount", e);
			throw new GenericServiceException("Unable to getAccount", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("Returned Account to the caller: " + account);

		return account;
	}

	private void setContacts(Account account, ResultSet rs, Connection conn) throws SQLException {
		String operatorId = String.valueOf(account.getOperatorId());
		String accountId = String.valueOf(account.getAccountId());

		List<Contact> contacts = fetchContactsForAccount(operatorId, accountId, conn);

		int i = 0;

		for (Contact contact : contacts) {
			if (contact.isValid()) {
				
				if (i == 0) {
					account.setPrimaryContact(contact);
				} else {
					account.getAdditionalContacts().add(contact);
				}
				i++;
			}
		}
	}
	
	
	private void setAccountOverageBucket(Account account, Connection conn) throws SQLException {
		String accountId = String.valueOf(account.getAccountId());
		List<AccountOverageBucket> accOverageBktList = new ArrayList<AccountOverageBucket>();
		String getAccOverageBkt = "select * from aerisgen.ACCOUNT_OVERAGE_BUCKET where account_id = " + accountId ;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// Create Prepared Statement
			ps = conn.prepareStatement(getAccOverageBkt);
			LOG.info("getAccountOverageBuckets: validating if account exists: " + accountId);
			rs = ps.executeQuery();
			while(rs.next()) {
				AccountOverageBucket accountOverageBucket = new AccountOverageBucket();
				accountOverageBucket.setAccountId(Long.valueOf(accountId));
				accountOverageBucket.setAutoAdd(rs.getInt("auto_add"));
				accountOverageBucket.setBucketPrice(rs.getDouble("bucket_price"));
				accountOverageBucket.setBucketSize(rs.getInt("bucket_size"));
				accountOverageBucket.setBucketSizeType(rs.getString("bucket_size_type"));
				double overageSize = accountOverageBucket.getBucketSize();
				if(accountOverageBucket.getBucketSizeType() != null && accountOverageBucket.getBucketSizeType().equalsIgnoreCase("GB")){
					overageSize = overageSize / 1024 ;
					BigDecimal bDec = new BigDecimal(String.valueOf(overageSize)).setScale(2, BigDecimal.ROUND_HALF_UP);
					overageSize = bDec.doubleValue();

				}
				accountOverageBucket.setBucketSize(overageSize);
				accountOverageBucket.setNoAccessFeeDeviceCount(rs.getInt("no_access_fee_device_count"));
				accountOverageBucket.setStartDate(rs.getDate("start_date"));
				accountOverageBucket.setEndDate(rs.getDate("end_date"));
				accountOverageBucket.setCreatedDate(rs.getDate("created_date"));
				accountOverageBucket.setUpdatedDate(rs.getDate("last_modified_date"));
				accountOverageBucket.setCreatedBy(rs.getString("created_by"));
				accountOverageBucket.setUpdatedBy(rs.getString("last_modified_by"));
				accountOverageBucket.setRepTimeStamp(ApplicationUtils.getGMTDateStr(rs.getTimestamp("rep_timestamp")));
				accOverageBktList.add(accountOverageBucket);
			}
			account.setAccountOverageBucket(accOverageBktList);
		} catch (SQLException e) {
			LOG.error("Exception during getAccountOverageBucket for accountId= " + accountId, e);
			throw new GenericServiceException("Exception during getAccountOverageBucket", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		
	}

	@Override
	public List<Account> getChildAccounts(String operatorId, String accountId) throws AccountNotFoundException, AccountException {
		List<Account> accounts = new ArrayList<Account>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean accountExists = false;
		long acctId = Long.parseLong(accountId);
		
		// If available in cache
		if(!childCache.getValues().isEmpty())
		{
			Map<Long, Account> childMap = childCache.get(acctId);
			
			if(childMap != null)
			{
				return new ArrayList<Account>(childMap.values());
			}
		}
		
		try {
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_ACCOUNT_COUNT);
			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, Long.parseLong(accountId));

			LOG.info("getChildAccounts: validating if account exists: " + accountId);
			rs = ps.executeQuery();

			if (rs.next()) {
				accountExists = (rs.getInt("account_cnt") > 0);// Set if account
																// exists
			}

			if (!accountExists) {
				throw new AccountNotFoundException("Account does not exist for account id: " + accountId);
			}
		} catch (SQLException e) {
			LOG.error("DB Exception during getChildAccounts - validateAccount", e);
			throw new AccountException("DB Exception during getChildAccounts - validateAccount", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		try {
			//Fetch Child Accounts from DB
			accounts = childAccountStoredProc.execute(accountId, operatorId, null);
			 
			// Put In Cache 
			putInChildCache(acctId, accounts);
			
			LOG.info("GetChildAccountsStoredProc: Returned " + accounts.size() + " accounts to the caller");		
		} catch (StoredProcedureException e) {
			LOG.error("DB Exception during getChildAccounts", e);
			throw new AccountException("DB Exception during getChildAccounts", e);
		} 

		LOG.info("Returned Child Accounts to the caller: " + accounts);

		return accounts;
	}

	@Override
	public List<Contact> getContactsForAccount(String operatorId, String accountId) throws AccountNotFoundException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean accountExists = false;
		List<Contact> contacts = null;

		try {
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_ACCOUNT_COUNT);
			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, Long.parseLong(accountId));

			LOG.info("getContactsForAccount: validating if account exists: " + accountId);
			
			rs = ps.executeQuery();

			if (rs.next()) {
				accountExists = (rs.getInt("account_cnt") > 0);// Set if account exists
			}

			if (!accountExists) {
				throw new AccountNotFoundException("Account does not exist for account id: " + accountId);
			}

			// Fetch contacts from helper method
			contacts = fetchContactsForAccount(operatorId, accountId, conn);
		} catch (SQLException e) {
			LOG.error("Exception during getContactsForAccount - validateAccount", e);
			throw new GenericServiceException("Exception during getContactsForAccount", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return contacts;
	}

	private List<Contact> fetchContactsForAccount(String operatorId, String accountId, Connection conn) {
		List<Contact> contacts = new ArrayList<Contact>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			// Fetch Contacts
			ps = conn.prepareStatement(SQL_GET_ACCOUNT_CONTACTS);

			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, Long.parseLong(accountId));

			LOG.debug("getContactsForAccount: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				Contact contact = new Contact();

				setContactName(rs, contact);
				contact.setRole(ContactType.fromValue(rs.getInt("contact_type")));
				contact.setJobTitle(rs.getString("job_title"));
				contact.setEmail(rs.getString("email"));
				contact.setPhoneNumber(rs.getString("phone"));

				contacts.add(contact);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getContactsForAccount", e);
			throw new GenericServiceException("Unable to getContactsForAccount", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		LOG.info("getContactsForAccount: Returned " + contacts.size() + " contacts to the caller");

		return contacts;
	}

	private String setContactName(ResultSet rs, Contact contact) throws SQLException {
		String contactName = rs.getString("contact_name");

		if (StringUtils.isNotEmpty(contactName)) {
			String[] contactNameArr = contactName.split(SEPARATOR, 2);

			if (contactNameArr != null) {
				if (contactNameArr.length == 2) {
					contact.setFirstName(contactNameArr[0]);
					contact.setLastName(contactNameArr[1]);
				} else if (contactNameArr.length == 1) {
					contact.setFirstName(contactNameArr[0]);
				}
			}
		}
		return contactName;
	}

	@Override
	public Account createNewAccount(String operatorId, String accountName, Region region, AccountType accountType, AccountStatus status, ApprovalStatus approvalStatus, 
			InvoiceAddress invoiceAddress, List<String> productIds, String parentAccountId, String parentLabel, String carrierAccountId, Contact primaryContact, 
			List<Contact> additionalContacts, String webAddress, BillableStatus billableStatus, BillingDetails billingDetails, String netSuiteId, String specialInstructions, 
			String tags, int includedAlertProfiles, String verticalId, int maxPortalAccounts, int maxScheduledReports, int maxReportHistory, String[] serviceImpairmentEmail, 
			String[] myAlertsEmail, String[] scheduledReportsEmail, String[] supportContactEmails, Date requestedDate, String requestedUser, 
			List<AccountOverageBucket> accountOverageBucket, String agentId, String distributorId, String acctType, boolean enableCreateWholesale, String wholesaleAccountId, 
			Distributor distRequest, long creditLimit)
			throws AccountException, SubAccountException, ApiKeyException, DuplicateAccountException {
		Account account = null;
		Connection conn = null;

		LOG.info("createNewAccount: processing new account creation...");

		// Generate account Id for the new account
		long newAccountId = 0;
		
		boolean contractInsertedInCache = false;
		
		try {
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			
			// Set Auto Commit as false
			conn.setAutoCommit(false);

			newAccountId = generateNewAccountId(conn, Integer.parseInt(operatorId));
			LOG.info("Fetched accountId for the new account to be created: " + newAccountId);
			if (newAccountId <= -1) {
				throw new AccountException("Failed to generate accountId");
			}
			
			// Validate account name
			validateDuplicateAccountName(conn, Integer.parseInt(operatorId), 0, accountName, distributorId);
			
			// Create new account
			createNewAccountInfo(newAccountId, accountName, accountType, region, primaryContact, requestedDate, requestedUser, webAddress,
					conn);
			LOG.info("Created account information for the new account: " + newAccountId);

			// Add Account Additional Info
			createAccountAdditionalInfo(newAccountId, operatorId, status, approvalStatus, invoiceAddress, carrierAccountId, billableStatus, netSuiteId, specialInstructions, tags, 
					includedAlertProfiles, verticalId, maxPortalAccounts, maxScheduledReports, maxReportHistory, serviceImpairmentEmail, myAlertsEmail, scheduledReportsEmail, 
					supportContactEmails, requestedDate, requestedUser, parentAccountId, agentId, acctType, enableCreateWholesale, wholesaleAccountId, conn, creditLimit);
			LOG.info("Created account additional information for the new account: " + newAccountId);

			// Add Account Contacts
			createAccountContacts(newAccountId, primaryContact, additionalContacts, requestedDate, requestedUser, conn);
			LOG.info("Added account contacts information for the new account: " + newAccountId);

			// Attach account to parent account
			if (StringUtils.isNotEmpty(parentAccountId) && NumberUtils.isNumber(parentAccountId)) {
				createParentChildRelationship(newAccountId, Long.parseLong(parentAccountId), parentLabel, billableStatus, requestedDate, requestedUser, conn);
			}

			// Add account product subscriptions
			createAccountProductSubscriptions(newAccountId, productIds, conn);

			// Create Master API Key
			String masterApiKey = createMasterApiKey(newAccountId, requestedUser, conn);
			
			// Set up feature codes for connectivity and aercloud
			setupAccountFeatureCodes(newAccountId, true, oaUtils.hasAerCloud(productIds), conn);
			
			//create overage charges
			if(accountOverageBucket != null) {
				createAccountOverageBucket(newAccountId, accountOverageBucket, conn);
			}
            
            Distributor distributor= null;
          //Save account as distributor
			if (null != distRequest
					&& null != acctType
					&& ("wholesale".equalsIgnoreCase(acctType) || "distributor"
							.equalsIgnoreCase(acctType))) {
				distRequest.setAccountId(newAccountId);
				distRequest.setDistributorName(accountName);
				//validate unique access link
				validateDuplicateAccessLink(conn, Integer.parseInt(operatorId), 0, distRequest.getAccessLink());
				distributor = createNewDistributor(Long.parseLong(operatorId),distRequest, requestedUser, conn);
				/*if (distributorId == null || distributorId.trim().isEmpty())
					distributorId = String.valueOf(distributor.getDistributorId());*/
			}
			if (distributorId != null && !distributorId.trim().isEmpty()) {
                setDistributorId(distributorId, newAccountId, conn);
            }
			
			//AERPORT-6795/AERPORT-6835 : Accounts which has ATT product enabled should be defaultly mapped with contract 1 (Aeris). But if the account is a subaccount, 
			//then it should have a contract id of the parent account 
			if(productIds.contains(Constants.ATT_PRODUCT_DUAL_MODE_A_LH)){
				if (StringUtils.isNotEmpty(parentAccountId) && NumberUtils.isNumber(parentAccountId)){
					Contract contract = contractEnum.getContract(Long.parseLong(parentAccountId));
					LOG.info("Account is a subaccount, so inserting parent Account's contract id ...");
					createAccountContract(newAccountId, contract.getContractId(), requestedDate, requestedUser, conn);
				}else{
					LOG.info("Inserting default contract Id (1)...");
					createAccountContract(newAccountId, 1, requestedDate, requestedUser, conn);
				}
				
			}
			
			
			
			// Commit the changes
			conn.commit();
			LOG.info("Committed createNewAccount changes to db");

			// Return the account created to the client
			account = new Account();
			account.setAccountId(newAccountId);
			account.setAccountName(accountName);
			account.setType(accountType);
			account.setPrimaryContact(primaryContact);
			account.setAdditionalContacts(additionalContacts);
			account.setBillingDetails(billingDetails);
			account.setIncludedAlertProfiles(includedAlertProfiles);
			account.setInvoiceAddress(invoiceAddress);
			account.setProductIds(productIds);
			account.setSpecialInstructions(specialInstructions);
			account.setTags(tags);
			account.setBillableStatus(billableStatus);
			account.setCarrierAccountId(carrierAccountId);
			account.setNetSuiteId(netSuiteId);
			account.setOperatorId(Integer.parseInt(operatorId));
			account.setParentAccountId(parentAccountId);
			account.setStatus(status);
			account.setApprovalStatus(approvalStatus);
			account.setApiKey(masterApiKey);
			account.setAgentId(agentId);
			account.setAccountOverageBucket(accountOverageBucket);
            account.setLastModifiedDate(new java.sql.Date(requestedDate.getTime()));
            account.setAccountType(acctType);
            account.setWholesaleAccountId(wholesaleAccountId);
            
            if (distributorId != null && !distributorId.trim().isEmpty()) {
                account.setDistributorId(distributorId);
            }
            if (null != distRequest
					&& null != acctType
					&& ("wholesale".equalsIgnoreCase(acctType) || "distributor"
							.equalsIgnoreCase(acctType))) {
            	account.setDistributor(distributor);
			}
			// Put in Cache
			cache.put(newAccountId, account);
			
			//AERPORT-6795 :If account has ATT product enabled, we need to add ACCOUNT_CONTRACT entry in contractCache
			if(productIds.contains(Constants.ATT_PRODUCT_DUAL_MODE_A_LH)){
				Contract contract = getAccountContract(newAccountId, conn);
				contractEnum.putContractInCache(newAccountId, contract);
				contractInsertedInCache = true;
			}
			
			// Update Child Cache if the parent account is specified.
			if (NumberUtils.isNumber(parentAccountId)) {
				long parentAcctId = Long.parseLong(parentAccountId);
				
				if(parentAcctId > 0)
				{
					updateChildCache(parentAcctId, account);
				}
			}
			
			LOG.info("Returning account created...");
		} catch (SQLException e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, false, newAccountId, conn, e);
			
			throw new AccountException("Create new account failed due to db error", e);
		} catch (DuplicateAccountException e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, false, newAccountId, conn, e);
			
			throw e;
		} catch (SubAccountException e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, false, newAccountId, conn, e);
			
			throw e;
		} catch (ApiKeyException e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, false, newAccountId, conn, e);
						
			throw new AccountException("Create new account failed due to api key creation error", e);
		} catch (Exception e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, false, newAccountId, conn, e);
			
			throw new AccountException("Create new account failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn);
		}

		LOG.info("Returned Account to the caller: " + account);

		return account;
	}

	private void validateDuplicateAccountName(Connection conn, int operatorId, long ignoreAccountId, String accountName, String distributorId) throws SQLException, DuplicateAccountException {
		boolean duplicateAccountName = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		      try {
            LOG.debug("validateDuplicateAccountName: checking if the account name in aersys_accounts table is already in use");
            if (distributorId == null) {
                // Create SQL Statement
                ps = conn.prepareStatement(SQL_GET_ACCOUNT_NAME_COUNT);
                // Set Parameters
                ps.setInt(1, operatorId);
                ps.setString(2, accountName);
                ps.setLong(3, ignoreAccountId);
            } else {
                // Create SQL Statement
                ps = conn.prepareStatement(SQL_GET_ACCOUNT_NAME_COUNT_DISTRIBUTOR);
                // Set Parameters
                ps.setInt(1, operatorId);
                ps.setString(2, accountName);
                ps.setLong(3, ignoreAccountId);
                ps.setLong(4, Long.parseLong(distributorId));
            }
            // Run Query
            rs = ps.executeQuery();

            // Get the count for the duplicate account name in the table
            if (rs.next()) {
                duplicateAccountName = (rs.getInt("cnt") > 0);
                LOG.info("Account Name already used: " + duplicateAccountName);
            }

            if (duplicateAccountName) {
                LOG.error("Error Creating account with a duplicate account name: " + accountName);
                throw new DuplicateAccountException("Account Name already in use");
            }
            
        } catch (SQLException e) {
            LOG.error("Exception during validateDuplicateAccountName", e);
            throw e;
        } finally {
            DBUtils.cleanup(null, ps, rs);
        }
	}
	
	private void validateDuplicateAccessLink(Connection conn, int operatorId, long ignoreAccountId, String accessLink) throws SQLException, DuplicateAccountException {
		boolean duplicateAccessLink = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		      try {
            LOG.debug("validateDuplicateAccessLink: checking if the accessLink in distributor table is already in use"); 
                // Create SQL Statement
                ps = conn.prepareStatement(SQL_GET_ACCESS_LINK_COUNT);
                // Set Parameters
                ps.setInt(1, operatorId);
                ps.setString(2, accessLink);
                ps.setLong(3, ignoreAccountId); 
            // Run Query
            rs = ps.executeQuery();

            // Get the count for the duplicate accessLink in the table
            if (rs.next()) {
                duplicateAccessLink = (rs.getInt("cnt") > 0);
                LOG.info("Access Link already used: " + duplicateAccessLink);
            }

            if (duplicateAccessLink) {
                LOG.error("Error Creating account with a duplicate accessLink: " + accessLink);
                throw new DuplicateAccountException("Access Link already in use");
            }
            
        } catch (SQLException e) {
            LOG.error("Exception during validateDuplicateAccessLink", e);
            throw e;
        } finally {
            DBUtils.cleanup(null, ps, rs);
        }
	}
	
	
	private void validateServiceProfileMapping( String operatorId, String accountId, List<String> productIds) throws AccountMappingException {
		boolean productAlreadyMapped = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			List<ServiceProfile> accountSProfiles = serviceProfileDAO.getAllServiceProfiles( operatorId, accountId, false);
            for (ServiceProfile serviceProfile : accountSProfiles) {                
                if(!productIds.contains("" + serviceProfile.getProductId())){
					productAlreadyMapped = true ;
					break ;
				}
			}
			if(productAlreadyMapped) {
				LOG.error("Cannot remove products already assigned to service profiles " + productIds);
				throw new AccountMappingException("Cannot remove products already assigned to service profiles ");
			}
			
		} catch (AccountMappingException e) {
			LOG.error("Exception ser ", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
	}
	
	
	
	
	/**
	 * Generates the 
	 * 
	 * @param conn
	 * @param operatorId
	 * @return
	 * @throws SQLException
	 */
	public long generateNewAccountId(Connection conn, int operatorId) throws SQLException {
		long newAccountId = -1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			LOG.debug("createNewAccount: fetching the max account id from sequence tables for operators");

			// Set Parameters
			if(operatorId == OPERATOR_AERIS) {
				ps = conn.prepareStatement("SELECT aeris_acc_seq.nextval FROM dual");
			} else if(operatorId == OPERATOR_SPRINT) {
				ps = conn.prepareStatement("SELECT sprint_acc_seq.nextval FROM dual");
			} else {
				ps = conn.prepareStatement("SELECT cisco_acc_seq.nextval FROM dual");
			} 
			// Run Query
			rs = ps.executeQuery();
			// Get the last account_id in the table
			if (rs.next()) {
				newAccountId = rs.getLong(1);
			}
			LOG.info("Account Id generated: " + newAccountId);
		} catch (SQLException e) {
			LOG.error("Exception during createNewAccount - fetchMaxAccountId", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return newAccountId;
	}

	private long createNewAccountInfo(long newAccountId, String accountName, AccountType accountType, Region region,
			Contact primaryContact, Date requestedDate, String requestedUser, String webAddress, Connection conn) throws SQLException {
		LOG.info("Creating New AccountInfo");

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in aersys_accounts table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_ACCOUNT_INFO);

			LOG.debug("Setting parameters");

			ps.setInt(1, accountType.getValue());
			ps.setLong(2, newAccountId);
			ps.setString(3, accountName);
			ps.setInt(4, region.getValue());
			ps.setDate(5, new java.sql.Date(requestedDate.getTime()));
			ps.setInt(6, NOT_USED);

			if (StringUtils.isNotEmpty(webAddress)) {
				ps.setString(7, webAddress);
			} else {
				ps.setNull(7, Types.VARCHAR);
			}

			LOG.info("inserting account info in the db");

			ps.executeUpdate();

			LOG.info("Created Account with account id = " + newAccountId + " in database");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating account info in table aersys_accounts", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return newAccountId;
	}

	private void createAccountAdditionalInfo(long newAccountId, String operatorId, AccountStatus status, ApprovalStatus approvalStatus, InvoiceAddress invoiceAddress, 
			String carrierAccountId, BillableStatus billableStatus, String netSuiteId, String specialInstructions, String tags, int includedAlertProfiles, 
			String verticalId, int maxPortalAccounts, int maxScheduledReports, int maxReportHistory, String[] serviceImpairmentEmail, String[] myAlertsEmail, 
			String[] scheduledReportsEmail,	String[] supportContactEmails, Date requestedDate, String requestedUser, String parentAccountId, String agentId, 
			String accountType, boolean enableCreateWholesale, String wholesaleAccountId, Connection conn, long creditLimit) 
					throws SQLException {
		LOG.info("Creating New Account Additional Info");

		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		ResultSet rs = null;
		try {
			String invoiceAddr = fetchInvoiceAddress(invoiceAddress);
			int billMRCInAdvance = 0;
			
			LOG.debug("Creating Prepared Statement to insert the data in ws_accts_additional_info table");
			
			try{
				//if it is a retail account of wholeseller, then BILL_MRC_IN_ADVANCE value of the wholesale account should be considered
				// otherwise BILL_MRC_IN_ADVANCE of the operator should be considered.
				if(wholesaleAccountId != null && !wholesaleAccountId.trim().isEmpty()){
					ps1 = conn.prepareStatement("select BILL_MRC_IN_ADVANCE  from ws_accts_additional_info where account_id = ?");
					ps1.setInt(1, Integer.parseInt(wholesaleAccountId));
				}else{
					ps1 = conn.prepareStatement("select BILL_MRC_IN_ADVANCE  from ws_operator where operator_id = ?");
					ps1.setInt(1, Integer.parseInt(operatorId));
				}
				rs = ps1.executeQuery();
				if(rs.next()){
					billMRCInAdvance = rs.getInt("BILL_MRC_IN_ADVANCE");
				}
			}catch (SQLException sqle) {
				LOG.error("Exception during getting BILL_MRC_IN_ADVANCE :", sqle);
				throw sqle;
			} finally {
				DBUtils.cleanup(null, ps1,rs);
			}

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_ACCT_ADDITIONAL_INFO);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(1, newAccountId);// account_Id
			ps.setInt(2, status.getValue()); // account_status
			ps.setString(3, invoiceAddr);// invoice_address
			ps.setString(4, specialInstructions);// special_instruction
			ps.setInt(5, includedAlertProfiles);// included_alert_profiles
			ps.setString(6, tags);// tags
			ps.setInt(7, approvalStatus.getValue());// approval_status
			ps.setInt(8, billableStatus.getValue());// billable_status
			ps.setInt(9, Integer.parseInt(operatorId));// operator_id
			ps.setString(10, netSuiteId);// netsuite_id

			if (NumberUtils.isNumber(parentAccountId)) {
				ps.setLong(11, Long.valueOf(parentAccountId));// parent_account_id
			} else {
				ps.setNull(11, Types.BIGINT);
			}

			if (NumberUtils.isNumber(carrierAccountId)) {
				ps.setLong(12, Long.valueOf(carrierAccountId));// carrier_account_id
			} else {
				ps.setNull(12, Types.BIGINT);
			}

			ps.setInt(13, Integer.parseInt(verticalId));// vertical_id
			ps.setInt(14, maxPortalAccounts);// max_portal_accounts
			ps.setInt(15, maxScheduledReports);// max_sheduled_reports
			ps.setInt(16, maxReportHistory);// max_report_history
			ps.setString(17, StringUtils.join(serviceImpairmentEmail, EMAIL_SEPERATOR));// service_impairment_email
			ps.setString(18, StringUtils.join(myAlertsEmail, EMAIL_SEPERATOR));// my_alerts_email
			ps.setString(19, StringUtils.join(scheduledReportsEmail, EMAIL_SEPERATOR));// scheduled_report_recipients
            ps.setString(20, requestedUser);// created_by
			ps.setDate(21, new java.sql.Date(requestedDate.getTime()));// date_created
			ps.setString(22, requestedUser);// last_modified_by
			ps.setDate(23, new java.sql.Date(requestedDate.getTime()));// last_modified_date
            ps.setString(24, StringUtils.join(supportContactEmails, EMAIL_SEPERATOR)); // support_contact_emails
            ps.setString(25, agentId);
            ps.setString(26, accountType);
            ps.setBoolean(27, enableCreateWholesale);
            if (NumberUtils.isNumber(wholesaleAccountId)) {
				ps.setLong(28, Long.valueOf(wholesaleAccountId));// wholesale_account_id
			} else {
				ps.setNull(28, Types.BIGINT);
			}
            if(creditLimit == 0){
            	ps.setNull(29, Types.NUMERIC);
            }else{
            	ps.setLong(29, creditLimit);
            }
            ps.setInt(30, billMRCInAdvance);
            
			LOG.debug("Inserting account additional info the db");

			// Insert additional info into the db
			ps.executeUpdate();

			LOG.info("Created Account Additional Info for account id = " + newAccountId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating account additional info in table ws_accts_additional_info", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	
	private void createAccountOverageBucket(long accountId, List<AccountOverageBucket> accountOverageBucketList, Connection conn) 
					throws SQLException {
		LOG.info("Creating New Account Overage Bucket");
		
		PreparedStatement ps = null;
		
		String insertUpdateOverageBucket = "INSERT INTO aerisgen.ACCOUNT_OVERAGE_BUCKET "
				+ "(ACCOUNT_ID, START_DATE, END_DATE, BUCKET_SIZE, BUCKET_SIZE_TYPE, BUCKET_PRICE, NO_ACCESS_FEE_DEVICE_COUNT, "
				+ "AUTO_ADD, CREATED_DATE, LAST_MODIFIED_DATE, CREATED_BY, LAST_MODIFIED_BY,  REP_TIMESTAMP) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

		//delete the associated account overage buckets first
		deleteAccountOverageBuckets(accountId, conn);
		
		try {
			if(accountOverageBucketList != null && accountOverageBucketList.size() > 0) {
				LOG.debug("Creating Prepared Statement to insert the data in aerisgen.ACCOUNT_OVERAGE_BUCKET table");
				// Create SQL Statement
				ps = conn.prepareStatement(insertUpdateOverageBucket);
				for (AccountOverageBucket accountOverageBucket : accountOverageBucketList) {
					
					LOG.debug("Setting aerisgen.ACCOUNT_OVERAGE_BUCKET Parameters");
					int index = 1;
					// Set Parameters
					ps.setLong(index++, accountId);
					ps.setDate(index++, accountOverageBucket.getStartDate());
					ps.setDate(index++,accountOverageBucket.getEndDate());
					//set the bucket in MB here if the unit is in GB
					double overageSize = accountOverageBucket.getBucketSize();
					if(accountOverageBucket.getBucketSizeType() != null && accountOverageBucket.getBucketSizeType().equalsIgnoreCase("GB")){
						overageSize = overageSize * 1024 ;
						BigDecimal bDec = new BigDecimal(String.valueOf(overageSize)).setScale(2, BigDecimal.ROUND_HALF_UP);
						overageSize = bDec.doubleValue();
					}
					ps.setDouble(index++, overageSize);
					ps.setString(index++, accountOverageBucket.getBucketSizeType());
					ps.setDouble(index++, accountOverageBucket.getBucketPrice());
					ps.setInt(index++, accountOverageBucket.getNoAccessFeeDeviceCount());
					ps.setInt(index++, accountOverageBucket.getAutoAdd());
					if(accountOverageBucket.getCreatedDate() == null) {
						accountOverageBucket.setCreatedDate(new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					if(accountOverageBucket.getUpdatedDate() == null) {
						accountOverageBucket.setUpdatedDate(new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					ps.setDate(index++, accountOverageBucket.getCreatedDate()!= null ? accountOverageBucket.getCreatedDate() : new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					ps.setDate(index++, accountOverageBucket.getUpdatedDate() != null ? accountOverageBucket.getUpdatedDate() : new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()) );
					//If createdBy is not present use value from updatedBy and vice-versa
					if(accountOverageBucket.getCreatedBy() == null) {
						accountOverageBucket.setCreatedBy(accountOverageBucket.getUpdatedBy());
					}
					if(accountOverageBucket.getUpdatedBy() == null) {
						accountOverageBucket.setUpdatedBy(accountOverageBucket.getCreatedBy());
					}
					ps.setString(index ++ , accountOverageBucket.getCreatedBy());
					ps.setString(index ++ , accountOverageBucket.getUpdatedBy());
                                        String repTimestamp = accountOverageBucket.getRepTimeStamp();
					if(repTimestamp != null && !repTimestamp.trim().isEmpty()) {
						ps.setTimestamp(index++, ApplicationUtils.convertStringToTS(accountOverageBucket.getRepTimeStamp()));
					}else {
						ps.setTimestamp(index++, new Timestamp(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					LOG.debug("Inserting aerisgen.ACCOUNT_OVERAGE_BUCKET into the db");
					
					ps.addBatch();
					
				}
				ps.executeBatch();
				LOG.info("Created aerisgen.ACCOUNT_OVERAGE_BUCKET for account id = " + accountId + " Successfully");
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during creating aerisgen.ACCOUNT_OVERAGE_BUCKET", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	
	/**
	 * Deletes the associated rate plan overage buckets 
	 * 
	 * @param ratePlanId
	 * @param accountId
	 * @param conn
	 * @throws SQLException
	 */
	private void deleteAccountOverageBuckets(long accountId, Connection conn ) throws SQLException {
		String deleteAccountOverageBucket = "DELETE FROM aerisgen.ACCOUNT_OVERAGE_BUCKET WHERE account_id = " + accountId ;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(deleteAccountOverageBucket);
			ps.executeUpdate();
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting aerisgen.ACCOUNT_OVERAGE_BUCKET for accountId=" + accountId , sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}


	private String fetchInvoiceAddress(InvoiceAddress invoiceAddress) {
		LOG.info("Fetching invoice address");

		// If full address is specified
		String invoiceAddr = null;

		if (invoiceAddress != null) {
			invoiceAddr = invoiceAddress.getFullAddress();

			if (StringUtils.isEmpty(invoiceAddr)) {
				// Otherwise, build address from individual fields
				StringBuffer sb = new StringBuffer(invoiceAddress.getAddressFirstLine());

				sb.append(invoiceAddress.getAddressSecondLine()).append(invoiceAddress.getState()).append(invoiceAddress.getCountry())
						.append(invoiceAddress.getZipCode());

				invoiceAddr = sb.toString();
			}
		}

		LOG.info("Invoice address: " + invoiceAddr);

		return invoiceAddr;
	}

	private void createAccountContacts(long newAccountId, Contact primaryContact, List<Contact> additionalContacts, Date requestedDate,
			String requestedUser, Connection conn) throws SQLException {
		LOG.info("Creating New Account Contacts");

		PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in ws_account_contacts table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_CONTACT_INFO);

			// Add Primary Contact Information
			if (primaryContact != null) {
				LOG.info("Creating Primary Contact Info");
				addContactToBatch(newAccountId, primaryContact, requestedDate, requestedUser, ps);
			}

			// Add Additional Contacts Information
			if (additionalContacts != null) {
				LOG.info("Creating Additional Contacts Information");

				for (Contact contact : additionalContacts) {
					if (contact.isValid()) {
						addContactToBatch(newAccountId, contact, requestedDate, requestedUser, ps);
					}
				}
			}

			LOG.debug("Inserting contacts information into the db");

			// Execute the sql batch now
			ps.executeBatch();

			LOG.info("Created Contacts for account id = " + newAccountId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating account contacts info in table ws_account_contacts", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void addContactToBatch(long newAccountId, Contact contact, Date requestedDate, String requestedUser, PreparedStatement ps)
			throws SQLException {
		LOG.debug("Setting Parameters in the Prepared Statement");

		ps.clearParameters();

		ps.setLong(1, newAccountId);
		ps.setInt(2, contact.getRole().getValue());
		ps.setString(3, StringUtils.join(new Object[] { contact.getFirstName(), contact.getLastName() }, SEPARATOR));
		ps.setString(4, contact.getRole().name());
		ps.setString(5, contact.getJobTitle());
		ps.setString(6, contact.getEmail());
		ps.setString(7, contact.getPhoneNumber());
		ps.setString(8, requestedUser);
		ps.setDate(9, new java.sql.Date(requestedDate.getTime()));
		ps.setString(10, requestedUser);
		ps.setDate(11, new java.sql.Date(requestedDate.getTime()));

		LOG.debug("Prepared Statement added to the batch");

		ps.addBatch();
	}

	private void updateParentChildRelationship(long accountId, long parentAccountId, String parentLabel,
			BillableStatus billableStatus, Date requestedDate, String requestedUser, Connection conn) throws SQLException, SubAccountException {
		LOG.info("Checking parent billable status");

		BillableStatus parentBillableStatus = getParentBillableStatus(parentAccountId);
		
		LOG.info("Parent billable status: "+parentBillableStatus);
		LOG.info("Child billable status: "+billableStatus); 
		
		// Check if there is a valid billable status in case of MOBO/SUB-ACCOUNT
		if(isValidBillableStatus(billableStatus, parentBillableStatus))
		{
			// MOBO or SUB-ACCOUNT group
			String group = billableStatus == BillableStatus.BILLABLE ? MOBO : SUB_ACCOUNT;
			
			long parentSubAccountId = getSubAccountId(parentAccountId, group, conn);
			LOG.info("Parent Sub Account Heirarchy ID: "+parentSubAccountId);
			
			long childSubAccountId = getSubAccountId(accountId, group, conn);
			LOG.info("Child Sub Account Heirarchy ID: "+parentSubAccountId);
			
			if(parentSubAccountId != -1)
			{
				if(childSubAccountId != -1)
				{
					setParentSubAccountId(childSubAccountId, parentSubAccountId, requestedDate, requestedUser, conn);
				}
				else
				{
					LOG.info("Creating sub account entry for the child account id: "+accountId);
					createSubAccount(accountId, parentSubAccountId, parentLabel, group, requestedDate, requestedUser, conn);		
				}		
			}
			else
			{
				LOG.info("Creating master and sub account entry for the parent account id: "+parentAccountId+" and child account id: "+accountId);
				createMasterAndSubAccount(parentAccountId, accountId, parentLabel, group, requestedDate, requestedUser, conn);		
			}
		}
		else
		{
			LOG.error("MOBO/Sub Account error: The sub account can be billable only when master account is non-billable, Master Account Billable Status: "+parentBillableStatus+", Sub Account Billable Status: "+billableStatus);
			throw new SubAccountException("MOBO/Sub Account error: The sub account can be billable only when master account is non-billable, Master Account Billable Status: "+parentBillableStatus+", Sub Account Billable Status: "+billableStatus);
		}
	}

	private boolean isValidBillableStatus(BillableStatus billableStatus, BillableStatus parentBillableStatus) {
		// FIXME: Check if we need to validate the parent and child billable status to create a MOBO/SUB-ACCOUNT 
		// Check child should be billable only when parent is not billable
		// return parentBillableStatus != null && billableStatus != null && parentBillableStatus != billableStatus;
		return true;
	}

	private void setParentSubAccountId(long childSubAccountId, long parentSubAccountId, Date requestedDate, String requestedUser, Connection conn) throws SubAccountException, SQLException {
		PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to update the account hierarchy in sub_accounts2 table");

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_UPDATE_ACCOUNT_HEIRARCHY);

			LOG.debug("Setting Parameters");

			ps.setLong(1, parentSubAccountId);
			ps.setString(2, requestedUser);
			ps.setDate(3, new java.sql.Date(requestedDate.getTime()));
			ps.setLong(4, childSubAccountId);

			LOG.debug("Updating the account info in the db");

			int count = ps.executeUpdate();

			if (count <= 0) {
				LOG.error("setParentSubAccountId: update parent sub account failed for child sub account id: " + childSubAccountId);
				throw new SubAccountException("setParentSubAccountId: update parent sub account failed for child sub account id: " + childSubAccountId);
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during setting ParentSubAccountId in table sub_accounts2", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void createParentChildRelationship(long newAccountId, long parentAccountId, String parentLabel,
			BillableStatus billableStatus, Date requestedDate, String requestedUser, Connection conn) throws SQLException, SubAccountException {
		LOG.info("Checking parent billable status");

		BillableStatus parentBillableStatus = getParentBillableStatus(parentAccountId);
		
		LOG.info("Parent billable status: "+parentBillableStatus);
		LOG.info("Child billable status: "+billableStatus); 
		
		// Check child should be billable only when parent is not billable
		// MOBO or SUB-ACCOUNT group
		String group = (billableStatus == BillableStatus.BILLABLE ? MOBO : SUB_ACCOUNT);
		
		// Check child should be billable only when parent is not billable
		// MOBO or SUB-ACCOUNT group
		String parentGroup = (parentBillableStatus == BillableStatus.BILLABLE ? MOBO : SUB_ACCOUNT);
		
		long parentSubAccountId = getSubAccountId(parentAccountId, parentGroup, conn);
		LOG.info("Parent Sub Account Heirarchy ID: "+parentSubAccountId);
		
		if(parentSubAccountId != -1)
		{
			LOG.info("Creating sub account entry for the child account id: "+newAccountId);
			createSubAccount(newAccountId, parentSubAccountId, parentLabel, group, requestedDate, requestedUser, conn);				
		}
		else
		{
			LOG.info("Creating master and sub account entry for the parent account id: "+parentAccountId+" and child account id: "+newAccountId);
			createMasterAndSubAccount(parentAccountId, newAccountId, parentLabel, group, requestedDate, requestedUser, conn);		
		}
	}

	private void createMasterAndSubAccount(long parentAccountId, long newAccountId, String parentLabel, String group, Date requestedDate, String requestedUser,
			Connection conn) throws SQLException, SubAccountException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long parentSubAccountId = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to insert the data in account_hierarchy table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_SUB_ACCOUNT, new String[]{"sa_id"});

			LOG.debug("Setting Parameters");

			String userId = requestedUser;
			
			LOG.info("Creating master account");

			// Set Parameters
			ps.setLong(1, parentAccountId);// Account id
			ps.setNull(2, Types.INTEGER); // Account status
			ps.setNull(3, Types.VARCHAR);
			ps.setString(4, group);
			ps.setString(5, userId);
			ps.setString(6, userId);
			ps.setDate(7, new java.sql.Date(requestedDate.getTime()));
			
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			
			if(rs.next())
			{
				parentSubAccountId = rs.getLong(1);
				LOG.info("Created parentSubAccountId: "+parentSubAccountId);
			}
			
			if(parentSubAccountId == 0)
			{
				LOG.error("MOBO/Sub Account error: Unable to create master account for account: "+newAccountId);
				throw new SubAccountException("MOBO/Sub Account error: Unable to create master account for account: "+newAccountId);
			}
			
			
			LOG.info("Creating child account");

			// Set Parameters
			ps.clearParameters();
			ps.setLong(1, newAccountId);// Account id
			ps.setLong(2, parentSubAccountId); // Account status

			// If Parent Label is specified
			if (StringUtils.isNotEmpty(parentLabel)) {
				ps.setString(3, parentLabel);
			}
			// Otherwise set as NULL
			else {
				ps.setNull(3, Types.VARCHAR);
			}

			ps.setString(4, group);
			ps.setString(5, userId);
			ps.setString(6, userId);
			ps.setDate(7, new java.sql.Date(requestedDate.getTime()));
			
			LOG.debug("Inserting sub account information for account" + newAccountId + " with parent as : " + parentSubAccountId + " relationship into the db");

			// Execute the sql
			ps.executeUpdate();

			LOG.info("Created Parent-Child relationship for account id: " + newAccountId + " and parent sub account id: " + parentSubAccountId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating sub account info in table sub_accounts2", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
	}

	private BillableStatus getParentBillableStatus(long parentAccountId) {
		BillableStatus parentBillable = BillableStatus.NON_BILLABLE;
		
		Account parentAccount = cache.get(parentAccountId);
		
		if(parentAccount != null)
		{
			parentBillable = parentAccount.getBillableStatus();
		}
		
		return parentBillable;
	}

	private long getSubAccountId(long parentAccountId, String group, Connection conn) throws SQLException {
		LOG.debug("Checking if parent is a top node");

		long parent = -1;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Creating Prepared Statement to fetch the data from subb_accounts2 table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_SUB_ACCOUNT);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(1, parentAccountId);

			LOG.debug("Fetching parent sub account id from db");

			// Run Query
			rs = ps.executeQuery();

			// Get the last account_id in the table
			if (rs.next()) {
				parent = rs.getLong("sa_id");
			}

			LOG.info("Parent: " + parentAccountId + (parent != -1 ? " is available in the heirarchy" : "is not available in the heirarchy"));
		} catch (SQLException e) {
			LOG.error("Exception during getParentSubAccountId", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return parent;
	}

	private void createSubAccount(long newAccountId, long parentSubAccountId, String parentLabel, String group, Date requestedDate, 
			String requestedUser, Connection conn) throws SQLException {
		PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in account_hierarchy table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_SUB_ACCOUNT);

			LOG.debug("Setting Parameters");

			String userId = requestedUser;

			// Set Parameters
			ps.setLong(1, newAccountId);// Account id
			ps.setLong(2, parentSubAccountId); // Account status

			// If Parent Label is specified
			if (StringUtils.isNotEmpty(parentLabel)) {
				ps.setString(3, parentLabel);
			}
			// Otherwise set as NULL
			else {
				ps.setNull(3, Types.VARCHAR);
			}

			ps.setString(4, group);
			ps.setString(5, userId);
			ps.setString(6, userId);
			ps.setDate(7, new java.sql.Date(requestedDate.getTime()));

			LOG.debug("Inserting sub account information for account" + newAccountId + " with parent as : " + parentSubAccountId + " relationship into the db");

			// Execute the sql
			ps.executeUpdate();

			LOG.info("Created Parent-Child relationship for account id: " + newAccountId + " and parent sub account id: " + parentSubAccountId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating sub account info in table sub_accounts2", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void createAccountProductSubscriptions(long newAccountId, List<String> productIds, Connection conn) throws SQLException {
		PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in account_product_subscription table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_ACCOUNT_PRODUCT_SUBSCRIPTION);

			LOG.debug("Setting Parameters");

			for (String productId : productIds) {
				ps.clearParameters();

				// Set Parameters
				ps.setLong(1, newAccountId); // Account status
				ps.setInt(2, Integer.parseInt(productId));
				ps.setString(3, SubscriptionStatus.ENABLED.getValue());

				ps.addBatch();
			}

			LOG.debug("Inserting product subscriptions: (" + StringUtils.join(productIds, ",") + ") for account id: " + newAccountId
					+ " into the db");

			// Execute the sql batch
			ps.executeBatch();

			LOG.info("Registered account id: " + newAccountId + " with product ids: (" + StringUtils.join(productIds.toArray(), ",")
					+ ") Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating account contacts info in table ws_account_contacts", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	@Override
	public Account updateAccount(String operatorId, String accountId, String accountName, Region region, AccountType accountType,
			AccountStatus status, ApprovalStatus approvalStatus, InvoiceAddress invoiceAddress, List<String> productIds, String parentAccountId, String parentLabel,
			String carrierAccountId, Contact primaryContact, List<Contact> additionalContacts, String webAddress,
			BillableStatus billableStatus, BillingDetails billingDetails, String netSuiteId, String specialInstructions, String tags,
			int includedAlertProfiles, String verticalId, int maxPortalAccounts, int maxScheduledReports, int maxReportHistory, String[] serviceImpairmentEmail, 
			String[] myAlertsEmail,	String[] scheduledReportsEmail, String[] supportContactEmails, Date requestedDate, String requestedUser,
			List<AccountOverageBucket> accountOverageBucket, String agentId, String distributorId, String acctType, boolean enableCreateWholesale, String wholesaleAccountId,
			Distributor distRequest, long creditLimit) 
			throws AccountNotFoundException, AccountException, SubAccountException, DuplicateAccountException, AccountMappingException {
		Account account = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean accountExists = false;
		long acctId = Long.parseLong(accountId);
		String masterApiKey = null;

		LOG.info("updateAccount: processing update account...");

		try {
			LOG.debug("updateAccount: Fetched db connection"); 

			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch the data in ws_accounts_additional_info table");

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_ACCOUNT_COUNT);

			LOG.debug("Setting Parameters");

			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, Long.parseLong(accountId));

			LOG.info("updateAccount: validating if account exists with account id: " + accountId);

			rs = ps.executeQuery();

			if (rs.next()) {
				accountExists = (rs.getInt("account_cnt") > 0);// Set if account
																// exists
			}

			if (!accountExists) {
				LOG.error("Account does not exist for account id: " + accountId);
				throw new AccountNotFoundException("Account does not exist for account id: " + accountId);
			}
		} catch (SQLException e) {
			LOG.error("Exception during updateAccount - validateAccount", e);
			throw new AccountException("Exception during updateAccount", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		boolean contractInsertedInCache = false;
        boolean contractRemovedFromCache = false;
        
		try {
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			
			// Set Auto Commit as false
			conn.setAutoCommit(false);
			
			// Fetch master api key
			ApiKey apiKey = accountApplicationDAO.getMasterApiKey(Long.parseLong(accountId));
			
			if(apiKey != null)
			{
				masterApiKey = apiKey.getApiKey();
			}

			// Validate account name
			validateDuplicateAccountName(conn, Integer.parseInt(operatorId), Long.parseLong(accountId), accountName, distributorId);			
			
			//validate product association with service profile a
			validateServiceProfileMapping(operatorId, accountId, productIds);
						
			// Update Basic Account Info
			updateAccountInfo(acctId, accountName, region, accountType, webAddress, requestedDate, conn);
			LOG.info("Updated account information for the account id: " + acctId);

			// Update Account Additional Info
			updateAccountAdditionalInfo(acctId, operatorId, status, approvalStatus, invoiceAddress, carrierAccountId, billableStatus, netSuiteId,
					specialInstructions, tags, includedAlertProfiles, verticalId, maxPortalAccounts, maxScheduledReports, maxReportHistory, 
					serviceImpairmentEmail, myAlertsEmail, scheduledReportsEmail, supportContactEmails, requestedDate, requestedUser, parentAccountId,
					agentId, acctType,enableCreateWholesale, wholesaleAccountId, conn , creditLimit);
			LOG.info("Updated account additional information for the account id: " + acctId);
			
			//update overage charges
			createAccountOverageBucket(acctId, accountOverageBucket, conn);

			// Update Contacts, Parent and Products
			updateAccountContactsAndProducts(acctId, productIds, parentAccountId, parentLabel, primaryContact, additionalContacts,
					billableStatus, requestedDate, requestedUser, conn);
			LOG.info("Updated account contacts and products information for the account id: " + acctId);

			// Set up feature codes for connectivity and aercloud
			setupAccountFeatureCodes(acctId, true, oaUtils.hasAerCloud(productIds), conn);
            
            Distributor distributor = null;
            if (null != distRequest
					&& null != acctType
					&& ("wholesale".equalsIgnoreCase(acctType) || "distributor"
							.equalsIgnoreCase(acctType))) {
            	distRequest.setAccountId(acctId);
            	//validate unique access link
    			validateDuplicateAccessLink(conn, Integer.parseInt(operatorId), Long.parseLong(accountId), distRequest.getAccessLink());
            	distributor = updateDistributor(Long.parseLong(operatorId), distRequest.getDistributorId(), distRequest, requestedUser, conn);           	
            }
            if (distributorId != null && !distributorId.trim().isEmpty()) {
                setDistributorId(distributorId, acctId, conn);
            }
            
          //Get account before update from cache..
            Account accBfrUpdate = cache.get(acctId);
            
            //check if the products are modified
            if(!(accBfrUpdate.getProductIds().containsAll(productIds) && productIds.containsAll(accBfrUpdate.getProductIds()))){
            	// if product list is modified and AT&T product is added as a new product, then insert entry in ACCOUNT_CONTRACT 
            	if(productIds.contains(Constants.ATT_PRODUCT_DUAL_MODE_A_LH)){
					// if its a child account, insert contract of the parent account
            		if(parentAccountId != null && !parentAccountId.isEmpty()){
            			Contract contract = contractEnum.getContract(Long.parseLong(parentAccountId));
    					LOG.info("Account is a subaccount, so inserting parent Account's contract id ...");
						if(!contractExistsForAccount(acctId, conn)){
							createAccountContract(acctId, 1, requestedDate, requestedUser, conn);
						}else{
							updateAccountContract(acctId, 1, requestedDate, requestedUser, conn);
						}

            		}else{
            		//	if its a parent account, insert default contract (i.e. 1)
            			LOG.info("Inserting default contract Id (1)...");
						if(!contractExistsForAccount(acctId,conn)){
							createAccountContract(acctId, 1, requestedDate, requestedUser, conn);
						}
					}
            		// add account contract entry in cache
            		Contract contract = getAccountContract(acctId, conn);
    				contractEnum.putContractInCache(acctId, contract);
    				contractInsertedInCache = true;
            	}else{
            	// if product list is modified and AT&T product is removed , then remove entry from ACCOUNT_CONTRACT
            		LOG.info("Removing entry from ACCOUNT_CONTRACT for accountId "+acctId);
            		removeAccountContract(acctId, conn);
            		
            		// remove account contract entry from cache
            		contractEnum.removeContractFromCache(acctId);
            		contractRemovedFromCache = true;
            	}
            }
            
			// Commit the changes
			conn.commit();
			
			// Return the account created to the client
			account = new Account();
			account.setAccountId(acctId);
			account.setAccountName(accountName);
			account.setType(accountType);
			account.setPrimaryContact(primaryContact);
			account.setAdditionalContacts(additionalContacts);
			account.setBillingDetails(billingDetails);
			account.setIncludedAlertProfiles(includedAlertProfiles);
			account.setInvoiceAddress(invoiceAddress);
			account.setProductIds(productIds);
			account.setSpecialInstructions(specialInstructions);
			account.setTags(tags);
			account.setBillableStatus(billableStatus);
			account.setRegion(region);
			account.setCarrierAccountId(carrierAccountId);
			account.setNetSuiteId(netSuiteId);
			account.setOperatorId(Integer.parseInt(operatorId));
			account.setParentAccountId(parentAccountId);
			account.setStatus(status);
			account.setApprovalStatus(approvalStatus);
			account.setApiKey(masterApiKey);
			account.setAccountOverageBucket(accountOverageBucket);
			account.setLastModifiedDate(new java.sql.Date(requestedDate.getTime()));
            account.setAgentId(agentId);
            account.setWholesaleAccountId(wholesaleAccountId);
            account.setAccountType(acctType);
            account.setCreditLimit(creditLimit);
            
            if (distributorId != null && !distributorId.trim().isEmpty()) {
                account.setDistributorId(distributorId);
            }
            if (null != distRequest
					&& null != acctType
					&& ("wholesale".equalsIgnoreCase(acctType) || "distributor"
							.equalsIgnoreCase(acctType))) {
            	account.setDistributor(distributor);
            }
            
			// Put In Cache
			cache.put(acctId, account);
			
			// Update Child Cache if the parent account is specified.
			if (NumberUtils.isNumber(parentAccountId)) {
				long parentAcctId = Long.parseLong(parentAccountId);
				
				if(parentAcctId > 0)
				{
					updateChildCache(parentAcctId, account);
				}
			}
			
			LOG.info("Account updated successfully.");
		} catch (SQLException e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, contractRemovedFromCache, acctId, conn, e);

			throw new AccountException("Update Account failed due to db error: ", e);
		} catch (DuplicateAccountException e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, contractRemovedFromCache, acctId, conn, e);

			throw e;
		}catch (AccountMappingException e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, contractRemovedFromCache, acctId, conn, e);

			throw e;
		} catch (SubAccountException e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, contractRemovedFromCache, acctId, conn, e);

			throw e;
		} catch (Exception e) {
			rollbackAndRevertAccountContractCacheChanges(contractInsertedInCache, contractRemovedFromCache, acctId, conn, e);
			
			throw new AccountException("Create new account failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn, ps);
		}

		return account;
	}

	/**
	 * Checks if contract exists for the passed account or not.
	 * @param accountId
	 * @return
	 */
	private boolean contractExistsForAccount(Long accountId, Connection conn) throws  AccountException{

		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean accountContractExists = false;

		try {
			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_ACCOUNT_CONTRACT_COUNT);

			LOG.debug("Setting Parameters");

			ps.setLong(1, accountId);

			LOG.info("contractExistsForAccount: validating if account contract exists with account id: " + accountId);

			rs = ps.executeQuery();

			if (rs.next()) {
				accountContractExists = (rs.getInt("contractCount") > 0);// Set if account account exists
			}
		}catch (SQLException ex){
			LOG.error("Exception during updateAccount - validateAccount", ex);
			throw new AccountException("Exception during updateAccount", ex);
		}finally{
			DBUtils.cleanup(null, ps, rs);
		}

		return accountContractExists;
	}

	private void updateAccountInfo(long acctId, String accountName, Region region, AccountType accountType, String webAddress,
			Date requestedDate, Connection conn) throws SQLException, AccountNotFoundException {
		PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to update the basic account info in aersys_accounts table");

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_UPDATE_ACCOUNT_INFO);

			LOG.debug("Setting Parameters");

			ps.setInt(1, accountType.getValue());
			ps.setString(2, accountName);
			ps.setInt(3, region.getValue());
			ps.setDate(4, new java.sql.Date(requestedDate.getTime()));
			ps.setInt(5, USED);

			if (StringUtils.isNotEmpty(webAddress)) {
				ps.setString(6, webAddress);
			} else {
				ps.setNull(6, Types.VARCHAR);
			}

			ps.setLong(7, acctId);

			LOG.debug("Updating the account info in the db");

			int count = ps.executeUpdate();

			if (count <= 0) {
				LOG.error("updateAccount: update account failed for account id: " + acctId);
				throw new AccountNotFoundException("Account id: " + acctId + " is not valid");
			}
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void updateBasicAccountStatus(long acctId, AccountStatus status, ApprovalStatus approvalStatus, Date requestedDate, Connection conn) throws SQLException, 
	AccountNotFoundException {
		PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to update the basic account status in aersys_accounts table");

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_UPDATE_ACCOUNT_STATUS);

			LOG.debug("Setting Parameters");
			
			ps.setInt(1, (approvalStatus == ApprovalStatus.APPROVED && status != AccountStatus.SUSPENDED) ? USED : NOT_USED);
			ps.setDate(2, new java.sql.Date(requestedDate.getTime()));
			ps.setLong(3, acctId);

			LOG.debug("Updating the account info in the db");

			int count = ps.executeUpdate();

			if (count <= 0) {
				LOG.error("updateAccount: update account status failed for account id: " + acctId);
				throw new AccountNotFoundException("Account id: " + acctId + " is not valid");
			}
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void updateAccountAdditionalInfo(long accountId, String operatorId, AccountStatus status, ApprovalStatus approvalStatus, InvoiceAddress invoiceAddress, String carrierAccountId, 
			BillableStatus billableStatus, String netSuiteId, String specialInstructions, String tags, int includedAlertProfiles, String verticalId,
			int maxPortalAccounts, int maxScheduledReports, int maxReportHistory, String[] serviceImpairmentEmail, String[] myAlertsEmail, String[] scheduledReportsEmail, 
			String[] supportContactEmails, Date requestedDate, String requestedUser, String parentAccountId, String agentId, String accountType, boolean enableCreateWholesale, String wholesaleAccountId, Connection conn, long creditLimit)
			throws SQLException, AccountNotFoundException {
	PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to update the basic account info in aersys_accounts table");

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_UPDATE_ACCT_ADDITIONAL_INFO);

			LOG.debug("Setting Parameters");

			String invoiceAddr = fetchInvoiceAddress(invoiceAddress);

			// Set Parameters
			ps.setInt(1, status.getValue()); // account_status
			ps.setString(2, invoiceAddr);// invoice_address
			ps.setString(3, specialInstructions);// special_instruction
			ps.setInt(4, includedAlertProfiles);// included_alert_profiles
			ps.setString(5, tags);// tags
			ps.setInt(6, approvalStatus.getValue());// approval_status
			ps.setInt(7, billableStatus.getValue());// billable_status
			ps.setString(8, netSuiteId);// netsuite_id
			ps.setString(9, parentAccountId);// parent_account_id
			ps.setString(10, carrierAccountId);// carrier_account_id
			ps.setInt(11, Integer.parseInt(verticalId));// vertical_id			
			ps.setInt(12, maxPortalAccounts);// max_portal_accounts
			ps.setInt(13, maxScheduledReports);// max_sheduled_reports
			ps.setInt(14, maxReportHistory);// max_report_history
			ps.setString(15, StringUtils.join(serviceImpairmentEmail, EMAIL_SEPERATOR));// service_impairment_email
			ps.setString(16, StringUtils.join(myAlertsEmail, EMAIL_SEPERATOR));// my_alerts_email
			ps.setString(17, StringUtils.join(scheduledReportsEmail, EMAIL_SEPERATOR));// scheduled_report_recipients
			ps.setString(18, requestedUser);// last_modified_by
			ps.setDate(19, new java.sql.Date(requestedDate.getTime()));// last_modified_date
            ps.setString(20, StringUtils.join(supportContactEmails, EMAIL_SEPERATOR));// support_contact_emails
            ps.setString(21, agentId);
            ps.setString(22, accountType);
            ps.setBoolean(23, enableCreateWholesale);
            ps.setString(24, wholesaleAccountId);
            if(creditLimit != 0){
            	ps.setLong(25, creditLimit);
            }else{
            	ps.setNull(25, Types.NUMERIC);
            }
			ps.setInt(26, Integer.parseInt(operatorId));// last_modified_by
			ps.setLong(27, accountId);// last_modified_date
            
			LOG.debug("Updating the account additional info in the db");

			int count = ps.executeUpdate();

			if (count <= 0) {
				LOG.error("updateAccountAdditionalInfo: update account failed for account id: " + accountId);
				throw new AccountNotFoundException("Account id: " + accountId + " is not valid");
			}
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void updateAccountContactsAndProducts(long accountId, List<String> productIds, String parentAccountId, String parentLabel,
			Contact primaryContact, List<Contact> additionalContacts, BillableStatus billableStatus, Date requestedDate, String requestedUser, Connection conn)
			throws SQLException, SubAccountException {
		Statement stmt = conn.createStatement();

		try {
			LOG.debug("Creating Statement Batch to update the contacts, products and the heirarchy information for accountId: " + accountId);

			// Delete Existing Products
			String deleteProductsQuery = SQL_DELETE_ACCOUNT_PRODUCT_SUBSCRIPTION.replace(":account_id", String.valueOf(accountId));

			LOG.debug("Adding query to batch: " + deleteProductsQuery);
			stmt.addBatch(deleteProductsQuery);

			// Delete Existing Account Contacts
			String deleteContactsQuery = SQL_DELETE_ACCOUNT_CONTACT_INFO.replace(":account_id", String.valueOf(accountId));

			LOG.debug("Adding query to batch: " + deleteContactsQuery);
			stmt.addBatch(deleteContactsQuery);

			LOG.info("Deleted old contacts, products and heirarchy information for account: " + accountId);

			stmt.executeBatch();

			LOG.info("Updating the account with new contacts, products and heirarchy information");

			// Update Account Contacts
			createAccountContacts(accountId, primaryContact, additionalContacts, requestedDate, requestedUser, conn);
			LOG.info("Updated account contacts information for the account: " + accountId);

			// Update Relationship by attaching account to new parent account
			if (StringUtils.isNotEmpty(parentAccountId) && NumberUtils.isNumber(parentAccountId)) {
				updateParentChildRelationship(accountId, Long.parseLong(parentAccountId), parentLabel, billableStatus, requestedDate, requestedUser, conn);
				LOG.info("Updated parent-child heirarchy for the account: " + accountId);
			}

			// Update account product subscriptions
			createAccountProductSubscriptions(accountId, productIds, conn);
			LOG.info("Updated product subscriptions for the account: " + accountId);
		} finally {
			DBUtils.cleanup(null, stmt);
		}
	}

	@Override
	public boolean updateAccountStatus(String operatorId, String accountId, AccountStatus status, ApprovalStatus approvalStatus, Date requestedDate, String requestedUser) 
			throws AccountNotFoundException, AccountException, AccountDeactivationException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean accountExists = false;
		boolean success = false;

		LOG.info("updateAccountStatus: processing update account status...");

		try {
			LOG.debug("updateAccountStatus: Fetched db connection");

			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch the data in ws_accounts_additional_info table");

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_ACCOUNT_COUNT);

			LOG.debug("Setting Parameters");

			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, Long.parseLong(accountId));

			LOG.info("updateAccountStatus: validating if account exists with account id: " + accountId);

			rs = ps.executeQuery();

			if (rs.next()) {
				accountExists = (rs.getInt("account_cnt") > 0);// Set if account
																// exists
			}

			if (!accountExists) {
				LOG.error("Account does not exist for account id: " + accountId);
				throw new AccountNotFoundException("Account does not exist for account id: " + accountId);
			}
		} catch (SQLException e) {
			LOG.error("Exception during updateAccountStatus - validateAccount", e);
			throw new AccountException("Exception during updateAccountStatus", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		try {
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			LOG.debug("Creating Prepared Statement to update the basic account info in aersys_accounts table");
			
			// Set Auto Commit as false
			conn.setAutoCommit(false);
			
			// Do not allow deactivation if there are active child accounts
			validateChildAccountsOnDeactivation(Long.parseLong(accountId), status, conn);
									
			// Update not_used = 0
			updateBasicAccountStatus(Long.parseLong(accountId), status, approvalStatus, requestedDate, conn);
			
			// Update approval_status
			updateApprovalStatus(operatorId, accountId, status, approvalStatus, requestedDate, requestedUser, conn);
			
			// Commit the changes
			conn.commit();
			
			// Update Cache
			Account account = cache.get(Long.parseLong(accountId));
			
			if(account != null)
			{
				account.setStatus(status);
				account.setApprovalStatus(approvalStatus);
				cache.put(Long.parseLong(accountId), account);
			}
			
			success = true;
		} catch (SQLException e) {
	
			try {
				conn.rollback();
				LOG.info("Rolled back changes due to previous exception " + e);
			} catch (SQLException e1) {
				LOG.error("Exception during roll back " + e1);
			}
	
			throw new AccountException("Update Account Status failed due to db error", e);
		} catch (AccountNotFoundException e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw e;
		}  catch (AccountDeactivationException e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw e;
		}  catch (Exception e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw new AccountException("Update Account Status failed due to fatal error", e);
		}  finally {
			DBUtils.cleanup(conn, ps);
		}
		
		return success;
	}
	
	private int getActiveChildAccountsCount(long parentAccountId, Connection conn) throws SQLException {
		LOG.info("Fetching active children count for parent account id: " + parentAccountId);

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to fetch the active child account count in sub_accounts2 table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_ACTIVE_CHILD_ACCOUNT_COUNT);

			LOG.debug("Setting parameters");

			ps.setLong(1, parentAccountId);	
			
			LOG.info("Fetched active child accounts count in the db");

			rs = ps.executeQuery();
			
			if(rs.next())
			{
				count = rs.getInt(1);				
				LOG.info("Active Children count for parent account " + parentAccountId + ": " + count);
			}
			
			if(count <= 0)
			{
				LOG.info("No active child accounts found for account: "+parentAccountId);
			}
			else
			{
				LOG.info("Active child accounts associated to the parent account "+parentAccountId+": "+count);
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during fetch active children count", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}		
		
		return count;
	}

	private void validateChildAccountsOnDeactivation(long accountId, AccountStatus status, Connection conn) 
	throws AccountDeactivationException, SQLException {
		if(status == AccountStatus.SUSPENDED)
		{
			LOG.info("Checking if any child accounts available before deactivation...");
			
			int childCount = getActiveChildAccountsCount(accountId, conn);
			
			if(childCount > 0)
			{
				LOG.error("Active child accounts for account: "+accountId+": "+childCount);
				throw new AccountDeactivationException("Account cannot be deactivated as there are active children");
			}
			else
			{
				LOG.info("No active child accounts found for account: "+accountId);				
			}
		}
	}

	private void updateApprovalStatus(String operatorId, String accountId, AccountStatus status, ApprovalStatus approvalStatus, Date requestedDate, String requestedUser,
			Connection conn) throws SQLException, AccountNotFoundException {
		PreparedStatement ps = null;
		
		try
		{
			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_UPDATE_ACCT_STATUS);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setInt(1, status.getValue()); // account_status
			ps.setInt(2, approvalStatus.getValue());// approval_status
			ps.setString(3, requestedUser);// last_modified_by
			ps.setDate(4, new java.sql.Date(requestedDate.getTime()));// last_modified_date
			ps.setInt(5, Integer.parseInt(operatorId));// last_modified_by
			ps.setLong(6, Long.parseLong(accountId));// last_modified_date

			LOG.debug("Updating the account additional info in the db");

			int count = ps.executeUpdate();

			if (count <= 0) {
				LOG.error("updateAccountStatus: update account status failed for account id: " + accountId);
				throw new AccountNotFoundException("Account id: " + accountId + " is not valid");
			}
		}
		finally {
			DBUtils.cleanup(null, ps);
		}
	}

	@Override
	public boolean deleteAccount(String operatorId, String accountId, Date requestedDate, String requestedUser)
			throws AccountNotFoundException, AccountException {
		boolean success = false;
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean accountExists = false;

		LOG.info("deleteAccount: processing delete account...");

		try {
			LOG.debug("deleteAccount: Fetching db connection");

			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to validate if the account exists");

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_ACCOUNT_COUNT);

			LOG.debug("Setting Parameters");

			ps.setLong(1, Long.parseLong(operatorId));
			ps.setLong(2, Long.parseLong(accountId));

			LOG.debug("deleteAccount: validating if account exists: " + accountId);

			rs = ps.executeQuery();

			if (rs.next()) {
				accountExists = (rs.getInt("account_cnt") > 0);// Set if account
																// exists
			}

			if (!accountExists) {
				LOG.error("deleteAccount: Account does not exist for account id: " + accountId);
				throw new AccountNotFoundException("Account does not exist for account id: " + accountId);
			}
		} catch (SQLException e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw new AccountException("Delete Account failed due to db error", e);
		} catch (Exception e) {			
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw new AccountException("Delete Account failed due to fatal error", e);
		}  finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		// Delete Account using SQL Batch
		try {
			// Delete API Keys for the account
			accountApplicationDAO.deleteApiKeys(Long.parseLong(accountId), requestedDate, requestedUser);

			LOG.debug("deleteAccount: Fetching db connection");

			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

			LOG.debug("Creating Statement Batch to delete account from account_product_subscription, account_hierarchy, ws_account_contacts, ws_accts_additional_info, aersys_accounts tables");
			stmt = conn.createStatement();

			// Delete Products
			String deleteProductsQuery = SQL_DELETE_ACCOUNT_PRODUCT_SUBSCRIPTION.replace(":account_id", accountId);

			LOG.info("Adding query to batch: " + deleteProductsQuery);
			stmt.addBatch(deleteProductsQuery);

			// Delete Parent Account Association
			String deleteParentChildRelationshipQuery = SQL_DELETE_ACCOUNT_HEIRARCHY.replace(":child_account_id", accountId).replace(
					":account_id", String.valueOf(accountId));

			LOG.info("Adding query to batch: " + deleteParentChildRelationshipQuery);
			stmt.addBatch(deleteParentChildRelationshipQuery);

			// Delete Account Contacts
			String deleteContactsQuery = SQL_DELETE_ACCOUNT_CONTACT_INFO.replace(":account_id", accountId);

			LOG.info("Adding query to batch: " + deleteContactsQuery);
			stmt.addBatch(deleteContactsQuery);
			
			//Delete Account Overage bucket
			String deleteAccountOverageBucket = "DELETE FROM aerisgen.ACCOUNT_OVERAGE_BUCKET WHERE account_id = " + accountId;
			LOG.info("Adding query to batch: " + deleteAccountOverageBucket);
			stmt.addBatch(deleteAccountOverageBucket);

			// Delete Account Additional Info
			String deleteAdditionalAcctInfoQuery = SQL_DELETE_ACCT_ADDITIONAL_INFO.replace(":operator_id", operatorId).replace(
					":account_id", accountId);

			LOG.info("Adding query to batch: " + deleteAdditionalAcctInfoQuery);
			stmt.addBatch(deleteAdditionalAcctInfoQuery);

			// Delete Account Info
			String deleteAccountInfoQuery = SQL_DELETE_ACCOUNT_INFO.replace(":account_id", accountId);

			LOG.info("Adding query to batch: " + deleteAccountInfoQuery);
			stmt.addBatch(deleteAccountInfoQuery);
			

			LOG.debug("Deleting account from the db");

			stmt.executeBatch();

			// Commit the changes
			conn.commit();
			
			// Evict from Cache
			cache.evict(Long.parseLong(accountId));
			
			// Evict from Child Cache
			deleteFromChildCache(Long.parseLong(accountId));

			LOG.info("Delete Account successful.");

			success = true;
		} catch (SQLException e) {

			try {
				conn.rollback();
				LOG.info("Rolled back changes due to previous exception " + e);
			} catch (SQLException e1) {
				LOG.error("Exception during roll back " + e1);
			}

			throw new AccountException("Delete Account failed due to db error", e);
		} catch (Exception e) {			
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw new AccountException("Delete Account failed due to fatal error", e);
		}  finally {
			DBUtils.cleanup(conn, stmt);
		}

		return success;
	}
	

	private void deleteFromChildCache(long accountId) {
		if(parentChildMap.get(accountId) != null)
		{
			long parentId = parentChildMap.get(accountId);
			
			Map<Long, Account> childMap = childCache.get(parentId);
			
			if(childMap != null)
			{
				childMap.remove(accountId);
			}
		}
	}
	
	private void setupAccountFeatureCodes(long accountId, boolean connectivityEnabled, boolean aerCloudEnabled, Connection conn) throws SQLException {
		LOG.info("Setting up feature codes for account id: " + accountId+", connectivityEnabled: "+connectivityEnabled+", aerCloudEnabled: "+aerCloudEnabled);
		
		deleteFeatureCode(accountId, FEATURE_CODE_CONNECTIVITY, conn);
		deleteFeatureCode(accountId, FEATURE_CODE_AERCLOUD, conn);
		
		if(connectivityEnabled)
		{
			LOG.info("Enabling connectivity feature code: "+FEATURE_CODE_CONNECTIVITY+" for account id: "+accountId);
			insertFeatureCode(accountId, FEATURE_CODE_CONNECTIVITY, conn);
		}
		
		if(aerCloudEnabled)
		{
			LOG.info("Enabling aercloud feature code: "+FEATURE_CODE_AERCLOUD+" for account id: "+accountId);
			insertFeatureCode(accountId, FEATURE_CODE_AERCLOUD, conn);
		}
	}
	
	private void insertFeatureCode(long accountId, int featureCode, Connection conn) throws SQLException {
		PreparedStatement ps = null;
		
		try
		{
			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_INSERT_FEATURE_CODE);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(1, accountId); // account_id
			ps.setInt(2, featureCode);// feature_code
			
			LOG.debug("inserting feature code into ws_account_features table");

			int count = ps.executeUpdate();

			if (count > 0) {
				LOG.info("Added feature code for account id: " + accountId+" with feature code: "+featureCode);
			} 
		}
		finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void deleteFeatureCode(long accountId, int featureCode, Connection conn) throws SQLException {
		PreparedStatement ps = null;
		
		try
		{
			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_DELETE_FEATURE_CODE);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(1, accountId); // account_id
			ps.setInt(2, featureCode);// feature_code
			
			LOG.debug("deleting feature code into ws_account_features table");

			int count = ps.executeUpdate();

			if (count > 0) {
				LOG.info("Deleted feature code for account id: " + accountId+" with feature code: "+featureCode);
			} else	{
				LOG.info("Feature code "+featureCode+" does not exist for account id: " + accountId);
			}
		}
		finally {
			DBUtils.cleanup(null, ps);
		}
	}

	@Override
	public List<Account> getSelectedAccounts(final String operatorId, String... accountIds) {
		LOG.info("getSelectedAccounts: fetching accounts: "+accountIds);
		
		List<String> accountIdList = new ArrayList<String>();
		Collections.addAll(accountIdList, accountIds);
		
		List<Account> accounts = getAllAccounts(operatorId, null, null, accountIdList, false, null);
		
		LOG.info("returning "+accounts.size()+" accounts to the caller");		
		return accounts;
	}
	
	private String createMasterApiKey(long accountId, String requestedUser, Connection conn) throws SQLException, ApiKeyException {
		String apiKey = generateApiKey();

		LOG.info("Creating master api key: " + apiKey + " for account: " + accountId);

		Calendar invalidationDate = Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT));
		invalidationDate.set(2999, Calendar.DECEMBER, 31);

		String globalResource = "/*";
		String fullPermissions = "read,write,update,delete";

		String desc = "Master Key for Account: " + accountId;

		List<ResourcePermission> permissions = new ArrayList<ResourcePermission>();
		permissions.add(new ResourcePermission(globalResource, fullPermissions));

		LOG.info("Setting Master API Key Permissions: " + permissions + " to " + apiKey);

		try {
			LOG.debug("Calling Account Application DAO...");

			ApiKey key = accountApplicationDAO.createNewApiKey(accountId, permissions, Calendar.getInstance().getTime(), requestedUser,
					invalidationDate.getTime(), desc, "ACTIVE", apiKey, conn);

			LOG.info("Master API Key: " + key.getApiKey() + " has been attached to account id: " + accountId + " successfully");
			if(key.getPermissions().size() <= 0) {
				ApiKeyException permissionError = new ApiKeyException("Error while applying permissions to master apiKey.");
				LOG.error("Error while applying permissions to master apiKey.", permissionError);
				throw permissionError ;
			}

			return apiKey;
		} catch (ApiKeyException e) {
			LOG.error("Api Key Creation failed while creating master api key for account: "+accountId);
			throw e;
		} 
	}

	protected static String generateApiKey() {
		TimeBasedGenerator uuidGenerator = Generators.timeBasedGenerator();
		return uuidGenerator.generate().toString();
	}
	
	private List<Account> getAllAccounts() {
		List<Account> accounts = new ArrayList<Account>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			LOG.debug("getAllAccounts: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			ps = buildGetAccountsQuery(conn);

			//ps.setFetchSize(1000);
			

			LOG.debug("getAllAccounts: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				Account account = new Account();
				account.setAccountId(rs.getLong("account_id"));
				account.setAccountName(rs.getString("name"));
				account.setContractId(rs.getInt("contract_id"));
				account.setStatus(AccountStatus.fromValue(rs.getInt("account_status")));
				account.setApprovalStatus(ApprovalStatus.fromValue(rs.getInt("approval_status")));
				
				// set Products
				List<String> productIds = getProductIdsForAccount(rs.getString("product_ids"));
				account.setProductIds(productIds);

				account.setBillableStatus(BillableStatus.fromValue(rs.getInt("billable_status")));
				account.setCarrierAccountId(rs.getString("carrier_account_id"));
				account.setNetSuiteId(rs.getString("netsuite_id"));
				account.setOperatorId(rs.getInt("operator_id"));
				account.setParentAccountId(rs.getString("parent_account_id"));
                account.setLastModifiedDate(rs.getDate("last_modified_date"));
                account.setAccountType(rs.getString("account_type_b"));
                account.setCreditLimit(rs.getLong("credit_limit"));         

				accounts.add(account);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getAllAccounts", e);
			throw new GenericServiceException("Unable to getAllAccounts", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getAllAccounts" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		} 

		LOG.info("getAllAccounts: Returned " + accounts.size() + " accounts to the caller");

		return accounts;
	}
	
	private PreparedStatement buildGetAccountsQuery(Connection conn) throws SQLException {
		PreparedStatement ps;
		
		// Use this Query if the account id and status are not in account search
		// criteria
		StringBuilder query = new StringBuilder(SQL_GET_ACCOUNTS);

		// Add Group By Clause
		String sqlQuery = query.toString().replace(":group_by_clause", GROUP_BY_CLAUSE_WITH_CONTRACT);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(sqlQuery);
		
		LOG.debug("SQL Query is: " + query);

		return ps;
	}

	private void putInCache(Account... accounts) {
		for (Account account : accounts) {
			cache.put(account.getAccountId(), account);
		}
	}

	private void putInChildCache(Long parentAccount, List<Account> accounts) {
		Map<Long, Account> childMap = new HashMap<Long, Account>();
		
		for (Account account : accounts) {
			// Update child list
			childMap.put(account.getAccountId(), account);
			
			// Add parent child relationship
			parentChildMap.put(account.getAccountId(), parentAccount);
		}
		
		childCache.put(parentAccount, childMap);
	}
	
	private void updateChildCache(Long parentAccount, Account account) {
		Map<Long, Account> childMap = childCache.get(parentAccount);
		
		if(childMap != null && !childMap.isEmpty())
		{			
			// Update child list
			childMap.put(account.getAccountId(), account);	
			childCache.put(parentAccount, childMap);			
			
			// Update parent id
			parentChildMap.put(account.getAccountId(), parentAccount);
		}
	}
	
	private int getStartRange(String operatorId)
	{
		if(Integer.parseInt(operatorId) == OPERATOR_SPRINT)
		{
			return 40000;
		}
		
		return 0;
	}
	
	private int getEndRange(String operatorId)
	{
		if(Integer.parseInt(operatorId) == OPERATOR_SPRINT)
		{
			return 49999;
		}
		
		return 20000;
	}
	
	private void setAuxServicesFeatureCodes(Account account, Connection conn) throws SQLException {
		String accountId = String.valueOf(account.getAccountId());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// Create Prepared Statement
			//ps = conn.prepareStatement(SQL_GET_AUX_SERVICE_FEATURE_DESC);
			ps = conn.prepareStatement(SQL_GET_AUX_SERVICE_FEATURE_DESC.replace("?", accountId));
			//ps.setLong(1, Long.parseLong(accountId));
			
			LOG.info("setAuxServicesFeatureCodes: : executed query: " + ps);
			rs = ps.executeQuery();
			List<Integer> featureCodes = new ArrayList<Integer>();
			while(rs.next()) {
				List<String> auxServices = getAuxServicesForAccount(rs.getString("service_id"));
				account.setAuxServices(auxServices);
				featureCodes.add(rs.getInt("feature_code"));
				account.setFeatureCodes(featureCodes);
			}
		} catch (SQLException e) {
			LOG.error("Exception during setAuxServicesFeatureCodes", e);
			throw new GenericServiceException("Unable to getAuxServicesFeatureCodes", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
	}
	
	private void setAuxServicesFeatureCodes1(Account account, Connection conn) throws SQLException {
		String accountId = String.valueOf(account.getAccountId());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// Create Prepared Statement
			//ps = conn.prepareStatement(SQL_GET_AUX_SERVICE_FEATURE_DESC);
			ps = conn.prepareStatement(SQL_GET_AUX_SERVICE_FEATURE_DESC.replace("?", accountId));
			//ps.setLong(1, Long.parseLong(accountId));
			
			LOG.info("setAuxServicesFeatureCodes: : executed query: " + ps);
			rs = ps.executeQuery();
			while(rs.next()) {
				List<String> auxServices = getAuxServicesForAccount(rs.getString("service_id"));
				account.setAuxServices(auxServices);
				List<Integer> featureCodes = getFeatureCodeForAccount(rs.getString("feature_code"));
				account.setFeatureCodes(featureCodes);
			}
		} catch (SQLException e) {
			LOG.error("Exception during setAuxServicesFeatureCodes", e);
			throw new GenericServiceException("Unable to getAuxServicesFeatureCodes", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
	}
	
	private List<String> getAuxServicesForAccount(String auxService) {
		Set<String> auxServices = new HashSet<String>();
		if (auxService != null) {
			String[] auxServiceArr = auxService.split(",");
			Collections.addAll(auxServices, auxServiceArr);
		}
		return new ArrayList<String>(auxServices);
	}
	
	private List<Integer> getFeatureCodeForAccount(String featureCode) {
		List<Integer> featureCodes = new ArrayList<Integer>();
		if (featureCode != null) {
			String[] featureCodeArr = featureCode.split(",");
			for (String fCode : featureCodeArr) {
				featureCodes.add(Integer.parseInt(fCode));
			}
		}
		return featureCodes;
	}
    
    /**
     * This method sets the distributor_id value for the newly created account.
     * @param distributorId ID of distributor.
     * @param accountId ID of account created from distributor portal.
     * @param conn
     * @throws AccountException 
     */
    private void setDistributorId(String distributorId, long accountId, Connection conn) throws AccountException {
        LOG.debug("setDistributorId() - {}", distributorId);
        PreparedStatement ps = null;
        try {
            // Update distributor_id column for the account.
            ps = conn.prepareStatement(SQL_UPDATE_DISTRIBUTOR_ID);
            // Set Parameters
            ps.setLong(1, Long.parseLong(distributorId));
            ps.setLong(2, accountId);
            // Update row
            ps.executeUpdate();
        } catch (SQLException e) {
            LOG.error("setDistributorId() failed due to db error: " + e.getMessage());
            throw new AccountException("Could not set the distributor id due to db error");
        } finally {
            DBUtils.cleanup(null, ps);
        }
        LOG.debug("setDistributorId() - {} is set successfully for account {}", distributorId, accountId);
    }
    private Distributor createNewDistributor(long operatorId, Distributor request, String userId, Connection conn) throws SQLException {		
		PreparedStatement ps = null;
		Distributor distributor =  null ;
		try {
			ps = conn.prepareStatement(SQL_CREATE_DISTRIBUTOR);
			long distributorId = generateDistributorId(conn, operatorId);
			if(distributorId < 1) {
				throw new SQLException("Exception while generating distributorId. DB error");
			}
			ps.setLong(1, distributorId);
			ps.setLong(2, operatorId);
			ps.setString(3, request.getDistributorName());
			ps.setLong(4, request.getAccountId());
			ps.setString(5, StringUtils.join(request.getContactEmails(), ','));
            ps.setString(6, request.getAccessLink());
			ps.setString(7, request.getPrivacyPolicyUrls());
			ps.setInt(8, request.getStatus());
            ps.setLong(9, request.getLiferayCompanyId());
            ps.setString(10, request.getStoreUrl());
            ps.setBoolean(11, request.isWholesale());
            ps.setString(12, request.getDistributorName()+"-"+request.getAccountId());
            ps.setString(13, request.getPlanPricingUrl());
            ps.setString(14, request.getDistributorName()+"-"+request.getAccountId());
			ps.setString(15, userId);
			ps.setString(16, userId);
			int count = ps.executeUpdate();
			if(count != 1) {
				throw new SQLException("Exception while saving distributor. DB error");
			}
			distributor = new Distributor();
			distributor.setOperatorId(Long.valueOf(operatorId));
			distributor.setDistributorId(distributorId);
			distributor.setDistributorName(request.getDistributorName());
			distributor.setAccountId(request.getAccountId());
			distributor.setCreatedBy(userId);
			distributor.setLastModifiedBy(userId);
			distributor.setStatus(request.getStatus());
			distributor.setCreatedDate(request.getCreatedDate());
			distributor.setLastModifiedDate(request.getLastModifiedDate());
			distributor.setRepTimestamp(request.getRepTimestamp());
            distributor.setAccessLink(request.getAccessLink());
			distributor.setPrivacyPolicyUrls(request.getPrivacyPolicyUrls());
			distributor.setContactEmails(request.getContactEmails());
            distributor.setLiferayCompanyId(request.getLiferayCompanyId());
            distributor.setStoreUrl(request.getStoreUrl());
            distributor.setWholesale(request.isWholesale());
            distributor.setBrand(request.getDistributorName()+"-"+request.getAccountId());
            distributor.setPlanPricingUrl(request.getPlanPricingUrl());
            distributor.setRealm(request.getDistributorName()+"-"+request.getAccountId());
            distributorDAO.put(distributor.getDistributorId(), distributor);
		} catch(SQLException e) {
            LOG.error("SQLException occurred while creating distributor", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps);
		}
		return distributor;
	}
    /**
	 * Generates distributorId next-val for distributor creation
	 * 
	 * @param conn
	 * @param operatorId
	 * @return
	 * @throws SQLException
	 */
	private long generateDistributorId(Connection conn, long operatorId) throws SQLException {
		long newDistributorId = -1;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("createNewDistributor: fetching the next distributorId for new distributor creation");
			ps = conn.prepareStatement("SELECT aerisgen.distributor_id_seq.nextval FROM dual");
			rs = ps.executeQuery();
			if (rs.next()) {
				newDistributorId = rs.getLong(1);
			}
			LOG.info("DistributorId generated: " + newDistributorId);
		} catch (SQLException e) {
			LOG.error("Exception during createNewDistributor", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		return newDistributorId;
	}
	private Distributor updateDistributor(long operatorId,long distributorId, Distributor request, String userId, Connection conn) throws SQLException {
		PreparedStatement ps = null;
		Distributor distributor = null ;
		Distributor distDtls = distributorDAO.get(distributorId);
		if(distDtls == null ) {
			throw new SQLException("Distributor not found for distributorId=" + distributorId);
		}
		try {
			ps = conn.prepareStatement(SQL_UPDATE_DISTRIBUTOR);
			ps.setString(1, request.getDistributorName());
			ps.setLong(2, request.getAccountId());
			ps.setString(3, StringUtils.join(request.getContactEmails(), ','));
            ps.setString(4, request.getAccessLink());
			ps.setString(5, request.getPrivacyPolicyUrls());
			ps.setInt(6, request.getStatus());
            ps.setLong(7, request.getLiferayCompanyId());
            ps.setString(8, request.getStoreUrl());
            ps.setBoolean(9, request.isWholesale());
            ps.setString(10, request.getPlanPricingUrl());
			ps.setString(11, userId);
			ps.setLong(12, distributorId);
			ps.setLong(13, operatorId);
			int updated = ps.executeUpdate();
			if(updated != 1) {
				throw new SQLException("Exception while updating distributor. DB error");
			}
			distributor = new Distributor();
			distributor.setDistributorId(distributorId);
			distributor.setAccountId(request.getAccountId());
			distributor.setCreatedBy(distDtls.getCreatedBy());
			distributor.setLastModifiedBy(userId);
			distributor.setStatus(request.getStatus());
			distributor.setCreatedDate(request.getCreatedDate());
            distributor.setAccessLink(request.getAccessLink());
			distributor.setPrivacyPolicyUrls(request.getPrivacyPolicyUrls());
			distributor.setContactEmails(request.getContactEmails());
            distributor.setLiferayCompanyId(request.getLiferayCompanyId());
            distributor.setStoreUrl(request.getStoreUrl());
            distributor.setWholesale(request.isWholesale());
            distributor.setBrand(request.getDistributorName()+"-"+request.getAccountId());
            distributor.setPlanPricingUrl(request.getPlanPricingUrl());
            distributor.setDistributorName(request.getDistributorName());
            distributor.setRealm(request.getDistributorName()+"-"+request.getAccountId());
            distributor.setOperatorId(operatorId);
			distributorDAO.put(distributor.getDistributorId(), distributor);
		} catch(SQLException e) {
            LOG.error("SQLException occurred while updating distributor with distributorId {}", distributorId, e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps);
		}
		return distributor;
	}
	
	private void createAccountContract(long accountId, int contractId, Date requestedDate, String requestedUser, Connection conn) throws SQLException{
		PreparedStatement ps = null;
		
		try{
			LOG.info("Prepating prepared statment for ACCOUNT_CONTRACT insert for accountId-"+accountId+", Contract Id-"+contractId);
			ps = conn.prepareStatement(SQL_INSERT_ACCOUNT_CONTRACT);
			
			ps.setLong(1, accountId);
			ps.setInt(2, contractId);
			ps.setDate(3, new java.sql.Date(requestedDate.getTime()));
			ps.setString(4, requestedUser);
			ps.setDate(5, new java.sql.Date(requestedDate.getTime()));
			ps.setString(6, requestedUser);
			
			ps.executeUpdate();
		}catch (SQLException sqle) {
			LOG.error("Exception during creating account_contract ", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

    /**
     * Update Account contract for passed account
     * @param accountId
     * @param contractId
     * @param requestedDate
     * @param requestedUser
     * @param conn
     * @throws SQLException
     */
	private void updateAccountContract(long accountId, int contractId, Date requestedDate, String requestedUser, Connection conn) throws SQLException{
		PreparedStatement ps = null;

		try{
			LOG.info("Prepating prepared statment for ACCOUNT_CONTRACT update for accountId-"+accountId+", Contract Id-"+contractId);
			ps = conn.prepareStatement(SQL_UPDATE_ACCOUNT_CONTRACT);


			ps.setInt(1, contractId);
			ps.setDate(2, new java.sql.Date(requestedDate.getTime()));
			ps.setString(3, requestedUser);
			ps.setDate(4, new java.sql.Date(requestedDate.getTime()));
			ps.setString(5, requestedUser);
            ps.setLong(6, accountId);

			ps.executeUpdate();
		}catch (SQLException sqle) {
			LOG.error("Exception during updating account_contract ", sqle);
			throw sqle;
		}finally{
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void removeAccountContract(long accountId, Connection conn) throws SQLException{
		PreparedStatement ps = null;
		
		try{
			LOG.info("Preparing prepared statment for ACCOUNT_CONTRACT delete for accountId-"+accountId);
			ps = conn.prepareStatement(SQL_DELETE_ACCOUNT_CONTRACT);
			
			ps.setLong(1, accountId);
			
			ps.executeUpdate();
		}catch (SQLException sqle) {
			LOG.error("Exception during removing account_contract ", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	public Contract getAccountContract(long accountId, Connection conn){
		LOG.info("getting account contract  ..");

		PreparedStatement ps = null;
		ResultSet rs = null;
		Contract contract = null;
		try{
			String ACCOUNT_CONTRACTS_SQL = "select ac.ACCOUNT_ID, c.id, c.NAME , c.PARTNER_ID, c.AUTHORIZATION, c.GATEWAY_URL, c.SMPP_CONNECTION_ID, c.LICENSE_KEY  from ACCOUNT_CONTRACT ac, CONTRACT c where ac.CONTRACT_ID = c.id and ac.account_id = ?";

			if(LOG.isDebugEnabled()){
				LOG.debug("ACCOUNT_CONTRACTS_SQL :"+ACCOUNT_CONTRACTS_SQL);
			}

			ps = conn.prepareStatement(ACCOUNT_CONTRACTS_SQL);
			ps.setLong(1, accountId);
			rs = ps.executeQuery();

			if(rs.next()){
				contract = new Contract();
				long accId = rs.getLong("ACCOUNT_ID");
				contract.setAccountId(accId);
				contract.setContractId(rs.getInt("id"));
				contract.setContractName(rs.getString("NAME"));
				contract.setPartnerId(rs.getString("PARTNER_ID"));
				contract.setAuthorization(rs.getString("AUTHORIZATION"));
				contract.setGatewayUrl(rs.getString("GATEWAY_URL"));
				contract.setSmppConnectionId(rs.getLong("SMPP_CONNECTION_ID"));
				contract.setLicenseKey(rs.getString("LICENSE_KEY"));
			}

		}catch (Exception e) {
			LOG.error("Exception during GetAccountContract " + e);
			throw new GenericServiceException("Exception during GetAccountContract - ", e);
		}  finally {
			DBUtils.cleanup(null, ps, rs);
		}
		
		return contract;
	}

	private void rollbackAndRevertAccountContractCacheChanges(boolean contractInsertedInCache, boolean contractRemovedFromCache, long acctId, Connection conn, Exception e){
		try {
			if(conn != null){
				conn.rollback();
				LOG.info("Rolled back changes due to previous exception " + e);
			}
		} catch (SQLException e1) {
			LOG.error("Exception during roll back " + e1);
		}
		
		if(contractInsertedInCache){
			contractEnum.removeContractFromCache(acctId);
			LOG.info("reverted insertion of contract in cache...");
		}
		if (contractRemovedFromCache){
			Contract contract = getAccountContract(acctId, conn);
			contractEnum.putContractInCache(acctId, contract);
			LOG.info("reverted removal of contract from cache...");
		}
	}
}
