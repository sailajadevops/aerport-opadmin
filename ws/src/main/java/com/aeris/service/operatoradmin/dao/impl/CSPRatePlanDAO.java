package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.ICSPRatePlanDAO;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.ResultStatus;
import com.aeris.service.operatoradmin.enums.IPacketRoundingPolicyEnum;
import com.aeris.service.operatoradmin.enums.IPoolingPolicyEnum;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.exception.BadStartDateException;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.RatePlanException;
import com.aeris.service.operatoradmin.exception.RatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.exception.SubRatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.BillingEvent;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.PacketRoundingPolicy;
import com.aeris.service.operatoradmin.model.PoolingPolicy;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.ProvisionTriggerSettings;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanPaymentType;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.RatePlanTier;
import com.aeris.service.operatoradmin.model.RatePlanType;
import com.aeris.service.operatoradmin.model.SuspendTriggerSettings;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZoneRatePlan;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class CSPRatePlanDAO implements ICSPRatePlanDAO {
	private static Logger LOG = LoggerFactory.getLogger(CSPRatePlanDAO.class);

	private static final String SQL_GET_RATE_PLAN_ID_BY_NAME = "select rate_plan_id from csp_rate_plan where upper(rate_plan_name) = upper(?)";
	
	private static final String SQL_UPDATE_RATE_PLAN_NAME_BY_ID = 	"update csp_rate_plan set " +
																	"rate_plan_name = ?, " +
																	"plan_description = ? " +
																	"where rate_plan_id = ?";
	
	private static final String SQL_UPDATE_PLAN_ELEMENT = 	"update plan_element set " +
															"overage_charge_special_carrier = ?, " +
															"overage_charge_us_roaming = ? " +
															"where rate_plan_id = ? " +
															"and billing_event_id = ?";
    
    private static final String SQL_UPDATE_INCL_ROAM_FLG_BY_ID = "update csp_rate_plan set incl_roam_flg = ? where rate_plan_id = ?";	

    private static final String SQL_UPDATE_TECHNOLOGY_BY_ID = "update csp_rate_plan set technology_type = ? where rate_plan_id = ?";
    
	@Inject
	@Named("createCSPRatePlanCDMAStoredProcedure")
	private IStoredProcedure<ResultStatus> createCSPRatePlanCDMAStoredProcedure;
	
	@Inject
	@Named("createCSPRatePlanGSMStoredProcedure")
	private IStoredProcedure<ResultStatus> createCSPRatePlanGSMStoredProcedure;
	
	@Inject
	@Named("updateCSPRatePlanCDMAStoredProcedure")
	private IStoredProcedure<ResultStatus> updateCSPRatePlanCDMAStoredProcedure;
	
	@Inject
	@Named("updateCSPRatePlanGSMStoredProcedure")
	private IStoredProcedure<ResultStatus> updateCSPRatePlanGSMStoredProcedure;
	
	@Inject
	private IZoneDAO zoneDAO;
	
	@Inject
	private IProductEnum productCache;
	
	@Inject
	private IPoolingPolicyEnum poolingPolicyCache;

	@Inject
	private IPacketRoundingPolicyEnum packetRoundingPolicyCache;
	
	@Inject
	private OAUtils oaUtils;
	
	@Override
	public long createNewRatePlan(int operatorId, int productId, String ratePlanName, RatePlanType ratePlanType, RatePlanPaymentType paymentType,
			RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded, RatePlanStatus status, int tierCriteria,
			int homeZoneId, String description, String specialNotes, int carrierRatePlanId, long accountId, Date startDate, Date endDate, String currencyName,
			int includedPeriodMonths, int devicePoolingPolicyId, int packetRoundingPolicyId, RatePlanFeeDetails ratePlanFeeDetails,
			ProvisionTriggerSettings provisionTriggerSettings, SuspendTriggerSettings suspendTriggerSettings, List<ZoneRatePlan> zoneRatePlanSettings,
			List<RatePlanTier> ratePlanTiers, boolean roamingIncluded, Date requestedDate, String requestedUser,int zoneSetId, String technology, Connection conn) throws RatePlanException, DuplicateRatePlanException,
			ZoneNotFoundException, ZoneException {
		long newRatePlanId = 0;
		
		Product product = productCache.getProductById(productId);
        			
		LOG.info("Creating new CSP Rate Plan with name: "+ratePlanName+" for product: "+product.getProductName()+" and technology: "+ technology);
		Zone[] overageZones = zoneDAO.getOverageZonesByZoneSet(operatorId, zoneSetId, homeZoneId);
		
		double[] domesticOverages = getOverageCharges(overageZones[0], zoneRatePlanSettings);// Fetch Domestic Overage Charges
		LOG.info("Home Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(domesticOverages));
		
		double[] canadaOverages = getOverageCharges(overageZones[1], zoneRatePlanSettings);// Fetch Canada Overage Charges
		LOG.info("Canada Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(canadaOverages));
		
		double[] mexicoOverages = getOverageCharges(overageZones[2], zoneRatePlanSettings);// Fetch Mexico Overage Charges
		LOG.info("Mexico Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(mexicoOverages));
		
		double[] internationalOverages = getOverageCharges(overageZones[3], zoneRatePlanSettings);// Fetch International Overage Charges		
		LOG.info("International Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(internationalOverages));
		
		double[] specialCarrierOverages = getOverageCharges(overageZones[4], zoneRatePlanSettings);// Fetch Special Carrier Overage Charges
		LOG.info("Special Carrier Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(specialCarrierOverages));
		
		double[] usRoamingOverages = getOverageCharges(overageZones[5], zoneRatePlanSettings);// Fetch US Roaming Overage Charges		
		LOG.info("US Roaming Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(usRoamingOverages));
		
		long[] includedPlan = getIncludedPlan(overageZones[0], zoneRatePlanSettings);// Fetch Domestic included plan details
		LOG.info("Included Plan {sms (in count), voice (in mins), packet (in kb)}; "+toString(includedPlan));
		
		String includedRolloverPolicy = expireIncluded ? "No Rollover" : "Rollover";	
				
		double accessFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getAccessFee() : 0;
		LOG.info("Access Fee: "+accessFee);
		
		double activationFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getActivationFee() : 0;
		LOG.info("Activation Fee: "+activationFee);
		
		double suspendFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getSuspendFee() : 0;
		LOG.info("Suspend Fee: "+suspendFee);
		
		double deactivationFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getDeactivationFee() : 0;
		LOG.info("Deactivation Fee: "+deactivationFee);
		
		double reactivationFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getReactivationFee() : 0;
		LOG.info("Reactivation Fee: "+reactivationFee);
		
		double unsuspendFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getUnsuspendFee() : 0;		
		LOG.info("Unsuspend Fee: "+unsuspendFee);
		
		double perSmsPriceProv = provisionTriggerSettings != null ? provisionTriggerSettings.getPerMTSmsPrice() : 0;
		double perPacketKBPriceProv = provisionTriggerSettings != null ? provisionTriggerSettings.getPerKBPacketPrice() : 0;
		double perMinVoicePriceProv = provisionTriggerSettings != null ? provisionTriggerSettings.getPerMinMTVoicePrice() : 0;	
		
		int provTrafficDays = provisionTriggerSettings != null ? provisionTriggerSettings.getTrafficDays() : 0;
		int provSmsThreshold = provisionTriggerSettings != null ? provisionTriggerSettings.getMtSmsThreshold() : 0;
		long provPacketKBThreshold = provisionTriggerSettings != null ? provisionTriggerSettings.getPacketThresholdInKB() : 0;
		int provVoiceMinsThreshold = provisionTriggerSettings != null ? provisionTriggerSettings.getMtVoiceMinsThreshold() : 0;
		int provDurationMonths = provisionTriggerSettings != null ? provisionTriggerSettings.getDurationInMonths() : 0;
		
		int suspTrafficDays = suspendTriggerSettings != null ? suspendTriggerSettings.getTrafficDays() : 0;
		int suspSmsThreshold = suspendTriggerSettings != null ? suspendTriggerSettings.getMtSmsThreshold() : 0;
		long suspPacketKBThreshold = suspendTriggerSettings != null ? suspendTriggerSettings.getPacketThresholdInKB() : 0;
		int suspVoiceMinsThreshold = suspendTriggerSettings != null ? suspendTriggerSettings.getMtVoiceMinsThreshold() : 0;
		int suspDurationMonths = suspendTriggerSettings != null ? suspendTriggerSettings.getDurationInMonths() : 0;
		
		PoolingPolicy devicePoolingPolicy = poolingPolicyCache.getPoolingPolicyById(devicePoolingPolicyId);
		PacketRoundingPolicy packetRoundingPolicy = packetRoundingPolicyCache.getPacketRoundingPolicyById(packetRoundingPolicyId);
		
		if(devicePoolingPolicy != null)
		{
			LOG.info("Device Pooling Policy: "+devicePoolingPolicy.getPolicyName());
		}
		
		if(packetRoundingPolicy != null)
		{
			LOG.info("Packet Data Rounding Policy: "+packetRoundingPolicy.getPolicyName());
		}
		
		boolean success = false;
        
		LOG.info("Creating a {} rate plan", technology);
		if("CDMA".equalsIgnoreCase(technology))
		{
			success = createCDMARatePlan(productId, ratePlanName, accessFeeMonths, startDate, endDate, requestedUser, domesticOverages, canadaOverages,
					mexicoOverages, specialCarrierOverages, usRoamingOverages, includedPlan, includedRolloverPolicy, accessFee, activationFee, suspendFee,
					deactivationFee, reactivationFee, unsuspendFee, perSmsPriceProv, perPacketKBPriceProv, perMinVoicePriceProv, includedPeriodMonths,
					devicePoolingPolicy, packetRoundingPolicy, provTrafficDays, provSmsThreshold, provPacketKBThreshold, provVoiceMinsThreshold,
					provDurationMonths, suspTrafficDays, suspSmsThreshold, suspPacketKBThreshold, suspVoiceMinsThreshold, suspDurationMonths, conn);
			
			if(success)
			{
				LOG.info("CDMA rate plan created successfully");
			}
			else
			{
				LOG.warn("Failed to create CDMA rate plan");
			}
		}
		else {
			success = createGSMRatePlan(productId, ratePlanName, accessFeeMonths, startDate, endDate, requestedUser, domesticOverages, canadaOverages,
					mexicoOverages, internationalOverages, specialCarrierOverages, usRoamingOverages, includedPlan, includedRolloverPolicy, accessFee,
					activationFee, suspendFee, deactivationFee, reactivationFee, unsuspendFee, perSmsPriceProv, perPacketKBPriceProv, perMinVoicePriceProv,
					includedPeriodMonths, devicePoolingPolicy, packetRoundingPolicy, provTrafficDays, provSmsThreshold, provPacketKBThreshold,
					provVoiceMinsThreshold, provDurationMonths, suspTrafficDays, suspSmsThreshold, suspPacketKBThreshold, suspVoiceMinsThreshold,
					suspDurationMonths, conn);
			
			if(success)
			{
				LOG.info("{} rate plan created successfully", technology);
			}
			else
			{
				LOG.warn("Failed to create {} rate plan", technology);
			}
		}
		
		// Create Rate Plan to Overage Zones mapping
		if(success)
		{						
			LOG.info("Fetching the newly created rate plan id...");			
			newRatePlanId = getCSPRatePlanIdByName(ratePlanName, conn);
			
			// Update Special Carrier And US Roaming overages for the rate plan 
			if(newRatePlanId > 0)
			{
				LOG.info("Updating special carrier and us roaming overages for the new rate plan id: "+newRatePlanId);
				updateSpecialCarrierAndUsRoamingOverages(newRatePlanId, specialCarrierOverages, usRoamingOverages, conn);
			}
            setRoamingIncluded(roamingIncluded, newRatePlanId, conn);
            setTechnology(technology, newRatePlanId, conn);
		}
		
		return newRatePlanId; 
	}

	private void updateSpecialCarrierAndUsRoamingOverages(long newRatePlanId, double[] specialCarrierOverages, double[] usRoamingOverages, Connection conn) 
		throws RatePlanException {
		PreparedStatement ps = null;
		
		try
		{
			// Update rate plan name to auto generated name from algo
			ps = conn.prepareStatement(SQL_UPDATE_PLAN_ELEMENT);
			
			// Update SMS
			updateOveragesInPlanElement(newRatePlanId, specialCarrierOverages, usRoamingOverages, ps, BillingEvent.SMS, 0);
			
			// Update GPRS
			updateOveragesInPlanElement(newRatePlanId, specialCarrierOverages, usRoamingOverages, ps, BillingEvent.GPRS, 2);
			
			// Update 1xRTT
			updateOveragesInPlanElement(newRatePlanId, specialCarrierOverages, usRoamingOverages, ps, BillingEvent.PACKET, 2);

			// Update Voice			
			updateOveragesInPlanElement(newRatePlanId, specialCarrierOverages, usRoamingOverages, ps, BillingEvent.VOICE, 1);

			LOG.info("Executing batch. Updating Special Carrier And Us Roaming Overages...");
			
			// Update row
			ps.executeBatch();
			
			LOG.info("Update Special Carrier And Us Roaming Overages successful");
		}
		catch (SQLException e) {
			LOG.error("Update Special Carrier And Us Roaming Overages failed due to db error: "+e.getMessage());
			throw new RatePlanException("Could not update Special Carrier And Us Roaming Overages due to db error");
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}		
	}

	private void updateOveragesInPlanElement(long newRatePlanId, double[] specialCarrierOverages, double[] usRoamingOverages, PreparedStatement ps,
			BillingEvent billingEvent, int index) throws SQLException {
		double specialCarrierOverage = specialCarrierOverages[index];
		double usRoamingOverage = usRoamingOverages[index];
		int billingEventId = billingEvent.getValue();

		// Update only when the specialCarrier and usRoaming overages are valid
		if(specialCarrierOverage != 0 && usRoamingOverage != 0)
		{
			ps.clearParameters();
			
			LOG.info("Creating sql statement: " + ps +" to update plan element with special carrier overage: "+specialCarrierOverages[index]+
					" and us roaming overage: "+usRoamingOverages[index]+" for billing event: "+billingEvent +" and rate plan id: "+newRatePlanId);

			// Set Parameters
			ps.setDouble(1, specialCarrierOverages[index]);
			ps.setDouble(2, usRoamingOverages[index]);
			ps.setLong(3, newRatePlanId);
			ps.setLong(4, billingEventId);
			
			LOG.info("Adding to batch");
			ps.addBatch();
		}	
	}

	private IncludedPlanDetails getOverridenZoneRates(List<ZoneRatePlan> zoneProfiles, int zoneId)
	{
		LOG.info("Checking if the zone: "+zoneId+" is overriden...");
		
		if(zoneProfiles != null)
		{
			for (ZoneRatePlan zoneRatePlan : zoneProfiles) {
				if(zoneRatePlan.getZoneId() == zoneId)
				{
					LOG.info("Zone: "+zoneId+" is overriden. Returning overriden rates.");
					return zoneRatePlan.getIncludedPlanDetails();
				}
			}
		}
		
		LOG.info("Zone: "+zoneId+" is not overriden");
		return null;
	}
	
	
	@Override
	public String updateAndGetRatePlanNameUsingAlgo(int operatorId, int productId, long ratePlanId, Connection conn) throws RatePlanException {
		String newAutoGeneratedRatePlanName = null;
				
		// Update with the auto generated rate plan name from algo
		if(ratePlanId > 0)
		{
			LOG.info("Executing the algo to generate the auto generated rate plan name for rate plan id: "+ratePlanId);
			newAutoGeneratedRatePlanName = oaUtils.generateRatePlanName(operatorId, ratePlanId, productId);
			
			LOG.info("Setting rate plan: "+ratePlanId+" with the auto generated rate plan name: "+newAutoGeneratedRatePlanName);
			setRatePlanNameFromAlgo(newAutoGeneratedRatePlanName, ratePlanId, conn);	
			
			LOG.info("Set rate plan: "+ratePlanId+" with the auto generated rate plan name: "+newAutoGeneratedRatePlanName+" successfully");
		}
		
		return newAutoGeneratedRatePlanName;
	}

	private void setRatePlanNameFromAlgo(String newAutoGeneratedRatePlanName, long ratePlanId, Connection conn) throws RatePlanException {
		PreparedStatement ps = null;
		
		try
		{
			// Update rate plan name to auto generated name from algo
			ps = conn.prepareStatement(SQL_UPDATE_RATE_PLAN_NAME_BY_ID);	
			
			LOG.info("Creating sql statement: " + ps +" to update rate plan name to : "+newAutoGeneratedRatePlanName+" for rate plan id: "+ratePlanId);

			// Set Parameters
			ps.setString(1, newAutoGeneratedRatePlanName);
			ps.setString(2, newAutoGeneratedRatePlanName);
			ps.setLong(3, ratePlanId);
			
			// Update row
			ps.executeUpdate();
		}
		catch (SQLException e) {
			LOG.error("setRatePlanNameFromAlgo failed due to db error: "+e.getMessage());
			throw new RatePlanException("Could not set the auto generated grate plan name due to db error");
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}		
		
		LOG.info("Rate Plan name updated to: "+newAutoGeneratedRatePlanName+" for rate plan id: "+ratePlanId+" successfully");
	}
	
	private long getCSPRatePlanIdByName(String ratePlanName, Connection conn) throws RatePlanException {
		long ratePlanId = 0;

		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try
		{
			// Create Query
			ps = conn.prepareStatement(SQL_GET_RATE_PLAN_ID_BY_NAME);	
			
			LOG.info("Creating sql statement: " + ps +" to get rate plan id against rate plan name: "+ratePlanName);
	
			ps.setString(1, ratePlanName);
			
			rs = ps.executeQuery();
			
			if(rs.next())
			{
				ratePlanId = rs.getLong(1);
			}
			
			LOG.info("Rate Plan Id for rate plan name: "+ratePlanName+" is: "+ratePlanId);
		}
		catch (SQLException e) {
			LOG.error("getCSPRatePlanIdByName failed due to db error: "+e.getMessage());
			throw new RatePlanException("Could not fetch the rate plan id for the new rate plan name due to db error");
		}
		finally
		{
			DBUtils.cleanup(null, ps, rs);
		}
		
		if(ratePlanId <= 0)
		{
			throw new RatePlanException("Could not fetch the rate plan id for the new rate plan name: "+ratePlanName);
		}
		
		return ratePlanId;
	}
		
	private boolean createCDMARatePlan(int productId, String ratePlanLabel, int accessFeeMonths, Date startDate, Date endDate, String requestedUser,
			double[] domesticOverages, double[] canadaOverages, double[] mexicoOverages, double[] specialCarrierOverages, double[] usRoamingOverages,
			long[] includedPlan, String includedRolloverPolicy, double accessFee, double activationFee, double suspendFee, double deactivationFee,
			double reactivationFee, double unsuspendFee, double perSmsPriceProv, double perPacketKBPriceProv, double perMinVoicePriceProv,
			int includedPeriodMonths, PoolingPolicy devicePoolingPolicy, PacketRoundingPolicy packetRoundingPolicy, int provTrafficDays, int provSmsThreshold,
			long provPacketKBThreshold, int provVoiceMinsThreshold, int provDurationMonths, int suspTrafficDays, int suspSmsThreshold,
			long suspPacketKBThreshold, int suspVoiceMinsThreshold, int suspDurationMonths, Connection conn) throws DuplicateRatePlanException,
			RatePlanException {
		ResultStatus result;
		
		try
		{
			String packetRoundingPolicyName = packetRoundingPolicy.getPolicyName();
			String devicePoolingPolicyName = devicePoolingPolicy != null ? devicePoolingPolicy.getPolicyName() : "None";			
			
			// Create CDMA Rate Plan 
			result = createCSPRatePlanCDMAStoredProcedure.execute(
						conn,
						ratePlanLabel,//in_plan_name
						new java.sql.Date(startDate.getTime()),//in_start_date
						new java.sql.Date(endDate.getTime()),//in_end_date
						accessFee,//in_access_fee
						
						includedPlan[0],//in_sms_included
						domesticOverages[0],//in_sms_overage_domestic
						canadaOverages[0],//in_sms_overage_canada
						mexicoOverages[0],//in_sms_overage_mexico
						
						includedPlan[2],//in_1xrtt_included
						domesticOverages[2],//in_1xrtt_overage_domestic
						canadaOverages[2],//in_1xrtt_overage_canada
						mexicoOverages[2],//in_1xrtt_overage_mexico
						
						0,//in_mburst_included
						0,//in_mburst_overage_domestic
						0,//in_mburst_overage_canada
						0,//in_mburst_overage_mexico
						
						includedPlan[1],//in_voice_included
						domesticOverages[1],//in_voice_overage_domestic
						canadaOverages[1],//in_voice_overage_canada
						mexicoOverages[1],//in_voice_overage_mexico
						
						activationFee,//in_first_bill_activation_fee
						suspendFee,//in_suspend_fee
						deactivationFee,//in_deactivation_fee
						reactivationFee,//in_reactivation_fee
						unsuspendFee,//in_unsuspend_fee
						
						0,//in_prov_microburst_pkt_price
						perSmsPriceProv,//in_prov_sms_msg_price
						perPacketKBPriceProv,//in_prov_1xrtt_kb_price
						perMinVoicePriceProv,//in_prov_voice_minutes_price
						
						provTrafficDays,//in_allowed_prov_days_traffic
						0,//in_allowed_prov_mburst_pkts
						provSmsThreshold,//in_allowed_prov_sms_msgs
						provPacketKBThreshold,//in_allowed_prov_1xrtt_kb
						provVoiceMinsThreshold,//in_allowed_prov_voice_mins
						provDurationMonths,//in_allowed_prov_months
						
						suspTrafficDays,//in_allowed_susp_days_traffic
						0,//in_allowed_susp_mburst_pkts
						suspSmsThreshold,//in_allowed_susp_sms_msgs
						suspPacketKBThreshold,//in_allowed_susp_1xrtt_kb
						suspVoiceMinsThreshold,//in_allowed_susp_voice_mins
						suspDurationMonths,//in_allowed_susp_months
						
						packetRoundingPolicyName,//in_packet_data_rounding_policy
						"Monthly",//in_access_fee_period_type
						accessFeeMonths,//in_access_fee_period_length
						"Monthly",//in_included_period_type
						includedPeriodMonths,//in_included_period_length
						includedRolloverPolicy,//in_included_rollover_policy
						devicePoolingPolicyName,//in_device_pooling_policy
						
						"No Comments",//in_comments
						StringUtils.abbreviate(requestedUser, 25),//in_created_by
						null,//in_incentive_unit_volume
						null,//in_incentive_expiration
						null,//in_special_carrier_id
						null,//in_1xrtt_overage_spec_carrier
						null,//in_1xrtt_overage_us_roaming
						0,//in_monthly_fee_ip
						productId,//in_product_id
						1//in_status
						);
			
			// Success
			if(result.getResultCode() == 0)
			{
				LOG.info("createCSPRatePlan: createCSPRatePlanCDMAStoredProcedure: success");
				return true;				
			}
			// Duplicate Rate Plan Name
			else if(result.getResultCode() == 535)
			{
				throw new DuplicateRatePlanException("The rate plan with the same name already exists");
			}
			
		} catch (StoredProcedureException e) {
			LOG.error("StoredProcedureException while creating csp rate plan");
			throw new RatePlanException("Error creating csp rate plan due to db error", e);
		}
		
		return false;
	}
	
	private boolean createGSMRatePlan(int productId, String ratePlanLabel, int accessFeeMonths, Date startDate, Date endDate, String requestedUser,
			double[] domesticOverages, double[] canadaOverages, double[] mexicoOverages, double[] internationalOverages, double[] specialCarrierOverages,
			double[] usRoamingOverages, long[] includedPlan, String includedRolloverPolicy, double accessFee, double activationFee, double suspendFee,
			double deactivationFee, double reactivationFee, double unsuspendFee, double perSmsPriceProv, double perPacketKBPriceProv,
			double perMinVoicePriceProv, int includedPeriodMonths, PoolingPolicy devicePoolingPolicy, PacketRoundingPolicy packetRoundingPolicy,
			int provTrafficDays, int provSmsThreshold, long provPacketKBThreshold, int provVoiceMinsThreshold, int provDurationMonths, int suspTrafficDays,
			int suspSmsThreshold, long suspPacketKBThreshold, int suspVoiceMinsThreshold, int suspDurationMonths, Connection conn)
			throws DuplicateRatePlanException, RatePlanException {
		ResultStatus result;

		try
		{
			String packetRoundingPolicyName = packetRoundingPolicy.getPolicyName();
			String devicePoolingPolicyName = devicePoolingPolicy != null ? devicePoolingPolicy.getPolicyName() : "None";
			
			// Create GSM Rate Plan 
			result = createCSPRatePlanGSMStoredProcedure.execute(
						conn,
						ratePlanLabel,//in_plan_name
						new java.sql.Date(startDate.getTime()),//in_start_date
						new java.sql.Date(endDate.getTime()),//in_end_date
						accessFee,//in_access_fee
						
						includedPlan[0],//in_sms_included
						0,//in_sms_monthly_charge
						domesticOverages[0],//in_sms_overage_domestic
						internationalOverages[0],//in_sms_overage_intl
						canadaOverages[0],//in_sms_overage_canada
						mexicoOverages[0],//in_sms_overage_mexico
						
						includedPlan[2],//in_gprs_included
						0,//in_gprs_monthly_charge
						domesticOverages[2],//in_gprs_overage_domestic
						internationalOverages[2],//in_gprs_overage_intl
						canadaOverages[2],//in_gprs_overage_canada
						mexicoOverages[2],//in_gprs_overage_mexico
						
						0,//in_mburst_included
						0,//in_mburst_monthly_charge
						0,//in_mburst_overage_domestic
						0,//in_mburst_overage_intl
						0,//in_mburst_overage_canada
						0,//in_mburst_overage_mexico
						
						includedPlan[1],//in_voice_included
						0,//in_voice_monthly_charge
						domesticOverages[1],//in_voice_overage_domestic
						internationalOverages[1],//in_voice_overage_intl
						canadaOverages[1],//in_voice_overage_canada
						mexicoOverages[1],//in_voice_overage_mexico
						
						activationFee,//in_first_bill_activation_fee
						suspendFee,//in_suspend_fee
						deactivationFee,//in_deactivation_fee
						reactivationFee,//in_reactivation_fee
						unsuspendFee,//in_unsuspend_fee
						
						0,//in_deact_waived_after_months
						0,//in_prov_microburst_pkt_price
						perSmsPriceProv,//in_prov_sms_msg_price
						perPacketKBPriceProv,//in_prov_1xrtt_kb_price
						perMinVoicePriceProv,//in_prov_voice_minutes_price
						
						provTrafficDays,//in_allowed_prov_days_traffic
						0,//in_allowed_prov_mburst_pkts
						provSmsThreshold,//in_allowed_prov_sms_msgs
						provPacketKBThreshold,//in_allowed_prov_1xrtt_kb
						provVoiceMinsThreshold,//in_allowed_prov_voice_mins
						provDurationMonths,//in_allowed_prov_months
						
						suspTrafficDays,//in_allowed_susp_days_traffic
						0,//in_allowed_susp_mburst_pkts
						suspSmsThreshold,//in_allowed_susp_sms_msgs
						suspPacketKBThreshold,//in_allowed_susp_1xrtt_kb
						suspVoiceMinsThreshold,//in_allowed_susp_voice_mins
						suspDurationMonths,//in_allowed_susp_months
						
						0,//in_allowed_reprov_days_traffic
						0,//in_allowed_reprov_mburst_pkts
						0,//in_allowed_reprov_sms_msgs
						0,//in_allowed_reprov_1xrtt_kb
						0,//in_allowed_reprov_voice_mins
						0,//in_allowed_reprov_months
						
						packetRoundingPolicyName,//in_packet_data_rounding_policy
						"Monthly",//in_access_fee_period_type
						accessFeeMonths,//in_access_fee_period_length
						
						"No Comments",//in_comments
						StringUtils.abbreviate(requestedUser, 25),//in_created_by
						productId,//in_product_id
						1,//in_status
						devicePoolingPolicyName
						);
			// Success
			if(result.getResultCode() == 0)
			{
				LOG.info("createCSPRatePlan: createCSPRatePlanCDMAStoredProcedure: success");
				return true;				
			}
			// Duplicate Rate Plan Name
			else if(result.getResultCode() == 535)
			{
				throw new DuplicateRatePlanException("The rate plan with the same name already exists");
			}
			
		} catch (StoredProcedureException e) {
			LOG.error("StoredProcedureException while creating csp rate plan");
			throw new RatePlanException("Error creating csp rate plan due to db error", e);
		}
		
		return false;
	}
	
	private boolean updateGSMRatePlan(long ratePlanId, int productId, String ratePlanLabel, int accessFeeMonths, Date startDate, Date endDate,
			String requestedUser, double[] domesticOverages, double[] canadaOverages, double[] mexicoOverages, double[] internationalOverages,
			double[] specialCarrierOverages, double[] usRoamingOverages, long[] includedPlan, String includedRolloverPolicy, double accessFee,
			double activationFee, double suspendFee, double deactivationFee, double reactivationFee, double unsuspendFee, double perSmsPriceProv,
			double perPacketKBPriceProv, double perMinVoicePriceProv, int includedPeriodMonths, PoolingPolicy devicePoolingPolicy,
			PacketRoundingPolicy packetRoundingPolicy, int provTrafficDays, int provSmsThreshold, long provPacketKBThreshold, int provVoiceMinsThreshold,
			int provDurationMonths, int suspTrafficDays, int suspSmsThreshold, long suspPacketKBThreshold, int suspVoiceMinsThreshold, int suspDurationMonths,
			Connection conn) throws DuplicateRatePlanException, RatePlanException {
	ResultStatus result;

		try
		{
			String packetRoundingPolicyName = packetRoundingPolicy != null ? WordUtils.capitalizeFully(packetRoundingPolicy.getPolicyName()) : "None";
			String devicePoolingPolicyName = devicePoolingPolicy != null ? devicePoolingPolicy.getPolicyName() : "None";
			// Update GSM Rate Plan 
			result = updateCSPRatePlanGSMStoredProcedure.execute(
						conn,
						ratePlanId,//in_rate_plan_id 
						ratePlanLabel,//in_plan_name
						new java.sql.Date(startDate.getTime()),//in_start_date
						new java.sql.Date(endDate.getTime()),//in_end_date
						accessFee,//in_access_fee
						
						includedPlan[0],//in_sms_included
						0,//in_sms_monthly_charge
						domesticOverages[0],//in_sms_overage_domestic
						internationalOverages[0],//in_sms_overage_intl
						canadaOverages[0],//in_sms_overage_canada
						mexicoOverages[0],//in_sms_overage_mexico
						
						includedPlan[2],//in_gprs_included
						0,//in_gprs_monthly_charge
						domesticOverages[2],//in_gprs_overage_domestic
						internationalOverages[2],//in_gprs_overage_intl
						canadaOverages[2],//in_gprs_overage_canada
						mexicoOverages[2],//in_gprs_overage_mexico
						
						0,//in_mburst_included
						0,//in_mburst_monthly_charge
						0,//in_mburst_overage_domestic
						0,//in_mburst_overage_intl
						0,//in_mburst_overage_canada
						0,//in_mburst_overage_mexico
						
						includedPlan[1],//in_voice_included
						0,//in_voice_monthly_charge
						domesticOverages[1],//in_voice_overage_domestic
						internationalOverages[1],//in_voice_overage_intl
						canadaOverages[1],//in_voice_overage_canada
						mexicoOverages[1],//in_voice_overage_mexico
						
						activationFee,//in_first_bill_activation_fee
						suspendFee,//in_suspend_fee
						deactivationFee,//in_deactivation_fee
						reactivationFee,//in_reactivation_fee
						unsuspendFee,//in_unsuspend_fee
						
						0,//in_deact_waived_after_months
						0,//in_prov_microburst_pkt_price
						perSmsPriceProv,//in_prov_sms_msg_price
						perPacketKBPriceProv,//in_prov_1xrtt_kb_price
						perMinVoicePriceProv,//in_prov_voice_minutes_price
						
						provTrafficDays,//in_allowed_prov_days_traffic
						0,//in_allowed_prov_mburst_pkts
						provSmsThreshold,//in_allowed_prov_sms_msgs
						provPacketKBThreshold,//in_allowed_prov_1xrtt_kb
						provVoiceMinsThreshold,//in_allowed_prov_voice_mins
						provDurationMonths,//in_allowed_prov_months
						
						suspTrafficDays,//in_allowed_susp_days_traffic
						0,//in_allowed_susp_mburst_pkts
						suspSmsThreshold,//in_allowed_susp_sms_msgs
						suspPacketKBThreshold,//in_allowed_susp_1xrtt_kb
						suspVoiceMinsThreshold,//in_allowed_susp_voice_mins
						suspDurationMonths,//in_allowed_susp_months
						
						0,//in_allowed_reprov_days_traffic
						0,//in_allowed_reprov_mburst_pkts
						0,//in_allowed_reprov_sms_msgs
						0,//in_allowed_reprov_1xrtt_kb
						0,//in_allowed_reprov_voice_mins
						0,//in_allowed_reprov_months
						
						packetRoundingPolicyName,//in_packet_data_rounding_policy
						"No Comments",//in_comments
						StringUtils.abbreviate(requestedUser, 25),//in_changed_by
						productId,//in_product_id
						1,//in_status
						devicePoolingPolicyName
						);
			// Success
			if(result.getResultCode() == 0)
			{
				LOG.info("updateCSPRatePlan: updateCSPRatePlanGSMStoredProcedure: success");
				return true;				
			}
			// Duplicate Rate Plan Name
			else if(result.getResultCode() == 535)
			{
				throw new DuplicateRatePlanException("The rate plan with the same name already exists");
			}
			
		} catch (StoredProcedureException e) {
			LOG.error("StoredProcedureException while updating csp rate plan");
			throw new RatePlanException("Error updating csp rate plan due to db error", e);
		}
		
		return false;
	}
	
	private boolean updateCDMARatePlan(long ratePlanId, int productId, String ratePlanLabel, int accessFeeMonths, Date startDate, Date endDate,
			String requestedUser, double[] domesticOverages, double[] canadaOverages, double[] mexicoOverages, double[] internationalOverages,
			double[] specialCarrierOverages, double[] usRoamingOverages, long[] includedPlan, String includedRolloverPolicy, double accessFee,
			double activationFee, double suspendFee, double deactivationFee, double reactivationFee, double unsuspendFee, double perSmsPriceProv,
			double perPacketKBPriceProv, double perMinVoicePriceProv, int includedPeriodMonths, PoolingPolicy devicePoolingPolicy,
			PacketRoundingPolicy packetRoundingPolicy, int provTrafficDays, int provSmsThreshold, long provPacketKBThreshold, int provVoiceMinsThreshold,
			int provDurationMonths, int suspTrafficDays, int suspSmsThreshold, long suspPacketKBThreshold, int suspVoiceMinsThreshold, int suspDurationMonths,
			Connection conn) throws DuplicateRatePlanException, RatePlanException {
	ResultStatus result;

		try
		{
			String packetRoundingPolicyName = packetRoundingPolicy != null ? WordUtils.capitalizeFully(packetRoundingPolicy.getPolicyName()) : "None";
			String devicePoolingPolicyName = devicePoolingPolicy == null ? null : devicePoolingPolicy.getPolicyName();
			
			// Update CDMA Rate Plan 
			result = updateCSPRatePlanCDMAStoredProcedure.execute(
						conn,
						ratePlanId,//in_rate_plan_id 
						ratePlanLabel,//in_plan_name
						new java.sql.Date(startDate.getTime()),//in_start_date
						new java.sql.Date(endDate.getTime()),//in_end_date
						accessFee,//in_access_fee
						
						includedPlan[0],//in_sms_included
						0,//in_sms_monthly_charge
						domesticOverages[0],//in_sms_overage_domestic
						internationalOverages[0],//in_sms_overage_intl
						canadaOverages[0],//in_sms_overage_canada
						mexicoOverages[0],//in_sms_overage_mexico
						
						includedPlan[2],//in_1xrtt_included
						0,//in_1xrtt_monthly_charge
						domesticOverages[2],//in_1xrtt_overage_domestic
						internationalOverages[2],//in_1xrtt_overage_intl
						canadaOverages[2],//in_1xrtt_overage_canada
						mexicoOverages[2],//in_1xrtt_overage_mexico
						0,//in_1xrtt_overage_us_roaming
						
						0,//in_mburst_included
						0,//in_mburst_monthly_charge
						0,//in_mburst_overage_domestic
						0,//in_mburst_overage_intl
						0,//in_mburst_overage_canada
						0,//in_mburst_overage_mexico
						
						includedPlan[1],//in_voice_included
						0,//in_voice_monthly_charge
						domesticOverages[1],//in_voice_overage_domestic
						internationalOverages[1],//in_voice_overage_intl
						canadaOverages[1],//in_voice_overage_canada
						mexicoOverages[1],//in_voice_overage_mexico
						
						activationFee,//in_first_bill_activation_fee
						suspendFee,//in_suspend_fee
						deactivationFee,//in_deactivation_fee
						reactivationFee,//in_reactivation_fee
						unsuspendFee,//in_unsuspend_fee
						
						0,//in_deact_waived_after_months
						0,//in_prov_microburst_pkt_price
						perSmsPriceProv,//in_prov_sms_msg_price
						perPacketKBPriceProv,//in_prov_1xrtt_kb_price
						perMinVoicePriceProv,//in_prov_voice_minutes_price
						
						provTrafficDays,//in_allowed_prov_days_traffic
						0,//in_allowed_prov_mburst_pkts
						provSmsThreshold,//in_allowed_prov_sms_msgs
						provPacketKBThreshold,//in_allowed_prov_1xrtt_kb
						provVoiceMinsThreshold,//in_allowed_prov_voice_mins
						provDurationMonths,//in_allowed_prov_months
						
						suspTrafficDays,//in_allowed_susp_days_traffic
						0,//in_allowed_susp_mburst_pkts
						suspSmsThreshold,//in_allowed_susp_sms_msgs
						suspPacketKBThreshold,//in_allowed_susp_1xrtt_kb
						suspVoiceMinsThreshold,//in_allowed_susp_voice_mins
						suspDurationMonths,//in_allowed_susp_months
						
						0,//in_allowed_reprov_days_traffic
						0,//in_allowed_reprov_mburst_pkts
						0,//in_allowed_reprov_sms_msgs
						0,//in_allowed_reprov_1xrtt_kb
						0,//in_allowed_reprov_voice_mins
						0,//in_allowed_reprov_months
						
						packetRoundingPolicyName,//in_packet_data_rounding_policy
						"No Comments",//in_comments
						StringUtils.abbreviate(requestedUser, 25),//in_changed_by
						productId,//in_product_id
						1,//in_status
						devicePoolingPolicyName // in_device_pooling_policy
						);
			// Success
			if(result.getResultCode() == 0)
			{
				LOG.info("updateCSPRatePlan: updateCSPRatePlanCDMAStoredProcedure: success");
				return true;				
			}
			// Duplicate Rate Plan Name
			else if(result.getResultCode() == 535)
			{
				throw new DuplicateRatePlanException("The rate plan with the same name already exists");
			}
			
		} catch (StoredProcedureException e) {
			LOG.error("StoredProcedureException while updating csp rate plan");
			throw new RatePlanException("Error updating csp rate plan due to db error", e);
		}
		
		return false;
	}

	private double[] getOverageCharges(Zone zone, List<ZoneRatePlan> zoneRatePlanSettings) {
		double[] overages = new double[3];
		
		double smsOverage = 0;
		double voiceOverage = 0;
		double packetOverage = 0;
		
		if(zone != null)
		{
			LOG.info("Checking if the zone: "+zone.getZoneName()+" is overriden in the rate plan");
			
			// Fetch overriden rates if zone is overriden
			IncludedPlanDetails planDetails = getOverridenZoneRates(zoneRatePlanSettings, zone.getZoneId());
			
			// If zone is not overriden, get the default rates defined in zone.
			if(planDetails == null)
			{
				planDetails = zone.getIncludedPlan();
			}
			
			if(planDetails != null)
			{
				smsOverage = planDetails.getPerMTSmsPrice();
				voiceOverage = planDetails.getPerMinMTVoicePrice();
				packetOverage = planDetails.getPerKBPacketPrice();
			}
		}
		
		overages[0] = smsOverage;
		overages[1] = voiceOverage;
		overages[2] = packetOverage;
		
		return overages;
	}
	
	private long[] getIncludedPlan(Zone zone, List<ZoneRatePlan> zoneRatePlanSettings) {
		long[] includedPlan = new long[3];
		
		long includedSms = 0;
		long includeVoiceMins = 0;
		long includedPacketKB = 0;
		
		if(zone != null)
		{
			LOG.info("Checking if the zone: "+zone.getZoneName()+" is overriden in the rate plan");
			
			// Fetch overriden rates if zone is overriden
			IncludedPlanDetails planDetails = getOverridenZoneRates(zoneRatePlanSettings, zone.getZoneId());
			
			// If zone is not overriden, get the default rates defined in zone.
			if(planDetails == null)
			{
				planDetails = zone.getIncludedPlan();
			}
			
			if(planDetails != null)
			{
				includedSms = planDetails.getIncludedMTSms();
				includeVoiceMins = planDetails.getIncludedMTVoiceMins();
				includedPacketKB = planDetails.getIncludedPacketKB();
			}
		}
		
		includedPlan[0] = includedSms;
		includedPlan[1] = includeVoiceMins;
		includedPlan[2] = includedPacketKB;
		
		return includedPlan;
	}

	@Override
	public boolean updateRatePlan(int operatorId, long ratePlanId, int productId, String ratePlanName, RatePlanType ratePlanType,
			RatePlanPaymentType paymentType, RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded,
			RatePlanStatus status, int tierCriteria, int homeZoneId, String description, String specialNotes, int carrierRatePlanId, long accountId,
			Date startDate, Date endDate, String currencyName, int includedPeriodMonths, int devicePoolingPolicyId, int packetRoundingPolicyId,
			RatePlanFeeDetails ratePlanFeeDetails, ProvisionTriggerSettings provisionTriggerSettings, SuspendTriggerSettings suspendTriggerSettings,
			List<ZoneRatePlan> zoneRatePlanSettings, List<RatePlanTier> ratePlanTiers, boolean roamingIncluded, Date requestedDate, String requestedUser,int zoneSetId, String technology, Connection conn)
			throws RatePlanNotFoundException, RatePlanException, BadStartDateException, SubRatePlanNotFoundException, DuplicateRatePlanException,
			ZoneNotFoundException, DuplicateZoneException, ZoneException {
		Product product = productCache.getProductById(productId);
			
		LOG.info("Updating new CSP Rate Plan with name: "+ratePlanName+" for product: "+product.getProductName()+" and technology: "+ technology);
		Zone[] overageZones = zoneDAO.getOverageZonesByZoneSet(operatorId, zoneSetId, homeZoneId);
		
		double[] domesticOverages = getOverageCharges(overageZones[0], zoneRatePlanSettings);// Fetch Domestic Overage Charges
		LOG.info("Home Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(domesticOverages));
		
		double[] canadaOverages = getOverageCharges(overageZones[1], zoneRatePlanSettings);// Fetch Canada Overage Charges
		LOG.info("Canada Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(canadaOverages));
		
		double[] mexicoOverages = getOverageCharges(overageZones[2], zoneRatePlanSettings);// Fetch Mexico Overage Charges
		LOG.info("Mexico Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(mexicoOverages));
		
		double[] internationalOverages = getOverageCharges(overageZones[3], zoneRatePlanSettings);// Fetch International Overage Charges		
		LOG.info("International Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(internationalOverages));
		
		double[] specialCarrierOverages = getOverageCharges(overageZones[4], zoneRatePlanSettings);// Fetch Special Carrier Overage Charges
		LOG.info("Special Carrier Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(specialCarrierOverages));
		
		double[] usRoamingOverages = getOverageCharges(overageZones[5], zoneRatePlanSettings);// Fetch US Roaming Overage Charges		
		LOG.info("US Roaming Zone Overage Charges {sms (per count), voice (per min), packet (per kb)}; "+toString(usRoamingOverages));
		
		
		long[] includedPlan = getIncludedPlan(overageZones[0], zoneRatePlanSettings);// Fetch Domestic included plan details
		LOG.info("Included Plan {sms (in count), voice (in mins), packet (in kb)}; "+toString(includedPlan));
		
		String includedRolloverPolicy = expireIncluded ? "Rollover" : "No Rollover";	
				
		double accessFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getAccessFee() : 0;
		LOG.info("Access Fee: "+accessFee);
		
		double activationFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getActivationFee() : 0;
		LOG.info("Activation Fee: "+accessFee);
		
		double suspendFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getSuspendFee() : 0;
		LOG.info("Suspend Fee: "+accessFee);
		
		double deactivationFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getDeactivationFee() : 0;
		LOG.info("Deactivation Fee: "+accessFee);
		
		double reactivationFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getReactivationFee() : 0;
		LOG.info("Reactivation Fee: "+accessFee);
		
		double unsuspendFee = ratePlanFeeDetails != null ? ratePlanFeeDetails.getUnsuspendFee() : 0;		
		LOG.info("Unsuspend Fee: "+accessFee);
		
		double perSmsPriceProv = ratePlanFeeDetails != null ? provisionTriggerSettings.getPerMTSmsPrice() : 0;
		double perPacketKBPriceProv = ratePlanFeeDetails != null ? provisionTriggerSettings.getPerKBPacketPrice() : 0;
		double perMinVoicePriceProv = ratePlanFeeDetails != null ? provisionTriggerSettings.getPerMinMTVoicePrice() : 0;	
		
		int provTrafficDays = provisionTriggerSettings != null ? provisionTriggerSettings.getTrafficDays() : 0;
		int provSmsThreshold = provisionTriggerSettings != null ? provisionTriggerSettings.getMtSmsThreshold() : 0;
		long provPacketKBThreshold = provisionTriggerSettings != null ? provisionTriggerSettings.getPacketThresholdInKB() : 0;
		int provVoiceMinsThreshold = provisionTriggerSettings != null ? provisionTriggerSettings.getMtVoiceMinsThreshold() : 0;
		int provDurationMonths = provisionTriggerSettings != null ? provisionTriggerSettings.getDurationInMonths() : 0;
		
		int suspTrafficDays = suspendTriggerSettings != null ? suspendTriggerSettings.getTrafficDays() : 0;
		int suspSmsThreshold = suspendTriggerSettings != null ? suspendTriggerSettings.getMtSmsThreshold() : 0;
		long suspPacketKBThreshold = suspendTriggerSettings != null ? suspendTriggerSettings.getPacketThresholdInKB() : 0;
		int suspVoiceMinsThreshold = suspendTriggerSettings != null ? suspendTriggerSettings.getMtVoiceMinsThreshold() : 0;
		int suspDurationMonths = suspendTriggerSettings != null ? suspendTriggerSettings.getDurationInMonths() : 0;
		
		PoolingPolicy devicePoolingPolicy = poolingPolicyCache.getPoolingPolicyById(devicePoolingPolicyId);
		PacketRoundingPolicy packetRoundingPolicy = packetRoundingPolicyCache.getPacketRoundingPolicyById(packetRoundingPolicyId);
		
		if(devicePoolingPolicy != null)
		{
			LOG.info("Device Pooling Policy: "+devicePoolingPolicy.getPolicyName());
		}
		
		if(packetRoundingPolicy != null)
		{
			LOG.info("Packet Data Rounding Policy: "+packetRoundingPolicy.getPolicyName());
		}
		
		boolean success = false;
        
		LOG.info("Updating a {} rate plan {}", technology, ratePlanId);
		if("CDMA".equalsIgnoreCase(technology))
		{
			success = updateCDMARatePlan(ratePlanId, productId, ratePlanName, accessFeeMonths, startDate, endDate, requestedUser, domesticOverages,
					canadaOverages, mexicoOverages, internationalOverages, specialCarrierOverages, usRoamingOverages, includedPlan, includedRolloverPolicy,
					accessFee, activationFee, suspendFee, deactivationFee, reactivationFee, unsuspendFee, perSmsPriceProv, perPacketKBPriceProv,
					perMinVoicePriceProv, includedPeriodMonths, devicePoolingPolicy, packetRoundingPolicy, provTrafficDays, provSmsThreshold,
					provPacketKBThreshold, provVoiceMinsThreshold, provDurationMonths, suspTrafficDays, suspSmsThreshold, suspPacketKBThreshold,
					suspVoiceMinsThreshold, suspDurationMonths, conn);
			
			if(success)
			{
				LOG.info("Updated a {} rate plan {} successfully.", technology, ratePlanId);
			}
			else
			{
				LOG.info("Not updated a {} rate plan {} successfully.", technology, ratePlanId);
			}
		}
		else {
			success = updateGSMRatePlan(ratePlanId, productId, ratePlanName, accessFeeMonths, startDate, endDate, requestedUser, domesticOverages,
					canadaOverages, mexicoOverages, internationalOverages, specialCarrierOverages, usRoamingOverages, includedPlan, includedRolloverPolicy,
					accessFee, activationFee, suspendFee, deactivationFee, reactivationFee, unsuspendFee, perSmsPriceProv, perPacketKBPriceProv,
					perMinVoicePriceProv, includedPeriodMonths, devicePoolingPolicy, packetRoundingPolicy, provTrafficDays, provSmsThreshold,
					provPacketKBThreshold, provVoiceMinsThreshold, provDurationMonths, suspTrafficDays, suspSmsThreshold, suspPacketKBThreshold,
					suspVoiceMinsThreshold, suspDurationMonths, conn);
			
			if(success)
			{
				LOG.info("{} rate plan {} updated successfully", technology, ratePlanId);
			}
			else
			{
				LOG.info("Not updated a {} rate plan {} successfully.", technology, ratePlanId);
			}
		}
		
		LOG.info("updateCSPRatePlan: "+success);
		
		if(success)
		{
			// Update Special Carrier And US Roaming overages for the rate plan 
			LOG.info("Updating special carrier and us roaming overages for the new rate plan id: "+ratePlanId);
			
			updateSpecialCarrierAndUsRoamingOverages(ratePlanId, specialCarrierOverages, usRoamingOverages, conn);
            setRoamingIncluded(roamingIncluded, ratePlanId, conn);
            setTechnology(technology, ratePlanId, conn);
		}
		
		return success; 
	}
	
	private String toString(Object obj)
	{
		return ReflectionToStringBuilder.toString(obj, ToStringStyle.SIMPLE_STYLE);
	}
    
    private void setRoamingIncluded(boolean roamingIncluded, long ratePlanId, Connection conn) throws RatePlanException {
        LOG.info("setRoamingIncluded: " + roamingIncluded + " :: " + ratePlanId);
        PreparedStatement ps = null;
        try {
            // Update rate plan name to auto generated name from algo
            ps = conn.prepareStatement(SQL_UPDATE_INCL_ROAM_FLG_BY_ID);
            LOG.info("Creating sql statement: " + ps + " to update incl_roam_flag to : " + (roamingIncluded ? 1 : 0) + " for rate plan id: " + ratePlanId);
            // Set Parameters
            ps.setInt(1, (roamingIncluded ? 1 : 0));
            ps.setLong(2, ratePlanId);
            // Update row
            ps.executeUpdate();
        } catch (SQLException e) {
            LOG.error("setRoamingIncluded failed due to db error: " + e.getMessage());
            throw new RatePlanException("Could not set the roaming included due to db error");
        } finally {
            DBUtils.cleanup(null, ps);
        }
        LOG.info("Rate Plan name updated to: " + (roamingIncluded ? 1 : 0) + " for rate plan id: " + ratePlanId + " successfully.");
    }
    
    private void setTechnology(String technology, long ratePlanId, Connection conn) throws RatePlanException {
        LOG.info("setTechnology: " + technology + " :: " + ratePlanId);
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(SQL_UPDATE_TECHNOLOGY_BY_ID);
            LOG.info("Creating sql statement: " + ps + " to update technology to : " + technology + " for rate plan id: " + ratePlanId);
            // Set Parameters
            ps.setString(1, technology);
            ps.setLong(2, ratePlanId);
            // Update row
            ps.executeUpdate();
        } catch (SQLException e) {
            LOG.error("setTechnology failed due to db error: " + e.getMessage());
            throw new RatePlanException("Could not set the technology due to db error");
        } finally {
            DBUtils.cleanup(null, ps);
        }
        LOG.info("Technology updated to: " + technology + " for rate plan id: " + ratePlanId + " successfully.");
    }
}
