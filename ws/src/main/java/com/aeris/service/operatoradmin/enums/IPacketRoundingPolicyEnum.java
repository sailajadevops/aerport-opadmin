package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.PacketRoundingPolicy;

public interface IPacketRoundingPolicyEnum  extends IBaseEnum{
	PacketRoundingPolicy getPacketRoundingPolicyByName(String name);
	PacketRoundingPolicy getPacketRoundingPolicyById(int id);
	List<PacketRoundingPolicy> getAllPacketRoundingPolicies();
}
