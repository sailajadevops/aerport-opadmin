package com.aeris.service.operatoradmin.delegate.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.delegate.ITemplateProcessor;
import com.aeris.service.operatoradmin.delegate.TemplateData;
import com.aeris.service.operatoradmin.exception.ExportException;
import com.aeris.service.operatoradmin.utils.OAUtils;

public class SimpleTemplateProcessor implements ITemplateProcessor {
private static final String VARIABLE_REGEX = "%(([a-zA-Z0-9_#]+)?)%";
	private static final String VARIABLE_REGEX_2 = "%((([a-zA-Z0-9_]+)(#:var)))%";

	private static Logger LOG = LoggerFactory.getLogger(SimpleTemplateProcessor.class);
	
	private static final String FOR_REGEX = "^#FOR(\\s*)\\[(.+)(\\s*)<(\\s*)%(.*?)%\\](\\s*)$";
	private static final String EXPR_REGEX = "^#(.+)(\\s*)\\[(.+)\\](\\s*)$";
	private static final String IF_REGEX = "^#IF(\\s*)\\[(.+)(\\s*)([<>=])(\\s*)(.+)\\](\\s*)$";
	
	private OAUtils oaUtils;
	String newline;
	
	public SimpleTemplateProcessor(String newline) {
		oaUtils = OperatorAdminListener.sInjector.getInstance(OAUtils.class);
		this.newline = newline;
	}
		
	@Override
	public byte[] createByteStreamFromTemplate(String templatePath, TemplateData templateData) throws ExportException{		
		byte[] bytes = null;
		
		try
		{			
			File templateFile = oaUtils.getTemplate(templatePath);
		    
		    List<String> lines = FileUtils.readLines(templateFile);
		    
		    StringBuffer sb = new StringBuffer();		   
			
		    for (int i = 0; i < lines.size(); i++) {
		    	String templateLine = lines.get(i);
				
				if(isExpression(templateLine))
				{
					StringBuffer exprContent = new StringBuffer();
					int startIndex = i;						
					int endIndex = parseExpression(exprContent, lines, startIndex, templateData);
					
					if(endIndex != startIndex)
					{
						String[] exprLines = exprContent.toString().split("\n");
						
						for (int count = (endIndex - startIndex) + 1; count > 0; count --) {
							lines.remove(startIndex);
						}
						
						int j = startIndex; 
						
						for (String exprLine : exprLines) {
							lines.add(j++, exprLine);
						}	
					}
				}
		    }
		    	
		    for (int i = 0; i < lines.size(); i++) {
		    	String templateLine = lines.get(i);
		    	String processedLine = templateLine;
		    	
			    // Check for %KEY% pattern
				Pattern pattern = Pattern.compile(VARIABLE_REGEX);		
				Matcher matcher = pattern.matcher(templateLine);
				
				// Replace with the values in template data
				while(matcher.find())
				{
					String subseq = matcher.group();
					String key = matcher.group(1);
					int startIndex = matcher.start();
					int endIndex = matcher.end();
					
					String defaultValue = "NONE";
					
					processedLine = processTemplateLine(templateData, processedLine, key, subseq, startIndex, endIndex, defaultValue);   
				}
				
				sb.append(processedLine).append(newline);
			}
			
			bytes = sb.toString().getBytes();
		}
		catch(IOException e)
		{
			LOG.error("IO Exception while exporting report from template");
			throw new ExportException("IO Exception while exporting report from template", e);
		}
		
		return bytes;
	}

	private boolean isExpression(String subseq) {
		return subseq.matches(EXPR_REGEX);
	}

	private String processTemplateLine(TemplateData data, String line, String key, String subseq, int startIndex, int endIndex, String defaultValue) {
		String value = null;
		int lineWidth = subseq.length();
					
		if(data != null)
		{
			String localDefaultValue = data.getDefaultValue(key);
			String localValue = data.get(key);
			
			if(localDefaultValue != null)
			{
				defaultValue = localDefaultValue;
			}
			
			if(localValue != null)
			{
				value = localValue;
			}
			
			Map<String, Integer> dimensions = data.getItemDimensions();
			Set<String> dimensionKeys = dimensions.keySet();
			
			for (String dimensionKey : dimensionKeys) {
				if(key.matches(dimensionKey))
				{
					lineWidth = dimensions.get(dimensionKey);	
					break;
				}	
			}
		}
		
		if(value == null)
		{
			value = defaultValue;
		}
		
		if(lineWidth < value.length())
		{
			int padding = value.length() - subseq.length();
			String suffix = line.substring(endIndex < line.length() - 1 ? endIndex : line.length() - 1, (endIndex + padding) < line.length() - 1 ? endIndex + padding : line.length() - 1);
			
			if(StringUtils.isNotEmpty(suffix) && StringUtils.isBlank(suffix))
			{
				lineWidth = subseq.length() + suffix.length(); 
			}
		}
		
		// Pad the value to line width
		value = StringUtils.rightPad(value, lineWidth);
		subseq = StringUtils.rightPad(subseq, lineWidth);
		
		// Replace the pattern found with the value 
		line = line.replaceAll(subseq, value);
		
		return line;
	}
	
	private int parseExpression(StringBuffer exprContent, List<String> lines, int startIndex, TemplateData data) throws ExportException {
		int endIndex = startIndex;
		
		if(lines != null){
			String startLine = lines.get(startIndex);
			
			if(startLine.matches(FOR_REGEX))
			{
				endIndex = parseLoop(exprContent, lines, startIndex, data);
			}
			else if(startLine.matches(IF_REGEX))
			{
				endIndex = parseIf(exprContent, lines, startIndex, data);
			}
		}		
		
		return endIndex;
	}

	private int parseIf(StringBuffer exprContent, List<String> lines, int startIndex, TemplateData data) throws ExportException {
		int endIndex = startIndex;
		String lhs = null;
		String rhs = null;
		String operator = null;
		boolean validCondition = false;
		
		String line = lines.get(startIndex);
		
		// Check for FOR pattern
		Pattern pat = Pattern.compile(IF_REGEX);		
		Matcher matcher = pat.matcher(line);
		
		// Replace with the values in template data
		if(matcher.find())
		{
			lhs = matcher.group(2).trim();
			operator = matcher.group(4).trim();
			rhs = matcher.group(6).trim();
			
			validCondition = compare(lhs, rhs, operator, data);
		}
		
		if(validCondition)
		{
			for (int k = startIndex + 1; k < lines.size(); k++) 
			{
				line = lines.get(k);
				
				if(line.matches("^#END(\\s*)$"))
				{
					endIndex = k;
					break;
				}
				else if(k == lines.size() - 1)
				{
					throw new ExportException("Missing #END in #FOR loop in the template");
				}
			
				// Check for %KEY% pattern
				pat = Pattern.compile(VARIABLE_REGEX);		
				matcher = pat.matcher(line);
				
				// Replace with the values in template data
				while(matcher.find())
				{
					String subseq = matcher.group();
					String key = matcher.group(1);
					int startInd = matcher.start();
					int endInd = matcher.end();
					
					String defaultValue = "NONE";
					
					line = processTemplateLine(data, line, key, subseq, startInd, endInd, defaultValue);   
				}			
				
				exprContent.append(line).append("\n");
			}
		}
		else
		{
			for (int k = startIndex + 1; k < lines.size(); k++) 
			{
				line = lines.get(k);
				
				if(line.matches("^#END(\\s*)$"))
				{
					endIndex = k;
					break;
				}
				else if(k == lines.size() - 1)
				{
					throw new ExportException("Missing #END in #FOR loop in the template");
				}
			}
		}
		
		return endIndex;
	}

	private int parseLoop(StringBuffer exprContent, List<String> lines, int startIndex, TemplateData data) throws ExportException {
		int endIndex = startIndex;
		String forLiteral = null;
		int loopLength = 0;
		
		String line = lines.get(startIndex);
		
		// Check for FOR pattern
		Pattern pat = Pattern.compile(FOR_REGEX);		
		Matcher matcher = pat.matcher(line);
		
		// Replace with the values in template data
		if(matcher.find())
		{
			forLiteral = matcher.group(2).trim();
			
			String loopLenVariable = matcher.group(5).trim();
			loopLength = NumberUtils.toInt(data.get(loopLenVariable));
		}
		
		if(loopLength <= 0)
		{
			for (int k = startIndex + 1; k < lines.size(); k++) 
			{
				line = lines.get(k);
				
				if(line.matches("^#END(\\s*)$"))
				{
					endIndex = k;
					break;
				}
				else if(k == lines.size() - 1)
				{
					throw new ExportException("Missing #END in #FOR loop in the template");
				}
			}
		}
		else
		{
			for(int j = 0; j < loopLength; j++)
			{	
				for (int k = startIndex + 1; k < lines.size(); k++) 
				{
					line = lines.get(k);
					
					if(line.matches("^#END(\\s*)$"))
					{
						endIndex = k;
						break;
					}
					else if(k == lines.size() - 1)
					{
						throw new ExportException("Missing #END in #FOR loop in the template");
					}
				
					// Check for %KEY% pattern
					pat = Pattern.compile(VARIABLE_REGEX);		
					matcher = pat.matcher(line);
					
					Map<String, String> replacementMap = new HashMap<String, String>();
					
					// Replace with the values in template data
					while(matcher.find())
					{
						// Check for %KEY% pattern
						Pattern subPat = Pattern.compile(VARIABLE_REGEX_2.replace(":var", forLiteral));		
						Matcher subMatcher = subPat.matcher(matcher.group().trim());
						
						// Replace with the values in template data
						if(subMatcher.find())
						{
							String prefix = subMatcher.group(3);
							String subseq = "#"+forLiteral;
							String word = matcher.group();
							String replacedKey = prefix+subseq;
							
							String replacedWord = word.replaceAll(subseq, String.valueOf(j + 1));
							
							// Check for %KEY% pattern
							Pattern dimPat = Pattern.compile(VARIABLE_REGEX);		
							Matcher dimMatcher = dimPat.matcher(replacedWord);
							
							if(dimMatcher.find())
							{
								String workKey = dimMatcher.group(1);
								String exprKey = "%"+replacedKey+"%";
								int lineWidth = exprKey.length();
								
								Map<String, Integer> dimensions = data.getItemDimensions();
								Set<String> dimensionKeys = dimensions.keySet();
								
								for (String dimensionKey : dimensionKeys) {
									if(workKey.matches(dimensionKey))
									{
										lineWidth = dimensions.get(dimensionKey);	
										break;
									}	
								}
								
								replacedKey = StringUtils.rightPad(exprKey, lineWidth); 
								replacedWord = StringUtils.rightPad(replacedWord, lineWidth);
								
								replacementMap.put(replacedKey, replacedWord);							
							}
						}
					}
					
					Set<String> keys = replacementMap.keySet();
					
					for (String key : keys) {
						String value = replacementMap.get(key);					
						line = line.replaceAll(key, value);
						LOG.info(line);
					}				
					
					exprContent.append(line).append("\n");
				}
			}
		}		

		return endIndex;
	}
	
	private String evaluateValue(String variable, TemplateData data)
	{
		if(variable == null)
		{
			return null;
		}
		
		// Check for %KEY% pattern
		Pattern pattern = Pattern.compile(VARIABLE_REGEX);		
		Matcher matcher = pattern.matcher(variable);
		
		if(matcher.find())
		{
			String key = matcher.group(1);
			
			variable = data.get(key);
			
			if(variable == null)
			{
				variable = data.getDefaultValue(key);
			}
		}
		
		return variable;
	}
	
	private boolean compare(String lhs, String rhs, String operator, TemplateData data)
	{
		String lhsValue = evaluateValue(lhs, data);
		String rhsValue = evaluateValue(rhs, data);
		
		boolean result = false;
		
		if("=".equals(operator))
		{
			result = StringUtils.equals(lhsValue, rhsValue);
		}
		else if(">".equals(operator))
		{
			if(NumberUtils.isNumber(lhsValue) && NumberUtils.isNumber(rhsValue))
			{
				result = (NumberUtils.toInt(lhsValue) > NumberUtils.toInt(rhsValue));
			}
			else
			{
				result = false;
			}
		}
		else if("<".equals(operator))
		{
			if(NumberUtils.isNumber(lhsValue) && NumberUtils.isNumber(rhsValue))
			{
				result = (NumberUtils.toInt(lhsValue) < NumberUtils.toInt(rhsValue));
			}
			else
			{
				result = false;
			}
		}
		
		return result;
	}
}