package com.aeris.service.operatoradmin.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.dao.IZoneSetDAO;
import com.aeris.service.operatoradmin.enums.IRESEnum;
import com.aeris.service.operatoradmin.exception.DuplicateZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetNotFoundException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.ZoneSet;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateZoneSetRequest;
import com.aeris.service.operatoradmin.payload.UpdateZoneSetRequest;
import com.aeris.service.operatoradmin.utils.Constants;

@Path("/operators/{operatorId}/zoneSets")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class ZoneSetResource {
	private static final Logger LOG = LoggerFactory.getLogger(ZoneResource.class);
	
	@Inject
	private IZoneSetDAO zoneSetDAO;
	
	@Inject
	private IZoneDAO zoneDAO;

	@Inject
	private IAccountDAO accountDAO;
	
	@Inject
	private IRESEnum resCache;
	/**
	 * If accountId is passed , this method will return all the customZones for the specified account, product & operator. 
	 * else it will return the standard zones of the given product & operator.
	 *
	 * @param operatorId
	 * @param productId
	 * @param accountId
	 * @return A list of matching ZoneSets
	 */
	@GET 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"}) 
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllZoneSets(@PathParam("operatorId") @ValidOperator final String operatorId,
			@QueryParam("productId") @Pattern(regexp = "[0-9]+", message = "{productId.number}") final String productId,
			@QueryParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId){
		
		LOG.info("getAllZoneSets : " + "operatorId="+operatorId+", productId="+productId+", accountId="+accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		List<ZoneSet> zoneSets = null;
		try {
			if(NumberUtils.isNumber(productId)){
				if(NumberUtils.isNumber(accountId)){
					LOG.info("Fetching ZoneSets by Operator Id, Product ID & Account Id...");
					zoneSets = zoneSetDAO.getZoneSets(Integer.parseInt(operatorId), Integer.parseInt(productId), Integer.parseInt(accountId));
				}else{
					LOG.info("Fetching ZoneSets by Operator Id & Product ID ..");
					zoneSets = zoneSetDAO.getZoneSets(Integer.parseInt(operatorId), Integer.parseInt(productId));
				}
			}else{
				LOG.info("Fetching ZoneSets by Operator Id...");
				zoneSets = zoneSetDAO.getAllZoneSets(Integer.parseInt(operatorId));
			}
			
			if (zoneSets != null) {
				LOG.info("Fetched "+zoneSets.size()+" zoneSets successfully...");

				ObjectMapper mapper = new ObjectMapper();
				String zonesJson = mapper.writeValueAsString(zoneSets);
				builder.entity(zonesJson);
			} else {
				LOG.info("No zoneSets found...");
				return Response.status(Status.NOT_FOUND).entity("No zoneSet found").build();
			}
		}catch (ZoneSetException e) {
			LOG.error("ZoneSetException occured : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}catch (Exception e){
			LOG.error("Exception occured in getAllZoneSets : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting zoneSets").build();
		}
		
		Response response = builder.build();
		LOG.debug("Returning response from getAllZoneSets: " + response);
		return response;
	}
	/**
	 * This method will return all the customZones for the specified retail accounts, applicable products for parent wholesale account & operator. 
	 * 
	 *
	 * @param operatorId
	 * @param productIds List of products applicable for parent wholesale account
	 * @param accountIds  List of retail accounts
	 * @return A list of matching ZoneSets
	 */
	@Path("/retail")
	@GET 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"}) 
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRetailZoneSets(@PathParam("operatorId") @ValidOperator final String operatorId,
			@QueryParam("productIds") @Pattern(regexp = "(\\d+)(,\\s*\\d+)*", message = "{productIds.pattern}") final String productIds,
			@QueryParam("wholesaleAccountId") final String wholesaleAccountId){
		
		LOG.info("getRetailZoneSets : " + "operatorId="+operatorId+", productIds="+productIds+", wholesaleAccountId="+wholesaleAccountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<String> accountIdList = new ArrayList<String>();
		List<Account> retailAccounts = accountDAO.getRetailAccounts(operatorId, null, null, wholesaleAccountId);
		for(Account account: retailAccounts){
			accountIdList.add(String.valueOf(account.getAccountId()));
		}
		List<String> productIdList = null;
		productIdList = idsToList(productIds);
		List<ZoneSet> zoneSets = null;
		try {
				LOG.info("Fetching ZoneSets by Operator Id, Product ID & Account Id...");
				zoneSets = zoneSetDAO.getZoneSets(Integer.parseInt(operatorId), productIdList, accountIdList);
	 
			 
			if (zoneSets != null) {
				LOG.info("Fetched "+zoneSets.size()+" zoneSets successfully...");

				ObjectMapper mapper = new ObjectMapper();
				String zonesJson = mapper.writeValueAsString(zoneSets);
				builder.entity(zonesJson);
			} else {
				LOG.info("No zoneSets found...");
				return Response.status(Status.NOT_FOUND).entity("No zoneSet found").build();
			}
		}catch (ZoneSetException e) {
			LOG.error("ZoneSetException occured : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}catch (Exception e){
			LOG.error("Exception occured in getAllZoneSets : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting retail zoneSets").build();
		}
		
		Response response = builder.build();
		LOG.debug("Returning response from getRetailZoneSets: " + response);
		return response;
	}
	/**
	 * Converts comma seperated account ids to list of account ids 
	 * 
	 * @param accountIds the comma seperated account ids
	 * 
	 * @return list of account ids
	 */
	private List<String> idsToList(final String ids) {
		List<String> idList = new ArrayList<String>();
		boolean idsNotEmpty = StringUtils.isNotEmpty(ids);

		if (idsNotEmpty) {
			Collections.addAll(idList, ids.split(",\\s*"));
		}

		return idList;
	}
	/**
	 * Return the ZoneSet details for the given ZoneSetId
	 * @param operatorId
	 * @param zoneSetId
	 * @return ZoneSet details for the given ZoneSetId
	 */
	@Path("/{zoneSetId}")
	@GET 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"}) 
	@Produces(MediaType.APPLICATION_JSON)
	public Response getZoneSet(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("zoneSetId") @Pattern(regexp = "[0-9]+", message = "{zoneSetId.number}") final String zoneSetId){
		LOG.info("getZoneSet : " + "operatorId="+operatorId+", zoneSetId="+zoneSetId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		ZoneSet zoneSet = null;
		
		try {
			zoneSet = zoneSetDAO.getZoneSet(Integer.parseInt(operatorId), Integer.parseInt(zoneSetId));
		
			if (zoneSet != null) {
				LOG.info("Fetched zoneSet successfully...");

				ObjectMapper mapper = new ObjectMapper();
				String zoneSetJson = mapper.writeValueAsString(zoneSet);
				builder.entity(zoneSetJson);
			} else {
				LOG.info("No zoneSet found...");
				return Response.status(Status.NOT_FOUND).entity("No zoneSet found").build();
			}
		}catch (ZoneSetException e) {
			LOG.error("ZoneSetException occured : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}catch (Exception e){
			LOG.error("Exception occured in getAllZoneSets : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting zoneSet").build();
		}
		
		Response response = builder.build();
		LOG.debug("Returning response from getZoneSet: " + response);
		return response;
	}
	
	/**
	 * Creates a new ZoneSet
	 * @param operatorId
	 * @param request
	 * @return details of news created ZoneSet
	 */
	@POST 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createNewZoneSet(@PathParam("operatorId") @ValidOperator final String operatorId, @Valid CreateZoneSetRequest request) {
		LOG.info("Received request to create new zoneSet under operator: " + operatorId);
		
		//For AT&T product, RES ID is mandatory
		String prodId = request.getProductId();
		if(prodId.equals(Constants.ATT_PRODUCT_DUAL_MODE_A_LH)){
			if (request.getResId() == 0) {
				return Response.status(Status.BAD_REQUEST).entity("RES Id must be provided").build();
			}else{
				boolean isValid = resCache.isValidRES(Integer.valueOf(prodId),request.getResId());
				if(!isValid){
					return Response.status(Status.BAD_REQUEST).entity("Invalid RES for the given zoneset").build();
				}
			}
		}
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		ZoneSet zoneSet = null;
		
		int accountId = 0;
		if(NumberUtils.isNumber(request.getAccountId()))
			accountId = Integer.parseInt(request.getAccountId());
		
		try {
			zoneSet = 	zoneSetDAO.createNewZoneSet(Integer.parseInt(operatorId), Integer.parseInt(request.getProductId()), accountId, request.getZoneSetName(), 
						new Date(), request.getUserId(), request.getZones(), request.getResId());
			
			ObjectMapper mapper = new ObjectMapper();
			String zoneSetJson = mapper.writeValueAsString(zoneSet);
			builder.entity(zoneSetJson);
			
		}catch (DuplicateZoneSetException e) {
			LOG.error("DuplicateZoneSetException occured in createNewZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("ZoneSet Name already Exists.").build();
		}catch (ZoneSetException e) {
			LOG.error("ZoneSetException occured in createNewZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}catch (Exception e) {
			LOG.error("Exception occured in createNewZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while creating zoneSet").build();
		}
		
		Response response = builder.build();
		LOG.debug("Sent response from createNewZoneSet: " + response);
		return response;
	}
	
	/**
	 * Updated the given ZoneSet
	 * @param operatorId
	 * @param zoneSetId
	 * @param request
	 * @return details of the updated ZoneSet
	 */
	@Path("/{zoneSetId}")
	@PUT 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateZoneSet(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("zoneSetId") @Pattern(regexp = "[0-9]+", message = "{zoneSetId.number}") final String zoneSetId,
			@Valid UpdateZoneSetRequest request) {
		LOG.info("Received request to update zoneSet with id: " + zoneSetId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		ZoneSet zoneSet = null;
		
		int accountId = 0;
		if(NumberUtils.isNumber(request.getAccountId()))
			accountId = Integer.parseInt(request.getAccountId());
		
		try {
			zoneSet = zoneSetDAO.updateZoneSet(Integer.parseInt(zoneSetId), Integer.parseInt(operatorId), Integer.parseInt(request.getProductId()), accountId, request.getZoneSetName(), new Date(), request.getUserId());
			
			ObjectMapper mapper = new ObjectMapper();
			String zoneSetJson = mapper.writeValueAsString(zoneSet);
			builder.entity(zoneSetJson);
			
		}catch (DuplicateZoneSetException e) {
			LOG.error("DuplicateZoneSetException occured in updateZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("ZoneSet Name already Exists.").build();
		}catch (ZoneSetException e) {
			LOG.error("ZoneSetException occured in updateZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}catch (Exception e) {
			LOG.error("Exception occured in createNewZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while updating zoneSet").build();
		}
		
		Response response = builder.build();
		LOG.debug("Sent response from updateZoneSet: " + response);
		return response;
	}
	
	/**
	 * delete the specified ZoneSet
	 * @param operatorId
	 * @param zoneSetId
	 * @param userId
	 * @return 
	 */
	@DELETE 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	@Path("/{zoneSetId}")
	public Response deleteZoneSet(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("zoneSetId") @Pattern(regexp = "[0-9]+", message = "{zoneSetId.number}") final String zoneSetId,
			@QueryParam("userId") @Email(message = "{userId.email}") final String userId) {
		LOG.info("Received request to delete zoneSet with id: " + zoneSetId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		try {
			boolean success = zoneSetDAO.deleteZoneSet(Integer.parseInt(operatorId), Integer.parseInt(zoneSetId), new Date(), userId);
			
			if (!success) {
				LOG.info("deleteZoneSet failed for zoneSetId: " + zoneSetId);
				return Response.status(Status.NOT_FOUND).entity("deleteZoneSet failed for zoneSetId: " + zoneSetId).build();
			} else {
				LOG.info("ZoneSet with zoneSet id: " + zoneSetId + " deleted successfully");

				ObjectMapper mapper = new ObjectMapper();
				String deleteJson = mapper.writeValueAsString(success);
				builder.entity(deleteJson);
			}
		}catch (ZoneSetException e) {
			LOG.error("ZoneSetException occured in deleteZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}catch (ZoneSetNotFoundException e) {
			LOG.error("ZoneSetNotFoundException occured in deleteZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}catch (Exception e) {
			LOG.error("Exception occured in deleteZoneSet : "+e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while deleting zoneSet").build();
		}
		
		Response response = builder.build();

		LOG.debug("Sent response from deleteZoneSet: " + response);
		return response;
	}
}
