package com.aeris.service.operatoradmin.client;

import java.io.Serializable;

import org.apache.http.HttpStatus;

public class AerCloudResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	private int responseCode;
	private String responseMessage;

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	public boolean isOK()
	{
		return (responseCode == HttpStatus.SC_OK);
	}
}
