package com.aeris.service.operatoradmin.resources;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IRatePlanPoolDAO;
import com.aeris.service.operatoradmin.exception.BadStartDateException;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanPoolException;
import com.aeris.service.operatoradmin.exception.InvalidRatePlanPoolException;
import com.aeris.service.operatoradmin.exception.RatePlanPoolException;
import com.aeris.service.operatoradmin.exception.RatePlanPoolNotFoundException;
import com.aeris.service.operatoradmin.model.AccountRatePlanPool;
import com.aeris.service.operatoradmin.model.RatePlanPoolStatus;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateApiKeyRequest;
import com.aeris.service.operatoradmin.payload.CreateRatePlanPoolRequest;
import com.aeris.service.operatoradmin.payload.UpdateAccountRequest;
import com.aeris.service.operatoradmin.payload.UpdateRatePlanPoolRequest;
import com.aeris.service.operatoradmin.payload.UpdateRatePlanPoolStatusRequest;

/**
 * Rest Resource for Rate Plan management. Includes methods for
 * 
 * <ul>
 * <li>
 * Create a new Rate Plan
 * </li>
 * <li>
 * Get Rate Plans assigned to an account
 * </li>
 * <li>
 * Assign Rate Plan to an account
 * </li>
 * <li>
 * Expire Rate Plan assigned to an account
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}/accounts")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class RatePlanPoolResource {
	private static final Logger LOG = LoggerFactory.getLogger(RatePlanPoolResource.class);

	@Inject
	private IRatePlanPoolDAO ratePlanPoolDAO;

	
	/**
	 * <p>
	 * Fetches List of Account details per operator.
	 * Allows to filter the Accounts based of status or list of specific account ids.
	 * </p>
	 * 
	 * @param operatorId The Operator Id
	 * @param status The Status
	 * @param start The start index for pagination
	 * @param count The record count for pagination
	 * @param accountIds The list of specific account ids to be fetched 
	 * @param extendAttributes true if the api sends extended account information to the client
	 * 
	 * @return The List of Accounts
	 */
	@GET
	@Path("/{accountId}/rateplanpools")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
	public Response getAccountRatePlanPools(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@QueryParam("status") @DefaultValue("all") final String status, @QueryParam("start") final String start,
			@QueryParam("count") final String count,
			@QueryParam("extendAttributes") final boolean extendAttributes) {
		LOG.info("Received request to fetch all rate plan pools for operatorId: " + operatorId+" assigned to account: "+accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		List<AccountRatePlanPool> ratePlanPools = null;

		try {
			if (paginated) {
				LOG.info("Returning a max record size of " + count + " all assigned rate plan pools starting from " + start + " for accountId: " + accountId);
			} else {
				LOG.info("Query is not paginated, returning all assigned rate plan pools for accountId: " + accountId);
			}

			// Fetch orders with any status
			if (!RatePlanPoolStatus.isValid(status)) {
				// Fetch from database
				ratePlanPools = ratePlanPoolDAO.getAccountRatePlanPools(Integer.parseInt(operatorId), Integer.parseInt(accountId), start, count);
			}
			// Fetch orders with specific status like Open, In Progress,
			// Completed etc.
			else {
				// Fetch from database
				ratePlanPools = ratePlanPoolDAO.getAccountRatePlanPools(Integer.parseInt(operatorId), Integer.parseInt(accountId), start, count, RatePlanPoolStatus.fromName(status));
			}

			if (ratePlanPools != null) {
				LOG.info("fetched rate plan pools successfully, no of assigned rate plan pools retreived : " + ratePlanPools.size());

				ObjectMapper mapper = new ObjectMapper();
				String ratePlansJson = mapper.writeValueAsString(ratePlanPools);

				builder.entity(ratePlansJson);
			} else {
				LOG.info("No rate plan pools for account : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No rate plan pools found for the account").build();
			}
		} catch (Exception e) {
			LOG.error("getAssignedRatePlanPools Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting assigned rate plan pools").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAssignedRatePlanPools: " + response);
		return response;
	}

	/**
	 * Fetch a specific account identified by account id
	 * 
	 * @param operatorId the operator id
	 * @param accountId the account id
	 * 
	 * @return The Account details
	 */
	@GET
	@Path("/{accountId}/rateplanpools/{ratePlanPoolId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
	public Response getAccountRatePlanPool(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("ratePlanPoolId") @Pattern(regexp = "[0-9]+", message = "{ratePlanPoolId.number}") final String ratePlanPoolId) {
		LOG.info("Received request to fetch RatePlanPool for ratePlanPoolId: " + ratePlanPoolId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			// Fetch from database
			AccountRatePlanPool ratePlanPool = ratePlanPoolDAO.getAccountRatePlanPool(Integer.parseInt(operatorId), Long.parseLong(accountId), Integer.parseInt(ratePlanPoolId));

			if (ratePlanPool != null) {
				LOG.info("fetched ratePlanPool successfully for ratePlanId: " + ratePlanPoolId);

				ObjectMapper mapper = new ObjectMapper();
				String accountJson = mapper.writeValueAsString(ratePlanPool);

				builder.entity(accountJson);
			} else {
				LOG.info("No RatePlanPool found for ratePlanPoolId: " + ratePlanPoolId);
				return Response.status(Status.NOT_FOUND).entity("No RatePlanPool found for ratePlanPoolId: " + ratePlanPoolId).build();
			}
		} catch (Exception e) {
			LOG.error("getRatePlanPool Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting ratePlanPool").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getRatePlanPool: " + response);
		return response;
	}

	/**
	 * Create a new account for an operator based on the information 
	 * specified in the {@link CreateApiKeyRequest} by the client
	 * 
	 * The request accepts the user credentials, account name, 
	 * contacts information, billing details and list of products enrolled for this account
	 * 
	 * @param operatorId the operator id
	 * @param request the create account request sent by the client
	 * 
	 * @return the newly created account information
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{accountId}/rateplanpools")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response createNewRatePlanPool(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@Valid CreateRatePlanPoolRequest request) {

		LOG.info("Received request to create new rate plan pool under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String ratePlanPoolName = request.getRatePlanPoolName();			
		Date startDate = request.getStartDate();		
		Date endDate = request.getEndDate();
		int productId = Integer.parseInt(request.getProductId());
		List<String> ratePlans = request.getRatePlans();
		RatePlanPoolStatus ratePlanPoolStatus = request.getStatus();		
		String userId = request.getUserId();
		if (StringUtils.isBlank(userId)) {
			return Response.status(Status.BAD_REQUEST).entity("userId must be provided").build();
		}
		Date requestedDate = new Date();

		try {
			LOG.info("Calling RatePlanPoolDAO");

			AccountRatePlanPool ratePlanPool = ratePlanPoolDAO.createNewAccountRatePlanPool(Integer.parseInt(operatorId), Long.parseLong(accountId), productId, ratePlanPoolName, 
					ratePlans, startDate, endDate, ratePlanPoolStatus, requestedDate, userId, request.getRatePlanPoolOverageBucket());

			ObjectMapper mapper = new ObjectMapper();
			String ratePlanPoolJson = mapper.writeValueAsString(ratePlanPool);
			builder.entity(ratePlanPoolJson);

			LOG.info("Created RatePlanPool Response: " + ratePlanPoolJson);
		} catch (DuplicateRatePlanPoolException e) {   
			LOG.error("DuplicateRatePlanPoolException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (InvalidRatePlanPoolException e) {   
			LOG.error("InvalidRatePlanPoolException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (RatePlanPoolException e) {
			LOG.error("RatePlanPoolException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("createNewRatePlanPool Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while creating rate plan pool").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewRatePlanPool: " + response);
		return response;
	}

	/**
	 * Update an exisiting account for an operator based on the information 
	 * specified in the {@link UpdateAccountRequest} by the client
	 * 
	 * The request accepts the account name, contacts information, billing details 
	 * and list of products enrolled for this account
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param request The update account requested by the user client
	 * 
	 * @return The Updated Account serialized back to the client
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{accountId}/rateplanpools/{ratePlanPoolId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateRatePlanPool(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("ratePlanPoolId") @Pattern(regexp = "[0-9]+", message = "{ratePlanPoolId.number}") final String ratePlanPoolId,
			@Valid UpdateRatePlanPoolRequest request) {

		LOG.info("Received request to update rate plan pool with ratePlanPoolId: " + ratePlanPoolId + " under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String ratePlanPoolName = request.getRatePlanPoolName();			
		Date startDate = request.getStartDate();		
		Date endDate = request.getEndDate();
		int productId = Integer.parseInt(request.getProductId());
		List<String> ratePlans = request.getRatePlans();
		RatePlanPoolStatus ratePlanPoolStatus = request.getStatus();		
		String userId = request.getUserId();
		if (StringUtils.isBlank(userId)) {
			return Response.status(Status.BAD_REQUEST).entity("userId must be provided").build();
		}
		Date requestedDate = new Date();

		try {
			LOG.info("Calling ratePlanPoolDAO.updateRatePlanPool()");
			
			AccountRatePlanPool ratePlanPool = ratePlanPoolDAO.updateAccountRatePlanPool(Integer.parseInt(operatorId), Long.parseLong(accountId), 
					Integer.parseInt(ratePlanPoolId), productId, ratePlanPoolName, ratePlans, startDate, endDate, ratePlanPoolStatus, requestedDate, userId, request.getRatePlanPoolOverageBucket());

			ObjectMapper mapper = new ObjectMapper();

			String ratePlanJson = mapper.writeValueAsString(ratePlanPool);
			builder.entity(ratePlanJson);

			LOG.info("Update Rate Plan Pool Response: " + ratePlanJson);
		} catch (RatePlanPoolNotFoundException e) {
			LOG.error("RatePlanPoolNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (RatePlanPoolException e) {   
			LOG.error("RatePlanPoolException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (BadStartDateException e) {
			LOG.error("BadStartDateException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (DuplicateRatePlanPoolException e) {   
			LOG.error("DuplicateRatePlanPoolException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (InvalidRatePlanPoolException e) {   
			LOG.error("InvalidRatePlanPoolException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateRatePlanPool Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while updating rate plan pool").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateRatePlan: " + response);
		return response;
	}

	/**
	 * Delete an existing account identified by account id and request authorized user
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param userId The user id of the user who has sent the request  
	 * 
	 * @return true if the delete is success otherwise false
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{accountId}/rateplanpools/{ratePlanPoolId}/status")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateRatePlanPoolStatus(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("ratePlanPoolId") @Pattern(regexp = "[0-9]+", message = "{ratePlanPoolId.number}") final String ratePlanPoolId,
			@Valid UpdateRatePlanPoolStatusRequest request) {
		LOG.info("Received request to update rate plan pool status with ratePlanPoolId: " + ratePlanPoolId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Date requestedDate = new Date();
		String userId = request.getUserId();
		RatePlanPoolStatus status = request.getStatus();
		if (StringUtils.isBlank(userId)) {
			return Response.status(Status.BAD_REQUEST).entity("userId must be provided").build();
		}
		try {
			// Fetch from database
			boolean success = ratePlanPoolDAO.updateRatePlanPoolStatus(Integer.parseInt(operatorId), Long.parseLong(accountId), Integer.parseInt(ratePlanPoolId), 
					status, requestedDate, userId);

			if (!success) {
				LOG.info("updateRatePlanPoolStatus failed for rate plan pool id: " + ratePlanPoolId);
				return Response.status(Status.NOT_FOUND).entity("updateRatePlanPoolStatus failed for rate plan pool id: " + ratePlanPoolId).build();
			} else {
				LOG.info("Rate plan pool with rate plan pool id: " + ratePlanPoolId + " updated successfully to status: "+status+" for account id: "+accountId);

				ObjectMapper mapper = new ObjectMapper();
				String approveJson = mapper.writeValueAsString(success);

				builder.entity(approveJson);
			}
		} catch (RatePlanPoolNotFoundException e) {
			LOG.error("RatePlanPoolNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (RatePlanPoolException e) {
			LOG.error("RatePlanPoolNotFoundException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("deleteRatePlanPool Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while deleting rate plan pool").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateRatePlanPoolStatus: " + response);
		return response;
	}
	
	/**
	 * Delete an existing account identified by account id and request authorized user
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param userId The user id of the user who has sent the request
	 * 
	 * @return true if the delete is success otherwise false
	 */
	@DELETE
	@Path("/{accountId}/rateplanpools/{ratePlanPoolId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response invalidateRatePlanPool(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("ratePlanPoolId") @Pattern(regexp = "[0-9]+", message = "{ratePlanPoolId.number}") final String ratePlanPoolId,
			@QueryParam("userId") @Email(message = "{userId.email}") final String userId) {
		LOG.info("Received request to delete rate plan pool with ratePlanPoolId: " + ratePlanPoolId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Date requestedDate = new Date();
		if (StringUtils.isBlank(userId)) {
			return Response.status(Status.BAD_REQUEST).entity("userId must be provided").build();
		}
		try {
			// Fetch from database
			boolean success = ratePlanPoolDAO.invalidateRatePlanPool(Integer.parseInt(operatorId), Long.parseLong(accountId), Integer.parseInt(ratePlanPoolId), 
					requestedDate, userId);

			if (!success) {
				LOG.info("deleteRatePlan failed for rate plan pool id: " + ratePlanPoolId);
				return Response.status(Status.NOT_FOUND).entity("deleteRatePlan failed for rate plan pool id: " + ratePlanPoolId).build();
			} else {
				LOG.info("Rate plan pool with rate plan pool id: " + ratePlanPoolId + " deleted successfully");

				ObjectMapper mapper = new ObjectMapper();
				String deleteJson = mapper.writeValueAsString(success);

				builder.entity(deleteJson);
			}
		} catch (RatePlanPoolNotFoundException e) {
			LOG.error("RatePlanPoolNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (RatePlanPoolException e) {
			LOG.error("RatePlanPoolNotFoundException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("deleteRatePlanPool Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while deleting rate plan pool").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from deleteRatePlanPool: " + response);
		return response;
	}
}
