package com.aeris.service.operatoradmin.model;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public class ApplicationProperties {
	private static Logger LOG = LoggerFactory.getLogger(ApplicationProperties.class);
	
	private String aercloudUrl;
	private String aercloudAdminKey;
	private String defaultDataModelName;
	private String defaultDataModelId;
	private String defaultContainerId;
	private String myAlertsDataModelName;
	private String myAlertsDataModelId;
	private String myAlertsContainerId;
	private String aerEventStreamDataModelName;
	private String aerEventStreamDataModelId;
	private String aerEventStreamContainerId;
	private String myAlertsAppSclId;
	private String myAlertsTestDeviceSclId;
	private String aeradminUrl; 
	
	private String aercloudAccountUrl;
	private String aercloudDataModelUrl;
	private String aercloudContainerUrl;
	private String aercloudDevicePersistUrl;
	private String aeradminDeviceProvisionUrl;

	private String deleteAercloudContainerUrl;
	private String deleteAercloudDataModelUrl;
	private String deleteAercloudAccountUrl;

	private String fetchAercloudAccountUrl;
	
	private String enableAuth ;
	private String platformAdminKey ;
    private String as4gUrl;
    private boolean ratePlanFilterByOperatorEnabled;
	
	@Inject
	public ApplicationProperties(@Named("aercloud.url") String aercloudUrl, 
			@Named("aeradmin.url") String aeradminUrl,
			@Named("aercloud.adminkey") String aercloudAdminKey,
			@Named("default.data.model.name") String defaultDataModelName,
			@Named("default.data.model.id") String defaultDataModelId,
			@Named("default.container.id") String defaultContainerId,
			@Named("myalerts.data.model.name") String myAlertsDataModelName,
			@Named("myalerts.data.model.id") String myAlertsDataModelId,
			@Named("myalerts.container.id") String myAlertsContainerId,
			@Named("aereventstream.data.model.name") String aerEventStreamDataModelName,
			@Named("aereventstream.data.model.id") String aerEventStreamDataModelId,
			@Named("aereventstream.container.id") String aerEventStreamContainerId,
			@Named("myalerts.app.scl.id") String myAlertsAppSclId,
			@Named("myalerts.testdevice.scl.id") String myAlertsTestDeviceSclId,
			@Named("operator.admin.enable.authorization") String enableAuth,
			@Named("platform.admin.secret.key") String platformAdminKey,
            @Named("as4g.url") String as4gUrl,
            @Named("rateplan.filter.by.operator") boolean ratePlanFilterByOperator) {
		this.aercloudUrl = aercloudUrl;
		this.aeradminUrl = aeradminUrl;
		this.aercloudAdminKey = aercloudAdminKey;
		this.defaultDataModelName = defaultDataModelName;
		this.defaultDataModelId = defaultDataModelId;
		this.defaultContainerId = defaultContainerId;
		this.myAlertsDataModelName = myAlertsDataModelName;
		this.myAlertsDataModelId = myAlertsDataModelId;
		this.myAlertsContainerId = myAlertsContainerId;
		this.aerEventStreamDataModelName = aerEventStreamDataModelName;
		this.aerEventStreamDataModelId = aerEventStreamDataModelId;
		this.aerEventStreamContainerId = aerEventStreamContainerId;
		this.myAlertsAppSclId = myAlertsAppSclId;
		this.myAlertsTestDeviceSclId = myAlertsTestDeviceSclId;		
		
		this.aercloudAccountUrl = aercloudUrl;
		this.aercloudDataModelUrl = aercloudUrl + "{accountId}/scls/dataModels";
		this.aercloudContainerUrl = aercloudUrl + "{accountId}/containers";
		this.aercloudDevicePersistUrl = aercloudUrl + "{accountId}/scls";
		this.aeradminDeviceProvisionUrl = aeradminUrl + "/devices/aercloud/provision";
		
		this.deleteAercloudAccountUrl = aercloudUrl + "{accountId}";
		this.deleteAercloudDataModelUrl = aercloudUrl + "{accountId}/scls/dataModels/{dataModelId}";
		this.deleteAercloudContainerUrl = aercloudUrl + "{accountId}/containers/{containerId}";	
		
		this.enableAuth = enableAuth ;
		this.platformAdminKey = platformAdminKey ;
        this.as4gUrl = as4gUrl;
        
        this.ratePlanFilterByOperatorEnabled = ratePlanFilterByOperator;
		
		this.fetchAercloudAccountUrl = aercloudUrl + "{accountId}";
		
		LOG.info("aercloud properties: "+ReflectionToStringBuilder.toString(this));
	}

	public String getAercloudUrl() {
		return aercloudUrl;
	}

	public void setAercloudUrl(String aercloudUrl) {
		this.aercloudUrl = aercloudUrl;
	}

	public String getAercloudAdminKey() {
		return aercloudAdminKey;
	}

	public void setAercloudAdminKey(String aercloudAdminKey) {
		this.aercloudAdminKey = aercloudAdminKey;
	}

	public String getDefaultDataModelName() {
		return defaultDataModelName;
	}

	public void setDefaultDataModelName(String defaultDataModelName) {
		this.defaultDataModelName = defaultDataModelName;
	}

	public String getDefaultDataModelId() {
		return defaultDataModelId;
	}

	public void setDefaultDataModelId(String defaultDataModelId) {
		this.defaultDataModelId = defaultDataModelId;
	}

	public String getDefaultContainerId() {
		return defaultContainerId;
	}

	public void setDefaultContainerId(String defaultContainerId) {
		this.defaultContainerId = defaultContainerId;
	}

	public String getMyAlertsDataModelName() {
		return myAlertsDataModelName;
	}

	public void setMyAlertsDataModelName(String myAlertsDataModelName) {
		this.myAlertsDataModelName = myAlertsDataModelName;
	}

	public String getMyAlertsDataModelId() {
		return myAlertsDataModelId;
	}

	public void setMyAlertsDataModelId(String myAlertsDataModelId) {
		this.myAlertsDataModelId = myAlertsDataModelId;
	}

	public String getMyAlertsContainerId() {
		return myAlertsContainerId;
	}

	public void setMyAlertsContainerId(String myAlertsContainerId) {
		this.myAlertsContainerId = myAlertsContainerId;
	}
	
	public String getAerEventStreamDataModelName() {
		return aerEventStreamDataModelName;
	}
	
	public void setAerEventStreamDataModelName(String aerEventStreamDataModelName) {
		this.aerEventStreamDataModelName = aerEventStreamDataModelName;
	}
	
	public String getAerEventStreamDataModelId() {
		return aerEventStreamDataModelId;
	}
	
	public void setAerEventStreamDataModelId(String aerEventStreamDataModelId) {
		this.aerEventStreamDataModelId = aerEventStreamDataModelId;
	}
	
	public String getAerEventStreamContainerId() {
		return aerEventStreamContainerId;
	}
	
	public void setAerEventStreamContainerId(String aerEventStreamContainerId) {
		this.aerEventStreamContainerId = aerEventStreamContainerId;
	}

	public String getMyAlertsAppSclId() {
		return myAlertsAppSclId;
	}

	public void setMyAlertsAppSclId(String myAlertsAppSclId) {
		this.myAlertsAppSclId = myAlertsAppSclId;
	}

	public String getMyAlertsTestDeviceSclId() {
		return myAlertsTestDeviceSclId;
	}

	public void setMyAlertsTestDeviceSclId(String myAlertsTestDeviceSclId) {
		this.myAlertsTestDeviceSclId = myAlertsTestDeviceSclId;
	}

	public String getAeradminUrl() {
		return aeradminUrl;
	}

	public void setAeradminUrl(String aeradminUrl) {
		this.aeradminUrl = aeradminUrl;
	}

	public String getAercloudAccountUrl() {
		return aercloudAccountUrl;
	}

	public void setAercloudAccountUrl(String aercloudAccountUrl) {
		this.aercloudAccountUrl = aercloudAccountUrl;
	}

	public String getAercloudDataModelUrl() {
		return aercloudDataModelUrl;
	}

	public void setAercloudDataModelUrl(String aercloudDataModelUrl) {
		this.aercloudDataModelUrl = aercloudDataModelUrl;
	}

	public String getAercloudContainerUrl() {
		return aercloudContainerUrl;
	}

	public void setAercloudContainerUrl(String aercloudContainerUrl) {
		this.aercloudContainerUrl = aercloudContainerUrl;
	}

	public String getAercloudDevicePersistUrl() {
		return aercloudDevicePersistUrl;
	}

	public void setAercloudDevicePersistUrl(String aercloudDevicePersistUrl) {
		this.aercloudDevicePersistUrl = aercloudDevicePersistUrl;
	}

	public String getAeradminDeviceProvisionUrl() {
		return aeradminDeviceProvisionUrl;
	}

	public void setAeradminDeviceProvisionUrl(String aeradminDeviceProvisionUrl) {
		this.aeradminDeviceProvisionUrl = aeradminDeviceProvisionUrl;
	}

	public String getDeleteAercloudContainerUrl() {
		return deleteAercloudContainerUrl;
	}

	public void setDeleteAercloudContainerUrl(String deleteAercloudContainerUrl) {
		this.deleteAercloudContainerUrl = deleteAercloudContainerUrl;
	}

	public String getDeleteAercloudDataModelUrl() {
		return deleteAercloudDataModelUrl;
	}

	public void setDeleteAercloudDataModelUrl(String deleteAercloudDataModelUrl) {
		this.deleteAercloudDataModelUrl = deleteAercloudDataModelUrl;
	}

	public String getDeleteAercloudAccountUrl() {
		return deleteAercloudAccountUrl;
	}

	public void setDeleteAercloudAccountUrl(String deleteAercloudAccountUrl) {
		this.deleteAercloudAccountUrl = deleteAercloudAccountUrl;
	}

	public String getFetchAercloudAccountUrl() {
		return fetchAercloudAccountUrl;
	}
	
	public void setFetchAercloudAccountUrl(String fetchAercloudAccountUrl) {
		this.fetchAercloudAccountUrl = fetchAercloudAccountUrl; 
	}

	public String getEnableAuth() {
		return enableAuth;
	}

	public String getPlatformAdminKey() {
		return platformAdminKey;
	}

    public String getAs4gUrl() {
        return as4gUrl;
    }

    public void setAs4gUrl(String as4gUrl) {
        this.as4gUrl = as4gUrl;
    }

    public boolean isRatePlanFilterByOperatorEnabled() {
        return ratePlanFilterByOperatorEnabled;
    }

    public void setRatePlanFilterByOperatorEnabled(boolean ratePlanFilterByOperatorEnabled) {
        this.ratePlanFilterByOperatorEnabled = ratePlanFilterByOperatorEnabled;
    }
    
}
