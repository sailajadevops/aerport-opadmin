package com.aeris.service.operatoradmin;


import static com.aeris.service.common.db.MBeanService.AERIS_JMX_QUALIFIER;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.CacheProvider;
import com.aeris.service.common.db.MBeanService;
import com.aeris.service.operatoradmin.guice.RestServletModule;
import com.aeris.service.operatoradmin.guice.ServiceModule;
import com.google.common.base.Throwables;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * {@link OperatorAdminListener} class to initialize Operator Admin webservice.
 *
 */
public class OperatorAdminListener implements ServletContextListener {

    private static final Logger LOG = LoggerFactory.getLogger(OperatorAdminListener.class);
    public static final String CONTEXT_NAME="operator-admin-ws";
    public static Injector sInjector;
    

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
        	sInjector = Guice.createInjector(new RestServletModule(),new ServiceModule());
        } catch (Exception ex) {
            LOG.error("Error in creating Guice Injector. " + ex);
            Throwables.propagate(ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {  
    	CacheProvider.shutdown();
        unregisterAerAdminMBean(servletContextEvent.getServletContext());
    }

    public void unregisterAerAdminMBean(ServletContext servletContext) {
        try {
            ObjectName objAerAdminDBManager;
            objAerAdminDBManager = new ObjectName(AERIS_JMX_QUALIFIER + ":Name=DBPoolManager,Type=*"
                    + servletContext.getServletContextName());
            for (ObjectName objAerAdminDBPoolmgr : MBeanService.getServer().queryNames(objAerAdminDBManager, null)) {
                LOG.info("Unregistering DBPool MBean [{}]", objAerAdminDBPoolmgr);
                MBeanService.getServer().unregisterMBean(objAerAdminDBPoolmgr);
            }
           
        } catch (MalformedObjectNameException e) {
            LOG.error("Error in Object Name query", e);
        } catch (NullPointerException e) {
            LOG.error("Error in Object Name query", e);
        } catch (MBeanRegistrationException e) {
            LOG.error("Unable to unregister OpStatistics", e);
        } catch (InstanceNotFoundException e) {
            LOG.error("Unable to unregister OpStatistics", e);
        }
    }

    public Injector getInjector() {
        return sInjector;
    }
}