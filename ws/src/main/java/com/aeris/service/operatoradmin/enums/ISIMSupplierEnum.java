package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.SIMSupplier;

public interface ISIMSupplierEnum  extends IBaseEnum{
	SIMSupplier getSIMSupplierByName(String name);
	SIMSupplier getSIMSupplierById(int id);
	List<SIMSupplier> getAllSIMSuppliers();
}
