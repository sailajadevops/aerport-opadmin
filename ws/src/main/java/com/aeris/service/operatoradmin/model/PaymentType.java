package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum PaymentType implements Serializable {
	CREDIT_CARD(1, "Credit Card"), DEBIT_CARD(1, "Debit Card");
	int code;
	String value;

	PaymentType(int code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public int getCode() {
		return code;
	}

	public static boolean isValid(String value) {
		PaymentType[] paymentTypes = values();

		for (PaymentType paymentType : paymentTypes) {
			if (paymentType.getValue().equalsIgnoreCase(value)) {
				return true;
			}
		}

		return false;
	}

	public static PaymentType fromValue(String value) {
		PaymentType[] paymentTypes = values();

		for (PaymentType paymentType : paymentTypes) {
			if (paymentType.getValue().equalsIgnoreCase(value)) {
				return paymentType;
			}
		}

		return null;
	}
}
