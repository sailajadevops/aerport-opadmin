package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class Resource implements Serializable {
	private static final long serialVersionUID = -3701838117105711188L;
	private int resourceId;
	private String resourcePath;
	private String resourceType;

	public void setResourceId(int resourceId) {
		this.resourceId = resourceId;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public int getResourceId() {
		return resourceId;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
}
