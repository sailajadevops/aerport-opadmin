package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public class BillingDetails implements Serializable {
	private static final long serialVersionUID = 8761702590743536235L;
	private PaymentType paymentType = PaymentType.CREDIT_CARD;
	private PaymentDetails paymentDetails;
	private BillingAddress billingAddress;

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public PaymentDetails getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(PaymentDetails paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public String validate() {
		StringBuffer errors = new StringBuffer();

		if (paymentDetails == null) {
			errors.append("paymentDetails\n");
		}

		if (billingAddress == null) {
			errors.append("billingAddress\n");
		}

		return errors.toString();
	}
}
