package com.aeris.service.operatoradmin.resources;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.ISalesAgentDAO;
import com.aeris.service.operatoradmin.exception.SalesAgentNotFoundException;
import com.aeris.service.operatoradmin.model.SalesAgent;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.google.common.base.Preconditions;

/**
 * Rest Resource for Sales Agent management. Includes methods for creating a new
 * Sales Agent , retrieving existing Sales Agents.
 * 
 * @author Shital Deshmukh
 */
@Path("/operators/{operatorId}/salesagents")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class SalesAgentMgmtResource {
	private static Logger LOG = LoggerFactory
			.getLogger(SalesAgentMgmtResource.class);

	@Inject
	private ISalesAgentDAO salesAgentDAO;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public SalesAgent createSalesAgent(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@Valid SalesAgent salesAgent) {
		Preconditions.checkNotNull(salesAgent);
		LOG.info("Request {}, Input {}", "createSalesAgent", salesAgent);
		SalesAgent agent = salesAgentDAO.createSalesAgent(salesAgent,operatorId);
		LOG.info("Sales Agent successfully created.");
		return agent;
	}

	@PUT
	@Path("/{agentID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public SalesAgent updateSalesAgent(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("agentID") final String agentId,
			@Valid SalesAgent salesAgent) {
		Preconditions.checkNotNull(salesAgent);
		LOG.info("Request {}, Input {}", "updateSalesAgent", salesAgent);
		SalesAgent agent = salesAgentDAO.updateSalesAgent(salesAgent,operatorId);
		LOG.info("Sales Agent successfully updated...");
		return agent;
	}

	@DELETE
	@Path("/{agentID}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public String deleteSalesAgent(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("agentID") final String agentId,
			@QueryParam("userLogin") String userLogin) {
		Preconditions.checkArgument(StringUtils.isNotBlank(userLogin),
				"User login should not be blank");
		LOG.info("Request {}, Input {} userLogin {}", "deleteSalesAgent",
					agentId, userLogin);
			boolean success = salesAgentDAO
					.deleteSalesAgent(agentId, userLogin, operatorId);
			if (!success) {
				LOG.info("Delete Sales Agent failed for AgentId: {} ", agentId);
				return "Delete Sales Agent failed for AgentId: " + agentId;
			} else {
				LOG.info("Sales Agent with agent id: {} deleted successfully",
						agentId);
				return "Sales Agent with AgentId: "+ agentId +" deleted successfully";
			}
	}

	@GET
	//@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	@PermitAll
	public List<SalesAgent> getAllSalesAgents(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@DefaultValue("false") @QueryParam("activeAgents") boolean activeAgents) {
		LOG.info("Request {}, Input {}", "getAllSalesAgents", "none");
		List<SalesAgent> agents = salesAgentDAO.getAllSalesAgents(operatorId, activeAgents);
		LOG.info("Fetched agents successfully, no of agents: {} ",
				agents.size());
		return agents;
	}

	@GET
	@Path("/{agentID}")
	//@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	@PermitAll
	public SalesAgent getSalesAgent(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("agentID") final String agentId) {
		LOG.info("Request {}, Input {}", "getSalesAgent", agentId);
		SalesAgent agent = salesAgentDAO.getSalesAgent(agentId,operatorId);
		if (agent != null) {
			LOG.info("Found Sales Agent: {}", agent);
			return agent;

		} else {
			LOG.info("Sales Agent not found.");
			throw new SalesAgentNotFoundException("Sales Agent not found.");
		}
	}
}
