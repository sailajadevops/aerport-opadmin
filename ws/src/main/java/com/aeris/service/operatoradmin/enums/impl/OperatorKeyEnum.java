package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IOperatorKeyEnum;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.ApiKey;
import com.aeris.service.operatoradmin.model.Permission;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * API Key Cache used for lookup purposes
 * 
 * @author Saurabh.sharma@aeris.net
 */
@Singleton
public class OperatorKeyEnum implements IOperatorKeyEnum {
	private Logger LOG = LoggerFactory.getLogger(OperatorKeyEnum.class);

	private Cache<String, ApiKey> cache;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "OperatorKeyCache/name") Cache<String, ApiKey> cache) {
		this.cache = cache;	
		loadApiKeys();
	}

	/**
	 * Loads the ApiKeys object into the memory for future lookup.
	 */
	void loadApiKeys() {
		LOG.info("loading operator-key from db");
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		StringBuilder query = new StringBuilder("select oa.OPERATOR_ID , " +
				" wa.APP_ID , " +
				" wa.API_KEY , " +
				" wa.DESCRIPTION , " +
				" wa.EXPIRES , " +
				" wa.STATUS , " +
				" arp.PERMISSION_ID , p.DESCRIPTION as perm_value, " +
				" arp.RESOURCE_ID from AERISGEN.WS_OPERATOR_APPLICATIONS oa , " +
				" aerisgen.WS_APPLICATION wa , aerisgen.WS_PERMISSION p, " +
				" aerisgen.WS_APP_RES_PERMISSIONS arp where wa.APP_ID = oa.APP_ID and wa.APP_ID = arp.APP_ID " +
				" and p.PERMISSION_ID = arp.PERMISSION_ID " +
				" and wa.STATUS = 'ACTIVE' and(" +
				" wa.EXPIRES is null or wa.EXPIRES > SYS_EXTRACT_UTC(SYSTIMESTAMP) " +
				" )");

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				ApiKey apiKey = new ApiKey();
				apiKey.setOperatorId(rs.getLong("operator_id"));
				apiKey.setApiKey(rs.getString("api_key"));
				apiKey.setAppId(rs.getInt("app_id"));
				apiKey.setStatus(rs.getString("status"));
				apiKey.setExpiryDate(rs.getString("expires"));
				List<Permission> permissions = new ArrayList<Permission>();
				Permission permission = new Permission();
				permission.setPermissions(Integer.valueOf(rs.getString("perm_value")));
				permissions.add(permission);
				apiKey.setPermissions(permissions);
				cache.put(apiKey.getApiKey(), apiKey);
			}

			LOG.info("operator-apiKeys loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during operator-apiKeys", e);
			throw new GenericServiceException("Unable to operator-apiKeys", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}


	@Override
	public boolean isValid(String apiKey) {
		return cache.get(apiKey) != null; 
	}

	/**
	 * Fetches the ApiKey Definition matching apiKey string value
	 * 
	 * @param apiKey string
	 * @return the ApiKey VO
	 */
	@Override
	public ApiKey get(String apiKey) {
		return cache.get(apiKey);
	}
}
