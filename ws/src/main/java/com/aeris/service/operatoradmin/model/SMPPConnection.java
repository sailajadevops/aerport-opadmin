package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class SMPPConnection implements Serializable {
	private static final long serialVersionUID = 9148549189475720810L;
	private int smppConnectionId;
	private String smppConnectionName;
	private int productId;
	private int carrierId;
	
	@JsonIgnore
	private int smppId;

	public int getSmppConnectionId() {
		return smppConnectionId;
	}

	public void setSmppConnectionId(int smppConnectionId) {
		this.smppConnectionId = smppConnectionId;
	}

	public String getSmppConnectionName() {
		return smppConnectionName;
	}

	public void setSmppConnectionName(String smppConnectionName) {
		this.smppConnectionName = smppConnectionName;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}
	
	public int getSmppId() {
		return smppId;
	}
	
	public void setSmppId(int smppId) {
		this.smppId = smppId;
	}
}
