package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.jdbc.annotations.Query;
import com.aeris.service.operatoradmin.dao.ISalesAgentDAO;
import com.aeris.service.operatoradmin.exception.DuplicateSalesAgentException;
import com.aeris.service.operatoradmin.exception.SalesAgentException;
import com.aeris.service.operatoradmin.exception.SalesAgentNotFoundException;
import com.aeris.service.operatoradmin.model.SalesAgent;
import com.aeris.service.operatoradmin.utils.Constants;
import com.aeris.service.operatoradmin.utils.DBUtils;

public class SalesAgentDAO implements ISalesAgentDAO {
	@Query("agentSeqSql")
	private String agentSeqSql;

	@Query("agentAddSql")
	private String agentAddSql;

	@Query("updateAgentSql")
	private String updateAgentSql;

	@Query("deleteAgentSql")
	private String deleteAgentSql;
	
	@Query("deleteAgentFromAccountSql")
	private String deleteAgentFromAccountSql;

	@Query("getAllAgentsSql")
	private String getAllAgentsSql;

	@Query("getAllActiveAgentsSql")
	private String getAllActiveAgentsSql;

	@Query("getAgentSql")
	private String getAgentSql;

	private static final Logger LOG = LoggerFactory
			.getLogger(SalesAgentDAO.class);

	@Override
	public SalesAgent createSalesAgent(SalesAgent salesAgent, String operatorId)
			throws SalesAgentException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		LOG.info("Processing create new Sales Agent...");

		// Generate internal agent Id for the new agent
		try {
			SalesAgent existingAgent = getSalesAgent(salesAgent.getAgentId(), operatorId);
			if (existingAgent != null) {
				throw new DuplicateSalesAgentException("Agent with id: "
						+ salesAgent.getAgentId() + " already exists.");
			}
			conn = DBConnectionManager.getInstance()
					.getProvisionDatabaseConnection();
			ps = conn.prepareStatement(agentSeqSql);
			rs = ps.executeQuery();
			while (rs.next()) {
				salesAgent.setId(rs.getLong("seq"));
			}
			DBUtils.cleanup(null, ps, rs);

			long currentTime = Calendar
					.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis();
			salesAgent.setCreatedDate(new java.sql.Date(currentTime));
			salesAgent.setLastModifiedDate(new java.sql.Date(currentTime));
			salesAgent.setRepTimestamp(new Timestamp(currentTime));
			if (salesAgent.getLastModifiedBy() == null) {
				salesAgent.setLastModifiedBy(salesAgent.getCreatedBy());
			}
			ps = conn.prepareStatement(agentAddSql);
			int index = 1;
			ps.setLong(index++, salesAgent.getId());
			ps.setString(index++, salesAgent.getAgentId());
			ps.setString(index++, salesAgent.getFirstName());
			ps.setString(index++, salesAgent.getLastName());
			ps.setString(index++, salesAgent.getCompany());
			ps.setString(index++, salesAgent.getAgentType());
			ps.setDate(index++, salesAgent.getStartDate());
			ps.setDate(index++, salesAgent.getEndDate());
			ps.setInt(index++, salesAgent.getNotUsed());
			ps.setDate(index++, salesAgent.getCreatedDate());
			ps.setString(index++, salesAgent.getCreatedBy());
			ps.setDate(index++, salesAgent.getLastModifiedDate());
			ps.setString(index++, salesAgent.getLastModifiedBy());
			ps.setTimestamp(index++, salesAgent.getRepTimestamp());
			ps.setInt(index++, Integer.parseInt(operatorId));
			int result = ps.executeUpdate();
			if (result <= 0) {
				LOG.error("Failed to create Sales Agent with id: {}",
						salesAgent.getAgentId());
				throw new SalesAgentException(
						"Failed to create Sales Agent with id: "
								+ salesAgent.getAgentId());
			}
			LOG.debug("Sales Agent with id: {} sucessfully added to database.",
					salesAgent.getAgentId());
		} catch (SQLException e) {
			LOG.error("Failed to create Sales Agent due to db error. Cause: {}", e);
			throw new SalesAgentException(
					"Failed to create Sales Agent due to db error.");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return salesAgent;
	}

	@Override
	public SalesAgent updateSalesAgent(SalesAgent salesAgent, String operatorId)
			throws SalesAgentException, SalesAgentNotFoundException {
		Connection conn = null;
		PreparedStatement ps = null;
		LOG.info("Processing update Sales Agent...");

		try {
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance()
					.getProvisionDatabaseConnection();

			long currentTime = Calendar
					.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis();
			salesAgent.setLastModifiedDate(new java.sql.Date(currentTime));
			salesAgent.setRepTimestamp(new Timestamp(currentTime));
			if (salesAgent.getLastModifiedBy() == null) {
				salesAgent.setLastModifiedBy(salesAgent.getCreatedBy());
			}
			ps = conn.prepareStatement(updateAgentSql);
			int index = 1;
			ps.setString(index++, salesAgent.getFirstName());
			ps.setString(index++, salesAgent.getLastName());
			ps.setString(index++, salesAgent.getCompany());
			ps.setString(index++, salesAgent.getAgentType());
			ps.setDate(index++, salesAgent.getStartDate());
			ps.setDate(index++, salesAgent.getEndDate());
			ps.setDate(index++, salesAgent.getLastModifiedDate());
			ps.setString(index++, salesAgent.getLastModifiedBy());
			ps.setTimestamp(index++, salesAgent.getRepTimestamp());
			ps.setString(index++, salesAgent.getAgentId());
			ps.setInt(index++, Integer.parseInt(operatorId));
			int result = ps.executeUpdate();
			if (result <= 0) {
				LOG.debug("Sales Agent with id: {} not found.",
						salesAgent.getAgentId());
				throw new SalesAgentNotFoundException("Sales Agent with id: "
						+ salesAgent.getAgentId() + " not found.");
			}
			LOG.debug(
					"Sales Agent with id: {} sucessfully updated to database.",
					salesAgent.getAgentId());
			try {
				salesAgent = getSalesAgent(salesAgent.getAgentId(), operatorId);
			} catch (Exception e) {
				LOG.debug(
						"Failed to read details for updated agent with id: {}",
						salesAgent.getAgentId());
			}
		} catch (SQLException e) {
			LOG.error(
					"Failed to update Sales Agent with id:{} due to db error. Cause: {}",
					salesAgent.getAgentId(), e);
			throw new SalesAgentException(
					"Failed to update the Sales Agent due to db error.");
		} finally {
			DBUtils.cleanup(conn, ps);
		}
		return salesAgent;
	}

	@Override
	public boolean deleteSalesAgent(String agentId, String lastUpdatedBy, String operatorId)
			throws SalesAgentNotFoundException, SalesAgentException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBConnectionManager.getInstance()
					.getProvisionDatabaseConnection();
			
			// Delete Agent
			long currentTime = Calendar
					.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis();
			ps = conn.prepareStatement(deleteAgentSql);
			ps.setDate(1, new java.sql.Date(currentTime));
			ps.setString(2, lastUpdatedBy);
			ps.setTimestamp(3, new Timestamp(currentTime));
			ps.setString(4, agentId);
			ps.setInt(5, Integer.parseInt(operatorId));
			int result = ps.executeUpdate();
			if (result <= 0) {
				LOG.debug("Sales Agent with id: {} not found.", agentId);
				throw new SalesAgentNotFoundException("Sales Agent with id: "
						+ agentId + " not found.");
			}
			DBUtils.cleanup(null, ps, null);
			ps = conn.prepareStatement(deleteAgentFromAccountSql);
			ps.setDate(1, new java.sql.Date(currentTime));
			ps.setString(2, lastUpdatedBy);
			ps.setTimestamp(3, new Timestamp(currentTime));
			ps.setString(4, agentId);
			result = ps.executeUpdate();
			if (result <= 0) {
				LOG.info("Sales Agent with id: {} not associated with any account.", agentId);
			}
			LOG.debug("Agent with id: {} sucessfully deleted.", agentId);
		} catch (SQLException e) {
			LOG.error("Failed to delete Sales Agent due to db error. Cause: {}", e);
			throw new SalesAgentException(
					"Failed to delete Sales Agent due to db error.");
		} finally {
			DBUtils.cleanup(conn, ps);
		}
		return true;
	}

	@Override
	public List<SalesAgent> getAllSalesAgents(String operatorId, boolean activeAgents) throws SalesAgentException {
		List<SalesAgent> agents = new ArrayList<SalesAgent>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBConnectionManager.getInstance()
					.getProvisionDatabaseConnection();

			// Get all Agents
			if(activeAgents){
				ps = conn.prepareStatement(getAllActiveAgentsSql);
			}
			else{
				ps = conn.prepareStatement(getAllAgentsSql);				
			}
			ps.setInt(1, Integer.parseInt(operatorId));
			rs = ps.executeQuery();
			while (rs.next()) {
				SalesAgent agent = new SalesAgent();
				agent.setId(rs.getInt("id"));
				agent.setAgentId(rs.getString("agent_id"));
				agent.setFirstName(rs.getString("first_name"));
				agent.setLastName(rs.getString("last_name"));
				agent.setCompany(rs.getString("company"));
				agent.setAgentType(rs.getString("agent_type"));
				agent.setStartDate(rs.getDate("start_date"));
				agent.setEndDate(rs.getDate("end_date"));
				agent.setCreatedDate(rs.getDate("created_date"));
				agent.setLastModifiedDate(rs.getDate("last_modified_date"));
				agent.setNotUsed(rs.getInt("not_used"));
				agent.setCreatedBy(rs.getString("created_by"));
				agent.setLastModifiedBy(rs.getString("last_modified_by"));
				agent.setRepTimestamp(rs.getTimestamp("rep_timestamp"));
				agents.add(agent);
			}
			LOG.debug("Sales Agents List {}", agents);
		} catch (SQLException e) {
			LOG.error("Exception while retrieving Sales Agents list. Cause: {}"
					, e);
			throw new SalesAgentException(
					"Failed to retrieve Sales Agents due to db error.");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return agents;
	}

	@Override
	public SalesAgent getSalesAgent(String agentId, String operatorId) throws SalesAgentException {
		SalesAgent agent = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBConnectionManager.getInstance()
					.getProvisionDatabaseConnection();

			// Get all Agents
			ps = conn.prepareStatement(getAgentSql);
			ps.setString(1, agentId);
			ps.setInt(2, Integer.parseInt(operatorId));
			
			rs = ps.executeQuery();
			if (rs.next()) {
				agent = new SalesAgent();
				agent.setId(rs.getInt("id"));
				agent.setAgentId(rs.getString("agent_id"));
				agent.setFirstName(rs.getString("first_name"));
				agent.setLastName(rs.getString("last_name"));
				agent.setCompany(rs.getString("company"));
				agent.setAgentType(rs.getString("agent_type"));
				agent.setStartDate(rs.getDate("start_date"));
				agent.setEndDate(rs.getDate("end_date"));
				agent.setCreatedDate(rs.getDate("created_date"));
				agent.setLastModifiedDate(rs.getDate("last_modified_date"));
				agent.setNotUsed(rs.getInt("not_used"));
				agent.setCreatedBy(rs.getString("created_by"));
				agent.setLastModifiedBy(rs.getString("last_modified_by"));
				agent.setRepTimestamp(rs.getTimestamp("rep_timestamp"));
			}
			LOG.debug("Retrieved details for Sales Agent with id: {}", agentId);
		} catch (SQLException e) {
			LOG.error(
					"Exception while retrieving Sales Agent with id: {}, cause: {}",
					agentId, e);
			throw new SalesAgentException(
					"Failed to retrieve Sales Agent due to db error.");
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		return agent;
	}

}
