package com.aeris.service.operatoradmin.exception;

public class DistributorException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DistributorException(String s) {
		super(s);
	}

	public DistributorException(String ex, Throwable t) {
		super(ex, t);
	}
}
