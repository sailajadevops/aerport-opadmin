package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.ApiKey;

public interface IApiKeyEnum extends IBaseEnum {

	boolean isValid(String apiKey);

	ApiKey get(String apiKey);

	List<ApiKey> get(Long accountId);

	void updateAccountKeyCache(Long accountId, ApiKey key);

}
