package com.aeris.service.operatoradmin.payload;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.IMSIRangeType;
import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@XmlRootElement
public class CreateSupplierSIMOrderRequest {
	@NotNull(message = "{quantity.notnull}")
	@Min(value = 1, message = "{quantity.minimum}")
	@Max(value = 10000, message = "{quantity.maximum}")
	private long quantity;

	@NotNull(message = "{supplierId.notnull}")
	@Min(value = 1, message = "{supplierId.number}")
	private int supplierId;

	@NotNull(message = "{simWarehouseId.notnull}")
	@Min(value = 1, message = "{simWarehouseId.number}")
	private int simWarehouseId;

	@NotNull(message = "{simFormatId.notnull}")
	@Min(value = 1, message = "{simFormatId.number}")
	private int simFormatId;

	@Pattern(regexp = "[0-9]+", message = "{productId.number}")
	@NotNull(message = "{productId.notnull}")
	private String productId;

	@Enumeration(value=OrderStatus.class, message="{status.enum}")
	private OrderStatus status = OrderStatus.OPEN;

	@NotNull(message = "{iccIdRange.notnull}")
	private String iccIdRange;

	@NotNull(message = "{imsiRange.notnull}")
	private String imsiRange;

	@Enumeration(value=IMSIRangeType.class, message="{imsiRangeType.enum}")
	private IMSIRangeType imsiRangeType;

	private String iccIdStart;

	private String imsiStart;

	@NotNull(message = "{userId.notnull}")
	@Email(message = "{userId.email.pattern}")
	private String userId;

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public int getSimWarehouseId() {
		return simWarehouseId;
	}

	public void setSimWarehouseId(int simWarehouseId) {
		this.simWarehouseId = simWarehouseId;
	}

	public int getSimFormatId() {
		return simFormatId;
	}

	public void setSimFormatId(int simFormatId) {
		this.simFormatId = simFormatId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public String getIccIdRange() {
		return iccIdRange;
	}

	public void setIccIdRange(String iccIdRange) {
		this.iccIdRange = iccIdRange;
	}

	public String getImsiRange() {
		return imsiRange;
	}

	public void setImsiRange(String imsiRange) {
		this.imsiRange = imsiRange;
	}

	public IMSIRangeType getImsiRangeType() {
		return imsiRangeType;
	}

	public void setImsiRangeType(IMSIRangeType imsiRangeType) {
		this.imsiRangeType = imsiRangeType;
	}

	public String getIccIdStart() {
		return iccIdStart;
	}

	public void setIccIdStart(String iccIdStart) {
		this.iccIdStart = iccIdStart;
	}

	public String getImsiStart() {
		return imsiStart;
	}

	public void setImsiStart(String imsiStart) {
		this.imsiStart = imsiStart;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
