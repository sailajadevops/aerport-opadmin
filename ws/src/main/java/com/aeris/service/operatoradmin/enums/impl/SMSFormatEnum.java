package com.aeris.service.operatoradmin.enums.impl;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.ISMSFormatEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SMSFormat;
import com.google.inject.Singleton;

/**
 * SMSFormat Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class SMSFormatEnum implements ISMSFormatEnum, IEventListener {
	private Logger LOG = LoggerFactory.getLogger(SMSFormatEnum.class);
	private Cache<String, SMSFormat> cache;
	private Cache<String, SMSFormat> cacheById;
	
	@Inject
	private IStoredProcedure<List<SMSFormat>> getSMSFormatsStoredProcedure;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "SMSFormatCache/name") Cache<String, SMSFormat> cache, 
			@Hazelcast(cache = "SMSFormatCache/id") Cache<String, SMSFormat> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");

		try {
			// Call Get_Product Stored Procedure
			List<SMSFormat> smsFormats = getSMSFormatsStoredProcedure.execute();	
			
			for(SMSFormat smsFormat:smsFormats) 
			{
				cache.put(smsFormat.getFormatString(), smsFormat);
				cacheById.put(String.valueOf(smsFormat.getFormatCode()), smsFormat);
			}
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during getSMSFormatsStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} 
	}
	
	@Override
	public SMSFormat getSMSFormatByName(String name) {
		return cache.get(name);
	}

	@Override
	public SMSFormat getSMSFormatById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<SMSFormat> getAllSMSFormats() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
