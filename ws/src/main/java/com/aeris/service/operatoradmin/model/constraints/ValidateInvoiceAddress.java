package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.apache.commons.lang.StringUtils;

import com.aeris.service.operatoradmin.model.InvoiceAddress;

@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD, PARAMETER, TYPE})
@Documented
@Constraint(validatedBy = ValidateInvoiceAddress.Validator.class)
public @interface ValidateInvoiceAddress {
	String message() default "{com.aeris.service.operatoradmin.model.constraints.ValidateInvoiceAddress.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class Validator implements ConstraintValidator<ValidateInvoiceAddress, InvoiceAddress> {

		@Override
		public void initialize(final ValidateInvoiceAddress enumClazz) {
			// do nothing
		}

		public boolean isValid(final InvoiceAddress address, final ConstraintValidatorContext constraintValidatorContext) {
			if(StringUtils.isNotEmpty(address.getFullAddress()))
			{
				return true;
			}
			
			return StringUtils.isNotEmpty(address.getAddressFirstLine()) && StringUtils.isNotEmpty(address.getCountry())
					&& StringUtils.isNotEmpty(address.getState()) && StringUtils.isNotEmpty(address.getZipCode());
		}
	}
}
