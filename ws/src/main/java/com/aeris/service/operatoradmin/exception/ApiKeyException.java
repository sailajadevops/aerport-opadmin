package com.aeris.service.operatoradmin.exception;

public class ApiKeyException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public ApiKeyException(String s) {
		super(s);
	}

	public ApiKeyException(String ex, Throwable t) {
		super(ex, t);
	}
}
