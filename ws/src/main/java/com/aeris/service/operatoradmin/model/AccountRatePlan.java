package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class AccountRatePlan implements Serializable {
	private static final long serialVersionUID = -4585326150740378971L;
	private long accountRatePlanId;
	private long accountId;
	private RatePlan ratePlan;
	private Date startDate;
	private Date endDate;
	private RatePlanStatus status;
	private int carrierRatePlanId;

	public long getAccountRatePlanId() {
		return accountRatePlanId;
	}

	public void setAccountRatePlanId(long accountRatePlanId) {
		this.accountRatePlanId = accountRatePlanId;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public RatePlan getRatePlan() {
		return ratePlan;
	}

	public void setRatePlan(RatePlan ratePlan) {
		this.ratePlan = ratePlan;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public RatePlanStatus getStatus() {
		return status;
	}
	
	public void setStatus(RatePlanStatus status) {
		this.status = status;
	}
	
	public int getCarrierRatePlanId() {
		return carrierRatePlanId;
	}
	
	public void setCarrierRatePlanId(int carrierRatePlanId) {
		this.carrierRatePlanId = carrierRatePlanId;
	}

}
