package com.aeris.service.operatoradmin.jmx;

import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.MBeanService;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventDispatcher;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * This is a standard MBean class. It provides the ability to manage caches in operator
 * admin
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class CacheManager implements CacheManagerMBean{

	public static final Logger LOG = LoggerFactory.getLogger(CacheManager.class);

	private static final String JMX_QUALIFIER = "com.aeris.service.operatoradmin.jmx"; 
	private static final String CONTEXT_NAME = "operatoradmin_ws_2_0";

	ObjectName objName = null;
	 
	IEventDispatcher dispatcher;

	@Inject
	public CacheManager(IEventDispatcher dispatcher) {
		this.dispatcher = dispatcher;
	}
	
	@Inject
	public void registerMBean() {
		try {
			LOG.info("Registering CacheManagerBean with context : " + CONTEXT_NAME);
			objName = new ObjectName(JMX_QUALIFIER + ":Name=CacheManagerBean,Type=" + CONTEXT_NAME);
			MBeanService.getServer().registerMBean(this, objName);
			LOG.info("CacheManagerBean Registered....");
		} catch (Exception e) {
			LOG.error("Unable to register MBean", e.getMessage(), e);
		}
	}

	@Override
	public void refreshAll() {
		dispatcher.propogateEvent(Event.REFRESH_CACHE);
	}
}
