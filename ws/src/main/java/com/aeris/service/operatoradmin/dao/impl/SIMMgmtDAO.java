package com.aeris.service.operatoradmin.dao.impl;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.ISIMMgmtDAO;
import com.aeris.service.operatoradmin.exception.DAOException;
import com.aeris.service.operatoradmin.exception.SIMInventoryException;
import com.aeris.service.operatoradmin.payload.AssignSIMsToAccountRequest;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.google.common.collect.Lists;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SIMMgmtDAO implements ISIMMgmtDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SIMMgmtDAO.class);

    @Override
    public void assignSIMsToAccount(String operatorIdStr, String accountIdStr, AssignSIMsToAccountRequest request) throws DAOException {
        LOGGER.info("in assignSIMsToAccount()");
        int operatorId = Integer.parseInt(operatorIdStr);
        long accountId = Long.parseLong(accountIdStr);
        String iccidStart = request.getIccidStart();
        String iccidEnd = request.getIccidEnd();
        String iccidAdditional = request.getIccidAdditional();
        long orderId = request.getOrderId();
        List<SIM> sims = getSIMsToAllocate(request.getIccidStart(), request.getIccidEnd());
        LOGGER.info("Number of SIMs from iccid range {} to {} is {}", iccidStart, iccidEnd, sims.size());
        sims = getAdditionalSIMsToAllocate(iccidAdditional, sims);
        LOGGER.info("Total number of SIMs after considering additional iccids {} : {}", iccidAdditional, sims.size());
        try {
            List<List<SIM>> partitionList = Lists.partition(sims, 1000);
            for (List<SIM> list : partitionList) {
                assignSIMs(orderId, list, accountId, request.getUserEmail());
            }
            refreshSIMAssignedMV();
        } catch (SQLException ex) {
            LOGGER.error("Failed to assign sims to account {}", accountId, ex);
            throw new DAOException("Failed to assign sims to account " + accountId + " due to " + ex.getMessage());
        } catch (SIMInventoryException ex) {
            LOGGER.error("Failed to assign sims to account {}", accountId, ex);
            throw new DAOException("Failed to assign sims to account " + accountId + " due to " + ex.getMessage());
        }
        LOGGER.info("Request to assign SIMs to account {} is completed successfully.", accountId);
    }

    @Override
    public void assignSIMsToAccountFromSIMPack(String operatorId, String accountId, String simPackId, String userId, long orderId) throws DAOException {
        LOGGER.info("in assignSIMsToAccountFromSIMPack() :: accountId = {}, simPackId = {}, userId = {}", accountId, simPackId, userId);
        // TODO Validate input params
        SIMPackInfo simPack = getSIMPackInfo(simPackId);
        List<SIM> sims = getSIMsToAllocate(simPack.getIccidStart(), simPack.getIccidEnd());
        LOGGER.info("Number of SIMs from iccid range {} to {} is {}", simPack.getIccidStart(), simPack.getIccidEnd(), sims.size());
        sims = getAdditionalSIMsToAllocate(simPack.getIccidAdditional(), sims);
        LOGGER.info("Total number of SIMs after considering additional iccids {} : {}", simPack.getIccidAdditional(), sims.size());
        try {
            assignSIMs(orderId, sims, Integer.parseInt(accountId), userId);
            refreshSIMAssignedMV();
        } catch (SQLException ex) {
            LOGGER.error("Failed to assign sims to account {}", accountId, ex);
            throw new DAOException("Failed to assign sims to account " + accountId + " due to " + ex.getMessage());
        } catch (SIMInventoryException ex) {
            LOGGER.error("Failed to assign sims to account {}", accountId, ex);
            throw new DAOException("Failed to assign sims to account " + accountId + " due to " + ex.getMessage());
        }
        LOGGER.info("Request to assign SIMs to account {} is completed successfully.", accountId);
    }

    private static final String GET_SIM_PACKAGE_INFO = "SELECT seq_no, unique_id, iccid_start, iccid_end, packet_size, sim_type, ICCID_ADDITIONAL "
            + "FROM sim_inventory.simpack "
            + "WHERE unique_id = ? and used = 1";

    private SIMPackInfo getSIMPackInfo(String uniqueId) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            ps = con.prepareStatement(GET_SIM_PACKAGE_INFO);
            ps.setString(1, uniqueId);
            rs = ps.executeQuery();
            SIMPackInfo simPackInfo = null;
            if (rs.next()) {
                simPackInfo = new SIMPackInfo();
                simPackInfo.setIccidEnd(rs.getString("iccid_end"));
                simPackInfo.setIccidStart(rs.getString("iccid_start"));
                simPackInfo.setId(rs.getLong("seq_no"));
                simPackInfo.setPacketSize(rs.getInt("packet_size"));
                simPackInfo.setUnique_id(rs.getString("unique_id"));
                simPackInfo.setType(rs.getString("sim_type"));
                simPackInfo.setIccidAdditional(rs.getString("ICCID_ADDITIONAL"));
            } else {
                throw new DAOException("No SIMPack found for unique id " + uniqueId);
            }
            return simPackInfo;
        } catch (SQLException e) {
            LOGGER.error("Failed to fetch SIMPack info for {}", uniqueId, e);
            throw new DAOException("Failed to fetch SIMPACK info due to " + e);
        } finally {
            DBConnectionManager.getInstance().cleanup(con, ps, rs);
        }
    }

    private static final String GET_SIMS_TO_ALLOCATE = "select iccid, imsi, carrier "
            + "from sim_inventory.cingular_sim_inventory where substr(iccid, 1, 19) between ? and ? AND assign_date is null";

    private List<SIM> getSIMsToAllocate(String iccidStart, String iccidEnd) throws DAOException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            ps = con.prepareStatement(GET_SIMS_TO_ALLOCATE);
            ps.setString(1, iccidStart.substring(0, 19));
            ps.setString(2, iccidEnd.substring(0, 19));
            rs = ps.executeQuery();
            List<SIM> sims = new ArrayList<SIM>();
            while (rs.next()) {
                SIM sim = new SIM();
                sim.setIccid(rs.getString("iccid"));
                sim.setImsi(rs.getString("imsi"));
                sim.setCarrier(rs.getString("carrier"));
                sims.add(sim);
            }
            return sims;
        } catch (SQLException e) {
            LOGGER.error("Failed to fetch SIMs for allocation {} {}", iccidStart, iccidEnd, e);
            throw new DAOException("Failed to fetch SIMs for allocation due to " + e.getMessage());
        } finally {
            DBConnectionManager.getInstance().cleanup(con, ps, rs);
        }
    }

    private List<SIM> getAdditionalSIMsToAllocate(String iccidAdditional, List<SIM> sims) throws DAOException {
        if (iccidAdditional == null || iccidAdditional.isEmpty()) {
            LOGGER.info("iccidAdditional is null or empty.");
            return sims;
        }

        List<String> additionalSims = new ArrayList<String>();
        String iccidValues[] = iccidAdditional.split(",");
        for (String iccidValue : iccidValues) {
            iccidValue = iccidValue.trim();
            boolean found = false;
            for (int j = 0; j < sims.size(); j++) {
                if (sims.get(j).getIccid().equals(iccidValue)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                additionalSims.add(iccidValue);
            }
        }
        String iccidAdditionalFiltered = StringUtils.join(additionalSims, ",");
        if (iccidAdditionalFiltered == null || iccidAdditionalFiltered.isEmpty()) {
            LOGGER.info("iccidAdditionalFiltered is null or empty.");
            return sims;
        }

        String GET_ADDITIONAL_SIMS_TO_ALLOCATE = "select iccid, imsi, carrier "
                + "from sim_inventory.cingular_sim_inventory where iccid in (" + iccidAdditionalFiltered + ") AND assign_date is null";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            ps = con.prepareStatement(GET_ADDITIONAL_SIMS_TO_ALLOCATE);
            rs = ps.executeQuery();
            List<SIM> additionalSimsCollection = new ArrayList<SIM>();
            while (rs.next()) {
                SIM sim = new SIM();
                sim.setIccid(rs.getString("iccid"));
                sim.setImsi(rs.getString("imsi"));
                sim.setCarrier(rs.getString("carrier"));
                additionalSimsCollection.add(sim);
            }
            sims.addAll(additionalSimsCollection);
            return sims;
        } catch (SQLException e) {
            LOGGER.error("Failed to fetch SIMs for allocation {}", iccidAdditional, e);
            throw new DAOException("Failed to fetch SIMs for allocation due to " + e.getMessage());
        } finally {
            DBConnectionManager.getInstance().cleanup(con, ps, rs);
        }
    }

    private static final String ALLOCATE_SIM_QUERY = "insert into sim_inventory.cingular_sim_assigned "
            + "(iccid, imsi, order_number, account_id, assigne_date, last_change, last_changed_by, carrier) "
            + "values (?, ?, ?, ?, sysgmtdate, sysgmtdate, ?, ?)";

    private static final String MARK_SIM_ASSIGNED_QUERY = "update sim_inventory.cingular_sim_inventory set assign_date = sysgmtdate where iccid in ";

    private void assignSIMs(long orderId, List<SIM> sims, long accountId, String userId) throws DAOException, SQLException, SIMInventoryException {
        Connection con = null;
        PreparedStatement ps = null;
        Statement stmt;
        List<String> iccidArrayList = new ArrayList<String>();
        try {
            con = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            setAutoCommit(con, false);
            ps = con.prepareStatement(ALLOCATE_SIM_QUERY);
            String assignedBy = userId;
            if (userId.length() > 24) {
                assignedBy = userId.substring(0, 24);
            }
            for (SIM sim : sims) {
                ps.setString(1, sim.getIccid());
                ps.setString(2, sim.getImsi());
                ps.setLong(3, orderId);
                ps.setLong(4, accountId);
                ps.setString(5, assignedBy);
                ps.setString(6, sim.getCarrier());
                ps.addBatch();
                iccidArrayList.add(sim.getIccid());
            }
            checkIfSIMsAssignedAlready(iccidArrayList);
            ps.executeBatch();
            String iccidStr = OAUtils.getOracleINString(iccidArrayList);
            String query = MARK_SIM_ASSIGNED_QUERY + "(" + iccidStr + ")";
            LOGGER.info("Allocating SIMS query {}", query);
            stmt = con.createStatement();
            int countUpdated = stmt.executeUpdate(query);
            if (countUpdated != sims.size()) {
                LOGGER.error("Allocated only {} out of {} for account {}", countUpdated, sims.size(), accountId);
                throw new DAOException("Failed to update assign date of all SIMs.");
            }
            con.commit();
        } catch (DAOException e) {
            if (con != null) {
                con.rollback();
            }
            LOGGER.error("Failed to allocate SIM for orderId {}, accountId {}", orderId, accountId, e);
            throw new DAOException("Failed to assign SIMs due to " + e.getMessage());
        } catch (SQLException e) {
            if (con != null) {
                con.rollback();
            }
            LOGGER.error("Failed to allocate SIM for orderId {}, accountId {}", orderId, accountId, e);
            throw new DAOException("Failed to assign SIMs due to " + e.getMessage());
        } finally {
            setAutoCommit(con, true);
            DBConnectionManager.getInstance().cleanup(con, ps, null);
        }
    }

    public void refreshSIMAssignedMV() throws DAOException {
        Connection con = null;
        CallableStatement stmt = null;
        try {
            con = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
            stmt = con.prepareCall("call DBMS_SNAPSHOT.REFRESH('mv_sim_assigned','f')");
            stmt.execute();
        } catch (SQLException e) {
            LOGGER.error("Failed to refresh view", e);
            throw new DAOException("Failed to refresh view mv_sim_assigned due to " + e.getMessage());
        } finally {
            DBConnectionManager.getInstance().cleanup(con, stmt, null);
        }
    }

    private void setAutoCommit(Connection conn, boolean autoCommit) throws SIMInventoryException {
        if (conn == null) {
            return;
        }
        try {
            conn.setAutoCommit(autoCommit);
        } catch (SQLException e) {
            throw new SIMInventoryException("Exception occurred while setting autoCommit value for connection. DB error", e);
        }
    }

    private static final String GET_SIMS_QUERY = "select iccid from sim_inventory.cingular_sim_inventory where substr(iccid, 1, 19) between ? and ? and assign_date is null";

    private final String CHECK_IF_SIMS_ASSIGNED_QUERY = "select count(*) from sim_inventory.cingular_sim_assigned where iccid in (ICCID_LIST)";

    private void checkIfSIMsAssignedAlready(List<String> iccidList) throws DAOException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<List<String>> partitionList = Lists.partition(iccidList, 1000);
        for (List<String> list : partitionList) {
            String iccids = OAUtils.getOracleINString(list);
            String query = CHECK_IF_SIMS_ASSIGNED_QUERY.replaceAll("ICCID_LIST", iccids);
            try {
                DBConnectionManager connectionManager = DBConnectionManager.getInstance();
                conn = connectionManager.getBillingDatabaseConnection();
                ps = conn.prepareStatement(query);
                rs = ps.executeQuery();
                if (rs.next()) {
                    if (rs.getInt(1) > 0) {
                        LOGGER.error("Some of the ICCIDs are already assigned to other account. Please choose another one.");
                        throw new DAOException("Some of the ICCIDs are already assigned to other account. Please choose another one.");
                    }
                }
            } catch (SQLException e) {
                LOGGER.error("SQLException occurred while checking if ICCIDs are already assigned.", e);
                throw new DAOException("Exception occurred while checking if ICCIDs are already assigned due to " + e.getMessage());
            } finally {
                DBConnectionManager.getInstance().cleanup(conn, ps, rs);
            }
        }
    }
}

class SIM {

    String iccid;
    String imsi;
    String carrier;

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}

class SIMPackInfo {

    private long id;
    private String unique_id;
    private String iccidStart;
    private String iccidEnd;
    private int packetSize;
    private String type;
    private String iccidAdditional;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public int getPacketSize() {
        return packetSize;
    }

    public void setPacketSize(int packetSize) {
        this.packetSize = packetSize;
    }

    public String getIccidStart() {
        return iccidStart;
    }

    public void setIccidStart(String iccidStart) {
        this.iccidStart = iccidStart;
    }

    public String getIccidEnd() {
        return iccidEnd;
    }

    public void setIccidEnd(String iccidEnd) {
        this.iccidEnd = iccidEnd;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getIccidAdditional() {
        return iccidAdditional;
    }

    public void setIccidAdditional(String iccidAdditional) {
        this.iccidAdditional = iccidAdditional;
    }

}
