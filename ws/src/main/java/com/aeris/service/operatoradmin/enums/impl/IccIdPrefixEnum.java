package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IIccIdPrefixEnum;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.IccIdPrefix;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * Resource Definition Cache used for lookup purposes 
 * 
 * @author SP00125222
 */
@Singleton
public class IccIdPrefixEnum implements IIccIdPrefixEnum {
	private Logger LOG = LoggerFactory.getLogger(IccIdPrefixEnum.class);
	static final String SQL_GET_ICCID_PREFIX = "select range, tail from iccid_tail";
	
	private Cache<String, IccIdPrefix> cache;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "IccIdPrefixCache/name") Cache<String, IccIdPrefix> cache) {
		this.cache = cache;	
		loadResources();
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_ICCID_PREFIX);
			
			while (rs.next()) {
				IccIdPrefix iccIdPrefix = new IccIdPrefix();
				
				iccIdPrefix.setRange(rs.getInt("range"));
				iccIdPrefix.setTail(rs.getLong("tail"));
				
				cache.put(String.valueOf(iccIdPrefix.getRange()), iccIdPrefix);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}

	@Override
	public IccIdPrefix getIccIdPrefixByRange(int range) {
		return cache.get(String.valueOf(range));
	}

	@Override
	public List<IccIdPrefix> getAllIccIdPrefixes() {
		return new ArrayList<IccIdPrefix>(cache.getValues());
	}	
	
	@Override
	public boolean isValid(String id) {
		return cache.get(id) != null; 
	}
}
