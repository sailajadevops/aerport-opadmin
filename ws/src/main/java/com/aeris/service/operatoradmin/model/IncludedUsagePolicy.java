package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class IncludedUsagePolicy implements Serializable {
	private static final long serialVersionUID = -1030174390065044504L;
	public boolean rolloverUsage;
	public int includedMonths;
	public boolean poolUsageAcrossZones;

	public boolean isRolloverUsage() {
		return rolloverUsage;
	}

	public void setRolloverUsage(boolean rolloverUsage) {
		this.rolloverUsage = rolloverUsage;
	}

	public int getIncludedMonths() {
		return includedMonths;
	}

	public void setIncludedMonths(int includedMonths) {
		this.includedMonths = includedMonths;
	}

	public boolean isPoolUsageAcrossZones() {
		return poolUsageAcrossZones;
	}

	public void setPoolUsageAcrossZones(boolean poolUsageAcrossZones) {
		this.poolUsageAcrossZones = poolUsageAcrossZones;
	}

}
