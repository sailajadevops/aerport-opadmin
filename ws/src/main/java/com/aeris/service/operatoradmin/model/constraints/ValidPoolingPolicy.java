package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.enums.IPoolingPolicyEnum;
import com.aeris.service.operatoradmin.enums.impl.PoolingPolicyEnum;

@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD, PARAMETER, TYPE})
@Documented
@Constraint(validatedBy = {ValidPoolingPolicy.IntegerValidator.class, ValidPoolingPolicy.StringValidator.class})
public @interface ValidPoolingPolicy {
	String message() default "{poolingPolicyId.number}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class IntegerValidator implements ConstraintValidator<ValidPoolingPolicy, Integer> {
		
		IPoolingPolicyEnum poolingPolicyEnum;

		@Override
		public void initialize(final ValidPoolingPolicy enumClazz) {
			poolingPolicyEnum = OperatorAdminListener.sInjector.getInstance(PoolingPolicyEnum.class);
		}

		public boolean isValid(final Integer poolingPolicy, final ConstraintValidatorContext constraintValidatorContext) {
			if(poolingPolicy == 0)
			{
				return true;
			}
			
			return poolingPolicyEnum.isValid(String.valueOf(poolingPolicy));
		}
	}

	public class StringValidator implements ConstraintValidator<ValidPoolingPolicy, String> {
		
		IPoolingPolicyEnum poolingPolicyEnum;

		@Override
		public void initialize(final ValidPoolingPolicy enumClazz) {
			poolingPolicyEnum = OperatorAdminListener.sInjector.getInstance(PoolingPolicyEnum.class);
		}

		public boolean isValid(final String poolingPolicy, final ConstraintValidatorContext constraintValidatorContext) {
			if(poolingPolicy == null)
			{
				return true;
			}
			
			if(NumberUtils.isNumber(poolingPolicy))
			{
				return poolingPolicyEnum.isValid(poolingPolicy);
			}
			
			return false;
		}
	}
}
