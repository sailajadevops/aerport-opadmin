package com.aeris.service.operatoradmin.exception;

public class SubAccountException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SubAccountException(String s) {
		super(s);
	}

	public SubAccountException(String ex, Throwable t) {
		super(ex, t);
	}
}
