package com.aeris.service.operatoradmin.enums.impl;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.IVerticalEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Vertical;
import com.google.inject.Singleton;

/**
 * Vertical Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class VerticalEnum implements IVerticalEnum, IEventListener  {
	private Logger LOG = LoggerFactory.getLogger(ProductEnum.class);
	private Cache<String, Vertical> cache;
	private Cache<String, Vertical> cacheById;
	
	@Inject
	private IStoredProcedure<List<Vertical>> getVerticalsStoredProcedure;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "VerticalCache/name") Cache<String, Vertical> cache, 
			@Hazelcast(cache = "VerticalCache/id") Cache<String, Vertical> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");

		try {
			// Call Get_Product Stored Procedure
			List<Vertical> verticals = getVerticalsStoredProcedure.execute();	
			
			for(Vertical vertical:verticals) 
			{
				cache.put(vertical.getVerticalName(), vertical);
				cacheById.put(String.valueOf(vertical.getVerticalId()), vertical);
			}
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during GetVerticalsStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} 
	}
	

	@Override
	public Vertical getVerticalByName(String name) {
		return cache.get(name);
	}

	@Override
	public Vertical getVerticalById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<Vertical> getAllVerticals() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
