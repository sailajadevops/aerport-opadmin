package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum SIMStatus implements Serializable {
	ALLOCATED, USED, NOT_USED
}
