package com.aeris.service.operatoradmin.enums.impl;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Product;
import com.google.inject.Singleton;

/**
 * Product Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class ProductEnum implements IProductEnum, IEventListener {
	private Logger LOG = LoggerFactory.getLogger(ProductEnum.class);
	private Cache<String, Product> cache;
	private Cache<String, Product> cacheById;
	
	@Inject
	private IStoredProcedure<List<Product>> getProductsStoredProcedure;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "ProductCache/name") Cache<String, Product> cache, 
			@Hazelcast(cache = "ProductCache/id") Cache<String, Product> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");

		try {
			// Call Get_Product Stored Procedure
			List<Product> products = getProductsStoredProcedure.execute();	
			
			for(Product product:products) 
			{
				cache.put(product.getProductName(), product);
				cacheById.put(String.valueOf(product.getProductId()), product);
			}
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during GetProductsStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} 
	}
	
	@Override
	public Product getProductByName(String name) {
		return cache.get(name);
	}

	@Override
	public Product getProductById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<Product> getAllProducts() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}
	
	@Override
	public void putToCache(String id, Product product) {
		this.cacheById.put(id, product);
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
