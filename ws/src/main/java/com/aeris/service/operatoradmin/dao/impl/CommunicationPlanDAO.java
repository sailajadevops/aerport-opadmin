package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.ICommunicationPlanDAO;
import com.aeris.service.operatoradmin.enums.IAccessPointNameEnum;
import com.aeris.service.operatoradmin.exception.ServiceProfileValidationException;
import com.aeris.service.operatoradmin.model.APNConfiguration;
import com.aeris.service.operatoradmin.model.EnrolledServices;

/**
 * @author SG00436722
 * DAO Layer to server AT&T's Communication Plan related requests
 */
public class CommunicationPlanDAO implements ICommunicationPlanDAO {
	
	@Inject
	private IAccessPointNameEnum accessPointNameCache;
	
	private static Logger LOG = LoggerFactory.getLogger(CommunicationPlanDAO.class);
	private static final String GET_COMM_PLAN_ID_SQL = "select cp.CP_CODE, cp.APN from AERBILL_PROV.COMMUNICATION_PLAN cp,ACCOUNT_CONTRACT ac where cp.CONTRACT_ID = ac.CONTRACT_ID and cp.MT_VOICE=? AND cp.MO_VOICE=? AND cp.MT_SMS=? AND cp.MO_SMS =? "
			+ " AND cp.PACKET=? AND cp.STATIC_IP=? AND cp.RES= ? and ac.account_id = ?";

	/* 
	 *	This will return matching Communication Plan id for ZoneSet with selected services
	 * 
	 */
	@Override
	public Integer getCommPlanId(EnrolledServices enrolledServices, APNConfiguration apnConfig, int RES, long accountId)
			throws SQLException {

		Integer commPlanId = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			List<String> UIAPNList = new ArrayList<String>();
			if(apnConfig != null){
				int[] intAPNFrmUI = null;
				int[] dyynamicAPNs = apnConfig.getDynamicIpApns();
				int[] staticAPNs = apnConfig.getStaticIpApns();
				if(dyynamicAPNs != null && dyynamicAPNs.length >= 1 ){
					intAPNFrmUI = dyynamicAPNs;
				}else if(staticAPNs != null && staticAPNs.length >= 1){
					intAPNFrmUI = staticAPNs;
				}
				//Convert int[] of  APNFrmUI to List<String>
				for(int apnId : intAPNFrmUI){
					UIAPNList.add(String.valueOf(apnId));
				}
			}
			
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			ps = conn.prepareStatement(GET_COMM_PLAN_ID_SQL);
			ps.setInt(1, enrolledServices.isAllowMTVoice() ? 1 : 0);
			ps.setInt(2, enrolledServices.isAllowMOVoice() ? 1 : 0);
			ps.setInt(3, enrolledServices.isAllowMTSms() ? 1 : 0);
			ps.setInt(4, enrolledServices.isAllowMOSms() ? 1 : 0);
			//ps.setInt(5, enrolledServices.isAllowRoaming() ? 1 : 0);
			ps.setInt(5, enrolledServices.isAllowPacket() ? 1 : 0);
			ps.setInt(6, enrolledServices.isAllowStaticIP() ? 1 : 0);
			if(RES > 0){
				ps.setString(7, String.valueOf(RES));
			}else{
				ps.setNull(7, Types.VARCHAR);
			}
			ps.setLong(8, accountId);
			rs = ps.executeQuery();

			while(rs.next()) {
				// Check if APNs passed from UI matches with the APNs in Table Row
				String DBAPNs = rs.getString("APN");
				List<String> dbAPNList = Arrays.asList(DBAPNs.split("\\s*,\\s*"));
				if(dbAPNList.size() > 0){
					if(UIAPNList.size() > 0 && dbAPNList.containsAll(UIAPNList)){
						commPlanId = rs.getInt("CP_CODE");
						break;
					}
				}else{
					if(UIAPNList.size() == 0){
						commPlanId = rs.getInt("CP_CODE");
						break;
					}
				}
				
				if(dbAPNList.containsAll(UIAPNList)){
					commPlanId = rs.getInt("CP_CODE");
					break;
				}
			}
		} finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}
		LOG.info("Matching CommunicationPlan Id : " + commPlanId);
		return commPlanId;
	}
	
	
}
