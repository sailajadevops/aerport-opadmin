package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AccountApplication implements Serializable {
	private static final long serialVersionUID = 325157828518062187L;
	private String accountId;
	private List<ApiKey> apiKeys;
	private String operatorId;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public List<ApiKey> getApiKeys() {
		if (apiKeys == null) {
			apiKeys = new ArrayList<ApiKey>();
		}

		return apiKeys;
	}

	public void setApiKeys(List<ApiKey> apiKeys) {
		this.apiKeys = apiKeys;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof AccountApplication)) {
			return false;
		}

		return accountId.equals(((AccountApplication) obj).accountId);
	}
}
