package com.aeris.service.operatoradmin.payload;

import com.aeris.service.operatoradmin.events.Event;
import java.io.Serializable;
import java.util.Arrays;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Email;

@XmlRootElement
public class NotificationRequest implements Serializable {

    @Email(message = "{userId.email}")
    private String userId;
    @NotNull(message = "{toEmailRecipients.notnull}")
    @Size(min = 1, message = "{toEmailRecipients.size}")
    private String[] toEmailRecipients;
    private String[] ccEmailRecipients;
    @NotNull(message = "{entityID.notnull}")
    private String entityID;
    @NotNull(message = "{event.notnull}")
    private Event event;
    private String viewUrl;
    private String accountId;
    private int status;
    private String operatorId;
    private String additionalEmailBodyContents;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String[] getToEmailRecipients() {
        return toEmailRecipients;
    }

    public void setToEmailRecipients(String[] toEmailRecipients) {
        this.toEmailRecipients = toEmailRecipients;
    }

    public String getEntityID() {
        return entityID;
    }

    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getViewUrl() {
        return viewUrl;
    }

    public void setViewUrl(String viewUrl) {
        this.viewUrl = viewUrl;
    }

    public String[] getCcEmailRecipients() {
        return ccEmailRecipients;
    }

    public void setCcEmailRecipients(String[] ccEmailRecipients) {
        this.ccEmailRecipients = ccEmailRecipients;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getAdditionalEmailBodyContents() {
        return additionalEmailBodyContents;
    }

    public void setAdditionalEmailBodyContents(String additionalEmailBodyContents) {
        this.additionalEmailBodyContents = additionalEmailBodyContents;
    }
        
    @Override
    public String toString() {
        return "SendEmailRequest{" 
                + "event=" + event 
                + ", operatorId=" + operatorId 
                + ", toEmailRecipients=" + Arrays.toString(toEmailRecipients) 
                + ", ccEmailRecipients=" + Arrays.toString(ccEmailRecipients) 
                + ", entityID=" + entityID 
                + ", accountId=" + accountId 
                + ", viewUrl=" + viewUrl 
                + ", userId=" + userId + '}';
    }
}
