package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class RatePlanFeeDetails implements Serializable {
	private static final long serialVersionUID = 6330386572224820088L;

	@NotNull(message = "{accessFee.notnull}")
	private double accessFee;

	@NotNull(message = "{activationFee.notnull}")
	private double activationFee;

	@NotNull(message = "{suspendFee.notnull}")
	private double suspendFee;

	@NotNull(message = "{unsuspendFee.notnull}")
	private double unsuspendFee;

	@NotNull(message = "{deactivationFee.notnull}")
	private double deactivationFee;

	@NotNull(message = "{reactivationFee.notnull}")
	private double reactivationFee;

	public double getAccessFee() {
		return accessFee;
	}

	public void setAccessFee(double accessFee) {
		this.accessFee = accessFee;
	}

	public double getActivationFee() {
		return activationFee;
	}

	public void setActivationFee(double activationFee) {
		this.activationFee = activationFee;
	}

	public double getSuspendFee() {
		return suspendFee;
	}

	public void setSuspendFee(double suspendFee) {
		this.suspendFee = suspendFee;
	}

	public double getUnsuspendFee() {
		return unsuspendFee;
	}

	public void setUnsuspendFee(double unsuspendFee) {
		this.unsuspendFee = unsuspendFee;
	}

	public double getDeactivationFee() {
		return deactivationFee;
	}

	public void setDeactivationFee(double deactivationFee) {
		this.deactivationFee = deactivationFee;
	}

	public double getReactivationFee() {
		return reactivationFee;
	}

	public void setReactivationFee(double reactivationFee) {
		this.reactivationFee = reactivationFee;
	}
}
