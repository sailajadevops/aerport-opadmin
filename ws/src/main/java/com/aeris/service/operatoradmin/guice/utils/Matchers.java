package com.aeris.service.operatoradmin.guice.utils;

import java.io.Serializable;

import com.google.inject.TypeLiteral;
import com.google.inject.matcher.AbstractMatcher;
import com.google.inject.matcher.Matcher;

/**
 * Matchers implementation use to configure guice objects 
 * 
 * @author Srinivas Puranam
 */
public class Matchers {

	/**
	 * Returns a matcher which matches with any object with in the package.
	 */
	public static Matcher<Object> packageMatcher(String name) {
		return new PackageMatcher(name);
	}

	private static class PackageMatcher extends AbstractMatcher<Object> implements Serializable {
		private String packageName;

		public PackageMatcher(String packageName) {
			this.packageName = packageName;
		}

		public boolean matches(Object o) {
			if (o instanceof TypeLiteral<?>) {
				TypeLiteral<?> typeLiteral = (TypeLiteral<?>) o;
				return typeLiteral.getRawType().getPackage().getName().contains(packageName);
			}

			return o.getClass().getPackage().getName().contains(packageName);
		}

		@Override
		public boolean equals(Object other) {
			return other instanceof PackageMatcher && ((PackageMatcher) other).packageName.equals(packageName);
		}

		@Override
		public int hashCode() {
			return 37 * packageName.hashCode();
		}

		@Override
		public String toString() {
			return "packageMatcher(" + packageName + ")";
		}

		private static final long serialVersionUID = 0;
	}
}
