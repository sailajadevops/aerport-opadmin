package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum AccessPointNameType implements Serializable {
	STATIC_IP(1), DYNAMIC_IP(2);
	int value;

	AccessPointNameType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static AccessPointNameType fromValue(int value) {
		AccessPointNameType[] types = values();

		for (AccessPointNameType apnType : types) {
			if (apnType.getValue() == value) {
				return apnType;
			}
		}

		return null;
	}

	public static AccessPointNameType fromName(String value) {
		AccessPointNameType[] types = values();

		for (AccessPointNameType apnType : types) {
			if (apnType.name().equalsIgnoreCase(value)) {
				return apnType;
			}
		}

		return null;
	}
}
