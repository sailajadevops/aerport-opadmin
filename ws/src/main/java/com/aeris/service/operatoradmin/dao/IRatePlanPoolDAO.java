package com.aeris.service.operatoradmin.dao;

import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.BadStartDateException;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanPoolException;
import com.aeris.service.operatoradmin.exception.InvalidRatePlanPoolException;
import com.aeris.service.operatoradmin.exception.RatePlanPoolException;
import com.aeris.service.operatoradmin.exception.RatePlanPoolNotFoundException;
import com.aeris.service.operatoradmin.model.AccountRatePlanPool;
import com.aeris.service.operatoradmin.model.AccountRatePlanPoolOverageBucket;
import com.aeris.service.operatoradmin.model.RatePlanPoolStatus;

public interface IRatePlanPoolDAO {
	List<AccountRatePlanPool> getAccountRatePlanPools(int operatorId, long accountId, String start, String count, RatePlanPoolStatus... rateplanPoolStatusList);

	AccountRatePlanPool getAccountRatePlanPool(int operatorId, long accountId, long ratePlanPoolId);

	AccountRatePlanPool createNewAccountRatePlanPool(int operatorId, long accountId, int productId, String ratePlanPoolName, List<String> ratePlans,
			Date startDate, Date endDate, RatePlanPoolStatus status, Date requestedDate, String requestedUser,
			List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBucket) throws RatePlanPoolException, DuplicateRatePlanPoolException, InvalidRatePlanPoolException;

	AccountRatePlanPool updateAccountRatePlanPool(int operatorId, long accountId, long ratePlanPoolId, int productId, String ratePlanPoolName,
			List<String> ratePlans, Date startDate, Date endDate, RatePlanPoolStatus status, Date requestedDate, String requestedUser, List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBuckets)
			throws RatePlanPoolNotFoundException, RatePlanPoolException, BadStartDateException, DuplicateRatePlanPoolException, InvalidRatePlanPoolException;

	boolean invalidateRatePlanPool(int operatorId, long accountId, long ratePlanPoolId, Date requestedDate, String requestedUser)
			throws RatePlanPoolNotFoundException, RatePlanPoolException;

	boolean updateRatePlanPoolStatus(int operatorId, long accountId, long ratePlanPoolId, RatePlanPoolStatus status, Date requestedDate, String requestedUser)
			throws RatePlanPoolNotFoundException, RatePlanPoolException;
}