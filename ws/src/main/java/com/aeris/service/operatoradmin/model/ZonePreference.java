package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@XmlRootElement
@Enumeration(value = ZonePreference.class, message = "{type.enum}")
public enum ZonePreference implements Serializable {
	HOME(1), PREFERRED(2), NON_PREFERRED(3), BLOCKED(4);

	int value;

	ZonePreference(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static ZonePreference fromValue(int value) {
		ZonePreference[] preferences = values();

		for (ZonePreference preference : preferences) {
			if (preference.getValue() == value) {
				return preference;
			}
		}

		return null;
	}

	public static ZonePreference fromName(String value) {
		ZonePreference[] preferences = values();

		for (ZonePreference preference : preferences) {
			if (preference.name().equalsIgnoreCase(value)) {
				return preference;
			}
		}

		return null;
	}
}
