package com.aeris.service.operatoradmin.exception;

public class ConnectionLimitException extends Exception{
	
	public ConnectionLimitException(String s){
		super(s);
	}
	
	public ConnectionLimitException(String s, Throwable t){
		super(s, t);
	}
}
