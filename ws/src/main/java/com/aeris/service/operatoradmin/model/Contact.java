package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@XmlRootElement 
public class Contact implements Serializable {
	private static final long serialVersionUID = -1849187147843648224L;

	@Enumeration(value = ContactType.class, message = "{contact.type.enum}")
	private ContactType role;

	@NotNull(message = "{contact.firstName.notnull}")
	@Size(min = 1, max = 30, message = "{contact.firstName.size}")
	private String firstName;

	@Size(min = 0, max = 30, message = "{contact.lastName.size}")
	private String lastName;

	private String fullName;

	private String jobTitle;

	private String phoneNumber;

	@NotNull(message = "{contact.email.notnull}")
	@Email(message = "{contact.email.pattern}")
	private String email;

	public ContactType getRole() {
		return role;
	}

	public void setRole(ContactType role) {
		this.role = role;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonIgnore
	public boolean isValid() {
		return (firstName != null || lastName != null || fullName != null) && role != null;
	}
}
