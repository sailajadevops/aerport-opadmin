package com.aeris.service.operatoradmin.events;

import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class EventDispatcher implements IEventDispatcher {

	public static final Logger LOG = LoggerFactory.getLogger(EventDispatcher.class);
	private static ClassLoader loader = Thread.currentThread().getContextClassLoader();
	
	private final ExecutorService pool;
	
	@Inject
	Set<IEventListener> eventListeners;
	
	public EventDispatcher() {
		pool = Executors.newFixedThreadPool(10);
	}

    /* (non-Javadoc)
	 * @see com.aeris.service.operatoradmin.IEventDispatcher#propogateEvent(com.aeris.service.operatoradmin.model.Event)
	 */
    @Override
	public void propogateEvent(final Event event, final Object... params) {
    	for(final IEventListener eventListener: eventListeners)
    	{
    		Callable<Event> callable = new Callable<Event>() {
                @Override
                public Event call() throws Exception {
                	synchronized (eventListener) {
                        Thread.currentThread().setContextClassLoader(loader);
                        eventListener.onEvent(event, params);
					}
                	
            		return event;
                }
            };
            
            final Future<Event> future = pool.submit(callable);
            
            try {
    			LOG.info("processed event "+future.get());
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ExecutionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    }

    public void shutdown() {
        pool.shutdown();
    }
}