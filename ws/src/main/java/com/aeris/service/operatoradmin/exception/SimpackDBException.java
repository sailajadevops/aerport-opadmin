package com.aeris.service.operatoradmin.exception;

public class SimpackDBException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SimpackDBException(String s) {
		super(s);
	}

	public SimpackDBException(String ex, Throwable t) {
		super(ex, t);
	}
}
