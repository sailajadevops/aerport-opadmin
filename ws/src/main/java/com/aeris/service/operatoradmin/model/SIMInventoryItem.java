package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.sql.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class SIMInventoryItem implements Serializable {
	private static final long serialVersionUID = 335991855738018124L;
	private long supplierOrderId;
	private long simWarehouseId;
	private long simSupplierId;
	private long simFormatId;
	private SIMInfo info;
	private Date createdDate;
	private String userId;

	public long getSupplierOrderId() {
		return supplierOrderId;
	}

	public void setSupplierOrderId(long supplierOrderId) {
		this.supplierOrderId = supplierOrderId;
	}

	public long getSimWarehouseId() {
		return simWarehouseId;
	}

	public void setSimWarehouseId(long simWarehouseId) {
		this.simWarehouseId = simWarehouseId;
	}

	public long getSimSupplierId() {
		return simSupplierId;
	}

	public void setSimSupplierId(long simSupplierId) {
		this.simSupplierId = simSupplierId;
	}

	public long getSimFormatId() {
		return simFormatId;
	}

	public void setSimFormatId(long simFormatId) {
		this.simFormatId = simFormatId;
	}

	public SIMInfo getInfo() {
		return info;
	}

	public void setInfo(SIMInfo info) {
		this.info = info;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
