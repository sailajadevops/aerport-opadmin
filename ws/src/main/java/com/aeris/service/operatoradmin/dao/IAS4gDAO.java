package com.aeris.service.operatoradmin.dao;

import com.aeris.service.operatoradmin.model.HssApnConfig;
import com.aeris.service.operatoradmin.model.HssPSConfig;
import com.aeris.service.operatoradmin.model.HssProfile;
import java.util.List;

public interface IAS4gDAO {

    List<HssProfile> getHssProfiles();

    List<HssPSConfig> getHssPSConfigs();

    List<HssApnConfig> getHssApnConfigs();

    String getHssApnValues(String hssProfileId);
}
