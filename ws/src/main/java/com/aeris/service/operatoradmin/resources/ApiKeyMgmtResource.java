package com.aeris.service.operatoradmin.resources;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IAccountApplicationDAO;
import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.AtleastOneApiKeyRequiredException;
import com.aeris.service.operatoradmin.exception.InvalidApiKeyException;
import com.aeris.service.operatoradmin.model.AccountApplication;
import com.aeris.service.operatoradmin.model.ApiKey;
import com.aeris.service.operatoradmin.model.Resource;
import com.aeris.service.operatoradmin.model.ResourcePermission;
import com.aeris.service.operatoradmin.payload.CreateApiKeyRequest;
import com.aeris.service.operatoradmin.payload.UpdateApiKeyRequest;

/**
 * Rest Resource for api key management. Includes methods for creating a new api
 * key, retrieving existing api keys for account deleting an existing api key
 * and expiring an existing api key
 * 
 * @author Srinivas Puranam
 */
@Path("/admin")
@Singleton
//@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
@PermitAll
public class ApiKeyMgmtResource{
	private static final Logger LOG = LoggerFactory.getLogger(ApiKeyMgmtResource.class);
	private static final String DATE_FORMAT = "MM-dd-yyyy";

    @Inject
	private IAccountApplicationDAO accountApplicationDAO;

        /**
	 * Fetch the Api keys for given account id. Paginate the results using start
	 * and count values.
	 * 
	 * @param request
	 *            the get api keys request sent by the user
	 * @param start
	 *            the start value
	 * @param count
	 *            the no of api keys to fetch
	 * 
	 * @return The json response with the list of accounts and their Api Keys
	 */
	@Path("/account/{accountId}/apikeys")
	@GET
	public Response getApiKeys(@PathParam("accountId") final String accountId, @QueryParam("listExpired") final boolean listExpired, @QueryParam("start") final String start, @QueryParam("count") final String count) {
		LOG.info("Received request to fetch api keys for account id: " + accountId +", listExpired: "+ listExpired);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		try {
			if (StringUtils.isEmpty(accountId)) {
				LOG.error("Missing accountId in request, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("Missing accountId in request").build();
			}

			if (paginated) {
				LOG.info("Returning a max record size of " + count + " records starting from " + start);
			} else {
				LOG.info("Query is not paginated, returning all available api keys for account: " + accountId);
			}

			// Fetch from database
			List<AccountApplication> accountApplications = accountApplicationDAO.getApiKeys(start, count, listExpired, accountId);
			
			if (accountApplications != null) {
				LOG.info("fetched api keys successfully");
				ObjectMapper mapper = new ObjectMapper();
				String apiKeysJson = mapper.writeValueAsString(accountApplications);
				builder.entity(apiKeysJson);
			} else {
				LOG.info("No API Keys found for account id : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No API Keys found for the accountId: " + accountId).build();
			}
		} catch (AccountNotFoundException e) {
			LOG.error("getApiKeys Exception occured :", e);
			return Response.status(Status.BAD_REQUEST).entity("apiKeys not found for account=" + accountId).build();
		} catch (Exception e) {
			LOG.error("getApiKeys Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting API Keys").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getApiKeys: " + response);
		return response;
	}

	

	/**
	 * Create a new api key for accountId with the required permissions
	 * specified in the {@link CreateApiKeyRequest}
	 * 
	 * @param request
	 *            the request containing the account id and the required
	 *            permissions for the new api key
	 * @return the new api key created for the account id
	 */
	@Path("/account/{accountId}/apikeys")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createApiKey(@PathParam("accountId") final String accountId, CreateApiKeyRequest request) {
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		Date requestedDate = new Date();
		String requestedUser = request.getUserId();
		String expiresStr = request.getExpiryDate();
		String description = request.getDescription();
		List<ResourcePermission> permissionsList = request.getPermissions();	
		
		Date expires = new Date();

		LOG.info("Received request to create new api key for account id: " + accountId);

		try {
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("Missing accountId in request, value is: " + accountId);
				return Response.status(Status.BAD_REQUEST).entity("Missing account id in request").build();
			}

			if (StringUtils.isBlank(requestedUser)) {
				LOG.error("Missing requestedUser in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing requested user in request").build();
			}
			
			if (permissionsList == null || permissionsList.isEmpty()) {
				LOG.error("Missing api key permissions in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing api key permissions in request").build();
			}

			if (StringUtils.isBlank(expiresStr)) {
				LOG.error("Missing expiryDate in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing expiryDate in request").build();
			}
			
			// If date parse failed, throw error
			if((expires = parseDate(expiresStr)) == null)
			{
				return Response.status(Status.BAD_REQUEST).entity("expiry date format is not valid, usage: "+DATE_FORMAT).build();
			}
			
			if(!expires.after(requestedDate))
			{
				return Response.status(Status.BAD_REQUEST).entity("expiry date must be a future date, not a valid date: "+expiresStr).build();
			}
			
			if(StringUtils.isEmpty(description))
			{
				return Response.status(Status.BAD_REQUEST).entity("Missing description in request").build();
			}

			LOG.info("Account ID: " + accountId + " Permissions requested: " + request.getPermissions() + " User: " + requestedUser + " Date: " + requestedDate);

			ApiKey newApiKey = accountApplicationDAO.createNewApiKey(Integer.parseInt(accountId), request.getPermissions(), requestedDate, requestedUser, expires, description, request.getStatus());

			if (StringUtils.isBlank(newApiKey.getApiKey())) {
				LOG.info("createNewApiKey failed for account id: " + accountId);

				return Response.status(Status.NOT_FOUND).entity("createNewApiKey failed for account id: " + accountId).build();
			} else {
				LOG.info("Api Key created successfully for account id: " + accountId);

				ObjectMapper mapper = new ObjectMapper();
				String createJson = mapper.writeValueAsString(newApiKey);

				builder.entity(createJson);
			}
		} catch (Exception e) {
			LOG.error("createNewApiKey Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while creating new ApiKey").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewApiKey: " + response);
		return response;
	}

	private Date parseDate(String expiresStr) {
		Date parsedDate = null;
		
		try {
			parsedDate = DateUtils.parseDateStrictly(expiresStr, new String[] { DATE_FORMAT });
			LOG.info("Parse date success");
		} catch (ParseException e) {
			LOG.error("Failed to parse date " + expiresStr);
		}

		return parsedDate;
	}

	/**
	 * Create a new api key for accountId with the required permissions
	 * specified in the {@link CreateApiKeyRequest}
	 * 
	 * @param request
	 *            the request containing the account id and the required
	 *            permissions for the new api key
	 * @return the new api key created for the account id
	 */
	@Path("/account/{accountId}/apikeys/{apiKey}")
	@DELETE
	public Response deleteApiKey(@PathParam("accountId") final String accountId, @PathParam("apiKey") final String apiKey,
			@QueryParam("userId") final String userId) {
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		Date requestedDate = new Date();  
		String requestedUser = userId; 

		LOG.info("Received request to delete api key '"+apiKey+"' for account id: " + accountId);

		try {
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("Missing accountId in request, value is: " + accountId);
				return Response.status(Status.BAD_REQUEST).entity("Missing account id in request").build();
			}

			if (StringUtils.isBlank(apiKey)) {
				LOG.error("Missing api key in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing api key in request").build();
			}

			if (StringUtils.isBlank(requestedUser)) {
				LOG.error("Missing user in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing user in request").build();
			}

			LOG.info("Account ID: " + accountId + " API Key: " + apiKey + " User: " + requestedUser + " Date: " + requestedDate);

			boolean success = accountApplicationDAO.deleteApiKey(Integer.parseInt(accountId), apiKey, requestedDate, requestedUser);

			if (!success) {
				LOG.info("deleteApiKey failed for account id: " + accountId);

				return Response.status(Status.NOT_FOUND).entity("deleteApiKey failed for account id: " + accountId).build();
			} else {
				LOG.info("Api Key deleting successfully for account id: " + accountId);

				ObjectMapper mapper = new ObjectMapper();
				String deleteJson = mapper.writeValueAsString(success);

				builder.entity(deleteJson);
			}
		} catch (InvalidApiKeyException e) {
			LOG.error("InvalidApiKeyException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (AtleastOneApiKeyRequiredException e) {
			LOG.error("AtleastOneApiKeyRequiredException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("deleteApiKey Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while deleting ApiKey").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from deleteApiKey: " + response);
		return response;
	}

	/**
	 * Create a new api key for accountId with the required permissions
	 * specified in the {@link CreateApiKeyRequest}
	 * 
	 * @param request
	 *            the request containing the account id and the required
	 *            permissions for the new api key
	 * @return the new api key created for the account id
	 */
	@Path("/account/{accountId}/apikeys/{apiKey}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateApiKey(@PathParam("accountId") final String accountId, @PathParam("apiKey") final String apiKey,
			final UpdateApiKeyRequest request) {
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		Date requestedDate = new Date();
		String requestedUser = request.getUserId();
		Date expires = new Date();
		String expiresStr = request.getExpiryDate();
		String description = request.getDescription();
		List<ResourcePermission> permissionsList = request.getPermissions();	
		
		LOG.info("Received request to update api key '"+apiKey+"' for account id: " + accountId);

		try {
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("Missing accountId in request, value is: " + accountId);
				return Response.status(Status.BAD_REQUEST).entity("Missing account id in request").build();
			}

			if (StringUtils.isBlank(apiKey)) {
				LOG.error("Missing api key in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing api key in request").build();
			}

			if (permissionsList == null || permissionsList.isEmpty()) {
				LOG.error("Missing api key permissions in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing api key permissions in request").build();
			}
			
			if (StringUtils.isBlank(requestedUser)) {
				LOG.error("Missing user in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing user in request").build();
			}
			
			if (StringUtils.isBlank(expiresStr)) {
				LOG.error("Missing expiryDate in request");
				return Response.status(Status.BAD_REQUEST).entity("Missing expiryDate in request").build();
			}
			
			// If date parse failed, throw error
			if((expires = parseDate(expiresStr)) == null)
			{
				return Response.status(Status.BAD_REQUEST).entity("expiry date format is not valid, usage: "+DATE_FORMAT).build();
			}
			
			if(!expires.after(requestedDate))
			{
				return Response.status(Status.BAD_REQUEST).entity("expiry date must be a future date, not a valid date: "+expiresStr).build();
			}
			
			if(StringUtils.isEmpty(description))
			{
				return Response.status(Status.BAD_REQUEST).entity("Missing description in request").build();
			}
		
			LOG.info("Account ID: " + accountId + " API Key: " + apiKey + " User: " + requestedUser + " Date: " + requestedDate);

			ApiKey updatedApiKey = accountApplicationDAO.updateApiKey(Integer.parseInt(accountId), apiKey, request.getPermissions(), requestedDate, requestedUser, expires, description, request.getStatus());

			if (StringUtils.isBlank(updatedApiKey.getApiKey())) {
				LOG.info("updateApiKey failed for account id: " + accountId);
				return Response.status(Status.NOT_FOUND).entity("updateApiKey failed for account id: " + accountId).build();
			} else {
				LOG.info("Api Key updated successfully for account id: " + accountId);

				ObjectMapper mapper = new ObjectMapper();
				String apiKeyJson = mapper.writeValueAsString(updatedApiKey);

				builder.entity(apiKeyJson);
			}
		} catch (InvalidApiKeyException e) {
			LOG.error("InvalidApiKeyException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (AtleastOneApiKeyRequiredException e) {
			LOG.error("AtleastOneApiKeyRequiredException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateApiKey Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while updating ApiKey").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateApiKey: " + response);
		return response;
	}
	
	/**
	 * Fetch the resources available to the account.
	 * 
	 * @param accountId
	 *            the account identifier
	 * 
	 * @return The json response with the list of resources
	 */
	@Path("/account/{accountId}/apikeys/resources")
	@GET
	public Response getWebResources(@PathParam("accountId") String accountId) {
		LOG.info("Received request to fetch available account resources");
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("Missing accountId in request, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("Missing valid accountId in request").build();
			}

			// Fetch from database
			List<Resource> resources = accountApplicationDAO.findResourcesByAccount(Integer.parseInt(accountId));

			if (resources != null) {
				LOG.info("fetched resources successfully");

				ObjectMapper mapper = new ObjectMapper();
				String resourcesJson = mapper.writeValueAsString(resources);

				builder.entity(resourcesJson);
			} else {
				LOG.info("No resources found for account id : " + accountId);

				return Response.status(Status.NOT_FOUND).entity("No resources found for the accountId: " + accountId).build();
			}
		} catch (Exception e) {
			LOG.error("getResources Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting API Keys").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getApiKeys: " + response);
		return response;
	}
}
