package com.aeris.service.operatoradmin.guice;

import com.aeris.service.common.db.DBPoolManager;
import com.aeris.service.common.exception.RuntimeExceptionMapper;
import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.jmx.CacheManager;
import com.google.inject.Scopes;
import com.google.inject.name.Names;
import com.google.inject.servlet.ServletModule;

/**
 * {@link RestServletModule} servlet module to configure Guice Injector
 * 
 * @author srinivas.puranam@aeris.net
 * @since 24/12/2013
 * 
 */
public class RestServletModule extends ServletModule {

	public RestServletModule() {
	}

	@Override
	protected void configureServlets() {

		// ConfigureServlets
		super.configureServlets();
		Names.bindProperties(binder(), System.getProperties());

		bind(DBPoolManager.class).toInstance(new DBPoolManager() {
			{
				setContextName(OperatorAdminListener.CONTEXT_NAME);
			}
		});

		bind(CacheManager.class).asEagerSingleton();

		// FIXME A better way to map container exception?
		bind(RuntimeExceptionMapper.class).in(Scopes.SINGLETON);
	}
	
	
	/*@Override
	protected final void configureServlets() {
    
		super.configureServlets();
		Map<String, String> params = new HashMap<String, String>();

        //Add Resource Package Scanning feature
        params.put(PackagesResourceConfig.PROPERTY_PACKAGES,
        		"com.aeris.service.operatoradmin.resources");
        
        //Configure Authentication Filter
        params.put(PackagesResourceConfig.PROPERTY_RESOURCE_FILTER_FACTORIES,
        		AuthFilterFactory.class.getName());
        
        Names.bindProperties(binder(), System.getProperties());

		bind(DBPoolManager.class).toInstance(new DBPoolManager() {
			{
				setContextName(OperatorAdminListener.CONTEXT_NAME);
			}
		});

		bind(CacheManager.class).asEagerSingleton();

		bind(RuntimeExceptionMapper.class).in(Scopes.SINGLETON);
        
		filter("/*").through(SwaggerFilter.class);
		serve("/*").with(GuiceContainer.class, params);
	}*/
}
