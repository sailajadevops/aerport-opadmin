package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.enums.IPacketRoundingPolicyEnum;
import com.aeris.service.operatoradmin.enums.impl.PacketRoundingPolicyEnum;

@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD, PARAMETER, TYPE})
@Documented
@Constraint(validatedBy = {ValidPacketRoundingPolicy.IntegerValidator.class, ValidPacketRoundingPolicy.StringValidator.class})
public @interface ValidPacketRoundingPolicy {
	String message() default "{packetRoundingPolicyId.number}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class IntegerValidator implements ConstraintValidator<ValidPacketRoundingPolicy, Integer> {
		
		IPacketRoundingPolicyEnum packetRoundingPolicyEnum;

		@Override
		public void initialize(final ValidPacketRoundingPolicy enumClazz) {
			packetRoundingPolicyEnum = OperatorAdminListener.sInjector.getInstance(PacketRoundingPolicyEnum.class);
		}

		public boolean isValid(final Integer packetRoundingPolicy, final ConstraintValidatorContext constraintValidatorContext) {
			if(packetRoundingPolicy == 0)
			{
				return true;
			}
			
			return packetRoundingPolicyEnum.isValid(String.valueOf(packetRoundingPolicy));
		}
	}

	public class StringValidator implements ConstraintValidator<ValidPacketRoundingPolicy, String> {
		
		IPacketRoundingPolicyEnum packetRoundingPolicyEnum;

		@Override
		public void initialize(final ValidPacketRoundingPolicy enumClazz) {
			packetRoundingPolicyEnum = OperatorAdminListener.sInjector.getInstance(PacketRoundingPolicyEnum.class);
		}

		public boolean isValid(final String packetRoundingPolicy, final ConstraintValidatorContext constraintValidatorContext) {
			if(packetRoundingPolicy == null)
			{
				return true;
			}
			
			if(NumberUtils.isNumber(packetRoundingPolicy))
			{
				return packetRoundingPolicyEnum.isValid(packetRoundingPolicy);
			}
			
			return false;
		}
	}
}
