package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.ICountryEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.Country;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * Country Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class CountryEnum implements ICountryEnum, IEventListener  {
	private Logger LOG = LoggerFactory.getLogger(CountryEnum.class);
	
	static final String SQL_GET_COUNTRIES = "select country_id, country_name from NETWORK_STORE.COUNTRY order by country_id";
	
	private Cache<String, Country> cache;
	private Cache<String, Country> cacheById;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "CountryCache/name") Cache<String, Country> cache, 
			@Hazelcast(cache = "CountryCache/id") Cache<String, Country> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_COUNTRIES);
			
			while (rs.next()) {
				Country country = new Country();
				
				country.setCountryId(rs.getInt("country_id"));
				country.setCountryName(rs.getString("country_name"));
				
				cache.put(country.getCountryName(), country);
				cacheById.put(String.valueOf(country.getCountryId()), country);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}
	

	@Override
	public Country getCountryByName(String name) {
		return cache.get(name);
	}

	@Override
	public Country getCountryById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<Country> getAllCountries() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
