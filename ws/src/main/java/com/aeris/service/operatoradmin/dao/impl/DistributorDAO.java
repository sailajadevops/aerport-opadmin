package com.aeris.service.operatoradmin.dao.impl;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.IDistributorDAO;
import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.DistributorAccountException;
import com.aeris.service.operatoradmin.exception.DistributorDBException;
import com.aeris.service.operatoradmin.exception.DistributorException;
import com.aeris.service.operatoradmin.exception.DistributorNotFoundException;
import com.aeris.service.operatoradmin.exception.SIMPackException;
import com.aeris.service.operatoradmin.exception.SimpackDBException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.Distributor;
import com.aeris.service.operatoradmin.payload.AssignSIMsFromInventoryRequest;
import com.aeris.service.operatoradmin.payload.AssignSIMsFromSIMPACKRequest;
import com.aeris.service.operatoradmin.payload.CreateDistributorCode;
import com.aeris.service.operatoradmin.payload.CreateDistributorRequest;
import com.aeris.service.operatoradmin.payload.CreateSimpackRequest;
import com.aeris.service.operatoradmin.payload.Simpack;
import com.aeris.service.operatoradmin.payload.UpdateDistributorRequest;
import com.aeris.service.operatoradmin.payload.UpdateEmptySIMPACKRequest;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;

/**
 * DAO implementation class for distributor CRUD operations
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class DistributorDAO implements IDistributorDAO {

	private static Logger LOG = LoggerFactory.getLogger(DistributorDAO.class);
	private Cache<Long, Distributor> cache;
	@Inject
	private IAccountDAO accountDAO;
	
	private static final String UPDATE_ORDER_STATUS = "UPDATE aerisgen.customer_order SET status = 5, last_modified_date = sysgmtdate, last_modified_by = ? WHERE order_id = ? AND EXISTS ( " +
	    		"SELECT 1 " +
	    	    "FROM aerisgen.catalog c, aerisgen.customer_order_item_shipment cois, aerisgen.customer_order co " +
	    	    "WHERE cois.order_id = ? AND cois.item_id = c.item_id AND c.category = 'SIM' AND status = 4 AND co.order_id = cois.order_id AND packages_received = packages_sent)";
	
	private static final String SQL_GET_ALL_DISTRIBUTORS = "select * from aerisgen.distributor";

	private static final String SQL_GET_ALL_DISTRIBUTORS_BY_OPERATOR = "select * from aerisgen.distributor where operator_id = ?";

	private static final String SQL_UPDATE_DISTRIBUTOR = "UPDATE aerisgen.distributor set " +
			"DISTRIBUTOR_NAME = ?, " +
			"AERIS_ACCOUNT_ID = ?, " +
			"CONTACT_EMAILS = ?, " +
            "ACCESS_LINK = ?, " +
			"LOGO_URL = ?, " +
			"PRIVACY_POLICY_URL = ?, " +
			"STATUS = ?, " +
            "LIFERAY_COMPANY_ID = ?, "+
            "STORE_URL = ?, "+
            "IS_WHOLESALE = ?, "+
            "BRAND = ?, " +
            "PLAN_PRICING_URL = ?, " +
            "REALM = ?, " +
			"LAST_MODIFIED_BY = ?, " +
			"LAST_MODIFIED_DATE = sysgmtdate, " +
			"REP_TIMESTAMP = systimestamp " +			
			"where distributor_id = ? and operator_id = ?";

	private static final String SQL_CREATE_DISTRIBUTOR = "INSERT INTO aerisgen.distributor (" +
			"DISTRIBUTOR_ID, " +
			"OPERATOR_ID, " +
			"DISTRIBUTOR_NAME, " +
			"AERIS_ACCOUNT_ID, " +
			"CONTACT_EMAILS, " +
            "ACCESS_LINK, " +
			"LOGO_URL, " +
			"PRIVACY_POLICY_URL, " +
			"STATUS, " +
            "LIFERAY_COMPANY_ID, "+
            "STORE_URL, "+
            "IS_WHOLESALE, "+
            "BRAND, " +
            "PLAN_PRICING_URL, " +
            "REALM, " +
			"CREATED_BY, " +
			"CREATED_DATE, " +
			"LAST_MODIFIED_BY, " +
			"LAST_MODIFIED_DATE, " +
			"REP_TIMESTAMP )" +
			"values " +
			"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, sysgmtdate, ?, sysgmtdate, systimestamp)";

	private static final String SQL_DELETE_DISTRIBUTOR = "delete from aerisgen.distributor where distributor_id = ? and operator_id = ?";

	private static final String SQL_GET_DISTRIBUTOR = "select * from aerisgen.distributor where operator_id = ? and distributor_id = ?";
	
	private static final String SQL_GET_DISTRIBUTOR_BY_AERIS_ACCOUNT_ID = "select * from aerisgen.distributor where operator_id = ? and aeris_account_id = ?";
	
	private final static String UPDATE_CINGULARSIM_ASSIGNED_QUERY = "update sim_inventory.cingular_sim_assigned " +
			" set account_id = ?, assigne_date = sysgmtdate, last_change = sysgmtdate, last_changed_by = ? " +
			" where iccid = ? and order_number = ?";
	
	private final static String CREATE_SIMPACK_FOR_DISTRIBUTOR_QUERY = "INSERT INTO sim_inventory.SIMPACK (REP_TIMESTAMP,SEQ_NO, UNIQUE_ID, USED, "
			+ "ICCID_START, ICCID_END, PACKET_SIZE, DISTRIBUTOR_ID, ICCID_ASSIGNED)"
			+ "VALUES(sys_extract_utc(systimestamp),SIM_PACK_SEQ.nextval,?,?,?,?,?,?,?)";
	
	private final static String SQL_INSERT_SIMPACK_QUERY = "INSERT INTO sim_inventory.SIMPACK ("
            + "REP_TIMESTAMP, " // 
            + "SEQ_NO, " // 
            + "UNIQUE_ID, " // 1
            + "USED, " // 2
            + "ICCID_START, " // 3
            + "ICCID_END, " // 4
            + "PACKET_SIZE, " // 5
            + "SIM_TYPE, " // 6
            + "OPERATOR_ID, " // 7
            + "ACCOUNT_ID, " // 8
            + "DISTRIBUTOR_ID, " // 9
            + "ICCIDS, " // 10
            + "CREATED_BY, " // 11
            + "CREATED_DATE, " // 12
            + "LAST_MODIFIED_BY, " // 13
            + "LAST_MODIFIED_DATE" // 14
            + ")"
			+ " VALUES("
            + "sys_extract_utc(systimestamp),"
            + "SIM_PACK_SEQ.nextval,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "?,"
            + "sysgmtdate,"
            + "?,"
            + "sysgmtdate)";
	
	/**
	 */
	
	private final String INSERT_SIM_INVENTORY_SQL = "insert into sim_inventory.cingular_sim_assigned " +
			"(iccid, imsi, order_number, account_id, assigne_date, last_change, last_changed_by, carrier) " +
			"values (?, ?, ?, ?, sysgmtdate, sysgmtdate, ?, ?)";
	
	private String SELECT_SIMASSIGNMENT_TO_ACCOUNT_SQL = "select count(*) from sim_inventory.cingular_sim_assigned where iccid in (ICCID_LIST)";
	
	private final String UPDATE_ICCIDS_TO_SIMPACK_SQL = "update sim_inventory.SIMPACK set iccid_start = ?, iccid_end = ?, ICCID_ASSIGNED = ?, SIM_TYPE = ? " +
			" where OPERATOR_ID = ? and UNIQUE_ID = ? and DISTRIBUTOR_ID = ?";
	
	private final static String GET_SIMPACKS_SQL = "SELECT seq_no, unique_id, iccid_start, iccid_end, iccids, packet_size, sim_type "
			+ "FROM sim_inventory.simpack WHERE operator_id = ? and used = ?";
	
	private final String GET_SIMPACK_BY_UNIQUE_ID_SQL = "SELECT seq_no, unique_id, iccid_start, iccid_end, packet_size, sim_type "
			+ "FROM sim_inventory.simpack WHERE unique_id = ? and used = 1";
	
	private final String UPDATE_CINGULAR_SIM_INVENTORY_SQL = "update sim_inventory.cingular_sim_inventory set assign_date = sysgmtdate where iccid in ";
	
	
	String UPDATE_SIMPACK_FOR_DISTRIBUTOR_SQL = "INSERT INTO sim_inventory.SIMPACK (REP_TIMESTAMP,SEQ_NO, UNIQUE_ID, USED, DISTRIBUTOR_ID, CREATED_BY)"
			+ "VALUES(sys_extract_utc(systimestamp),SIM_PACK_SEQ.nextval,?,?,?,?)";

	public void init(@Hazelcast(cache = "DistributorCache/id") Cache<Long, Distributor> cache) {
		this.cache = cache;
		List<Distributor> distributorList = new ArrayList<Distributor>();
		try {
			distributorList = getAllDistributors();
		} catch (DistributorDBException e) {
			LOG.error("Error while updating distriburor cache", e);
		}
		putInCache(distributorList);
	}


	private void putInCache(List<Distributor> accountsList) {
		for (Distributor d : accountsList) {
			this.cache.put(d.getDistributorId(), d);
		}
	}
    
	public void put(Long distributorId, Distributor d){
		this.cache.put(distributorId, d);
	}
	public Distributor get(Long distributorId){
		return this.cache.get(distributorId);
	}

	private List<Distributor> getAllDistributors() throws DistributorDBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List<Distributor> distributorList = new ArrayList<Distributor>();
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			ps = conn.prepareStatement(SQL_GET_ALL_DISTRIBUTORS);
			rs = ps.executeQuery();
			while (rs.next()) {
				distributorList.add(mapRow(rs));
			}
			LOG.debug("Total distributors fetch count= " + distributorList.size());
		} catch(Exception e) {
			throw new DistributorDBException("Exception while fetching distributors. DB error");
		} finally {
			DBUtils.cleanup(conn, ps);
		}
		return distributorList;
	}


	/**
	 * maps a single row from result-set to Distributor Model object
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Distributor mapRow(ResultSet rs) throws SQLException {
		Distributor distributor = new Distributor();
		distributor.setDistributorId(rs.getLong("DISTRIBUTOR_ID"));
		distributor.setAccountId(rs.getLong("AERIS_ACCOUNT_ID"));
        distributor.setOperatorId(rs.getLong("OPERATOR_ID"));
		distributor.setCreatedBy(rs.getString("CREATED_BY"));
		distributor.setLastModifiedBy(rs.getString("LAST_MODIFIED_BY"));
		distributor.setStatus(rs.getInt("STATUS"));
		distributor.setCreatedDate(rs.getDate("CREATED_DATE"));
		distributor.setLastModifiedDate(rs.getDate("LAST_MODIFIED_DATE"));
		distributor.setRepTimestamp(rs.getTimestamp("REP_TIMESTAMP"));
        distributor.setAccessLink(rs.getString("ACCESS_LINK"));
		distributor.setLogoUrls(rs.getString("LOGO_URL"));
		distributor.setPrivacyPolicyUrls(rs.getString("PRIVACY_POLICY_URL"));
		distributor.setDistributorName(rs.getString("DISTRIBUTOR_NAME"));
		distributor.setWholesale(rs.getBoolean("IS_WHOLESALE"));
		String contactEmailStr = rs.getString("CONTACT_EMAILS");
		if(!StringUtils.isEmpty(contactEmailStr)) {
			distributor.setContactEmails(Arrays.asList(contactEmailStr.split(",")));
		}
        distributor.setLiferayCompanyId(rs.getLong("LIFERAY_COMPANY_ID"));
        distributor.setStoreUrl(rs.getString("STORE_URL"));
        distributor.setPlanPricingUrl(rs.getString("PLAN_PRICING_URL"));
        distributor.setBrand(rs.getString("BRAND"));
        distributor.setRealm(rs.getString("REALM"));
		return distributor ;
	}


	@Override
	public List<Distributor> getAllDistributors(final long operatorId) throws DistributorDBException, Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Distributor> distributors = new ArrayList<Distributor>();
		if(cache.getValues() != null ) {
			return new ArrayList<Distributor>(Collections2.filter(cache.getValues(), new Predicate<Distributor>() {
				@Override
				public boolean apply(Distributor input) {
					return operatorId == (input.getOperatorId());
				}
			}));
		}
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			StringBuilder query = new StringBuilder(SQL_GET_ALL_DISTRIBUTORS_BY_OPERATOR);
			ps = conn.prepareStatement(query.toString());
			ps.setLong(1, Long.valueOf(operatorId));
			rs = ps.executeQuery();
			while (rs.next()) {
				distributors.add(mapRow(rs));
			}
			putInCache(distributors);
		} catch(SQLException e) {
            LOG.error("Unable to fetch distributors: DB Error", e);
            throw new DistributorDBException("Unable to fetch distributors: DB Error " + operatorId, e);
		} catch(Exception e) {
            LOG.error("Exception occurred while fetching all distributors", e);
            throw new DistributorException("Exception occurred while fetching all distributors ", e);
		} finally {
            DBUtils.cleanup(conn, ps, rs);
		}
		return distributors;

	}

	@Override
	public Distributor getDistributor(long operatorId, long distributorId) throws DistributorNotFoundException, DistributorDBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Distributor distributor = null;
		if(cache.getValues() != null && cache.get(distributorId) != null) {
			distributor = cache.get(distributorId);
			LOG.debug("distributor ID " +  distributor.getDistributorId() +" distributor operatorId " + distributor.getOperatorId());
			if(distributor.getOperatorId() == operatorId)
				return distributor ;
			else 
				throw new DistributorNotFoundException("Distributor does not belong to operator");
		}
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			ps = conn.prepareStatement(SQL_GET_DISTRIBUTOR);
			ps.setLong(1, Long.valueOf(operatorId));
			ps.setLong(2, distributorId);
			rs = ps.executeQuery();
			while (rs.next()) {
				distributor = mapRow(rs);
                cache.put(distributorId, distributor);
			}
		} catch(SQLException e) {
            LOG.error("SQLException occurred while fetching distributor with distributorId {}", distributorId, e);
			throw new DistributorDBException("SQLException while fetching distributor with distributorId=" + distributorId, e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
        if (distributor == null) {
            throw new DistributorNotFoundException("No distributor exists with distributorId " + distributorId, null);
	}
		return distributor;
	}
	@Override
	public Distributor getDistributorByAerisAccountId(long operatorId, long aerisAccountId) throws DistributorNotFoundException, DistributorDBException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Distributor distributor = null;		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			ps = conn.prepareStatement(SQL_GET_DISTRIBUTOR_BY_AERIS_ACCOUNT_ID);
			ps.setLong(1, Long.valueOf(operatorId));
			ps.setLong(2, aerisAccountId);
			rs = ps.executeQuery();
			while (rs.next()) {
				distributor = mapRow(rs);
			}
		} catch(SQLException e) {
            LOG.error("SQLException occurred while fetching distributor with aerisAccountId {}", aerisAccountId, e);
			throw new DistributorDBException("SQLException while fetching distributor with aerisAccountId=" + aerisAccountId, e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
        if (distributor == null) {
            throw new DistributorNotFoundException("No distributor exists with aerisAccountId " + aerisAccountId, null);
	}
		return distributor;
	}


	@Override
	public Distributor createNewDistributor(long operatorId, CreateDistributorRequest request) throws DistributorException, DistributorDBException {
		Connection conn = null;
		PreparedStatement ps = null;
		Distributor distributor =  null ;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			ps = conn.prepareStatement(SQL_CREATE_DISTRIBUTOR);
			long distributorId = generateDistributorId(conn, operatorId);
			if(distributorId < 1) {
				throw new DistributorDBException("Exception while generating distributorId. DB error");
			}
			ps.setLong(1, distributorId);
			ps.setLong(2, Long.valueOf(operatorId));
			ps.setString(3, request.getDistributorName());
			ps.setLong(4, request.getAccountId());
			ps.setString(5, StringUtils.join(request.getContactEmails(), ','));
            ps.setString(6, request.getAccessLink());
			ps.setString(7, request.getLogoUrls());
			ps.setString(8, request.getPrivacyPolicyUrls());
			ps.setInt(9, request.getStatus());
            ps.setLong(10, request.getLiferayCompanyId());
            ps.setString(11, request.getStoreUrl());
            ps.setBoolean(12, request.isWholesale());
            ps.setString(13, request.getBrand());
            ps.setString(14, request.getPlanPricingUrl());
            ps.setString(15, request.getRealm());
			ps.setString(16, request.getUserId());
			ps.setString(17, request.getUserId());
			int count = ps.executeUpdate();
			if(count != 1) {
				conn.rollback();
				throw new DistributorDBException("Exception while saving distributor. DB error");
			}
			distributor = new Distributor();
			distributor.setOperatorId(Long.valueOf(operatorId));
			distributor.setDistributorId(distributorId);
			distributor.setDistributorName(request.getDistributorName());
			distributor.setAccountId(request.getAccountId());
			distributor.setCreatedBy(request.getUserId());
			distributor.setLastModifiedBy(request.getUserId());
			distributor.setStatus(request.getStatus());
			distributor.setCreatedDate(request.getCreatedDate());
			distributor.setLastModifiedDate(request.getLastModifiedDate());
			distributor.setRepTimestamp(request.getRepTimestamp());
            distributor.setAccessLink(request.getAccessLink());
			distributor.setLogoUrls(request.getLogoUrls());
			distributor.setPrivacyPolicyUrls(request.getPrivacyPolicyUrls());
			distributor.setContactEmails(request.getContactEmails());
            distributor.setLiferayCompanyId(request.getLiferayCompanyId());
            distributor.setStoreUrl(request.getStoreUrl());
            distributor.setWholesale(request.isWholesale());
            distributor.setBrand(request.getBrand());
            distributor.setPlanPricingUrl(request.getPlanPricingUrl());
            distributor.setRealm(request.getRealm());
			cache.put(distributor.getDistributorId(), distributor);
		} catch(SQLException e) {
            LOG.error("SQLException occurred while creating distributor", e);
			throw new DistributorDBException("Unable to fetch distributors: DB Error", e);
		} catch (Exception e) {
            LOG.error("Exception while creating distributor", e);
            throw new DistributorException("Exception while creating distributor", e);
		} finally {
			DBUtils.cleanup(conn, ps);
		}
		return distributor ;
	}


	/**
	 * Generates distributorId next-val for distributor creation
	 * 
	 * @param conn
	 * @param operatorId
	 * @return
	 * @throws SQLException
	 */
	private long generateDistributorId(Connection conn, long operatorId) throws DistributorDBException {
		long newDistributorId = -1;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("createNewDistributor: fetching the next distributorId for new distributor creation");
			ps = conn.prepareStatement("SELECT aerisgen.distributor_id_seq.nextval FROM dual");
			rs = ps.executeQuery();
			if (rs.next()) {
				newDistributorId = rs.getLong(1);
			}
			LOG.info("DistributorId generated: " + newDistributorId);
		} catch (SQLException e) {
			LOG.error("Exception during createNewDistributor", e);
			throw new DistributorDBException("Unable to create distributorId. DB error occured.", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		return newDistributorId;
	}


	@Override
	public Distributor updateDistributor(long operatorId,long distributorId, UpdateDistributorRequest request) throws DistributorNotFoundException, DistributorDBException {
		Connection conn = null;
		PreparedStatement ps = null;
		Distributor distributor = null ;
		Distributor distDtls = cache.get(distributorId);
		if(distDtls == null ) {
			throw new DistributorNotFoundException("Distributor not found for distributorId=" + distributorId);
		}
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			resetAutoCommit(conn, false);
			ps = conn.prepareStatement(SQL_UPDATE_DISTRIBUTOR);
			ps.setString(1, request.getDistributorName());
			ps.setLong(2, request.getAccountId());
			ps.setString(3, StringUtils.join(request.getContactEmails(), ','));
            ps.setString(4, request.getAccessLink());
			ps.setString(5, request.getLogoUrls());
			ps.setString(6, request.getPrivacyPolicyUrls());
			ps.setInt(7, request.getStatus());
            ps.setLong(8, request.getLiferayCompanyId());
            ps.setString(9, request.getStoreUrl());
            ps.setBoolean(10, request.isWholesale());
            ps.setString(11, request.getBrand());
            ps.setString(12, request.getPlanPricingUrl());
            ps.setString(13, request.getRealm());
			ps.setString(14, request.getUserId());
			ps.setLong(15, distributorId);
			ps.setLong(16, operatorId);
			int updated = ps.executeUpdate();
			if(updated != 1) {
				conn.rollback();
				throw new DistributorDBException("Exception while updating distributor. DB error");
			}
			conn.commit();
			distributor = new Distributor();
			distributor.setDistributorId(distributorId);
			distributor.setAccountId(request.getAccountId());
			distributor.setCreatedBy(distDtls.getCreatedBy());
			distributor.setLastModifiedBy(request.getUserId());
			distributor.setStatus(request.getStatus());
			distributor.setCreatedDate(request.getCreatedDate());
            distributor.setAccessLink(request.getAccessLink());
			distributor.setLogoUrls(request.getLogoUrls());
			distributor.setPrivacyPolicyUrls(request.getPrivacyPolicyUrls());
			distributor.setContactEmails(request.getContactEmails());
            distributor.setLiferayCompanyId(request.getLiferayCompanyId());
            distributor.setStoreUrl(request.getStoreUrl());
            distributor.setWholesale(request.isWholesale());
            distributor.setBrand(request.getBrand());
            distributor.setPlanPricingUrl(request.getPlanPricingUrl());
            distributor.setDistributorName(request.getDistributorName());
            distributor.setRealm(request.getRealm());
			cache.put(distributor.getDistributorId(), distributor);
		} catch(SQLException e) {
            LOG.error("SQLException occurred while updating distributor with distributorId {}", distributorId, e);
			throw new DistributorDBException("Error occurred while updating distributor with distributorId " + distributorId, e);
		} finally {
			resetAutoCommit(conn, true);
			DBUtils.cleanup(conn, ps);
		}
		return distributor;
	}


	@Override
	public boolean deleteDistributor(long operatorId, long distributorId, Date requestedDate, String requestedUser)
			throws DistributorNotFoundException, DistributorException, DistributorDBException {
		Connection conn = null;
		PreparedStatement ps = null;
		Distributor dist = cache.get(distributorId);
		if(dist == null) {
			throw new DistributorNotFoundException("Distributor not found for distributorId=" + distributorId);
		}
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			resetAutoCommit(conn, false);
			ps = conn.prepareStatement(SQL_DELETE_DISTRIBUTOR);
			ps.setLong(1, distributorId);
			ps.setLong(2, operatorId);
			int count = ps.executeUpdate();
			if(count != 1) {
				conn.rollback();
				throw new DistributorDBException("Exception while deleting distributor. DB error");
			}
			conn.commit();
			cache.evict(distributorId);
			return true ;
		} catch(Exception e) {
			throw new DistributorDBException("Distributor not found with distributorId:" + distributorId , e);
		} finally {
			resetAutoCommit(conn, true);
			DBUtils.cleanup(conn, ps);
		}
	}

	
	/**
	 * Creates unique distributor-code list and inserts to simpack table entry
	 * 
	 * @param distributorId
	 * @param request
	 * @throws SQLException
	 */
	@Override
	public List<String> createEmptySimpacks4Distributor(long operatorId, long distributorId, CreateDistributorCode request) 
			throws DistributorNotFoundException, DistributorException {

		Connection conn = null;
		PreparedStatement preparedStatement = null;
		List<String> distributorCodeList = new ArrayList<String>();
		Account account = accountDAO.getAccount(String.valueOf(operatorId), String.valueOf(distributorId));
		if(account == null) {
			throw new DistributorNotFoundException("Account not found, distributorId=" + distributorId);
		}
		try {
			
			conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
			List<String> uniqueCodes = getUniqueIds(request.getNumberOfCodes());
			preparedStatement = conn.prepareStatement(CREATE_SIMPACK_FOR_DISTRIBUTOR_QUERY);
			for (String uniqueId : uniqueCodes) {
				preparedStatement.setString(1, uniqueId);
				preparedStatement.setString(2, "1");
				preparedStatement.setNull(3, Types.VARCHAR);
				preparedStatement.setNull(4, Types.VARCHAR);
				preparedStatement.setNull(5, Types.INTEGER);
				preparedStatement.setLong(6, distributorId);
				preparedStatement.setNull(7, Types.VARCHAR);
				preparedStatement.addBatch();
				distributorCodeList.add(uniqueId);
			}
			preparedStatement.executeBatch();
			conn.commit();
			//TODO : send email notification with uniqueCodes
		} catch (Exception e) {
			LOG.error("Error while saving distributorCodes to simpack", e);
			throw new DistributorException("Exception while saving unique distributorCodes to HLR database", e);
		} finally {
			DBUtils.cleanup(conn, preparedStatement, null);
		}
		LOG.info("Insert completed for all " + request.getNumberOfCodes() + " records");

		return distributorCodeList;
	}
	
	
	/**
	 * Generates uniqueIds non-existent in database, namely simpack table
	 * 
	 * @param count
	 * @return
	 * @throws SimpackDBException
	 */
	private List<String> getUniqueIds(int count) throws SimpackDBException {
		
		List<String> uniqueCodes = new ArrayList<String>();
		if(count == 0) return uniqueCodes;
		while(uniqueCodes.size() != count) {
			String randomDigit = RandomStringUtils.randomNumeric(5);
			String uniqueId = randomDigit + OAUtils.generateDigit(randomDigit);
	    	Connection conn = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try {
	        	conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
	            ps = conn.prepareStatement("SELECT count(*) FROM sim_inventory.simpack WHERE unique_id = ? ");
	            ps.setString(1, uniqueId);
	            rs = ps.executeQuery();
	            if(rs.next()) {
	            	if(rs.getInt(1) == 0 ){
	            		uniqueCodes.add(uniqueId);
	            	}
	            }
	        } catch (SQLException e) {
	        	LOG.error("Failed to fetch uniqueId from DB ", e);
	        	throw new SimpackDBException("Exception while validating uniqueId. DB error occured", e); 
	        } finally {
	        	DBConnectionManager.getInstance().cleanup(conn, ps, rs);
	        }
	    
		}
		return uniqueCodes ;
	}
	
	
    @Override
	public List<Simpack> createSIMPacksForOperator(long operatorId, CreateSimpackRequest request) throws SIMPackException, DistributorDBException, SimpackDBException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		List<Simpack> simpackList = new ArrayList<Simpack>();
		
		validateSimpackCount(request);
        
        Date requestedDate = new Date();
		
		try {
			//Ignore the last digit from the iccid range
			BigInteger iccidStart = new BigInteger(request.getIccidStart().substring(0, request.getIccidStart().length() - 1));
			BigInteger iccidEnd = new BigInteger(request.getIccidEnd().substring(0, request.getIccidEnd().length() - 1));
			BigInteger packSize = new BigInteger("" + request.getPackSize());
			conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
			resetAutoCommit(conn, false);
			preparedStatement = conn.prepareStatement(SQL_INSERT_SIMPACK_QUERY);
			List<String> uniqueIds = getUniqueIds(request.getPackCount());
			int counter = 0 ;
			for(String uniqueId : uniqueIds) {
				preparedStatement.setString(1, uniqueId);
				preparedStatement.setString(2, "1");
				if(counter != 0) {
					iccidStart = iccidStart.add(packSize) ;
				}
				//validate each batch of iccids before they are processed
				validateSimAssignmentToAccount(prepareICCIDList(request.getIccidStart(), request.getIccidEnd(), request.getIccids())); 
				iccidEnd = iccidStart.add(new BigInteger("" + (request.getPackSize() - 1))) ;
				preparedStatement.setString(3, String.valueOf(iccidStart));
				preparedStatement.setString(4, String.valueOf(iccidEnd));
				preparedStatement.setLong(5, request.getPackSize());
				preparedStatement.setString(6, request.getSimType());
				preparedStatement.setLong(7, Long.valueOf(operatorId));
				if(StringUtils.isEmpty(request.getAccountId())) {
					preparedStatement.setNull(8, Types.NUMERIC);
				} else {
					preparedStatement.setLong(8, Long.valueOf(request.getAccountId()));
				}
				if(StringUtils.isEmpty(request.getAccountId())) {
					preparedStatement.setNull(9, Types.NUMERIC);
				} else {
					preparedStatement.setLong(9, Long.valueOf(request.getDistributorId()));
				}
                //TODO check clob datatype
                preparedStatement.setString(10, request.getIccids());
                preparedStatement.setString(11, request.getUserId());
                preparedStatement.setString(12, request.getUserId());
				Simpack simpack = new Simpack();
				simpack.setSimpackId(uniqueId);
				simpack.setIccidStart(String.valueOf(iccidStart));
				simpack.setIccidEnd(String.valueOf(iccidEnd));
				simpack.setSimType(request.getSimType());
				simpack.setPackSize(request.getPackSize());
                simpack.setAccountId(request.getAccountId());
                simpack.setOperatorId(String.valueOf(operatorId));
                simpack.setDistributorId(request.getDistributorId());
                simpack.setIccids(request.getIccids());
                simpack.setCreatedBy(request.getUserId());
                simpack.setLastModifiedBy(request.getUserId());
                simpack.setIccids(request.getIccids());
                simpack.setCreatedDate(new java.sql.Date(requestedDate.getTime()));
                simpack.setLastModifiedDate(new java.sql.Date(requestedDate.getTime()));
				simpackList.add(simpack);
				preparedStatement.addBatch();
				counter++ ;
			}
			preparedStatement.executeBatch();
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOG.error("Exception while rollback", e);
			}
			LOG.error("Unable to fetch simpacks: DB Error", e);
			throw new DistributorDBException("Error while inserting uniqueCodes for operator. DB error occured", e ) ;
		} finally {
			resetAutoCommit(conn, true);
			DBUtils.cleanup(conn, preparedStatement, null);
		}
		LOG.info("Insert completed for all " + request.getPackCount() + " records");

		return simpackList;
	}

	
	/**
	 * Validates simpack count and packSize against the sim range
	 * 
	 * @param request
	 * @throws SIMPackException
	 */
	private void validateSimpackCount(CreateSimpackRequest request) throws SIMPackException {
		if(StringUtils.isEmpty(request.getIccidStart()) || StringUtils.isEmpty(request.getIccidEnd())){
			throw new SIMPackException("IccidStart/IccidEnd not found ..");
		}
		BigInteger iccidStart = new BigInteger(request.getIccidStart().substring(0, request.getIccidStart().length() - 1));
		BigInteger iccidEnd = new BigInteger(request.getIccidEnd().substring(0, request.getIccidEnd().length() - 1));
		BigInteger packSize = new BigInteger("" + request.getPackSize());
		BigInteger packCount = new BigInteger("" +request.getPackCount());
		BigInteger totalIccidCount = iccidEnd.subtract(iccidStart);
		BigInteger totalOfPacks =  packSize.multiply(packCount) ;
		if(!totalIccidCount.equals(totalOfPacks)) {
			throw new SIMPackException("Iccid range packSize/packCount does not match ..");
		}
	}
	
    
    @Override
    public boolean assignSIMsToAccount(long operatorId, String accountId, AssignSIMsFromInventoryRequest request) 
    		throws AccountNotFoundException, SIMPackException, DistributorDBException, DistributorAccountException {
    	return assignSIMsToAccount(operatorId, accountId, request.getIccidStart(), request.getIccidEnd(), request.getIccids(), 
    			request.getOrderId(), request.getOrderId());
    }
    
    
    /**
     * Assigns sims to a distributor account
     * 
     * @param operatorId
     * @param accountId
     * @param iccidStart
     * @param iccidEnd
     * @param iccids
     * @param orderId
     * @param userId
     * @return
     * @throws AccountNotFoundException
     * @throws SIMPackException
     * @throws DistributorDBException
     * @throws DistributorAccountException
     */
    public boolean assignSIMsToAccount(long operatorId, String accountId, String iccidStart, String iccidEnd, String iccids, String orderId, String userId) 
    		throws AccountNotFoundException, SIMPackException, DistributorDBException, DistributorAccountException {

    	Connection conn = null;
    	PreparedStatement ps = null;
    	
    	List<String> iccidList = prepareICCIDList(iccidStart, iccidEnd, iccids);

    	validateSimAssignmentToAccount(iccidList);
    	
    	conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
    	resetAutoCommit(conn, false);
    	try {
    		ps = conn.prepareStatement(INSERT_SIM_INVENTORY_SQL); 
    		for (String iccid : iccidList) {
    			List<String> imsiAndCarrier = getIMSIAndCarrierforICCID(iccid);
    			ps.setString(1, iccid);
    			ps.setString(2, imsiAndCarrier.get(0));
    			ps.setLong(3, Long.valueOf(orderId));
    			ps.setLong(4, Long.valueOf(accountId));
    			ps.setString(5, userId);
    			ps.setString(6, imsiAndCarrier.get(1));
    			ps.addBatch();
    		}
    		int[] count = ps.executeBatch();
    		if(count.length != iccidList.size()) {
    			conn.rollback();
    			throw new SimpackDBException("Unable to update the complete iccid range. DB error");
    		}
    		//update the cingularSimInventory as well with the iccids
    		updateCingularSimInventory(conn, iccidList, accountId);
    		conn.commit();
    	} catch (Exception e) {
    		try {
				conn.rollback();
			} catch (SQLException e1) {
				LOG.error("Exception while invoking rollback. DB error", e);
			}
    		LOG.error("Failed to allocate SIM for orderId {}, accou;2ntId {}", orderId, accountId, e);
    		throw new DistributorAccountException("Failed to allocate SIM", e);
    	} finally {
    		//reset the commit settings again
    		resetAutoCommit(conn, true);
    		DBUtils.cleanup(conn, ps, null);
    	}
    	return true;
    }



	/**
	 * Prepares list of ICCID string using on IccidStart/IccidEnd or commma separated iccid string
	 * 
	 * @param request
	 * @return
	 * @throws DistributorException
	 */
	private List<String> prepareICCIDList(String iccidStartStr, String iccidEndStr, String iccidsStr) throws SIMPackException {
		
		BigInteger iccidStart = null;
    	BigInteger iccidEnd = null ;
    	String iccids = iccidsStr ;
    	List<String> iccidList = new ArrayList<String>();
    	if(!StringUtils.isEmpty(iccidStartStr) && !StringUtils.isEmpty(iccidEndStr)) {
    		iccidStart = new BigInteger(iccidStartStr.substring(0, iccidStartStr.length() - 1));
    		iccidEnd = new BigInteger(iccidEndStr.substring(0, iccidEndStr.length() - 1));
    		BigInteger count = iccidEnd.subtract(iccidStart);
    		for(int i = 0; i < count.intValue(); i++) {
    			iccidList.add("" + (iccidStart.add(new BigInteger(String.valueOf(i)))));
    		}
    	} else if (!StringUtils.isEmpty(iccids)) {
    		iccidList.addAll(Arrays.asList(StringUtils.stripAll(iccids.split(","), " ")));
    	} else {
    		throw new SIMPackException("No ICCIDS found to assign to account ..");
    	}
		return iccidList;
	}
    
    
    /**
     * Updates the sim inventory with latest date
     * 
     * @param conn
     * @param iccidList
     * @param accountId
     * @throws SimpackDBException
     */
    @SuppressWarnings("resource")
	private void updateCingularSimInventory(Connection conn, List<String> iccidList, String accountId) throws SimpackDBException {
    	
    	Statement stmt = null ;
    	try {
    		stmt = conn.prepareStatement(UPDATE_CINGULAR_SIM_INVENTORY_SQL);
    		List<List<String>> partitionedList = Lists.partition(iccidList, 1000);
    		for (List<String> list : partitionedList) {
    			String query = UPDATE_CINGULAR_SIM_INVENTORY_SQL + "(" + OAUtils.getOracleINString(iccidList) + ")";
    			stmt = conn.createStatement();
    			int countUpdated = stmt.executeUpdate(query);
    			LOG.debug("Updated {} iccids " , countUpdated);
    			if (countUpdated != iccidList.size()) {
    				LOG.error("Allocated only {} out of {} for account {}", countUpdated,  list.size(), accountId);
    				throw new DistributorException("Failed to update all sims. DB error");
    			}
			}
    	} catch (Exception e) {
    		try {
				conn.rollback();
			} catch (SQLException e1) {
				LOG.error("Exception while rolling back. DB connection error", e);
			}
    		LOG.error("Failed to update sim_inventory, accountId {}", accountId, e);
    		throw new SimpackDBException("Failed to update cingular_sim_inventory. DB error", e);
    	} finally {
    		DBUtils.cleanup(null, stmt, null);
    	}
    }
    
    
    /**
     * validates sim assignment to distributor account
     * 
     * @param iccidList
     * @throws SIMPackException
     */
    private void validateSimAssignmentToAccount(List<String> iccidList) throws SIMPackException {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<List<String>> partitionList = Lists.partition(iccidList, 1000);
		for (List<String> list : partitionList) {
			String iccids = OAUtils.getOracleINString(list);
			String query = SELECT_SIMASSIGNMENT_TO_ACCOUNT_SQL.replaceAll("ICCID_LIST", iccids);
			try {
				DBConnectionManager connectionManager = DBConnectionManager.getInstance();
				conn = connectionManager.getBillingDatabaseConnection();
				ps = conn.prepareStatement(query);
				rs = ps.executeQuery();
				if(rs.next())
					if(rs.getInt(1) > 0) {
						throw new DistributorException("ICCID already assigned to other account.. please choose another one");
					}
			} catch(Exception e) {
				throw new SIMPackException("Exception while validating iccid list ..", e);
			} finally {
				DBUtils.cleanup(conn, ps, rs);
			}
		}
	
    }
    
    
    /**
     * Gets distributorId for accountId null if no distributorId is associated yet
     * 
     * @param accountId
     * @return
     * @throws DistributorException
     */
    public Long getDistributorId(String accountId) throws DistributorException {

    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	Long distributorId = null ;
    	String getSimAssignmentToAccountQuery = "select distributor_id from AERISGEN.WS_ACCTS_ADDITIONAL_INFO where account_id = " + accountId; 
    	try {
    		DBConnectionManager connectionManager = DBConnectionManager.getInstance();
    		conn = connectionManager.getProvisionDatabaseConnection();
    		ps = conn.prepareStatement(getSimAssignmentToAccountQuery);
    		rs = ps.executeQuery();
    		if(rs.next())
    			distributorId = rs.getLong("distributor_id");
    	} catch(Exception e) {
    		throw new DistributorException("Exception while validating distributorId for account=" + accountId, e);
    	} finally {
    		DBUtils.cleanup(conn, ps, rs);
    	}
    	return distributorId;

    }
    
    
    @Override
    public boolean assignSIMsFromSIMPACKToAccount(long operatorId, String accountId, AssignSIMsFromSIMPACKRequest request) throws 
    	AccountNotFoundException, SIMPackException, DistributorNotFoundException, DistributorException, DistributorAccountException, DistributorDBException {
    	Connection conn = null;
    	Connection provConn = null ;
    	PreparedStatement ps = null;
    	Simpack simpack = getSimPackByUniqueId(request.getSimpackId());
    	if(simpack == null) {
    		throw new SIMPackException(String.format("SimpackId %s not found", request.getSimpackId()));
    	}
    	Long distributorId = getDistributorId(accountId);
    	try {
    		resetAutoCommit(conn, false);
    		if(distributorId != null) {
    			Distributor distributor = getDistributor(operatorId, distributorId);
    			long distributorAccountId = distributor.getAccountId();
    			validateSimpackIdForDistributor(Long.valueOf(simpack.getSimpackId()), distributorId);
    			List<String> iccidList = prepareICCIDList(simpack.getIccidStart(), simpack.getIccidEnd(), simpack.getIccids());
    			conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
    			ps = conn.prepareStatement(UPDATE_CINGULARSIM_ASSIGNED_QUERY);
    			for (String iccid : iccidList) {
        			ps.setLong(1, distributorAccountId);
        			ps.setString(2, request.getUserId());
        			ps.setString(3, iccid);
        			ps.setString(4, request.getOrderId());
        			ps.addBatch();
        		}
        		ps.executeBatch();
        		conn.commit();
        		return true ;
    		} else {
    			provConn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
    			resetAutoCommit(provConn, false);
    			updateOrderStatus(provConn, Long.valueOf(request.getOrderId()), request.getUserId());
    			boolean simAssigned = assignSIMsToAccount(operatorId, accountId, simpack.getIccidStart(), simpack.getIccidEnd(), 
    					simpack.getIccids(), request.getOrderId(), request.getUserId());
    			if(simAssigned)
    				provConn.commit();
    			return simAssigned ;
    		}
    	} catch(DistributorException e) {
    		throw e ;
    	} catch(SQLException e) {
    		throw new DistributorDBException("Exception while saving simpacks to distributor. DB error", e);
    	}  finally {
    		resetAutoCommit(provConn, true);
    		resetAutoCommit(conn, true);
    		DBUtils.cleanup(conn, ps, null);
    		DBUtils.cleanup(provConn, ps, null);
    	}
    }
    
    
    /**
     * Updates customer_order status for orderId
     * 
     * @param orderId
     * @param userId
     * @throws DistributorException
     */
    private void updateOrderStatus(Connection conn, long orderId, String userId) throws DistributorDBException {
    	PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(UPDATE_ORDER_STATUS);
            ps.setString(1, userId);
            ps.setLong(2, orderId);
            ps.setLong(3, orderId);
            ps.executeUpdate();
        } catch (SQLException e) {
        	LOG.error("Failed to update order status. DB error", e);
            throw new DistributorDBException("Failed to update order status", e);
        } finally {
        	DBUtils.cleanup(null, ps, null);
        }
        return ;
    }
    
   
    /**
     * Validates simpack id is assigned to the distributorId or not
     * 
     * @param simpackId
     * @param distributorId
     * @throws DistributorException
     */
    private void validateSimpackIdForDistributor(Long simpackId, Long distributorId) throws DistributorException, DistributorDBException {
    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null ;
    	
    	try {
			conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
			ps = conn.prepareStatement("select count(*) from sim_inventory.simpack where distributor_id = ? and unique_id = ?");
			ps.setLong(1, distributorId);
			ps.setLong(2, simpackId);
			rs = ps.executeQuery();
			if(rs.next()) {
				int count = rs.getInt(1);
				if(count > 0) {
					throw new DistributorException(String.format("SimpackId %l not found for distributorId %l", simpackId, distributorId));
				}
			}
		} catch (SQLException e) {
			LOG.error("Error while fetching simpacks for distributorId=" + distributorId, e);
			throw new DistributorDBException(String.format("Error while fetching simpacks for distributorId=" + distributorId, distributorId));
		} catch (Exception e) {
			LOG.error("Error while fetching simpacks for distributorId=" + distributorId, e);
			throw new DistributorDBException(String.format("Error while fetching simpacks for distributorId=" + distributorId, distributorId));
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
    }
    
    @Override
    public List<String> updateEmptySimpacksOfDistributor(long operatorId, long distributorId, UpdateEmptySIMPACKRequest request) 
    		throws AccountNotFoundException, DistributorException, DistributorDBException {

		Connection conn = null;
		PreparedStatement preparedStatement = null;
		Account account = accountDAO.getAccount(String.valueOf(operatorId), String.valueOf(distributorId) );
		if(account == null) {
			throw new AccountNotFoundException("Account not found, distributorId=" + distributorId);
		}
		List<String> simpacks = new ArrayList<String>();
		try {
			
			conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
			preparedStatement = conn.prepareStatement(UPDATE_SIMPACK_FOR_DISTRIBUTOR_SQL);
			List<String> uniqueIds = getUniqueIds(request.getSimpacks().size());
			for (String uniqueId : uniqueIds) {
				preparedStatement.setString(1, uniqueId);
				preparedStatement.setString(2, "0");
				preparedStatement.setLong(3, distributorId);
				preparedStatement.setString(4, request.getUserId());
				preparedStatement.addBatch();
				simpacks.add(uniqueId);
			}
			preparedStatement.executeBatch();
			conn.commit();
			LOG.info("Insert completed for all " + request.getSimpacks().size() + " records");
			return simpacks ;
		} catch (SQLException e) {
			LOG.error("Error while saving distributorCodes to simpack for distributorId=" + distributorId, e);
			throw new DistributorDBException(String.format("Error while fetching simpacks for distributorId=" + distributorId, distributorId));
		} catch (Exception e) {
			LOG.error("Error while saving distributorCodes to simpack", e);
			throw new DistributorException("Exception while saving unique distributorCodes. DB error", e);
		} finally {
			DBUtils.cleanup(conn, preparedStatement, null);
		}
    }

    
    @Override
    public boolean addSimpacksToDistributor(long operatorId, long distributorId, UpdateEmptySIMPACKRequest request) 
    		throws DistributorNotFoundException, DistributorException, DistributorDBException {

		Connection conn = null;
		PreparedStatement preparedStatement = null;
		boolean updateSuccess = false ;
		Distributor distributor = cache.get(distributorId);
		if(distributor == null) {
			throw new DistributorNotFoundException("Distributor not found for distributorId=" + distributorId);
		}
		
		try {
			conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
			preparedStatement = conn.prepareStatement(UPDATE_ICCIDS_TO_SIMPACK_SQL);
			for (Simpack simpack : request.getSimpacks()) {
				List<String> iccidList = prepareICCIDList(simpack.getIccidStart(), simpack.getIccidEnd(), simpack.getIccids());
				validateSimAssignmentToAccount(iccidList);
				validateSimpackIdForDistributor(Long.valueOf(simpack.getSimpackId()), distributorId);
				preparedStatement.setString(1, simpack.getIccidStart());
				preparedStatement.setString(2, simpack.getIccidEnd());
				preparedStatement.setString(3, simpack.getIccids());
				preparedStatement.setString(4, simpack.getSimType());
				preparedStatement.setLong(5, Long.valueOf(operatorId));
				preparedStatement.setString(6, simpack.getSimpackId());
				preparedStatement.setLong(7, distributorId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			conn.commit();
			LOG.info("Insert iccids completed for all " + request.getSimpacks().size() + " records");
			updateSuccess = true ;
			//TODO : send email notification to email recipients after update
		} catch (DistributorException e) {
			LOG.error("Error while saving iccids to simpack", e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOG.error("Exception while calling rollback");
			}
			throw e;
		} catch (Exception e) {
			LOG.error("Error while saving iccids to simpack", e);
			try {
				conn.rollback();
			} catch (SQLException e1) {
				LOG.error("Exception while calling rollback");
			}
			throw new DistributorException("Exception while ICCIDs to distributorId=" + distributorId, e);
		} finally {
			DBUtils.cleanup(conn, preparedStatement, null);
		}
		return updateSuccess ;
    }

    
	@Override
    public List<String> getClientEmails(long accountId){
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String email = null;
        String getCustomerEmailQuery = "SELECT MY_ALERTS_EMAIL FROM AERISGEN.WS_ACCTS_ADDITIONAL_INFO"
                + " WHERE ACCOUNT_ID = ? AND MY_ALERTS_EMAIL IS NOT NULL";
        try {
            conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
            ps = conn.prepareStatement(getCustomerEmailQuery);

            ps.setLong(1, accountId);
            rs = ps.executeQuery();
            if (rs.next()) {
                email = rs.getString("MY_ALERTS_EMAIL");
            }

            if (email != null && email.length() > 0) {
                String[] addr = email.split(";");
                LOG.debug("Email Addresses: {}", Arrays.toString(addr));
                return (Arrays.asList(addr));
            }

        } catch (Exception e) {
            LOG.error("Exception while getting email address for distributor accountId {}", accountId, e);
        } finally {
            DBConnectionManager.getInstance().cleanup(conn, ps, rs);
        }
        return new ArrayList<String>(0);
    }
	
	
    @Override
    public Simpack getSimPackByUniqueId(String uniqueId) throws DistributorException {
    	
    	Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
        	conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            ps = conn.prepareStatement(GET_SIMPACK_BY_UNIQUE_ID_SQL);
            ps.setString(1, uniqueId);
            rs = ps.executeQuery();
            Simpack simpack = null;
            if(rs.next()) {
            	simpack = new Simpack();
            	simpack.setIccidEnd(rs.getString("iccid_end"));
            	simpack.setIccidStart(rs.getString("iccid_start"));
            	simpack.setPackSize(rs.getInt("packet_size"));
            	simpack.setSimpackId(rs.getString("unique_id"));
            	simpack.setSimType(rs.getString("sim_type"));
            }
            return simpack;
        } catch (SQLException e) {
        	LOG.error("Failed to fetch package info for {}", uniqueId, e);
            throw new DistributorException("Failed to fetch package info", e);
        } finally {
        	DBConnectionManager.getInstance().cleanup(conn, ps, rs);
        }
    }
    
    
    /* 
     * Gets the list of operator and distributor simpacks 
     * (non-Javadoc)
     * @see com.aeris.service.operatoradmin.dao.IDistributorDAO#getSimpacks(java.lang.String, java.lang.String)
     */
    @Override
    public List<Simpack> getSimpacks(long operatorId, long distributorId, String used) throws SimpackDBException {
    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<Simpack> simpacks = new ArrayList<Simpack>();
    	String getSimpackQuery = GET_SIMPACKS_SQL ;
    	try {
    		conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
    		if(distributorId != -1) {
    			getSimpackQuery += " and distributor_id = ? " ;
    		}
    		ps = conn.prepareStatement(getSimpackQuery);
    		ps.setLong(1, Long.valueOf(operatorId));
    		ps.setString(2, used );
    		if(distributorId != -1) {
    			ps.setLong(3, Long.valueOf(distributorId));
    		}
    		rs = ps.executeQuery();
    		while(rs.next()) {
    			Simpack simpack = new Simpack();
    			simpack.setIccidEnd(rs.getString("iccid_end"));
    			simpack.setIccidStart(rs.getString("iccid_start"));
    			simpack.setPackSize(rs.getInt("packet_size"));
    			simpack.setIccids(rs.getString("iccids"));
    			simpack.setSimpackId(rs.getString("unique_id"));
    			simpack.setSimType(rs.getString("sim_type"));
    			simpacks.add(simpack);
    		}
    		return simpacks;
    	} catch (SQLException e) {
    		LOG.error("Failed to fetch simpacks info for operatorId {}", operatorId, e);
    		throw new SimpackDBException("Failed to fetch package info", e);
    	} finally {
    		DBConnectionManager.getInstance().cleanup(conn, ps, rs);
    	}
    }
    
    
    /**
     * fetches corresponding imsi and carrier from sim_inventory for iccid value in the same order
     * 
     * @param iccid
     * @return
     * @throws SimpackDBException
     */
    public List<String> getIMSIAndCarrierforICCID(String iccid) throws SimpackDBException {

    	Connection conn = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<String> imsiAndCarrier = new ArrayList<String>();
    	String getImsiAndCarrierFromSimInventory = "select imsi, carrier from sim_inventory.CINGULAR_SIM_INVENTORY where iccid = '" + iccid + "'"; 
    	try {
    		DBConnectionManager connectionManager = DBConnectionManager.getInstance();
    		conn = connectionManager.getBillingDatabaseConnection();
    		ps = conn.prepareStatement(getImsiAndCarrierFromSimInventory);
    		rs = ps.executeQuery();
    		if(rs.next()) {
    			imsiAndCarrier.add(rs.getString("imsi"));
    			imsiAndCarrier.add(rs.getString("carrier"));
    		}
    	} catch(Exception e) {
    		throw new SimpackDBException("Exception while fetching imsi for iccid from sim_inventory. DB error. iccid=" + iccid, e);
    	} finally {
    		DBUtils.cleanup(conn, ps, rs);
    	}
    	return imsiAndCarrier;

    }

    
    
    private void resetAutoCommit(Connection conn, boolean autoCommit) throws DistributorDBException {
		try {
			conn.setAutoCommit(autoCommit);
		} catch (SQLException e) {
			throw new DistributorDBException("Exception while setting autoCommit value. DB error" , e );
		}
	}

}
