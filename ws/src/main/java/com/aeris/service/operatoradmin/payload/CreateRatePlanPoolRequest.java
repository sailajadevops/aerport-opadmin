package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.AccountRatePlanPoolOverageBucket;
import com.aeris.service.operatoradmin.model.RatePlanPoolStatus;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;
import com.aeris.service.operatoradmin.model.constraints.NotPast;
import com.aeris.service.operatoradmin.model.constraints.ValidateRatePlanPoolDate;

@XmlRootElement
@ValidateRatePlanPoolDate(message="{endDate.after.startDate}")
public class CreateRatePlanPoolRequest implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;
	
	@Email(message="{userId.email}")
	private String userId;
	
	@NotNull(message="{ratePlanPoolName.notnull}")
	@Size(max = 24, message = "{ratePlanPoolName.size}")
	private String ratePlanPoolName;
	
	@NotNull(message="{startDate.notnull}")
	@NotPast(message="{startDate.notpast}")
	private Date startDate;
	
	@NotNull(message="{endDate.notnull}")
	@NotPast(message="{endDate.notpast}")
	private Date endDate;
	
	@Pattern(regexp = "[0-9]+", message = "{productId.number}")
	@NotNull(message="{productId.notnull}")
	private String productId;
		
	@NotNull(message="{ratePlans.notnull}")
	@Size(min = 1, message = "{ratePlans.size}")
	private List<String> ratePlans;
	
	@Enumeration(value = RatePlanPoolStatus.class, message = "{status.enum}")
	private RatePlanPoolStatus status = RatePlanPoolStatus.PENDING;
	
	@Valid
	private List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBucket;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRatePlanPoolName() {
		return ratePlanPoolName;
	}

	public void setRatePlanPoolName(String ratePlanPoolName) {
		this.ratePlanPoolName = ratePlanPoolName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public List<String> getRatePlans() {
		return ratePlans;
	}

	public void setRatePlans(List<String> ratePlans) {
		this.ratePlans = ratePlans;
	}

	public RatePlanPoolStatus getStatus() {
		return status;
	}

	public void setStatus(RatePlanPoolStatus status) {
		this.status = status;
	}

	public List<AccountRatePlanPoolOverageBucket> getRatePlanPoolOverageBucket() {
		return ratePlanPoolOverageBucket;
	}

	public void setRatePlanPoolOverageBucket(List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBucket) {
		this.ratePlanPoolOverageBucket = ratePlanPoolOverageBucket;
	}
}
