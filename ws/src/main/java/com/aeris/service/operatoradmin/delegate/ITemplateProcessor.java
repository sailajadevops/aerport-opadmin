package com.aeris.service.operatoradmin.delegate;

import com.aeris.service.operatoradmin.exception.ExportException;

public interface ITemplateProcessor {
	byte[] createByteStreamFromTemplate(String templatePath, TemplateData data) throws ExportException;
}
