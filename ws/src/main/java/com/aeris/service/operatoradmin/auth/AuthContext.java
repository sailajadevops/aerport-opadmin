package com.aeris.service.operatoradmin.auth;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.model.Role;

/**
 * Class for role mapping security context 
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class AuthContext implements SecurityContext {
	
	private AccountPrincipal principal;
	private static final Logger LOG = LoggerFactory.getLogger(AuthContext.class);
	private boolean isFilterDisabled ;
	
	
	public AuthContext(final Role role) {
		this.principal = new AccountPrincipal(role);
	}
	
	public AuthContext(final Role role, boolean isFilterDisabled) {
		this.principal = new AccountPrincipal(role);
		this.isFilterDisabled = isFilterDisabled ;
	}
	
	@Override
	public String getAuthenticationScheme() {
		return SecurityContext.BASIC_AUTH;
	}

	@Override
	public Principal getUserPrincipal() {
		return principal;
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public boolean isUserInRole(final String role) {
		if(isFilterDisabled) {
			return true ;
		}
		if ( this.principal != null 
				&& this.principal.getRole() != null 
				&& this.principal.getRole().equals(Role.valueOf(role))) {
			LOG.info("Request Authorized!!! Calling the Webservice...");
			return true;
		}
		
		return false;
	}
}
