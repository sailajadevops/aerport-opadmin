package com.aeris.service.operatoradmin.client;

public class DeviceID {

    private String sclId;

    public DeviceID() {
    }

    public DeviceID(String sclId) {
        this.sclId = sclId;
    }

    public String getSclId() {
        return sclId;
    }

    public void setSclId(String sclId) {
        this.sclId = sclId;
    }
}