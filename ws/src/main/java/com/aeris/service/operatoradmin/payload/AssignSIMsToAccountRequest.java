package com.aeris.service.operatoradmin.payload;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Email;

@XmlRootElement
public class AssignSIMsToAccountRequest {

    @Email(message = "{userEmail.email}")
    @NotNull(message = "{userEmail.notnull}")
    private String userEmail;
    private String iccidStart;
    private String iccidEnd;
    private String iccidAdditional;
    private String simPackId;
    private long orderId;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getIccidStart() {
        return iccidStart;
    }

    public void setIccidStart(String iccidStart) {
        this.iccidStart = iccidStart;
    }

    public String getIccidEnd() {
        return iccidEnd;
    }

    public void setIccidEnd(String iccidEnd) {
        this.iccidEnd = iccidEnd;
    }

    public String getIccidAdditional() {
        return iccidAdditional;
    }

    public void setIccidAdditional(String iccidAdditional) {
        this.iccidAdditional = iccidAdditional;
    }

    public String getSimPackId() {
        return simPackId;
    }

    public void setSimPackId(String simPackId) {
        this.simPackId = simPackId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "AssignSIMsToAccountRequest{" + "userEmail=" + userEmail + ", iccidStart=" + iccidStart + ", iccidEnd=" + iccidEnd + ", iccidAdditional=" + iccidAdditional + ", simPackId=" + simPackId + ", orderId=" + orderId + '}';
    }    
}
