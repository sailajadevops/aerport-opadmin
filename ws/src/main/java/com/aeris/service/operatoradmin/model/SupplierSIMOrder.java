package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class SupplierSIMOrder implements Serializable {
	private static final long serialVersionUID = -6889511890324637760L;
	private long orderId;
	private long quantity;
	private int supplierId;
	private int simWarehouseId;
	private int simFormatId;
	private int productId;
	private OrderStatus status;
	private String iccIdRange;
	private String imsiRange;
	private IMSIRangeType imsiRangeType;
	private String iccIdStart;
	private String imsiStart;
	private String createdBy;
	private String lastModifiedBy;
	private String createdDate;
	private String lastModifiedDate;
	
	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public int getSimWarehouseId() {
		return simWarehouseId;
	}

	public void setSimWarehouseId(int simWarehouseId) {
		this.simWarehouseId = simWarehouseId;
	}

	public int getSimFormatId() {
		return simFormatId;
	}

	public void setSimFormatId(int simFormatId) {
		this.simFormatId = simFormatId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public String getIccIdRange() {
		return iccIdRange;
	}

	public void setIccIdRange(String iccIdRange) {
		this.iccIdRange = iccIdRange;
	}

	public String getImsiRange() {
		return imsiRange;
	}

	public void setImsiRange(String imsiRange) {
		this.imsiRange = imsiRange;
	}

	public IMSIRangeType getImsiRangeType() {
		return imsiRangeType;
	}

	public void setImsiRangeType(IMSIRangeType imsiRangeType) {
		this.imsiRangeType = imsiRangeType;
	}

	public String getIccIdStart() {
		return iccIdStart;
	}

	public void setIccIdStart(String iccIdStart) {
		this.iccIdStart = iccIdStart;
	}

	public String getImsiStart() {
		return imsiStart;
	}

	public void setImsiStart(String imsiStart) {
		this.imsiStart = imsiStart;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
}
