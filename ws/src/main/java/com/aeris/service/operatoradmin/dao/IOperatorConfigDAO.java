package com.aeris.service.operatoradmin.dao;

import java.util.List;
import java.util.Map;

import com.aeris.service.operatoradmin.exception.OperatorException;
import com.aeris.service.operatoradmin.model.Country;
import com.aeris.service.operatoradmin.model.RoamingOperator;

public interface IOperatorConfigDAO {
	Map<String, String> getOperatorConfiguration(int operatorId) throws OperatorException;
	List<Country> getRoamingCountries(int operatorId, int filteredZoneId) throws OperatorException;
	List<Country> getRoamingCountries(int operatorId, int filteredZoneId, int zoneSetId) throws OperatorException;
	List<RoamingOperator> getRoamingOperators(int operatorId, int filteredZoneId) throws OperatorException;
	List<RoamingOperator> getRoamingOperators(int operatorId, int filteredZoneId, int zoneSetId) throws OperatorException;
	Map<Long, String> getOperatorKeys();
	List<Country> getAllCountries();
	List<RoamingOperator> getAllOperators();
}
