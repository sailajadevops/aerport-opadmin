package com.aeris.service.operatoradmin.dao;

import java.util.List;

import com.aeris.service.operatoradmin.exception.SIMInventoryException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.SIMInfo;
import com.aeris.service.operatoradmin.model.SIMInventoryItem;
import com.aeris.service.operatoradmin.model.SIMStatus;
import com.aeris.service.operatoradmin.model.SIMSummary;
import com.aeris.service.operatoradmin.model.SIMSupplier;
import com.aeris.service.operatoradmin.model.SIMWarehouse;
import com.aeris.service.operatoradmin.model.SupplierSIMOrder;

public interface IGlobalSIMInventoryDAO {
	SIMSummary<SIMWarehouse> getSIMSummaryGroupedByWarehouse();
	SIMSummary<SIMSupplier> getSIMSummaryGroupedBySupplier(SIMWarehouse warehouse);
	SIMSummary<SIMStatus> getSIMSummaryGroupedByStatus(Account account);
	List<SIMInventoryItem> getSIMInventoryItemsPerAccount(Account account);
	SIMInventoryItem getSIMInventoryItem(SIMInfo simInfo);
	List<SIMInventoryItem> addSIMsToInventory(SupplierSIMOrder supplierSIMOrder, List<SIMInfo> sims, String userId) throws SIMInventoryException;
	void removeSIMsFromInventory(List<SIMInventoryItem> items);
	void allocateSIMstoAccount(Account account, List<SIMInventoryItem> items);
	void deallocateSIMsFromAccount(Account account, List<SIMInventoryItem> items);
}
