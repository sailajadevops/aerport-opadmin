package com.aeris.service.operatoradmin.payload;

import java.util.List;

/**
 * The DTO class for distributorCode
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class CreateDistributorCode {

	private long distributorId ;
	private int numberOfCodes ;
	private List<String> emailRecipients ;
	private long accountId ;
	private String iccidStart;
	private String iccidEnd ;
	private String assignedIccid ;
	
	
	public long getDistributorId() {
		return distributorId;
	}
	
	public void setDistributorId(long distributorId) {
		this.distributorId = distributorId;
	}
	
	public int getNumberOfCodes() {
		return numberOfCodes;
	}
	
	public void setNumberOfCodes(int numberOfCodes) {
		this.numberOfCodes = numberOfCodes;
	}
	
	public List<String> getEmailRecipients() {
		return emailRecipients;
	}
	
	public void setEmailRecipients(List<String> emailRecipients) {
		this.emailRecipients = emailRecipients;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public String getIccidStart() {
		return iccidStart;
	}

	public void setIccidStart(String iccidStart) {
		this.iccidStart = iccidStart;
	}

	public String getIccidEnd() {
		return iccidEnd;
	}

	public void setIccidEnd(String iccidEnd) {
		this.iccidEnd = iccidEnd;
	}

	public String getAssignedIccid() {
		return assignedIccid;
	}

	public void setAssignedIccid(String assignedIccid) {
		this.assignedIccid = assignedIccid;
	}


}
