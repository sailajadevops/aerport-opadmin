package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum AccountStatus implements Serializable {
	COMMERCIAL(1), ON_BOARDING(2), TRIAL(4), SUSPENDED(0);
	int value;

	AccountStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static AccountStatus fromValue(int value) {
		AccountStatus[] statuses = values();

		for (AccountStatus accountStatus : statuses) {
			if (accountStatus.getValue() == value) {
				return accountStatus;
			}
		}

		return null;
	}

	public static AccountStatus fromName(String value) {
		AccountStatus[] statuses = values();

		for (AccountStatus accountStatus : statuses) {
			if (accountStatus.name().equalsIgnoreCase(value)) {
				return accountStatus;
			}
		}

		return null;
	}
}
