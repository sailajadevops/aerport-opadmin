package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SIMWarehouse;
import com.google.inject.Inject;

public class GetWarehousesStoredProcedure extends AbstractProcedureCall<List<SIMWarehouse>> {

	private static final String SQL_GET_WAREHOUSES = "common_util_pkg.get_sim_warehouse";

	@Inject
	public GetWarehousesStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_WAREHOUSES, true);
		
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SIMWarehouse> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<SIMWarehouse> warehouses = createWarehouseList(cursor);

		return warehouses;
	}

	private List<SIMWarehouse> createWarehouseList(List<Map<String, Object>> cursor) {
		List<SIMWarehouse> warehouses = new ArrayList<SIMWarehouse>();

		for (Map<String, Object> row : cursor) {
			SIMWarehouse warehouse = new SIMWarehouse();

			String warehouseId = (String) row.get("warehouse_id");

			if (NumberUtils.isNumber(warehouseId)) {
				warehouse.setWarehouseId(Integer.parseInt(warehouseId));
			}

			String warehouseName = (String) row.get("warehouse_name");

			if (StringUtils.isNotEmpty(warehouseName)) {
				warehouse.setWarehouseName(warehouseName);
			}
			
			warehouses.add(warehouse);
		}

		return warehouses;
	}
}
