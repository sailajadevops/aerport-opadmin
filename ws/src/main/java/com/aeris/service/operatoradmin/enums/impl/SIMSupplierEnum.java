package com.aeris.service.operatoradmin.enums.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.ISIMSupplierEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SIMSupplier;

public class SIMSupplierEnum implements ISIMSupplierEnum, IEventListener {
	private Logger LOG = LoggerFactory.getLogger(SIMFormatEnum.class);
	private Cache<String, SIMSupplier> cache;
	private Cache<String, SIMSupplier> cacheById;
	
	@Inject
	private IStoredProcedure<List<SIMSupplier>> getSuppliersStoredProcedure;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "SIMSupplierCache/name") Cache<String, SIMSupplier> cache, 
			@Hazelcast(cache = "SIMSupplierCache/id") Cache<String, SIMSupplier> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;	
		loadResources();
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		try {
			// Call Get_Product Stored Procedure
			List<SIMSupplier> suppliers = getSuppliersStoredProcedure.execute();	
			
			for(SIMSupplier supplier: suppliers) 
			{
				cache.put(supplier.getName(), supplier);
				cacheById.put(String.valueOf(supplier.getSupplierId()), supplier);
			}
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during getSuppliersStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		}
	}
	
	@Override
	public SIMSupplier getSIMSupplierByName(String name) {
		return cache.get(name);
	}

	@Override
	public SIMSupplier getSIMSupplierById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<SIMSupplier> getAllSIMSuppliers() {
		return new ArrayList<SIMSupplier>(cacheById.getValues());
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}

}
