package com.aeris.service.operatoradmin.dao.storedproc;

import java.io.Serializable;

public class ResultStatus implements Serializable {
	private static final long serialVersionUID = -2700078016245339115L;
	private int resultCode;
	private String resultMessage;
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
}