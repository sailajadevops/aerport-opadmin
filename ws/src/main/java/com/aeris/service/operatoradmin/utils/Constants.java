package com.aeris.service.operatoradmin.utils;

public class Constants {
	
	public static final String TIMEZONE_GMT = "GMT";
	public static final String PRODUCT_ATT_LTE = "ATT-LTE";
	public static final String TECHNOLOGY_LTE = "LTE";
	public static final String ATT_PRODUCT_DUAL_MODE_A_LH = "28";
	public static final String PROPERTY_CREDIT_LIMIT_THRESHOLD_PERCENT = "creditLimitThresholdPercentage";
	public static final String PROPERTY_CREDIT_LIMIT_SETTING_ENABLED = "creditLimitEnabled";
	public static final String USCC_OPEARTOR_ID = "18";
	

}
