package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.PoolingPolicy;

public interface IPoolingPolicyEnum  extends IBaseEnum{
	PoolingPolicy getPoolingPolicyByName(String name);
	PoolingPolicy getPoolingPolicyById(int id);
	List<PoolingPolicy> getAllPoolingPolicies();
}
