package com.aeris.service.operatoradmin.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IRatePlanDAO;
import com.aeris.service.operatoradmin.dao.IRatePlanPoolDAO;
import com.aeris.service.operatoradmin.exception.BadStartDateException;
import com.aeris.service.operatoradmin.exception.DAOException;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanPoolException;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.InvalidRatePlanPoolException;
import com.aeris.service.operatoradmin.exception.RatePlanPoolException;
import com.aeris.service.operatoradmin.exception.RatePlanPoolNotFoundException;
import com.aeris.service.operatoradmin.model.AccountRatePlanPool;
import com.aeris.service.operatoradmin.model.AccountRatePlanPoolOverageBucket;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.aeris.service.operatoradmin.model.RatePlanPoolStatus;
import com.aeris.service.operatoradmin.utils.ApplicationUtils;
import com.aeris.service.operatoradmin.utils.Constants;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.google.inject.Inject;

public class RatePlanPoolDAO implements IRatePlanPoolDAO {
	private static Logger LOG = LoggerFactory.getLogger(RatePlanPoolDAO.class);
	
	private static final String SQL_GET_MAX_ACCOUNT_RATE_PLAN_POOL_ID = "select aerisgen.account_rate_pool_map_seq.nextval account_rate_pool_map_id from dual";
	 
	private static final String SQL_GET_MAX_RATE_PLAN_POOL_ID = "select aerisgen.account_pool_seq.nextval pool_id from dual";
	
	private static final String SQL_GET_YEARLY_RATE_PLAN_COUNT = "select count(*)  as yearly_count "+
																	"from aerbill_prov.csp_rate_plan a, "+ 
																	"aerbill_prov.account_rate_plan_v1 b "+ 
																	"where a.rate_plan_name in :rate_plans "+ 
																	"and a.access_fee_period_length > 1 "+ 
																	"and a.rate_plan_id = b.csp_rate_plan_id "+ 
																	"and b.account_id = ?";
	
	private static final String SQL_GET_MONTHLY_RATE_PLAN_COUNT = "select count(*) as monthly_count "+
																	"from aerbill_prov.csp_rate_plan a, "+ 
																	"aerbill_prov.account_rate_plan_v1 b "+ 
																	"where a.rate_plan_name in :rate_plans "+ 
																	"and a.access_fee_period_length = 1 "+ 
																	"and a.rate_plan_id = b.csp_rate_plan_id "+ 
																	"and b.account_id = ?";	
	
	private final static String SQL_GET_ASSIGNED_RATE_PLAN_POOLS =  "select * "+ 
																	"from (" +
																	"select distinct a.pool_id, "+
																	"a.pool_name, "+
																	"a.start_date, "+
																	"a.end_date, "+
																	"a.status_id as valid_flag, "+
																	"a.product_id, "+
																	"ROW_NUMBER() OVER (ORDER BY a.pool_id) AS rnum "+ 
																	"from "+ 
																	"aerisgen.account_pool a "+
																	"where a.account_id = ? "+
																	"and a.end_date > SYSGMTDATE "+
																	"group by ("+
																	"a.pool_id, "+
																	"a.pool_name, "+
																	"a.start_date, "+
																	"a.end_date, "+
																	"a.status_id, "+
																	"a.product_id)"+
																	")";
	
	 
	private final static String SQL_GET_ACCOUNT_RATE_PLAN_POOL = "select b.pool_id, "+
																	"b.pool_name, "+
																	"b.start_date, "+
																	"b.end_date, "+
																	"b.status_id, "+
																	"b.product_id, "+
																	"RTRIM(replace(replace(XMLAGG (XMLELEMENT (e, c.rate_plan_name || ',')),'<E>'),'</E>'), ',') as rate_plans, "+
																	"ROW_NUMBER() OVER (ORDER BY b.pool_id) AS rnum "+ 
																	"from "+ 
																	"aerisgen.account_rate_plan_pool_map a, "+
																	"aerisgen.account_pool b, "+
																	"aerisgen.rate_plan c " +
																	"where b.account_id = ? "+
																	"and b.pool_id = ? "+
																	"and a.pool_id  = b.pool_id "+
																	"and a.rate_plan_id = c.rate_plan_id "+
																	"and b.end_date > SYSGMTDATE "+
																	"group by ("+
																	"b.pool_id, "+
																	"b.pool_name, "+
																	"b.start_date, "+
																	"b.end_date, "+
																	"b.status_id, "+
																	"b.product_id"+
																	")";
	
	private static final String SQL_CREATE_ACCOUNT_RATE_POOL = "INSERT INTO t_account_rate_pool_map " +
																"(account_rate_pool_map_id, " +
																"pool_name, " +
																"rate_plan, " +
																"account_id," +
																"start_date," +
																"end_date, " +
																"valid_flag," +
																"last_modified_date, " +
																"last_modified_by) " +
																"values " +
																"(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_CREATE_RATE_PLAN_POOL = "INSERT INTO t_pool " +
															"(pool_id, " +
															"pool_name, " +
															"start_date," +
															"end_date, " +
															"valid_flag," +
															"status," +
															"product_id," +
															"last_modified_date, " +
															"last_modified_by) " +
															"values " +
															"(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_CREATE_RATE_PLAN_POOL_OVRG_BKT = "INSERT INTO aerisgen.ACCOUNT_POOL_OVERAGE_BUCKET " +
			"(RATE_PLAN_POOL_ID, " +
			"ACCOUNT_ID, " +
			"START_DATE, " +
			"END_DATE, " +
			"BUCKET_SIZE," +
			"BUCKET_SIZE_TYPE," +
			"BUCKET_PRICE, " +
			"NO_ACCESS_FEE_DEVICE_COUNT," +
			"AUTO_ADD," + 
			"CREATED_DATE," +
			"LAST_MODIFIED_DATE," + 
			"CREATED_BY," +
			"LAST_MODIFIED_BY," +
			"REP_TIMESTAMP ) " +
			"values (?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_UPDATE_RATE_PLAN_POOL = "update t_pool set " +
															"pool_name = ?, " +
															"start_date = ?, " +
															"end_date = ?, " +
															"valid_flag = ?, " +
															"product_id = ?, " +
															"last_modified_date = ?, " +
															"last_modified_by = ? " +
															"where pool_id = ?";	
		
	private static final String SQL_APPROVE_ACCOUNT_RATE_POOL = "update t_account_rate_pool_map set " +
																"valid_flag = 1 " +
																"where pool_name = (select pool_name from t_pool where pool_id = ?) " +
																"and account_id = ?";	
	
	private static final String SQL_APPROVE_RATE_PLAN_POOL = "update t_pool set " +
																"valid_flag = 1, status = ? " +
																"where pool_id = ?";	

	private static final String SQL_DEACTIVATE_ACCOUNT_RATE_POOL = "update t_account_rate_pool_map set " +
																"valid_flag = 2, " +
																"end_date = ? "+
																"where pool_name = (select pool_name from t_pool where pool_id = ?) " +
																"and account_id = ?";	
	
	private static final String SQL_DEACTIVATE_RATE_PLAN_POOL = "update t_pool set " +
																"valid_flag = 2, status = ?, " +
																"end_date = ? "+
																"where pool_id = ?";	

	private static final String SQL_DELETE_ACCOUNT_RATE_POOL = "delete from " +
																"t_account_rate_pool_map " +
																"where pool_name = ? and account_id = ?";

	private static final String SQL_VALIDATE_ACCOUNT_RATE_POOL = "select count(*) as rate_pool_count " +
																	"from t_account_rate_pool_map " +
																	"where pool_name = ? and " +
																	"account_id = ? and " + 
																	"end_date > SYSGMTDATE";

	private static final String SQL_VALIDATE_ACCOUNT_RATE_POOL_FOR_UPDATE = "select count(*) as rate_pool_count " +
																			"from t_account_rate_pool_map " +
																			"where pool_name = ? and " +
																			"account_id = ? and " + 
																			"pool_name != ? and " + 
																			"end_date > SYSGMTDATE";
	
	private static final String SQL_GET_RATE_PLAN_POOLS_PAGINATION_CLAUSE = " where rnum between ? and ?";
	
	private Cache<Long, Map<Long, AccountRatePlanPool>> cache;
	
	@Inject
	private IRatePlanDAO ratePlanDAO ;
	
	@Inject
	private OAUtils oaUtils;
	
	public void init(@Hazelcast(cache = "AccountRatePlanPoolCache/id") Cache<Long, Map<Long, AccountRatePlanPool>> cache) {
		this.cache = cache;
	}

	@Override
	public List<AccountRatePlanPool> getAccountRatePlanPools(int operatorId, long accountId, String start, String count,
			RatePlanPoolStatus... rateplanPoolStatusList){		
		if(cache.get(accountId) != null)
		{
			LOG.debug("getAccountRatePlanPools: Fetching from AccountRatePlanPool cache...");
			return getCachedAccountRatePlanPools(operatorId, accountId, start, count, rateplanPoolStatusList);
		}
		
		List<AccountRatePlanPool> accountRatePlanPools = new ArrayList<AccountRatePlanPool>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAssignedRatePlanPools: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			ps = buildGetRatePlanPoolsByAccountIdQuery(conn, start, count, operatorId, accountId, rateplanPoolStatusList);

			LOG.debug("getAssignedRatePlanPools: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				AccountRatePlanPool accountRatePlanPool = new AccountRatePlanPool();
				
				accountRatePlanPool.setAccountId(accountId);
				accountRatePlanPool.setRatePlanPoolId(rs.getLong("pool_id"));
				accountRatePlanPool.setRatePlanPoolName(rs.getString("pool_name"));
				accountRatePlanPool.setEndDate(rs.getDate("end_date"));
				accountRatePlanPool.setStartDate(rs.getDate("start_date"));
				accountRatePlanPool.setEndDate(rs.getDate("end_date"));
				accountRatePlanPool.setProductId(rs.getInt("product_id"));
				accountRatePlanPool.setStatus(RatePlanPoolStatus.fromValue(rs.getInt("valid_flag")));
				
				accountRatePlanPools.add(accountRatePlanPool);
			}
			
			// Put in cache
			putInCache(accountId, accountRatePlanPools);			
		} catch (SQLException e) {
			LOG.error("Exception during getAssignedRatePlanPools", e);
			throw new GenericServiceException("Unable to getAssignedRatePlanPools", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getAssignedRatePlanPools" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}
		
		LOG.info("getAssignedRatePlanPools: Returned " + accountRatePlanPools.size() + " rate plan pools to the caller");

		
		return accountRatePlanPools;
	}

	private PreparedStatement buildGetRatePlanPoolsByAccountIdQuery(Connection conn, String start, String count, int operatorId,
			long accountId, RatePlanPoolStatus... rateplanPoolStatusList) throws SQLException {
		PreparedStatement ps; 

		// Check if pagination parameters are valid
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);
		
		// Check if status is a search field
		boolean queryByStatus = ArrayUtils.isNotEmpty(rateplanPoolStatusList);
		
		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_ASSIGNED_RATE_PLAN_POOLS);
				
		// Query if the status is used in the order search criteria
		// Uncomment when status column is introduced
//		if(queryByStatus)
//		{
//			// Build getOrders by Status query
//			StringBuffer inClause = new StringBuffer("(");
//			
//			for (int i = 0; i < ratePlanStatusList.length; i++) {
//				inClause.append("?");
//
//				if (i != ratePlanStatusList.length - 1) {
//					inClause.append(",");
//				}
//			}
//
//			inClause.append(")");
//			
//			String getOrdersWithStatusQuery = SQL_GET_CUST_ORDERS_BY_STATUS.replace(":status_list", inClause.toString());
//			
//			query = new StringBuilder(getOrdersWithStatusQuery);
//		}
		
		// Append Pagination parameters
		if (paginated) {
			query.append(SQL_GET_RATE_PLAN_POOLS_PAGINATION_CLAUSE);
		}

		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		
		// Set Account Id
		ps.setLong(++i, accountId);
		
		// Uncomment when the operatorId is introduced
		// Set Operator Id
		//ps.setInt(++i, operatorId);
			
		// Set Status values
		// Uncomment when status column is introduced
//		if(queryByStatus)
//		{
//			int j = 0;
//			
//			// In clause for account ids
//			while (j < orderStatusList.length) {
//				String status = orderStatusList[j++].getValue();
//				ps.setString(++i, status);
//			}
//		}

		// paginate the results if required
		if (paginated) {
			long rownumStart = Long.parseLong(start);
			long rownumEnd = Long.parseLong(start) + Long.parseLong(count);

			ps.setLong(++i, rownumStart);
			ps.setLong(++i, rownumEnd);
		}

		return ps;
	}
	
	private PreparedStatement buildGetRatePlanPoolQuery(Connection conn, int operatorId, long accountId, long ratePlanPoolId) throws SQLException {
		PreparedStatement ps;
		
		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_ACCOUNT_RATE_PLAN_POOL);

		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		
		// Set Account Id
		ps.setLong(++i, accountId);
		
		// Set Rate Plan Pool Id
		ps.setLong(++i, ratePlanPoolId);

		return ps;
	}
	
	@Override
	public AccountRatePlanPool getAccountRatePlanPool(int operatorId, long accountId, long ratePlanPoolId){
		AccountRatePlanPool accountRatePlanPool = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAccountRatePlanPool: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			ps = buildGetRatePlanPoolQuery(conn, operatorId, accountId, ratePlanPoolId);

			LOG.debug("getAccountRatePlanPool: executed query: " + ps);
			rs = ps.executeQuery();

			if (rs.next()) {
				accountRatePlanPool = new AccountRatePlanPool();
				
				accountRatePlanPool.setAccountId(accountId);
				accountRatePlanPool.setRatePlanPoolId(rs.getLong("pool_id"));
				accountRatePlanPool.setRatePlanPoolName(rs.getString("pool_name"));
				accountRatePlanPool.setEndDate(rs.getDate("end_date"));
				accountRatePlanPool.setStartDate(rs.getDate("start_date"));
				accountRatePlanPool.setEndDate(rs.getDate("end_date"));
				accountRatePlanPool.setProductId(rs.getInt("product_id"));
				int status = rs.getInt("status_id");
				RatePlanPoolStatus pStatus = RatePlanPoolStatus.PENDING;
				if (status != 0) {
					pStatus = RatePlanPoolStatus.fromValue(status);
				}
				accountRatePlanPool.setStatus(pStatus);
				
				// Set Rate Plans
				List<String> ratePlans = getRatePlansForRatePlanPool(rs.getString("rate_plans"));
				accountRatePlanPool.setRatePlans(ratePlans);
				//set the account overage bucket to the pool
				setAccountOverageBucket(accountRatePlanPool, accountId, ratePlanPoolId, conn);
			}
			
		} catch (SQLException e) {
			LOG.error("Exception during getAssignedRatePlanPools", e);
			throw new GenericServiceException("Unable to getAssignedRatePlanPools", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getAssignedRatePlanPools" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("Returned account rate plan pool to the caller: " + accountRatePlanPool);

		return accountRatePlanPool;
	}

	private List<String> getRatePlansForRatePlanPool(String ratePlan) {
		Set<String> ratePlans = new HashSet<String>();

		if (ratePlan != null) {
			String[] ratePlanArr = ratePlan.split(",");
			Collections.addAll(ratePlans, ratePlanArr);
		}

		return new ArrayList<String>(ratePlans);
	}
	
	private List<AccountRatePlanPool> getCachedAccountRatePlanPools(int operatorId, long accountId, String start, String count, final RatePlanPoolStatus... ratePlanPoolStatusList) {
		List<AccountRatePlanPool> cachedaccountRatePlanPools = new ArrayList<AccountRatePlanPool>();
		long rownumStart = 0;
		long rownumEnd = 0;
		
		Map<Long, AccountRatePlanPool> accountRatePlanPools = cache.get(accountId);
			
		if(accountRatePlanPools != null)
		{
			cachedaccountRatePlanPools.addAll(accountRatePlanPools.values());			
			LOG.info("Found "+cachedaccountRatePlanPools.size()+" Rate Plan Pools for account id: "+accountId);
		}
		else
		{
			LOG.info("No Rate Plan Pools found in the cache for account id: "+accountId);
		}

		if (NumberUtils.isNumber(start) && NumberUtils.isNumber(count)) {
			rownumStart = Long.parseLong(start);
			rownumEnd = Long.parseLong(start) + Long.parseLong(count);
		}
		
		for (int i = 0; i < cachedaccountRatePlanPools.size(); i++) {
			boolean success = true;
			AccountRatePlanPool accountRatePlanPool = cachedaccountRatePlanPools.get(i);

			for (RatePlanPoolStatus ratePlanPoolStatus : ratePlanPoolStatusList) {
				success = success && (ratePlanPoolStatus == accountRatePlanPool.getStatus());

				if (success) {
					break;
				}
			}

			if (rownumStart > 0 && rownumEnd > rownumStart) {
				success = success && rownumStart <= i && i <= rownumEnd;
			}

			if (!success) {
				cachedaccountRatePlanPools.remove(accountRatePlanPool);
			}
		}

		return cachedaccountRatePlanPools;
	}
	
	private static final String INSERT_ACCOUNT_POOL = "INSERT INTO aerisgen.ACCOUNT_POOL (POOL_ID, POOL_NAME, POOL_LABEL, DESCRIPTION, " +
			" STATUS_ID, START_DATE, END_DATE, POOL_IN_HIERARCHY, ACCOUNT_ID, PRODUCT_ID, CREATED_DATE, CREATED_BY, LAST_MODIFIED_DATE, " +
			" LAST_MODIFIED_BY, REP_TIMESTAMP)"
			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String GET_CURRENT_ACCOUNT_POOL_ID = "SELECT aerisgen.account_pool_seq.currval pool_id FROM DUAL";
	
	private void createAccountPool(Connection conn, long accountId, int productId, long poolId, String poolName, String poolLabel, String description, RatePlanPoolStatus status, Date startDate, Date endDate, boolean poolInHierarchy, List<Long> ratePlanIds, Date requestedDate, String requestedBy) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(INSERT_ACCOUNT_POOL);
			ps.setLong(1, poolId);
			ps.setString(2, poolName);
			ps.setString(3, poolLabel);
			ps.setString(4, description);
			ps.setInt(5, status.getValue());
			ps.setDate(6, new java.sql.Date(startDate.getTime()));
			ps.setDate(7, new java.sql.Date(endDate.getTime()));
			ps.setBoolean(8, poolInHierarchy);
			ps.setLong(9, accountId);
			ps.setInt(10, productId);
			ps.setTimestamp(11, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(12, requestedBy);
			ps.setTimestamp(13, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(14, requestedBy);
			ps.setTimestamp(15, new java.sql.Timestamp(requestedDate.getTime()));
			int count = ps.executeUpdate();
			LOG.debug("Inserted {} rows in account_pool", count);
			
			insertInAccountRatePlanPoolMap(conn, poolId, ratePlanIds, startDate, endDate, requestedDate, requestedBy);
		} catch (SQLException ex) {
			LOG.error("Failed to create account pool. AccountId {}, Poolname {}", accountId, poolName, ex);
			throw new DAOException("Failed to create account pool");
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
	}
	
	private static final String UPDATE_ACCOUNT_POOL = "UPDATE aerisgen.ACCOUNT_POOL SET POOL_NAME = ?, POOL_LABEL = ?, DESCRIPTION = ?, STATUS_ID = ?, START_DATE = ?, END_DATE = ?, POOL_IN_HIERARCHY = ?, LAST_MODIFIED_DATE = ?, LAST_MODIFIED_BY = ?, REP_TIMESTAMP = ?, PRODUCT_ID = ? WHERE POOL_ID = ?";

	private void updateAccountPool(Connection conn, long poolId, String poolName, String poolLabel, String description, RatePlanPoolStatus status, Date startDate, Date endDate, boolean poolInHierarchy, Date requestedDate, int productId, String requestedBy) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = conn.prepareStatement(UPDATE_ACCOUNT_POOL);
			ps.setString(1, poolName);
			ps.setString(2, poolLabel);
			ps.setString(3, description);
			ps.setInt(4, status.getValue());
			ps.setDate(5, new java.sql.Date(startDate.getTime()));
			ps.setDate(6, new java.sql.Date(endDate.getTime()));
			ps.setBoolean(7, poolInHierarchy);
			ps.setTimestamp(8, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(9, requestedBy);
			ps.setTimestamp(10, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setInt(11, productId);
			ps.setLong(12, poolId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			LOG.error("Failed to update account pool. PoolId {}", poolId, ex);
			throw new DAOException("Failed to update account pool");
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
	}
	
	private static final String INSERT_ACCOUNT_RATE_PLAN_POOL_MAP = "INSERT INTO aerisgen.ACCOUNT_RATE_PLAN_POOL_MAP (POOL_ID, RATE_PLAN_ID, " +
			" START_DATE, END_DATE, CREATED_DATE, CREATED_BY, LAST_MODIFIED_DATE, LAST_MODIFIED_BY, REP_TIMESTAMP)"
			+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	/*
	 * Must call within transaction (autocommit = false)
	 */
	private void insertInAccountRatePlanPoolMap(Connection conn, long poolId, List<Long> ratePlanIds, Date startDate, Date endDate, Date requestedDate, String requestedBy) throws DAOException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(INSERT_ACCOUNT_RATE_PLAN_POOL_MAP);
			for (Long ratePlanId : ratePlanIds) {
				ps.setLong(1, poolId);
				ps.setLong(2, ratePlanId);
				ps.setDate(3, new java.sql.Date(startDate.getTime()));
				ps.setDate(4, new java.sql.Date(endDate.getTime()));
				ps.setTimestamp(5, new java.sql.Timestamp(requestedDate.getTime()));
				ps.setString(6, requestedBy);
				ps.setTimestamp(7, new java.sql.Timestamp(requestedDate.getTime()));
				ps.setString(8, requestedBy);
				ps.setTimestamp(9, new java.sql.Timestamp(requestedDate.getTime()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException ex) {
			LOG.error("Failed to create account rate plan pool map. PoolID {}", poolId, ex);
			throw new DAOException("Failed to create account rate plan pool map");
		} finally {
			DBUtils.cleanup(null, ps);
		}

	}
	
	private static final String DELETE_ACCOUNT_RATE_PLAN_POOL_MAP = "DELETE FROM aerisgen.ACCOUNT_RATE_PLAN_POOL_MAP WHERE pool_id = ?";

	/*
	 * Must call within a transaction (autocommit = false)
	 */
	private void deleteAccountRatePlanPoolMap(Connection conn, long poolId) throws DAOException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(DELETE_ACCOUNT_RATE_PLAN_POOL_MAP);
			ps.setLong(1, poolId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			LOG.error("Failed to delete account rate plan pool map. PoolID {}", poolId, ex);
			throw new DAOException("Failed to delete account rate plan pool map");
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private static final String APPROVE_ACCOUNT_POOL = "UPDATE aerisgen.ACCOUNT_POOL SET status_id = ?, LAST_MODIFIED_DATE = ?, LAST_MODIFIED_BY = ?, REP_TIMESTAMP = ? WHERE pool_id = ?";

	private void approveAccountPool(Connection conn, long poolId, Date requestedDate, String requestedBy) throws DAOException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(APPROVE_ACCOUNT_POOL);
			ps.setInt(1, RatePlanPoolStatus.APPROVED.getValue());
			ps.setTimestamp(2, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(3, requestedBy);
			ps.setTimestamp(4, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setLong(5, poolId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			LOG.error("Failed to approve account pool. PoolID {}", poolId, ex);
			throw new DAOException("Failed to approve account pool");
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private static final String EXPIRE_ACCOUNT_POOL = "UPDATE aerisgen.ACCOUNT_POOL SET status_id = ?, end_date = ?, LAST_MODIFIED_DATE = ?, LAST_MODIFIED_BY = ?, REP_TIMESTAMP = ? WHERE pool_id = ?";

	private void expireAccountPool(Connection conn, long poolId, Date endDate, Date requestedDate, String requestedBy) throws DAOException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(EXPIRE_ACCOUNT_POOL);
			ps.setInt(1, RatePlanPoolStatus.EXPIRED.getValue());
			ps.setDate(2, new java.sql.Date(endDate.getTime()));
			ps.setTimestamp(3, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(4, requestedBy);
			ps.setTimestamp(5, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setLong(6, poolId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			LOG.error("Failed to expire account pool. PoolID {}", poolId, ex);
			throw new DAOException("Failed to expire account pool");
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private static final String EXPIRE_ACCOUNT_RATE_PLAN_POOL_MAP = "UPDATE aerisgen.ACCOUNT_RATE_PLAN_POOL_MAP SET end_date = ?, LAST_MODIFIED_DATE = ?, LAST_MODIFIED_BY = ?, REP_TIMESTAMP = ? WHERE pool_id = ?";
	
	private void expireAccountRatePlanPoolMap(Connection conn, long poolId, Date endDate, Date requestedDate, String requestedBy) throws DAOException {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(EXPIRE_ACCOUNT_RATE_PLAN_POOL_MAP);
			ps.setDate(1, new java.sql.Date(endDate.getTime()));
			ps.setTimestamp(2, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(3, requestedBy);
			ps.setTimestamp(4, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setLong(5, poolId);
			ps.executeUpdate();
		} catch (SQLException ex) {
			LOG.error("Failed to expire account rate plan pool map. PoolID {}", poolId, ex);
			throw new DAOException("Failed to expire account rate plan pool map");
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private static final String QUERY_RATE_PLAN_IDS = "SELECT rate_plan_id FROM aerisgen.rate_plan WHERE rate_plan_name in (:ratePlanIds)";
	
	private List<Long> getRatePlanIds(Connection conn, List<String> ratePlanNames) throws DAOException {
		Statement stmt = null;
		ResultSet rs = null;
		List<Long> ratePlanIds = new ArrayList<Long>();
		try {
			StringBuilder ratePlanNamesStrBuilder = new StringBuilder();
			for (String ratePlanName : ratePlanNames) {
				ratePlanNamesStrBuilder.append("'").append(ratePlanName).append("',");
			}
			String ratePlanNamesStr = ratePlanNamesStrBuilder.substring(0, ratePlanNamesStrBuilder.length()-1);
			String query = QUERY_RATE_PLAN_IDS.replace(":ratePlanIds", ratePlanNamesStr);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while(rs.next()) {
				ratePlanIds.add(rs.getLong("rate_plan_id"));
			}
		} catch (SQLException ex) {
			LOG.error("Failed to fetch rate plan ids", ex);
			throw new DAOException("Failed to fetch rate plan ids for rate plans");
		} finally {
			DBUtils.cleanup(null, stmt, rs);
		}
		return ratePlanIds;
	}

	@Override
	public AccountRatePlanPool createNewAccountRatePlanPool(int operatorId, long accountId, int productId, String ratePlanPoolName, List<String> ratePlans, Date startDate, 
			Date endDate, RatePlanPoolStatus status, Date requestedDate, String requestedUser, List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBucket) 
			throws RatePlanPoolException, DuplicateRatePlanPoolException, InvalidRatePlanPoolException {
		Connection conn = null;
		AccountRatePlanPool accountRatePlanPool = null;
		long newRatePlanPoolId = -1;
		
		LOG.info("createNewAccountRatePlanPool: processing create new account rate plan pool. AccountId {}, ratePlanPoolName {}", accountId, ratePlanPoolName);
		
		try {
			// Store pool name in upper case
			ratePlanPoolName = ratePlanPoolName.toUpperCase();
			
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);
			LOG.info("createNewAccountRatePlanPool: Fetched DB Connection");
			
			LOG.info("createNewAccountRatePlanPool: Validating pool name");
			validateAccountRatePlanPool(accountId, ratePlanPoolName, conn);
			
			LOG.info("createNewAccountRatePlanPool: Validating rate plans selected");
			validateRatePlans(accountId, ratePlans, conn);
			
			// Create Rate Plan Pool
			LOG.info("Creating rate plan pool");
			newRatePlanPoolId = createRatePlanPool(productId, ratePlanPoolName, startDate, endDate, status, requestedDate, requestedUser, conn);
			
			// Create Account Rate Plan Pool
			LOG.info("Creating account rate plan pool");
			createAccountRatePlanPool(accountId, ratePlanPoolName, ratePlans, startDate, endDate, status, requestedDate, requestedUser, conn, false);
			
			//TODO Need to come from input
			boolean poolInHierarchy = false;
			//TODO Should get ids from input instead of names
			List<Long> ratePlanIds = getRatePlanIds(conn, ratePlans);
			LOG.info("Inserting into account_pool and account_rate_plan_pool_map");
			createAccountPool(conn, accountId, productId, newRatePlanPoolId, ratePlanPoolName, ratePlanPoolName, ratePlanPoolName, status, startDate, endDate, poolInHierarchy, ratePlanIds, requestedDate, requestedUser);
			//create rate plan pool bucket as well
			createRatePlanPoolOverageBucket(accountId, newRatePlanPoolId, ratePlanPoolName, ratePlanPoolOverageBucket, conn);
			
			//AERPORT-5986 : Pool definition of Tiered rate plan should be propagated to child tiers
			List<RatePlan> subRatePlanIds = ratePlanDAO.getSubRatePlansInUse(ratePlanIds);
			if(subRatePlanIds != null && subRatePlanIds.size() > 0){
				List<Long> subRPIDs = new ArrayList<Long>();
				List<String> subRPNames = new ArrayList<String>();
				for(RatePlan rp : subRatePlanIds){
					subRPIDs.add(rp.getRatePlanId());
					subRPNames.add(rp.getRatePlanName());
				}
				insertInAccountRatePlanPoolMap(conn, newRatePlanPoolId, subRPIDs, startDate, endDate, requestedDate, requestedUser);
				
				createAccountRatePlanPool(accountId, ratePlanPoolName, subRPNames, startDate, endDate, status, requestedDate, requestedUser, conn, false);
			}
			
			
			// Commit the changes
			if(conn != null)
			{
				conn.commit();
			}
			
			// Return created account rate plan pool
			accountRatePlanPool = new AccountRatePlanPool();
			accountRatePlanPool.setAccountId(accountId);
			accountRatePlanPool.setRatePlanPoolId(newRatePlanPoolId);
			accountRatePlanPool.setRatePlanPoolName(ratePlanPoolName);
			accountRatePlanPool.setProductId(productId);
			accountRatePlanPool.setRatePlans(ratePlans);
			accountRatePlanPool.setStartDate(new java.sql.Date(startDate.getTime()));
			accountRatePlanPool.setEndDate(new java.sql.Date(endDate.getTime()));
			accountRatePlanPool.setStatus(status);
			accountRatePlanPool.setOperatorId(operatorId);		
			accountRatePlanPool.setRatePlanPoolOverageBucket(ratePlanPoolOverageBucket);
			
			// Put in cache
			putInCache(accountId, newRatePlanPoolId, accountRatePlanPool);
			
			LOG.info("Created New account rate plan pool = " + ReflectionToStringBuilder.toString(accountRatePlanPool));
		} catch (SQLException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new RatePlanPoolException(e.getMessage());
		} catch (DuplicateRatePlanPoolException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (InvalidRatePlanPoolException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (Exception e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new RatePlanPoolException(e.getMessage());
		} finally {
			DBUtils.cleanup(conn);
		}

		return accountRatePlanPool;
	}

	private void validateRatePlans(long accountId, List<String> ratePlans, Connection conn) throws SQLException, InvalidRatePlanPoolException {
		long monthlyRatePlanCount = getMonthlyRatePlansCount(accountId, ratePlans, conn);
		long yearlyRatePlanCount = getYearlyRatePlansCount(accountId, ratePlans, conn);
		
		if(monthlyRatePlanCount > 0 && yearlyRatePlanCount > 0)
		{
			LOG.error("createNewAccountRatePlanPool: Rate Plan pool cannot contain both monthly and yearly rate plans: rate plans: "+ReflectionToStringBuilder.toString(ratePlans));
			throw new InvalidRatePlanPoolException("Rate Plan pool cannot contain both monthly and yearly rate plans: rate plans: "+ReflectionToStringBuilder.toString(ratePlans));
		}
	}
	
	private long createRatePlanPool(long productId, String ratePlanPoolName, Date startDate, Date endDate, RatePlanPoolStatus status, Date requestedDate, String requestedUser,
			Connection conn) throws SQLException
	{		
		long newRatePlanPoolId = -1;
		PreparedStatement ps = null;
		
		try
		{
			newRatePlanPoolId = generateNewRatePlanPoolId(conn);
			LOG.info("Fetched newRatePlanPoolId for the new account rate plan pool to be created: " + newRatePlanPoolId);
			
			LOG.info("Creating sql statement: " + ps +", to create new rate plan pool with newRatePlanPoolId: "+newRatePlanPoolId);
			ps = conn.prepareStatement(SQL_CREATE_RATE_PLAN_POOL);
			
			LOG.debug("Setting Parameters");
			
			ps.setLong(1, newRatePlanPoolId);// pool_id
			ps.setString(2, ratePlanPoolName);// pool_name
			ps.setDate(3, new java.sql.Date(startDate.getTime()));// start_date 	
			ps.setDate(4, new java.sql.Date(endDate.getTime()));// end_date
			ps.setInt(5, 0);// valid_flag - default pending status
			ps.setInt(6, status.getValue());// valid_flag - default pending status
			ps.setLong(7, productId);// product_id
			ps.setDate(8, new java.sql.Date(requestedDate.getTime()));// last_modified_date
			ps.setString(9, requestedUser);// last_modified_by

			ps.executeUpdate();			
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
		
		return newRatePlanPoolId;
	}
	
	
	
	/**
	 * Creates account rate plan pool overage bucket
	 * 
	 * @param accountId
	 * @param newRatePlanPoolId
	 * @param ratePlanPoolName
	 * @param ratePlanPoolOverageBucketList
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	private long createRatePlanPoolOverageBucket(long accountId,long newRatePlanPoolId, String ratePlanPoolName, 
			List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBucketList, Connection conn) throws SQLException {		
		PreparedStatement ps = null;
		try {
			
			ps = conn.prepareStatement(SQL_CREATE_RATE_PLAN_POOL_OVRG_BKT);
			LOG.info("Creating sql statement: " + ps + ", create new overage bucket for ratePlanPoolId=" + newRatePlanPoolId );
			LOG.debug("Deleting existing RatePlanPoolOverageBucket entries from DB");
			//first delete any existing rate plan pool overage bucket
			deleteAccountRatePlanPoolOverageBuckets(accountId, newRatePlanPoolId, conn);
			if(ratePlanPoolOverageBucketList != null) {
				
				LOG.debug("Setting RatePlanPoolOverageBucket Parameters");
				for (AccountRatePlanPoolOverageBucket ratePlanPoolOverageBucket : ratePlanPoolOverageBucketList) {
					ps.setLong(1, newRatePlanPoolId);
					ps.setLong(2, accountId);
					ps.setDate(3, ratePlanPoolOverageBucket.getStartDate() != null ? new java.sql.Date(ratePlanPoolOverageBucket.getStartDate().getTime())  : new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					ps.setDate(4, ratePlanPoolOverageBucket.getEndDate() != null ? new java.sql.Date(ratePlanPoolOverageBucket.getEndDate().getTime())  : new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()) );
					double overageSize = ratePlanPoolOverageBucket.getBucketSize();
					if(ratePlanPoolOverageBucket.getBucketSizeType() != null && ratePlanPoolOverageBucket.getBucketSizeType().equalsIgnoreCase("GB")){
						overageSize = overageSize * 1024 ;
						BigDecimal bDec = new BigDecimal(String.valueOf(overageSize)).setScale(2, BigDecimal.ROUND_HALF_UP);
						overageSize = bDec.doubleValue();
					}
					ps.setDouble(5, overageSize);
					ps.setString(6, ratePlanPoolOverageBucket.getBucketSizeType());
					ps.setDouble(7, ratePlanPoolOverageBucket.getBucketPrice());
					ps.setInt(8, ratePlanPoolOverageBucket.getNoAccessFeeDeviceCount());
					ps.setInt(9, ratePlanPoolOverageBucket.getAutoAdd());
					if(ratePlanPoolOverageBucket.getCreatedDate() == null) {
						ratePlanPoolOverageBucket.setCreatedDate(new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					if(ratePlanPoolOverageBucket.getUpdatedDate() == null) {
						ratePlanPoolOverageBucket.setUpdatedDate(new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					ps.setDate(10, ratePlanPoolOverageBucket.getCreatedDate() != null ? ratePlanPoolOverageBucket.getCreatedDate() : new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					ps.setDate(11, ratePlanPoolOverageBucket.getUpdatedDate() != null ? ratePlanPoolOverageBucket.getUpdatedDate() : new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()) );
					//If createdBy is not present use value from updatedBy and vice-versa
					if(ratePlanPoolOverageBucket.getCreatedBy() == null) {
						ratePlanPoolOverageBucket.setCreatedBy(ratePlanPoolOverageBucket.getUpdatedBy());
					}
					if(ratePlanPoolOverageBucket.getUpdatedBy() == null) {
						ratePlanPoolOverageBucket.setUpdatedBy(ratePlanPoolOverageBucket.getCreatedBy());
					}
					ps.setString(12 , ratePlanPoolOverageBucket.getCreatedBy() );
					ps.setString(13 , ratePlanPoolOverageBucket.getUpdatedBy() );
                                        String repTimestamp = ratePlanPoolOverageBucket.getRepTimeStamp();
					if(repTimestamp != null && !repTimestamp.trim().isEmpty()) {
						ps.setTimestamp(14, ApplicationUtils.convertStringToTS(ratePlanPoolOverageBucket.getRepTimeStamp()));
					}else {
						ps.setTimestamp(14, new Timestamp(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					ps.addBatch();
				}
				LOG.debug("Inserting aerisgen.RatePlanPoolOverageBucket into the db");
				ps.executeBatch();		
			}
		} finally {
			DBUtils.cleanup(null, ps);
		}
		
		return newRatePlanPoolId;
	}
	
	
	/**
	 * sets the account rate plan pool overage buckets 
	 * 
	 * @param ratePlanPool
	 * @param accountId
	 * @param ratePlanPoolId
	 * @param conn
	 * @throws SQLException
	 */
	private void setAccountOverageBucket(AccountRatePlanPool ratePlanPool, long accountId, long ratePlanPoolId, Connection conn) throws SQLException {
		List<AccountRatePlanPoolOverageBucket> accOverageBktList = new ArrayList<AccountRatePlanPoolOverageBucket>();
		String getAccRatePlanPoolOverageBkt = "select * from aerisgen.ACCOUNT_POOL_OVERAGE_BUCKET where account_id = " 
				+ accountId + " and rate_plan_pool_id = " + ratePlanPoolId ;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// Create Prepared Statement
			ps = conn.prepareStatement(getAccRatePlanPoolOverageBkt);
			LOG.info("Created statement for fetching ACCOUNT_POOL_OVERAGE_BUCKET for accountId=" + accountId + " and rate_plan_pool_id =" + ratePlanPoolId );
			rs = ps.executeQuery();
			while(rs.next()) {
				AccountRatePlanPoolOverageBucket ratePlanPoolOvgBkt = new AccountRatePlanPoolOverageBucket();
				ratePlanPoolOvgBkt.setAutoAdd(rs.getInt("auto_add"));
				ratePlanPoolOvgBkt.setBucketPrice(rs.getDouble("bucket_price"));
				ratePlanPoolOvgBkt.setBucketSize(rs.getInt("bucket_size"));
				ratePlanPoolOvgBkt.setBucketSizeType(rs.getString("bucket_size_type"));
				double overageSize = ratePlanPoolOvgBkt.getBucketSize();
				if(ratePlanPoolOvgBkt.getBucketSizeType() != null && ratePlanPoolOvgBkt.getBucketSizeType().equalsIgnoreCase("GB")){
					overageSize = overageSize / 1024 ;
					BigDecimal bDec = new BigDecimal(String.valueOf(overageSize)).setScale(2, BigDecimal.ROUND_HALF_UP);
					overageSize = bDec.doubleValue();
				}
				ratePlanPoolOvgBkt.setBucketSize(overageSize);
				ratePlanPoolOvgBkt.setNoAccessFeeDeviceCount(rs.getInt("no_access_fee_device_count"));
				ratePlanPoolOvgBkt.setStartDate(rs.getDate("start_date"));
				ratePlanPoolOvgBkt.setEndDate(rs.getDate("end_date"));
				ratePlanPoolOvgBkt.setCreatedDate(rs.getDate("created_date"));
				ratePlanPoolOvgBkt.setUpdatedDate(rs.getDate("last_modified_date"));
				ratePlanPoolOvgBkt.setCreatedBy(rs.getString("created_by"));
				ratePlanPoolOvgBkt.setUpdatedBy(rs.getString("last_modified_by"));
				ratePlanPoolOvgBkt.setRepTimeStamp(ApplicationUtils.getGMTDateStr(rs.getTimestamp("rep_timestamp")));
				accOverageBktList.add(ratePlanPoolOvgBkt);
			}
			ratePlanPool.setRatePlanPoolOverageBucket(accOverageBktList);
		} catch (SQLException e) {
			LOG.error("Exception during getAccountOverageBucket for accountId= " + accountId, e);
			throw new GenericServiceException("Exception during getAccountOverageBucket", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		
	}
	
	private void createAccountRatePlanPool(long accountId, String ratePlanPoolName, List<String> ratePlans, Date startDate, Date endDate, RatePlanPoolStatus status,
			Date requestedDate,	String requestedUser, Connection conn, boolean isUpldateRatePlan) throws SQLException
	{		
		long newAccountRatePlanPoolId = -1;
		PreparedStatement ps = null;
		
		try
		{
			LOG.info("Fetched newAccountRatePlanPoolId for the new account rate plan pool to be created: " + newAccountRatePlanPoolId);
			
			LOG.debug("Creating Prepared Statement to create account rate plan pool data in t_account_rate_pool_map table");
			
			ps = conn.prepareStatement(SQL_CREATE_ACCOUNT_RATE_POOL);

			LOG.debug("Setting Parameters");
			
			for (String ratePlan : ratePlans) {
				newAccountRatePlanPoolId = generateNewAccountRatePlanPoolId(conn);
				ps.clearParameters();
								
				ps.setLong(1, newAccountRatePlanPoolId);// account_rate_pool_map_id
				ps.setString(2, ratePlanPoolName);// pool_name
				ps.setString(3, ratePlan);// rate_plan
				ps.setLong(4, accountId);// account_id
				ps.setDate(5, new java.sql.Date(startDate.getTime()));// start_date 	
				ps.setDate(6, new java.sql.Date(endDate.getTime()));// end_date
				if(isUpldateRatePlan)
					ps.setInt(7, status.getValue());// valid_flag - in case of update rateplanPool call
				else 
					ps.setInt(7, 0);// valid_flag - default pending status
				ps.setDate(8, new java.sql.Date(requestedDate.getTime()));// last_modified_date
				ps.setString(9, requestedUser);// last_modified_by
				
				ps.addBatch();
			}

			ps.executeBatch();
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}		
	}
	

	@Override
	public AccountRatePlanPool updateAccountRatePlanPool(int operatorId, long accountId, long ratePlanPoolId, int productId, String ratePlanPoolName, 
			List<String> ratePlans, Date startDate,	Date endDate, RatePlanPoolStatus status, Date requestedDate, String requestedUser, List<AccountRatePlanPoolOverageBucket> ratePlanPoolOverageBuckets) 
			throws RatePlanPoolNotFoundException, RatePlanPoolException, BadStartDateException, DuplicateRatePlanPoolException, InvalidRatePlanPoolException {
		Connection conn = null;
		AccountRatePlanPool accountRatePlanPool = null;
		
		LOG.info("updateAccountRatePlanPool: processing update account rate plan pool...");
		
		try {
			// Store pool name in upper case
			ratePlanPoolName = ratePlanPoolName.toUpperCase();
			
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);
			LOG.info("updateAccountRatePlanPool: Fetched DB Connection");
			
			LOG.info("updateAccountRatePlanPool: Validating if rate plan pool exists for the account");
			AccountRatePlanPool ratePlanPool = getAccountRatePlanPool(operatorId, accountId, ratePlanPoolId);
			
			if(ratePlanPool == null)
			{
				LOG.error("Rate Plan Pool: "+ratePlanPoolId+" does not exist for account: "+accountId);
				throw new RatePlanPoolNotFoundException("Rate Plan Pool: "+ratePlanPoolId+" does not exist for account: "+accountId);
			}
			if(!ratePlanPoolName.equalsIgnoreCase(ratePlanPool.getRatePlanPoolName())) {
				LOG.error("RatePlanPoolName cannot be edited, ratePlanPoolId=" + ratePlanPoolId);
				throw new RatePlanPoolException("Rate Plan Pool Name cannot be edited");
			}
			
			// Check the start date is unaltered
			if(!startDate.equals(ratePlanPool.getStartDate()))
			{	
				// If startDate is altered, if it in the past, then throw bad request
				if(!oaUtils.isFutureDate(startDate))
				{
					LOG.error("The startDate cannot be in the past, startDate: "+startDate);
					throw new BadStartDateException("The startDate cannot be in the past");
				}
				else
				{
					LOG.info("The startDate is validated as a future date");					
				}
			}
			else
			{
				LOG.info("The startDate has not been changed, current start date value: "+ratePlanPool.getStartDate()+", selected start date value: "+startDate);					
			}
						
			LOG.info("updateAccountRatePlanPool: Validating pool name");
			validateAccountRatePlanPoolForUpdate(accountId, ratePlanPool.getRatePlanPoolName(), ratePlanPoolName, conn);
			
			LOG.info("updateAccountRatePlanPool: Validating rate plans selected");
			validateRatePlans(accountId, ratePlans, conn);
			
			// Update Rate Plan Pool
			LOG.info("updating rate plan pool");
			updateRatePlanPool(ratePlanPoolId, productId, ratePlanPoolName, startDate, endDate, status, requestedDate, requestedUser, conn);
			
			// Update Account Rate Plan Pool
			LOG.info("updating account rate plan pool");
			updateAccountRatePlanPool(accountId, ratePlanPoolName, status, ratePlans, startDate, endDate, requestedDate, requestedUser, conn);
			
			//TODO Need to come from input
			boolean poolInHierarchy = false;
			//TODO Should get ids from input instead of names
			List<Long> ratePlanIds = getRatePlanIds(conn, ratePlans);
			LOG.info("Inserting into account_pool and account_rate_plan_pool_map");
			updateAccountPool(conn, ratePlanPoolId, ratePlanPoolName, ratePlanPoolName, ratePlanPoolName, status, startDate, endDate, poolInHierarchy, requestedDate, productId, requestedUser);
			
			//Delete and insert in account_rate_plan_pool_map
			deleteAccountRatePlanPoolMap(conn, ratePlanPoolId);
			insertInAccountRatePlanPoolMap(conn, ratePlanPoolId, ratePlanIds, startDate, endDate, requestedDate, requestedUser);
			//Update the account rate plan overage bucket
			createRatePlanPoolOverageBucket(accountId, ratePlanPoolId, ratePlanPoolName, ratePlanPoolOverageBuckets, conn);
			
			//AERPORT-5986 : Pool definition of Tiered rate plan should be propagated to child tiers
			List<RatePlan> subRatePlanIds = ratePlanDAO.getSubRatePlansInUse(ratePlanIds);
			if(subRatePlanIds != null && subRatePlanIds.size() > 0){
				List<Long> subRPIDs = new ArrayList<Long>();
				List<String> subRPNames = new ArrayList<String>();
				for(RatePlan rp : subRatePlanIds){
					subRPIDs.add(rp.getRatePlanId());
					subRPNames.add(rp.getRatePlanName());
				}
				//delete is already done. now insert in t_account_rate_pool_map
				createAccountRatePlanPool(accountId, ratePlanPoolName, subRPNames, startDate, endDate, status, requestedDate, requestedUser, conn, true);
				
				//delete is already done. now insert in account_rate_plan_pool_map
				insertInAccountRatePlanPoolMap(conn, ratePlanPoolId, subRPIDs, startDate, endDate, requestedDate, requestedUser);
			}
			
			// Commit the changes
			if(conn != null)
			{
				conn.commit();
			}
			
			// Return updated account rate plan pool
			accountRatePlanPool = new AccountRatePlanPool();
			accountRatePlanPool.setAccountId(accountId);
			accountRatePlanPool.setRatePlanPoolId(ratePlanPoolId);
			accountRatePlanPool.setRatePlanPoolName(ratePlanPoolName);
			accountRatePlanPool.setProductId(productId);
			accountRatePlanPool.setRatePlans(ratePlans);
			accountRatePlanPool.setStartDate(new java.sql.Date(startDate.getTime()));
			accountRatePlanPool.setEndDate(new java.sql.Date(endDate.getTime()));
			accountRatePlanPool.setStatus(status);
			accountRatePlanPool.setOperatorId(operatorId);		
			accountRatePlanPool.setRatePlanPoolOverageBucket(ratePlanPoolOverageBuckets);
			
			// Put in cache
			putInCache(accountId, ratePlanPoolId, accountRatePlanPool);
			
			LOG.info("Updated account rate plan pool = " + ReflectionToStringBuilder.toString(accountRatePlanPool));
		} catch (SQLException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new RatePlanPoolException(e.getMessage());
		}  catch (RatePlanPoolNotFoundException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (DuplicateRatePlanPoolException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (InvalidRatePlanPoolException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (BadStartDateException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (Exception e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new RatePlanPoolException(e.getMessage());
		} finally {
			DBUtils.cleanup(conn);
		}

		return accountRatePlanPool;
	}
	
	private void updateAccountRatePlanPool(long accountId, String ratePlanPoolName, RatePlanPoolStatus status, List<String> ratePlans, Date startDate, Date endDate, 
			Date requestedDate, String requestedUser, Connection conn) throws SQLException {
		LOG.info("Delete existing account rate plan pools with ratePlanPoolName: "+ratePlanPoolName+" for accountId: "+accountId);
		deleteAccountRatePlanPool(accountId, ratePlanPoolName, conn);
		
		LOG.info("Creating account rate plan pool");
		createAccountRatePlanPool(accountId, ratePlanPoolName, ratePlans, startDate, endDate, status, requestedDate, requestedUser, conn, true);
	}

	private boolean deleteAccountRatePlanPool(long accountId, String ratePlanPoolName, Connection conn) throws SQLException {
		boolean success = false;
		PreparedStatement ps = null;
		
		try
		{
			LOG.info("Creating sql statement: " + ps +", to delete rate plan pools with ratePlanPoolName: "+ratePlanPoolName);
			ps = conn.prepareStatement(SQL_DELETE_ACCOUNT_RATE_POOL);
			
			LOG.debug("Setting Parameters");
			
			ps.setString(1, ratePlanPoolName);// pool_name
			ps.setLong(2, accountId);// account_id
			
			ps.executeUpdate();	
			success = true;
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
		
		return success;
	}
	
	
	/**
	 * Deletes the associated rate plan pool overage buckets 
	 * 
	 * @param accountId
	 * @param ratePlanPoolId
	 * @param conn
	 * @throws SQLException
	 */
	private void deleteAccountRatePlanPoolOverageBuckets(long accountId, long ratePlanPoolId, Connection conn ) throws SQLException {
		String deleteRatePlanPoolOvgBkt = "DELETE FROM aerisgen.ACCOUNT_POOL_OVERAGE_BUCKET WHERE account_id = " + accountId  + " and rate_plan_pool_id = " + ratePlanPoolId;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(deleteRatePlanPoolOvgBkt);
			ps.executeUpdate();
			LOG.info("Deleted aerisgen.ACCOUNT_POOL_OVERAGE_BUCKET with accountId=" + accountId + " and ratePlanPoolId=" + ratePlanPoolId);
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting aerisgen.ACCOUNT_POOL_OVERAGE_BUCKET for accountId=" + accountId  + " and ratePlanPoolId=" + ratePlanPoolId , sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private boolean updateRatePlanPool(long ratePlanPoolId, long productId, String ratePlanPoolName, Date startDate, Date endDate, RatePlanPoolStatus status, 
			Date requestedDate, String requestedUser, Connection conn) throws SQLException, RatePlanPoolNotFoundException
	{		
		boolean success = false;
		PreparedStatement ps = null;
		
		try
		{
			LOG.info("Creating sql statement: " + ps +", to update rate plan pool with ratePlanPoolId: "+ratePlanPoolId);
			ps = conn.prepareStatement(SQL_UPDATE_RATE_PLAN_POOL);
			
			LOG.debug("Setting Parameters");
			
			ps.setString(1, ratePlanPoolName);// pool_name
			ps.setDate(2, new java.sql.Date(startDate.getTime()));// start_date 	
			ps.setDate(3, new java.sql.Date(endDate.getTime()));// end_date
			ps.setInt(4, 1);// valid_flag - default approved
			ps.setLong(5, productId);// product_id
			ps.setDate(6, new java.sql.Date(requestedDate.getTime()));// last_modified_date
			ps.setString(7, requestedUser);// last_modified_by
			ps.setLong(8, ratePlanPoolId);// pool_id
			
			int count = ps.executeUpdate();
			
			if(count <= 0)
			{
				throw new RatePlanPoolNotFoundException("Rate Plan Pool: "+ratePlanPoolId+" not found");
			}
			
			success = true;
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
		
		return success;
	}

	@Override
	public boolean invalidateRatePlanPool(int operatorId, long accountId, long ratePlanPoolId, Date requestedDate, String requestedUser) 
			throws RatePlanPoolNotFoundException, RatePlanPoolException {
		Connection conn = null;
		boolean success = false;
		
		LOG.info("invalidateRatePlanPool: processing invalidate rate plan pool request...");
		
		try
		{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);
			LOG.info("invalidateRatePlanPool: Fetched DB Connection");
						
			LOG.info("invalidateRatePlanPool: invalidating account rate plan pool...");
			invalidateAccountRatePlanPool(operatorId, accountId, ratePlanPoolId, requestedDate, requestedUser, conn);
			
			LOG.info("invalidateRatePlanPool: invalidating account rate plan pool...");
			invalidateRatePlanPool(operatorId, accountId, ratePlanPoolId, requestedDate, requestedUser, conn);	
			
			//expire in account_pool and account_rate_plan_pool_map
			expireAccountPool(conn, ratePlanPoolId, requestedDate, requestedDate, requestedUser);
			expireAccountRatePlanPoolMap(conn, ratePlanPoolId, requestedDate, requestedDate, requestedUser);
			
			// Commit the changes
			if(conn != null)
			{
				conn.commit();
			}
			
			// Remove expired rate plan pool from Cache
			removeFromCache(accountId, ratePlanPoolId);
			
			success = true;
		} catch (SQLException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new RatePlanPoolException(e.getMessage());
		}  catch (RatePlanPoolNotFoundException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (Exception e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new RatePlanPoolException(e.getMessage());
		} finally {
			DBUtils.cleanup(conn);
		}
		
		return success;
	}
	
	private void invalidateAccountRatePlanPool(int operatorId, long accountId, long ratePlanPoolId, Date requestedDate, String requestedUser, Connection conn) 
			throws RatePlanPoolNotFoundException, SQLException {
		PreparedStatement ps = null;
		
		try
		{
			LOG.info("Creating sql statement: " + ps +", to invalidate account rate plan pool with ratePlanPoolId: "+ratePlanPoolId);
			ps = conn.prepareStatement(SQL_DEACTIVATE_ACCOUNT_RATE_POOL);
			
			LOG.debug("Setting Parameters");
			
			ps.setDate(1, new java.sql.Date(requestedDate.getTime()));// account_id
			ps.setLong(2, ratePlanPoolId);// pool_id
			ps.setLong(3, accountId);// account_id
			
			int updateCnt = ps.executeUpdate();	
			
			if(updateCnt == 0)
			{
				throw new RatePlanPoolNotFoundException("Rate Plan Pool: "+ratePlanPoolId+" does not exist for account: "+accountId);
			}
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void invalidateRatePlanPool(int operatorId, long accountId, long ratePlanPoolId, Date requestedDate, String requestedUser, Connection conn) 
			throws RatePlanPoolNotFoundException, SQLException {
		PreparedStatement ps = null;
		
		LOG.info("invalidateRatePlanPool: invalidating rate plan pool...");
		
		try {
			
			LOG.info("Creating sql statement: " + ps +", to invalidate rate plan pool with ratePlanPoolId: "+ratePlanPoolId);
			ps = conn.prepareStatement(SQL_DEACTIVATE_RATE_PLAN_POOL);
			
			LOG.debug("Setting Parameters");
			
			ps.setInt(1, RatePlanPoolStatus.EXPIRED.getValue());// status
			ps.setDate(2, new java.sql.Date(requestedDate.getTime()));// end_date
			ps.setLong(3, ratePlanPoolId);// pool_id
			
			int updateCnt = ps.executeUpdate();	
			
			if(updateCnt == 0)
			{
				throw new RatePlanPoolNotFoundException("Rate Plan Pool: "+ratePlanPoolId+" does not exist");				
			}
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
	}
	


	@Override
	public boolean updateRatePlanPoolStatus(int operatorId, long accountId, long ratePlanPoolId, RatePlanPoolStatus status, Date requestedDate, String requestedUser) throws RatePlanPoolNotFoundException,
			RatePlanPoolException {
		Connection conn = null;
		boolean success = false;
		
		LOG.info("approveRatePlanPool: processing approve rate plan pool request...");
		
		try
		{
			LOG.info("approveRatePlanPool: Validating if rate plan pool exists for the account");
			
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			conn.setAutoCommit(false);
			LOG.info("approveRatePlanPool: Fetched DB Connection");
						
			LOG.info("approveRatePlanPool: approving account rate plan pool...");
			updateAccountRatePlanPoolStatus(operatorId, accountId, ratePlanPoolId, status, requestedDate, requestedUser, conn);
			
			LOG.info("approveRatePlanPool: approving account rate plan pool...");
			updateRatePlanPoolStatus(operatorId, accountId, ratePlanPoolId, status, requestedDate, requestedUser, conn);	
			
			LOG.info("Approve in account_pool");
			approveAccountPool(conn, ratePlanPoolId, requestedDate, requestedUser);
			// Commit the changes
			if(conn != null)
			{
				conn.commit();
			}
			
			// Update Cache
			AccountRatePlanPool accountRatePlanPool = getFromCache(accountId, ratePlanPoolId);
			
			if(accountRatePlanPool != null)
			{
				accountRatePlanPool.setStatus(status);
			}
			
			// Put in cache
			putInCache(accountId, ratePlanPoolId, accountRatePlanPool);
			
			success = true;
		} catch (SQLException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new RatePlanPoolException(e.getMessage());
		}  catch (RatePlanPoolNotFoundException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw e;
		} catch (Exception e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}

			throw new RatePlanPoolException(e.getMessage());
		} finally {
			DBUtils.cleanup(conn);
		}
		
		return success;
	}
	
	private void updateAccountRatePlanPoolStatus(int operatorId, long accountId, long ratePlanPoolId, RatePlanPoolStatus status, 
			Date requestedDate, String requestedUser, Connection conn) 
			throws RatePlanPoolNotFoundException, SQLException {
		PreparedStatement ps = null;
		
		LOG.info("approveAccountRatePlanPool: approving account rate plan pool...");
		
		try
		{
			LOG.info("Creating sql statement: " + ps +", to approve account rate plan pool with ratePlanPoolId: "+ratePlanPoolId);
			ps = conn.prepareStatement(SQL_APPROVE_ACCOUNT_RATE_POOL);
			
			LOG.debug("Setting Parameters");
			
			ps.setLong(1, ratePlanPoolId);// pool_id
			ps.setLong(2, accountId);// account_id
			
			int updateCnt = ps.executeUpdate();	
			
			if(updateCnt == 0)
			{
				throw new RatePlanPoolNotFoundException("Rate Plan Pool: "+ratePlanPoolId+" does not exist for account: "+accountId);
			}
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void updateRatePlanPoolStatus(int operatorId, long accountId, long ratePlanPoolId, RatePlanPoolStatus status, Date requestedDate, String requestedUser, Connection conn) 
			throws RatePlanPoolNotFoundException, SQLException {
		PreparedStatement ps = null;
		
		LOG.info("approveRatePlanPool: approving rate plan pool...");
		
		try {
			
			LOG.info("Creating sql statement: " + ps +", to approval rate plan pool with ratePlanPoolId: "+ratePlanPoolId);
			ps = conn.prepareStatement(SQL_APPROVE_RATE_PLAN_POOL);
			
			LOG.debug("Setting Parameters");
			
			ps.setInt(1, status.getValue());// status
			ps.setLong(2, ratePlanPoolId);// pool_id
			
			int updateCnt = ps.executeUpdate();	
			
			if(updateCnt == 0)
			{
				throw new RatePlanPoolNotFoundException("Rate Plan Pool: "+ratePlanPoolId+" does not exist for account: "+accountId);
			}
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
	}
		
	private long getMonthlyRatePlansCount(long accountId, List<String> ratePlans, Connection conn) throws SQLException {
		LOG.info("getMonthlyRatePlansCount: fetching monthly rate plan count from the list of rate plans: "+ReflectionToStringBuilder.toString(ratePlans));
		
		long monthlyRatePlanCount = getRatePlansCount(accountId, ratePlans, SQL_GET_MONTHLY_RATE_PLAN_COUNT, conn);
		LOG.info("getMonthlyRatePlansCount: no of monthly rate plan found: "+monthlyRatePlanCount);
		
		return monthlyRatePlanCount;
	}
		
	private long getYearlyRatePlansCount(long accountId, List<String> ratePlans, Connection conn) throws SQLException {
		LOG.info("getYearlyRatePlansCount: fetching monthly rate plan count from the list of rate plans: "+ReflectionToStringBuilder.toString(ratePlans));
		
		long yearyRatePlanCount = getRatePlansCount(accountId, ratePlans, SQL_GET_YEARLY_RATE_PLAN_COUNT, conn);
		LOG.info("getYearlyRatePlansCount: no of yearly rate plan found: "+yearyRatePlanCount);
		
		return yearyRatePlanCount;
	}
		
	private long getRatePlansCount(long accountId, List<String> ratePlans, String ratePlanCountQuery, Connection conn) throws SQLException {
		long ratePlanCount = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("getRatePlansCount: fetching rate plans count for rate plans: "+ReflectionToStringBuilder.toString(ratePlans));

			StringBuilder query = buildGetRatePlanCountQuery(ratePlanCountQuery, ratePlans);
			
			int index = 0;
			
			// Create SQL Statement
			ps = conn.prepareStatement(query.toString());

			for (int i = 0; i < ratePlans.size(); i++) {
				String ratePlan = ratePlans.get(i);
				
				if (ratePlan != null) {
					ps.setString(++index, ratePlan);
				}
			}			
			
			ps.setLong(++index, accountId);

			LOG.info("getRatePlansCount: Executing query: "+ps.toString());

			// Run Query
			rs = ps.executeQuery();

			// Get the last account_id in the table
			if (rs.next()) {
				ratePlanCount = rs.getInt(1);
			}

			LOG.info("No of RatePlans: " + ratePlanCount);
		} catch (SQLException e) {
			LOG.error("Exception during getRatePlansCount - while fetching count", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return ratePlanCount;
	}
	
	private StringBuilder buildGetRatePlanCountQuery(String ratePlanCountQuery, List<String> ratePlans) {
		StringBuilder query;

		// Build getAccounts by Status query
		StringBuffer inClause = new StringBuffer();

		for (int i = 0; i < ratePlans.size(); i++) {
			String ratePlan = ratePlans.get(i);
			
			if (ratePlan != null) {
				inClause.append("?");

				if (i != ratePlans.size() - 1) {
					inClause.append(",");
				}
			}
		}

		if (StringUtils.isNotEmpty(inClause.toString())) {
			inClause = new StringBuffer("(").append(inClause.toString()).append(")");
		}

		String getMonthlyRatePlanCountQuery = ratePlanCountQuery.replace(":rate_plans", inClause.toString());
		query = new StringBuilder(getMonthlyRatePlanCountQuery);
		
		return query;
	}
	
	private boolean validateAccountRatePlanPool(long accountId, String ratePlanPoolName, Connection conn) throws SQLException, DuplicateRatePlanPoolException {
		boolean success = true;
		int ratePlanPoolCount = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("validateAccountRatePlanPool: validating the account rate plan pool name in t_account_rate_pool_map table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_VALIDATE_ACCOUNT_RATE_POOL);
			ps.setString(1, ratePlanPoolName);
			ps.setLong(2, accountId);

			// Run Query
			rs = ps.executeQuery();

			// Get the last account_id in the table
			while (rs.next()) {
				ratePlanPoolCount = rs.getInt("rate_pool_count");
				success = (ratePlanPoolCount == 0); 
			}

			if(success)
			{
				LOG.info("Rate Plan Pool Name is validated successfully");
			}
			else
			{
				LOG.error("Pool Name already exists for the account: "+accountId);
				throw new DuplicateRatePlanPoolException("Pool Name already exists for the account");
			}
		} catch (SQLException e) {
			LOG.error("Exception during validateAccountRatePlanPool", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return success;
	}
	
	private boolean validateAccountRatePlanPoolForUpdate(long accountId, String oldRatePlanPoolName, String ratePlanPoolName, Connection conn) 
			throws SQLException, DuplicateRatePlanPoolException {
		boolean success = true;
		int ratePlanPoolCount = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("validateAccountRatePlanPoolForUpdate: validating the account rate plan pool name in t_account_rate_pool_map table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_VALIDATE_ACCOUNT_RATE_POOL_FOR_UPDATE);
			ps.setString(1, ratePlanPoolName);
			ps.setLong(2, accountId);
			ps.setString(3, oldRatePlanPoolName);

			// Run Query
			rs = ps.executeQuery();

			// Get the last account_id in the table
			while (rs.next()) {
				ratePlanPoolCount = rs.getInt("rate_pool_count");
				success = (ratePlanPoolCount == 0); 
			}

			if(success)
			{
				LOG.info("Rate Plan Pool Name is validated successfully");
			}
			else
			{
				LOG.error("Pool Name already exists for the account: "+accountId);
				throw new DuplicateRatePlanPoolException("Pool Name already in use by another rate plan pool");
			}
		} catch (SQLException e) {
			LOG.error("Exception during validateAccountRatePlanPoolForUpdate", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return success;
	}
	
	private long generateNewAccountRatePlanPoolId(Connection conn) throws SQLException {
		long accountRatePlanPoolId = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("createNewAccount: fetching the max account rate plan pool id in t_account_rate_pool_map table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_MAX_ACCOUNT_RATE_PLAN_POOL_ID);

			// Run Query
			rs = ps.executeQuery();

			// Get the last account_id in the table
			if (rs.next()) {
				accountRatePlanPoolId = rs.getLong("account_rate_pool_map_id");
			}

			LOG.info("Account Rate Plan Pool Id generated: " + accountRatePlanPoolId);
		} catch (SQLException e) {
			LOG.error("Exception during createNewAccountRatePlan - fetchMaxAccountRatePlanPoolId", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return accountRatePlanPoolId;
	}
	

	private long generateNewRatePlanPoolId(Connection conn) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long poolId = 0;
		try {
			LOG.debug("createNewAccount: fetching the max rate plan pool id in t_pool table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_MAX_RATE_PLAN_POOL_ID);

			// Run Query
			rs = ps.executeQuery();

			// Get the last account_id in the table
			if (rs.next()) {
				poolId = rs.getLong("pool_id");
			}
			
			LOG.info("Rate Plan Pool Id generated: " + poolId);
		} catch (SQLException e) {
			LOG.error("Exception during createNewAccountRatePlan - fetchMaxRatePlanPoolId", e);
			throw e;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return poolId;
	}
	
	private AccountRatePlanPool getFromCache(long accountId, long ratePlanPoolId) {
		AccountRatePlanPool accountRatePlanPool = null;
		Map<Long, AccountRatePlanPool> accountRatePlanPoolMap = cache.get(accountId);
		
		if(accountRatePlanPoolMap != null)
		{
			accountRatePlanPool = accountRatePlanPoolMap.get(ratePlanPoolId);	
		}
		
		return accountRatePlanPool;
	}
	
	private void removeFromCache(long accountId, long ratePlanPoolId){
		Map<Long, AccountRatePlanPool> accountRatePlanPoolMap = cache.get(accountId);
		if(accountRatePlanPoolMap != null)
		{
			accountRatePlanPoolMap.remove(ratePlanPoolId);
			//update cache
			cache.put(accountId,accountRatePlanPoolMap);
		}
		
	}
	
	private void putInCache(long accountId, List<AccountRatePlanPool> accountRatePlanPools) {
		Map<Long, AccountRatePlanPool> accountRatePlanPoolMap = cache.get(accountId);
		
		if(accountRatePlanPoolMap == null)
		{
			accountRatePlanPoolMap = new HashMap<Long, AccountRatePlanPool>();	
		}
		
		for (AccountRatePlanPool accountRatePlanPool : accountRatePlanPools) {
			accountRatePlanPoolMap.put(accountRatePlanPool.getRatePlanPoolId(), accountRatePlanPool);
		}
		
		cache.put(accountId, accountRatePlanPoolMap);
	}
	
	private void putInCache(long accountId, long ratePlanPoolId, AccountRatePlanPool accountRatePlanPool)
	{
		Map<Long, AccountRatePlanPool> accountRatePlanPoolMap = cache.get(accountId);
		
		if(accountRatePlanPoolMap == null)
		{
			accountRatePlanPoolMap = new HashMap<Long, AccountRatePlanPool>();	
		}
		
		// Update Cache
		accountRatePlanPoolMap.put(ratePlanPoolId, accountRatePlanPool);
		cache.put(accountId, accountRatePlanPoolMap);
	}
	
	
}
