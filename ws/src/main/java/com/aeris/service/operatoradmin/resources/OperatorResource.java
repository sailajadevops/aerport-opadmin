package com.aeris.service.operatoradmin.resources;

import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.constraints.Pattern;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IOperatorConfigDAO;
import com.aeris.service.operatoradmin.dao.IOperatorProductDAO;
import com.aeris.service.operatoradmin.dao.IProductCarrierDAO;
import com.aeris.service.operatoradmin.dao.IRatePlanDAO;
import com.aeris.service.operatoradmin.exception.OperatorException;
import com.aeris.service.operatoradmin.model.Carrier;
import com.aeris.service.operatoradmin.model.CarrierRatePlan;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;

/**
 * Internal Rest Resource for fetching master data in portal. 
 * Includes methods for
 *  
 * <ul>
 * <li>
 * Fetch Products available in portal
 * </li>
 * <li>
 * Fetch list of operators in portal
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class OperatorResource{
	private static final Logger LOG = LoggerFactory.getLogger(OperatorResource.class);

    @Inject
	private IOperatorProductDAO operatorProductDAO;  
    
    @Inject
	private IProductCarrierDAO productCarrierDAO;
    
    @Inject
	private IOperatorConfigDAO operatorConfigDAO;
    
    @Inject
	private IRatePlanDAO ratePlanDAO;
    
    @GET
    @Path("/products")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getProducts(@PathParam("operatorId") @ValidOperator final String operatorId) {
    	LOG.info("Received request to fetch all Products associated to operator id: "+operatorId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<Product> products = null;
		
		try {
			// Fetch from database
			products = operatorProductDAO.getProductsByOperator(Integer.parseInt(operatorId));
						
			if (products != null) {
				LOG.info("fetched products successfully, no of products: "+products.size());

				ObjectMapper mapper = new ObjectMapper();
				String productsJson = mapper.writeValueAsString(products);

				builder.entity(productsJson);
			} else {
				LOG.info("No products found in the db associated to operator id: "+operatorId);
				return Response.status(Status.NOT_FOUND).entity("No products found in the db associated to operator id: "+operatorId).build();
			}
		} catch (Exception e) {
			LOG.error("getProducts Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting products").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getProducts: " + response);
		return response;
	}
    
    @GET
    @Path("/products/{productId}/carriers")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getCarriers(@PathParam("productId") @Pattern(regexp = "[0-9]+", message = "{productId.number}") final String productId) {
    	LOG.info("Received request to fetch all carriers associated to product id: "+productId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<Carrier> carriers = null;
		
		try {
			// Fetch from database
			carriers = productCarrierDAO.getCarriersByProduct(Integer.parseInt(productId));
						
			if (carriers != null) {
				LOG.info("fetched carriers successfully, no of carriers: "+carriers.size());

				ObjectMapper mapper = new ObjectMapper();
				String carriersJson = mapper.writeValueAsString(carriers);

				builder.entity(carriersJson);
			} else {
				LOG.info("No carriers found in the db associated to product id: "+productId);
				return Response.status(Status.NOT_FOUND).entity("No carriers found in the db associated to product id: "+productId).build();
			}
		} catch (Exception e) {
			LOG.error("getCarriers Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting carriers").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getCarriers: " + response);
		return response;
	}    
    
    @GET
    @Path("/products/{productId}/carrierrateplans")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getCarrierRatePlans(@PathParam("productId") @Pattern(regexp = "[0-9]+", message = "{productId.number}") final String productId, 
			@QueryParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId) {
    	LOG.info("Received request to fetch all carrier rate plans associated to product id: "+productId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		List<CarrierRatePlan> carrierRatePlans = null;
		
		try {
			// Fetch from database
			if(accountId != null && !accountId.isEmpty()){
				carrierRatePlans = ratePlanDAO.getCarrierRatePlans(Integer.parseInt(productId), Integer.parseInt(accountId));
			}else{
				carrierRatePlans = ratePlanDAO.getCarrierRatePlans(Integer.parseInt(productId));
			}
			
						
			if (carrierRatePlans != null) {
				LOG.info("fetched carrier rate plans successfully, no of carrier rate plans: "+carrierRatePlans.size());

				ObjectMapper mapper = new ObjectMapper();
				String carrierRatePlansJson = mapper.writeValueAsString(carrierRatePlans);

				builder.entity(carrierRatePlansJson);
			} else {
				LOG.info("No carrier rate plans found in the db associated to product id: "+productId);
				return Response.status(Status.NOT_FOUND).entity("No carrier rate plans found in the db associated to product id: "+productId).build();
			}
		} catch (Exception e) {
			LOG.error("getCarrierRatePlans Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting carrier rate plans").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getCarrierRatePlans: " + response);
		return response;
	}    

    @GET
    @Path("/config")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getOperatorConfiguration(@PathParam("operatorId") @ValidOperator final String operatorId) {
    	LOG.info("Received request to fetch operator configuration for operator id: "+operatorId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Map<String, String> configuration = null;
		
		try {
			// Fetch from database
			configuration = operatorConfigDAO.getOperatorConfiguration(Integer.parseInt(operatorId));
						
			if (configuration != null) {
				LOG.info("fetched configuration successfully, no of configuration properties: "+configuration.size());

				ObjectMapper mapper = new ObjectMapper();
				String configurationJson = mapper.writeValueAsString(configuration);

				builder.entity(configurationJson);	
			} else {
				LOG.info("No operator configuration properties found in the db associated to operator id: "+operatorId);
				return Response.status(Status.NOT_FOUND).entity("No operator configuration properties found in the db associated to operator id: "+operatorId).build();
			}
		} catch (OperatorException e) {
			LOG.error("OperatorException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getOperatorConfiguration Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting operator configuration").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getOperatorConfiguration: " + response);
		return response;
	} 
    
}
