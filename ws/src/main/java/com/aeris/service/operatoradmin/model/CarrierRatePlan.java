package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class CarrierRatePlan implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;
	private long ratePlanId;
	private int carrierId;
	private int productId;
	private String ratePlanName;
	private Date startDate;
	private Date endDate;
	private String description;
	private double initialCharges;
	private double monthlyFee;
	private double maxKb;

	public long getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(long ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public int getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getRatePlanName() {
		return ratePlanName;
	}

	public void setRatePlanName(String ratePlanName) {
		this.ratePlanName = ratePlanName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getInitialCharges() {
		return initialCharges;
	}

	public void setInitialCharges(double initialCharges) {
		this.initialCharges = initialCharges;
	}

	public double getMonthlyFee() {
		return monthlyFee;
	}

	public void setMonthlyFee(double monthlyFee) {
		this.monthlyFee = monthlyFee;
	}

	public double getMaxKb() {
		return maxKb;
	}

	public void setMaxKb(double maxKb) {
		this.maxKb = maxKb;
	}
}
