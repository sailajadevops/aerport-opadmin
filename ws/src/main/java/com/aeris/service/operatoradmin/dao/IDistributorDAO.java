package com.aeris.service.operatoradmin.dao;

import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.DistributorAccountException;
import com.aeris.service.operatoradmin.exception.DistributorDBException;
import com.aeris.service.operatoradmin.exception.DistributorException;
import com.aeris.service.operatoradmin.exception.DistributorNotFoundException;
import com.aeris.service.operatoradmin.exception.SimpackDBException;
import com.aeris.service.operatoradmin.exception.SIMPackException;
import com.aeris.service.operatoradmin.model.Distributor;
import com.aeris.service.operatoradmin.payload.AssignSIMsFromInventoryRequest;
import com.aeris.service.operatoradmin.payload.AssignSIMsFromSIMPACKRequest;
import com.aeris.service.operatoradmin.payload.CreateDistributorCode;
import com.aeris.service.operatoradmin.payload.CreateDistributorRequest;
import com.aeris.service.operatoradmin.payload.CreateSimpackRequest;
import com.aeris.service.operatoradmin.payload.Simpack;
import com.aeris.service.operatoradmin.payload.UpdateDistributorRequest;
import com.aeris.service.operatoradmin.payload.UpdateEmptySIMPACKRequest;

/**
 * Distributor DAO class for CRUD operations on distributor
 *
 * @author saurabh.sharma@aeris.net
 *
 */
public interface IDistributorDAO {

    List<Distributor> getAllDistributors(long operatorId) throws DistributorDBException, Exception;

    Distributor getDistributor(long operatorId, long distributorId) throws DistributorNotFoundException, DistributorDBException;
    Distributor getDistributorByAerisAccountId(long operatorId, long aerisAccountId) throws DistributorNotFoundException, DistributorDBException;

    Distributor createNewDistributor(long operatorId, CreateDistributorRequest request) throws DistributorException, DistributorDBException;

    Distributor updateDistributor(long operatorId, long distributorId, UpdateDistributorRequest distributor) throws DistributorNotFoundException, DistributorDBException;

    boolean deleteDistributor(long operatorId, long distributorId, Date requestedDate, String requestedUser) throws DistributorNotFoundException, DistributorException, DistributorDBException;

    List<String> getClientEmails(long accountId);

    List<Simpack> createSIMPacksForOperator(long operatorId, CreateSimpackRequest request) throws SIMPackException, DistributorDBException, SimpackDBException;

    boolean assignSIMsToAccount(long operatorId, String accountId, AssignSIMsFromInventoryRequest request) throws AccountNotFoundException, SIMPackException, DistributorAccountException, DistributorDBException;

    boolean assignSIMsFromSIMPACKToAccount(long operatorId, String accountId, AssignSIMsFromSIMPACKRequest request) throws AccountNotFoundException, 
    	DistributorNotFoundException, SIMPackException, DistributorException, DistributorAccountException, DistributorDBException;

    List<String> createEmptySimpacks4Distributor(long operatorId,
            long accountId, CreateDistributorCode request) throws DistributorException, DistributorNotFoundException;

    List<String> updateEmptySimpacksOfDistributor(long operatorId, long distributorId, UpdateEmptySIMPACKRequest request) throws AccountNotFoundException, DistributorException, DistributorDBException;

    List<Simpack> getSimpacks(long operatorId, long distributorId, String flag) throws SimpackDBException;

    Simpack getSimPackByUniqueId(String uniqueId) throws DistributorException;

    boolean addSimpacksToDistributor(long operatorId,
            long distributorId, UpdateEmptySIMPACKRequest request) throws DistributorException, DistributorNotFoundException, DistributorDBException;
    void put(Long distributorId, Distributor d);
    Distributor get(Long distributorId);
}
