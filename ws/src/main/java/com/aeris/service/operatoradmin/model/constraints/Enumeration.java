package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE, FIELD, METHOD, PARAMETER})
@Documented
@Constraint(validatedBy = Enumeration.Validator.class)
public @interface Enumeration {
	String message() default "{com.aeris.service.operatoradmin.model.constraints.Enumeration.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	Class<? extends Enum<?>> value();

	public class Validator implements ConstraintValidator<Enumeration, Object> {
		private Class<? extends Enum<?>>  enumClazz;

		@Override
		public void initialize(final Enumeration enumClazz) {
			this.enumClazz = enumClazz.value();
		}

		public boolean isValid(final Object myval, final ConstraintValidatorContext constraintValidatorContext) {
			if ((myval != null) && (enumClazz != null)) {
				Enum<?>[] enumValues = enumClazz.getEnumConstants();
				Object enumValue = null;
				for (Enum<?> enumerable : enumValues) {
					if (myval.equals(enumerable))
						return true;
					enumValue = getEnumValue(enumerable);
					if ((enumValue != null) && (myval.toString().equalsIgnoreCase(enumValue.toString()))) {
						return true;
					}
				}
			}
			
			return false;
		}

		private Object getEnumValue(Enum<?> enumerable) {
			try {
				for (Method method : enumerable.getClass().getDeclaredMethods()) {
					if (method.getName().equals("getValue")) {
						return method.invoke(enumerable);
					}
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
}
