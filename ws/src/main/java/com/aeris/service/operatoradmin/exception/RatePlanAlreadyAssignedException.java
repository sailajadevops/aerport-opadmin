package com.aeris.service.operatoradmin.exception;

public class RatePlanAlreadyAssignedException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public RatePlanAlreadyAssignedException(String s) {
		super(s);
	}

	public RatePlanAlreadyAssignedException(String ex, Throwable t) {
		super(ex, t);
	}
}
