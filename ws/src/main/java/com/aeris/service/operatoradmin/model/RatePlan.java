package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class RatePlan implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;
	
	private long ratePlanId;
	private String ratePlanLabel;
	private String ratePlanName;
	private RatePlanType ratePlanType;
	private RatePlanPaymentType paymentType;
	private RatePlanAccessType accessType;
	private RatePlanPeriodType periodType;
	private int accessFeeMonths;
	private boolean expireIncluded;
	private int includedPeriodMonths;
	private int devicePoolingPolicyId;
	private int packetDataRoundingPolicyId;
	private int productId;
	private int tierCriteria;
	private int homeZoneId;
	private String description;
	private String specialNotes;
	private int carrierRatePlanId;
	private long accountId;
	private RatePlanStatus status;
	private Date startDate;
	private Date endDate;
	private String currencyName;
	private boolean overrideRoamingIncluded;
	private RatePlanFeeDetails ratePlanFeeDetails;
	private ProvisionTriggerSettings provisionTriggerSettings;
	private SuspendTriggerSettings suspendTriggerSettings;
	private List<ZoneRatePlan> zoneRatePlanSettings;
	private List<RatePlanTier> ratePlanTiers;
	private String createdBy;
	private String lastModifiedBy;
	private int operatorId;
	private int proRateCancellationFee ;
	private int contractTerm ;
	private int cancellationFeeReductionInterval ;
	private int autoCancelAtTermEnd ;
	private int accountForDaysInProvisionState ;
	private int accountForDaysInSuspendState ;
    private boolean roamingIncluded;
    private int zoneSetId ;
	private String technology;
	private boolean waterfallEnabled ;
	private List<RatePlanOverageBucket> ratePlanOverageBucket;
	private long wholesaleRateplanId;
        private boolean isWholesaleRatePlan;
        
	public long getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(long ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getRatePlanLabel() {
		return ratePlanLabel;
	}

	public void setRatePlanLabel(String ratePlanLabel) {
		this.ratePlanLabel = ratePlanLabel;
	}

	public String getRatePlanName() {
		return ratePlanName;
	}

	public void setRatePlanName(String ratePlanName) {
		this.ratePlanName = ratePlanName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	
	public boolean isOverrideRoamingIncluded() {
		return overrideRoamingIncluded;
	}
	
	public void setOverrideRoamingIncluded(boolean overrideRoamingIncluded) {
		this.overrideRoamingIncluded = overrideRoamingIncluded;
	}

	public RatePlanFeeDetails getRatePlanFeeDetails() {
		return ratePlanFeeDetails;
	}

	public void setRatePlanFeeDetails(RatePlanFeeDetails ratePlanFeeDetails) {
		this.ratePlanFeeDetails = ratePlanFeeDetails;
	}

	public ProvisionTriggerSettings getProvisionTriggerSettings() {
		return provisionTriggerSettings;
	}

	public void setProvisionTriggerSettings(ProvisionTriggerSettings provisionTriggerSettings) {
		this.provisionTriggerSettings = provisionTriggerSettings;
	}

	public SuspendTriggerSettings getSuspendTriggerSettings() {
		return suspendTriggerSettings;
	}

	public void setSuspendTriggerSettings(SuspendTriggerSettings suspendTriggerSettings) {
		this.suspendTriggerSettings = suspendTriggerSettings;
	}

	public List<ZoneRatePlan> getZoneRatePlanSettings() {
		return zoneRatePlanSettings;
	}

	public void setZoneRatePlanSettings(List<ZoneRatePlan> zoneRatePlanSettings) {
		this.zoneRatePlanSettings = zoneRatePlanSettings;
	}

	public RatePlanType getRatePlanType() {
		return ratePlanType;
	}

	public void setRatePlanType(RatePlanType ratePlanType) {
		this.ratePlanType = ratePlanType;
	}

	public RatePlanPaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(RatePlanPaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public RatePlanAccessType getAccessType() {
		return accessType;
	}

	public void setAccessType(RatePlanAccessType accessType) {
		this.accessType = accessType;
	}

	public RatePlanPeriodType getPeriodType() {
		return periodType;
	}

	public void setPeriodType(RatePlanPeriodType periodType) {
		this.periodType = periodType;
	}

	public int getAccessFeeMonths() {
		return accessFeeMonths;
	}
	
	public void setAccessFeeMonths(int accessFeeMonths) {
		this.accessFeeMonths = accessFeeMonths;
	}
	
	public boolean isExpireIncluded() {
		return expireIncluded;
	}
	
	public void setExpireIncluded(boolean expireIncluded) {
		this.expireIncluded = expireIncluded;
	}
	
	public int getIncludedPeriodMonths() {
		return includedPeriodMonths;
	}
	
	public void setIncludedPeriodMonths(int includedPeriodMonths) {
		this.includedPeriodMonths = includedPeriodMonths;
	}
	
	public int getDevicePoolingPolicyId() {
		return devicePoolingPolicyId;
	}
	
	public void setDevicePoolingPolicyId(int devicePoolingPolicyId) {
		this.devicePoolingPolicyId = devicePoolingPolicyId;
	}
	
	public int getPacketDataRoundingPolicyId() {
		return packetDataRoundingPolicyId;
	}
	
	public void setPacketDataRoundingPolicyId(int packetDataRoundingPolicyId) {
		this.packetDataRoundingPolicyId = packetDataRoundingPolicyId;
	}
	
	public int getTierCriteria() {
		return tierCriteria;
	}

	public void setTierCriteria(int tierCriteria) {
		this.tierCriteria = tierCriteria;
	}

	public int getHomeZoneId() {
		return homeZoneId;
	}

	public void setHomeZoneId(int homeZoneId) {
		this.homeZoneId = homeZoneId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSpecialNotes() {
		return specialNotes;
	}

	public void setSpecialNotes(String specialNotes) {
		this.specialNotes = specialNotes;
	}

	public int getCarrierRatePlanId() {
		return carrierRatePlanId;
	}

	public void setCarrierRatePlanId(int carrierRatePlanId) {
		this.carrierRatePlanId = carrierRatePlanId;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public RatePlanStatus getStatus() {
		return status;
	}

	public void setStatus(RatePlanStatus status) {
		this.status = status;
	}
	
	public List<RatePlanTier> getRatePlanTiers() {
		return ratePlanTiers;
	}
	
	public void setRatePlanTiers(List<RatePlanTier> ratePlanTiers) {
		this.ratePlanTiers = ratePlanTiers;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public int getOperatorId() {
		return operatorId;
	}
	
	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public int getProRateCancellationFee() {
		return proRateCancellationFee;
	}

	public void setProRateCancellationFee(int proRateCancellationFee) {
		this.proRateCancellationFee = proRateCancellationFee;
	}

	public int getContractTerm() {
		return contractTerm;
	}

	public void setContractTerm(int contractTerm) {
		this.contractTerm = contractTerm;
	}

	public int getCancellationFeeReductionInterval() {
		return cancellationFeeReductionInterval;
	}

	public void setCancellationFeeReductionInterval(
			int cancellationFeeReductionInterval) {
		this.cancellationFeeReductionInterval = cancellationFeeReductionInterval;
	}

	public int getAccountForDaysInProvisionState() {
		return accountForDaysInProvisionState;
	}

	public void setAccountForDaysInProvisionState(int accountForDaysInProvisionState) {
		this.accountForDaysInProvisionState = accountForDaysInProvisionState;
	}

	public int getAccountForDaysInSuspendState() {
		return accountForDaysInSuspendState;
	}

	public void setAccountForDaysInSuspendState(int accountForDaysInSuspendState) {
		this.accountForDaysInSuspendState = accountForDaysInSuspendState;
	}

	public int getAutoCancelAtTermEnd() {
		return autoCancelAtTermEnd;
	}

	public void setAutoCancelAtTermEnd(int autoCancelAtTermEnd) {
		this.autoCancelAtTermEnd = autoCancelAtTermEnd;
	}

	public List<RatePlanOverageBucket> getRatePlanOverageBucket() {
		return ratePlanOverageBucket;
	}

	public void setRatePlanOverageBucket(List<RatePlanOverageBucket> ratePlanOverageBucket) {
		this.ratePlanOverageBucket = ratePlanOverageBucket;
	}

    public boolean isRoamingIncluded() {
        return roamingIncluded;
    }

    public void setRoamingIncluded(boolean roamingIncluded) {
        this.roamingIncluded = roamingIncluded;
    }

	public int getZoneSetId() {
		return zoneSetId;
	}

	public void setZoneSetId(int zoneSetId) {
		this.zoneSetId = zoneSetId;
	}

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

	public boolean isWaterfallEnabled() {
		return waterfallEnabled;
	}

	public void setWaterfallEnabled(boolean waterfallEnabled) {
		this.waterfallEnabled = waterfallEnabled;
	}
	
	public long getWholesaleRateplanId() {
		return wholesaleRateplanId;
	}

	public void setWholesaleRateplanId(long wholesaleRateplanId) {
		this.wholesaleRateplanId = wholesaleRateplanId;
	}
        
        public boolean getIsWholesaleRatePlan(){
            return isWholesaleRatePlan;
        }
        
        public void setIsWholesaleRatePlan(boolean isWholesaleRatePlan){
            this.isWholesaleRatePlan = isWholesaleRatePlan;
        }
}
