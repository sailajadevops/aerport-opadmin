package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class ServiceProfile implements Serializable {
	private static final long serialVersionUID = -4253048262970386258L;
	private long serviceCode;
	private String serviceProfileName;
	private String serviceUserName;
	private String servicePassword;
	private String regexMOVoice;
	private EnrolledServices services;
	private List<ZoneServiceProfile> zoneProfiles;
	private List<TechnologyServiceProfile> technologyProfiles;
	private String accountId;
	private SMPPConfiguration smppConfiguration;
	private boolean smsNonAerisDevice;
	private APNConfiguration apnConfiguration;
	private int productId;
	private int serviceProviderId;
    private String hssProfileId;
    private String pccProfileId;
    private int zoneSetId;
    private boolean notUsed;
    private String apnsLTE;
    private String technology;
    
	public long getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(long serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getServiceProfileName() {
		return serviceProfileName;
	}

	public void setServiceProfileName(String serviceProfileName) {
		this.serviceProfileName = serviceProfileName;
	}

	public String getServiceUserName() {
		return serviceUserName;
	}

	public void setServiceUserName(String serviceUserName) {
		this.serviceUserName = serviceUserName;
	}

	public String getServicePassword() {
		return servicePassword;
	}

	public void setServicePassword(String servicePassword) {
		this.servicePassword = servicePassword;
	}

	public String getRegexMOVoice() {
		return regexMOVoice;
	}

	public void setRegexMOVoice(String regexMOVoice) {
		this.regexMOVoice = regexMOVoice;
	}

	public EnrolledServices getServices() {
		return services;
	}

	public void setServices(EnrolledServices services) {
		this.services = services;
	}

	public List<ZoneServiceProfile> getZoneProfiles() {
		return zoneProfiles;
	}

	public void setZoneProfiles(List<ZoneServiceProfile> zoneProfiles) {
		this.zoneProfiles = zoneProfiles;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public APNConfiguration getApnConfiguration() {
		return apnConfiguration;
	}

	public void setApnConfiguration(APNConfiguration apnConfiguration) {
		this.apnConfiguration = apnConfiguration;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	public SMPPConfiguration getSmppConfiguration() {
		return smppConfiguration;
	}

	public void setSmppConfiguration(SMPPConfiguration smppConfiguration) {
		this.smppConfiguration = smppConfiguration;
	}
	
	public int getServiceProviderId() {
		return serviceProviderId;
	}
	
	public void setServiceProviderId(int serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}
	
	public boolean isSmsNonAerisDevice() {
		return smsNonAerisDevice;
	}
	
	public void setSmsNonAerisDevice(boolean smsNonAerisDevice) {
		this.smsNonAerisDevice = smsNonAerisDevice;
	}

    public String getHssProfileId() {
        return hssProfileId;
    }

    public void setHssProfileId(String hssProfileId) {
        this.hssProfileId = hssProfileId;
    }

    public String getPccProfileId() {
        return pccProfileId;
    }

    public void setPccProfileId(String pccProfileId) {
        this.pccProfileId = pccProfileId;
    }

	public int getZoneSetId() {
		return zoneSetId;
	}

	public void setZoneSetId(int zoneSetId) {
		this.zoneSetId = zoneSetId;
	}

    public boolean isNotUsed() {
        return notUsed;
    }

    public void setNotUsed(boolean notUsed) {
        this.notUsed = notUsed;
    }

    public String getApnsLTE() {
        return apnsLTE;
    }

    public void setApnsLTE(String apnsLTE) {
        this.apnsLTE = apnsLTE;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

	public List<TechnologyServiceProfile> getTechnologyProfiles() {
		return technologyProfiles;
	}

	public void setTechnologyProfiles(
			List<TechnologyServiceProfile> technologyProfiles) {
		this.technologyProfiles = technologyProfiles;
	}    
}
