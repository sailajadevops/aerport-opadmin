package com.aeris.service.operatoradmin.resources;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.dao.ICommunicationPlanDAO;
import com.aeris.service.operatoradmin.dao.IServiceProfileDAO;
import com.aeris.service.operatoradmin.exception.DuplicateServiceProfileException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.ServiceProfileException;
import com.aeris.service.operatoradmin.exception.ServiceProfileNotFoundException;
import com.aeris.service.operatoradmin.exception.ServiceProfileValidationException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.APNConfiguration;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.SMPPConfiguration;
import com.aeris.service.operatoradmin.model.ServiceProfile;
import com.aeris.service.operatoradmin.model.TechnologyServiceProfile;
import com.aeris.service.operatoradmin.model.ZoneServiceProfile;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateApiKeyRequest;
import com.aeris.service.operatoradmin.payload.CreateServiceProfileRequest;
import com.aeris.service.operatoradmin.payload.UpdateServiceProfileRequest;
import com.aeris.service.operatoradmin.utils.Constants;

/**
 * Rest Resource for Service Profile management per account. 
 * Includes methods for
 * 
 * <ul>
 * <li>
 * Create a new Service Profile
 * </li>
 * <li>
 * Edit an existing service profile
 * </li>
 * <li>
 * Fetch the service profiles per account
 * </li>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}/accounts/{accountId}/serviceprofiles")
@Singleton
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class ServiceProfileResource {
	private static final Logger LOG = LoggerFactory.getLogger(ServiceProfileResource.class);

	@Inject
	private IServiceProfileDAO serviceProfileDAO;
	
	@Inject
	private ICommunicationPlanDAO communicationPlanDAO;
    
    @Inject
    private IProductEnum productEnum;

	@GET
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
	public Response getAllServiceProfiles(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@QueryParam("extendAttributes") final boolean extendAttributes) {
		LOG.info("Received request to fetch all service profiles for account id: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		List<ServiceProfile> serviceProfiles = null;

		try {
			// Fetch from database
			serviceProfiles = serviceProfileDAO.getAllServiceProfiles(operatorId, accountId, extendAttributes);

			if (serviceProfiles != null) {
				LOG.info("fetched service profiles successfully, no of service profiles fetched: " + serviceProfiles.size());

				ObjectMapper mapper = new ObjectMapper();
				String serviceProfilesJson = mapper.writeValueAsString(serviceProfiles);

				builder.entity(serviceProfilesJson);
			} else {
				LOG.info("No service profiles found for the account : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No service profiles found for the account").build();
			}
		} catch (Exception e) {
			LOG.error("getAllServiceProfiles Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting service profiles").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllServiceProfiles: " + response);
		return response;
	}

	@GET
	@Path("/{serviceCode}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
	public Response getServiceProfile(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("serviceCode") @Pattern(regexp = "[0-9]+", message = "{serviceCode.number}") final String serviceCode,
			@QueryParam("extendAttributes") final boolean extendAttributes) {
		LOG.info("Received request to fetch service profile for service profile id: " + serviceCode);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		ServiceProfile serviceProfile = null;

		try {
			// Fetch from database
			serviceProfile = serviceProfileDAO.getServiceProfile(operatorId, accountId, Long.parseLong(serviceCode), extendAttributes);

			if (serviceProfile != null) {
				LOG.info("fetched service profile successfully for serviceCode: " + serviceCode);

				ObjectMapper mapper = new ObjectMapper();
				String serviceProfileJson = mapper.writeValueAsString(serviceProfile);

				builder.entity(serviceProfileJson);
			} else {
				LOG.info("No service profile found for the serviceCode : " + serviceCode);
				return Response.status(Status.NOT_FOUND).entity("No service profile found for the serviceCode").build();
			}
		} catch (Exception e) {
			LOG.error("getServiceProfile Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting service profile").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllServiceProfiles: " + response);
		return response;
	}

	/**
	 * Create a new api key for accountId with the required permissions
	 * specified in the {@link CreateApiKeyRequest}
	 * 
	 * @param request
	 *            the request containing the account id and the required
	 *            permissions for the new api key
	 * @return the new api key created for the account id
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response createNewServiceProfile(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@Valid CreateServiceProfileRequest request) {

		LOG.info("Received request to create new service profile under account: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String serviceProfileName = request.getServiceProfileName();
		String servicePassword = request.getServicePassword();
		String serviceUserName = request.getServiceUserName();
		String regexMOVoice = request.getRegexMOVoice();
		List<ZoneServiceProfile> zoneProfiles = request.getZoneProfiles();
		SMPPConfiguration smppConfiguration = request.getSmppConfiguration();
		boolean smsNonAerisDevice = request.isSmsNonAerisDevice();
		APNConfiguration apnConfiguration = request.getApnConfiguration();
		int productId = Integer.parseInt(request.getProductId());
		EnrolledServices enrolledServices = request.getServices();
		
		int serviceProviderId = 0;
		
		if(NumberUtils.isNumber(request.getServiceProviderId()))
		{
			serviceProviderId = Integer.parseInt(request.getServiceProviderId());
		}
		
		String userId = request.getUserId();
		Date requestedDate = new Date();
        
        String hssProfileId = request.getHssProfileId();
        String pccProfileId = request.getPccProfileId();
        
        // TODO To implement expire service profile feature and make use of this flag
        boolean notUsed = true; // request.isNotUsed();
        
        String technology = request.getTechnology();
        
        /*if (technology == null) {
            technology = getProductTechnology(productId);
        }*/
        boolean validateCommPLan = request.isValidateCommPlan();
		try {
			List<TechnologyServiceProfile> technologyProfiles = request.getTechnologyProfiles();
			
			LOG.info("Calling ServiceProfifeDAO.createNewServiceProfile()");

			ServiceProfile serviceProfile = serviceProfileDAO.createNewServiceProfile(operatorId, accountId, serviceProfileName, serviceUserName,
					servicePassword, productId, serviceProviderId, zoneProfiles, technologyProfiles, regexMOVoice, enrolledServices, smppConfiguration, smsNonAerisDevice, 
					apnConfiguration, hssProfileId, pccProfileId, request.getZoneSetId(), notUsed, technology, requestedDate, userId, validateCommPLan);

			ObjectMapper mapper = new ObjectMapper();
			String serviceProfileJson = mapper.writeValueAsString(serviceProfile);
			builder.entity(serviceProfileJson);

			LOG.info("createNewServiceProfile() Response: " + serviceProfileJson);
		} catch (DuplicateServiceProfileException e) {
			LOG.error("DuplicateServiceProfileException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ServiceProfileValidationException e) {
			LOG.error("ServiceProfileValidationException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (DuplicateZoneException e) {
			LOG.error("DuplicateZoneException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ServiceProfileException e) {
			LOG.error("ServiceProfileException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("Fatal Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while creating service profile").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewServiceProfile: " + response);
		return response;
	}

	/**
	 * Create a new api key for accountId with the required permissions
	 * specified in the {@link CreateApiKeyRequest}
	 * 
	 * @param request
	 *            the request containing the account id and the required
	 *            permissions for the new api key
	 * @return the new api key created for the account id
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{serviceCode}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateServiceProfile(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("serviceCode") @Pattern(regexp = "[0-9]+", message = "{serviceCode.number}") final String serviceCode,
			@Valid UpdateServiceProfileRequest request) {

		LOG.info("Received request to update service profile with service profile id: " + serviceCode + " under account: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String serviceProfileName = request.getServiceProfileName();
		String servicePassword = request.getServicePassword();
		String serviceUserName = request.getServiceUserName();
		String regexMOVoice = request.getRegexMOVoice();
		List<ZoneServiceProfile> zoneProfiles = request.getZoneProfiles();
		SMPPConfiguration smppConfiguration = request.getSmppConfiguration();
		boolean smsNonAerisDevice = request.isSmsNonAerisDevice();
		APNConfiguration apnConfiguration = request.getApnConfiguration();
		int productId = Integer.parseInt(request.getProductId());
		EnrolledServices enrolledServices = request.getServices();

		int serviceProviderId = 0;
		
		if(NumberUtils.isNumber(request.getServiceProviderId()))
		{
			serviceProviderId = Integer.parseInt(request.getServiceProviderId());
		}
		
		String userId = request.getUserId();
		Date requestedDate = new Date();
        
        String hssProfileId = request.getHssProfileId();
        String pccProfileId = request.getPccProfileId();
        
        // TODO To implement expire service profile feature and make use of this flag
        boolean notUsed = true; // request.isNotUsed();
        
        String technology = request.getTechnology();
        if (technology == null) {
            technology = getProductTechnology(productId);
        }
        
		try {
			LOG.info("Calling AccountDAO.updateServiceProfile()");

			ServiceProfile serviceProfile = serviceProfileDAO.updateServiceProfile(operatorId, accountId, Long.parseLong(serviceCode), serviceProfileName,
					serviceUserName, servicePassword, productId, serviceProviderId, zoneProfiles, request.getTechnologyProfiles(), regexMOVoice, enrolledServices, smppConfiguration,
					smsNonAerisDevice, apnConfiguration, hssProfileId, pccProfileId, request.getZoneSetId(), notUsed, technology, requestedDate, userId, request.isValidateCommPlan());

			ObjectMapper mapper = new ObjectMapper();

			String serviceProfileJson = mapper.writeValueAsString(serviceProfile);
			builder.entity(serviceProfileJson);

			LOG.info("updateServiceProfile() Response: " + serviceProfileJson);
		} catch (ServiceProfileNotFoundException e) {
			LOG.error("ServiceProfileNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (DuplicateServiceProfileException e) {
			LOG.error("DuplicateServiceProfileException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (DuplicateZoneException e) {
			LOG.error("DuplicateZoneException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ServiceProfileValidationException e) {
			LOG.error("ServiceProfileValidationException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ServiceProfileException e) {
			LOG.error("ServiceProfileException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("Fatal Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while updating service profile").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateServiceProfile: " + response);
		return response;
	}

	@DELETE
	@Path("/{serviceCode}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response deleteServiceProfile(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("serviceCode") @Pattern(regexp = "[0-9]+", message = "{serviceCode.number}") final String serviceCode,
			@QueryParam("userId") @Email(message = "{userId.email}") final String userId) {
		LOG.info("Received request to delete service profile with service profile id: " + serviceCode);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Date requestedDate = new Date();

		try {
			// Fetch from database
			boolean success = serviceProfileDAO.deleteServiceProfile(operatorId, accountId, Long.parseLong(serviceCode), requestedDate, userId);

			if (!success) {
				LOG.info("deleteServiceProfile failed for serviceProfileId: " + serviceCode);
				return Response.status(Status.NOT_FOUND).entity("deleteServiceProfile failed for serviceProfileId: " + serviceCode).build();
			} else {
				LOG.info("Service Profile with service profile id: " + serviceCode + " deleted successfully");

				ObjectMapper mapper = new ObjectMapper();
				String deleteJson = mapper.writeValueAsString(success);

				builder.entity(deleteJson);
			}
		} catch (ServiceProfileNotFoundException e) {
			LOG.error("ServiceProfileNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (ServiceProfileValidationException e) {
			LOG.error("ServiceProfileValidationException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ServiceProfileException e) {
			LOG.error("ServiceProfileException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("Fatal Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while deleting service profile").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from deleteServiceProfile: " + response);
		return response;
	}
	
	@GET
	@Path("/carrierId")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
	public Response getCarrierId(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@QueryParam("productId")  @Pattern(regexp = "[0-9]+", message = "{productId.number}") final String productId) {
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		String carrierId;
		
		try {
			carrierId = serviceProfileDAO.getCarrierId(accountId, productId);
			
			ObjectMapper mapper = new ObjectMapper();
			String carrierIdJson = mapper.writeValueAsString(carrierId);
			builder.entity(carrierIdJson);
			
		} catch (Exception e) {
			LOG.error("Fatal Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting CarrierId ").build();
		}
		
		Response response = builder.build();

		LOG.debug("Sent response from getCarrierId: " + response);
		return response;
	}
    
    private String getProductTechnology(int productId) {
        Product product = productEnum.getProductById(productId);
        return product.getTechnology();
    }	
}
