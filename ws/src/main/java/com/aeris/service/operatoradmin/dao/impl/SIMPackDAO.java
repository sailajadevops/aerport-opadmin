package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.ISIMPackDAO;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventDispatcher;
import com.aeris.service.operatoradmin.exception.DistributorDBException;
import com.aeris.service.operatoradmin.exception.DistributorException;
import com.aeris.service.operatoradmin.exception.DistributorNotFoundException;
import com.aeris.service.operatoradmin.exception.InvalidSIMPackException;
import com.aeris.service.operatoradmin.exception.SIMPackException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.SIMPack;
import com.aeris.service.operatoradmin.payload.CreateNeoSIMPacksRequest;
import com.aeris.service.operatoradmin.payload.CreateSIMPacksRequest;
import com.aeris.service.operatoradmin.payload.CreateUniqueCodesRequest;
import com.aeris.service.operatoradmin.payload.NotificationRequest;
import com.aeris.service.operatoradmin.payload.UpdateSIMPacksRequest;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.google.common.collect.Lists;

public class SIMPackDAO implements ISIMPackDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SIMPackDAO.class);

    @Inject
    private IAccountDAO accountDAO;

    @Inject
    private IEventDispatcher eventDispatcher;

    private List<String> getUniqueCodes(int count) throws SIMPackException {
        List<String> uniqueCodes = new ArrayList<String>();
        if (count == 0) {
            return uniqueCodes;
        }
        while (uniqueCodes.size() != count) {
            String randomDigit = RandomStringUtils.randomNumeric(5);
            String uniqueCode = randomDigit + OAUtils.generateDigit(randomDigit);
            Connection conn = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
                ps = conn.prepareStatement("SELECT count(*) FROM sim_inventory.SIMPACK WHERE unique_id = ? ");
                ps.setString(1, uniqueCode);
                rs = ps.executeQuery();
                if (rs.next()) {
                    if (rs.getInt(1) == 0) {
                        uniqueCodes.add(uniqueCode);
                    }
                }
            } catch (SQLException e) {
                LOGGER.error("SQLException occurred while validating generated unique code: ", e);
                throw new SIMPackException("SQLException occurred while validating generated unique code: ", e);
            } finally {
                DBConnectionManager.getInstance().cleanup(conn, ps, rs);
            }

        }
        return uniqueCodes;
    }

    private final static String CREATE_EMPTY_SIMPACK_QUERY = "INSERT INTO sim_inventory.SIMPACK (REP_TIMESTAMP, SEQ_NO, UNIQUE_ID, USED, DISTRIBUTOR_ACCOUNT_ID, "
            + "CREATED_BY, " // 4
            + "CREATED_DATE, " // inline
            + "LAST_MODIFIED_BY, " // 5
            + "LAST_MODIFIED_DATE) " // inline
            + "VALUES (sys_extract_utc(systimestamp), sim_inventory.SIM_PACK_SEQ.nextval, ?, ?, ?, ?, sysdate, ?, sysdate)";
    
    
    private final static String DELETE_SIMPACK_QUERY = "delete from sim_inventory.SIMPACK where distributor_account_id = ? and unique_id = ? ";

    @Override
    public List<String> createUniqueCodes(CreateUniqueCodesRequest request) throws DistributorNotFoundException, SIMPackException, DistributorDBException {
        LOGGER.info("In SIMPackDAO.java createUniqueCodes()");
        List<String> simPackUniqueCodes = new ArrayList<String>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        Account account = accountDAO.getAccount(String.valueOf(1), String.valueOf(request.getDistributorAccountId()));
        if (account == null) {
            throw new DistributorNotFoundException("Distributor with account id " + request.getDistributorAccountId() + " does not exists.");
        }
        try {
            conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            setAutoCommit(conn, false);
            List<String> uniqueCodes = getUniqueCodes(request.getNumberOfUniqueCodes());
            preparedStatement = conn.prepareStatement(CREATE_EMPTY_SIMPACK_QUERY);
            for (String uniqueCode : uniqueCodes) {
                preparedStatement.setString(1, uniqueCode);
                preparedStatement.setString(2, "1"); // 1 for new code.
                preparedStatement.setLong(3, request.getDistributorAccountId());
                preparedStatement.setString(4, request.getUserEmail());
                preparedStatement.setString(5, request.getUserEmail());
                preparedStatement.addBatch();
                simPackUniqueCodes.add(uniqueCode);
            }
            preparedStatement.executeBatch();
            conn.commit();
            NotificationRequest notificationRequest = new NotificationRequest();
            notificationRequest.setUserId(request.getUserEmail());
            notificationRequest.setAccountId(Long.toString(account.getAccountId()));
            notificationRequest.setOperatorId("1");
            notificationRequest.setEntityID("");
            notificationRequest.setToEmailRecipients(request.getEmailRecipients());
            notificationRequest.setEvent(Event.UNIQUE_CODES_CREATED);
            notificationRequest.setAdditionalEmailBodyContents(StringUtils.join(uniqueCodes, "<br/>"));
            eventDispatcher.propogateEvent(Event.UNIQUE_CODES_CREATED, notificationRequest.getOperatorId(), notificationRequest);
            if (notificationRequest.getStatus() != 0) {
                LOGGER.error("Error occurred while sending email containing generated unique codes.");
            }
        } catch (SIMPackException e) {
            throw e;
        } catch (SQLException e) {
            LOGGER.error("Error occurred while creating unique codes.", e);
            throw new SIMPackException("Error occurred while creating unique codes.", e);
        } finally {
            setAutoCommit(conn, true);
            DBUtils.cleanup(conn, preparedStatement, null);
        }
        LOGGER.info("Returning generated unique codes: " + StringUtils.join(simPackUniqueCodes, ","));
        return simPackUniqueCodes;
    }

    private final static String SQL_INSERT_SIMPACK_QUERY = "INSERT INTO sim_inventory.SIMPACK "
            + "("
            + "REP_TIMESTAMP, " // inline 
            + "SEQ_NO, " // inline
            + "UNIQUE_ID, " // 1
            + "USED, " // 2
            + "ICCID_START, " // 3
            + "ICCID_END, " // 4
            + "CREATED_BY, " // 5
            + "CREATED_DATE, " // inline
            + "LAST_MODIFIED_BY, " // 6
            + "LAST_MODIFIED_DATE, " // inline
            + "DISTRIBUTOR_ACCOUNT_ID, " // 7
            + "PACKET_SIZE, " // 8
            + "SIM_TYPE " // 9
            + ") "
            + "VALUES( "
            + "sys_extract_utc(systimestamp), " // inline
            + "sim_inventory.SIM_PACK_SEQ.nextval, " // inline
            + "?, " // 1
            + "?, " // 2
            + "?, " // 3
            + "?, " // 4
            + "?, " // 5
            + "sysgmtdate, " // inline
            + "?, " // 6
            + "sysgmtdate, " // inline
            + "?, " // 7
            + "?, " // 8
            + "? " // 9
            + ")";

    @Override
    public List<SIMPack> createSIMPacks(CreateSIMPacksRequest request) throws SIMPackException, DistributorDBException, DistributorException {
        List<SIMPack> simPacks = new ArrayList<SIMPack>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        Date requestedDate = new Date();
        try {
            conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            setAutoCommit(conn, false);
            List<String> uniqueIds = getUniqueCodes(request.getSimPacks().size());
            preparedStatement = conn.prepareStatement(SQL_INSERT_SIMPACK_QUERY);
            int counter = 0;
            for (String uniqueId : uniqueIds) {
                checkIfSIMsAssignedAlready(prepareICCIDList(request.getSimPacks().get(counter).getIccidStart(), request.getSimPacks().get(counter).getIccidEnd(), null));
                preparedStatement.setString(1, uniqueId);
                preparedStatement.setString(2, "1"); // 1 as ICCID are mapped.
                preparedStatement.setString(3, request.getSimPacks().get(counter).getIccidStart());
                preparedStatement.setString(4, request.getSimPacks().get(counter).getIccidEnd());
                preparedStatement.setString(5, request.getUserEmail());
                preparedStatement.setString(6, request.getUserEmail());
                preparedStatement.setLong(7, request.getSimPacks().get(counter).getDistributorAccountId());
                preparedStatement.setInt(8, request.getSimPacks().get(counter).getPacketSize());
                preparedStatement.setString(9, request.getSimPacks().get(counter).getSimType());
                preparedStatement.addBatch();
                request.getSimPacks().get(counter).setUniqueCode(uniqueId);
                request.getSimPacks().get(counter).setUsed(true);
                request.getSimPacks().get(counter).setCreatedBy(request.getUserEmail());
                request.getSimPacks().get(counter).setLastModifiedBy(request.getUserEmail());
                request.getSimPacks().get(counter).setCreatedDate(new java.sql.Date(requestedDate.getTime()));
                request.getSimPacks().get(counter).setLastModifiedDate(new java.sql.Date(requestedDate.getTime()));
                simPacks.add(request.getSimPacks().get(counter));
                counter++;
            }
            preparedStatement.executeBatch();
            conn.commit();
        } catch (SIMPackException e) {
            throw e;
        } catch (SQLException e) {
            LOGGER.error("SQLException occurred while creating SIM packs.", e);
            throw new DistributorException("Error occurred while creating SIM packs.", e);
        } finally {
            setAutoCommit(conn, true);
            DBUtils.cleanup(conn, preparedStatement, null);
        }
        return simPacks;
    }

    private static final String GET_SIMS_QUERY = "select iccid from sim_inventory.cingular_sim_inventory where substr(iccid, 1, 19) between ? and ? and assign_date is null";

    private List<String> getSIMs(String iccidStart, String iccidEnd) throws SIMPackException {
        List<String> sims = new ArrayList<String>();
        LOGGER.info("Getting ICCIDs for range {} to {}", iccidStart, iccidEnd);
        if (iccidStart == null || iccidEnd == null) {
            LOGGER.info("ICCID range values are null.");
            return sims;
        }
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            ps = con.prepareStatement(GET_SIMS_QUERY);
            ps.setString(1, iccidStart.substring(0, 19));
            ps.setString(2, iccidEnd.substring(0, 19));
            rs = ps.executeQuery();
            while (rs.next()) {
                sims.add(rs.getString("iccid"));
            }
            if (sims.isEmpty()) {
                throw new SIMPackException("No ICCIDs are present as per range " + iccidStart + " - " + iccidEnd, null);
            }
            LOGGER.info("ICCIDs for range {} - {} are : {}", iccidStart, iccidEnd, StringUtils.join(sims, ","));
            return sims;
        } catch (SQLException e) {
            LOGGER.error("Failed to fetch SIMs for range {} - {}", iccidStart, iccidEnd, e);
        } finally {
            DBUtils.cleanup(con, ps, rs);
        }
        LOGGER.info("ICCIDs for range {} - {} are : {}", iccidStart, iccidEnd, StringUtils.join(sims, ","));
        return sims;
    }

    private List<String> prepareICCIDList(String iccidStartStr, String iccidEndStr, String iccidsAdditional) throws SIMPackException {
        List<String> iccidList = getSIMs(iccidStartStr, iccidEndStr);
        if (!StringUtils.isEmpty(iccidsAdditional)) {
            iccidList.addAll(Arrays.asList(StringUtils.stripAll(iccidsAdditional.split(","), " ")));
        }
        return iccidList;
    }

    private final String CHECK_IF_SIMS_ASSIGNED_QUERY = "select count(*) from sim_inventory.cingular_sim_assigned where iccid in (ICCID_LIST)";

    private void checkIfSIMsAssignedAlready(List<String> iccidList) throws SIMPackException {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<List<String>> partitionList = Lists.partition(iccidList, 1000);
        for (List<String> list : partitionList) {
            String iccids = OAUtils.getOracleINString(list);
            String query = CHECK_IF_SIMS_ASSIGNED_QUERY.replaceAll("ICCID_LIST", iccids);
            try {
                DBConnectionManager connectionManager = DBConnectionManager.getInstance();
                conn = connectionManager.getBillingDatabaseConnection();
                ps = conn.prepareStatement(query);
                rs = ps.executeQuery();
                if (rs.next()) {
                    if (rs.getInt(1) > 0) {
                        LOGGER.error("Some of the ICCIDs are already assigned to other account. Please choose another one.");
                        throw new SIMPackException("Some of the ICCIDs are already assigned to other account. Please choose another one.");
                    }
                }
            } catch (SQLException e) {
                LOGGER.error("SQLException occurred while checking if ICCIDs are already assigned.", e);
                throw new SIMPackException("Exception occurred while checking if ICCIDs are already assigned.", e);
            } finally {
                DBUtils.cleanup(conn, ps, rs);
            }
        }
    }

    private final static String SQL_UPDATE_SIMPACK_QUERY = "UPDATE sim_inventory.SIMPACK set "
            + "USED = ?, "
            + "ICCID_START = ?, "
            + "ICCID_END = ?, "
            + "ICCID_ADDITIONAL = ?, "
            + "LAST_MODIFIED_BY = ?, "
            + "LAST_MODIFIED_DATE = sysgmtdate "
            + "WHERE UNIQUE_ID = ? and DISTRIBUTOR_ACCOUNT_ID = ?";

    @Override
    public boolean updateSIMPacks(UpdateSIMPacksRequest request) throws SIMPackException, DistributorDBException {
        LOGGER.info("In SIMPackDAO.java updateSIMPacks()");
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            setAutoCommit(conn, false);
            preparedStatement = conn.prepareStatement(SQL_UPDATE_SIMPACK_QUERY);
            for (SIMPack simPack : request.getSimPacks()) {
                if (getSIMPack(simPack.getUniqueCode()) == null) {
                    LOGGER.error("SIM Pack with unique code " + simPack.getUniqueCode() + " does not exists.");
                    throw new SIMPackException("SIM Pack with unique code " + simPack.getUniqueCode() + " does not exists.");
                }
                List<String> iccidList = prepareICCIDList(simPack.getIccidStart(), simPack.getIccidEnd(), simPack.getIccidAdditional());
                if (iccidList.isEmpty()) {
                    LOGGER.info("ICCID list is empty.");
                    return false;
                }
                checkIfSIMsAssignedAlready(iccidList);
                preparedStatement.setString(1, "1");
                preparedStatement.setString(2, simPack.getIccidStart());
                preparedStatement.setString(3, simPack.getIccidEnd());
                preparedStatement.setString(4, simPack.getIccidAdditional());
                preparedStatement.setString(5, request.getUserEmail());
                preparedStatement.setString(6, simPack.getUniqueCode());
                preparedStatement.setLong(7, simPack.getDistributorAccountId());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            conn.commit();
            return true;
        } catch (SQLException ex) {
            LOGGER.error("SQLException occurred while updating SIM packs.", ex);
            throw new SIMPackException("Exception occurred while occurred while updating SIM packs.", ex);
        } finally {
            setAutoCommit(conn, false);
            DBUtils.cleanup(conn, preparedStatement, null);
        }
    }
    
    @Override
    public void updateSIMPacksFromCSV(String operatorId, String distributorId, List<String[]> rows, String email) throws SIMPackException, DistributorDBException {
    	UpdateSIMPacksRequest request = new UpdateSIMPacksRequest();
    	List<SIMPack> simpacks = new ArrayList<SIMPack>();
    	for (int i = 5; i < rows.size(); i++) {
    		String[] row = rows.get(i);
			if(row == null || row.length < 7) {
				continue ;
			}
			SIMPack simpack = new SIMPack();
			simpack.setCreatedBy(email);
			simpack.setDistributorAccountId(Long.valueOf(row[0]));
			simpack.setIccidAdditional(row[1]);
			String iccidStart = row[2] ;
			String iccidEnd = row[3] ;
			if(!StringUtils.isEmpty(iccidStart)) {
				if(iccidStart.indexOf("'") != -1) {
					iccidStart = StringUtils.remove(iccidStart, "'");
				}
			}
			if(!StringUtils.isEmpty(iccidEnd)) {
				if(iccidEnd.indexOf("'") != -1) {
					iccidEnd = StringUtils.remove(iccidEnd, "'");
				}
			}
			simpack.setIccidStart(iccidStart);
			simpack.setIccidEnd(iccidEnd);
			simpack.setLastModifiedBy(email);
			simpack.setSimType(row[5]);
			simpack.setUniqueCode(row[6]);
			simpacks.add(simpack);
		}
    	request.setSimPacks(simpacks);
    	updateSIMPacks(request);
    }
   
    private static final String GET_SIM_PACKS_BY_DISTRIBUTOR = "SELECT * FROM sim_inventory.simpack WHERE DISTRIBUTOR_ACCOUNT_ID = ?";
    private static final String GET_ALL_SIM_PACKS = "SELECT * FROM sim_inventory.simpack";

    @Override
    public List<SIMPack> getAllSIMPacks(String distributorAccountId) throws SIMPackException {
        List<SIMPack> simPacks = new ArrayList<SIMPack>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            SIMPack simPack;
            conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            if (distributorAccountId == null) {
                ps = conn.prepareStatement(GET_ALL_SIM_PACKS);
            } else {
                ps = conn.prepareStatement(GET_SIM_PACKS_BY_DISTRIBUTOR);
                ps.setLong(1, Long.parseLong(distributorAccountId));
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                simPack = new SIMPack();
                simPack.setUniqueCode(rs.getString("UNIQUE_ID"));
                simPack.setCreatedBy(rs.getString("CREATED_BY"));
                simPack.setCreatedDate(rs.getDate("CREATED_DATE"));
                simPack.setCustomerAccountId(rs.getLong("CUSTOMER_ACCOUNT_ID"));
                simPack.setDistributorAccountId(rs.getLong("DISTRIBUTOR_ACCOUNT_ID"));
                simPack.setIccidAdditional(rs.getString("ICCID_ADDITIONAL"));
                simPack.setIccidEnd(rs.getString("ICCID_END"));
                simPack.setIccidStart(rs.getString("ICCID_START"));
                simPack.setLastModifiedBy(rs.getString("LAST_MODIFIED_BY"));
                simPack.setLastModifiedDate(rs.getDate("LAST_MODIFIED_DATE"));
                simPack.setPacketSize(rs.getInt("PACKET_SIZE"));
                simPack.setSimType(rs.getString("SIM_TYPE"));
                simPack.setUsed((rs.getInt("USED") != 0));
                simPacks.add(simPack);
            }
        } catch (SQLException e) {
            LOGGER.error("SQLException occurred while getting SIM packs: ", e);
            throw new SIMPackException("SQLException occurred while getting SIM packs: ", e);
        } finally {
            DBConnectionManager.getInstance().cleanup(conn, ps, rs);
        }
        return simPacks;
    }

    private static final String GET_SIM_PACK = "SELECT * FROM sim_inventory.simpack WHERE unique_id = ?";
    
    @Override
    public SIMPack getSIMPack(String simPackId) throws SIMPackException {
        SIMPack simPack = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            ps = conn.prepareStatement(GET_SIM_PACK);
            ps.setString(1, simPackId);
            rs = ps.executeQuery();
            if (rs.next()) {
                simPack = new SIMPack();
                simPack.setUniqueCode(rs.getString("UNIQUE_ID"));
                simPack.setCreatedBy(rs.getString("CREATED_BY"));
                simPack.setCreatedDate(rs.getDate("CREATED_DATE"));
                simPack.setCustomerAccountId(rs.getLong("CUSTOMER_ACCOUNT_ID"));
                simPack.setDistributorAccountId(rs.getLong("DISTRIBUTOR_ACCOUNT_ID"));
                simPack.setIccidAdditional(rs.getString("ICCID_ADDITIONAL"));
                simPack.setIccidEnd(rs.getString("ICCID_END"));
                simPack.setIccidStart(rs.getString("ICCID_START"));
                simPack.setLastModifiedBy(rs.getString("LAST_MODIFIED_BY"));
                simPack.setLastModifiedDate(rs.getDate("LAST_MODIFIED_DATE"));
                simPack.setPacketSize(rs.getInt("PACKET_SIZE"));
                simPack.setSimType(rs.getString("SIM_TYPE"));
                simPack.setUsed((rs.getInt("USED") != 0));
            }
            return simPack;
        } catch (SQLException e) {
            LOGGER.error("SQLException occurred while getting SIM pack: " + simPackId, e);
            throw new SIMPackException("SQLException occurred while getting SIM pack: " + simPackId, e);
        } finally {
            DBConnectionManager.getInstance().cleanup(conn, ps, rs);
        }
    }

    @Override
    public SIMPack updateSIMPack(UpdateSIMPacksRequest request) {
        SIMPack simPack = null;

        return simPack;
    }

    @Override
    public boolean deleteSIMPack(long distributorAccountId, long simPackId) throws SIMPackException, InvalidSIMPackException {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getBillingDatabaseConnection();
			ps = conn.prepareStatement(DELETE_SIMPACK_QUERY);
			ps.setLong(1, distributorAccountId);
			ps.setLong(2, simPackId);
			int count = ps.executeUpdate();
			if(count != 1) {
				conn.rollback();
				throw new InvalidSIMPackException("Simpack for distributorAccountId not found ");
			}
			conn.commit();
			return true ;
		} catch(InvalidSIMPackException e) {
			 LOGGER.error(e.getMessage() + simPackId, e);
	         throw e;
		}catch(Exception e) {
			 LOGGER.error("SQLException occurred while deleting SIM pack: " + simPackId, e);
	         throw new SIMPackException("SQLException occurred while deleting SIM pack: " + simPackId, e);
		} finally {
			DBUtils.cleanup(conn, ps);
		}
    }

    private void setAutoCommit(Connection conn, boolean autoCommit) throws DistributorDBException {
        if (conn == null) {
            return;
        }
        try {
            conn.setAutoCommit(autoCommit);
        } catch (SQLException e) {
            throw new DistributorDBException("Exception occurred while setting autoCommit value for connection. DB error", e);
        }
    }

    @Override
    public List<SIMPack> createNeoSIMPacks(CreateNeoSIMPacksRequest request) throws SIMPackException, DistributorDBException {
        List<SIMPack> simPacks = new ArrayList<SIMPack>();
        SIMPack simPack;
        // Input data
        int numberOfSIMPacks = request.getNumberOfSIMPacks();
        int packSize = request.getPackSize();
        String iccidStartWithoutLastDigitStr = request.getIccidStart().substring(0, (request.getIccidStart().length() - 1));
        String iccidEndWithoutLastDigitStr = request.getIccidEnd().substring(0, (request.getIccidEnd().length() - 1));
        Long iccidStartWithoutLastDigitL = Long.parseLong(iccidStartWithoutLastDigitStr);
        Long iccidEndWithoutLastDigitL = Long.parseLong(iccidEndWithoutLastDigitStr);
        Long distributorAccountId = request.getDistributorAccountId();
        if (distributorAccountId == null) {
            distributorAccountId = 1L; // Aeris
        }
        // Number of total sims = number of 6 digit codes * pack size
        Long totalIccids = iccidEndWithoutLastDigitL - iccidStartWithoutLastDigitL;
        if ((totalIccids.longValue() + 1) != (numberOfSIMPacks * packSize)) {
            throw new SIMPackException("Invalid request :: Total number of sims in range do not match number of sim packs and pack size.");
        }   
        if (request.isCheckInSIMInventory()) {
            // Check if SIMs are present and not already assigned
            try {
                checkIfSIMsAssignedAlready(prepareICCIDList(request.getIccidStart(), request.getIccidEnd(), null));
            } catch (SIMPackException simPackException) {
                LOGGER.error("SIMs specified in range are already assigned.");
                throw simPackException;
            }
        }
        List<String> uniqueIds = getUniqueCodes(numberOfSIMPacks);
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        Date requestedDate = new Date();
        try {
            conn = DBConnectionManager.getInstance().getBillingDatabaseConnection();
            setAutoCommit(conn, false);
            preparedStatement = conn.prepareStatement(SQL_INSERT_SIMPACK_QUERY);
            long previousIccidRangeEnd = 0L;
            for (String uniqueId : uniqueIds) {
                long iccidStartValue;
                long iccidEndValue;
                preparedStatement.setString(1, uniqueId);
                preparedStatement.setString(2, "1"); // 1 as ICCID are mapped.                
                if (previousIccidRangeEnd == 0L) {
                    iccidStartValue = iccidStartWithoutLastDigitL;
                } else {
                    iccidStartValue = previousIccidRangeEnd + 1;
                }
                iccidEndValue = iccidStartValue + (packSize - 1);
                previousIccidRangeEnd = iccidEndValue;
                String actualIccidStart = Long.toString(iccidStartValue) + OAUtils.generateDigit(Long.toString(iccidStartValue));
                String actualIccidEnd = Long.toString(iccidEndValue) + OAUtils.generateDigit(Long.toString(iccidEndValue));
                preparedStatement.setString(3, actualIccidStart);
                preparedStatement.setString(4, actualIccidEnd);
                preparedStatement.setString(5, request.getUserEmail());
                preparedStatement.setString(6, request.getUserEmail());
                preparedStatement.setLong(7, distributorAccountId.longValue());
                preparedStatement.setInt(8, request.getPackSize());
                preparedStatement.setString(9, request.getSimType());
                preparedStatement.addBatch();
                simPack = new SIMPack();
                simPack.setUniqueCode(uniqueId);
                simPack.setIccidStart(actualIccidStart);
                simPack.setIccidEnd(actualIccidEnd);
                simPack.setDistributorAccountId(distributorAccountId);
                simPack.setCreatedBy(request.getUserEmail());
                simPack.setLastModifiedBy(request.getUserEmail());
                simPack.setCreatedDate(new java.sql.Date(requestedDate.getTime()));
                simPack.setLastModifiedDate(new java.sql.Date(requestedDate.getTime()));
                simPack.setUsed(true);
                simPack.setPacketSize(packSize);
                simPack.setSimType(request.getSimType());
                simPacks.add(simPack);
            }
            preparedStatement.executeBatch();
            conn.commit();
        } catch (SQLException e) {
            LOGGER.error("SQLException occurred while creating SIM packs.", e);
            throw new SIMPackException("Error occurred while creating SIM packs.", e);
        } finally {
            setAutoCommit(conn, true);
            DBUtils.cleanup(conn, preparedStatement, null);
        }
        return simPacks;
    }

}
