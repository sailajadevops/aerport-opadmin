package com.aeris.service.operatoradmin.dao;

import java.util.List;

import com.aeris.service.operatoradmin.exception.RESException;
import com.aeris.service.operatoradmin.model.RES;

public interface IRESDAO {
	public List<RES> getRESList(int productId) 	throws RESException;
}
