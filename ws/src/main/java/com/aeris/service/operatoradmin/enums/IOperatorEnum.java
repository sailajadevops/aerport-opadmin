package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.Operator;

public interface IOperatorEnum extends IBaseEnum{
	Operator getOperatorByName(String name);
	Operator getOperatorById(int id);
	List<Operator> getAllOperators();
	Operator getOperatorByKey(String operatorKey);
}
