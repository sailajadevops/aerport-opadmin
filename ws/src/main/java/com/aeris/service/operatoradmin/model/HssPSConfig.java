package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HssPSConfig implements Serializable {

    private String id;
    private String apns;
    private String apnDefault;
    private String apnOiReplacement;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApns() {
        return apns;
    }

    public void setApns(String apns) {
        this.apns = apns;
    }

    public String getApnDefault() {
        return apnDefault;
    }

    public void setApnDefault(String apnDefault) {
        this.apnDefault = apnDefault;
    }

    public String getApnOiReplacement() {
        return apnOiReplacement;
    }

    public void setApnOiReplacement(String apnOiReplacement) {
        this.apnOiReplacement = apnOiReplacement;
    }
}
