package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class ZoneRatePlan implements Serializable {
	private static final long serialVersionUID = 7582472101912421122L;
	private int zoneId;
	private IncludedPlanDetails includedPlanDetails;

	public int getZoneId() {
		return zoneId;
	}

	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}

	public IncludedPlanDetails getIncludedPlanDetails() {
		return includedPlanDetails;
	}

	public void setIncludedPlanDetails(IncludedPlanDetails includedPlanDetails) {
		this.includedPlanDetails = includedPlanDetails;
	}
}
