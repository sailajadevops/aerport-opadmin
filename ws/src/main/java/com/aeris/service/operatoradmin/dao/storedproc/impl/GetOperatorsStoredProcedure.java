package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Operator;
import com.google.inject.Inject;

public class GetOperatorsStoredProcedure extends AbstractProcedureCall<List<Operator>> {

	private static final String SQL_GET_OPERATORS = "common_util_pkg.get_ws_operartor";

	@Inject
	public GetOperatorsStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_OPERATORS, true);
		
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Operator> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<Operator> products = createOperatorList(cursor);

		return products;
	}

	private List<Operator> createOperatorList(List<Map<String, Object>> cursor) {
		List<Operator> operators = new ArrayList<Operator>();

		for (Map<String, Object> row : cursor) {
			Operator operator = new Operator();

			String operatorId = (String) row.get("operator_id");

			if (NumberUtils.isNumber(operatorId)) {
				operator.setOperatorId(Integer.parseInt(operatorId));
			}

			String description = (String) row.get("description");

			if (StringUtils.isNotEmpty(description)) {
				operator.setOperatorName(description);
			}
			
			operators.add(operator);
		}

		return operators;
	}
}
