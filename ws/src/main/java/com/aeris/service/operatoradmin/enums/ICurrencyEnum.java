package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.Currency;

public interface ICurrencyEnum  extends IBaseEnum{
	Currency getCurrencyByName(String name);
	Currency getCurrencyByCode(String code);
	List<Currency> getAllCurrencies();
}
