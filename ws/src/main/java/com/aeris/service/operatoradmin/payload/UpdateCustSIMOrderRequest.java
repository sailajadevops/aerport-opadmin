package com.aeris.service.operatoradmin.payload;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@XmlRootElement
public class UpdateCustSIMOrderRequest {
	@Min(value = 1, message="{quantity.minimum}")
	@Max(value = 10000, message="{quantity.maximum}")
	@NotNull(message="{quantity.notnull}")
	private long quantity;

	@Enumeration(value=OrderStatus.class, message="{status.enum}")
	private OrderStatus status;
	
	private String comments;

	private String trackingNumber;

	private String description;
	
	@NotNull(message="{shippingAddress.notnull}")
	@Size(min=1, max=60, message="{shippingAddress.size}")
	private String shippingAddress;
	
	@NotNull(message="{userId.notnull}")
	@Email(message="{userId.email.pattern}")
	private String userId;
	
	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	
	public void setStatus(OrderStatus status) {
		this.status = status;
	}
	
	public OrderStatus getStatus() {
		return status;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
}
