package com.aeris.service.operatoradmin.resources;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.ISupplierSIMOrderDAO;
import com.aeris.service.operatoradmin.enums.IIccIdRangeEnum;
import com.aeris.service.operatoradmin.enums.IImsiRangeEnum;
import com.aeris.service.operatoradmin.enums.ISIMFormatEnum;
import com.aeris.service.operatoradmin.enums.ISIMSupplierEnum;
import com.aeris.service.operatoradmin.enums.ISIMWarehouseEnum;
import com.aeris.service.operatoradmin.exception.SupplierSIMOrderException;
import com.aeris.service.operatoradmin.model.IMSIRangeType;
import com.aeris.service.operatoradmin.model.IccIdRange;
import com.aeris.service.operatoradmin.model.ImsiRange;
import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.SIMFormat;
import com.aeris.service.operatoradmin.model.SIMSupplier;
import com.aeris.service.operatoradmin.model.SIMWarehouse;
import com.aeris.service.operatoradmin.model.SupplierSIMOrder;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateApiKeyRequest;
import com.aeris.service.operatoradmin.payload.CreateSupplierSIMOrderRequest;
import com.aeris.service.operatoradmin.payload.UpdateCustSIMOrderRequest;
import com.aeris.service.operatoradmin.payload.UpdateSupplierSIMOrderRequest;

/**
 * Rest Resource for Supplier SIM Order management. Includes methods for 
 * <ul>
 * <li>
 * Placing a new SIM order by an operator to the supplier
 * </li>
 * <li>
 * Fetching the SIM orders placed by an operator
 * </li>
 * <li>
 * Fetch SIM orders by status i.e pending, in progress or completed
 * </li>
 * <li>
 * Update a SIM order only with status and quantity
 * </li>
 * <li>
 * Update a SIM order
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}/simOrders")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class SupplierSIMOrderResource{
	private static final Logger LOG = LoggerFactory.getLogger(SupplierSIMOrderResource.class);

    @Inject
	private ISupplierSIMOrderDAO simOrderDAO;

	@Inject
	private ISIMSupplierEnum simSupplierCache;
    
    @Inject
	private ISIMFormatEnum simFormatCache;    

    @Inject
	private ISIMWarehouseEnum simWarehouseCache;	
    
    @Inject
	private IIccIdRangeEnum iccIdRangeCache;  
   
    @Inject
	private IImsiRangeEnum imsiRangeCache;  
    
    @GET
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response getAllSIMOrders(@PathParam("operatorId") @ValidOperator final String operatorId, 
			@QueryParam("supplierId") @Pattern(regexp = "[0-9]+", message = "{supplierId.number}") @DefaultValue("0") final String supplierId, 
			@QueryParam("status") @DefaultValue("all") final String status, 
			@QueryParam("start") final String start, 
			@QueryParam("count") final String count) {
		LOG.info("Received request to fetch all SIM Orders for operatorId: "+operatorId+", supplierId: " + supplierId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		try {
			if (paginated) {
				LOG.info("Returning a max record size of " + count + " records starting from " + start+" for supplierId: "+supplierId);
			} else {
				LOG.info("Query is not paginated, returning all available SIM Orders for supplierId: " + supplierId);
			}

			List<SupplierSIMOrder> simOrders = null;
			
			// Fetch orders with any status
			if(!OrderStatus.isValid(status))
			{
				// Fetch from database
				simOrders = simOrderDAO.getAllSIMOrders(operatorId, Integer.parseInt(supplierId), start, count);
			}
			// Fetch orders with specific status like Open, In Progress, Completed etc. 
			else
			{
				// Fetch from database
				simOrders = simOrderDAO.getAllSIMOrders(operatorId, Integer.parseInt(supplierId), start, count, OrderStatus.fromName(status));
			}
			
			if (simOrders != null) {
				LOG.info("fetched SIM Orders successfully, sim order count: "+simOrders.size());

				ObjectMapper mapper = new ObjectMapper();
				String simOrdersJson = mapper.writeValueAsString(simOrders);

				builder.entity(simOrdersJson);
			} else {
				LOG.info("No SIM Orders found for supplier : " + supplierId);
				return Response.status(Status.NOT_FOUND).entity("No sim orders found for the supplier").build();
			}
		} catch (Exception e) {
			LOG.error("getAllSIMOrders Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting SIM Orders for supplier").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllSIMOrders: " + response);
		return response;
	}
    
    @GET
    @Path("/{orderId}")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
    public Response getSIMOrder(@PathParam("operatorId") @ValidOperator final String operatorId, 
    		@PathParam("orderId") @Pattern(regexp = "[0-9]+", message = "{orderId.number}") final String orderId) {
		LOG.info("Received request to fetch Supplier SIM Order for orderId: " + orderId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			if (!NumberUtils.isNumber(operatorId)) {
				LOG.error("The format of operatorId in the request is not valid, value is: '" + operatorId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of operatorId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(orderId)) {
				LOG.error("The format of orderId in the request is not valid, value is: '" + orderId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of orderId in the request is not valid").build();
			}
			
			// Fetch from database
			SupplierSIMOrder simOrder = simOrderDAO.getSIMOrderByOrderId(operatorId, orderId);
			
			if (simOrder != null) {
				LOG.info("fetched Supplier SIM Order for orderId: "+orderId);

				ObjectMapper mapper = new ObjectMapper();
				String simOrderJson = mapper.writeValueAsString(simOrder);

				builder.entity(simOrderJson);
			} else {
				LOG.info("No Supplier SIM Order found for order id: "+orderId);
				return Response.status(Status.NOT_FOUND).entity("No Supplier SIM Order found for order id: "+orderId).build();
			}
		} catch (Exception e) {
			LOG.error("getSIMOrder Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting Supplier SIM Order").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getSIMOrder: " + response);
		return response;
	}
    
	/**
	 * Create a new api key for accountId with the required permissions
	 * specified in the {@link CreateApiKeyRequest}
	 * 
	 * @param request
	 *            the request containing the account id and the required
	 *            permissions for the new api key
	 * @return the new api key created for the account id
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response createNewOrder(@PathParam("operatorId")  @ValidOperator final String operatorId, 
			@Valid CreateSupplierSIMOrderRequest request) {

		LOG.info("Received request to create SIM Order for operatorId: " + operatorId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		int supplierId = request.getSupplierId();
		long quantity = request.getQuantity();		
		int simFormatId = request.getSimFormatId();
		int productId = Integer.parseInt(request.getProductId());
		OrderStatus status = request.getStatus(); 
		String iccIdRange = request.getIccIdRange(); 
		String imsiRange = request.getImsiRange(); 
		IMSIRangeType imsiRangeType = request.getImsiRangeType(); 
		String iccIdStart = request.getIccIdStart(); 
		String imsiStart = request.getImsiStart(); 
		int simWarehouseId = request.getSimWarehouseId(); 
		String userId = request.getUserId();
		Date requestedDate = new Date(); 
		
		try {
			SIMSupplier supplier = simSupplierCache.getSIMSupplierById(supplierId);
			SIMWarehouse warehouse = simWarehouseCache.getSIMWarehouseById(simWarehouseId);
			SIMFormat format = simFormatCache.getSIMFormatById(simFormatId);
			IccIdRange iccIdRangeVal = iccIdRangeCache.getIccIdRange(iccIdRange);
			ImsiRange ismiRangeVal = imsiRangeCache.getImsiRange(imsiRange);
			
			if(supplier == null)
			{
				throw new SupplierSIMOrderException("Supplier does not exist");
			}
			
			if (warehouse == null) {
				LOG.error("The simWarehouse in request is not valid, value is: '" + warehouse + "'");
				return Response.status(Status.BAD_REQUEST).entity("The simWarehouse in request is not valid").build();
			}
			
			if (format == null) {
				LOG.error("The simFormat in request is not valid, value is: '" + simFormatId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The simFormat in request is not valid").build();
			}
			
			if (iccIdRangeVal == null) {
				LOG.error("The iccIdRange in request is not valid, value is: '" + iccIdRange + "'");
				return Response.status(Status.BAD_REQUEST).entity("The iccIdRange in request is not valid").build();
			}
			
			if (ismiRangeVal == null) {
				LOG.error("The imsiRange in request is not valid, value is: '" + imsiRange + "'");
				return Response.status(Status.BAD_REQUEST).entity("The imsiRange in request is not valid").build();
			}

			// Create Supplier SIM Order in Database
			SupplierSIMOrder simOrder = simOrderDAO.createSIMOrder(operatorId, supplierId, quantity, simWarehouseId, simFormatId, productId, status,
					iccIdRange, imsiRange, imsiRangeType, iccIdStart, imsiStart, requestedDate, userId);
			
			if (simOrder != null && simOrder.getOrderId() > 0) {
				LOG.info("Created Supplier SIM Order Successfully, order id is : "+simOrder.getOrderId());

				ObjectMapper mapper = new ObjectMapper();
				String simOrderJson = mapper.writeValueAsString(simOrder);

				builder.entity(simOrderJson);
			} else {
				LOG.info("Create new sim order failed");
				return Response.status(Status.NOT_FOUND).entity("Create new sim order failed").build();
			}
		} catch (SupplierSIMOrderException e) {
			LOG.error("SupplierSIMOrderException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("createNewOrder Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while creating SIM Order").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewOrder: " + response);
		return response;
	}
	
	/**
	 * Update a SIM Order identified by the orderId for the specified account under the specified operator. The update SIM Order request {@link UpdateCustSIMOrderRequest} 
	 * will take the following values as input:
	 * 
	 *  <ul>
	 *  <li>
	 *  Quantity of the SIMs
	 *  </li>
	 *  <li>
	 *  The New Order Status
	 *  </li>
	 *  <li>
	 *  Start ICCID
	 *  </li>
	 *  <li>
	 *  Shipping Address
	 *  </li>
	 *  <li>
	 *  Description
	 *  </li>
	 *  </ul>
	 *  
	 * @param operatorId The operator for which the account belongs
	 * @param accountId The Account Identifier
	 * @param orderId The Order Id for the SIM Order which is updated
	 * @param request The payload {@link UpdateCustSIMOrderRequest}
	 * 
	 * @return {@link Response} with success message.
	 */
	@PUT
	@Path("/{orderId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateOrder(@PathParam("operatorId") @ValidOperator final String operatorId, 
			@PathParam("orderId") @Pattern(regexp = "[0-9]+", message = "{orderId.number}") final String orderId, 
			@Valid UpdateSupplierSIMOrderRequest request) {

		LOG.info("Received request to Update Supplier SIM Order for operatorId: " + operatorId+" and order id: " + orderId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		long quantity = request.getQuantity();		
		OrderStatus status = request.getStatus(); 
		String iccIdRange = request.getIccIdRange(); 
		String imsiRange = request.getImsiRange(); 
		IMSIRangeType imsiRangeType = request.getImsiRangeType(); 
		String iccIdStart = request.getIccIdStart(); 
		String imsiStart = request.getImsiStart(); 
		String userId = request.getUserId();
		Date requestedDate = new Date(); 
		
		try {
			IccIdRange iccIdRangeVal = iccIdRangeCache.getIccIdRange(iccIdRange);
			ImsiRange ismiRangeVal = imsiRangeCache.getImsiRange(imsiRange);
			
			if (iccIdRangeVal == null) {
				LOG.error("The iccIdRange in request is not valid, value is: '" + iccIdRange + "'");
				return Response.status(Status.BAD_REQUEST).entity("The iccIdRange in request is not valid").build();
			}
			
			if (ismiRangeVal == null) {
				LOG.error("The imsiRange in request is not valid, value is: '" + imsiRange + "'");
				return Response.status(Status.BAD_REQUEST).entity("The imsiRange in request is not valid").build();
			}
			
			// Update Supplier SIM Order in Database
			boolean flag = simOrderDAO.updateSIMOrder(operatorId, Long.parseLong(orderId), status, quantity, iccIdRange, imsiRange, imsiRangeType,
					iccIdStart, imsiStart, requestedDate, userId);
			
			if (flag) {
				LOG.info("Update Supplier SIM Order Successfully, order id is : "+orderId);

				ObjectMapper mapper = new ObjectMapper();
				String updateOrderJson = mapper.writeValueAsString("Supplier SIM Order Id: "+orderId+" updated successfully");

				builder.entity(updateOrderJson);
			} else {
				LOG.info("Update sim order failed");
				return Response.status(Status.NOT_FOUND).entity("Update sim order failed").build();
			}
		} catch (SupplierSIMOrderException e) {
			LOG.error("SupplierSIMOrderException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateOrder Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while updating Supplier SIM Order").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateOrder: " + response);
		return response;
	}
		
	/**
	 * Update a SIM Order identified by the orderId for the specified account under the specified operator. The update SIM Order request {@link UpdateCustSIMOrderRequest} 
	 * will take the following values as input:
	 * 
	 *  <ul>
	 *  <li>
	 *  Quantity of the SIMs
	 *  </li>
	 *  <li>
	 *  The New Order Status
	 *  </li>
	 *  <li>
	 *  Start ICCID
	 *  </li>
	 *  <li>
	 *  Shipping Address
	 *  </li>
	 *  <li>
	 *  Description
	 *  </li>
	 *  </ul>
	 *  
	 * @param operatorId The operator for which the account belongs
	 * @param accountId The Account Identifier
	 * @param orderId The Order Id for the SIM Order which is updated
	 * @param request The payload {@link UpdateCustSIMOrderRequest}
	 * 
	 * @return {@link Response} with success message.
	 */
	@DELETE
	@Path("/{orderId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response deleteOrder(@PathParam("operatorId") @ValidOperator final String operatorId, 
			@PathParam("orderId") @Pattern(regexp = "[0-9]+", message = "{orderId.number}") final String orderId, 
			@QueryParam("userId") @Email(message="{userId.email.pattern}") final String requestedUser) {

		LOG.info("Received request to Update Supplier SIM Order for operatorId: " + operatorId+" and order id: " + orderId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		// requestedDate and userId will be used for Auditing purposes
		Date requestedDate  = new Date();
		
		try {
			// Delete Supplier SIM Order in Database
			boolean flag = simOrderDAO.deleteSIMOrder(operatorId, Long.parseLong(orderId), requestedDate, requestedUser);
			
			if (flag) {
				LOG.info("Delete Supplier SIM Order Successfully, order id is : "+orderId);

				ObjectMapper mapper = new ObjectMapper();
				String updateOrderJson = mapper.writeValueAsString("Supplier SIM Order Id: "+orderId+" deleted successfully");

				builder.entity(updateOrderJson);
			} else {
				LOG.info("Delete sim order failed");
				return Response.status(Status.NOT_FOUND).entity("Delete sim order failed").build();
			}
		} catch (SupplierSIMOrderException e) {
			LOG.error("SupplierSIMOrderException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("deleteOrder Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while deleting Supplier SIM Order").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from deleteOrder: " + response);
		return response;
	}
}
