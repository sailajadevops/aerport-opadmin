package com.aeris.service.operatoradmin.enums;

import com.aeris.service.operatoradmin.model.Contract;

public interface IContractEnum {
	Contract getContract(long accountId);
	void putContractInCache(long accountId, Contract contract);
	void removeContractFromCache(long accountId);
}
