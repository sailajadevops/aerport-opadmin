package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.sql.Connection;
import java.sql.Types;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.dao.storedproc.ResultStatus;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.google.inject.Inject;

public class UpdateCSPRatePlanGSMStoredProcedure extends AbstractProcedureCall<ResultStatus> {

	private static final String SQL_UPDATE_GSM_RATE_PLAN = "rate_plan_update_gsm_v1";

	@Inject
	public UpdateCSPRatePlanGSMStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_UPDATE_GSM_RATE_PLAN);

		// In Parameters
		registerParameter(new InParameter("in_rate_plan_id", Types.INTEGER));
		registerParameter(new InParameter("in_plan_name", Types.VARCHAR));
		registerParameter(new InParameter("in_start_date", Types.DATE));
		registerParameter(new InParameter("in_end_date", Types.DATE));
		registerParameter(new InParameter("in_access_fee", Types.NUMERIC));
		registerParameter(new InParameter("in_sms_included", Types.NUMERIC));
		registerParameter(new InParameter("in_sms_monthly_charge", Types.NUMERIC));
		registerParameter(new InParameter("in_sms_overage_domestic", Types.NUMERIC));
		registerParameter(new InParameter("in_sms_overage_intl", Types.NUMERIC));
		registerParameter(new InParameter("in_sms_overage_canada", Types.NUMERIC));
		registerParameter(new InParameter("in_sms_overage_mexico ", Types.NUMERIC));
		registerParameter(new InParameter("in_gprs_included", Types.NUMERIC));
		registerParameter(new InParameter("in_gprs_monthly_charge", Types.NUMERIC));
		registerParameter(new InParameter("in_gprs_overage_domestic", Types.NUMERIC));
		registerParameter(new InParameter("in_gprs_overage_intl", Types.NUMERIC));
		registerParameter(new InParameter("in_gprs_overage_canada", Types.NUMERIC));
		registerParameter(new InParameter("in_gprs_overage_mexico", Types.NUMERIC));
		registerParameter(new InParameter("in_mburst_included", Types.NUMERIC));
		registerParameter(new InParameter("in_mburst_monthly_charge", Types.NUMERIC));
		registerParameter(new InParameter("in_mburst_overage_domestic", Types.NUMERIC));
		registerParameter(new InParameter("in_mburst_overage_intl", Types.NUMERIC));
		registerParameter(new InParameter("in_mburst_overage_canada", Types.NUMERIC));
		registerParameter(new InParameter("in_mburst_overage_mexico", Types.NUMERIC));
		registerParameter(new InParameter("in_voice_included", Types.NUMERIC));
		registerParameter(new InParameter("in_voice_monthly_charge", Types.NUMERIC));
		registerParameter(new InParameter("in_voice_overage_domestic", Types.NUMERIC));
		registerParameter(new InParameter("in_voice_overage_intl", Types.NUMERIC));
		registerParameter(new InParameter("in_voice_overage_canada", Types.NUMERIC));
		registerParameter(new InParameter("in_voice_overage_mexico", Types.NUMERIC));
		registerParameter(new InParameter("in_first_bill_activation_fee", Types.NUMERIC));
		registerParameter(new InParameter("in_suspend_fee", Types.NUMERIC));
		registerParameter(new InParameter("in_deactivation_fee", Types.NUMERIC));
		registerParameter(new InParameter("in_reactivation_fee", Types.NUMERIC));
		registerParameter(new InParameter("in_unsuspend_fee", Types.NUMERIC));
		registerParameter(new InParameter("in_deact_waived_after_months", Types.INTEGER));
		registerParameter(new InParameter("in_prov_microburst_pkt_price", Types.NUMERIC));
		registerParameter(new InParameter("in_prov_sms_msg_price", Types.NUMERIC));
		registerParameter(new InParameter("in_prov_1xrtt_kb_price", Types.NUMERIC));
		registerParameter(new InParameter("in_prov_voice_minutes_price", Types.NUMERIC));
		registerParameter(new InParameter("in_allowed_prov_days_traffic", Types.NUMERIC));
		registerParameter(new InParameter("in_allowed_prov_mburst_pkts", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_prov_sms_msgs", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_prov_1xrtt_kb", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_prov_voice_mins", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_prov_months", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_susp_days_traffic", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_susp_mburst_pkts", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_susp_sms_msgs", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_susp_1xrtt_kb", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_susp_voice_mins", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_susp_months", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_reprov_days_traffic", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_reprov_mburst_pkts", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_reprov_sms_msgs", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_reprov_1xrtt_kb", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_reprov_voice_mins", Types.INTEGER));
		registerParameter(new InParameter("in_allowed_reprov_months", Types.INTEGER));
		registerParameter(new InParameter("in_packet_data_rounding_policy", Types.VARCHAR));
		registerParameter(new InParameter("in_comments", Types.VARCHAR));
		registerParameter(new InParameter("in_changed_by", Types.VARCHAR));
		registerParameter(new InParameter("in_product_id", Types.NUMERIC));
		registerParameter(new InParameter("in_status", Types.NUMERIC)); 
		registerParameter(new InParameter("in_device_pooling_policy", Types.VARCHAR));
		// Out Parameter
		registerParameter(new OutParameter("ret_status", Types.NUMERIC));
	}

	@Override
	@SuppressWarnings("unchecked")
	public ResultStatus execute(Connection conn, Object... input) throws StoredProcedureException {
		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(conn, input);
		ResultStatus status = new ResultStatus();

		// Read the status
		if (results != null) {
			String retStatus = (String) results.get("ret_status");

			if (NumberUtils.isNumber(retStatus)) {
				int statusCode = Integer.parseInt(retStatus);
				status.setResultCode(statusCode); // success_code == 0
			}
		}

		return status;
	}
}
