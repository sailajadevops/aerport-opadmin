package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement 
public class Contract implements Serializable{
	private int contractId;
	private String contractName;
	private String partnerId;
	private String authorization;
	private String gatewayUrl;
	private long smppConnectionId;
	private long accountId;
	private String licenseKey;
	private boolean permanentDialableNo;
	
	
	public boolean isPermanentDialableNo() {
		return permanentDialableNo;
	}
	public void setPermanentDialableNo(boolean permanentDialableNo) {
		this.permanentDialableNo = permanentDialableNo;
	}
	public String getLicenseKey() {
		return licenseKey;
	}
	public void setLicenseKey(String licenseKey) {
		this.licenseKey = licenseKey;
	}
	public int getContractId() {
		return contractId;
	}
	public void setContractId(int contractId) {
		this.contractId = contractId;
	}
	public String getContractName() {
		return contractName;
	}
	public void setContractName(String contractName) {
		this.contractName = contractName;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	public String getGatewayUrl() {
		return gatewayUrl;
	}
	public void setGatewayUrl(String gatewayUrl) {
		this.gatewayUrl = gatewayUrl;
	}
	public long getSmppConnectionId() {
		return smppConnectionId;
	}
	public void setSmppConnectionId(long smppConnectionId) {
		this.smppConnectionId = smppConnectionId;
	}
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	
	
	
}
