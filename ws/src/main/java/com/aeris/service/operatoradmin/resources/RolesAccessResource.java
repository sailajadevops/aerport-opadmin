package com.aeris.service.operatoradmin.resources;

import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Singleton;
import javax.validation.constraints.Pattern;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.glassfish.jersey.process.internal.RequestScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.security.SecurityContext;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.utils.Constants;

/**
 * Ping Resource for health monitoring of the operator admin 2.0 
 * webservices
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}")
@Singleton
@RequestScoped
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class RolesAccessResource {
	
	private static final Logger LOG = LoggerFactory.getLogger(RolesAccessResource.class);

	@Context
    SecurityContext securityContext;
	/**
	 * <p>
	 * Checks if the operator admin 2.0 webservices are up and running.
	 * Send Ok if the webservices is up and running.
	 * </p>
	 * 
	 * @return Ok for successful ping, otherwise error
	 */
	@Path("/ping")
	@GET
	@RolesAllowed({"OPERATOR", "ACCOUNT", "PLATFORM_ADMIN", "OPERATOR_WR", "PING_ONLY"})
	public Response ping(@PathParam("operatorId") @ValidOperator final String operatorId,
			@QueryParam("productId") @Pattern(regexp = "[0-9]+", message = "{productId.number}") final String productId) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		LOG.debug("Ping response: Ok");
		
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	
	@Path("/ping1")
	@GET
	public Response ping1(@PathParam("operatorId")  @ValidOperator final String operatorId,
			@QueryParam("productId") 
	 		@Pattern(regexp = "[0-9]+", message = "{zoneId.number}")
			final String productId) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		LOG.debug("Ping response: Ok");
		
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	
	@Path("/ping2")
	@GET
	@RolesAllowed({"OPERATOR", "ACCOUNT", "PLATFORM_ADMIN"})
	public Response ping2(@Context SecurityContext sc, @PathParam("apiKey") final String apiKey) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		LOG.debug("Ping response: Ok");
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	
	@Path("/ping3")
	@GET
	@DenyAll
	public Response ping3(@Context SecurityContext sc, @PathParam("apiKey") final String apiKey) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		LOG.debug("Ping response: Ok");
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	
	@Path("/accts/{accountId}/platformAdminOnly")
	@GET
	@RolesAllowed({"PLATFORM_ADMIN"})
	public Response platformAdminOnly(@Context SecurityContext sc, @PathParam("apiKey") final String apiKey) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		LOG.debug("Ping response: Ok");
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	
	@Path("/accts/{accountId}/operatorReadWriteOnly")
	@GET
	@RolesAllowed({"OPERATOR_WR"})
	public Response operatorReadWriteOnly(@Context SecurityContext sc, @PathParam("apiKey") final String apiKey) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		LOG.debug("Ping response: Ok");
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	
	@Path("/accts/{accountId}/pAdminNOperatorWR")
	@GET
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response pAdminNOperatorWR(@Context SecurityContext sc, @PathParam("apiKey") final String apiKey) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		LOG.debug("Ping response: Ok");
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	@Path("/accts/{accountId}/pAdminNOperators")
	@GET
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response pAdminNOperators(@Context SecurityContext sc, @PathParam("apiKey") final String apiKey) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		LOG.debug("Ping response: Ok");
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	
	@Path("/accts/{accountId}/adminsOperatorsNAccount")
	@GET
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR", "ACCOUNT"})
	public Response adminsOperatorsNAccount(@Context SecurityContext sc, @PathParam("apiKey") final String apiKey) {
		LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		LOG.debug("Ping response: Ok");
		return builder.entity("OperatorAdmin 2.0").build();
	}
	
	
	
}
