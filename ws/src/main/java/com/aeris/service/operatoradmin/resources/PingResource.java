package com.aeris.service.operatoradmin.resources;

import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.glassfish.jersey.process.internal.RequestScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.security.SecurityContext;
import com.aeris.service.operatoradmin.ApplicationVersion;
import com.aeris.service.operatoradmin.utils.Constants;

/**
 * Ping Resource for health monitoring of the operator admin 2.0 
 * webservices
 * 
 * @author Srinivas Puranam
 */
@Singleton
@RequestScoped
@Path("/ping")
@PermitAll
public class PingResource {
	private static final Logger LOG = LoggerFactory.getLogger(PingResource.class);

    @Inject
private ApplicationVersion appVersion;

	@Context
    SecurityContext securityContext;
	/**
	 * <p>
	 * Checks if the operator admin 2.0 webservices are up and running.
	 * Send Ok if the webservices is up and running.
	 * </p>
	 * 
	 * @return Ok for successful ping, otherwise error
	 */
	
	@GET
	public Response ping() {
		      LOG.info("Received ping request at time: " + Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTime());

        ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

        LOG.debug("Ping response: Ok");
        StringBuilder sb = new StringBuilder();
        sb.append("Operator Admin ").append(appVersion.getVersion());
        LOG.info("ping = " + sb.toString());
        return builder.entity(sb.toString()).build();
	}
	
}
