package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum RatePlanType implements Serializable {
	NON_TIERED(1), TIERED(2), SUB_RATE_PLAN(3);
	int value;

	RatePlanType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int value) {
		RatePlanType[] statuses = values();

		for (RatePlanType ratePlanType : statuses) {
			if (ratePlanType.getValue() == value) {
				return true;
			}
		}

		return false;
	}

	public static RatePlanType fromValue(int value) {
		RatePlanType[] statuses = values();

		for (RatePlanType ratePlanType : statuses) {
			if (ratePlanType.getValue() == value) {
				return ratePlanType;
			}
		}

		return null;
	}

	public static RatePlanType fromName(String value) {
		RatePlanType[] statuses = values();

		for (RatePlanType ratePlanType : statuses) {
			if (ratePlanType.name().equalsIgnoreCase(value)) {
				return ratePlanType;
			}
		}

		return null;
	}
}
