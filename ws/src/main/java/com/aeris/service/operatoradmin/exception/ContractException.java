package com.aeris.service.operatoradmin.exception;

public class ContractException extends Exception {

	public ContractException(String s) {
		super(s);
	}

	public ContractException(String ex, Throwable t) {
		super(ex, t);
	}
}
