package com.aeris.service.operatoradmin.resources;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.client.AerCloudClient;
import com.aeris.service.operatoradmin.client.AerCloudResponse;
import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.IDistributorDAO;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventDispatcher;
import com.aeris.service.operatoradmin.exception.AccountDeactivationException;
import com.aeris.service.operatoradmin.exception.AccountException;
import com.aeris.service.operatoradmin.exception.AccountMappingException;
import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.DuplicateAccountException;
import com.aeris.service.operatoradmin.exception.SubAccountException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.AccountStatus;
import com.aeris.service.operatoradmin.model.AccountType;
import com.aeris.service.operatoradmin.model.ApprovalStatus;
import com.aeris.service.operatoradmin.model.BillableStatus;
import com.aeris.service.operatoradmin.model.BillingDetails;
import com.aeris.service.operatoradmin.model.Contact;
import com.aeris.service.operatoradmin.model.InvoiceAddress;
import com.aeris.service.operatoradmin.model.Region;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateAccountRequest;
import com.aeris.service.operatoradmin.payload.NotificationRequest;
import com.aeris.service.operatoradmin.payload.UpdateAccountRequest;
import com.aeris.service.operatoradmin.payload.UpdateAccountStatusRequest;
import com.aeris.service.operatoradmin.utils.OAUtils;

/**
 * Rest Resource for Account management. Includes methods for
 * <ul>
 * <li>
 * Create a new Account
 * </li>
 * <li>
 * Fetch existing accounts by operator
 * </li>
 * <li>
 * Fetch existing account by account id
 * </li>
 * <li>
 * Update exisiting Account details
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}/accounts")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class AccountResource {
	private static final Logger LOG = LoggerFactory.getLogger(AccountResource.class);

	@Inject
	private IAccountDAO accountDAO;
	
	@Inject
	private IDistributorDAO distributorDAO;
	
	@Inject
	private AerCloudClient aerCloudClient;
	
	@Inject
	private OAUtils oaUtils;
    
    @Inject
	private IEventDispatcher eventDispatcher;
	
	/**
	 * <p>
	 * Fetches List of Account details per operator.
	 * Allows to filter the Accounts based of status or list of specific account ids.
	 * </p>
	 * 
	 * @param operatorId The Operator Id
	 * @param status The Status
	 * @param start The start index for pagination
	 * @param count The record count for pagination
	 * @param accountIds The list of specific account ids to be fetched 
	 * @param extendAttributes true if the api sends extended account information to the client
	 * @param wholesaleManagementEnabled If true then returns wholesale accounts and Regular accounts under the current operator
	 * @param wholesaleAccountId If specified then returns only Retail accounts under the given wholesaleAccountId
	 * 
	 * @return The List of Accounts
	 */
	@GET
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllAccounts(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@QueryParam("status") @DefaultValue("all") final String status, @QueryParam("start") final String start,
			@QueryParam("count") final String count,
			@QueryParam("accountIds") @Pattern(regexp = "(\\d+)(,\\s*\\d+)*", message = "{accountIds.pattern}") final String accountIds,
			@QueryParam("extendAttributes") final boolean extendAttributes,
			@QueryParam("wholesaleManagementEnabled") final boolean wholesaleManagementEnabled) {
		LOG.info("Received request to fetch all Accounts for operatorId: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		List<String> accountIdList = null;
		List<Account> accounts = null;

		try {
			if (paginated) {
				LOG.info("Returning a max record size of " + count + " accounts starting from " + start + " for operatorId: " + operatorId);
			} else {
				LOG.info("Query is not paginated, returning all available accounts for operatorId: " + operatorId);
			}
			
			// Parse Account Ids
			accountIdList = accountIdsToList(accountIds);
			// Fetch orders with any status
			if (!AccountStatus.isValid(status)) {
				// Fetch from database				
				accounts = accountDAO.getAllAccounts(operatorId, start, count, accountIdList, wholesaleManagementEnabled);
			}
			// Fetch orders with specific status like Open, In Progress,
			// Completed etc.
			else {
				// Fetch from database
				accounts = accountDAO.getAllAccounts(operatorId, start, count, accountIdList, wholesaleManagementEnabled, AccountStatus.fromName(status));
			}
			if (accounts != null) {
				LOG.info("fetched accounts successfully, no of accounts: " + accounts.size());

				ObjectMapper mapper = new ObjectMapper();
				String accountsJson = mapper.writeValueAsString(accounts);

				builder.entity(accountsJson);
			} else {
				LOG.info("No accounts for operator : " + operatorId);
				return Response.status(Status.NOT_FOUND).entity("No accounts found for the supplier").build();
			}
		} catch (Exception e) {
			LOG.error("getAccounts Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting accounts").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllAccounts: " + response);
		return response;
	}
	/**
	 * <p>
	 * Fetches List of Retail Account for given wholesale account id 
	 * </p>
	 * 
	 * @param operatorId The Operator Id
	 * @param status The Status
	 * @param start The start index for pagination
	 * @param count The record count for pagination
	 * @param wholesaleAccountId If specified then returns only Retail accounts under the given wholesaleAccountId
	 * 
	 * @return The List of Accounts
	 */
	@GET
	@Path("/retail")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getRetailAccounts(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@QueryParam("start") final String start,
			@QueryParam("count") final String count,
			@QueryParam("wholesaleAccountId") final String wholesaleAccountId) {
		LOG.info("Received request to fetch Retail Accounts for wholesaleAccountId: " + wholesaleAccountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		List<Account> accounts = null;

		try {
			if (paginated) {
				LOG.info("Returning a max record size of " + count + " accounts starting from " + start + " for operatorId: " + operatorId);
			} else {
				LOG.info("Query is not paginated, returning all retail available accounts for wholesaleAccountId: " + wholesaleAccountId);
			} 
			 
			// Fetch from database				
			accounts = accountDAO.getRetailAccounts(operatorId, start, count, wholesaleAccountId); 
			if (accounts != null) {
				LOG.info("fetched accounts successfully, no of accounts: " + accounts.size());

				ObjectMapper mapper = new ObjectMapper();
				String accountsJson = mapper.writeValueAsString(accounts);

				builder.entity(accountsJson);
			} else {
				LOG.info("No accounts for operator : " + operatorId);
				return Response.status(Status.NOT_FOUND).entity("No accounts found for the supplier").build();
			}
		} catch (Exception e) {
			LOG.error("getAccounts Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting accounts").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getRetailAccounts: " + response);
		return response;
	}

	/**
	 * Converts comma seperated account ids to list of account ids 
	 * 
	 * @param accountIds the comma seperated account ids
	 * 
	 * @return list of account ids
	 */
	private List<String> accountIdsToList(final String accountIds) {
		List<String> accountIdList = new ArrayList<String>();
		boolean accountIdsNotEmpty = StringUtils.isNotEmpty(accountIds);

		if (accountIdsNotEmpty) {
			Collections.addAll(accountIdList, accountIds.split(",\\s*"));
		}

		return accountIdList;
	}

	/**
	 * Fetch a specific account identified by account id
	 * 
	 * @param operatorId the operator id
	 * @param accountId the account id
	 * 
	 * @return The Account details
	 */
	@GET
	@Path("/{accountId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "ACCOUNT", "OPERATOR"})
	public Response getAccount(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId) {
		LOG.info("Received request to fetch account for accountId: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			// Fetch from database
			Account account = accountDAO.getAccount(operatorId, accountId);

			if (account != null) {
				LOG.info("fetched account successfully for accountId: " + accountId);

				ObjectMapper mapper = new ObjectMapper();
				String accountJson = mapper.writeValueAsString(account);

				builder.entity(accountJson);
			} else {
				LOG.info("No account found for accountId: " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No account found for account id: " + accountId).build();
			}
		} catch (Exception e) {
			LOG.error("getAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting account").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAccount: " + response);
		return response;
	}

	/**
	 * Create a new account for an operator based on the information 
	 * specified in the {@link CreateAccountRequest} by the client
	 * 
	 * The request accepts the user credentials, account name, 
	 * contacts information, billing details and list of products enrolled for this account
	 * 
	 * @param operatorId the operator id
	 * @param request the create account request sent by the client
	 * 
	 * @return the newly created account information
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response createNewAccount(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@Valid CreateAccountRequest request) {

		LOG.info("Received request to create account under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String userId = request.getUserId();
		String accountName = request.getAccountName();
		Contact primaryContact = request.getPrimaryContact();
		List<Contact> additionalContacts = request.getAdditionalContacts();
		BillingDetails billingDetails = request.getBillingDetails();
		List<String> productIds = request.getProductIds();
		AccountStatus status = request.getStatus();
		ApprovalStatus approvalStatus = request.getApprovalStatus();
		String parentAccountId = request.getParentAccountId();
		String carrierAccountId = request.getCarrierAccountId();
		String netSuiteId = request.getNetSuiteId();
		Region region = request.getRegion();
		AccountType accountType = request.getType();
		InvoiceAddress invoiceAddress = request.getInvoiceAddress();
		String parentLabel = request.getParentLabel();
		BillableStatus billableStatus = request.getBillableStatus();
		String specialInstructions = request.getSpecialInstructions();
		String tags = request.getTags();
		int includedAlertProfiles = request.getIncludedAlertProfiles();
		String verticalId = request.getVerticalId();
		int maxPortalAccounts = request.getMaxPortalAccounts();
		int maxReportHistory = request.getMaxReportHistory();
		int maxScheduledReports = request.getMaxScheduledReports();
		String[] serviceImpairmentEmail = request.getServiceImpairmentEmail();
		String[] myAlertsEmail = request.getMyAlertsEmail();
		String[] scheduledReportsEmail = request.getScheduledReportsEmail();
        String[] supportContactEmails = request.getSupportContactEmails();
		
		String webAddress = request.getWebAddress();

		Date requestedDate = new Date();
        
        String distributorId = request.getDistributorId();

		try {
			LOG.info("Calling Account DAO");

			Account account = accountDAO.createNewAccount(operatorId, accountName, region, accountType, status, approvalStatus, invoiceAddress, productIds,
					parentAccountId, parentLabel, carrierAccountId, primaryContact, additionalContacts, webAddress, billableStatus,	billingDetails, netSuiteId, 
					specialInstructions, tags, includedAlertProfiles, verticalId, maxPortalAccounts, maxScheduledReports, maxReportHistory, serviceImpairmentEmail, 
					myAlertsEmail, scheduledReportsEmail, supportContactEmails, requestedDate, userId, request.getAccountOverageBucket(), 
					request.getAgentId(), distributorId,request.getAccountType(), request.isEnableCreateWholesale(), request.getWholesaleAccountId(),request.getDistributor(), request.getCreditLimit());

            if (ApprovalStatus.APPROVED == approvalStatus) {
                try {
                    accountDAO.updateAccountStatus(operatorId, String.valueOf(account.getAccountId()), status, approvalStatus, requestedDate, userId);
                } catch (AccountNotFoundException ex) {
                    LOG.error("Error occurred while updating ApprovalStatus of account " + account.getAccountId() + " to APPROVED due to " + ex.getMessage());
                } catch (AccountException ex) {
                    LOG.error("Error occurred while updating ApprovalStatus of account " + account.getAccountId() + " to APPROVED due to " + ex.getMessage());
                } catch (AccountDeactivationException ex) {
                    LOG.error("Error occurred while updating ApprovalStatus of account " + account.getAccountId() + " to APPROVED due to " + ex.getMessage());
                }
            }
            
			if(oaUtils.hasAerCloud(productIds))
			{
				LOG.info("Aercloud Product selected for the account..");
				LOG.info("Invoking aercloud services...");
				
				createAercloudAccount(account);
				LOG.info("Aercloud Account Creation flow completed for the account");				
			}
			
			ObjectMapper mapper = new ObjectMapper();
			String accountJson = mapper.writeValueAsString(account);
			builder.entity(accountJson);

			LOG.info("Create Account Response: " + accountJson);
		} catch (AccountException e) {
			LOG.error("AccountException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (DuplicateAccountException e) {
			LOG.error("DuplicateAccountException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (SubAccountException e) {
			LOG.error("SubAccountException occured :", e);
			return Response.status(Status.PRECONDITION_FAILED).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("createNewAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error wshile creating account").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewAccount: " + response);
		return response;
	}

	private void createAercloudAccount(Account account) {
		long accountId = account.getAccountId();
		String apiKey = account.getApiKey();
		
		LOG.info("Creating aercloud account for the new account id: "+accountId);
		
		AerCloudResponse response = aerCloudClient.createAerCloudAccount(accountId, apiKey);
		
		if(response.isOK())
		{
			LOG.info("Aercloud account created successfully the new account id: "+accountId);
			
			LOG.info("Creating Aercloud Containers and Data Models...");
			
			// Create data model/container and set up device in aercloud
			aerCloudClient.createAerCloudDefaultDataModel(accountId, apiKey);
			aerCloudClient.createAerCloudDefaultContainer(accountId, apiKey);
			aerCloudClient.createAerCloudAlertDataModel(accountId, apiKey);
			aerCloudClient.createAerCloudAlertContainer(accountId, apiKey);
			aerCloudClient.createAerCloudEventStreamDataModel(accountId, apiKey);
			aerCloudClient.createAerCloudEventStreamContainer(accountId, apiKey);
			
			LOG.info("Persisting device...");
			aerCloudClient.persistAerCloudDevice(accountId, apiKey);
			
			LOG.info("Provisioning aercloud device in aeradmin...");
			
			// Provision device in AerAdmin
			aerCloudClient.provisionAerCloudDevice(accountId, apiKey);

			LOG.info("Aercloud account creation flow completed");	
		}		
		else
		{
			LOG.warn("Aercloud account creation failed with response code: "+response.getResponseCode());			
		}
	}
	
	private void updateAercloudAccount(Account account) {		
		LOG.info("Validating if aer cloud account exists for account id: "+account.getAccountId());
		AerCloudResponse response = aerCloudClient.fetchAerCloudAccount(account.getAccountId());
		
		// If there is no aercloud account for this account id, create it
		if(!response.isOK())
		{
			createAercloudAccount(account);
		}
		// Aercloud account exists for this account id
		else
		{
			LOG.warn("Aercloud account already exists for account id: "+account.getAccountId());	
		}
	}

	private void deleteAercloudAccount(long accountId) {		
		LOG.info("Validating if aer cloud account exists for account id: "+accountId);
		AerCloudResponse response = aerCloudClient.fetchAerCloudAccount(accountId);
		
		if(response.isOK())
		{
			LOG.info("Account found in aercloud for account id: "+accountId+". Deleting data models and containers..");
			
			// Delete data model/container set up in aercloud for this accountId
			aerCloudClient.deleteAerCloudDefaultContainer(accountId);
			aerCloudClient.deleteAerCloudDefaultDataModel(accountId);
			aerCloudClient.deleteAerCloudAlertsContainer(accountId);
			aerCloudClient.deleteAerCloudAlertsDataModel(accountId);
			aerCloudClient.deleteAerCloudEventStreamContainer(accountId);
			aerCloudClient.deleteAerCloudEventStreamDataModel(accountId);
			
			LOG.info("Deleting Aercloud account...");
			
			// Delete Aercloud account
			response = aerCloudClient.deleteAerCloudAccount(accountId);
			
			if(response.isOK())
			{
				LOG.info("Aercloud account deleted successfully for account id: "+accountId);
			}		
			else
			{
				LOG.warn("Aercloud account deletion failed with response code: "+response.getResponseCode());			
			}
		}
		else
		{
			LOG.warn("Aercloud account not found for account id: "+accountId);	
		}
	}

	/**
	 * Update an exisiting account for an operator based on the information 
	 * specified in the {@link UpdateAccountRequest} by the client
	 * 
	 * The request accepts the account name, contacts information, billing details 
	 * and list of products enrolled for this account
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * 
	 * @param request The update account requested by the user client
	 * 
	 * @return The Updated Account serialized back to the client
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{accountId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateAccount(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@Valid UpdateAccountRequest request) {

		LOG.info("Received request to update account with account id: " + accountId + " under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String userId = request.getUserId();
		String accountName = request.getAccountName();
		Contact primaryContact = request.getPrimaryContact();
		List<Contact> additionalContacts = request.getAdditionalContacts();
		BillingDetails billingDetails = request.getBillingDetails();
		List<String> productIds = request.getProductIds();
		AccountStatus status = request.getStatus();
		ApprovalStatus approvalStatus = request.getApprovalStatus();
		String parentAccountId = request.getParentAccountId();
		String carrierAccountId = request.getCarrierAccountId();
		String netSuiteId = request.getNetSuiteId();
		Region region = request.getRegion();
		AccountType accountType = request.getType();
		InvoiceAddress invoiceAddress = request.getInvoiceAddress();
		String parentLabel = request.getParentLabel();
		BillableStatus billableStatus = request.getBillableStatus();
		String specialInstructions = request.getSpecialInstructions();
		String tags = request.getTags();
		int includedAlertProfiles = request.getIncludedAlertProfiles();
		String verticalId = request.getVerticalId();
		int maxPortalAccounts = request.getMaxPortalAccounts();
		int maxReportHistory = request.getMaxReportHistory();
		int maxScheduledReports = request.getMaxScheduledReports();
		String[] serviceImpairmentEmail = request.getServiceImpairmentEmail();
		String[] myAlertsEmail = request.getMyAlertsEmail();
		String[] scheduledReportsEmail = request.getScheduledReportsEmail();
        String[] supportContactEmails = request.getSupportContactEmails();
		String webAddress = request.getWebAddress();

		Date requestedDate = new Date();
        
        String distributorId = request.getDistributorId();

		try {
			LOG.info("Calling AccountDAO.updateAccount()");

			Account account = accountDAO.updateAccount(operatorId, accountId, accountName, region, accountType, status, approvalStatus, invoiceAddress,
					productIds, parentAccountId, parentLabel, carrierAccountId, primaryContact, additionalContacts, webAddress,	billableStatus, billingDetails, 
					netSuiteId, specialInstructions, tags, includedAlertProfiles, verticalId, maxPortalAccounts, maxScheduledReports, maxReportHistory, 
					serviceImpairmentEmail, myAlertsEmail, scheduledReportsEmail, supportContactEmails, requestedDate, userId, request.getAccountOverageBucket(),
					request.getAgentId(), distributorId, request.getAccountType(), request.isEnableCreateWholesale(), request.getWholesaleAccountId(), request.getDistributor(), request.getCreditLimit());

			if(oaUtils.hasAerCloud(productIds))
			{
				LOG.info("Aercloud Product selected for the account..");
				LOG.info("Invoking aercloud services...");
				
				updateAercloudAccount(account);
				LOG.info("Aercloud Account Creation flow completed for the account");				
			}
			else
			{
				LOG.info("Aercloud Product has been de-selected for the account..");
				LOG.info("Invoking aercloud services...");
				
				deleteAercloudAccount(account.getAccountId());
				LOG.info("Aercloud Account Deletion flow completed for the account");		
			}
						
			ObjectMapper mapper = new ObjectMapper();

			String accountJson = mapper.writeValueAsString(account);
			builder.entity(accountJson);

			LOG.info("Update Account Response: " + accountJson);
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (DuplicateAccountException e) {
			LOG.error("DuplicateAccountException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		}catch (AccountMappingException e) {
			LOG.error("AccountMappingException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (AccountException e) {
			LOG.error("AccountException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (SubAccountException e) {
			LOG.error("SubAccountException occured :", e);
			return Response.status(Status.PRECONDITION_FAILED).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while updating account").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateAccount: " + response);
		return response;
	}
	
	/**
	 * Update an exisiting account for an operator based on the information 
	 * specified in the {@link UpdateAccountRequest} by the client
	 * 
	 * The request accepts the account name, contacts information, billing details 
	 * and list of products enrolled for this account
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param request The update account requested by the user client
	 * 
	 * @return The Updated Account serialized back to the client
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{accountId}/status")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateAccountStatus(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@Valid UpdateAccountStatusRequest request) {

		LOG.info("Received request to update account status with account id: " + accountId + " under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String userId = request.getUserId();
		AccountStatus status = request.getStatus();
		ApprovalStatus approvalStatus = request.getApprovalStatus();
		
		Date requestedDate = new Date();

		try {
			LOG.info("Calling AccountDAO.updateAccountStatus()");

			// Clean up aercloud account
			if(status == AccountStatus.SUSPENDED)
			{
				LOG.info("Cleaning up Aercloud account");
				deleteAercloudAccount(Integer.parseInt(accountId));
			}

			boolean success = accountDAO.updateAccountStatus(operatorId, accountId, status, approvalStatus, requestedDate, userId);
			
			ObjectMapper mapper = new ObjectMapper();

			String accountStatusJson = mapper.writeValueAsString(success);
			builder.entity(accountStatusJson);

			LOG.info("Update Account status Response: " + accountStatusJson);
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (AccountDeactivationException e) {
			LOG.error("AccountDeactivationException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (AccountException e) {
			LOG.error("AccountException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateAccountStatus Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while updating account status").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateAccountStatus: " + response);
		return response;
	}

	/**
	 * Delete an existing account identified by account id and request authorized user
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param userId The user id of the user who has sent the request
	 * 
	 * @return true if the delete is success otherwise false
	 */
	@DELETE
	@Path("/{accountId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response deleteAccount(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@QueryParam("userId") @Email(message = "{userId.email}") final String userId) {
		LOG.info("Received request to delete account with accountId: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Date requestedDate = new Date();

		try {
			// Fetch from database
			boolean success = accountDAO.deleteAccount(operatorId, accountId, requestedDate, userId);

			if (!success) {
				LOG.info("deleteAccount failed for account id: " + accountId);
				return Response.status(Status.NOT_FOUND).entity("deleteAccount failed for account id: " + accountId).build();
			} else {
				LOG.info("Account with account id: " + accountId + " deleted successfully");

				ObjectMapper mapper = new ObjectMapper();
				String deleteJson = mapper.writeValueAsString(success);

				builder.entity(deleteJson);
			}
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (AccountException e) {
			LOG.error("AccountException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("deleteAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while deleting account").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from deleteAccount: " + response);
		return response;
	}

	/**
	 * Fetches the child accounts for a specified account.
	 * 
	 * @param operatorId The Operator id
	 * @param accountId The Account id for which the child accounts are retreived
	 * 
	 * @return The list of child accounts
	 */
	@GET
	@Path("/{accountId}/children")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "ACCOUNT", "OPERATOR"})
	public Response getChildAccounts(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId) {
		LOG.info("Received request to fetch children for account with accountId: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			// Fetch from database
			List<Account> childAccounts = accountDAO.getChildAccounts(operatorId, accountId);

			if (childAccounts != null) {
				LOG.info("fetched children successfully for accountId: " + accountId);

				ObjectMapper mapper = new ObjectMapper();
				String childAccountsJson = mapper.writeValueAsString(childAccounts);

				builder.entity(childAccountsJson);
			} else {
				LOG.info("No children found for accountId: " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No children found for account id: " + accountId).build();
			}
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getContactsForAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting account contacts").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getContactsForAccount: " + response);
		return response;
	}

	/**
	 * Fetches the contact information for a specific account
	 * 
	 * @param operatorId the operator id
	 * @param accountId the account id
	 * 
	 * @return The List of contacts in the account
	 */
	@GET
	@Path("/{accountId}/contacts")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "ACCOUNT", "OPERATOR"})
	public Response getContactsForAccount(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId) {
		LOG.info("Received request to fetch contacts for account with accountId: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			// Fetch from database
			List<Contact> contacts = accountDAO.getContactsForAccount(operatorId, accountId);

			if (contacts != null) {
				LOG.info("fetched contacts successfully for accountId: " + accountId);

				ObjectMapper mapper = new ObjectMapper();
				String contactsJson = mapper.writeValueAsString(contacts);

				builder.entity(contactsJson);
			} else {
				LOG.info("No contacts found for accountId: " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No contacts found for account id: " + accountId).build();
			}
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getContactsForAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting account contacts").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getContactsForAccount: " + response);
		return response;
	}
    
    @POST
    @Path("/sendNotification")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response sendNotification(
            @PathParam("operatorId") @ValidOperator final String operatorId,
            @Valid NotificationRequest request) {
        request.setOperatorId(operatorId);
        LOG.info("Request to send notification is received.");
        LOG.info("Request details: " + request.toString());
        ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
        Event event = request.getEvent();        
        eventDispatcher.propogateEvent(event, operatorId, request);
        if (request.getStatus() != 0) {
            builder = Response.serverError().type(MediaType.APPLICATION_JSON);
        }
        Response response = builder.build();
        return response;
    }
}
