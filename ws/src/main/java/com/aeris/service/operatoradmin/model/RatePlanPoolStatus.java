package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum RatePlanPoolStatus implements Serializable {
	PENDING(2), APPROVED(1), EXPIRED(3);
	int value;

	RatePlanPoolStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static RatePlanPoolStatus fromValue(int value) {
		RatePlanPoolStatus[] statuses = values();

		for (RatePlanPoolStatus ratePlanPoolStatus : statuses) {
			if (ratePlanPoolStatus.getValue() == value) {
				return ratePlanPoolStatus;
			}
		}

		return null;
	}

	public static RatePlanPoolStatus fromName(String value) {
		RatePlanPoolStatus[] statuses = values();

		for (RatePlanPoolStatus ratePlanPoolStatus : statuses) {
			if (ratePlanPoolStatus.name().equalsIgnoreCase(value)) {
				return ratePlanPoolStatus;
			}
		}

		return null;
	}
}
