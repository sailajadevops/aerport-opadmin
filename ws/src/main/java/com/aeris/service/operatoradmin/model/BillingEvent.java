package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum BillingEvent implements Serializable {
	SMS(1), GPRS(2), PACKET(3), VOICE(5);
	int value;

	BillingEvent(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int value) {
		BillingEvent[] events = values();

		for (BillingEvent billingEvent : events) {
			if (billingEvent.getValue() == value) {
				return true;
			}
		}

		return false;
	}

	public static BillingEvent fromValue(int value) {
		BillingEvent[] events = values();

		for (BillingEvent billingEvent : events) {
			if (billingEvent.getValue() == value) {
				return billingEvent;
			}
		}

		return null;
	}

	public static BillingEvent fromName(String value) {
		BillingEvent[] events = values();

		for (BillingEvent billingEvent : events) {
			if (billingEvent.name().equalsIgnoreCase(value)) {
				return billingEvent;
			}
		}

		return null;
	}
}
