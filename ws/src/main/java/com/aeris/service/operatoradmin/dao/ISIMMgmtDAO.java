package com.aeris.service.operatoradmin.dao;

import com.aeris.service.operatoradmin.exception.DAOException;
import com.aeris.service.operatoradmin.payload.AssignSIMsToAccountRequest;

public interface ISIMMgmtDAO {

    void assignSIMsToAccount(String operatorId, String accountId, AssignSIMsToAccountRequest request) throws DAOException;

    void assignSIMsToAccountFromSIMPack(String operatorId, String accountId, String simPackId, String userId, long orderId) throws DAOException;
}
