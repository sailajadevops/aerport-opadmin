package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SMSFormat;
import com.google.inject.Inject;

public class GetSMSFormatsStoredProcedure extends AbstractProcedureCall<List<SMSFormat>> {

	private static final String SQL_GET_SMS_FORMATS = "common_util_pkg.get_sms_format";

	@Inject
	public GetSMSFormatsStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_SMS_FORMATS, true);
		
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<SMSFormat> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<SMSFormat> smsFormats = createSMSFormatList(cursor);

		return smsFormats;
	}

	private List<SMSFormat> createSMSFormatList(List<Map<String, Object>> cursor) {
		List<SMSFormat> smsFormats = new ArrayList<SMSFormat>();

		for (Map<String, Object> row : cursor) {
			SMSFormat smsFormat = new SMSFormat();  

			String formatCode = (String) row.get("format_code");

			if (NumberUtils.isNumber(formatCode)) {
				smsFormat.setFormatCode(Integer.parseInt(formatCode));
			}

			String formatString = (String) row.get("string");

			if (StringUtils.isNotEmpty(formatString)) {
				smsFormat.setFormatString(formatString);
			}
			
			smsFormats.add(smsFormat);
		}

		return smsFormats;
	}
}
