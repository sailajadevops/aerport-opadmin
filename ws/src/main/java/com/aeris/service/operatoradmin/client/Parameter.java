package com.aeris.service.operatoradmin.client;

public class Parameter {
	private String name;
	private String type;
	private MetaInfo metainfo;
	private String isIndexed;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public MetaInfo getMetainfo() {
		return metainfo;
	}

	public void setMetainfo(MetaInfo metainfo) {
		this.metainfo = metainfo;
	}

	public String getIsIndexed() {
		return isIndexed;
	}

	public void setIsIndexed(String isIndexed) {
		this.isIndexed = isIndexed;
	}
}
