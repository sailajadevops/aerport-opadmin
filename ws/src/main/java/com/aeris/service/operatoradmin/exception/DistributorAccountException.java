package com.aeris.service.operatoradmin.exception;

public class DistributorAccountException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DistributorAccountException(String s) {
		super(s);
	}

	public DistributorAccountException(String ex, Throwable t) {
		super(ex, t);
	}
}
