package com.aeris.service.operatoradmin.dao;

import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.AccountDeactivationException;
import com.aeris.service.operatoradmin.exception.AccountException;
import com.aeris.service.operatoradmin.exception.AccountMappingException;
import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.ApiKeyException;
import com.aeris.service.operatoradmin.exception.DuplicateAccountException;
import com.aeris.service.operatoradmin.exception.SubAccountException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.AccountOverageBucket;
import com.aeris.service.operatoradmin.model.AccountStatus;
import com.aeris.service.operatoradmin.model.AccountType;
import com.aeris.service.operatoradmin.model.ApprovalStatus;
import com.aeris.service.operatoradmin.model.BillableStatus;
import com.aeris.service.operatoradmin.model.BillingDetails;
import com.aeris.service.operatoradmin.model.Contact;
import com.aeris.service.operatoradmin.model.Distributor;
import com.aeris.service.operatoradmin.model.InvoiceAddress;
import com.aeris.service.operatoradmin.model.Region;
import com.aeris.service.operatoradmin.payload.CreateDistributorRequest;
import com.aeris.service.operatoradmin.payload.UpdateDistributorRequest;

public interface IAccountDAO {
	List<Account> getAllAccounts(String operatorId, String start, String count, List<String> accountIds, boolean wholesaleManagementEnabled, AccountStatus... accountStatusList);
	List<Account> getRetailAccounts(String operatorId, String start, String count, String wholesaleAccountId);

	Account getAccount(String operatorId, String accountId);

	List<Account> getChildAccounts(String operatorId, String accountId) throws AccountNotFoundException, AccountException;

	List<Contact> getContactsForAccount(String operatorId, String accountId) throws AccountNotFoundException;

	Account createNewAccount(String operatorId, String accountName, Region region, AccountType accountType, AccountStatus status, ApprovalStatus approvalStatus,
			InvoiceAddress invoiceAddress, List<String> productIds, String parentAccountId, String parentLabel, String carrierAccountId, Contact primaryContact,
			List<Contact> additionalContacts, String webAddress, BillableStatus billableStatus, BillingDetails billingDetails, String netSuiteId, String specialInstructions, String tags,
			int includedAlertProfiles, String verticalId, int maxPortalAccounts, int maxScheduledReports, int maxReportHistory, String[] serviceImpairmentEmail, String[] myAlertsEmail,
			String[] scheduledReportsEmail, String[] supportContactEmails, Date requestedDate, String requestedUser, List<AccountOverageBucket> accountOverageBucket,
			String agentId, String distributorId, String actType, boolean enableCreateWholesale, String wholesaleAccountId, Distributor distRequest, long creditLimit) throws AccountException, SubAccountException, ApiKeyException, DuplicateAccountException;

	Account updateAccount(String operatorId, String accountId, String accountName, Region region, AccountType accountType, AccountStatus status, ApprovalStatus approvalStatus,
			InvoiceAddress invoiceAddress, List<String> productIds, String parentAccountId, String parentLabel, String carrierAccountId, Contact primaryContact,
			List<Contact> additionalContacts, String webAddress, BillableStatus billableStatus, BillingDetails billingDetails, String netSuiteId, String specialInstructions, String tags,
			int includedAlertProfiles, String verticalId, int maxPortalAccounts, int maxScheduledReports, int maxReportHistory, String[] serviceImpairmentEmail, String[] myAlertsEmail,
			String[] scheduledReportsEmail, String[] supportContactEmails, Date requestedDate, String requestedUser,
			List<AccountOverageBucket> accountOverageBucket, String agentId, String distributorId, String acctType, boolean enableCreateWholesale, String wholesaleAccountId, Distributor distRequest, long creditLimit) throws AccountNotFoundException, AccountException, SubAccountException, DuplicateAccountException,
			AccountMappingException;

	boolean updateAccountStatus(String operatorId, String accountId, AccountStatus status, ApprovalStatus approvalStatus, Date requestedDate, String requestedUser)
			throws AccountNotFoundException, AccountException, AccountDeactivationException;

	boolean deleteAccount(String operatorId, String accountId, Date requestedDate, String requestedUser) throws AccountNotFoundException, AccountException;

	List<Account> getSelectedAccounts(String operatorId, String... accountIds);
}