package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum CardType implements Serializable {
	AMERICAN_EXPRESS(1, "American Express"), MASTER_CARD(1, "Master Card"), VISA(3, "VISA"), MAESTRO(4, "Maestro"), DISCOVER(5, "Maestro");
	int code;
	String value;

	CardType(int code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public int getCode() {
		return code;
	}

	public static boolean isValid(String value) {
		CardType[] cardTypes = values();

		for (CardType cardType : cardTypes) {
			if (cardType.getValue().equalsIgnoreCase(value)) {
				return true;
			}
		}

		return false;
	}

	public static CardType fromValue(String value) {
		CardType[] cardTypes = values();

		for (CardType cardType : cardTypes) {
			if (cardType.getValue().equalsIgnoreCase(value)) {
				return cardType;
			}
		}

		return null;
	}
}
