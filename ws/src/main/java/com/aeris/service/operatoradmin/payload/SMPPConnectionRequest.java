package com.aeris.service.operatoradmin.payload;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SMPPConnectionRequest {
	@NotNull(message = "{connectionLimit.notnull}")
	@Pattern(regexp = "[0-9]+", message = "{connectionLimit.number}")
	private String connectionLimit;

	public String getConnectionLimit() {
		return connectionLimit;
	}

	public void setConnectionLimit(String connectionLimit) {
		this.connectionLimit = connectionLimit;
	}
	
	
}
