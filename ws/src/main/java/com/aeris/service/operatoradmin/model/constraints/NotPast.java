package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Date;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import com.aeris.service.operatoradmin.utils.ApplicationUtils;
import com.aeris.service.operatoradmin.utils.Constants;

@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD, PARAMETER, TYPE})
@Documented
@Constraint(validatedBy = {NotPast.DateValidator.class}) 
public @interface NotPast {
	String message() default "{javax.validation.constraints.NotPast.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class DateValidator implements ConstraintValidator<NotPast, Date> {

		@Override
		public void initialize(final NotPast enumClazz) {
			// Ignore
		}

		public boolean isValid(final Date date, final ConstraintValidatorContext constraintValidatorContext) {
			//null values are valid
			if ( date == null ) {
				return true;
			}
			
			Date currDate = ApplicationUtils.getTodaysStartTimeStamp(Constants.TIMEZONE_GMT);
			
			return date.equals(currDate) || date.after(currDate);
		}
	}
}
