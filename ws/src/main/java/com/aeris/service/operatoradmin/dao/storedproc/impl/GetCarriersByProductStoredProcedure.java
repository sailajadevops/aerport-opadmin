package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Carrier;
import com.google.inject.Inject;

public class GetCarriersByProductStoredProcedure extends AbstractProcedureCall<List<Carrier>> {

	private static final String SQL_GET_CARRIERS_BY_PRODUCT = "common_util_pkg.get_ws_product_carrier";

	@Inject
	public GetCarriersByProductStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, SQL_GET_CARRIERS_BY_PRODUCT, true);
		
		registerParameter(new InParameter("p_product_id_i", Types.INTEGER));
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Carrier> execute(Object... input) throws StoredProcedureException {

		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;

		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<Carrier> carriers = createCarrierList(cursor);

		return carriers;
	}

	private List<Carrier> createCarrierList(List<Map<String, Object>> cursor) {
		List<Carrier> carriers = new ArrayList<Carrier>();

		for (Map<String, Object> row : cursor) {
			Carrier carrier = new Carrier();

			String carrierId = (String) row.get("carrier_id");

			if (NumberUtils.isNumber(carrierId)) {
				carrier.setCarrierId(Integer.parseInt(carrierId));
			}

			String carrierName = (String) row.get("carrier_name");

			if (StringUtils.isNotEmpty(carrierName)) {
				carrier.setCarrierName(carrierName);
			}

			String productId = (String) row.get("product_id");

			if (NumberUtils.isNumber(productId)) {
				carrier.setProductId(Integer.parseInt(productId));
			}
			
			carriers.add(carrier);
		}

		return carriers;
	}
}
