package com.aeris.service.operatoradmin.dao;

import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.SupplierSIMOrderException;
import com.aeris.service.operatoradmin.exception.SupplierSIMOrderNotFoundException;
import com.aeris.service.operatoradmin.model.IMSIRangeType;
import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.SupplierSIMOrder;

public interface ISupplierSIMOrderDAO {
	List<SupplierSIMOrder> getAllSIMOrders(String operatorId, int supplierId, String start, String count, OrderStatus... orderStatusList);

	SupplierSIMOrder getSIMOrderByOrderId(String operatorId, String orderId);

	SupplierSIMOrder createSIMOrder(String operatorId, int supplierId, long quantity, int simWarehouseId, int simFormatId, int productId, OrderStatus status,
			String iccidRange, String imsiRange, IMSIRangeType imsiRangeType, String iccidStart, String imsiStart, Date requestedDate, String requestedUser) 
			throws SupplierSIMOrderException;

	boolean updateSIMOrder(String operatorId, long orderId, OrderStatus status, long quantity, String iccidRange, String imsiRange, IMSIRangeType imsiRangeType, 
			String iccidStart, String imsiStart, Date requestedDate, String requestedUser) 
			throws SupplierSIMOrderException, SupplierSIMOrderNotFoundException;
	
	List<SupplierSIMOrder> getSIMOrdersBySupplierId(String operatorId, int supplierId);
	
	boolean deleteSIMOrder(String operatorId, long orderId, Date requestedDate, String requestedUser) throws SupplierSIMOrderException,
			SupplierSIMOrderNotFoundException;
}
