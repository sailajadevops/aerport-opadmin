package com.aeris.service.operatoradmin;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class OperatorAdminResponseFilter implements ContainerResponseFilter{
	@Override
	public void filter(ContainerRequestContext request, ContainerResponseContext response) throws IOException {		
		response.getHeaders().add("Access-Control-Allow-Origin", "*");
		response.getHeaders().add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
		response.getHeaders().add("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
		response.getHeaders().add("Access-Control-Max-Age", "1728000");		
	}
}
