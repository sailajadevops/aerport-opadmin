package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

/**
 * Role Enum class for various operator-admin access
 * 
 * @author saurabh.sharma@aeris.net	
 *
 */
public enum Role implements Serializable {
	
	OPERATOR_WR(false), OPERATOR(true), ACCOUNT(true), PLATFORM_ADMIN(true), PING_ONLY(false);
	boolean requireAuthorization;
	
	

	Role(boolean value) {
		this.requireAuthorization = value;
	}

	public boolean getValue() {
		return requireAuthorization;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static Role fromValue(boolean value) {
		Role[] statuses = values();

		for (Role accountStatus : statuses) {
			if (accountStatus.getValue() == value) {
				return accountStatus;
			}
		}

		return null;
	}

	public static Role fromName(String value) {
		Role[] statuses = values();

		for (Role accountStatus : statuses) {
			if (accountStatus.name().equalsIgnoreCase(value)) {
				return accountStatus;
			}
		}

		return null;
	}
}
