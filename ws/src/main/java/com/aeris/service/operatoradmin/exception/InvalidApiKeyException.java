package com.aeris.service.operatoradmin.exception;

public class InvalidApiKeyException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public InvalidApiKeyException(String s) {
		super(s);
	}

	public InvalidApiKeyException(String ex, Throwable t) {
		super(ex, t);
	}
}
