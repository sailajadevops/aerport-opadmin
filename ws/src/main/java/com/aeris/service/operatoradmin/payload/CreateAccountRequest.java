package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.AccountOverageBucket;
import com.aeris.service.operatoradmin.model.AccountStatus;
import com.aeris.service.operatoradmin.model.AccountType;
import com.aeris.service.operatoradmin.model.ApprovalStatus;
import com.aeris.service.operatoradmin.model.BillableStatus;
import com.aeris.service.operatoradmin.model.BillingDetails;
import com.aeris.service.operatoradmin.model.Contact;
import com.aeris.service.operatoradmin.model.Distributor;
import com.aeris.service.operatoradmin.model.InvoiceAddress;
import com.aeris.service.operatoradmin.model.Region;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@XmlRootElement
public class CreateAccountRequest implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;

	@Email(message = "{userId.email}")
	private String userId;

	private String password;

	@NotNull(message = "{accountName.notnull}")
	@Size(min = 1, max = 60, message = "{accountName.size}")
	private String accountName;

	@NotNull(message = "{includedAlertProfiles.notnull}")
	@Min(value = 1, message = "{includedAlertProfiles.number}")
	private int includedAlertProfiles;

	@NotNull(message = "{primaryContact.notnull}")
	@Valid
	private Contact primaryContact;

	@Valid
	private List<Contact> additionalContacts;

	@NotNull(message = "{productIds.notnull}")
	@Size(min = 1, message = "{productIds.size}")
	private List<String> productIds;

	@Valid
	private InvoiceAddress invoiceAddress;

	private BillingDetails billingDetails;

	@Enumeration(value = AccountType.class, message = "{type.enum}")
	private AccountType type = AccountType.APP_DEVELOPER;

	@Enumeration(value = AccountStatus.class, message = "{status.enum}")
	private AccountStatus status = AccountStatus.TRIAL;
	
	@Enumeration(value = ApprovalStatus.class, message = "{status.enum}")
	private ApprovalStatus approvalStatus = ApprovalStatus.PENDING;

	@Enumeration(value = Region.class, message = "{region.enum}")
	private Region region;

	private String parentAccountId;

	private String parentLabel;

	@NotNull(message = "{netSuiteId.notnull}")
	private String netSuiteId;

	@Enumeration(value = BillableStatus.class, message = "{billableStatus.enum}")
	private BillableStatus billableStatus;

	private String carrierAccountId;

	private String specialInstructions;

	private String tags;

	private String webAddress;
		
	@Pattern(regexp = "[0-9]+", message = "{verticalId.number}")
	@NotNull(message="{verticalId.notnull}")
	private String verticalId;

	private int maxPortalAccounts;

	private int maxScheduledReports;

	private int maxReportHistory;

	private String[] serviceImpairmentEmail;

	private String[] myAlertsEmail;

	private String[] scheduledReportsEmail;
    
    private String[] supportContactEmails;
    
    private String agentId;
    
    private String distributorId;
    
    private String accountType;
    private boolean enableCreateWholesale;
    private String wholesaleAccountId;
    private Distributor distributor;
    
    //USCC-28 : added new field creditLimit
    private long creditLimit;

	public long getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(long creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Distributor getDistributor() {
		return distributor;
	}

	public void setDistributor(
			Distributor distributor) {
		this.distributor = distributor;
	}

	@Valid
    private List<AccountOverageBucket> accountOverageBucket;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getIncludedAlertProfiles() {
		return includedAlertProfiles;
	}

	public void setIncludedAlertProfiles(int includedAlertProfiles) {
		this.includedAlertProfiles = includedAlertProfiles;
	}

	public Contact getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(Contact primaryContact) {
		this.primaryContact = primaryContact;
	}

	public List<Contact> getAdditionalContacts() {
		return additionalContacts;
	}

	public void setAdditionalContacts(List<Contact> additionalContacts) {
		this.additionalContacts = additionalContacts;
	}

	public List<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<String> productIds) {
		this.productIds = productIds;
	}

	public InvoiceAddress getInvoiceAddress() {
		return invoiceAddress;
	}

	public void setInvoiceAddress(InvoiceAddress invoiceAddress) {
		this.invoiceAddress = invoiceAddress;
	}

	public BillingDetails getBillingDetails() {
		return billingDetails;
	}

	public void setBillingDetails(BillingDetails billingDetails) {
		this.billingDetails = billingDetails;
	}

	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public ApprovalStatus getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(ApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public String getParentAccountId() {
		return parentAccountId;
	}

	public void setParentAccountId(String parentAccountId) {
		this.parentAccountId = parentAccountId;
	}

	public String getParentLabel() {
		return parentLabel;
	}

	public void setParentLabel(String parentLabel) {
		this.parentLabel = parentLabel;
	}

	public String getNetSuiteId() {
		return netSuiteId;
	}

	public void setNetSuiteId(String netSuiteId) {
		this.netSuiteId = netSuiteId;
	}

	public BillableStatus getBillableStatus() {
		return billableStatus;
	}

	public void setBillableStatus(BillableStatus billableStatus) {
		this.billableStatus = billableStatus;
	}

	public String getCarrierAccountId() {
		return carrierAccountId;
	}

	public void setCarrierAccountId(String carrierAccountId) {
		this.carrierAccountId = carrierAccountId;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public String getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(String verticalId) {
		this.verticalId = verticalId;
	}

	public int getMaxPortalAccounts() {
		return maxPortalAccounts;
	}

	public void setMaxPortalAccounts(int maxPortalAccounts) {
		this.maxPortalAccounts = maxPortalAccounts;
	}

	public int getMaxScheduledReports() {
		return maxScheduledReports;
	}

	public void setMaxScheduledReports(int maxScheduledReports) {
		this.maxScheduledReports = maxScheduledReports;
	}

	public int getMaxReportHistory() {
		return maxReportHistory;
	}

	public void setMaxReportHistory(int maxReportHistory) {
		this.maxReportHistory = maxReportHistory;
	}

	public String[] getServiceImpairmentEmail() {
		return serviceImpairmentEmail;
	}

	public void setServiceImpairmentEmail(String[] serviceImpairmentEmail) {
		this.serviceImpairmentEmail = serviceImpairmentEmail;
	}

	public String[] getMyAlertsEmail() {
		return myAlertsEmail;
	}

	public void setMyAlertsEmail(String[] myAlertsEmail) {
		this.myAlertsEmail = myAlertsEmail;
	}

	public String[] getScheduledReportsEmail() {
		return scheduledReportsEmail;
	}

	public void setScheduledReportsEmail(String[] scheduledReportsEmail) {
		this.scheduledReportsEmail = scheduledReportsEmail;
	}

    public String[] getSupportContactEmails() {
        return supportContactEmails;
    }

    public void setSupportContactEmails(String[] supportContactEmails) {
        this.supportContactEmails = supportContactEmails;
    }

	public List<AccountOverageBucket> getAccountOverageBucket() {
		return accountOverageBucket;
	}

	public void setAccountOverageBucket(List<AccountOverageBucket> accountOverageBucket) {
		this.accountOverageBucket = accountOverageBucket;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public boolean isEnableCreateWholesale() {
		return enableCreateWholesale;
	}

	public void setEnableCreateWholesale(boolean enableCreateWholesale) {
		this.enableCreateWholesale = enableCreateWholesale;
	}

	public String getWholesaleAccountId() {
		return wholesaleAccountId;
	}

	public void setWholesaleAccountId(String wholesaleAccountId) {
		this.wholesaleAccountId = wholesaleAccountId;
	}    
}
