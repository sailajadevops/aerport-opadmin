package com.aeris.service.operatoradmin.dao;

import com.aeris.service.operatoradmin.exception.DistributorDBException;
import com.aeris.service.operatoradmin.exception.DistributorException;
import com.aeris.service.operatoradmin.exception.DistributorNotFoundException;
import com.aeris.service.operatoradmin.exception.InvalidSIMPackException;
import com.aeris.service.operatoradmin.exception.SIMPackException;
import com.aeris.service.operatoradmin.model.SIMPack;
import com.aeris.service.operatoradmin.payload.CreateNeoSIMPacksRequest;
import com.aeris.service.operatoradmin.payload.CreateUniqueCodesRequest;
import com.aeris.service.operatoradmin.payload.CreateSIMPacksRequest;
import com.aeris.service.operatoradmin.payload.UpdateSIMPacksRequest;

import java.util.List;

public interface ISIMPackDAO {

    List<String> createUniqueCodes(CreateUniqueCodesRequest request) throws DistributorNotFoundException, SIMPackException, DistributorDBException;

    List<SIMPack> createSIMPacks(CreateSIMPacksRequest request) throws SIMPackException, DistributorDBException, DistributorException;

    boolean updateSIMPacks(UpdateSIMPacksRequest request) throws SIMPackException, DistributorDBException;

    List<SIMPack> getAllSIMPacks(String distributorAccountId) throws SIMPackException;

    SIMPack getSIMPack(String simPackId) throws SIMPackException;

    SIMPack updateSIMPack(UpdateSIMPacksRequest request);

    List<SIMPack> createNeoSIMPacks(CreateNeoSIMPacksRequest request) throws SIMPackException, DistributorDBException;

	void updateSIMPacksFromCSV(String operatorId, String distributorId, List<String[]> rows, String email) throws SIMPackException, DistributorDBException;

	boolean deleteSIMPack(long distributorAccountId, long simPackId) throws SIMPackException, InvalidSIMPackException;
}
