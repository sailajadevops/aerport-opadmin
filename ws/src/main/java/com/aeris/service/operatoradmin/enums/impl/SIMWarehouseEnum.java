package com.aeris.service.operatoradmin.enums.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.ISIMWarehouseEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SIMWarehouse;
import com.google.inject.Singleton;

/**
 * Resource Definition Cache used for lookup purposes 
 * 
 * @author SP00125222
 */
@Singleton
public class SIMWarehouseEnum implements ISIMWarehouseEnum, IEventListener {
	private Logger LOG = LoggerFactory.getLogger(SIMWarehouseEnum.class);
	private Cache<String, SIMWarehouse> cache;
	private Cache<String, SIMWarehouse> cacheById;
	
	@Inject
	private IStoredProcedure<List<SIMWarehouse>> getWarehousesStoredProcedure;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "SIMWarehouseCache/name") Cache<String, SIMWarehouse> cache, 
			@Hazelcast(cache = "SIMWarehouseCache/id") Cache<String, SIMWarehouse> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;		
		loadResources();
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		try {
			// Call Get_Product Stored Procedure
			List<SIMWarehouse> warehouses = getWarehousesStoredProcedure.execute();	
			
			for(SIMWarehouse warehouse: warehouses) 
			{
				cache.put(warehouse.getWarehouseName(), warehouse);
				cacheById.put(String.valueOf(warehouse.getWarehouseId()), warehouse);
			}
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during getWarehousesStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		}
	}
	
	@Override
	public SIMWarehouse getSIMWarehouseByName(String name) {
		return cache.get(name);
	}

	@Override
	public SIMWarehouse getSIMWarehouseById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<SIMWarehouse> getAllSIMWarehouses() {
		return new ArrayList<SIMWarehouse>(cacheById.getValues());
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
