package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.ITierCriteriaEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.TierCriteria;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * TierCriteria Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class TierCriteriaEnum implements ITierCriteriaEnum, IEventListener  {
	private Logger LOG = LoggerFactory.getLogger(TierCriteriaEnum.class);
	
	static final String SQL_GET_TIER_CRITERION = "select tier_criteria_id, tier_criteria_name from tier_criteria order by tier_criteria_id";
	
	private Cache<String, TierCriteria> cache;
	private Cache<String, TierCriteria> cacheById;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "TierCriteriaCache/name") Cache<String, TierCriteria> cache, 
			@Hazelcast(cache = "TierCriteriaCache/id") Cache<String, TierCriteria> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_TIER_CRITERION);
			
			while (rs.next()) {
				TierCriteria tierCriteria = new TierCriteria();
				
				tierCriteria.setCriteriaId(rs.getInt("tier_criteria_id"));
				tierCriteria.setCriteriaName(rs.getString("tier_criteria_name"));
				
				cache.put(tierCriteria.getCriteriaName(), tierCriteria);
				cacheById.put(String.valueOf(tierCriteria.getCriteriaId()), tierCriteria);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}	

	@Override
	public TierCriteria getTierCriteriaByName(String name) {
		return cache.get(name);
	}

	@Override
	public TierCriteria getTierCriteriaById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<TierCriteria> getAllTierCriteria() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
