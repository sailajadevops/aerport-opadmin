package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class IncentiveDetails implements Serializable {
	private static final long serialVersionUID = -8762360042405366442L;

	private int incentiveUnitVolume;

	@Future
	private int incentiveEndDate;

	@Min(value = 0, message = "{incentivePercentage.min}")
	@Max(value = 100, message = "{incentivePercentage.max}")
	private int incentivePercentage;

	public int getIncentiveUnitVolume() {
		return incentiveUnitVolume;
	}

	public void setIncentiveUnitVolume(int incentiveUnitVolume) {
		this.incentiveUnitVolume = incentiveUnitVolume;
	}

	public int getIncentiveEndDate() {
		return incentiveEndDate;
	}

	public void setIncentiveEndDate(int incentiveEndDate) {
		this.incentiveEndDate = incentiveEndDate;
	}

	public int getIncentivePercentage() {
		return incentivePercentage;
	}

	public void setIncentivePercentage(int incentivePercentage) {
		this.incentivePercentage = incentivePercentage;
	}
}
