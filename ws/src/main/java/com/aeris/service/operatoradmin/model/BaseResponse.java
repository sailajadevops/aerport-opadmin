package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class BaseResponse implements Serializable {

	private static final long serialVersionUID = -7203733733671046306L;
	private int resultCode = -1;
	private String resultMessage;

	/**
	 * @return the resultCode
	 */
	public final int getResultCode() {
		return resultCode;
	}

	/**
	 * @param resultCode
	 *            the resultCode to set
	 */
	public final void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	/**
	 * @return the resultMessage
	 */
	public final String getResultMessage() {
		return resultMessage;
	}

	/**
	 * @param resultMessage
	 *            the resultMessage to set
	 */
	public final void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
