package com.aeris.service.operatoradmin.payload;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.APNConfiguration;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.SMPPConfiguration;
import com.aeris.service.operatoradmin.model.TechnologyServiceProfile;
import com.aeris.service.operatoradmin.model.ZoneServiceProfile;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@XmlRootElement
public class UpdateServiceProfileRequest {
	@NotNull(message="{serviceProfileName.notnull}")
	@Size(min=1, max=24, message="{serviceProfileName.size}")
	private String serviceProfileName;
	
	@NotNull(message="{serviceUserName.notnull}")
	@Size(min=1, max=30, message="{serviceUserName.size}")
	private String serviceUserName;
	
	@NotNull(message="{servicePassword.notnull}")
	@Size(min=1, max=30, message="{servicePassword.size}")
	private String servicePassword;
	
	@Size(min=1, max=30, message="{regexMOVoice.size}")
	private String regexMOVoice;
	
	@Email(message="{userId.email}")
	private String userId;
	
	@Pattern(regexp = "[0-9]+", message = "{productId.number}")
	@NotNull(message="{productId.notnull}")
	private String productId;

	@Pattern(regexp = "[0-9]+", message = "{serviceProviderId.number}")
	private String serviceProviderId;
	
	@Valid
	@NotNull(message="{enrolledServices.notnull}")
	private EnrolledServices services;
	
	@Valid
	private List<ZoneServiceProfile> zoneProfiles;
	
	private List<TechnologyServiceProfile> technologyProfiles;
	
	@Valid
	private SMPPConfiguration smppConfiguration;
	
	boolean smsNonAerisDevice;
	
	@Valid
	private APNConfiguration apnConfiguration;
	
    private String hssProfileId;
    private String pccProfileId;
    
    private int zoneSetId;
    
    private boolean notUsed;
    
    private String technology;
    
    private boolean validateCommPlan;
    
	public String getServiceProfileName() {
		return serviceProfileName;
	}
	
	public void setServiceProfileName(String serviceProfileName) {
		this.serviceProfileName = serviceProfileName;
	}

	public String getServiceUserName() {
		return serviceUserName;
	}

	public void setServiceUserName(String serviceUserName) {
		this.serviceUserName = serviceUserName;
	}

	public String getServicePassword() {
		return servicePassword;
	}

	public void setServicePassword(String servicePassword) {
		this.servicePassword = servicePassword;
	}

	public String getRegexMOVoice() {
		return regexMOVoice;
	}

	public void setRegexMOVoice(String regexMOVoice) {
		this.regexMOVoice = regexMOVoice;
	}

	public EnrolledServices getServices() {
		return services;
	}
	
	public void setServices(EnrolledServices services) {
		this.services = services;
	}
	
	public List<ZoneServiceProfile> getZoneProfiles() {
		return zoneProfiles;
	}

	public void setZoneProfiles(List<ZoneServiceProfile> zoneProfiles) {
		this.zoneProfiles = zoneProfiles;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getProductId() {
		return productId;
	}
	
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public String getServiceProviderId() {
		return serviceProviderId;
	}
	
	public void setServiceProviderId(String serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}
	
	public SMPPConfiguration getSmppConfiguration() {
		return smppConfiguration;
	}
	
	public void setSmppConfiguration(SMPPConfiguration smppConfiguration) {
		this.smppConfiguration = smppConfiguration;
	}
	
	public boolean isSmsNonAerisDevice() {
		return smsNonAerisDevice;
	}
	
	public void setSmsNonAerisDevice(boolean smsNonAerisDevice) {
		this.smsNonAerisDevice = smsNonAerisDevice;
	}
	
	public APNConfiguration getApnConfiguration() {
		return apnConfiguration;
	}
	
	public void setApnConfiguration(APNConfiguration apnConfiguration) {
		this.apnConfiguration = apnConfiguration;
	}

    public String getHssProfileId() {
        return hssProfileId;
    }

    public void setHssProfileId(String hssProfileId) {
        this.hssProfileId = hssProfileId;
    }

    public String getPccProfileId() {
        return pccProfileId;
    }

    public void setPccProfileId(String pccProfileId) {
        this.pccProfileId = pccProfileId;
    }

	public int getZoneSetId() {
		return zoneSetId;
	}

	public void setZoneSetId(int zoneSetId) {
		this.zoneSetId = zoneSetId;
	}

    public boolean isNotUsed() {
        return notUsed;
    }

    public void setNotUsed(boolean notUsed) {
        this.notUsed = notUsed;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

	public List<TechnologyServiceProfile> getTechnologyProfiles() {
		return technologyProfiles;
	}

	public void setTechnologyProfiles(List<TechnologyServiceProfile> technologyProfiles) {
		this.technologyProfiles = technologyProfiles;
	}

	public boolean isValidateCommPlan() {
		return validateCommPlan;
	}

	public void setValidateCommPlan(boolean validateCommPlan) {
		this.validateCommPlan = validateCommPlan;
	}
	
}