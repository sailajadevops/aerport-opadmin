package com.aeris.service.operatoradmin.payload;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

@XmlRootElement
public class CreateCustSIMOrderRequest {
	
	@NotNull(message="{productId.notnull}")
	@Min(value=1, message="{productId.number}")
	private int productId;
	
	@NotNull(message="{simFormatId.notnull}")
	@Min(value=1, message="{simFormatId.number}")
	private int simFormatId;
	
	private int simWarehouseId;
	
	@Min(value = 1, message="{quantity.minimum}")
	@Max(value = 10000, message="{quantity.maximum}")
	@NotNull(message="{quantity.notnull}")
	private long quantity;

	private String comments;
	
	private String trackingNumber;
	
	private String description;
	
	@NotNull(message="{shippingAddress.notnull}")
	@Size(min=1, max=60, message="{shippingAddress.size}")
	private String shippingAddress;
	
	@NotNull(message="{userId.notnull}")
	@Email(message="{userId.email.pattern}")
	private String userId;

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getSimFormatId() {
		return simFormatId;
	}

	public void setSimFormatId(int simFormatId) {
		this.simFormatId = simFormatId;
	}

	public int getSimWarehouseId() {
		return simWarehouseId;
	}

	public void setSimWarehouseId(int simWarehouseId) {
		this.simWarehouseId = simWarehouseId;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}	
}
