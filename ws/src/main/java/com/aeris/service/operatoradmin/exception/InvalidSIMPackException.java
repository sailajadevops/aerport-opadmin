package com.aeris.service.operatoradmin.exception;

public class InvalidSIMPackException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public InvalidSIMPackException(String s) {
		super(s);
	}

	public InvalidSIMPackException(String ex, Throwable t) {
		super(ex, t);
	}
}
