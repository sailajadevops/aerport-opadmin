package com.aeris.service.operatoradmin.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.IProductCarrierDAO;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Carrier;

public class ProductCarrierDAO implements IProductCarrierDAO{

	@Inject
	private IStoredProcedure<List<Carrier>> getCarriersByProductStoredProcedure;
	
	private Cache<Integer, List<Carrier>> cache;

	private static Logger LOG = LoggerFactory.getLogger(OperatorProductDAO.class);
	
	public void init(@Hazelcast(cache = "ProductCarrierCache/id") Cache<Integer, List<Carrier>> cache) {
		this.cache = cache;
	}

	@Override
	public List<Carrier> getCarriersByProduct(final int productId) {
		LOG.debug("getCarriersByProduct: processing get carriers by product");
		
		// If already loaded, fetch from cache
		if(cache.get(productId) != null)
		{
			return cache.get(productId);
		}
		
		List<Carrier> carriers = new ArrayList<Carrier>();
		
		try {
			// Call Get_Carrier_By_Product Stored Procedure 
			carriers = getCarriersByProductStoredProcedure.execute(productId);	
			
			// Put in Cache
			cache.put(productId, carriers);
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during getCarriersStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		}

		LOG.info("getCarriersByProduct: Returned " + carriers.size() + " carriers to the caller");

		return carriers;
	}
}
