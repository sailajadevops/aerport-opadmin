package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Array;
import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.jdbc.annotations.Query;
import com.aeris.service.operatoradmin.dao.IAS4gDAO;
import com.aeris.service.operatoradmin.dao.ICommunicationPlanDAO;
import com.aeris.service.operatoradmin.dao.IOperatorProductDAO;
import com.aeris.service.operatoradmin.dao.IServiceProfileDAO;
import com.aeris.service.operatoradmin.dao.IZoneSetDAO;
import com.aeris.service.operatoradmin.enums.IAccessPointNameEnum;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.enums.ISMPPConnectionEnum;
import com.aeris.service.operatoradmin.exception.DuplicateServiceProfileException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.ServiceProfileException;
import com.aeris.service.operatoradmin.exception.ServiceProfileNotFoundException;
import com.aeris.service.operatoradmin.exception.ServiceProfileValidationException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.APNConfiguration;
import com.aeris.service.operatoradmin.model.AccessPointName;
import com.aeris.service.operatoradmin.model.AccessPointNameType;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.SMPPConfiguration;
import com.aeris.service.operatoradmin.model.SMPPConnection;
import com.aeris.service.operatoradmin.model.ServiceProfile;
import com.aeris.service.operatoradmin.model.TechnologyServiceProfile;
import com.aeris.service.operatoradmin.model.ZonePreference;
import com.aeris.service.operatoradmin.model.ZoneServiceProfile;
import com.aeris.service.operatoradmin.model.ZoneSet;
import com.aeris.service.operatoradmin.utils.Constants;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.inject.name.Named;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

public class ServiceProfileDAO implements IServiceProfileDAO {
	private static Logger LOG = LoggerFactory.getLogger(ServiceProfileDAO.class);
	
    @Query("getCommunicationPlanIdSql")
    private String getCommunicationPlanIdSql;

	private static final String SQL_CREATE_SERVICE_ZONE_CONFIG = "INSERT INTO service_zone_config " 
																	+ "(service_code, " 
																	+ "zone_id, " 
																	+ "mo_sms_allowed, "
																	+ "mt_sms_allowed, " 
																	+ "mo_voice_allowed, " 
																	+ "mt_voice_allowed, " 
																	+ "packet_allowed, " 
																	+ "zone_preferences, " 
																	+ "rep_timestamp, " 
																	+ "last_modified_date, " 
																	+ "last_modified_by) " 
																	+ "values "
																	+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_CREATE_SERVICE_PROFILE_TECHNOLOGY = "INSERT INTO SERVICE_PROFILE_TECHNOLOGY " 
			+ "(SERVICE_CODE, " 
			+ "TECHNOLOGY, " 
			+ "MSISDN_RANGE, "
			+ "STATIC_IP_RANGE, " 
			+ "SERVICE_PROVIDER_ID, " 
			+ "REP_TIMESTAMP, " 
			+ "LAST_MODIFIED_DATE, " 
			+ "CREATED_DATE, "
			+ "CREATED_BY,"
			+ "LAST_MODIFIED_BY,"
			+ "COMMUNICATION_PLAN_CODE) " 
			+ "values "
			+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_DELETE_SERVICE_PROFILE_TECHNOLOGY = "DELETE FROM SERVICE_PROFILE_TECHNOLOGY WHERE SERVICE_CODE = ?";
	
	private static final String SQL_GET_SERVICE_PROFILE_BY_ID = "SELECT a.zone_set_id, a.service_name, " 
																+ "a.service_code, " 
																+ "a.static_ip, "
																+ "a.mo_voice_regex, " 
																+ "a.mo_voice, "
																+ "a.mt_voice, "
																+ "a.mo_sms, "
																+ "a.mt_sms, "
																+ "d.smpp_profile, "
																+ "a.sms_non_aeris_device, "
																+ "a.service_provider_id, "
																+ "a.packet, "
																+ "a.product_id, "
                                                                + "a.hss_profile_id, "
                                                                + "a.pcc_profile_id, "
                                                                + "a.technology, "
																+ "b.login_name, " 
																+ "b.password, " 
																+ "RTRIM(replace(replace(XMLAGG (XMLELEMENT (e, c.zone_id || ';' || c.mo_sms_allowed || ';' || c.mt_sms_allowed " 
																+ "|| ';' || c.mo_voice_allowed || ';' || c.mt_voice_allowed || ';' || c.packet_allowed || ';' " 
																+ "|| c.zone_preferences || ',')),'<E>'),'</E>'), ',') as zone_profiles "
																+ "from aersys_service_profile a inner join account_list b " 
																+ "on a.service_code = b.service_code and "
																+ "a.account_id = ? and "
																+ "a.service_code = ? "
																+ "left join service_zone_config c " 
																+ "on b.service_code = c.service_code "
																+ "left join aersys_service_smpp_profile d " 
																+ "on b.service_code = d.service_code " 
																+ "and a.account_id = d.account_id "
																+ "group by (a.zone_set_id, a.service_name, "
																+ "a.service_code, "
																+ "a.static_ip, "
																+ "a.mo_voice_regex, " 
																+ "a.mo_voice, "
																+ "a.mt_voice, "
																+ "a.mo_sms, "
																+ "a.mt_sms, "
																+ "d.smpp_profile, "
																+ "a.sms_non_aeris_device, "
																+ "a.service_provider_id, "
																+ "a.packet, "
																+ "a.product_id, "
                                                                + "a.hss_profile_id, "
                                                                + "a.pcc_profile_id, "
                                                                + "a.technology, "
																+ "b.login_name, "
																+ "b.password)";

	private static final String SQL_GET_SERVICE_PROFILES_BY_ACCOUNT_ID = "SELECT a.zone_set_id, a.service_name, " 
															+ "a.service_code, "
															+ "a.static_ip, "
															+ "a.mo_voice_regex, " 
															+ "a.mo_voice, "
															+ "a.mt_voice, "
															+ "a.mo_sms, "
															+ "a.mt_sms, "
															+ "a.packet, "
															+ "a.product_id, "
															+ "a.service_provider_id, "
                                                            + "a.technology "
															+ "from aersys_service_profile a " 
															+ "where a.account_id = ? "
															+ "order by a.service_code";
	
	private static final String SQL_GET_SERVICE_PROFILES = "SELECT a.zone_set_id, a.service_name, " 
															+ "a.service_code, "
															+ "a.static_ip, "
															+ "a.mo_voice_regex, " 
															+ "a.mo_voice, "
															+ "a.mt_voice, "
															+ "a.mo_sms, "
															+ "a.mt_sms, "
															+ "a.packet, "
															+ "a.product_id, "
															+ "a.account_id, "
															+ "a.service_provider_id, "
                                                            + "a.technology "
															+ "from aersys_service_profile a " 
															+ "order by a.service_code";
	
	private static final String SQL_GET_SERVICE_PROFILE_APNS = "SELECT a.apn_id, " 
															+ "a.apn_value, "
															+ "a.service "
															+ "from service_apn a "
															+ "where a.service_profile_code = ? " 
															+ "order by a.apn_id";
	
	private static final String SQL_GET_TECHNOLOGY_SERVICE_PROFILES = "SELECT SERVICE_CODE, "
															+ "TECHNOLOGY, "
															+ "MSISDN_RANGE, "
															+ "STATIC_IP_RANGE, "
															+ "SERVICE_PROVIDER_ID,"
															+ "COMMUNICATION_PLAN_CODE "
															+ "FROM SERVICE_PROFILE_TECHNOLOGY "
															+ "WHERE SERVICE_CODE = ? ";
	
	private static final String SQL_GET_APN_SERVICE_NAMES = "SELECT b.service, " +
																"a.apn_id " +
																"from aerisgen.access_point_name a, " +
																"aerisgen.apn_service b " +
																"where a.apn_id in :apn_id_list and " +
																"a.apn_service_id = b.service_id and " +
																"a.carrier_id = :service_provider_id";
	
	private static final String SQL_CREATE_NEW_SERVICE_PROFILE = "BEGIN ACCOUNT_MANAGEMENT_2_0_V2.add_service_name_new(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); END;";

	private static final String SQL_GET_SERVICE_CODE = "SELECT SERVICE_CODE FROM aersys_service_profile WHERE account_id = ? AND service_name = ?";

	private static final String SQL_INSERT_OWNER_ACCT = "INSERT INTO OWNER_ACCOUNT (account_id, service_code, service_name, rep_timestamp) VALUES (:account_id, :service_code, :service_name, systimestamp)";
	
	private static final String SQL_INSERT_ACCT_LIST = "INSERT INTO ACCOUNT_LIST (login_name, password, service_code, rep_timestamp) VALUES (:login_name, :password, :service_code, systimestamp)";
	
	private static final String SQL_INSERT_OWNER_ACCT_TCDB = "INSERT INTO OWNER_ACCOUNT (account_id, service_code, service_name) VALUES (:account_id, :service_code, :service_name)";
	
	private static final String SQL_INSERT_ACCT_LIST_TCDB = "INSERT INTO ACCOUNT_LIST (login_name, password, service_code) VALUES (:login_name, :password, :service_code)";
	
	private static final String SQL_INSERT_SERVICE_APN = "INSERT INTO aerisgen.service_apn " +
															"(rep_timestamp, " +
															"service_profile_code, " +
															"service_profile_name, " +
															"service, " +
															"apn_id, " +	
															"apn_value) " +
															"values (?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_UPDATE_SERVICE_PROFILE = "update aersys_service_profile set " +
																"service_name = ?, "+ 
																"technology = ?, "+
																"mt_voice = ?, "+
																"mo_voice = ?, "+
																"mo_voice_regex = ?, "+
																"roaming = ?, "+
																"packet = ?, "+
																"mt_sms = ?, "+
																"mo_sms = ?, "+
																"static_ip = ?, "+
																"product_id = ?, "+
																"service_provider_id = ?, "+
																"show_akey = ?, "+
																"sms_format = ?, "+
																"static_ip_range = ?, "+
																"msisdn_range = ?, "+
																"sms_non_aeris_device = ?, "+
																"zone_set_id = ? "+
																"where account_id = ? and "+
																"service_code = ? ";
	
	private static final String SQL_INSERT_SMPP_PROFILE = "INSERT INTO aersys_service_smpp_profile " +
															"(account_id, " +
															"service_code, " +
															"smpp_profile, " +
															"rep_timestamp, " +
															"aersys_service_smpp_profile_id) " +
															"values (?, ?, ?, ?, ?)";
	
	
	private static final String SQL_UPDATE_CARRIER_ACCOUNT = "update cdma_carrier_account set " +
																"carrier_id = ?, " +
																"rep_timestamp = ? " +
																"where service_code = ?";
	

	private static final String SQL_GET_NEXT_SMPP_PROFILE_ID = "select service_smpp_profile_id_seq.NEXTVAL from dual";
	
	private static final String SQL_GET_DEVICE_COUNT_BY_SERVICE_CODE = "SELECT COUNT(*) "+
																		"as cnt "+
																		"FROM aerisgen.aersys_service_profile a, "+							
																		"aerbill_prov.device b "+
																		"WHERE a.account_id = ? "+
																		"AND a.service_code = ? "+
																		"AND a.service_name = b.service_name "+
																		"AND b.device_status <> 7";
	
	private static final String SQL_GET_PRODUCT_TECHNOLOGY = "SELECT TECHNOLOGY FROM PRODUCT_TECHNOLOGY WHERE PRODUCT_ID = ?";
	
	private static final String SQL_GET_ALL_PRODUCT_TECHNOLOGY = "SELECT * FROM PRODUCT_TECHNOLOGY WHERE PRODUCT_ID = ?";
	
	private static final String SQL_GET_MVNO_REQUEST_COUNT = "SELECT COUNT(*) "+
																"as cnt "+
																"FROM sprintmvno_request_v2 a "+		
																"WHERE a.account_id = ? "+
																"AND a.service_code = ? ";
	
	private static final String SQL_GET_TMO_REQUEST_COUNT = "SELECT COUNT(*) "+
																"as cnt "+
																"FROM tmo_pending_request_v3 a "+		
																"WHERE a.account_id = ? "+
																"AND a.service_code = ? ";
	
	private static final String SQL_GET_IP_ADDR_RANGE_COUNT = "SELECT COUNT(*) "+
																"as cnt "+
																"FROM radius.ip_address_range_exclusive a "+		
																"WHERE a.account_id = ? "+
																"AND a.service_code = ? ";
	
	private static final String SQL_GET_IP_ADDR_FREE_COUNT = "SELECT COUNT(*) "+
																"as cnt "+
																"FROM radius.ip_address_free_exclusive a "+		
																"WHERE a.account_id = ? "+
																"AND a.service_code = ? ";	
	
	private static final String SQL_DELETE_SERVICE_ZONE_CONFIG = "DELETE from service_zone_config where service_code = ?";
	
	private static final String SQL_DELETE_FROM_ACCOUNT_LIST = "DELETE from account_list where service_code = :service_code";

	private static final String SQL_DELETE_FROM_OWNER_ACCOUNT = "DELETE from owner_account where account_id = :account_id and service_code = :service_code";

	private static final String SQL_DELETE_SERVICE_APNS = "DELETE from service_apn where service_profile_code = :service_profile_code";

	private static final String SQL_DELETE_CARRIER_ACCOUNT = "DELETE from cdma_carrier_account where service_code = :service_code";
	
	private static final String SQL_DELETE_SMPP_PROFILE = "DELETE from aersys_service_smpp_profile where account_id = :account_id and service_code = :service_code";	
	
	private static final String SQL_DELETE_AERSYS_SERVICE_PROFILE = "DELETE from aersys_service_profile where service_code = :service_code";	
	
	private static final String SQL_UPDATE_SERVICE_PROFILE_HSS_PCC_PROFILE = "update aersys_service_profile set " +
																"hss_profile_id = ?, "+ 
																"pcc_profile_id = ?" +
																"where account_id = ? and "+
																"service_code = ? ";
	
	private static final String SQL_UPDATE_ZONE_SET_ID = "update aersys_service_profile set ZONE_SET_ID = ? WHERE account_id = ? AND service_code = ?";
    
	private static final int CARRIER_VODAFONE= 15;
    
    private static final int CARRIER_ATT = 18;

	private static final int DEFAULT_SHOW_SMS_FORMAT = 0;
    
	private static final int DEFAULT_SHOWAKEY = 1;
    
    private static final int PRODUCT_SELF_SERVE_SIM = 7;
    
    private static final int PRODUCT_CC_GLOBAL_SIM = 10;
    
    private static final String SQL_GET_CARRIER_ID = "SELECT CARRIER_ID from ACCOUNT_CARRIER_MAPPING where ACCOUNT_ID = ? AND PRODUCT_ID = ? ";
    
    private static final String SQL_UPDATE_SERVICE_PROFILE_NOT_USED = "update aersys_service_profile set " +
																"not_used = ? " +
																"where account_id = ? and "+
																"service_code = ? ";
    
    @Inject
	private IProductEnum productCache;

    @Inject
	private IAccessPointNameEnum accessPointNameCache;    
    
    @Inject
	private ISMPPConnectionEnum smppConnectionCache;
    
    @Inject @Named("neo.whitelist.carriercodes") 
    private String neoWLCarrierCodes;
    
    @Inject @Named("cc.blacklist.carriercodes") 
    private String ccBlklstCarrierCodes;
    
    @Inject
    private IAS4gDAO aS4gDAO;
    
	private Cache<Long, ServiceProfile> cache;
    
    @com.google.inject.Inject
	private IOperatorProductDAO operatorProductDAO;
    
    @Inject
    private ICommunicationPlanDAO communicationPlanDAO; 
    
    @Inject
    private IZoneSetDAO zoneSetDAO;
    
	public void init(@Hazelcast(cache = "ServiceProfileCache/id") Cache<Long, ServiceProfile> cache) {
		this.cache = cache;
		
		List<ServiceProfile> serviceProfilesList = getAllServiceProfiles();
		ServiceProfile[] serviceProfiles = new ServiceProfile[serviceProfilesList.size()];
		serviceProfilesList.toArray(serviceProfiles);
		
		putInCache(serviceProfiles);
	}

	@Override
	public List<ServiceProfile> getAllServiceProfiles(String operatorId, String accountId, boolean extendAttributes) {
		List<ServiceProfile> serviceProfiles = new ArrayList<ServiceProfile>();

		LOG.info("getAllServiceProfiles: getting service profiles for account id: "+accountId);
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		// If already loaded, fetch from cache
		if(!cache.getValues().isEmpty())
		{
			return getCachedServiceProfiles(operatorId, accountId);
		}
				
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAllServiceProfiles: Fetched db connection");
			
			// Fetch a Single Account 
			ps = conn.prepareStatement(SQL_GET_SERVICE_PROFILES_BY_ACCOUNT_ID);

			ps.setLong(1, Long.parseLong(accountId));
			
			LOG.debug("getAllServiceProfiles: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				ServiceProfile serviceProfile = new ServiceProfile();
				serviceProfile.setZoneSetId(rs.getInt("zone_set_id"));
				serviceProfile.setServiceCode(rs.getLong("service_code"));
				serviceProfile.setRegexMOVoice(rs.getString("mo_voice_regex"));
				serviceProfile.setServiceProfileName(rs.getString("service_name"));
				serviceProfile.setAccountId(accountId);
				serviceProfile.setProductId(rs.getInt("product_id"));
				serviceProfile.setServiceProviderId(rs.getInt("service_provider_id"));
				serviceProfile.setTechnology(rs.getString("technology"));
				setEnrolledServices(serviceProfile, rs);
				
				// Add Service Profile
				serviceProfiles.add(serviceProfile);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getAllServiceProfiles", e);
			throw new GenericServiceException("Unable to getAllServiceProfiles", e);
		} finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}

		LOG.info("Returned serviceProfiles to the caller: "+serviceProfiles);
		
		return serviceProfiles;
	}

	private List<ServiceProfile> getCachedServiceProfiles(final String operatorId, final String accountId) {
		List<ServiceProfile> serviceProfiles = new ArrayList<ServiceProfile>(Collections2.filter(cache.getValues(), new Predicate<ServiceProfile>() {
				@Override
				public boolean apply(ServiceProfile input) {
					if(accountId != null)
					{
						return accountId.equals(input.getAccountId());
					}
					
					return false;
				}
				
			}));
		
		return serviceProfiles;
	}

	private void setZoneProfiles(ResultSet rs, ServiceProfile serviceProfile) throws SQLException {
		List<ZoneServiceProfile> zoneProfiles = new ArrayList<ZoneServiceProfile>();
		
		String[] zoneProfilesStr = rs.getString("zone_profiles").split(",");
		
		for (String zoneProfile : zoneProfilesStr) {
			String[] zoneAttributes = zoneProfile.split(";");
			
			if(zoneAttributes.length > 0 && NumberUtils.isNumber(zoneAttributes[0]))
			{
				ZoneServiceProfile zoneServiceProfile = new ZoneServiceProfile();
				
				int zoneId = Integer.parseInt(zoneAttributes[0]);
				
				EnrolledServices enrolledServices = new EnrolledServices();
				
				enrolledServices.setAllowMOSms(Integer.parseInt(zoneAttributes[1]) == 1);
				enrolledServices.setAllowMTSms(Integer.parseInt(zoneAttributes[2]) == 1);
				enrolledServices.setAllowMOVoice(Integer.parseInt(zoneAttributes[3]) == 1);
				enrolledServices.setAllowMTVoice(Integer.parseInt(zoneAttributes[4]) == 1);
				enrolledServices.setAllowPacket(Integer.parseInt(zoneAttributes[5]) == 1);
				
				zoneServiceProfile.setZoneId(zoneId);
				zoneServiceProfile.setServices(enrolledServices);
				 
				// Set Zone Preferences for the Zone
				if(zoneAttributes.length > 6)
				{
					List<ZonePreference> zonePreferences = getZonePreferences(zoneAttributes[6]);				
					zoneServiceProfile.setZonePreferences(zonePreferences);
				}
				
				LOG.info("Setting Zone Profile: "+ReflectionToStringBuilder.toString(zoneServiceProfile));
				
				zoneProfiles.add(zoneServiceProfile);
			}
		}
		
		LOG.info("Zone Configuration Set for serviceProfile: "+serviceProfile.getServiceCode());
		
		serviceProfile.setZoneProfiles(zoneProfiles);
	}

	private List<ZonePreference> getZonePreferences(String zonePreferences) throws SQLException {
		List<ZonePreference> zonePrefList = null;
		
		if (StringUtils.isNotEmpty(zonePreferences)) {
			zonePrefList = new ArrayList<ZonePreference>();
			
			String[] zonePrefs = zonePreferences.split(":");
			
			for (String zonePref : zonePrefs) {
				if(ZonePreference.isValid(zonePref))
				{
					zonePrefList.add(ZonePreference.fromName(zonePref));
				}
			}

		}
		
		return zonePrefList;
	}

	@Override
	public ServiceProfile getServiceProfile(String operatorId, String accountId, long serviceCode, boolean extendAttributes) {
		ServiceProfile serviceProfile = null;
		
		LOG.info("getServiceProfile: getting service profile for account id: "+accountId+" and service code: "+serviceCode);
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getServiceProfile: Fetched db connection");
			
			// Fetch a Single Account 
			ps = conn.prepareStatement(SQL_GET_SERVICE_PROFILE_BY_ID);

			ps.setLong(1, Long.parseLong(accountId));
			ps.setLong(2, serviceCode);
			
			LOG.info("getServiceProfile: executed query: " + ps);
			rs = ps.executeQuery();

			if (rs.next()) {
				LOG.info("getServiceProfile: Creating service profile object");
				
				serviceProfile = new ServiceProfile();
				serviceProfile.setZoneSetId(rs.getInt("zone_set_id"));
				serviceProfile.setAccountId(accountId);
				serviceProfile.setRegexMOVoice(rs.getString("mo_voice_regex"));
				serviceProfile.setServiceCode(rs.getLong("service_code"));
				serviceProfile.setServicePassword(rs.getString("password"));
				serviceProfile.setServiceUserName(rs.getString("login_name"));
				serviceProfile.setServiceProfileName(rs.getString("service_name"));
				serviceProfile.setProductId(rs.getInt("product_id"));
				serviceProfile.setServiceProviderId(rs.getInt("service_provider_id"));
				serviceProfile.setSmsNonAerisDevice(rs.getInt("sms_non_aeris_device") == 1);
				serviceProfile.setTechnology(rs.getString("technology"));
				// Set services enabled
				setEnrolledServices(serviceProfile, rs);
				
				// Set SMPP configuration for smpp sms
				setSMPPConfiguration(serviceProfile, rs);
				
				// Set Zone level service configuration
				setZoneProfiles(rs, serviceProfile);
				
				//Set Technology service profile
				setTechnologyServiceProfile(serviceProfile, conn);
				
				// Set APN configuration
				setAPNConfiguration(serviceProfile, conn);
                
                serviceProfile.setHssProfileId(rs.getString("hss_profile_id"));
                serviceProfile.setPccProfileId(rs.getString("pcc_profile_id"));
                List<TechnologyServiceProfile> techProfiles = serviceProfile.getTechnologyProfiles();
                if(techProfiles != null) {
                	for (TechnologyServiceProfile techProfile : techProfiles) {
                		if (techProfile.getTechnology() != null && techProfile.getTechnology().equalsIgnoreCase("LTE")) {
                			serviceProfile.setApnsLTE(aS4gDAO.getHssApnValues(rs.getString("hss_profile_id")));
                			break ;
                		}
					}
                } else if((!StringUtils.isEmpty(serviceProfile.getTechnology())) && serviceProfile.getTechnology().equalsIgnoreCase("LTE")) {
                	serviceProfile.setApnsLTE(aS4gDAO.getHssApnValues(rs.getString("hss_profile_id")));
                }
			}
		} catch (SQLException e) {
			LOG.error("Exception during getServiceProfile", e);
			throw new GenericServiceException("Unable to getServiceProfile", e);
		} finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}

		LOG.info("Returned service profile to the caller: "+serviceProfile);
		
		return serviceProfile;
	}

	private void setEnrolledServices(ServiceProfile serviceProfile, ResultSet rs) throws SQLException {
		EnrolledServices enrolledServices = new EnrolledServices();
		
		enrolledServices.setAllowMOSms(rs.getInt("mo_sms") == 1);
		enrolledServices.setAllowMTSms(rs.getInt("mt_sms") == 1);
		enrolledServices.setAllowMOVoice(rs.getInt("mo_voice") == 1);
		enrolledServices.setAllowMTVoice(rs.getInt("mt_voice") == 1);
		enrolledServices.setAllowPacket(rs.getInt("packet") == 1);		
		enrolledServices.setAllowStaticIP(rs.getInt("static_ip") == 1);		
		
		serviceProfile.setServices(enrolledServices);
	}

	private void setSMPPConfiguration(ServiceProfile serviceProfile, ResultSet rs) throws SQLException {
		String smppProfile = rs.getString("smpp_profile");		
		boolean smppSms = (smppProfile != null);
		
		if(smppSms)
		{
			String[] smppProfileDetails = smppProfile.split(":");
			
			// Parse SMPP Profile
			if(smppProfileDetails.length == 2)
			{				
				String smppId = smppProfileDetails[0];
				String smsFormatCode = smppProfileDetails[1];
				
				LOG.info("SMPP Connection Id: "+smppId);
				LOG.info("SMS Format Code: "+smsFormatCode);
				
				// Set SMPP Profile only if the connection id and sms format code are valid
				if(NumberUtils.isNumber(smppId) && NumberUtils.isNumber(smsFormatCode))
				{
					// Get the SMPPConnection Profile for smppId and carrierId combination
					SMPPConnection smppConnection = smppConnectionCache.getSMPPConnectionBySmppId(serviceProfile.getProductId(), serviceProfile.getServiceProviderId(),
																										Integer.parseInt(smppId));

					if(smppConnection != null)
					{
						LOG.info("Valid SMPP Connection found, setting smpp profile");
						
						SMPPConfiguration smppConfiguration = new SMPPConfiguration();
						
						smppConfiguration.setSmppConnectionId(smppConnection.getSmppConnectionId());// Set SMPP Connection Id
						smppConfiguration.setSmsFormatCode(Integer.parseInt(smsFormatCode));// Set SMS Format Code
						
						serviceProfile.setSmppConfiguration(smppConfiguration);
					}
					else
					{
						LOG.info("No SMPP Connection found for smppId: "+smppId+", productId: "+serviceProfile.getProductId()+", carrierId: "+serviceProfile.getServiceProviderId());
					}
				}
				else
				{
					LOG.warn("Not a valid smpp profile: SMPP Connection Id: "+smppId+" and SMS Format Code: "+smsFormatCode+", ignoring..");					
				}
			}
		}
	}

	private void setAPNConfiguration(ServiceProfile serviceProfile, Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try
		{
			ps = conn.prepareStatement(SQL_GET_SERVICE_PROFILE_APNS);
			ps.setLong(1, serviceProfile.getServiceCode());
			LOG.debug("getServiceProfile: setAPNConfiguration: Create prepared statement to fetch apn configuration");
			
			LOG.info("getServiceProfileAPNConfiguration: executed query: " + ps);
			rs = ps.executeQuery();
	
			List<Integer> dynamicApns = new ArrayList<Integer>();
			List<Integer> staticApns = new ArrayList<Integer>();
			
			while (rs.next()) {
				int apnId = rs.getInt("apn_id");
				
				AccessPointNameType apnType = AccessPointNameType.fromName(rs.getString("service"));
				
				if(apnType == AccessPointNameType.DYNAMIC_IP)
				{
					dynamicApns.add(apnId);
				}
				else if(apnType == AccessPointNameType.STATIC_IP)
				{
					staticApns.add(apnId);
				}
			}
			
			// Set APN Configuration for the service profile
			LOG.info("getServiceProfile: setAPNConfiguration: Creating APNConfiguration object");
			
			if(!dynamicApns.isEmpty() || !staticApns.isEmpty())
			{
				APNConfiguration apnConfiguration = new APNConfiguration();
				
				if(!dynamicApns.isEmpty())
				{
					int[] dynamicApnArr = new int[dynamicApns.size()];
					int index = 0;
					
					for (int dynamicApn : dynamicApns) {
						dynamicApnArr[index++] = dynamicApn;
					}
					
					LOG.info("getServiceProfile: setAPNConfiguration: Added Dynamic APNs: "+dynamicApnArr);
					
					apnConfiguration.setDynamicIpApns(dynamicApnArr);
				}
				
				if(!staticApns.isEmpty())
				{
					int[] staticApnArr = new int[staticApns.size()];
					int index = 0;
					
					for (int staticApn : staticApns) {
						staticApnArr[index++] = staticApn;
					}

					LOG.info("getServiceProfile: setAPNConfiguration: Added Static APNs: "+staticApnArr);
					
					apnConfiguration.setStaticIpApns(staticApnArr);
				}

									
				serviceProfile.setApnConfiguration(apnConfiguration);
			}
		}
		catch (Exception e) 
		{
			LOG.error("Exception while setting apn configuration", e);
			throw new GenericServiceException("Unable to getServiceProfile", e);
		}
		finally
		{
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}
	}
	
	
	/**
	 * Sets technology service profile to service profile
	 * 
	 * @param serviceProfile
	 * @param conn
	 */
	private void setTechnologyServiceProfile(ServiceProfile serviceProfile, Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement(SQL_GET_TECHNOLOGY_SERVICE_PROFILES);
			ps.setLong(1, serviceProfile.getServiceCode());
			LOG.debug("setTechnologyServiceProfile: fetch all the service_profile_technology mappings for service profile");
			rs = ps.executeQuery();
			List<TechnologyServiceProfile> technologyProfiles = new ArrayList<TechnologyServiceProfile>();
			while (rs.next()) {
				TechnologyServiceProfile techProfile = new TechnologyServiceProfile();
				techProfile.setMsisdnRange(rs.getString("msisdn_range"));
				techProfile.setServiceProviderId(rs.getInt("service_provider_id"));
				techProfile.setTechnology(rs.getString("technology"));
				techProfile.setStaticIPRange(rs.getString("static_ip_range"));
				techProfile.setCommPlanId(rs.getInt("COMMUNICATION_PLAN_CODE"));
				technologyProfiles.add(techProfile);
			}
			serviceProfile.setTechnologyProfiles(technologyProfiles);
		} catch (Exception e) {
			LOG.error("Exception while setting service_profile_technology", e);
			throw new GenericServiceException("Unable to setTechnologyServiceProfile", e);
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}
	}
	
	@Override
	public ServiceProfile createNewServiceProfile(String operatorId,
			String accountId, String serviceProfileName,
			String serviceUsername, String servicePassword, int productId,
			int serviceProviderId, List<ZoneServiceProfile> zoneProfiles,
			List<TechnologyServiceProfile> technologyProfiles,
			String regexMOVoice, EnrolledServices enrolledServices,
			SMPPConfiguration smppConfig, boolean smsNonAerisDevice,
			APNConfiguration apnConfig, String hssProfileId,
			String pccProfileId, int zoneSetId, boolean notUsed,
			String technology, Date requestedDate, String requestedUser) throws ServiceProfileException,
			DuplicateServiceProfileException, ZoneNotFoundException,
			DuplicateZoneException, ServiceProfileValidationException {
		return createNewServiceProfile(operatorId, accountId, serviceProfileName, serviceUsername, servicePassword, productId, serviceProviderId, zoneProfiles, technologyProfiles, regexMOVoice, enrolledServices, smppConfig, smsNonAerisDevice, apnConfig, hssProfileId, pccProfileId, zoneSetId, notUsed, technology, requestedDate, requestedUser, false);
	}

	@Override
	public ServiceProfile createNewServiceProfile(String operatorId, String accountId, String serviceProfileName, String serviceUsername,
			String servicePassword, int productId, int serviceProviderId, List<ZoneServiceProfile> zoneProfiles, List<TechnologyServiceProfile> technologyProfiles, String regexMOVoice, EnrolledServices enrolledServices,
			SMPPConfiguration smppConfig, boolean smsNonAerisDevice, APNConfiguration apnConfig, String hssProfileId, String pccProfileId,int zoneSetId, boolean notUsed, String technology, Date requestedDate, String requestedUser, boolean validateCommPLan) 
					throws ServiceProfileException, DuplicateServiceProfileException, ZoneNotFoundException, DuplicateZoneException, ServiceProfileValidationException {
		ServiceProfile serviceProfile = null;
		Connection conn = null;
		Connection boneCpConn = null;
		CallableStatement cs = null;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		ResultSet rs = null;

		Connection tcdbConn = null;
		Connection tcdbVGConn = null;
		Statement tcdbStmt = null;

		// Generate account Id for the new account
		long newServiceCode = 0;

		LOG.info("createNewServiceProfile: processing new service profile creation...");
        //TODO: technology can be null for dual mode devices, need to apply this check here
        Product product = operatorProductDAO.getProductByOperatorAndProductId(Integer.parseInt(operatorId), productId);
        if (technology == null && !product.isMultiModeProduct()) {
            technology = product.getTechnology();
        }

		try {
			// Get Prov DB Connection
			boneCpConn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			conn = ((com.jolbox.bonecp.ConnectionHandle) boneCpConn).getInternalConnection();
			// Set Auto Commit to false to control commit/rollback
			conn.setAutoCommit(false);
			//validate the product technology mapping
			validateProductTechnology(productId, technologyProfiles, conn);
			Integer commPlanId = null;
			
			long acctId = Long.parseLong(accountId);
			if(validateCommPLan){
				// validate APNs. For AT & T product, Service profile to be created should not have both Dynamic & static APNs
				if(apnConfig != null && (apnConfig.getDynamicIpApns()!= null && apnConfig.getDynamicIpApns().length >= 1) && (apnConfig.getStaticIpApns()!= null && apnConfig.getStaticIpApns().length >= 1)){
					LOG.error("For AT & T product, Service Profile can not have both Dynamic & Static APNs");
					throw new ServiceProfileValidationException("For AT & T product, Service Profile can not have both Dynamic & Static APNs");
				}
				
				//validate Static APN. For AT & T product, Service profile can not have more than one static APN
				if(apnConfig != null && apnConfig.getStaticIpApns()!= null && apnConfig.getStaticIpApns().length > 1){
					LOG.error("For AT & T product, Service Profile can not have more than one static APN");
					throw new ServiceProfileValidationException("For AT & T product, Service Profile can not have more than one static APN");
				}
				
				int RESId = 0;
				//Zone set is mandatory for AT & T product
				if(zoneSetId == 0){
					LOG.error("For AT & T product, Service Profile should have Zone Set");
					throw new ServiceProfileValidationException("For AT & T product, Service Profile should have Zone Set");
				}else{
					//get RES id for the given zoneSetId
					ZoneSet zoneSet = zoneSetDAO.getZoneSet(Integer.parseInt(operatorId), zoneSetId);
					RESId = zoneSet.getResId();
				}
				
				commPlanId = communicationPlanDAO.getCommPlanId(enrolledServices,apnConfig, RESId, acctId);
				if(commPlanId == null){
					LOG.error("No matching Communication Plan found with selected services.");
					throw new ServiceProfileValidationException("No matching Communication Plan found with selected services");
				}
				
			}

			// Call Procedure to create and assign service profile to account
            
            if (serviceProviderId == 0) {
                String carrierId = getCarrierId(accountId, Integer.toString(productId));
                if (carrierId != null) {
                    serviceProviderId = Integer.parseInt(carrierId);
                }else{
                	//AERPORT-6146 : setting correct serviceProviderId for the given product 
                	serviceProviderId = product.getCarrierId();
                }
            }

			cs = createAssignServiceProfileToAccountQuery(acctId, serviceProfileName, serviceUsername, servicePassword, productId,
					serviceProviderId, regexMOVoice, enrolledServices, smppConfig, smsNonAerisDevice, apnConfig, technology, conn);

			cs.execute();

			// Fetch OUT Parameters 
			int dbResultCode = cs.getInt(28);
			String dbErrorMessage = cs.getString(29);
			
			// Read the Service Code created
			newServiceCode = getGeneratedServiceCode(serviceProfileName, conn, newServiceCode, acctId, dbResultCode, dbErrorMessage);

			LOG.info("serviceCode created in aersys_service_profile: " + newServiceCode);

			//Update Zone_set_ID for the generatedServiceCode
			ps1 = conn.prepareStatement(SQL_UPDATE_ZONE_SET_ID);
			ps1.setInt(1, zoneSetId);
			ps1.setLong(2, acctId);
			ps1.setLong(3, newServiceCode);
			int updateZoneSetIdCount = ps1.executeUpdate();
			
			if(updateZoneSetIdCount > 0)
				LOG.info("ZONE_SET_ID "+zoneSetId+" updated for ServiceCode "+newServiceCode);
			
			// Insert service code into TCDB
			if (newServiceCode > 0) {				
				tcdbConn = getTCDBConnection(serviceProviderId);
				LOG.info("Adding Service Credentials to SJ TCDB..");
				addServiceCodeToOwnerAccountInTCDB(accountId, serviceProfileName, serviceUsername, servicePassword, newServiceCode, tcdbConn);
				
			}

			// Set up zone level service profile configuration
			if (zoneProfiles != null) {
				createServiceProfileZoneConfiguration(newServiceCode, zoneProfiles, requestedDate, requestedUser, conn);
				LOG.info("Added zones configuration for the Service Profile with service code: " + newServiceCode);
			}
			if (technologyProfiles != null) {
				createTechnologyServiceProfile(Long.valueOf(accountId), productId, newServiceCode, technologyProfiles, requestedDate, requestedUser, conn, commPlanId);
				LOG.info("Added Technology Service Profile with service code: " + newServiceCode);
			} else {
				technologyProfiles = createDefaultTechnologyServiceProfile(operatorId, Long.valueOf(accountId), productId, requestedDate, requestedUser, conn, newServiceCode, serviceProviderId, technology, commPlanId);
				LOG.info("Added Default Technology Service Profiles with service code: " + newServiceCode);
			}
            
            if (productId == PRODUCT_SELF_SERVE_SIM) {
				// Add whitelist and black list to HLR
				setRoamingProfile(newServiceCode);
			}
            
            //Create blackList only if the operator is sprint and product is cc-global-sim
            if (operatorId.trim().equals("2") && productId == PRODUCT_CC_GLOBAL_SIM) {
				// Add blackList to HLR
				setRoamingProfileBlackList(newServiceCode);
			}
            
            if (hssProfileId != null && pccProfileId != null) {
                updateHssAndPccProfileId(accountId, newServiceCode, hssProfileId, pccProfileId, conn);
            }
            
            // Update not_used flag
            updateNotUsedFlag(accountId, newServiceCode, notUsed, conn);

			// Commit Changes
			if (conn != null) {
				conn.commit();
			}

			if (tcdbConn != null) {
				tcdbConn.commit();
			}

			LOG.info("Committed createNewServiceProfile changes to db");

			// Return the service profile created to the client
			serviceProfile = new ServiceProfile();
			serviceProfile.setServiceCode(newServiceCode);
			serviceProfile.setAccountId(accountId);
			serviceProfile.setRegexMOVoice(regexMOVoice);
			serviceProfile.setServiceProfileName(serviceProfileName);
			serviceProfile.setServiceUserName(serviceUsername);
			serviceProfile.setServicePassword(servicePassword);
			serviceProfile.setProductId(productId);
			serviceProfile.setServiceProviderId(serviceProviderId);
			serviceProfile.setServices(enrolledServices);
			serviceProfile.setZoneProfiles(zoneProfiles);
			serviceProfile.setApnConfiguration(apnConfig);
            
            serviceProfile.setTechnology(technology);
            serviceProfile.setHssProfileId(hssProfileId);
            serviceProfile.setPccProfileId(pccProfileId); 
            
            //set comm_Plan_id in technologyProfiles
			for(int i = 0; i < technologyProfiles.size() ; i++){
				technologyProfiles.get(i).setCommPlanId(commPlanId);
			}
            serviceProfile.setTechnologyProfiles(technologyProfiles);
			// Put in Cache
			cache.put(newServiceCode, serviceProfile);
			
			try{
				// If Vegas TCDB write fails, we need not to rollback PROV & SJ TCDB changes. Only log the vegas failure in log file.
				if (newServiceCode > 0) {
					// get TCDB connection for Vegas Db. For Vodafone, the connection would be NULL as we do not need to insert into vegas db
					tcdbVGConn = getTCDBVGConnection(serviceProviderId);
					if(tcdbVGConn != null){
						LOG.info("Adding Service Credentials to Vegas TCDB..");
						addServiceCodeToOwnerAccountInTCDB(accountId, serviceProfileName, serviceUsername, servicePassword, newServiceCode, tcdbVGConn);
					}
					
					try{
						if(tcdbVGConn != null){
							tcdbVGConn.commit();
						}
					}catch(SQLException e){
						rollBackConnection(tcdbVGConn, e);
						LOG.error("Error commiting Vegas TCDB connection :" + e);
					}
					
				}
			}catch(Exception e){
				rollBackConnection(tcdbVGConn, e);
				LOG.error("Service Profile creation in Vegas TCDB failed with error : " + e);
			}
						
			LOG.info("Returning service profile created...");
		} catch (SQLException e) {
			rollBackConnection(conn, e);
			
			rollBackConnection(tcdbConn, e);
			
			throw new ServiceProfileException("Create service profile failed due to db error", e);
		} catch (ZoneNotFoundException e) {
			
			rollBackConnection(conn, e);

			rollBackConnection(tcdbConn, e);

			throw e;
		} catch (DuplicateZoneException e) {
			rollBackConnection(conn, e);

			rollBackConnection(tcdbConn, e);

			throw e;
		} catch (ServiceProfileValidationException e) {
			rollBackConnection(conn, e);
			rollBackConnection(tcdbConn, e);
			throw e;
		}  catch (DuplicateServiceProfileException e) {
			rollBackConnection(conn, e);

			rollBackConnection(tcdbConn, e);

			throw e;
		}  catch (Exception e) {
			rollBackConnection(conn, e);

			rollBackConnection(tcdbConn, e);

			throw new ServiceProfileException("Create service profile failed due to fatal error", e);
		}  finally {
			DBConnectionManager.getInstance().cleanup(boneCpConn, cs);
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
			DBConnectionManager.getInstance().cleanup(null, ps1);
			DBConnectionManager.getInstance().cleanup(tcdbConn, tcdbStmt);
			DBConnectionManager.getInstance().cleanup(tcdbVGConn);
		}

		LOG.info("Returned Service Profile to the caller: " + serviceProfile);

		return serviceProfile;
	}
	
	public Integer getCommPlanCode(String technology, int carrierId, String serviceName) throws Throwable{
	    Integer commPlanId = null;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
            ps = conn.prepareStatement(getCommunicationPlanIdSql);
            ps.setString(1, technology);
            ps.setInt(2, carrierId);
            ps.setString(3, serviceName);
            rs = ps.executeQuery();
            if(rs.next()) {
                Integer id = rs.getInt("communication_plan_code");
                commPlanId = (id == 0 ? null : id) ;
            }
        } finally {
            DBConnectionManager.getInstance().cleanup(conn,ps,rs);
        }
        
        return commPlanId;
	}

    private List<TechnologyServiceProfile> createDefaultTechnologyServiceProfile(String operatorId, long accountId, int productId,
        Date requestedDate, String requestedUser, Connection conn,
        long newServiceCode, int serviceProviderId, String technology, Integer commPLanId) throws SQLException {
        LOG.info("createDefaultTechnologyServiceProfile operatorId = {}, accountId = {}, productId = {}, serviceProfile = {}, serviceProviderId = {}", operatorId, accountId, productId, newServiceCode, serviceProviderId);
        Product parentProduct = operatorProductDAO.getProductByOperatorAndProductId(Integer.parseInt(operatorId), productId);
        List<TechnologyServiceProfile> techProfiles;
        if (parentProduct.isMultiModeProduct() && parentProduct.isSubcriptionLevelOnly()) {
            List<Product> products = getProductTechnologyList(accountId, productId, conn);
            techProfiles = new ArrayList<TechnologyServiceProfile>(products.size());
            for (Product product : products) {
                TechnologyServiceProfile tProfile = new TechnologyServiceProfile();
                tProfile.setMsisdnRange(product.getMsisdnRange());
                tProfile.setServiceProviderId(product.getCarrierId());
                tProfile.setStaticIPRange(product.getStaticIpRange());
                tProfile.setTechnology(product.getTechnology());
                techProfiles.add(tProfile);
            }
        } else { // Single mode product
            techProfiles = new ArrayList<TechnologyServiceProfile>(1);
            TechnologyServiceProfile tProfile = new TechnologyServiceProfile();
            tProfile.setMsisdnRange(parentProduct.getMsisdnRange());
            tProfile.setServiceProviderId(serviceProviderId);
            tProfile.setStaticIPRange(parentProduct.getStaticIpRange());
            tProfile.setTechnology(technology);
            techProfiles.add(tProfile);
        }
        createTechnologyServiceProfile(Long.valueOf(accountId), productId, newServiceCode, techProfiles, requestedDate, requestedUser, conn, commPLanId);
        return techProfiles;
    }
    
    private static final String SERVICE_CODE_WHITELIST = "insert into g_service_code_whitelist (REP_TIMESTAMP, SERVICE_CODE, CARRIER_CODE) values (sysgmtseconds, ?, ?)";
	private static final String SERVICE_CODE_SGSN_WHITELIST = "insert into g_service_code_whitelist_sgsn (REP_TIMESTAMP, SERVICE_CODE, CARRIER_CODE) values (sysgmtseconds, ?, ?)";
	private static final String SERVICE_CODE_ROAMING_PREF = "insert into g_service_code_roam_def (REP_TIMESTAMP, SERVICE_CODE, DEFAULT_ROAMING_ALLOWED_FLAG) values (sysgmtseconds, ?, 0)";
	private static final String SERVICE_CODE_SGSN_ROAMING_PREF = "insert into g_service_code_roam_def_sgsn (REP_TIMESTAMP, SERVICE_CODE, DEFAULT_ROAMING_ALLOWED_FLAG) values (sysgmtseconds, ?, 0)";
	
	private void setRoamingProfile(long serviceCode) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			LOG.info("Creating roaming profile for serviceCode {} with carrier codes {}", serviceCode, neoWLCarrierCodes);
			String[] carrierCodes = neoWLCarrierCodes.split(",");
			
			conn = DBConnectionManager.getInstance().getGSMHLRDatabaseConnection();
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(SERVICE_CODE_WHITELIST);
			for (String carrierCode : carrierCodes) {
                ps.setLong(1, serviceCode);
                ps.setInt(2, Integer.parseInt(carrierCode));
                ps.addBatch();
            }
            ps.executeBatch();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(SERVICE_CODE_SGSN_WHITELIST);
			for (String carrierCode : carrierCodes) {
                ps.setLong(1, serviceCode);
                ps.setInt(2, Integer.parseInt(carrierCode));
                ps.addBatch();
            }
            ps.executeBatch();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(SERVICE_CODE_ROAMING_PREF);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(SERVICE_CODE_SGSN_ROAMING_PREF);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            
            conn.commit();
            LOG.info("Successfully created roaming profile for serviceCode {}", serviceCode);
		} catch (SQLException ex) {
			rollBack(conn);
			LOG.error("Error setting roaming profile for service code", ex);
			throw ex;
		} finally {
			toggleAutoCommit(conn, true);
			DBConnectionManager.getInstance().cleanup(conn, ps);
		}
		
	}
	
	private static final String DELETE_SERVICE_CODE_WHITELIST = "delete from g_service_code_whitelist where SERVICE_CODE = ?";
	private static final String DELETE_SERVICE_CODE_SGSN_WHITELIST = "delete from g_service_code_whitelist_sgsn where SERVICE_CODE = ?";
	private static final String DELETE_SERVICE_CODE_ROAMING_PREF = "delete from g_service_code_roam_def where SERVICE_CODE = ?";
	private static final String DELETE_SERVICE_CODE_SGSN_ROAMING_PREF = "delete from g_service_code_roam_def_sgsn where SERVICE_CODE = ?";

	private void deleteRoamingProfile(long serviceCode) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			LOG.info("Deleting roaming profile for serviceCode {} ", serviceCode);
			
			conn = DBConnectionManager.getInstance().getGSMHLRDatabaseConnection();
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(DELETE_SERVICE_CODE_WHITELIST);
			ps.setLong(1, serviceCode);
			ps.executeUpdate();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(DELETE_SERVICE_CODE_SGSN_WHITELIST);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(DELETE_SERVICE_CODE_ROAMING_PREF);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(DELETE_SERVICE_CODE_SGSN_ROAMING_PREF);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            
            conn.commit();
            LOG.info("Successfully deleted roaming profile for serviceCode {}", serviceCode);
		} catch (SQLException ex) {
			rollBack(conn);
			LOG.error("Error setting roaming profile for service code", ex);
			throw ex;
		} finally {
			toggleAutoCommit(conn, true);
			DBConnectionManager.getInstance().cleanup(conn, ps);
		}
	}
	
	
	private static final String SERVICE_CODE_BLACKLIST = "insert into g_service_code_blacklist (REP_TIMESTAMP, SERVICE_CODE, CARRIER_CODE) values (sysgmtseconds, ?, ?)";
	private static final String SERVICE_CODE_SGSN_BLACKLIST = "insert into g_service_code_blacklist_sgsn (REP_TIMESTAMP, SERVICE_CODE, CARRIER_CODE) values (sysgmtseconds, ?, ?)";
	private static final String SERVICE_CODE_ROAMING_PREF_BLACKLIST = "insert into g_service_code_roam_def (REP_TIMESTAMP, SERVICE_CODE, DEFAULT_ROAMING_ALLOWED_FLAG) values (sysgmtseconds, ?, 1)";
    private static final String SERVICE_CODE_SGSN_ROAMING_PREF_BLACKLIST = "insert into g_service_code_roam_def_sgsn (REP_TIMESTAMP, SERVICE_CODE, DEFAULT_ROAMING_ALLOWED_FLAG) values (sysgmtseconds, ?, 1)";
	
	
	private void setRoamingProfileBlackList(long serviceCode) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			LOG.info("Creating roaming profile of BlackList carriers for serviceCode {} with carrier codes {}", serviceCode, ccBlklstCarrierCodes);
			if(StringUtils.isEmpty(ccBlklstCarrierCodes)) {
				LOG.info("No blacklist carrier-codes configured, doing nothing.");
				return ;
			}
			String[] carrierCodes = ccBlklstCarrierCodes.split(",");
			
			conn = DBConnectionManager.getInstance().getGSMHLRDatabaseConnection();
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(SERVICE_CODE_BLACKLIST);
			for (String carrierCode : carrierCodes) {
                ps.setLong(1, serviceCode);
                ps.setInt(2, Integer.parseInt(carrierCode));
                ps.addBatch();
            }
            ps.executeBatch();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(SERVICE_CODE_SGSN_BLACKLIST);
			for (String carrierCode : carrierCodes) {
                ps.setLong(1, serviceCode);
                ps.setInt(2, Integer.parseInt(carrierCode));
                ps.addBatch();
            }
            ps.executeBatch();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(SERVICE_CODE_ROAMING_PREF_BLACKLIST);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(SERVICE_CODE_SGSN_ROAMING_PREF_BLACKLIST);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            
            conn.commit();
            LOG.info("Successfully created roaming profile of BlackList carriers for serviceCode {}", serviceCode);
		} catch (SQLException ex) {
			rollBack(conn);
			LOG.error("Error setting roaming profile of BlackList carriers for service code", ex);
			throw ex;
		} finally {
			toggleAutoCommit(conn, true);
			DBConnectionManager.getInstance().cleanup(conn, ps);
		}
		
	}
	

	private static final String DELETE_SERVICE_CODE_BLACKLIST = "delete from  g_service_code_blacklist where service_code = ?";
	private static final String DELETE_SERVICE_CODE_SGSN_BLACKLIST = "delete from  g_service_code_blacklist_sgsn  where service_code = ?";
	
	/**
	 * deletes black-list roaming profiles for CC accounts with Global CC SIM products only
	 * 
	 * @param serviceCode
	 * @throws SQLException
	 */
	private void deleteRoamingProfileBlackList(long serviceCode) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		
		try {
			LOG.info("Deleting roaming profile of BlackList carriers for serviceCode {} ", serviceCode);
			
			conn = DBConnectionManager.getInstance().getGSMHLRDatabaseConnection();
			conn.setAutoCommit(false);

			ps = conn.prepareStatement(DELETE_SERVICE_CODE_BLACKLIST);
			ps.setLong(1, serviceCode);
			ps.executeUpdate();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(DELETE_SERVICE_CODE_SGSN_BLACKLIST);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(DELETE_SERVICE_CODE_ROAMING_PREF);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            DBConnectionManager.getInstance().cleanup(null, ps);
            
            ps = conn.prepareStatement(DELETE_SERVICE_CODE_SGSN_ROAMING_PREF);
            ps.setLong(1, serviceCode);
            ps.executeUpdate();
            
            conn.commit();
            LOG.info("Successfully deleted roaming profile of BlackList carriers for serviceCode {}", serviceCode);
		} catch (SQLException ex) {
			rollBack(conn);
			LOG.error("Error setting roaming profile of BlackList carriers for service code", ex);
			throw ex;
		} finally {
			toggleAutoCommit(conn, true);
			DBConnectionManager.getInstance().cleanup(conn, ps);
		}
	}

	private void rollBack(Connection conn) {
		if (conn != null) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				LOG.error("Failed to rollback", e);
			}
		}
	}

	private void toggleAutoCommit(Connection conn, boolean autoCommit) {
		if (conn != null) {
			try {
				conn.setAutoCommit(autoCommit);
			} catch (SQLException e) {
				LOG.error("Error while setting auto commit to true", e);
			}
		}
	}

	private Connection getTCDBConnection(int serviceProviderId) throws SQLException {
		Connection tcdbConn;
		
		// For Vodafone, Insert into GSM TCDB
		if (serviceProviderId == CARRIER_VODAFONE || serviceProviderId == CARRIER_ATT) {
			tcdbConn = DBConnectionManager.getInstance().getTCDBGSMDatabaseConnection();
			tcdbConn.setAutoCommit(false);
			
			LOG.info("Fetched GSM tcdb connection: " + tcdbConn);
		}
		// For Non-Vodafone, Insert into CDMA TCDB
		else {
			tcdbConn = DBConnectionManager.getInstance().getTCDBDatabaseConnection();
			tcdbConn.setAutoCommit(false);
			
			LOG.info("Fetched CDMA tcdb connection: " + tcdbConn);
		}
		
		return tcdbConn;
	}
	
	
	private Connection getTCDBVGConnection(int serviceProviderId) throws SQLException {
		Connection tcdbConn = null;
		
		// For vodafone, no need to insert into GSm vegas tcdb
		if(serviceProviderId == CARRIER_VODAFONE){
			
		}// For Non-Vodafone, Insert into CDMA Vegas TCDB.
		else{
			tcdbConn = DBConnectionManager.getInstance().getTCDBVGDatabaseConnection();
			if(tcdbConn != null)
				tcdbConn.setAutoCommit(false);
			
			LOG.info("Fetched Vegas CDMA tcdb connection: " + tcdbConn);
		}
				
		return tcdbConn;
	}

	private CallableStatement createAssignServiceProfileToAccountQuery(long acctId, String serviceProfileName, String serviceUsername,
			String servicePassword, int productId, int serviceProviderId, String regexMOVoice, EnrolledServices enrolledServices, SMPPConfiguration smppConfiguration, 
			boolean smsNonAerisDevice, APNConfiguration apnConfiguration, String technology, Connection conn) throws NumberFormatException, SQLException {
		CallableStatement cs = conn.prepareCall(SQL_CREATE_NEW_SERVICE_PROFILE);
		
		//Product product = productCache.getProductById(productId);

		cs.setLong(1, acctId); // P_ACCOUNT_ID_I
		cs.setString(2, serviceProfileName); // P_SERVICE_NAME_I
		cs.setString(3, technology); // P_TECHNOLOGY_I // P_TECHNOLOGY_I
		cs.setString(4, serviceUsername); // P_LOGIN_I
		cs.setString(5, servicePassword); // P_PASSWORD_I
		cs.setInt(6, enrolledServices.isAllowMTVoice() ? 1 : 0); // P_MT_VOICE_I
		cs.setInt(7, enrolledServices.isAllowMOVoice() ? 1 : 0); // P_MO_VOICE_I
		cs.setString(8, regexMOVoice); // P_MO_VOICE_REGEX_I
		cs.setInt(9, enrolledServices.isAllowRoaming() ? 1 : 0); // P_ROAMING_I
		cs.setInt(10, enrolledServices.isAllowPacket() ? 1 : 0); // P_PACKET_I
		cs.setInt(11, enrolledServices.isAllowMTSms() ? 1 : 0); // P_MT_SMS_I
		cs.setInt(12, enrolledServices.isAllowMOSms() ? 1 : 0); // P_MO_SMS_I

		LOG.info("ProductId: " + productId);

		// Set Product Id
		setProductId(13, productId, cs);

		cs.setInt(14, enrolledServices.isAllowStaticIP() ? 1 : 0);

		// Set Service Provider Id
		setServiceProviderId(15, serviceProviderId, cs);

		// Set SMS Format
		cs.setInt(16, DEFAULT_SHOWAKEY);
		cs.setInt(17, DEFAULT_SHOW_SMS_FORMAT);
		
		LOG.info("Setting SMPP Configuration");
		
		// SMPP SMS Configuration
		setSMPPSmsFormatCode(18, smppConfiguration, cs);
		setSMPPConnectionId(19, smppConfiguration, cs);
		
		// Enable SMS From Aeris to Non-Aeris Device
		setSmsNonAerisDevice(20, smsNonAerisDevice, cs);
		
		LOG.info("Setting Apn Values");

		// Set apn values
		setApnValues(apnConfiguration, conn, cs);

		LOG.info("Registering Out Params");

		// Register out values
		cs.registerOutParameter(28, OracleTypes.INTEGER);
		cs.registerOutParameter(29, OracleTypes.VARCHAR);
		
		return cs;
	}
	
	private PreparedStatement updateAssignServiceProfileToAccountQuery(long acctId, long serviceCode, String serviceProfileName, String serviceUsername,
			String servicePassword, int productId, int serviceProviderId, String regexMOVoice, EnrolledServices enrolledServices,
			boolean smsNonAerisDevice, APNConfiguration apnConfiguration, int zoneSetId, String technology, Connection conn) throws NumberFormatException, SQLException {
		Product product = productCache.getProductById(productId);
		PreparedStatement ps = null;
		
		try
		{
			ps = conn.prepareStatement(SQL_UPDATE_SERVICE_PROFILE);

			ps.setString(1, serviceProfileName); // SERVICE_NAME
			ps.setString(2, technology); // TECHNOLOGY // TECHNOLOGY
			ps.setInt(3, enrolledServices.isAllowMTVoice() ? 1 : 0); // MT_VOICE
			ps.setInt(4, enrolledServices.isAllowMOVoice() ? 1 : 0); // MO_VOICE
			ps.setString(5, regexMOVoice); // MO_VOICE_REGEX
			ps.setInt(6, enrolledServices.isAllowRoaming() ? 1 : 0); // ROAMING
			ps.setInt(7, enrolledServices.isAllowPacket() ? 1 : 0); // PACKET
			ps.setInt(8, enrolledServices.isAllowMTSms() ? 1 : 0); // MT_SMS
			ps.setInt(9, enrolledServices.isAllowMOSms() ? 1 : 0); // MO_SMS
			ps.setInt(10, enrolledServices.isAllowStaticIP() ? 1 : 0);
			
			LOG.info("ProductId: " + productId);

			// Set Product Id
			setProductId(11, productId, ps);

			// Set Service Provider Id
			setServiceProviderId(12, serviceProviderId, ps);

			// Set SMS Format
			ps.setInt(13, DEFAULT_SHOWAKEY);
			ps.setInt(14, DEFAULT_SHOW_SMS_FORMAT);
			
			// Set Static IP Range
			setStaticIpRange(15, enrolledServices, ps, product);
			
			// Set MSISDN Range
			ps.setString(16, product.getMsisdnRange());
			
			LOG.info("Setting SMPP Configuration");
			
			// Enable SMS from aeris to non-aeris device
			setSmsNonAerisDevice(17, smsNonAerisDevice, ps);
			ps.setInt(18, zoneSetId);
			ps.setLong(19, acctId); // ACCOUNT_ID
			ps.setLong(20, serviceCode); // SERVICE_CODE		
			
			LOG.info("Setting Apn Values");

			// Set apn values
			updateApnValues(serviceCode, serviceProfileName, serviceProviderId, apnConfiguration, conn);
			
			ps.executeUpdate();
		}
		finally
		{
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
		
		return ps;
	}

	private void updateApnValues(long serviceCode, String serviceProfileName, int serviceProviderId, APNConfiguration apnConfiguration, Connection conn) throws SQLException {
		Map<Integer, String> serviceNameMap = getServiceNameMap(serviceProviderId, apnConfiguration, conn);
		
		if(!serviceNameMap.isEmpty())
		{
			Set<Integer> keySet = serviceNameMap.keySet();
			boolean success = false;
			PreparedStatement ps = null;
			
			try
			{
				ps = conn.prepareStatement(SQL_INSERT_SERVICE_APN);
				
				for (Integer apnId : keySet) {
					boolean added = addServiceApnToBatch(serviceCode, serviceProfileName, serviceNameMap, ps, apnId);
					
					if(added)
					{
						success = true;
					}
				}
				
				if(success)
				{
					ps.executeBatch();
				}					
			}
			finally
			{
				DBConnectionManager.getInstance().cleanup(null, ps);
			}
		}
	}

	private boolean addServiceApnToBatch(long serviceCode, String serviceProfileName, Map<Integer, String> serviceNameMap, PreparedStatement ps, Integer apnId) throws SQLException {
		AccessPointName accessPointName = accessPointNameCache.getAccessPointNameById(apnId);

		if(accessPointName != null)
		{
			ps.clearParameters();
			ps.setDate(1, new java.sql.Date(new Date().getTime()));
			ps.setLong(2, serviceCode);
			ps.setString(3, serviceProfileName);
			ps.setString(4, serviceNameMap.get(apnId));
			ps.setInt(5, apnId);
			ps.setString(6, accessPointName.getApnName());
			
			ps.addBatch();
			
			return  true;
		}
		
		return false;
	}

	private Map<Integer, String> getServiceNameMap(int serviceProviderId, APNConfiguration apnConfiguration, Connection conn) throws SQLException {
		Map<Integer, String> serviceNameMap = new HashMap<Integer, String>();	
		Statement stmt = null;
		ResultSet rs = null;
		
		String query = buildApnServiceNamesQuery(serviceProviderId, apnConfiguration);	
		
		if(query != null)
		{
			try
			{
				stmt = conn.createStatement();
				rs = stmt.executeQuery(query);
				
				while(rs.next())
				{
					int apnId = rs.getInt("apn_id");
					String serviceName = rs.getString("service");
					
					serviceNameMap.put(apnId, serviceName);
				}			
			}
			finally
			{
				DBConnectionManager.getInstance().cleanup(null, stmt, rs);
			}
		}
		
		return serviceNameMap;
	}

	private String buildApnServiceNamesQuery(int serviceProviderId, APNConfiguration apnConfiguration) {
		Set<Integer> set = new HashSet<Integer>();
		String query = null;
		
		if(apnConfiguration != null)
		{
			addApnsToSet(set, apnConfiguration.getDynamicIpApns());
			addApnsToSet(set, apnConfiguration.getStaticIpApns());
			addApnsToSet(set, apnConfiguration.getMoSmsApns());
			addApnsToSet(set, apnConfiguration.getMoVoiceApns());
			addApnsToSet(set, apnConfiguration.getMtSmsApns());
			addApnsToSet(set, apnConfiguration.getMtVoiceApns());
			addApnsToSet(set, apnConfiguration.getRoamingApns());
		}
		
		if(!set.isEmpty())
		{
			StringBuffer inClause = new StringBuffer("(");
			inClause.append(StringUtils.join(set, ","));
			inClause.append(")");
			
			query = SQL_GET_APN_SERVICE_NAMES.replace(":apn_id_list", inClause).replace(":service_provider_id", String.valueOf(serviceProviderId));
		}
		
		return query;
	}

	private void addApnsToSet(Set<Integer> set, int[] apns) {
		if(apns != null)
		{
			for (int apnId : apns) {
				set.add(apnId);
			}
		}
	}

	private void setStaticIpRange(int index, EnrolledServices enrolledServices, PreparedStatement ps, Product product) throws SQLException {
		if(enrolledServices.isAllowStaticIP())
		{
			ps.setString(index, product.getStaticIpRange());
		}
		else
		{
			ps.setNull(index, Types.VARCHAR);
		}
	}
	
	private void setSMPPSmsFormatCode(int index, SMPPConfiguration smppConfiguration, PreparedStatement ps) throws SQLException {
		if(smppConfiguration != null)
		{
			LOG.info("setting smpp sms format code: " + smppConfiguration.getSmsFormatCode());			
			ps.setInt(index, smppConfiguration.getSmsFormatCode());// P_SMS_FORMAT_CODE_I
		}
		else
		{
			ps.setNull(index, Types.INTEGER);// P_SMS_FORMAT_CODE_I
		}
	}
	
	private void setSMPPConnectionId(int index, SMPPConfiguration smppConfiguration, PreparedStatement ps) throws SQLException {
		if(smppConfiguration != null)
		{
			LOG.info("setting smpp connection id" + smppConfiguration.getSmppConnectionId());	
			
			SMPPConnection smppConnection = smppConnectionCache.getSMPPConnectionById(smppConfiguration.getSmppConnectionId());
			ps.setInt(index, (smppConnection != null ? smppConnection.getSmppId() : smppConfiguration.getSmppConnectionId()));// P_SMPP_CONNECTION_ID_I
		}
		else
		{
			ps.setNull(index, Types.INTEGER);// P_SMPP_CONNECTION_ID_I
		}
	}
	
	private void setSmsNonAerisDevice(int index, boolean smsNonAerisDevice, PreparedStatement ps) throws SQLException {
		ps.setInt(index, smsNonAerisDevice ? 1 : 0);// P_SMS_NON_AERIS_DEVICE_I
	}

	private void setApnValues(APNConfiguration apnConfiguration, Connection conn, CallableStatement cs) throws SQLException {
		int[] dynamicApns = null;
		int[] staticApns = null;
		int[] mtVoiceApns = null;
		int[] moVoiceApns = null;
		int[] mtSmsApns = null;
		int[] moSmsApns = null;
		int[] roamingApns = null;
		
		if(apnConfiguration != null)
		{
			dynamicApns = apnConfiguration.getDynamicIpApns();
			staticApns = apnConfiguration.getStaticIpApns();
			mtVoiceApns = apnConfiguration.getMtVoiceApns();
			moVoiceApns = apnConfiguration.getMoVoiceApns();
			mtSmsApns = apnConfiguration.getMtSmsApns();
			moSmsApns = apnConfiguration.getMoSmsApns();
			roamingApns = apnConfiguration.getRoamingApns();
		}
		
		setNumArray(cs, conn, 21, dynamicApns);
		setNumArray(cs, conn, 22, staticApns);
		setNumArray(cs, conn, 23, mtVoiceApns);
		setNumArray(cs, conn, 24, moVoiceApns);
		setNumArray(cs, conn, 25, mtSmsApns);
		setNumArray(cs, conn, 26, moSmsApns);
		setNumArray(cs, conn, 27, roamingApns);
	}

	private void addServiceCodeToOwnerAccount(String accountId, String serviceProfileName, String serviceUsername,
			String servicePassword, long newServiceCode, Connection conn) throws SQLException, DuplicateServiceProfileException {
		LOG.info("addServiceCode: DB Connection: " + conn);		
		Statement stmt = null;				
		
		try
		{
			stmt = conn.createStatement();
			
			LOG.info("Creating sql statement: " + stmt +", to insert service code");
			
			String ownerAcctQuery = SQL_INSERT_OWNER_ACCT.replace(":account_id", accountId)
					.replace(":service_code", String.valueOf(newServiceCode))
					.replace(":service_name", StringUtils.join(new Object[] { "'", serviceProfileName, "'" }));
	
			String acctListQuery = SQL_INSERT_ACCT_LIST
					.replace(":login_name", StringUtils.join(new Object[] { "'", serviceUsername, "'" }))
					.replace(":service_code", String.valueOf(newServiceCode))
					.replace(":password", StringUtils.join(new Object[] { "'", servicePassword, "'" }));
	
			stmt.addBatch(ownerAcctQuery);
			stmt.addBatch(acctListQuery);		
	
			LOG.info("Batched SQL to execute: "+ownerAcctQuery);
			LOG.info("Batched SQL to execute: "+acctListQuery);
	
			stmt.executeBatch();			
			
			LOG.info("Executed sql batch successfully");
		}
		catch (BatchUpdateException e)
		{
			LOG.error("Exception while storing credentials in table owner_account and account_list - batch update", e);
			throw new DuplicateServiceProfileException("User Name already used by another service profile: "+serviceUsername);
		}
		finally
		{
			DBConnectionManager.getInstance().cleanup(null, stmt);
		}
	}

	private void addServiceCodeToOwnerAccountInTCDB(String accountId, String serviceProfileName, String serviceUsername,
			String servicePassword, long newServiceCode, Connection conn) throws SQLException, DuplicateServiceProfileException {
		LOG.info("addServiceCode: DB Connection: " + conn);		
		Statement stmt = null;				
		
		try
		{
			stmt = conn.createStatement();
			
			LOG.info("Creating sql statement: " + stmt +", to insert service code");
			
			String ownerAcctQuery = SQL_INSERT_OWNER_ACCT_TCDB.replace(":account_id", accountId)
					.replace(":service_code", String.valueOf(newServiceCode))
					.replace(":service_name", StringUtils.join(new Object[] { "'", serviceProfileName, "'" }));
	
			String acctListQuery = SQL_INSERT_ACCT_LIST_TCDB
					.replace(":login_name", StringUtils.join(new Object[] { "'", serviceUsername, "'" }))
					.replace(":service_code", String.valueOf(newServiceCode))
					.replace(":password", StringUtils.join(new Object[] { "'", servicePassword, "'" }));
	
			stmt.addBatch(ownerAcctQuery);
			stmt.addBatch(acctListQuery);		
	
			LOG.info("Batched SQL to execute: "+ownerAcctQuery);
			LOG.info("Batched SQL to execute: "+acctListQuery);
	
			stmt.executeBatch();			
			
			LOG.info("Executed sql batch successfully");
		}
		catch (BatchUpdateException e)
		{
			LOG.error("Exception while storing credentials in table owner_account and account_list - batch update", e);
			throw new DuplicateServiceProfileException("User Name already used by another service profile: "+serviceUsername);
		}
		finally
		{
			DBConnectionManager.getInstance().cleanup(null, stmt);
		}
	}
	
	private void updateCarrierAccount(long serviceCode, int serviceProviderId, Connection conn) throws SQLException {
		LOG.info("updateCarrierAccount: DB Connection: " + conn);
		PreparedStatement ps = null;				
		
		try
		{
			ps = conn.prepareStatement(SQL_UPDATE_CARRIER_ACCOUNT);	
			
			LOG.info("Creating sql statement: " + ps +", to update carrier service provider id to: "+serviceProviderId+" for service code: "+serviceCode);
	
			ps.setInt(1, serviceProviderId);
			ps.setDate(2, new java.sql.Date(new Date().getTime()));
			ps.setLong(3, serviceCode);
	
			ps.executeUpdate();			
			
			LOG.info("Executed sql statement successfully");
		}
		finally
		{
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
	}
	
	private void insertSMPPProfile(long accountId, long serviceCode, SMPPConfiguration smppConfiguration, Connection conn) throws SQLException, ServiceProfileException {
		LOG.info("insertSMPPProfile: DB Connection: " + conn);
		PreparedStatement ps = null;				
		
		try
		{
			ps = conn.prepareStatement(SQL_INSERT_SMPP_PROFILE);	
			
			LOG.info("Creating sql statement: " + ps +", to insert smpp profile for account id : "+accountId+" for service code: "+
			serviceCode+" SMPP Configuration: "+smppConfiguration);
	
			 // account_id
			ps.setLong(1, accountId);
			
			// service_code
			ps.setLong(2, serviceCode); 
			
			SMPPConnection smppConnection = smppConnectionCache.getSMPPConnectionById(smppConfiguration.getSmppConnectionId());
			
			// smpp_profile (smpp_connection_id:sms_format_code)
			ps.setString(3, StringUtils.join(new Object[]{(smppConnection != null ? smppConnection.getSmppId() : smppConfiguration.getSmppConnectionId()), smppConfiguration.getSmsFormatCode()}, ":")); 
			
			// rep_timestamp
			ps.setDate(4, new java.sql.Date(new Date().getTime()));
			
			// aersys_service_smpp_profile_id
			ps.setLong(5, getNextSmppProfileId(conn));
	
			ps.executeUpdate();			
			
			LOG.info("Created smpp profile successfully");
		}
		finally
		{
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
	}

	private long getNextSmppProfileId(Connection conn) throws SQLException, ServiceProfileException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		long smppProfileId = -1;
		
		try
		{
			ps = conn.prepareStatement(SQL_GET_NEXT_SMPP_PROFILE_ID);	
			
			LOG.info("Creating sql statement: " + ps +" to get next smpp profile id");
	
			rs = ps.executeQuery();
			
			if(rs.next())
			{
				smppProfileId = rs.getLong(1);
			}
			
			LOG.info("Next Smpp Profile Id fetched: "+smppProfileId);
		}
		finally
		{
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}
		
		if(smppProfileId == -1)
		{
			throw new ServiceProfileException("Could not fetch the next smpp profile id while creating the smpp profile");
		}
		
		return smppProfileId;
	}

	private long getGeneratedServiceCode(String serviceProfileName, Connection conn, long newServiceCode, long acctId, int dbResultCode,
			String dbErrorMessage) throws SQLException, DuplicateServiceProfileException {
		PreparedStatement ps;
		ResultSet rs;
		
		if (dbResultCode == 0) {
			ps = conn.prepareStatement(SQL_GET_SERVICE_CODE);
			ps.setLong(1, acctId);
			ps.setString(2, serviceProfileName);

			rs = ps.executeQuery();

			if (rs.next()) {
				newServiceCode = rs.getLong("SERVICE_CODE");
			}
		}
		else
		{
			throw new DuplicateServiceProfileException(dbErrorMessage);
		}
		
		return newServiceCode;
	}

	private void setProductId(int index, int productId, PreparedStatement ps) throws SQLException {
		// Srinivas Puranam: Set only if product id is valid
		if (productId > 0) {
			ps.setInt(index, productId);
		}
		// otherwise set product id as null
		else {
			ps.setNull(index, Types.INTEGER);
		}
	}

	private void setNumArray(CallableStatement cs, Connection conn, int parameterIndex, int[] array) throws SQLException {
		int[] arr = (array == null ? new int[0] : array);
		
		ArrayDescriptor arrayDescriptor = ArrayDescriptor.createDescriptor("ARRAY_APN_ID_TYPE", conn);
		Array oraArray = new ARRAY(arrayDescriptor, conn, arr);
	    arrayDescriptor = null; 
	    cs.setArray(parameterIndex, oraArray);	
	}

	private void createServiceProfileZoneConfiguration(long newServiceCode, List<ZoneServiceProfile> zoneProfiles, Date requestedDate, String requestedUser, Connection conn)
			throws SQLException, ZoneNotFoundException, DuplicateZoneException {
		LOG.info("Creating service profile zone configuration for service code: " + newServiceCode);

		PreparedStatement ps = null;
		List<Integer> zoneIds = new ArrayList<Integer>();

		try {
			LOG.debug("Creating Prepared Statement to insert the data in service_zone_config table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_SERVICE_ZONE_CONFIG);

			LOG.debug("Setting parameters");		
			
			for (ZoneServiceProfile zoneServiceProfile : zoneProfiles) {
				EnrolledServices enrolledServices = zoneServiceProfile.getServices();
				
				if(enrolledServices != null)
				{	
					// Add Zone Config to the Service Profile
					addZoneConfigurationToServiceProfile(newServiceCode, requestedDate, requestedUser, ps, zoneServiceProfile.getZoneId(), zoneServiceProfile.getZonePreferences(),
							enrolledServices);
					
					if(!zoneIds.contains(zoneServiceProfile.getZoneId()))
					{
						zoneIds.add(zoneServiceProfile.getZoneId());
					}
					else
					{
						throw new DuplicateZoneException("Duplicate zone configuration for zone id: "+zoneServiceProfile.getZoneId());
					}
				}
			}

			LOG.info("inserting service profile zone configuration in the db");

			// Execute the sql batch now
			ps.executeBatch();

			LOG.info("Added service profile zone configuration for service code: " + newServiceCode + " in database");
		} catch (BatchUpdateException sqle) {
			LOG.error("Exception during creating service profile zone configuration in table service_zone_config - batch update", sqle);
			throw new ZoneNotFoundException("One of the zones specified is not valid :"+zoneIds);
		} catch (SQLException sqle) {
			LOG.error("Exception during creating service profile zone configuration in table service_zone_config", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
	}
	
	
	/**
	 * Create service profile technology entry for service profile
	 * 
	 * @param newServiceCode
	 * @param technologyProfiles
	 * @param requestedDate
	 * @param requestedUser
	 * @param conn
	 * @param commPlanId 
	 * @throws SQLException
	 */
	private void createTechnologyServiceProfile(long accountId, int productId, long newServiceCode, List<TechnologyServiceProfile> technologyProfiles, Date requestedDate, 
			String requestedUser, Connection conn, Integer commPlanId) throws SQLException {

		LOG.info("Creating service profile service_profile_technology for service code: " + newServiceCode);

		PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in service_profile_technology table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_SERVICE_PROFILE_TECHNOLOGY);

			LOG.debug("Setting parameters");		
			List<Product> products = getProductTechnologyList(accountId, productId, conn);
			for (TechnologyServiceProfile technologyProfile : technologyProfiles) {
				ps.setLong(1, newServiceCode);
				ps.setString(2, technologyProfile.getTechnology());
				if(StringUtils.isEmpty(technologyProfile.getMsisdnRange())) {
					for (Product product : products) {
						if(product.getTechnology() != null && technologyProfile.getTechnology() != null && product.getTechnology().equalsIgnoreCase(technologyProfile.getTechnology())) {
							technologyProfile.setMsisdnRange(product.getMsisdnRange());
							break;
						}
					}
				}
				if(StringUtils.isEmpty(technologyProfile.getStaticIPRange())) {
					for (Product product : products) {
						if(product.getTechnology() != null && technologyProfile.getTechnology() != null && product.getTechnology().equalsIgnoreCase(technologyProfile.getTechnology())) {
							technologyProfile.setStaticIPRange(product.getStaticIpRange());
							break;
						}
					}
				}
				if(technologyProfile.getServiceProviderId() <= 0) {
					for (Product product : products) {
						if(product.getTechnology() != null && technologyProfile.getTechnology() != null && product.getTechnology().equalsIgnoreCase(technologyProfile.getTechnology())) {
							technologyProfile.setServiceProviderId(product.getCarrierId());
							break;
						}
					}
				}
				ps.setString(3, technologyProfile.getMsisdnRange());
				ps.setString(4, technologyProfile.getStaticIPRange());
				ps.setInt(5, technologyProfile.getServiceProviderId());
				ps.setTimestamp(6, new java.sql.Timestamp(System.currentTimeMillis()));
				ps.setDate(7, new java.sql.Date(requestedDate.getTime()));
				ps.setDate(8, new java.sql.Date(requestedDate.getTime()));
				ps.setString(9, requestedUser);
				ps.setString(10, requestedUser);
				if(commPlanId != null)
					ps.setInt(11,commPlanId);
				else
					ps.setNull(11, Types.NUMERIC);
				
				ps.addBatch();
			}
			LOG.info("inserting service profile technology in the db");

			// Execute the sql batch now
			ps.executeBatch();

			LOG.info("Added service profile technology for service code: " + newServiceCode + " in database");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating service profile technology in table service_profile_technology", sqle);
			//TODO: remove this block once the trigger is removed from database
			if(!sqle.getMessage().contains("ORA-00001: unique constraint (AERBILL_PROV.SERVICE_PROFILE_TECHNOLOGYPK) violated")) {
				throw sqle;
			}
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
	
	}
	
	
	/**
	 * Updates service profile technology entry for service profile
	 * 
	 * @param serviceCode
	 * @param technologyProfiles
	 * @param requestedDate
	 * @param requestedUser
	 * @param conn
	 * @param commPlanId 
	 * @throws SQLException
	 */
	private void updateTechnologyServiceProfile(long accountId, int productId, long serviceCode, List<TechnologyServiceProfile> technologyProfiles, Date requestedDate, 
			String requestedUser, Connection conn, Integer commPlanId) throws SQLException {
		LOG.info("Updating service profile service_profile_technology for service code: " + serviceCode);

		try {
			LOG.debug("First deleting existing data in service_profile_technology table");
			deleteTechnologyServiceProfile(serviceCode, requestedUser, conn);
			LOG.debug("Inserting new service profile technology for service code: " + serviceCode + " in database");
			if(technologyProfiles != null) {
				createTechnologyServiceProfile(Long.valueOf(accountId), productId, serviceCode, technologyProfiles, requestedDate, requestedUser, conn, commPlanId);
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during updating service profile technology in table service_profile_technology", sqle);
			throw sqle;
		}
	}
	
	
	
	/**
	 * Deletes associated service profile technology entry from database for serviceCode
	 * 
	 * @param serviceCode
	 * @param requestedUser
	 * @param conn
	 * @throws SQLException
	 */
	private void deleteTechnologyServiceProfile(long serviceCode, String requestedUser, Connection conn) throws SQLException {
		LOG.debug("Deleting service profile service_profile_technology for service code: " + serviceCode);

		PreparedStatement ps = null;

		try {
			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_SERVICE_PROFILE_TECHNOLOGY);
			ps.setLong(1, serviceCode);
			ps.executeUpdate();
			LOG.debug("Successfully Deleted service profile technology for service code: " + serviceCode + " in database");
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting service profile technology in table service_profile_technology", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
	}

	private void addZoneConfigurationToServiceProfile(long newServiceCode, Date requestedDate, String requestedUser, PreparedStatement ps, int zoneId, 
			List<ZonePreference> zonePreferences, EnrolledServices services)
			throws SQLException {
		ps.clearParameters();

		if (zoneId > 0) {
			ps.setLong(1, newServiceCode);
			ps.setLong(2, zoneId);
			ps.setInt(3, services.isAllowMOSms() ? 1 : 0);
			ps.setInt(4, services.isAllowMTSms() ? 1 : 0);
			ps.setInt(5, services.isAllowMOVoice() ? 1 : 0);
			ps.setInt(6, services.isAllowMTVoice() ? 1 : 0);
			ps.setInt(7, services.isAllowPacket() ? 1 : 0);
			ps.setString(8,  StringUtils.join(zonePreferences, ":"));
			ps.setDate(9, new java.sql.Date(requestedDate.getTime()));
			ps.setDate(10, new java.sql.Date(requestedDate.getTime()));
			ps.setString(11, requestedUser);

			ps.addBatch();

			LOG.debug("Creating: Zone Configuration for zone " + zoneId + " added to service profile");
		} else {
			LOG.warn("Skipping: Zone Id is not valid, Zone Configuration for zone " + zoneId + " not added to service profile");
		}
	}
	
	private void setServiceProviderId(int index, int serviceProviderId, PreparedStatement ps) throws SQLException {
		if(serviceProviderId != 0)
		{
			ps.setInt(index, serviceProviderId);		
		}
		else
		{
			ps.setNull(index, Types.INTEGER);	
		}
		
		LOG.info("ServiceProviderId: " + serviceProviderId);
	}

	@Override
	public ServiceProfile updateServiceProfile(String operatorId, String accountId, long serviceCode, String serviceProfileName, String serviceUsername,
			String servicePassword, int productId, int serviceProviderId, List<ZoneServiceProfile> zoneProfiles, List<TechnologyServiceProfile> technologyProfiles, String regexMOVoice, 
			EnrolledServices enrolledServices, SMPPConfiguration smppConfig, boolean smsNonAerisDevice, APNConfiguration apnConfig, String hssProfileId, String pccProfileId,int zoneSetId, boolean notUsed, String technology, Date requestedDate, String requestedUser)
			throws ServiceProfileNotFoundException, DuplicateServiceProfileException, ZoneNotFoundException, ServiceProfileException, DuplicateZoneException, ServiceProfileValidationException {
		return updateServiceProfile(operatorId, accountId, serviceCode, serviceProfileName, serviceUsername, servicePassword, productId, serviceProviderId, zoneProfiles, technologyProfiles, regexMOVoice, enrolledServices, smppConfig, smsNonAerisDevice, apnConfig, hssProfileId, pccProfileId, zoneSetId, notUsed, technology, requestedDate, requestedUser, false);
	}
	
	@Override
	public ServiceProfile updateServiceProfile(String operatorId, String accountId, long serviceCode, String serviceProfileName, String serviceUsername,
			String servicePassword, int productId, int serviceProviderId, List<ZoneServiceProfile> zoneProfiles, List<TechnologyServiceProfile> technologyProfiles, String regexMOVoice, 
			EnrolledServices enrolledServices, SMPPConfiguration smppConfig, boolean smsNonAerisDevice, APNConfiguration apnConfig, String hssProfileId, String pccProfileId,int zoneSetId, boolean notUsed, String technology, Date requestedDate, String requestedUser, boolean validateCommPlan)
			throws ServiceProfileNotFoundException, DuplicateServiceProfileException, ZoneNotFoundException, ServiceProfileException, DuplicateZoneException, ServiceProfileValidationException {
		ServiceProfile serviceProfile = null;
		Connection conn = null;
		Connection oldTcdbConn = null;
		Connection tcdbConn = null;
		Connection oldTcdbVGConn = null;
		Connection tcdbVGConn = null;
		int currentServiceProviderId = -1;
				
		// Fetch Service Provider Id
		ServiceProfile profile = cache.get(serviceCode);
		
		if(profile != null)
		{
			LOG.info("Profile found in cache for service code: "+serviceCode);

			currentServiceProviderId = profile.getServiceProviderId();
			LOG.info("Service Provider Id: "+currentServiceProviderId);	
		}

		LOG.info("updateServiceProfile: processing service profile update request...");
		Product product = productCache.getProductById(productId);
		//Dual mode profiles may have technology as null
        if (technology == null && !product.isMultiModeProduct()) {
            technology = product.getTechnology();
        }
        
		try {
			Integer commPlanId = null;
			if(validateCommPlan){
				// validate APNs. For AT & T product, Service profile to be created should not have both Dynamic & static APNs
				if(apnConfig != null && (apnConfig.getDynamicIpApns()!= null && apnConfig.getDynamicIpApns().length >= 1) && (apnConfig.getStaticIpApns()!= null && apnConfig.getStaticIpApns().length >= 1)){
					LOG.error("For AT & T product, Service Profile can not have both Dynamic & Static APNs");
					throw new ServiceProfileValidationException("For AT & T product, Service Profile can not have both Dynamic & Static APNs");
				}
				
				//validate Static APN. For AT & T product, Service profile can not have more than one static APN
				if(apnConfig != null && apnConfig.getStaticIpApns()!= null && apnConfig.getStaticIpApns().length >= 1){
					LOG.error("For AT & T product, Service Profile can not have more than one static APN");
					throw new ServiceProfileValidationException("For AT & T product, Service Profile can not have more than one static APN");
				}
				
				int RESId = 0;
				//Zone set is mandatory for AT & T product
				if(zoneSetId == 0){
					LOG.error("For AT & T product, Service Profile should have Zone Set");
					throw new ServiceProfileValidationException("For AT & T product, Service Profile should have Zone Set");
				}else{
					//get RES id for the given zoneSetId
					ZoneSet zoneSet = zoneSetDAO.getZoneSet(Integer.parseInt(operatorId), zoneSetId);
					RESId = zoneSet.getResId();
				}
				
				commPlanId = communicationPlanDAO.getCommPlanId(enrolledServices,apnConfig, RESId, Long.parseLong(accountId));
				if(commPlanId == null){
					LOG.error("No matching Communication Plan found with selected services and zone set");
					throw new ServiceProfileValidationException("No matching Communication Plan found with selected services and zone set");
				}
			}
			
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

			LOG.info("Service Code for the requested service profile: " + serviceCode);

			conn.setAutoCommit(false);

			// Validate Service Profile if there are any active devices or pending requests
			validateServiceProfile(Integer.parseInt(accountId), serviceCode, conn);		
			validateProductTechnology(productId, technologyProfiles, conn);
			
			
			
			// Update the Service Zone Configuration
			LOG.info("Updating zones configuration for the Service Profile with service code: " + serviceCode);

			// Delete previous zone configuration
			deleteServiceProfileZoneConfiguration(serviceCode, conn);

			// Delete Account and APN configuration
			deleteAccountAndAPNConfigurationForServiceProfile(serviceCode, accountId, conn);

			// Insert service code details in owner_account and account_list
			// table - update with login, password and other details
			addServiceCodeToOwnerAccount(accountId, serviceProfileName, serviceUsername, servicePassword, serviceCode, conn);
			
			// Update smpp profile details
			if(smppConfig != null)
			{
				insertSMPPProfile(Long.parseLong(accountId), serviceCode, smppConfig, conn);
			}
		
			if(serviceProviderId > 0)
			{
				// Update service provider id in cdma_carrier_account table
				updateCarrierAccount(serviceCode, serviceProviderId, conn);				
			}
			
			// Update basic service profile
			updateAssignServiceProfileToAccountQuery(Long.parseLong(accountId), serviceCode, serviceProfileName, serviceUsername, servicePassword, productId, serviceProviderId,
					regexMOVoice, enrolledServices, smsNonAerisDevice, apnConfig,zoneSetId, technology, conn);
			
			if (zoneProfiles != null) {
				// Add Zone Configuration for the Service Profile
				createServiceProfileZoneConfiguration(serviceCode, zoneProfiles, requestedDate, requestedUser, conn);
				LOG.info("Added zones configuration for the Service Profile with service code: " + serviceCode);
			}
			//User may just want to delete
			updateTechnologyServiceProfile(Long.valueOf(accountId), productId, serviceCode, technologyProfiles, requestedDate, requestedUser, conn, commPlanId);
			LOG.info("Updates technology service profile with service code: " + serviceCode);
			
			// Update service code details in owner_account and account_list
			// If currentServiceProviderId and new serviceProviderId are same
			if ((currentServiceProviderId == CARRIER_VODAFONE && serviceProviderId == CARRIER_VODAFONE) || 
					(currentServiceProviderId != CARRIER_VODAFONE && serviceProviderId != CARRIER_VODAFONE)) {
				oldTcdbConn = getTCDBConnection(currentServiceProviderId);
				deleteServiceProfileFromOwnerAccount(serviceCode, accountId, oldTcdbConn);
				addServiceCodeToOwnerAccountInTCDB(accountId, serviceProfileName, serviceUsername, servicePassword, serviceCode, oldTcdbConn);
				
				// Update service code details on Vegas tcDb
				oldTcdbVGConn = getTCDBVGConnection(currentServiceProviderId);
				if(oldTcdbVGConn != null){
					deleteServiceProfileFromOwnerAccount(serviceCode, accountId, oldTcdbVGConn);
					addServiceCodeToOwnerAccountInTCDB(accountId, serviceProfileName, serviceUsername, servicePassword, serviceCode, oldTcdbVGConn);
				}
				
			}
			// If currentServiceProviderId and new serviceProviderId are different
			else
			{
				// Delete Service Profile from previously selected TCDB - the
				// service provider has changed from gsm to cdma tcdb or vice versa
				oldTcdbConn = getTCDBConnection(currentServiceProviderId);
				deleteServiceProfileFromOwnerAccount(serviceCode, accountId, oldTcdbConn);
				
				oldTcdbVGConn = getTCDBVGConnection(currentServiceProviderId);
				if(oldTcdbVGConn != null){
					deleteServiceProfileFromOwnerAccount(serviceCode, accountId, oldTcdbVGConn);
				}
				
				// Delete Service Profile from previously selected TCDB - the
				// service provider has changed from gsm to cdma tcdb or vice versa
				tcdbConn = getTCDBConnection(serviceProviderId);
				deleteServiceProfileFromOwnerAccount(serviceCode, accountId, tcdbConn);
				
				// Insert service code details in owner_account and account_list
				// table - update with login, password and other details
				addServiceCodeToOwnerAccountInTCDB(accountId, serviceProfileName, serviceUsername, servicePassword, serviceCode, tcdbConn);
				
				tcdbVGConn = getTCDBVGConnection(serviceProviderId);
				if(tcdbVGConn != null){
					deleteServiceProfileFromOwnerAccount(serviceCode, accountId, tcdbVGConn);
					addServiceCodeToOwnerAccountInTCDB(accountId, serviceProfileName, serviceUsername, servicePassword, serviceCode, tcdbVGConn);
				}
			}

            // Set up hss profile id and pcc profile id for service profile.
            if (hssProfileId != null && pccProfileId != null) {
                updateHssAndPccProfileId(accountId, serviceCode, hssProfileId, pccProfileId, conn);
            }
            
            // Update not_used flag
            updateNotUsedFlag(accountId, serviceCode, notUsed, conn);

			// Commit Changes
			if (conn != null) {
				conn.commit();
			}

			if (oldTcdbConn != null) {
				oldTcdbConn.commit();
			}
			
			if (tcdbConn != null) {
				tcdbConn.commit();
			}
			
			if(oldTcdbVGConn != null){
				oldTcdbVGConn.commit();
			}
			
			if(tcdbVGConn != null){
				tcdbVGConn.commit();
			}
			
			LOG.info("Committed updateServiceProfile changes to db");

			// Return the service profile created to the client
			serviceProfile = new ServiceProfile();
			serviceProfile.setServiceCode(serviceCode);
			serviceProfile.setAccountId(accountId);
			serviceProfile.setRegexMOVoice(regexMOVoice);
			serviceProfile.setServiceProfileName(serviceProfileName);
			serviceProfile.setServiceUserName(serviceUsername);
			serviceProfile.setServicePassword(servicePassword);
			serviceProfile.setProductId(productId);
			serviceProfile.setServiceProviderId(serviceProviderId);
			serviceProfile.setServices(enrolledServices);
			serviceProfile.setZoneProfiles(zoneProfiles);
			serviceProfile.setApnConfiguration(apnConfig);
			serviceProfile.setTechnology(technology);
			
			//set comm_Plan_id in technologyProfiles
			for(int i = 0; i < technologyProfiles.size() ; i++){
				technologyProfiles.get(i).setCommPlanId(commPlanId);
			}
            serviceProfile.setTechnologyProfiles(technologyProfiles);
			// Put in Cache
			cache.put(serviceCode, serviceProfile);
						
			LOG.info("Returning service profile created...");
		} catch (SQLException e) {
			rollBackConnection(conn, e);
			rollBackConnection(oldTcdbConn, e);
			rollBackConnection(tcdbConn, e);
			rollBackConnection(oldTcdbVGConn, e);
			rollBackConnection(tcdbVGConn, e);
			
			throw new ServiceProfileException("Update service profile failed due to db error", e);
		}  catch (ZoneNotFoundException e) {
			rollBackConnection(conn, e);

			throw e;
		} catch (DuplicateServiceProfileException e) {
			rollBackConnection(conn, e);
			rollBackConnection(oldTcdbConn, e);
			rollBackConnection(tcdbConn, e);
			rollBackConnection(oldTcdbVGConn, e);
			rollBackConnection(tcdbVGConn, e);
			
			throw e;
		}  catch (ServiceProfileValidationException e) {
			rollBackConnection(conn, e);
			
			throw e;
		}  catch (DuplicateZoneException e) {
			rollBackConnection(conn, e);
			
			throw e;
		} catch (Exception e) {
			rollBackConnection(conn, e);
			rollBackConnection(oldTcdbConn, e);
			rollBackConnection(tcdbConn, e);
			rollBackConnection(oldTcdbVGConn, e);
			rollBackConnection(tcdbVGConn, e);
			
			throw new ServiceProfileException("Update service profile failed due to fatal error", e);
		}  finally {
			DBConnectionManager.getInstance().cleanup(conn);
			DBConnectionManager.getInstance().cleanup(oldTcdbConn);
			DBConnectionManager.getInstance().cleanup(tcdbConn);
			DBConnectionManager.getInstance().cleanup(oldTcdbVGConn);
			DBConnectionManager.getInstance().cleanup(tcdbVGConn);
		}

		LOG.info("Returned Service Profile to the caller: " + serviceProfile);

		return serviceProfile;
	}
	
	private void deleteServiceProfileZoneConfiguration(long serviceCode, Connection conn) throws SQLException {
		LOG.info("Deleting zone service configuration for service code: " + serviceCode);

		PreparedStatement ps = null;
		
		try {
			LOG.debug("Creating Prepared Statement to delete the data in service_zone_config table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_SERVICE_ZONE_CONFIG);

			LOG.debug("Setting parameters");

			ps.setLong(1, serviceCode);
			
			LOG.info("Deleting zone service configuration info in the db");

			ps.executeUpdate();
			
			LOG.info("Deleted zone service configuration info for serviceCode: " + serviceCode + " in database");
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting zone service configuration info in table service_zone_config", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
	}
	
	private void deleteAccountAndAPNConfigurationForServiceProfile(long serviceCode, String accountId, Connection conn) throws SQLException {

		LOG.info("Deleting Account and APN information for service code: " + serviceCode);

		Statement stmt = null;
		
		try {
			LOG.debug("Creating Prepared Statement to delete the data in service_apn table and account_list table");

			// Create SQL Statement
			stmt = conn.createStatement();

			// Delete IP Account Association
			String deleteServiceAPNsQuery = SQL_DELETE_SERVICE_APNS.replace(":service_profile_code", String.valueOf(serviceCode));

			LOG.debug("Adding query to batch: " + deleteServiceAPNsQuery);
			stmt.addBatch(deleteServiceAPNsQuery);
			
			// Delete IP Account V1 Association
			String deleteFromAccountListQuery = SQL_DELETE_FROM_ACCOUNT_LIST.replace(":service_code", String.valueOf(serviceCode));

			LOG.debug("Adding query to batch: " + deleteFromAccountListQuery);
			stmt.addBatch(deleteFromAccountListQuery);

			// Delete IP Account Association
			String deleteFromOwnerAccountQuery = SQL_DELETE_FROM_OWNER_ACCOUNT.replace(":account_id", accountId).replace(":service_code", String.valueOf(serviceCode));
			
			LOG.debug("Adding query to batch: " + deleteFromOwnerAccountQuery);
			stmt.addBatch(deleteFromOwnerAccountQuery);
			
			// Delete IP Account Association
			String deleteFromSMPPProfile = SQL_DELETE_SMPP_PROFILE.replace(":account_id", accountId).replace(":service_code", String.valueOf(serviceCode));
			
			LOG.debug("Adding query to batch: " + deleteFromSMPPProfile);
			stmt.addBatch(deleteFromSMPPProfile);
						
			LOG.info("Deleting Accounts information in the db");

			stmt.executeBatch();
			
			LOG.info("Deleted Accounts and APN information for serviceCode: " + serviceCode + " in database");
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting Accounts and APN information", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, stmt);
		}
	}

	@Override
	public boolean deleteServiceProfile(String operatorId, String accountId, long serviceCode, Date requestedDate, String requestedUser)
			throws ServiceProfileNotFoundException, ServiceProfileException, ServiceProfileValidationException {
		Connection conn = null;
		Connection tcdbConn = null;
		Connection tcdbVGConn = null;
		boolean success = false;
		int serviceProviderId = -1;
		
		// Fetch Service Provider Id
		ServiceProfile profile = cache.get(serviceCode);
		
		if(profile != null)
		{
			LOG.info("Profile found in cache for service code: "+serviceCode);

			serviceProviderId = profile.getServiceProviderId();
			LOG.info("Service Provider Id: "+serviceProviderId);	
		}
		else
		{
			LOG.warn("Profile cannot be found in cache for service code: "+serviceCode);
		}
		
		LOG.info("deleteServiceProfile: processing service profile delete request...");

		// Get Prov11 Connection
		conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

		try {
			LOG.info("Service Code for the requested service profile: " + serviceCode);

			conn.setAutoCommit(false);
			
			// Validate Service Profile if there are any active devices or pending requests
			validateServiceProfile(Integer.parseInt(accountId), serviceCode, conn);
			
			// Delete Service Profile Zone Configuration
			deleteServiceProfileZoneConfiguration(serviceCode, conn);
			
			//Delete Technology service profile
			deleteTechnologyServiceProfile(serviceCode, requestedUser, conn);
						
			// Delete Old Account Information
			LOG.info("Deleting Old Voice, IP, MicroBurst related Information for the Service Profile with service code: " + serviceCode);
			deleteAccountAndAPNConfigurationForServiceProfile(serviceCode, accountId, conn);
			
			// Delete Service Profile Carrier Info
			deleteCarrierForServiceProfile(serviceCode, accountId, conn);
						
			// Delete Basic Service Profile Info
			int updateCount = deleteBasicServiceProfileInfo(serviceCode, accountId, conn);
			LOG.info("Updated basic service profile info for the Service Profile with service code: " + serviceCode);
			
			if(updateCount == 0)
			{
				LOG.error("Service Profile is not found for service code: "+serviceCode);
				throw new ServiceProfileNotFoundException("Service Profile is not found: "+serviceCode);
			}			
			
			// Delete service profile from TCDB
			tcdbConn = getTCDBConnection(serviceProviderId);
			LOG.info("Deleting Service Credentials from SJ TCDB..");
			deleteServiceProfileFromOwnerAccount(serviceCode, accountId, tcdbConn);
			
            if (profile.getProductId() == PRODUCT_SELF_SERVE_SIM) {
				deleteRoamingProfile(serviceCode);
			}
            
            //delete blackList only if the operator is sprint and product is cc-global-sim
            if (operatorId.trim().equals("2") && profile.getProductId() == PRODUCT_CC_GLOBAL_SIM) {
				deleteRoamingProfileBlackList(serviceCode);
			}
						
			// Commit the changes
			if (conn != null) {
				conn.commit();
			}

			if (tcdbConn != null) {
				tcdbConn.commit();
			}
			
			// Put in Cache
			cache.evict(serviceCode);
			
			// Mark delete operation as completed
			success = true;
			
		
			// If Vegas TCDB write fails, we need not to rollback PROV & SJ TCDB changes. Only log the vegas failure in log file.
			try{
				tcdbVGConn = getTCDBVGConnection(serviceProviderId);
				if(tcdbVGConn != null){
					LOG.info("Deleting Service Credentials from Vegas TCDB..");
					deleteServiceProfileFromOwnerAccount(serviceCode, accountId, tcdbVGConn);
				}
				
				try{
					if(tcdbVGConn != null){
						tcdbVGConn.commit();
					}
				}catch(SQLException e){
					if(tcdbVGConn != null)
						tcdbVGConn.rollback();
					LOG.error("Error occured while commiting Vegas TCDB connection :"+ e);
				}
				
			}catch(Exception e){
				if(tcdbVGConn != null)
					tcdbVGConn.rollback();
				LOG.error("Service Profile Deletion in Vegas TCDB failed with error : " + e);
			}
			
			LOG.info("Committed updateServiceProfile changes to db");
		} catch (SQLException e) {
			rollBackConnection(conn, e);
			rollBackConnection(tcdbConn, e);

			throw new ServiceProfileException("Delete service profile failed due to db error", e);
		} catch (ServiceProfileNotFoundException e) {
			rollBackConnection(conn, e);
			rollBackConnection(tcdbConn, e);
			
			throw e;
		} catch (ServiceProfileValidationException e) {
			rollBackConnection(conn, e);
			rollBackConnection(tcdbConn, e);
			
			throw e;
		} catch (Exception e) {
			rollBackConnection(conn, e);
			rollBackConnection(tcdbConn, e);
			
			throw new ServiceProfileException("Delete service profile failed due to fatal error", e);
		} finally {
			DBConnectionManager.getInstance().cleanup(conn);
			DBConnectionManager.getInstance().cleanup(tcdbConn);
			DBConnectionManager.getInstance().cleanup(tcdbVGConn);
		}

		LOG.info("Returned success flag to the caller: " + success);
		
		return success;
	}

	private void validateServiceProfile(int accountId, long serviceCode, Connection conn) throws SQLException, ServiceProfileValidationException {
		LOG.info("Validating service profile for service code: " + serviceCode);
		
		validateDeviceCount(accountId, serviceCode, conn);
		validateMVNORequestCount(accountId, serviceCode, conn);
		validateTMORequestCount(accountId, serviceCode, conn);
		validateExclusiveIPAddressRange(accountId, serviceCode, conn);
		validateFreeIPAddressRange(accountId, serviceCode, conn);
		
		LOG.info("Service profile validated successfully for service code: " + serviceCode);
	}
	
	
	private void validateProductTechnology(int productId, List<TechnologyServiceProfile> technologyProfiles, Connection conn) 
			throws ServiceProfileValidationException, SQLException  {
		//NO need to validate in case no profile provided
		if(technologyProfiles == null ) {
			return ;
		}
		Product product = productCache.getProductById(productId);
		List<String> technologies = new ArrayList<String>();
		if(product.getTechnologies() != null) {
			technologies.addAll(Arrays.asList(product.getTechnologies()));
		} else {
			List<String> techList = getProductTechnologies(productId, conn);
			if(techList != null) {
				String[] techArra = new String[techList.size()] ;
				product.setTechnologies(techList.toArray(techArra));
				productCache.putToCache("" + product.getProductId(), product);
				technologies = techList ;
			} else 
				technologies.add(product.getTechnology());
		}
		if(product != null) {
			for (TechnologyServiceProfile techProfile : technologyProfiles) {
				if(!technologies.contains(techProfile.getTechnology().toUpperCase())) {
					throw new ServiceProfileValidationException("Invalid product and technology combination");
				}
			}
		}
	}
	
	
	/**
	 * Fetches list of technologies associated with a product
	 * 
	 * @param productId
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	private List<String> getProductTechnologies(int productId, Connection conn) throws SQLException {
		List<String> technologies = new ArrayList<String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			LOG.debug("Creating Prepared Statement to fetch TECHNOLOGY FOR PRODUCTID " + productId);

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_PRODUCT_TECHNOLOGY);

			LOG.debug("Setting parameters");
			ps.setLong(1, productId);
			rs = ps.executeQuery();
			while(rs.next()) {
				technologies.add(rs.getString("technology"));
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during fetching product technology list", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}
		return technologies ;
	}
	
	
	private List<Product> getProductTechnologyList(long accountId, int productId, Connection conn) throws SQLException {
		List<Product> technologies = new ArrayList<Product>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			LOG.debug("Creating Prepared Statement to fetch all product_technology " + productId);

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_ALL_PRODUCT_TECHNOLOGY);

			LOG.debug("Setting parameters");
			ps.setLong(1, productId);
			rs = ps.executeQuery();
			String carrierIdStr = getCarrierId(""+ accountId, ""+productId);
			int carrierId = -1 ;
			if (!StringUtils.isEmpty(carrierIdStr)) {
				carrierId = Integer.valueOf(carrierIdStr);
			}
			while(rs.next()) {
				Product prod = new Product();
				prod.setTechnology(rs.getString("technology"));
				if(carrierId > 0 && prod.getTechnology() != null && (!prod.getTechnology().equalsIgnoreCase("LTE"))) {
					prod.setCarrierId(carrierId);
				} else {
					prod.setCarrierId(rs.getInt("service_provider_id"));
				}
				prod.setMsisdnRange(rs.getString("msisdn_range"));
				prod.setStaticIpRange(rs.getString("static_ip_range"));
				prod.setProductId(rs.getInt("product_id"));
				prod.setProductName("");
				technologies.add(prod);
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during fetching product technology list", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}
		return technologies ;
	}
	
	
	private void validateDeviceCount(long accountId, long serviceCode, Connection conn) throws SQLException, ServiceProfileValidationException {
		LOG.info("Validating device count for service code: " + serviceCode);

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to fetch the device count in device table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_DEVICE_COUNT_BY_SERVICE_CODE);

			LOG.debug("Setting parameters");

			ps.setLong(1, accountId);
			ps.setLong(2, serviceCode);			
			
			LOG.info("Fetched device count in the db");

			rs = ps.executeQuery();
			
			if(rs.next())
			{
				count = rs.getInt(1);				
				LOG.info("Device Count for service code " + serviceCode + ": " + count);
			}
			
			if(count <= 0)
			{
				LOG.info("No Active Devices with the service profile: "+serviceCode);
			}
			else
			{
				LOG.error("Active devices found for serviceCode. Validation failed.");
				throw new ServiceProfileValidationException("Request cannot be completed. There are "+count+" active devices with this profile");
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during validating device count", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}		
	}
	
	private void validateMVNORequestCount(long accountId, long serviceCode, Connection conn) throws SQLException, ServiceProfileValidationException {
		LOG.info("Validating MVNO request count for service code: " + serviceCode);

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to fetch the MVNO request count in sprintmvno_request_v2 table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_MVNO_REQUEST_COUNT);

			LOG.debug("Setting parameters");

			ps.setLong(1, accountId);
			ps.setLong(2, serviceCode);			
			
			LOG.info("Fetched MVNO request count in the db");

			rs = ps.executeQuery();
			
			if(rs.next())
			{
				count = rs.getInt(1);				
				LOG.info("MVNO request Count for service code " + serviceCode + ": " + count);
			}
			
			if(count <= 0)
			{
				LOG.info("No Active MVNO Requests with the service profile: "+serviceCode);
			}
			else
			{
				LOG.error("Pending MVNO Requests found for serviceCode. Validation failed.");
				throw new ServiceProfileValidationException("Request cannot be completed. There are "+count+" pending MVNO Requests with this profile");
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during validating MVNO Requests", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}		
	}
	
	private void validateTMORequestCount(long accountId, long serviceCode, Connection conn) throws SQLException, ServiceProfileValidationException {
		LOG.info("Validating TMO request count for service code: " + serviceCode);

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to fetch the TMO request count in tmo_pending_request_v3 table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_TMO_REQUEST_COUNT);

			LOG.debug("Setting parameters");

			ps.setLong(1, accountId);
			ps.setLong(2, serviceCode);			
			
			LOG.info("Fetched TMO request count in the db");

			rs = ps.executeQuery();
			
			if(rs.next())
			{
				count = rs.getInt(1);				
				LOG.info("TMO request Count for service code " + serviceCode + ": " + count);
			}
			
			if(count <= 0)
			{
				LOG.info("No Active TMO Requests with the service profile: "+serviceCode);
			}
			else
			{
				LOG.error("Pending TMO Requests found for serviceCode. Validation failed.");
				throw new ServiceProfileValidationException("Request cannot be completed. There are "+count+" pending TMO Requests with this profile");
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during validating TMO Requests", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}		
	}
	
	private void validateExclusiveIPAddressRange(long accountId, long serviceCode, Connection conn) throws SQLException, ServiceProfileValidationException {
		LOG.info("Validating if exclusive IP Address range exists for service code: " + serviceCode);

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to fetch the exclusive ip address range count in ip_address_range_exclusive table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_IP_ADDR_RANGE_COUNT);

			LOG.debug("Setting parameters");

			ps.setLong(1, accountId);
			ps.setLong(2, serviceCode);			
			
			LOG.info("Fetched exclusive ip address range count in the db");

			rs = ps.executeQuery();
			
			if(rs.next())
			{
				count = rs.getInt(1);				
				LOG.info("Exclusive IP Address range count for service code " + serviceCode + ": " + count);
			}
			
			if(count <= 0)
			{
				LOG.info("Exclusive IP Address range does not exist with the service profile: "+serviceCode);
			}
			else
			{
				LOG.error("Exclusive IP Address range exists for serviceCode. Validation failed.");
				throw new ServiceProfileValidationException("Request cannot be completed. There are "+count+" Exclusive IP Address range with this profile");
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during validating Exclusive IP Address range", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}		
	}
	
	private void validateFreeIPAddressRange(long accountId, long serviceCode, Connection conn) throws SQLException, ServiceProfileValidationException {
		LOG.info("Validating if free IP Address range exists for service code: " + serviceCode);

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to fetch the free ip address range count in ip_address_free_exclusive table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_GET_IP_ADDR_FREE_COUNT);

			LOG.debug("Setting parameters");

			ps.setLong(1, accountId);
			ps.setLong(2, serviceCode);			
			
			LOG.info("Fetched free ip address range count in the db");

			rs = ps.executeQuery();
			
			if(rs.next())
			{
				count = rs.getInt(1);				
				LOG.info("Free IP Address range count for service code " + serviceCode + ": " + count);
			}
			
			if(count <= 0)
			{
				LOG.info("Free IP Address range does not exist with the service profile: "+serviceCode);
			}
			else
			{
				LOG.error("Free IP Address range exists for serviceCode. Validation failed.");
				throw new ServiceProfileValidationException("Request cannot be completed. There are "+count+" Free IP Address range with this profile");
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during validating Free IP Address range", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps, rs);
		}		
	}
	
	private void deleteServiceProfileFromOwnerAccount(long serviceCode, String accountId, Connection conn) throws SQLException {
		LOG.info("Deleting service profile from tcdb for service code: "+serviceCode);
		LOG.info("Deleting service code : " + conn);		
		Statement stmt = null;				
		
		try
		{
			stmt = conn.createStatement();	
			
			LOG.info("Creating tcdb sql statement: " + stmt +", to insert service code into TCDB");
			
			String acctListQuery = SQL_DELETE_FROM_ACCOUNT_LIST
											.replace(":service_code", String.valueOf(serviceCode));
	
			String ownerAcctQuery = SQL_DELETE_FROM_OWNER_ACCOUNT
					.replace(":account_id", accountId)
					.replace(":service_code", String.valueOf(serviceCode));

			stmt.addBatch(acctListQuery);		
			stmt.addBatch(ownerAcctQuery);
			
			LOG.info("Batched SQL to execute: "+ownerAcctQuery);
			LOG.info("Batched SQL to execute: "+acctListQuery);
	
			stmt.executeBatch();			
			
			LOG.info("Executed sql batch successfully in TCDB");
		}
		finally
		{
			DBConnectionManager.getInstance().cleanup(null, stmt);
		}
	}

	private void deleteCarrierForServiceProfile(long serviceCode, String accountId, Connection conn) throws SQLException {
		LOG.info("Deleting carrier info for service code: " + serviceCode);

		PreparedStatement ps = null;
		
		try {
			LOG.debug("Creating Prepared Statement to delete the data in cdma_carrier_account table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_CARRIER_ACCOUNT);

			LOG.debug("Setting parameters");

			ps.setLong(1, serviceCode);
			
			LOG.info("Deleting carrier info in the db");

			ps.executeUpdate();
			
			LOG.info("Deleted carrier info for serviceCode: " + serviceCode + " in database");
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting carrier info in table cdma_carrier_account", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
	}
	
	private int deleteBasicServiceProfileInfo(long serviceCode, String accountId, Connection conn) throws SQLException {
		LOG.info("Deleting basic service profile info for service code: " + serviceCode);

		PreparedStatement ps = null;
		int updateCount = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to delete the data in aersys_service_profile table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_AERSYS_SERVICE_PROFILE);

			LOG.debug("Setting parameters");

			ps.setLong(1, serviceCode);
			
			LOG.info("Deleting basic service profile info in the db");

			updateCount = ps.executeUpdate();
			
			LOG.info("Deleted basic service profile info for serviceCode: " + serviceCode + " in database");
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting basic service profile info in table aersys_service_profile", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
		
		return updateCount;
	}
	
	private List<ServiceProfile> getAllServiceProfiles() {
		List<ServiceProfile> serviceProfiles = new ArrayList<ServiceProfile>();

		LOG.info("getAllServiceProfiles: getting service profiles");
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAllServiceProfiles: Fetched db connection");
			
			// Load all accounts 
			ps = conn.prepareStatement(SQL_GET_SERVICE_PROFILES);
			
			LOG.debug("getAllServiceProfiles: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				ServiceProfile serviceProfile = new ServiceProfile();
				serviceProfile.setZoneSetId(rs.getInt("zone_set_id"));
				serviceProfile.setServiceCode(rs.getLong("service_code"));
				serviceProfile.setRegexMOVoice(rs.getString("mo_voice_regex"));
				serviceProfile.setServiceProfileName(rs.getString("service_name"));
				serviceProfile.setAccountId(rs.getString("account_id"));
				serviceProfile.setProductId(rs.getInt("product_id"));
				serviceProfile.setServiceProviderId(rs.getInt("service_provider_id"));
				serviceProfile.setTechnology(rs.getString("technology"));
				setEnrolledServices(serviceProfile, rs);
				
				// Add Service Profile
				serviceProfiles.add(serviceProfile);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getAllServiceProfiles", e);
			throw new GenericServiceException("Unable to getAllServiceProfiles", e);
		} finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}

		LOG.info("Returned serviceProfiles to the caller: "+serviceProfiles.size());
		
		return serviceProfiles;
	}

	private void putInCache(ServiceProfile[] serviceProfiles) {
		for (ServiceProfile serviceProfile : serviceProfiles) {
			cache.put(serviceProfile.getServiceCode(), serviceProfile);
		}
	}
	
    private void updateHssAndPccProfileId(String accountId, long serviceCode, String hssProfileId, String pccProfileId, Connection conn) throws SQLException {
        PreparedStatement ps = null;

		try {
			LOG.info("Creating prepared statement to update hss_profile_id and pcc_profile_id of service profile " + serviceCode + " in aersys_service_profile table");
			// Create SQL Statement
			ps = conn.prepareStatement(SQL_UPDATE_SERVICE_PROFILE_HSS_PCC_PROFILE);
			LOG.debug("Setting parameters");	
            ps.setString(1, hssProfileId);
            ps.setString(2, pccProfileId);
			ps.setLong(3, Long.parseLong(accountId));
            ps.setLong(4, serviceCode);
			// Execute the sql batch now
			ps.executeUpdate();
			LOG.info("Updated hss_profile_id and pcc_profile_id of service profile " + serviceCode + " in aersys_service_profile table.");
		} catch (SQLException sqle) {
			LOG.error("Exception during updating hss_profile_id and pcc_profile_id of service profile " + serviceCode + " in aersys_service_profile table.", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(null, ps);
		}
    }
    
    private void updateNotUsedFlag(String accountId, long serviceCode, boolean notUsed, Connection conn) throws SQLException {
        PreparedStatement ps = null;

        try {
            LOG.debug("Creating prepared statement to update not_used flag of service profile " + serviceCode + " in aersys_service_profile table");
            // Create SQL Statement
            ps = conn.prepareStatement(SQL_UPDATE_SERVICE_PROFILE_NOT_USED);
            LOG.debug("Setting parameters");
            ps.setInt(1, notUsed ? 0 : 1);
            ps.setLong(2, Long.parseLong(accountId));
            ps.setLong(3, serviceCode);
            // Execute the sql batch now
            ps.executeUpdate();
            LOG.debug("Updated not_used flag of service profile " + serviceCode + " in aersys_service_profile table.");
        } catch (SQLException sqle) {
            LOG.error("Exception during updating not_used of service profile " + serviceCode + " in aersys_service_profile table.", sqle);
            throw sqle;
        } finally {
            DBConnectionManager.getInstance().cleanup(null, ps);
        }
    }
    
	public static void main(String[] args) {
		System.out.println(SQL_GET_SERVICE_PROFILE_BY_ID);
	}
	
	public String getCarrierId(String accountId, String productId) throws SQLException{
		String carrierId = null;
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement(SQL_GET_CARRIER_ID);
			LOG.debug("SQL_GET_CARRIER_ID :"+SQL_GET_CARRIER_ID);
			
			ps.setInt(1, Integer.parseInt(accountId));
			ps.setInt(2, Integer.parseInt(productId));
			
			rs = ps.executeQuery();
			
			if(rs.next()){
				int cId = rs.getInt("CARRIER_ID");
				if(cId > 0)
					carrierId = String.valueOf(cId);
			}
		}catch (SQLException sqle) {
			LOG.error("Exception getting CarrierId for accountId:"+accountId+", productId:"+productId+" ,", sqle);
			throw sqle;
		} finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}
		
		return carrierId;
	}
	
	
	private void rollBackConnection (Connection conn, Exception e){
		if (conn != null) {
			try {
				conn.rollback();
				LOG.info("Rolled back changes due to previous exception " + e);
			} catch (SQLException e1) {
				LOG.error("Exception during roll back " + e1);
			}
		}
	}
	
	@Override
	public List<ServiceProfile> getServiceProfiles(String accountId, long serviceCode, String serviceName, String serviceLoginName) throws ServiceProfileException {
		LOG.info("getting service profiles by accountId = "+accountId+", serviceCode = "+serviceCode+", serviceName = "+serviceName+", serviceLoginName="+serviceLoginName);
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ServiceProfile> serviceProfiles = new ArrayList<ServiceProfile>();
		try{
			StringBuilder query = new StringBuilder("select asf.account_id, asf.service_code, asf.service_name, al.LOGIN_NAME, al.password from aersys_service_profile asf, account_list al ");
			query.append("where asf.service_code = al.service_code ");
			if(accountId != null && !accountId.isEmpty()){
				query.append("and asf.account_id = ? ");
			}
			if(serviceCode != 0){
				query.append("and asf.service_code = ? ");
			}
			if(serviceName != null && !serviceName.isEmpty()){
				query.append("and asf.service_name = ? ");
			}
			if(serviceLoginName != null && !serviceLoginName.isEmpty()){
				query.append("and al.LOGIN_NAME = ? ");
			}
			
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			LOG.debug("getServiceProfiles Query = query.toString()");
			ps = conn.prepareStatement(query.toString());
			
			int index = 1;
			
			if(accountId != null && !accountId.isEmpty()){
				ps.setLong(index, Long.parseLong(accountId));
				index++;
			}
			if(serviceCode != 0){
				ps.setLong(index, serviceCode);
				index++;
			}
			if(serviceName != null && !serviceName.isEmpty()){
				ps.setString(index, serviceName);
				index++;
			}
			if(serviceLoginName != null && !serviceLoginName.isEmpty()){
				ps.setString(index, serviceLoginName);
				index++;
			}
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				ServiceProfile serviceProfile = new ServiceProfile();
				long accId = rs.getLong("account_id");
				if(accId != 0){
					serviceProfile.setAccountId( String.valueOf(accId));
				}
				serviceProfile.setServiceProfileName(rs.getString("service_name")); 
				serviceProfile.setServiceCode(rs.getLong("service_code"));
				serviceProfile.setServiceUserName(rs.getString("LOGIN_NAME"));
				serviceProfile.setServicePassword(rs.getString("password"));
				
				serviceProfiles.add(serviceProfile);
			}
		}catch (SQLException e) {
			LOG.error("Exception during getServiceProfiles", e);
			throw new ServiceProfileException("Unable to getServiceProfiles", e);
		} finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}
		
		LOG.info("returning serviceProfiles ="+serviceProfiles);	
		return serviceProfiles;
	}

}
