package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public class ApiKeyPermission implements Serializable {
	private static final long serialVersionUID = -2772325415732650684L;
	private int permissionId;
	private String permissionValue;

	public int getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public String getPermissionValue() {
		return permissionValue;
	}

	public void setPermissionValue(String permissionValue) {
		this.permissionValue = permissionValue;
	}
}
