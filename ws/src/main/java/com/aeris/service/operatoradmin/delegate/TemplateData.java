package com.aeris.service.operatoradmin.delegate;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class TemplateData implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Map<String, String> data;
	private Map<String, String> defaultData;
	private Map<String, Integer> dimensions;
	private static int defaultWidth;
	private static final int DEFAULT_ITEM_WIDTH = 10;
	
	public TemplateData() {
		data = new HashMap<String, String>();
		defaultData = new HashMap<String, String>();
		dimensions = new HashMap<String, Integer>();
		
		setDefaultItemWidth(DEFAULT_ITEM_WIDTH);
	}
	
	public void put(String key, String value, String defaultText)
	{
		data.put(key, value);	
		defaultData.put(key, defaultText);
	}
	
	public void put(String key, String value)
	{
		put(key, value, "NONE");					
	}
	
	public String get(String key)
	{
		return data.get(key);
	}
	
	public String getDefaultValue(String key)
	{
		return defaultData.get(key);
	}
		
	public Map<String, Integer>  getItemDimensions()
	{
		return new HashMap<String, Integer>(dimensions);
	}
	
	public void setItemWidth(String keyPattern, Integer width)
	{
		if("*".equals(keyPattern))
		{
			setDefaultItemWidth(width);
		}
		else
		{
			if(keyPattern != null)
			{
				StringBuffer sb = new StringBuffer("^");
				String prefix = "";
				String suffix = "";
				
				if(keyPattern.startsWith("*"))
				{
					keyPattern = keyPattern.substring(1, keyPattern.length());
					prefix = ".*";
				}
				
				if(keyPattern.endsWith("*"))
				{
					keyPattern = keyPattern.substring(0, keyPattern.length() - 1);
					suffix = ".*$";
				}
				
				keyPattern = sb.append(prefix).append(keyPattern).append(suffix).toString();
				
				dimensions.put(keyPattern, width);	
			}
		}
	}
	
	public void setDefaultItemWidth(Integer width)
	{
		defaultWidth = width;
	}
	
	public int getDefaultItemWidth()
	{
		return defaultWidth;
	}
}
