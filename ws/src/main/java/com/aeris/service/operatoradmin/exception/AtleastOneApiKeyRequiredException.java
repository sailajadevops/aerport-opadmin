package com.aeris.service.operatoradmin.exception;

public class AtleastOneApiKeyRequiredException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public AtleastOneApiKeyRequiredException(String s) {
		super(s);
	}

	public AtleastOneApiKeyRequiredException(String ex, Throwable t) {
		super(ex, t);
	}
}
