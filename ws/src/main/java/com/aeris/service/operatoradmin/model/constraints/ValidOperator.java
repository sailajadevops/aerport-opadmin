package com.aeris.service.operatoradmin.model.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.math.NumberUtils;

import com.aeris.service.operatoradmin.OperatorAdminListener;
import com.aeris.service.operatoradmin.enums.IOperatorEnum;
import com.aeris.service.operatoradmin.enums.impl.OperatorEnum;

@Pattern(regexp = "[0-9]+") 
@Retention(RetentionPolicy.RUNTIME)
@Target({ FIELD, METHOD, PARAMETER, TYPE})
@Documented
@Constraint(validatedBy = {ValidOperator.IntegerValidator.class, ValidOperator.StringValidator.class})
public @interface ValidOperator {
	String message() default "{operatorId.number}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class IntegerValidator implements ConstraintValidator<ValidOperator, Integer> {
		
		OperatorEnum operatorEnum;

		@Override
		public void initialize(final ValidOperator enumClazz) {
			operatorEnum = OperatorAdminListener.sInjector.getInstance(OperatorEnum.class);
		}

		public boolean isValid(final Integer operator, final ConstraintValidatorContext constraintValidatorContext) {
			return operatorEnum.isValid(String.valueOf(operator));
		}
	}

	public class StringValidator implements ConstraintValidator<ValidOperator, String> {
		
		IOperatorEnum operatorEnum;

		@Override
		public void initialize(final ValidOperator enumClazz) {
			operatorEnum = OperatorAdminListener.sInjector.getInstance(OperatorEnum.class);
		}

		public boolean isValid(final String operator, final ConstraintValidatorContext constraintValidatorContext) {
			if(NumberUtils.isNumber(operator))
			{
				return operatorEnum.isValid(operator);
			}
			
			return false;
		}
	}
}
