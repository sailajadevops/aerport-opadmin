package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IAccountApplicationDAO;
import com.aeris.service.operatoradmin.enums.IApiKeyPermissionEnum;
import com.aeris.service.operatoradmin.enums.IResourceEnum;
import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.ApiKeyException;
import com.aeris.service.operatoradmin.exception.AtleastOneApiKeyRequiredException;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.InvalidApiKeyException;
import com.aeris.service.operatoradmin.model.AccountApplication;
import com.aeris.service.operatoradmin.model.ApiKey;
import com.aeris.service.operatoradmin.model.ApiKeyPermission;
import com.aeris.service.operatoradmin.model.Permission;
import com.aeris.service.operatoradmin.model.Resource;
import com.aeris.service.operatoradmin.model.ResourcePermission;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedGenerator;

/**
 * Fetches Rate Plan related information using stored procedures.
 * 
 * <li>
 * Used by the upper layers to fetch the current rate plan info for the account
 * id and device id</li>
 * 
 * <li>
 * To fetch all the rate plans/pool names for the specified account along with
 * their mrc values</li>
 * 
 * @author Srinivas Puranam
 */
public class AccountApplicationDAO implements IAccountApplicationDAO {
	private Logger LOG = LoggerFactory.getLogger(AccountApplicationDAO.class);

	static final String SQL_GET_API_KEYS = "select * " + 
											"from " + 
											"(select a.account_id, " +
											"b.app_id," + 
											"b.api_key, " +
											"b.expires, " + 
											"b.created_by, " + 
											"b.date_created, " +
											"b.last_modified_by, " +
											"b.last_modified_date, " +
											"b.description, " +
											"b.status, " +
											"c.operator_id, " +
											"ROWNUM as rnum " +
											"from ws_account_applications a, " +
											"ws_application b, " +
											"ws_accts_additional_info c " +
											"where a.account_id = c.account_id and " +
											"a.app_id = b.app_id " +
											"order by a.account_id)";

	static final String SQL_GET_API_KEYS_PAGINATION_CLAUSE = " and rnum between ? and ?";
	static final String SQL_SHOW_ACTIVE_ONLY_CLAUSE = " and (expires is NULL or expires > ?)";

	static final String SQL_CREATE_APP_ID = "insert into ws_application(api_key, description, created_by, date_created, last_modified_by, last_modified_date, expires, status) values(?, ?, ?, ?, ?, ?, ?, ?)";
	static final String SQL_CREATE_ACCT_APP_ID = "insert into ws_account_applications(app_id, account_id, created_by, date_created, last_modified_by, last_modified_date) values(?, ?, ?, ?, ?, ?)";
	static final String SQL_CREATE_APP_RES_PERMS = "insert into ws_app_res_permissions(app_id, resource_id, permission_id, created_by, date_created, last_modified_by, last_modified_date) values(?, ?, ?, ?, ?, ?, ?)";
	static final String SQL_DELETE_ACCT_APP_ID_KEY = "delete from ws_account_applications where app_id = :app_id and account_id = :account_id";
	static final String SQL_DELETE_APP_RES_PERMS_KEY = "delete from ws_app_res_permissions where app_id = :app_id";
	static final String SQL_DELETE_APP_ID_KEY = "delete from ws_application where app_id = :app_id";
	static final String SQL_GET_API_KEY_COUNT = "select count(a.app_id) as api_key_cnt from ws_application a, ws_account_applications b where a.app_id = b.app_id and b.account_id = ?";
	static final String SQL_GET_APP_ID_BY_API_KEY = "select a.app_id from ws_application a, ws_account_applications b where api_key = ? and a.app_id = b.app_id and b.account_id = ?";
	static final String SQL_UPDATE_API_KEY = "update ws_application set expires = ?, description = ?, last_modified_by = ?, last_modified_date = ?, status = ? where app_id = ?";
	static final String SQL_GET_RES_PERMISSIONS_BY_APP_ID = "select app_id, resource_id, permission_id from ws_app_res_permissions where app_id in (:app_ids) order by app_id";
	static final String SQL_GET_APP_IDS = "select a.app_id from ws_application a, ws_account_applications b where a.app_id = b.app_id and b.account_id = ?";
	static final String SQL_GET_MASTER_API_KEY_BY_ACCOUNT_ID = "select a.api_key, "
																+ "a.expires, " 
																+ "a.created_by, "
																+ "a.date_created, "
																+ "a.last_modified_by, "
																+ "a.last_modified_date, "
																+ "a.description, "
																+ "a.status "
																+ "from ws_application a "
																+ "inner join ws_account_applications b "
																+ "on b.account_id = ? "
																+ "and b.app_id = a.app_id "
																+ "inner join ws_app_res_permissions c "
																+ "on c.app_id = b.app_id "
																+ "inner join ws_resource d "
																+ "on c.resource_id = d.resource_id "
																+ "and d.resource_path = '/*' "
																+ "inner join ws_permission e "  
																+ "on e.permission_id = c.permission_id "
																+ "and e.description = '1111'";
	
	static final String IN_DEVICE_ID = "in_device_id";
	static final String IN_EXT_DEVICE_ID = "in_identifier";
	static final String IN_EXT_DEVICE_ID_TYPE = "in_identifier_type";
	static final String OUT_CURR_RATE_PLAN_NAME = "out_rate_plan_name";
	static final String OUT_CURR_EFFECTIVE_DATE = "out_start_date";
	static final String OUT_STATUS = "out_status";
	static final String OUT_MSG = "out_msg";

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");

	@Inject
	private IResourceEnum resourceCache;
	
	@Inject
	private IApiKeyPermissionEnum permissionCache;

	@Override
	public List<AccountApplication> getApiKeys(String start, String count, boolean listExpired, String... accountIds) throws AccountNotFoundException {
		List<AccountApplication> accountApplications = new ArrayList<AccountApplication>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Fetch the api keys for the specified accounts
			ps = createApiKeysByAccountIdsQuery(conn, start, count, listExpired, accountIds);

			LOG.debug("getApiKeys: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				AccountApplication application = new AccountApplication();
				application.setAccountId(rs.getString("account_id"));

				if (accountApplications.contains(application)) {
					// Update Existing Account with multiple api keys
					int index = accountApplications.indexOf(application);
					application = accountApplications.get(index);

					ApiKey apiKey = new ApiKey();
					apiKey.setAppId(rs.getInt("app_id"));
					apiKey.setApiKey(rs.getString("api_key"));
					apiKey.setCreatedBy(rs.getString("created_by"));
					apiKey.setLastModifiedBy(rs.getString("last_modified_by"));
					apiKey.setDescription(rs.getString("description"));
					apiKey.setStatus(rs.getString("status"));
					apiKey.setAccountId(Long.valueOf(rs.getString("account_id")));

					if (rs.getDate("expires") != null) {
						apiKey.setExpiryDate(DATE_FORMAT.format(rs.getDate("expires")));
					}

					if (rs.getDate("date_created") != null) {
						apiKey.setDateCreated(DATE_FORMAT.format(rs.getDate("date_created")));
					}

					if (rs.getDate("last_modified_date") != null) {
						apiKey.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
					}

					application.getApiKeys().add(apiKey);
				} else {
					// Add a new Account with api key
					ApiKey apiKey = new ApiKey();
					apiKey.setAppId(rs.getInt("app_id"));
					apiKey.setApiKey(rs.getString("api_key"));
					apiKey.setCreatedBy(rs.getString("created_by"));
					apiKey.setLastModifiedBy(rs.getString("last_modified_by"));
					apiKey.setDescription(rs.getString("description"));
					apiKey.setStatus(rs.getString("status"));
					apiKey.setAccountId(Long.valueOf(rs.getString("account_id")));

					if (rs.getDate("expires") != null) {
						apiKey.setExpiryDate(DATE_FORMAT.format(rs.getDate("expires")));
					}

					if (rs.getDate("date_created") != null) {
						apiKey.setDateCreated(DATE_FORMAT.format(rs.getDate("date_created")));
					}

					if (rs.getDate("last_modified_date") != null) {
						apiKey.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
					}

					application.getApiKeys().add(apiKey);

					application.setOperatorId(rs.getString("operator_id"));

					accountApplications.add(application);
				}
			}
			
			if (!accountApplications.isEmpty()) {
				LOG.info("Fetching Permissions...");			
				accountApplications = addApiKeyPermissions(accountApplications);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getApiKeys", e);
			throw new GenericServiceException("Unable to getApiKeys", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getApiKeys" + e.getMessage());
			throw e;
		}catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException in getApiKeys" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return accountApplications;
	}
	
	@Override
	public List<ApiKey> getAllApiKeys() throws AccountNotFoundException {
		List<ApiKey> apiKeyList = new ArrayList<ApiKey>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//TODO : can use this query if not found
		final String GET_APIKEY_DETAILS_FOR__ALL_ACCOUNTS = "select rsrc.resource_path, pmn.app_id, pmn.permission_id, accapps.account_id, appln.api_key, appln.status, accapps.created_by from"
				+ " aerisgen.ws_app_res_permissions pmn, aerisgen.ws_resource rsrc, ws_account_applications accapps, ws_application appln"
				+ " where rsrc.resource_id = pmn.resource_id and accapps.app_id = pmn.app_id and accapps.app_id = appln.app_id "
				+ " and appln.status = 'ACTIVE' and (appln.expires is null or appln.expires > SYS_EXTRACT_UTC (SYSTIMESTAMP)) "
				+ " order by accapps.account_id";

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Fetch the api keys for the specified accounts
			ps = createGetAllApiKeysQuery(conn);

			LOG.debug("getApiKeys: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
					ApiKey apiKey = new ApiKey();
					apiKey.setAccountId(rs.getLong("account_id"));
					apiKey.setOperatorId(rs.getInt("operator_id"));
					apiKey.setAppId(rs.getInt("app_id"));
					apiKey.setApiKey(rs.getString("api_key"));
					apiKey.setCreatedBy(rs.getString("created_by"));
					apiKey.setLastModifiedBy(rs.getString("last_modified_by"));
					apiKey.setDescription(rs.getString("description"));
					apiKey.setStatus(rs.getString("status"));
					if (rs.getDate("expires") != null) {
						apiKey.setExpiryDate(DATE_FORMAT.format(rs.getDate("expires")));
					}
					if (rs.getDate("date_created") != null) {
						apiKey.setDateCreated(DATE_FORMAT.format(rs.getDate("date_created")));
					}

					if (rs.getDate("last_modified_date") != null) {
						apiKey.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
					}
					apiKeyList.add(apiKey);
			}
		} catch (SQLException e) {
			LOG.error("Exception during getApiKeys", e);
			throw new GenericServiceException("Unable to getApiKeys", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getApiKeys" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return apiKeyList;
	}
	
	@Override
	public ApiKey getMasterApiKey(long accountId) throws ApiKeyException {
		ApiKey apiKey = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Fetch the api keys for the specified accounts
			ps = conn.prepareStatement(SQL_GET_MASTER_API_KEY_BY_ACCOUNT_ID);
			
			// Set Parameters
			ps.setLong(1, accountId);

			LOG.debug("getMasterApiKey: executed query: " + ps);
			rs = ps.executeQuery();

			if (rs.next()) {
				apiKey = new ApiKey();
				apiKey.setApiKey(rs.getString("api_key"));
				apiKey.setCreatedBy(rs.getString("created_by"));
				apiKey.setLastModifiedBy(rs.getString("last_modified_by"));
				apiKey.setDescription(rs.getString("description"));
				apiKey.setStatus(rs.getString("status"));

				if (rs.getDate("expires") != null) {
					apiKey.setExpiryDate(DATE_FORMAT.format(rs.getDate("expires")));
				}

				if (rs.getDate("date_created") != null) {
					apiKey.setDateCreated(DATE_FORMAT.format(rs.getDate("date_created")));
				}

				if (rs.getDate("last_modified_date") != null) {
					apiKey.setLastModifiedDate(DATE_FORMAT.format(rs.getDate("last_modified_date")));
				}
			} 
		} catch (SQLException e) {
			LOG.error("DB Error while getting master key", e);
			throw new ApiKeyException("DB Error while getting master key", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("Returned master api key to the caller: "+ ReflectionToStringBuilder.toString(apiKey));
		return apiKey;
	}

	private PreparedStatement createApiKeysByAccountIdsQuery(Connection conn, String start, String count, boolean listExpired,
			String... accountIds) throws SQLException {
		PreparedStatement ps;

		// Check if pagination parameters are valid
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		// Build Query with account ids
		StringBuilder query = new StringBuilder(SQL_GET_API_KEYS);
		query.append(" where account_id in (");

		for (int i = 0; i < accountIds.length; i++) {
			query.append("?");

			if (i != accountIds.length - 1) {
				query.append(",");
			}
		}

		query.append(")");

		// Append Pagination parameters
		if (paginated) {
			query.append(SQL_GET_API_KEYS_PAGINATION_CLAUSE);
		}

		// Show active api keys only if the listExpired = false
		if (!listExpired) {
			query.append(SQL_SHOW_ACTIVE_ONLY_CLAUSE);
		}

		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;

		// In clause for account ids
		while (i < accountIds.length) {
			String accountId = accountIds[i];
			ps.setString(++i, accountId);
		}

		// paginate the results if required
		if (paginated) {
			long rownumStart = Long.parseLong(start);
			long rownumEnd = Long.parseLong(start + count);

			ps.setLong(++i, rownumStart);
			ps.setLong(++i, rownumEnd);
		}

		if (!listExpired) {
			ps.setDate(++i, new java.sql.Date(new Date().getTime()));
		}

		return ps;
	}
	
	
	private PreparedStatement createGetAllApiKeysQuery(Connection conn) throws SQLException {
		PreparedStatement ps;
		// Check if pagination parameters are valid
		// Build Query with account ids
		StringBuilder query = new StringBuilder(SQL_GET_API_KEYS);
		query.append(" where (expires is NULL or expires > ?) ");
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());
		int i = 0;
		ps.setDate(++i, new java.sql.Date(new Date().getTime()));
		return ps;
	}
	

	@Override
	public ApiKey createNewApiKey(long accountId, List<ResourcePermission> permissions, Date requestedDate, String requestedUser,
			Date expires, String description, String status) throws ApiKeyException {
		String newApiKey = generateApiKey();
		return createNewApiKey(accountId, permissions, requestedDate, requestedUser, expires, description, status, newApiKey);
	}

	@Override
	public ApiKey createNewApiKey(long accountId, List<ResourcePermission> permissions, Date requestedDate, String requestedUser,
			Date expires, String description, String status, String newApiKey) throws ApiKeyException {
		ApiKey apiKey = null;
		Connection conn = null;
		
		LOG.info("Processing new api key creation....");
		
		try {
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			conn.setAutoCommit(false);

			// Create the newly generate api key uuid in the ws_application
			// table
			long appId = createApiKey(conn, newApiKey, description, requestedDate, requestedUser, expires, status);
			LOG.debug("Added api key: " + newApiKey + " to the ws_application table");

			createAcctAppId(conn, accountId, appId, requestedDate, requestedUser);
			LOG.debug("Updated ws_account_applications table with the new api key");

			List<Permission> retPermissions = createAppResPermissions(conn, appId, permissions, requestedDate, requestedUser);
			LOG.debug("Added Permissions in ws_app_res_permissions table for api key: " + newApiKey);

			// Commit the changes
			conn.commit();
			LOG.info("Committed createNewApiKey changes to db");

			apiKey = new ApiKey();
			apiKey.setApiKey(newApiKey);
			apiKey.setExpiryDate(DATE_FORMAT.format(expires));
			apiKey.setCreatedBy(requestedUser);
			apiKey.setDateCreated(DATE_FORMAT.format(requestedDate));
			apiKey.setLastModifiedDate(DATE_FORMAT.format(requestedDate));
			apiKey.setLastModifiedBy(requestedUser);
			apiKey.setDescription(description);
			apiKey.setPermissions(retPermissions);
			apiKey.setStatus(status);
			apiKey.setAccountId(accountId);

			LOG.info("Returning api key created...");
		} catch (SQLException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ApiKeyException("Create new api key failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn, null, null);
		}

		return apiKey;
	}
	
	@Override
	public ApiKey createNewApiKey(long accountId, List<ResourcePermission> permissions, Date requestedDate, String requestedUser,
			Date expires, String description, String status, String newApiKey, Connection conn) throws ApiKeyException {
		ApiKey apiKey = null;
		LOG.info("Processing new api key creation....");
		
		try {
			// Create the newly generate api key uuid in the ws_application
			// table
			long appId = createApiKey(conn, newApiKey, description, requestedDate, requestedUser, expires, status);
			LOG.debug("Added api key: " + newApiKey + " to the ws_application table");

			createAcctAppId(conn, accountId, appId, requestedDate, requestedUser);
			LOG.debug("Updated ws_account_applications table with the new api key");

			List<Permission> retPermissions = createAppResPermissions(conn, appId, permissions, requestedDate, requestedUser);
			LOG.debug("Added Permissions in ws_app_res_permissions table for api key: " + newApiKey);

			LOG.info("Inserted new api key into db");

			apiKey = new ApiKey();
			apiKey.setApiKey(newApiKey);
			apiKey.setExpiryDate(DATE_FORMAT.format(expires));
			apiKey.setCreatedBy(requestedUser);
			apiKey.setDateCreated(DATE_FORMAT.format(requestedDate));
			apiKey.setLastModifiedDate(DATE_FORMAT.format(requestedDate));
			apiKey.setLastModifiedBy(requestedUser);
			apiKey.setDescription(description);
			apiKey.setPermissions(retPermissions);
			apiKey.setStatus(status);
			apiKey.setAccountId(accountId);
			LOG.info("Returning api key created...");
		} catch (SQLException e) {
			LOG.info("Create new api key failed due to SQLException: "+e.getMessage());
			throw new ApiKeyException("Create new api key failed due to fatal error", e);
		} 

		return apiKey;
	}

	private List<Permission> createAppResPermissions(Connection conn, long appId, List<ResourcePermission> permissions, Date requestedDate,
			String requestedUser) throws SQLException {
		PreparedStatement ps = null;
		String permissionValue = "1111";// generate permissionValue for input
		List<Permission> retPermissions = new ArrayList<Permission>();

		try {
			ps = conn.prepareStatement(SQL_CREATE_APP_RES_PERMS);

			boolean permissionsApplied = false;

			for (ResourcePermission permission : permissions) {
				permissionValue = permission.getDBPermission();

				ApiKeyPermission apiKeyPermission = permissionCache.getPermissionByValue(permissionValue);
				Resource resource = resourceCache.getResourceByPath(permission.getResource());

				if (resource != null && apiKeyPermission != null) {
					LOG.info("Added permission '" + apiKeyPermission.getPermissionValue() + "' to access resource '"
							+ permission.getResource() + "' for appId: " + appId);
					addPermission(ps, appId, resource.getResourceId(), apiKeyPermission.getPermissionId(), requestedDate, requestedUser);

					Permission retPermission = new Permission();
					retPermission.setResource(resource.getResourcePath());
					retPermission.setPermissions(Integer.parseInt(permissionValue));
					retPermissions.add(retPermission);

					permissionsApplied = true;
				}
			}

			// run execute batch only when the permissions are batched for
			// execution
			if (permissionsApplied) {
				ps.executeBatch();
				LOG.info("Permissions created for appId: " + appId);
			} else {
				LOG.info("No Permissions granted to " + appId);
			}

		} catch (SQLException sqle) {
			LOG.error("Exception during Permissions creation" + sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}

		return retPermissions;
	}

	private void addPermission(PreparedStatement ps, long appId, int resourceId, int permissionId, Date requestedDate, String requestedUser)
			throws SQLException {
		ps.setLong(1, appId);
		ps.setInt(2, resourceId);
		ps.setInt(3, permissionId);
		ps.setString(4, requestedUser);
		ps.setDate(5, new java.sql.Date(requestedDate.getTime()));
		ps.setString(6, requestedUser);
		ps.setDate(7, new java.sql.Date(requestedDate.getTime()));

		ps.addBatch();
	}

	private void createAcctAppId(Connection conn, long accountId, long appId, Date requestedDate, String requestedUser) throws SQLException {
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(SQL_CREATE_ACCT_APP_ID);

			ps.setLong(1, appId);
			ps.setLong(2, accountId);
			ps.setString(3, requestedUser);
			ps.setDate(4, new java.sql.Date(requestedDate.getTime()));
			ps.setString(5, requestedUser);
			ps.setDate(6, new java.sql.Date(requestedDate.getTime()));

			ps.executeUpdate();

			LOG.info("app id: " + appId + " is added to account id: " + accountId);
		} catch (SQLException sqle) {
			LOG.error("Exception during updating app id and account id in ws_account_applications" + sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private long createApiKey(Connection conn, String newApiKey, String description, Date requestedDate, String requestedUser,
			Date expires, String status) throws SQLException {
		PreparedStatement ps = null;
		long appId = -1;
		ResultSet rs = null;

		try {
			ps = conn.prepareStatement(SQL_CREATE_APP_ID, new String[] { "app_id" });

			ps.setString(1, newApiKey);
			ps.setString(2, description);
			ps.setString(3, requestedUser);
			ps.setDate(4, new java.sql.Date(requestedDate.getTime()));
			ps.setString(5, requestedUser);
			ps.setDate(6, new java.sql.Date(requestedDate.getTime()));

			if (expires != null) {
				ps.setDate(7, new java.sql.Date(expires.getTime()));
			} else {
				ps.setNull(7, Types.DATE);
			}

			ps.setString(8, status);

			ps.executeUpdate();

			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				appId = rs.getLong(1);
			}

			LOG.info("Created API Key= " + newApiKey + " Returned App Id: " + appId);
		} catch (SQLException sqle) {
			LOG.error("Exception during creating api key in ws_application" + sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}

		return appId;
	}

	protected static String generateApiKey() {

		TimeBasedGenerator uuidGenerator = Generators.timeBasedGenerator();
		return uuidGenerator.generate().toString();
	}

	@Override
	public boolean deleteApiKey(long accountId, String apiKey, Date requestedDate, String requestedUser) throws InvalidApiKeyException,
			AtleastOneApiKeyRequiredException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int appId = 0;
		int apiKeyCnt = 0;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_API_KEY_COUNT);
			ps.setLong(1, accountId);

			LOG.debug("deleteApiKey: validating the no of api keys available for account: " + accountId);
			rs = ps.executeQuery();

			while (rs.next()) {
				apiKeyCnt = rs.getInt("api_key_cnt");
			}

			if (apiKeyCnt <= 1) {
				throw new AtleastOneApiKeyRequiredException("Atleast one api key is required for the account: " + accountId);
			}
		} catch (SQLException e) {
			LOG.error("Exception during deleteApiKey - validateApiKeyCount", e);
			throw new GenericServiceException("Exception during deleteApiKey", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in deleteApiKey - validateApiKeyCount " + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_APP_ID_BY_API_KEY);
			ps.setString(1, apiKey);
			ps.setLong(2, accountId);

			LOG.debug("deleteApiKey: executed query to fetch app_id by api_key: " + apiKey);
			rs = ps.executeQuery();

			while (rs.next()) {
				appId = rs.getInt("app_id");
			}

			if (appId == 0) {
				throw new InvalidApiKeyException("Api Key is incorrect: " + apiKey);
			}
		} catch (SQLException e) {
			LOG.error("Exception during deleteApiKey - getAppIdByApiKey", e);
			throw new GenericServiceException("Exception during deleteApiKey", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in deleteApiKey - getAppIdByApiKey " + e.getMessage());
			throw e;
		} catch (InvalidApiKeyException e) {
			LOG.error("InvalidApiKeyException in deleteApiKey - getAppIdByApiKey " + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		Statement stmt = null;
		boolean success = false;

		try {
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			stmt = conn.createStatement();

			// Delete Permissions
			String deletePermsQuery = SQL_DELETE_APP_RES_PERMS_KEY.replace(":app_id", String.valueOf(appId));

			LOG.info("Executing query: " + deletePermsQuery);
			stmt.addBatch(deletePermsQuery);

			// Delete Account Application Id Link
			String deleteAcctAppIdQuery = SQL_DELETE_ACCT_APP_ID_KEY.replace(":app_id", String.valueOf(appId)).replace(":account_id",
					String.valueOf(accountId));
			;

			LOG.info("Executing query: " + deleteAcctAppIdQuery);
			stmt.addBatch(deleteAcctAppIdQuery);

			// Delete Application Id and API Key
			String deleteAppIdQuery = SQL_DELETE_APP_ID_KEY.replace(":app_id", String.valueOf(appId));

			LOG.info("Executing query: " + deleteAppIdQuery);
			stmt.addBatch(deleteAppIdQuery);

			stmt.executeBatch();

			// Commit the changes
			conn.commit();

			LOG.info("Delete API Key successful.");

			success = true;
		} catch (SQLException e) {

			try {
				conn.rollback();
				LOG.info("Rolled back changes due to previous exception " + e);
			} catch (SQLException e1) {
				LOG.error("Exception during roll back " + e1);
			}

			throw new GenericServiceException("Delete API Key failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn, stmt);
		}

		return success;
	}
	
	@Override
	public boolean deleteApiKeys(long accountId, Date requestedDate, String requestedUser) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Integer> appIds = new ArrayList<Integer>();
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_APP_IDS);
			ps.setLong(1, accountId);

			LOG.debug("deleteApiKey: executed query to fetch app_ids for account: " + accountId);
			rs = ps.executeQuery();

			while (rs.next()) {
				appIds.add(rs.getInt("app_id"));
			}
		} catch (SQLException e) {
			LOG.error("Exception during deleteApiKey - getAppIdByApiKey", e);
			throw new GenericServiceException("Exception during deleteApiKey", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in deleteApiKey - getAppIdByApiKey " + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		Statement stmt = null;
		boolean success = false;

		try {
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			stmt = conn.createStatement();

			for (int appId: appIds) {
				// Delete Permissions
				String deletePermsQuery = SQL_DELETE_APP_RES_PERMS_KEY.replace(":app_id", String.valueOf(appId));

				LOG.info("Executing query: " + deletePermsQuery);
				stmt.addBatch(deletePermsQuery);
			}
			
			for (int appId: appIds) {
				// Delete Account Application Id Link
				String deleteAcctAppIdQuery = SQL_DELETE_ACCT_APP_ID_KEY.replace(":app_id", String.valueOf(appId)).replace(":account_id",
						String.valueOf(accountId));

				LOG.info("Executing query: " + deleteAcctAppIdQuery);
				stmt.addBatch(deleteAcctAppIdQuery);
			}
			
			for (int appId: appIds) {
				// Delete Application Id and API Key
				String deleteAppIdQuery = SQL_DELETE_APP_ID_KEY.replace(":app_id", String.valueOf(appId));

				LOG.info("Executing query: " + deleteAppIdQuery);
				stmt.addBatch(deleteAppIdQuery);
			}
			
			stmt.executeBatch();

			// Commit the changes
			conn.commit();

			LOG.info("Delete API Keys successfully for account");

			success = true;
		} catch (SQLException e) {

			try {
				conn.rollback();
				LOG.info("Rolled back changes due to previous exception " + e);
			} catch (SQLException e1) {
				LOG.error("Exception during roll back " + e1);
			}

			throw new GenericServiceException("Delete API Keys failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn, stmt);
		}

		return success;
	}

	@Override
	public ApiKey updateApiKey(long accountId, String apiKey, List<ResourcePermission> permissions, Date requestedDate,
			String requestedUser, Date expiryDate, String description, String status) throws InvalidApiKeyException,
			AtleastOneApiKeyRequiredException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int appId = 0;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			ps = conn.prepareStatement(SQL_GET_APP_ID_BY_API_KEY);
			ps.setString(1, apiKey);
			ps.setLong(2, accountId);

			LOG.debug("updateApiKey: executed query to fetch app_id by api_key: " + apiKey);
			rs = ps.executeQuery();

			while (rs.next()) {
				appId = rs.getInt("app_id");
			}

			if (appId == 0) {
				throw new InvalidApiKeyException("Api Key is incorrect: " + apiKey);
			}
		} catch (SQLException e) {
			LOG.error("Exception during updateApiKey - getAppIdByApiKey", e);
			throw new GenericServiceException("Exception during updateApiKey", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in updateApiKey - getAppIdByApiKey " + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		ApiKey apiKeyBean = null;

		try {
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();

			// Update Validity
			ps = conn.prepareStatement(SQL_UPDATE_API_KEY);
			ps.setDate(1, new java.sql.Date(expiryDate.getTime()));
			ps.setString(2, description);
			ps.setString(3, requestedUser);
			ps.setDate(4, new java.sql.Date(requestedDate.getTime()));
			ps.setString(5, status);
			ps.setLong(6, appId);

			LOG.debug("updateApiKey: updating the expiry date for app id: " + appId);
			int count = ps.executeUpdate();

			if (count <= 0) {
				LOG.error("updateApiKey: updating failed for app id: " + appId);
				throw new InvalidApiKeyException("Api Key is incorrect: " + apiKey);
			}

			// Delete Old Permissions
			count = deleteOldAppResPermissions(conn, apiKey, appId);
			LOG.info("updateApiKey: deleted " + count + " resource permissions for api key: " + apiKey);

			// Create Permissions
			List<Permission> retPermissions = createAppResPermissions(conn, appId, permissions, requestedDate, requestedUser);
			LOG.debug("Added Permissions in ws_app_res_permissions table for api key: " + apiKey);

			// Commit the changes
			conn.commit();

			apiKeyBean = new ApiKey();
			apiKeyBean.setApiKey(apiKey);
			apiKeyBean.setExpiryDate(DATE_FORMAT.format(expiryDate));
			apiKeyBean.setCreatedBy(requestedUser);
			apiKeyBean.setDateCreated(DATE_FORMAT.format(requestedDate));
			apiKeyBean.setLastModifiedDate(DATE_FORMAT.format(requestedDate));
			apiKeyBean.setLastModifiedBy(requestedUser);
			apiKeyBean.setDescription(description);
			apiKeyBean.setPermissions(retPermissions);
			apiKeyBean.setStatus(status);
			apiKeyBean.setAccountId(accountId);

			LOG.info("API Key updated successfully.");
		} catch (SQLException e) {

			try {
				conn.rollback();
				LOG.info("Rolled back changes due to previous exception " + e);
			} catch (SQLException e1) {
				LOG.error("Exception during roll back " + e1);
			}

			throw new GenericServiceException("update API Key failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn, ps);
		}

		return apiKeyBean;
	}

	private int deleteOldAppResPermissions(Connection conn, String apiKey, int appId) throws SQLException {
		PreparedStatement ps = null;
		int count = 0;

		try {
			String deletePermsQuery = SQL_DELETE_APP_RES_PERMS_KEY.replace(":app_id", String.valueOf(appId));
			LOG.info("Deleting Old Permissions. Executing query: " + deletePermsQuery);

			ps = conn.prepareStatement(deletePermsQuery);
			count = ps.executeUpdate();

			if (count > 0) {
				LOG.info("updateApiKey: deleted old permissions succcessfully for api key: " + apiKey);
			} else {
				LOG.info("updateApiKey: no permissions available for api key: " + apiKey);
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during Permissions deletion" + sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}

		return count;
	}

	@Override
	public List<Resource> findResourcesByAccount(long accountId) {
		LOG.info("Fetching resouces for account id " + accountId + " from cache");
		return resourceCache.getResources();
	}

	@Override
	public Map<String, List<Permission>> getResourcePermissionsByAppId(List<Integer> appIds) {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Map<String, List<Permission>> permissions = new HashMap<String, List<Permission>>();

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			stmt = conn.createStatement();

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < appIds.size(); i++) {
				sb.append(appIds.get(i));

				if (i != appIds.size() - 1) {
					sb.append(",");
				}
			}

			String resourcePermissionsQuery = SQL_GET_RES_PERMISSIONS_BY_APP_ID.replace(":app_ids", sb.toString());

			LOG.debug("getResourcePermissionsByAppId: executed query: " + resourcePermissionsQuery);
			rs = stmt.executeQuery(resourcePermissionsQuery);

			while (rs.next()) {
				String appId = String.valueOf(rs.getInt("app_id"));
				int resourceId = rs.getInt("resource_id");
				int permissionId = rs.getInt("permission_id");

				String resource = resourceCache.getResourceById(resourceId).getResourcePath();
				String permissionValue = permissionCache.getPermissionById(permissionId).getPermissionValue();

				LOG.info("adding permission for appId " + appId + "= resource: " + resource + " permission: " + permissionValue);

				Permission permission = new Permission();
				permission.setResource(resource);
				permission.setPermissions(Integer.parseInt(permissionValue));

				if (permissions.containsKey(appId)) {
					LOG.info("Granting permission " + permission + " to appId: " + appId);
					permissions.get(appId).add(permission);
				} else {
					LOG.info("Granting permission " + permission + " to appId: " + appId);
					List<Permission> newPermissions = new ArrayList<Permission>();
					newPermissions.add(permission);

					permissions.put(appId, newPermissions);
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception during getResourcePermissionsByAppId", e);
			throw new GenericServiceException("Unable to getResourcePermissionsByAppId", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getResourcePermissionsByAppId" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}

		return permissions;
	}
	
	
	private List<AccountApplication> addApiKeyPermissions(List<AccountApplication> accountApplications) throws AccountNotFoundException {
		List<Integer> appIds = new ArrayList<Integer>();
		for (AccountApplication accountApplication : accountApplications) {
			List<ApiKey> apiKeys = accountApplication.getApiKeys();
			for (ApiKey apiKey : apiKeys) {
				appIds.add(apiKey.getAppId());
			}
		}			
		LOG.info("Calling getResourcePermissionsByAppId for appIds: "+appIds);
		Map<String, List<Permission>> permissions = this.getResourcePermissionsByAppId(appIds);
		
		for (AccountApplication accountApplication : accountApplications) {
			List<ApiKey> apiKeys = accountApplication.getApiKeys();
			
			for (ApiKey apiKey : apiKeys) { 
				String appId = String.valueOf(apiKey.getAppId());
				
				if(permissions.containsKey(appId)) {
					LOG.info("Setting Permissions for apiKey: "+apiKey.getApiKey());
					apiKey.setPermissions(permissions.get(appId));
				} else {
					LOG.warn("No Permissions for apiKey: "+apiKey.getApiKey());
				}
			}
		}
		
		return accountApplications;
	}
	
	
	/**
	 * fetches accountId associated with the apiKey
	 * @param apiKey
	 * @return
	 */
	public long getAccountIdForApiKey(String apiKey) {
		long accountId = -1l;
		
		String query = "select  account_id from ws_account_applications where app_id = (select app_id from ws_application where api_key = '" + apiKey + "')" ;
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			ps = conn.prepareStatement(query);
			LOG.info("ApiKey query for apiKey [ " + apiKey + " ] =" + query);
			rs = ps.executeQuery();
			while (rs.next()) {
				accountId = rs.getLong("account_id");
				break ;
			}

		} catch (SQLException sqle) {
			LOG.error("Exception in fetching accountId for apiKey=" + apiKey, sqle);
		} finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}
		
		return accountId ;
	}
	
	
	public List<Long> getAccountHierarchy(long accountId) {
		List<Long> accountIdList = new ArrayList<Long>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "select child_account_id from aerisgen.ACCOUNT_HIERARCHY where parent_account_id = ? " ;
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			ps = conn.prepareStatement(query);
			ps.setLong(1, accountId);
			rs = ps.executeQuery();
			while(rs.next()) {
				accountIdList.add(rs.getLong("child_account_id"));
			}
		} catch (Exception e) {
			LOG.error("Exception while fetching Account Hierarchy for accountId=" + accountId , e);
		}finally {
			DBConnectionManager.getInstance().cleanup(conn, ps, rs);
		}
		return accountIdList;
	}
}
