package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum RatePlanPeriodType implements Serializable {
	MONTHLY(1), VARIABLE(2);
	int value;

	RatePlanPeriodType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int value) {
		RatePlanPeriodType[] statuses = values();

		for (RatePlanPeriodType ratePlanPeriodType : statuses) {
			if (ratePlanPeriodType.getValue() == value) {
				return true;
			}
		}

		return false;
	}

	public static RatePlanPeriodType fromValue(int value) {
		RatePlanPeriodType[] statuses = values();

		for (RatePlanPeriodType ratePlanPeriodType : statuses) {
			if (ratePlanPeriodType.getValue() == value) {
				return ratePlanPeriodType;
			}
		}

		return null;
	}

	public static RatePlanPeriodType fromName(String value) {
		RatePlanPeriodType[] statuses = values();

		for (RatePlanPeriodType ratePlanPeriodType : statuses) {
			if (ratePlanPeriodType.name().equalsIgnoreCase(value)) {
				return ratePlanPeriodType;
			}
		}

		return null;
	}
}
