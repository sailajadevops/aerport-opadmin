package com.aeris.service.operatoradmin.exception;

public class SIMPackException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SIMPackException(String s) {
		super(s);
	}

	public SIMPackException(String ex, Throwable t) {
		super(ex, t);
	}
}
