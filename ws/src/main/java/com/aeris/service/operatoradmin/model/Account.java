package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class Account implements Serializable {
	private static final long serialVersionUID = -3287105098246119100L;
	private long accountId;
	private String accountName;
	private int includedAlertProfiles;
	private Contact primaryContact;
	private List<Contact> additionalContacts;
	private List<String> productIds;
	private InvoiceAddress invoiceAddress;
	private BillingDetails billingDetails;
	private AccountType type;
	private AccountStatus status;
	private ApprovalStatus approvalStatus;
	private Region region;
	private String parentAccountId;
	private String netSuiteId;
	private BillableStatus billableStatus;
	private String carrierAccountId;
	private String specialInstructions;
	private String tags;
	private int operatorId;
	private String webAddress;
	private String verticalId;
	private int maxPortalAccounts;
	private int maxScheduledReports;
	private int maxReportHistory;
	private String[] serviceImpairmentEmail;
	private String[] myAlertsEmail;
	private String[] scheduledReportsEmail;
    private String[] supportContactEmails;
	private String apiKey;
	private String agentId;
	private List<AccountOverageBucket> accountOverageBucket;
    private Date lastModifiedDate;
    private List<String> auxServices;
    private List<Integer> featureCodes;
    private String distributorId;
    private Distributor distributor;
    private String accountType;
    private boolean enableCreateWholesale;

    private long creditLimit;
    
    public long getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(long creditLimit) {
		this.creditLimit = creditLimit;
    }
	
    private int contractId;
    
    public int getContractId() {
		return contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public boolean isEnableCreateWholesale() {
		return enableCreateWholesale;
	}

	public void setEnableCreateWholesale(boolean enableCreateWholesale) {
		this.enableCreateWholesale = enableCreateWholesale;
	}

	public String getWholesaleAccountId() {
		return wholesaleAccountId;
	}

	public void setWholesaleAccountId(String wholesaleAccountId) {
		this.wholesaleAccountId = wholesaleAccountId;
	}

	private String wholesaleAccountId;

	public Distributor getDistributor() {
		return distributor;
	}

	public void setDistributor(Distributor distributor) {
		this.distributor = distributor;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public int getIncludedAlertProfiles() {
		return includedAlertProfiles;
	}

	public void setIncludedAlertProfiles(int includedAlertProfiles) {
		this.includedAlertProfiles = includedAlertProfiles;
	}

	public Contact getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(Contact primaryContact) {
		this.primaryContact = primaryContact;
	}

	public List<Contact> getAdditionalContacts() {
		if (additionalContacts == null) {
			additionalContacts = new ArrayList<Contact>();
		}

		return additionalContacts;
	}

	public void setAdditionalContacts(List<Contact> additionalContacts) {
		this.additionalContacts = additionalContacts;
	}

	public List<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<String> productIds) {
		this.productIds = productIds;
	}

	public InvoiceAddress getInvoiceAddress() {
		return invoiceAddress;
	}

	public void setInvoiceAddress(InvoiceAddress invoiceAddress) {
		this.invoiceAddress = invoiceAddress;
	}

	public BillingDetails getBillingDetails() {
		return billingDetails;
	}

	public void setBillingDetails(BillingDetails billingDetails) {
		this.billingDetails = billingDetails;
	}

	public AccountType getType() {
		return type;
	}

	public void setType(AccountType type) {
		this.type = type;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public ApprovalStatus getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(ApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getParentAccountId() {
		return parentAccountId;
	}

	public void setParentAccountId(String parentAccountId) {
		this.parentAccountId = parentAccountId;
	}

	public String getNetSuiteId() {
		return netSuiteId;
	}

	public void setNetSuiteId(String netSuiteId) {
		this.netSuiteId = netSuiteId;
	}

	public BillableStatus getBillableStatus() {
		return billableStatus;
	}

	public void setBillableStatus(BillableStatus billableStatus) {
		this.billableStatus = billableStatus;
	}

	public String getCarrierAccountId() {
		return carrierAccountId;
	}

	public void setCarrierAccountId(String carrierAccountId) {
		this.carrierAccountId = carrierAccountId;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public String getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(String verticalId) {
		this.verticalId = verticalId;
	}

	public int getMaxPortalAccounts() {
		return maxPortalAccounts;
	}

	public void setMaxPortalAccounts(int maxPortalAccounts) {
		this.maxPortalAccounts = maxPortalAccounts;
	}

	public int getMaxScheduledReports() {
		return maxScheduledReports;
	}

	public void setMaxScheduledReports(int maxScheduledReports) {
		this.maxScheduledReports = maxScheduledReports;
	}

	public int getMaxReportHistory() {
		return maxReportHistory;
	}

	public void setMaxReportHistory(int maxReportHistory) {
		this.maxReportHistory = maxReportHistory;
	}

	public String[] getServiceImpairmentEmail() {
		return serviceImpairmentEmail;
	}

	public void setServiceImpairmentEmail(String[] serviceImpairmentEmail) {
		this.serviceImpairmentEmail = serviceImpairmentEmail;
	}

	public String[] getMyAlertsEmail() {
		return myAlertsEmail;
	}

	public void setMyAlertsEmail(String[] myAlertsEmail) {
		this.myAlertsEmail = myAlertsEmail;
	}

	public String[] getScheduledReportsEmail() {
		return scheduledReportsEmail;
	}

	public void setScheduledReportsEmail(String[] scheduledReportsEmail) {
		this.scheduledReportsEmail = scheduledReportsEmail;
	}

    public String[] getSupportContactEmails() {
        return supportContactEmails;
    }

    public void setSupportContactEmails(String[] supportContactEmails) {
        this.supportContactEmails = supportContactEmails;
    }

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public List<AccountOverageBucket> getAccountOverageBucket() {
		return accountOverageBucket;
	}

	public void setAccountOverageBucket(List<AccountOverageBucket> accountOverageBucket) {
		this.accountOverageBucket = accountOverageBucket;
	}

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public List<String> getAuxServices() {
		return auxServices;
	}

	public void setAuxServices(List<String> auxServices) {
		this.auxServices = auxServices;
	}

	public List<Integer> getFeatureCodes() {
		return featureCodes;
	}

	public void setFeatureCodes(List<Integer> featureCodes) {
		this.featureCodes = featureCodes;
	}

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }
}
