package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public class SMSFormat implements Serializable {
	private static final long serialVersionUID = 1584710931060737335L;
	private int formatCode;
	private String formatString;

	public int getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(int formatCode) {
		this.formatCode = formatCode;
	}

	public String getFormatString() {
		return formatString;
	}

	public void setFormatString(String formatString) {
		this.formatString = formatString;
	}
}
