package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.ProvisionTriggerSettings;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanOverageBucket;
import com.aeris.service.operatoradmin.model.RatePlanPaymentType;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.RatePlanTier;
import com.aeris.service.operatoradmin.model.RatePlanType;
import com.aeris.service.operatoradmin.model.SuspendTriggerSettings;
import com.aeris.service.operatoradmin.model.ZoneRatePlan;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;
import com.aeris.service.operatoradmin.model.constraints.NotPast;
import com.aeris.service.operatoradmin.model.constraints.ValidPacketRoundingPolicy;
import com.aeris.service.operatoradmin.model.constraints.ValidPoolingPolicy;
import com.aeris.service.operatoradmin.model.constraints.ValidateRatePlanDate;

@XmlRootElement
@ValidateRatePlanDate(message="{endDate.after.startDate}")
public class UpdateRatePlanRequest implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;

	@Email(message = "{userId.email}")
	@NotNull(message = "{userId.notnull}")
	private String userId;

	@NotNull(message = "{ratePlanLabel.notnull}")
	@Pattern(regexp = "^[^*]*$", message = "{ratePlanLabel.pattern}")
	@Size(min = 1, max = 60, message = "{ratePlanLabel.size}")
	private String ratePlanLabel;

	@Enumeration(value = RatePlanType.class, message = "{ratePlanType.enum}")
	private RatePlanType ratePlanType;

	@Enumeration(value = RatePlanPaymentType.class, message = "{paymentType.enum}")
	private RatePlanPaymentType paymentType;

	@Enumeration(value = RatePlanAccessType.class, message = "{accessType.enum}")
	private RatePlanAccessType accessType;

	@Enumeration(value = RatePlanPeriodType.class, message = "{periodType.enum}")
	private RatePlanPeriodType periodType;

	@NotNull(message = "{accessFeeMonths.notnull}")
	@Min(value = 1, message = "{accessFeeMonths.number}")
	private int accessFeeMonths;

	@NotNull(message = "{expireIncluded.notnull}")
	private boolean expireIncluded;
	
	@NotNull(message = "{includedPeriodMonths.notnull}")
	private int includedPeriodMonths;
	
	@ValidPoolingPolicy
	private int devicePoolingPolicyId;
	
	@ValidPacketRoundingPolicy
	private int packetDataRoundingPolicyId;

	@Enumeration(value = RatePlanStatus.class, message = "{status.enum}")
	private RatePlanStatus status;

	@NotNull(message = "{productId.notnull}")
	@Min(value = 1, message = "{productId.number}")
	private int productId;

	private int tierCriteria;

	@NotNull(message = "{homeZoneId.notnull}")
	@Min(value = 1, message = "{homeZoneId.number}")
	private int homeZoneId;

	@NotNull(message = "{description.notnull}")
	private String description;

	private String specialNotes;

	@NotNull(message = "{carrierRatePlanId.notnull}")
	@Min(value = 1, message = "{carrierRatePlanId.number}")
	private int carrierRatePlanId;

	private long accountId; 

	/*TO DO - Check that can we use @NotPast validation here instead of validating it in DAO layer ? - Comment by Santosh G*/
	@NotNull(message = "{startDate.notnull}")
	private Date startDate;

	@NotNull(message = "{endDate.notnull}")
	@NotPast(message = "{endDate.notpast}")
	private Date endDate;

	@NotNull(message = "{currencyName.notnull}")
	private String currencyName;

	@NotNull(message = "{overrideRoamingIncluded.notnull}")
	private boolean overrideRoamingIncluded;

	@Valid
	private RatePlanFeeDetails ratePlanFeeDetails;

	@Valid
	private ProvisionTriggerSettings provisionTriggerSettings; 

	@Valid
	private SuspendTriggerSettings suspendTriggerSettings;

	@Valid
	private List<ZoneRatePlan> zoneRatePlanSettings;
	
	@Valid
	private List<RatePlanTier> ratePlanTiers;
	
	private int proRateCancellationFee;
	private int contractTerm;
	private int cancellationFeeReductionInterval;
	private int autoCancelAtTermEnd;
	private int accountForDaysInProvisionState;
	private int accountForDaysInSuspendState;
	private long wholesaleRateplanId;
	
	@Valid
	private List<RatePlanOverageBucket> ratePlanOverageBucket;
    
	private boolean roamingIncluded;
	
	@NotNull(message = "{zoneSetId.notnull}")
	private int zoneSetId;
    
    private String technology;
    
    private boolean waterfallEnabled ;

        private boolean isWholesaleRatePlan;
        
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRatePlanLabel() {
		return ratePlanLabel;
	}

	public void setRatePlanLabel(String ratePlanLabel) {
		this.ratePlanLabel = ratePlanLabel;
	}

	public RatePlanType getRatePlanType() {
		return ratePlanType;
	}

	public void setRatePlanType(RatePlanType ratePlanType) {
		this.ratePlanType = ratePlanType;
	}

	public RatePlanPaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(RatePlanPaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public RatePlanAccessType getAccessType() {
		return accessType;
	}

	public void setAccessType(RatePlanAccessType accessType) {
		this.accessType = accessType;
	}

	public RatePlanPeriodType getPeriodType() {
		return periodType;
	}

	public void setPeriodType(RatePlanPeriodType periodType) {
		this.periodType = periodType;
	}

	public int getAccessFeeMonths() {
		return accessFeeMonths;
	}

	public void setAccessFeeMonths(int accessFeeMonths) {
		this.accessFeeMonths = accessFeeMonths;
	}

	public boolean isExpireIncluded() {
		return expireIncluded;
	}

	public void setExpireIncluded(boolean expireIncluded) {
		this.expireIncluded = expireIncluded;
	}
	
	public int getIncludedPeriodMonths() {
		return includedPeriodMonths;
	}
	
	public void setIncludedPeriodMonths(int includedPeriodMonths) {
		this.includedPeriodMonths = includedPeriodMonths;
	}
		
	public int getDevicePoolingPolicyId() {
		return devicePoolingPolicyId;
	}
	
	public void setDevicePoolingPolicyId(int devicePoolingPolicyId) {
		this.devicePoolingPolicyId = devicePoolingPolicyId;
	}
	
	public int getPacketDataRoundingPolicyId() {
		return packetDataRoundingPolicyId;
	}
	
	public void setPacketDataRoundingPolicyId(int packetDataRoundingPolicyId) {
		this.packetDataRoundingPolicyId = packetDataRoundingPolicyId;
	}

	public RatePlanStatus getStatus() {
		return status;
	}

	public void setStatus(RatePlanStatus status) {
		this.status = status;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getTierCriteria() {
		return tierCriteria;
	}

	public void setTierCriteria(int tierCriteria) {
		this.tierCriteria = tierCriteria;
	}
	
	public int getHomeZoneId() {
		return homeZoneId;
	}

	public void setHomeZoneId(int homeZoneId) {
		this.homeZoneId = homeZoneId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSpecialNotes() {
		return specialNotes;
	}

	public void setSpecialNotes(String specialNotes) {
		this.specialNotes = specialNotes;
	}

	public int getCarrierRatePlanId() {
		return carrierRatePlanId;
	}

	public void setCarrierRatePlanId(int carrierRatePlanId) {
		this.carrierRatePlanId = carrierRatePlanId;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	
	public boolean isOverrideRoamingIncluded() {
		return overrideRoamingIncluded;
	}
	
	public void setOverrideRoamingIncluded(boolean overrideRoamingIncluded) {
		this.overrideRoamingIncluded = overrideRoamingIncluded;
	}

	public RatePlanFeeDetails getRatePlanFeeDetails() {
		return ratePlanFeeDetails;
	}

	public void setRatePlanFeeDetails(RatePlanFeeDetails ratePlanFeeDetails) {
		this.ratePlanFeeDetails = ratePlanFeeDetails;
	}

	public ProvisionTriggerSettings getProvisionTriggerSettings() {
		return provisionTriggerSettings;
	}

	public void setProvisionTriggerSettings(ProvisionTriggerSettings provisionTriggerSettings) {
		this.provisionTriggerSettings = provisionTriggerSettings;
	}

	public SuspendTriggerSettings getSuspendTriggerSettings() {
		return suspendTriggerSettings;
	}

	public void setSuspendTriggerSettings(SuspendTriggerSettings suspendTriggerSettings) {
		this.suspendTriggerSettings = suspendTriggerSettings;
	}

	public List<ZoneRatePlan> getZoneRatePlanSettings() {
		return zoneRatePlanSettings;
	}

	public void setZoneRatePlanSettings(List<ZoneRatePlan> zoneRatePlanSettings) {
		this.zoneRatePlanSettings = zoneRatePlanSettings;
	}
	
	public List<RatePlanTier> getRatePlanTiers() {
		return ratePlanTiers;
	}
	
	public void setRatePlanTiers(List<RatePlanTier> ratePlanTiers) {
		this.ratePlanTiers = ratePlanTiers;
	}

	public int getProRateCancellationFee() {
		return proRateCancellationFee;
	}

	public void setProRateCancellationFee(int proRateCancellationFee) {
		this.proRateCancellationFee = proRateCancellationFee;
	}

	public int getContractTerm() {
		return contractTerm;
	}

	public void setContractTerm(int contractTerm) {
		this.contractTerm = contractTerm;
	}

	public int getCancellationFeeReductionInterval() {
		return cancellationFeeReductionInterval;
	}

	public void setCancellationFeeReductionInterval(
			int cancellationFeeReductionInterval) {
		this.cancellationFeeReductionInterval = cancellationFeeReductionInterval;
	}

	public int getAutoCancelAtTermEnd() {
		return autoCancelAtTermEnd;
	}

	public void setAutoCancelAtTermEnd(int autoCancelAtTermEnd) {
		this.autoCancelAtTermEnd = autoCancelAtTermEnd;
	}

	public int getAccountForDaysInProvisionState() {
		return accountForDaysInProvisionState;
	}

	public void setAccountForDaysInProvisionState(int accountForDaysInProvisionState) {
		this.accountForDaysInProvisionState = accountForDaysInProvisionState;
	}

	public int getAccountForDaysInSuspendState() {
		return accountForDaysInSuspendState;
	}

	public void setAccountForDaysInSuspendState(int accountForDaysInSuspendState) {
		this.accountForDaysInSuspendState = accountForDaysInSuspendState;
	}

	public List<RatePlanOverageBucket> getRatePlanOverageBucket() {
		return ratePlanOverageBucket;
	}

	public void setRatePlanOverageBucket(List<RatePlanOverageBucket> ratePlanOverageBucket) {
		this.ratePlanOverageBucket = ratePlanOverageBucket;
	}

    public boolean isRoamingIncluded() {
        return roamingIncluded;
    }

    public void setRoamingIncluded(boolean roamingIncluded) {
        this.roamingIncluded = roamingIncluded;
    }

	public int getZoneSetId() {
		return zoneSetId;
	}

	public void setZoneSetId(int zoneSetId) {
		this.zoneSetId = zoneSetId;
	}  

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

	public boolean isWaterfallEnabled() {
		return waterfallEnabled;
	}

	public void setWaterfallEnabled(boolean waterfallEnabled) {
		this.waterfallEnabled = waterfallEnabled;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getWholesaleRateplanId() {
		return wholesaleRateplanId;
	}

	public void setWholesaleRateplanId(long wholesaleRateplanId) {
		this.wholesaleRateplanId = wholesaleRateplanId;
	}
        
        public void setIsWholesaleRatePlan(boolean isWholesaleRatePlan) {
		this.isWholesaleRatePlan = isWholesaleRatePlan;
	}

	public boolean getIsWholesaleRatePlan() {
		return isWholesaleRatePlan;
	} 
}
