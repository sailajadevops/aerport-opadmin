package com.aeris.service.operatoradmin.dao;

import java.util.List;

import com.aeris.service.operatoradmin.model.Product;

public interface IOperatorProductDAO {
	List<Product> getProductsByOperator(int operatorId);
    Product getProductByOperatorAndProductId(int operatorId, int productId);
}
