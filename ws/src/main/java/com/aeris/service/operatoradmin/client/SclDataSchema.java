package com.aeris.service.operatoradmin.client;

import java.io.Serializable;

public class SclDataSchema implements Serializable {
	private static final long serialVersionUID = 1L;
	private String encoding;
	private Parameter[] parameters;

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public Parameter[] getParameters() {
		return parameters;
	}

	public void setParameters(Parameter[] parameters) {
		this.parameters = parameters;
	}
}
