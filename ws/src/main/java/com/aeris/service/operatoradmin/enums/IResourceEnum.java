package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.Resource;

public interface IResourceEnum  extends IBaseEnum{
	Resource getResourceByPath(String resourcePath);
	List<Resource> getResources();
	Resource getResourceById(int resourceId);
}
