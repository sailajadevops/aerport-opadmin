package com.aeris.service.operatoradmin.dao;

import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.AccountRatePlanMappingException;
import com.aeris.service.operatoradmin.exception.BadStartDateException;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.RatePlanAlreadyAssignedException;
import com.aeris.service.operatoradmin.exception.RatePlanException;
import com.aeris.service.operatoradmin.exception.RatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.SubRatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.AccountRatePlan;
import com.aeris.service.operatoradmin.model.CarrierRatePlan;
import com.aeris.service.operatoradmin.model.ProvisionTriggerSettings;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanOverageBucket;
import com.aeris.service.operatoradmin.model.RatePlanPaymentType;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.RatePlanTier;
import com.aeris.service.operatoradmin.model.RatePlanType;
import com.aeris.service.operatoradmin.model.SuspendTriggerSettings;
import com.aeris.service.operatoradmin.model.ZoneRatePlan;

public interface IRatePlanDAO {
	List<RatePlan> getAllRatePlans(int operatorId, String start, String count, int[] productIds, RatePlanAccessType accessType, 
			RatePlanStatus... rateplanStatusList) throws RatePlanException;

	RatePlan getRatePlan(int operatorId, long ratePlanId) throws RatePlanException, RatePlanNotFoundException;

	RatePlan createNewRatePlan(int operatorId, int productId, String ratePlanLabel, RatePlanType ratePlanType, RatePlanPaymentType paymentType,
			RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded, RatePlanStatus status,
			boolean overrideRoamingIncluded, int tierCriteria, int homeZoneId, String description, String specialNotes, int carrierRatePlanId, long accountId,
			Date startDate, Date endDate, String currencyName, int includedPeriodMonths, int devicePoolingPolicyId, int packetRoundingPolicyId,
			RatePlanFeeDetails ratePlanFeeDetails, ProvisionTriggerSettings provisionTriggerSettings, SuspendTriggerSettings suspendTriggerSettings,
			List<ZoneRatePlan> zoneRatePlanSettings, List<RatePlanTier> ratePlanTiers, Date requestedDate, String requestedUser, int proRateCancellationFee,
			int contractTerm,int cancellationFeeReductionInterval, int cancelFeeAtTermEnd, int accountForDaysInProvState, int accountForDaysInSuspendState,
			List<RatePlanOverageBucket> rpOverageBucket, boolean roamingIncluded, int zoneSetId, String technology, boolean waterfallEnabled, Long wholesaleRateplanId, boolean isWholesaleRatePlan) throws RatePlanException,
			DuplicateRatePlanException, SubRatePlanNotFoundException, ZoneNotFoundException, DuplicateZoneException, ZoneException;

	RatePlan updateRatePlan(int operatorId, long ratePlanId, int productId, String ratePlanLabel, RatePlanType ratePlanType, RatePlanPaymentType paymentType,
			RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded, RatePlanStatus status,
			boolean overrideRoamingIncluded, int tierCriteria, int homeZoneId, String description, String specialNotes, int carrierRatePlanId, long accountId,
			Date startDate, Date endDate, String currencyName, int includedPeriodMonths, int devicePoolingPolicyId, int packetRoundingPolicyId,
			RatePlanFeeDetails ratePlanFeeDetails, ProvisionTriggerSettings provisionTriggerSettings, SuspendTriggerSettings suspendTriggerSettings,
			List<ZoneRatePlan> zoneRatePlanSettings, List<RatePlanTier> ratePlanTiers, Date requestedDate, String requestedUser, int proRateCancellationFee, 
			int contractTerm, int cancellationFeeReductionInterval,int cancelFeeAtTermEnd, int accountForDaysInProvState,int accountForDaysInSuspendState,
			List<RatePlanOverageBucket> rpOverageBucket, boolean roamingIncluded, int zoneSetId, String technology, boolean waterfallEnabled, Long wholesaleRateplanId, boolean isWholesaleRatePlan)
			throws RatePlanNotFoundException, RatePlanException, BadStartDateException, SubRatePlanNotFoundException, DuplicateRatePlanException,
			ZoneNotFoundException, DuplicateZoneException, RatePlanAlreadyAssignedException;

	List<AccountRatePlan> getAccountRatePlans(int operatorId, long accountId, String start, String count, RatePlanStatus... rateplanStatusList)
			throws RatePlanException;

	AccountRatePlan getAccountRatePlan(int operatorId, long accountId, long ratePlanId) throws RatePlanException, RatePlanNotFoundException;

	boolean assignRatePlanToAccount(int operatorId, long accountId, long ratePlanId, int carrierRatePlanId, Date startDate, Date endDate, String description,
			Date requestedDate, String requestedUser) throws RatePlanNotFoundException, RatePlanException, DuplicateRatePlanException;

	boolean invalidateAccountRatePlan(int operatorId, long accountId, long ratePlanId, Date requestedDate, String requestedUser)
			throws RatePlanNotFoundException, RatePlanException;

	List<CarrierRatePlan> getCarrierRatePlans(int productId) throws RatePlanException;
	
	List<CarrierRatePlan> getCarrierRatePlans(int productId, long accountId) throws RatePlanException;

	List<String> getUnpooledAccountRatePlans(int operatorId, long accountId, int filteredRatePlanPoolId, int productId) throws RatePlanException;

	boolean updateRatePlanStatus(int operatorId, long ratePlanId, RatePlanStatus status, String specialComments, Date requestedDate, String requestedUser, Date newEndDate)
			throws RatePlanNotFoundException, RatePlanException, AccountRatePlanMappingException;

	List<RatePlan> getSubRatePlans(int operatorId, long tieredRatePlanId) throws RatePlanException;


	List<RatePlan> getUnattachedSubRatePlans(int operatorId, long tieredRatePlanId) throws RatePlanException;

        List<RatePlan> getWholesaleRatePlans(long accountId, int[] productIds) throws RatePlanException;
        
        List<RatePlan> getResellerRetailRatePlans(long accountId, int[] productIds) throws RatePlanException;

        

    

    List<RatePlan> getSubRatePlansInUse(List<Long> parenRatePlanIds) throws RatePlanException;

}
