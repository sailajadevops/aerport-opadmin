package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum AccountType implements Serializable {
	CARRIER(1), APP_DEVELOPER(2), AERIS_INTERNAL(3), PARTNER(4);
	int value;

	AccountType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static AccountType fromValue(int value) {
		AccountType[] statuses = values();

		for (AccountType accountType : statuses) {
			if (accountType.getValue() == value) {
				return accountType;
			}
		}

		return null;
	}

	public static AccountType fromName(String value) {
		AccountType[] statuses = values();

		for (AccountType accountType : statuses) {
			if (accountType.name().equalsIgnoreCase(value)) {
				return accountType;
			}
		}

		return null;
	}
}
