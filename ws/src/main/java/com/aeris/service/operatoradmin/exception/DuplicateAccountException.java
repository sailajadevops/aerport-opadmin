package com.aeris.service.operatoradmin.exception;

public class DuplicateAccountException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DuplicateAccountException(String s) {
		super(s);
	}

	public DuplicateAccountException(String ex, Throwable t) {
		super(ex, t);
	}
}
