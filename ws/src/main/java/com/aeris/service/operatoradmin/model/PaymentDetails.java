package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class PaymentDetails implements Serializable {
	private static final long serialVersionUID = 3485119597821188106L;
	private CCAccountType accountType;
	private CardType cardType;
	private String cardNumber;
	private String cardHolderName;
	private String expiryDate;
	private String securityCode;

	public void setAccountType(CCAccountType accountType) {
		this.accountType = accountType;
	}

	public CCAccountType getAccountType() {
		return accountType;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}

	public String validate() {
		StringBuffer errors = new StringBuffer();

		if (accountType == null) {
			errors.append("accountType\n");
		}

		if (cardType == null) {
			errors.append("cardType\n");
		}

		if (StringUtils.isEmpty(cardNumber)) {
			errors.append("cardNumber\n");
		}

		if (StringUtils.isEmpty(cardHolderName)) {
			errors.append("cardHolderName\n");
		}

		if (expiryDate == null) {
			errors.append("expiryDate\n");
		}

		if (StringUtils.isEmpty(securityCode)) {
			errors.append("securityCode\n");
		}

		return errors.toString();
	}
}
