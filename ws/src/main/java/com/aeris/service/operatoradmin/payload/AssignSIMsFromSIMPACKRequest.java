package com.aeris.service.operatoradmin.payload;

/**
 *
 * @author SB00122138
 */
public class AssignSIMsFromSIMPACKRequest {

    private String userId;
    private String simpackId;
    private String orderId ;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSimpackId() {
        return simpackId;
    }

    public void setSimpackId(String simpackId) {
        this.simpackId = simpackId;
    }

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
