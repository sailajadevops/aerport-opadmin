package com.aeris.service.operatoradmin.exception;

public class RatePlanPoolException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public RatePlanPoolException(String s) {
		super(s);
	}

	public RatePlanPoolException(String ex, Throwable t) {
		super(ex, t);
	}
}
