package com.aeris.service.operatoradmin.exception;

public class SIMInventoryException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SIMInventoryException(String s) {
		super(s);
	}

	public SIMInventoryException(String ex, Throwable t) {
		super(ex, t);
	}
}
