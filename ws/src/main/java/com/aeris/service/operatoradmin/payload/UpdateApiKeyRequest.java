package com.aeris.service.operatoradmin.payload;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aeris.service.operatoradmin.model.ResourcePermission;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class UpdateApiKeyRequest {
	private List<ResourcePermission> permissions;
	private String userId;
	private String expiryDate;
	private String description;
	private String status;
	
	public List<ResourcePermission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<ResourcePermission> permissions) {
		this.permissions = permissions;
	}

	public void addPermission(String resource, String permissions) {
		if (this.permissions == null) {
			this.permissions = new ArrayList<ResourcePermission>();
		}
		
		this.permissions.add(new ResourcePermission(resource, permissions));
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
