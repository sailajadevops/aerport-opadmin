package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

@XmlRootElement
public class AssignRatePlanPoolRequest implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;
	
	@Email(message="{userId.email}")
	private String userId;
	
	@NotNull(message="{ratePlanId.notnull}")
	@Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") 
	private String ratePlanId;
	
	@NotNull(message="{startDate.notnull}")
	@Future
	private Date startDate;
	
	@NotNull(message="{endDate.notnull}")
	@Future
	private Date endDate;
	
	@NotNull(message="{description.notnull}")
	private String description;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getRatePlanId() {
		return ratePlanId;
	}
	
	public void setRatePlanId(String ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	} 
}
