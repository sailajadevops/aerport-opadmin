package com.aeris.service.operatoradmin.exception;

public class SIMConfigNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SIMConfigNotFoundException(String s) {
		super(s);
	}

	public SIMConfigNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
