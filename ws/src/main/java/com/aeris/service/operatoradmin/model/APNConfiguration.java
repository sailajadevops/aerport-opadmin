package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@XmlRootElement
public class APNConfiguration implements Serializable {
	private static final long serialVersionUID = -7168433614758442310L;
	private int[] dynamicIpApns;
	private int[] staticIpApns;
	private int[] mtSmsApns;
	private int[] moSmsApns;
	private int[] mtVoiceApns;
	private int[] moVoiceApns;
	private int[] roamingApns;

	public int[] getDynamicIpApns() {
		return dynamicIpApns;
	}

	public void setDynamicIpApns(int[] dynamicIpApns) {
		this.dynamicIpApns = dynamicIpApns;
	}

	public int[] getStaticIpApns() {
		return staticIpApns;
	}

	public void setStaticIpApns(int[] staticIpApns) {
		this.staticIpApns = staticIpApns;
	}

	public int[] getMtSmsApns() {
		return mtSmsApns;
	}

	public void setMtSmsApns(int[] mtSmsApns) {
		this.mtSmsApns = mtSmsApns;
	}

	public int[] getMoSmsApns() {
		return moSmsApns;
	}

	public void setMoSmsApns(int[] moSmsApns) {
		this.moSmsApns = moSmsApns;
	}

	public int[] getMtVoiceApns() {
		return mtVoiceApns;
	}

	public void setMtVoiceApns(int[] mtVoiceApns) {
		this.mtVoiceApns = mtVoiceApns;
	}

	public int[] getMoVoiceApns() {
		return moVoiceApns;
	}

	public void setMoVoiceApns(int[] moVoiceApns) {
		this.moVoiceApns = moVoiceApns;
	}

	public int[] getRoamingApns() {
		return roamingApns;
	}

	public void setRoamingApns(int[] roamingApns) {
		this.roamingApns = roamingApns;
	}

}
