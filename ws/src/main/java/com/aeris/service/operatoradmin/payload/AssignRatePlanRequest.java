package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.constraints.NotFuture;
import com.aeris.service.operatoradmin.model.constraints.NotPast;
import com.aeris.service.operatoradmin.model.constraints.ValidateRatePlanDate;

@XmlRootElement
@ValidateRatePlanDate(message="{endDate.after.startDate}")
public class AssignRatePlanRequest implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;
	
	@NotNull(message="{carrierRatePlanId.notnull}")
	@Min(value = 1, message = "{carrierRatePlanId.number}")
	private int carrierRatePlanId;
	
	@Email(message="{userId.email}")
	private String userId;
	
	@NotNull(message="{startDate.notnull}")
	@NotPast(message="{startDate.notpast}")
	@NotFuture(message="{startDate.notfuture}")
	private Date startDate;
	
	@NotNull(message="{endDate.notnull}")
	@NotPast(message="{endDate.notpast}")
	private Date endDate;
	
	@NotNull(message="{description.notnull}")
	private String description;

	public int getCarrierRatePlanId() {
		return carrierRatePlanId;
	}
	
	public void setCarrierRatePlanId(int carrierRatePlanId) {
		this.carrierRatePlanId = carrierRatePlanId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	} 
}
