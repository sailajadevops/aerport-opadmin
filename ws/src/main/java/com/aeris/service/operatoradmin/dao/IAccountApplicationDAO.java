package com.aeris.service.operatoradmin.dao;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.aeris.service.operatoradmin.exception.AccountNotFoundException;
import com.aeris.service.operatoradmin.exception.ApiKeyException;
import com.aeris.service.operatoradmin.exception.AtleastOneApiKeyRequiredException;
import com.aeris.service.operatoradmin.exception.InvalidApiKeyException;
import com.aeris.service.operatoradmin.model.AccountApplication;
import com.aeris.service.operatoradmin.model.ApiKey;
import com.aeris.service.operatoradmin.model.Permission;
import com.aeris.service.operatoradmin.model.Resource;
import com.aeris.service.operatoradmin.model.ResourcePermission;

public interface IAccountApplicationDAO {

	List<AccountApplication> getApiKeys(String start, String count, boolean showExpired, String... accountIds) throws AccountNotFoundException;

	ApiKey createNewApiKey(long accountId, List<ResourcePermission> permissions, Date requestedDate, String requestedUser, Date expires, String description,
			String status) throws ApiKeyException;

	ApiKey createNewApiKey(long accountId, List<ResourcePermission> permissions, Date requestedDate, String requestedUser, Date expires, String description,
			String status, String newApiKey) throws ApiKeyException;

	boolean deleteApiKey(long accountId, String apiKey, Date requestedDate, String requestedUser) throws InvalidApiKeyException,
			AtleastOneApiKeyRequiredException;

	ApiKey updateApiKey(long accountId, String apiKey, List<ResourcePermission> permissions, Date requestedDate, String requestedUser, Date expiryDate,
			String description, String status) throws InvalidApiKeyException, AtleastOneApiKeyRequiredException;

	List<Resource> findResourcesByAccount(long accountId);

	Map<String, List<Permission>> getResourcePermissionsByAppId(List<Integer> appIds) throws AccountNotFoundException;

	boolean deleteApiKeys(long accountId, Date requestedDate, String requestedUser);

	ApiKey createNewApiKey(long accountId, List<ResourcePermission> permissions, Date requestedDate, String requestedUser, Date expires, String description, String status, String newApiKey,
			Connection conn) throws ApiKeyException;

	ApiKey getMasterApiKey(long accountId) throws ApiKeyException;
	
	long getAccountIdForApiKey(String apiKey) ;

	List<Long> getAccountHierarchy(long accountIdForApiKey);

	List<ApiKey> getAllApiKeys() throws AccountNotFoundException;
}
