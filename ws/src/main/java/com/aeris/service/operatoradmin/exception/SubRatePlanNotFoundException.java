package com.aeris.service.operatoradmin.exception;

public class SubRatePlanNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public SubRatePlanNotFoundException(String s) {
		super(s);
	}

	public SubRatePlanNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
