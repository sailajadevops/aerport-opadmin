package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Distributor 
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class Distributor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1478693517622201289L;
	
	private long accountId;
	private long operatorId;
	private long distributorId;
	private String distributorName;
	private List<String> contactEmails;
	private String logoUrls;
	private String privacyPolicyUrls;
	private int status;
    private Date lastModifiedDate;
    private Date createdDate;
    private String createdBy ;
    private String lastModifiedBy ;
    private Timestamp repTimestamp ;
    private String accessLink;
    private long liferayCompanyId;
    private String storeUrl;
    private boolean isWholesale;
    private String brand;
    private String planPricingUrl;
    private String realm;
    
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getPlanPricingUrl() {
		return planPricingUrl;
	}

	public void setPlanPricingUrl(String planPricingUrl) {
		this.planPricingUrl = planPricingUrl;
	}

	public boolean isWholesale() {
		return isWholesale;
	}

	public void setWholesale(boolean isWholesale) {
		this.isWholesale = isWholesale;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public long getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(long distributorId) {
		this.distributorId = distributorId;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public List<String> getContactEmails() {
		return contactEmails;
	}

	public void setContactEmails(List<String> contactEmails) {
		this.contactEmails = contactEmails;
	}

	public String getLogoUrls() {
		return logoUrls;
	}

	public void setLogoUrls(String logoUrls) {
		this.logoUrls = logoUrls;
	}

	public String getPrivacyPolicyUrls() {
		return privacyPolicyUrls;
	}

	public void setPrivacyPolicyUrls(String privacyPolicyUrls) {
		this.privacyPolicyUrls = privacyPolicyUrls;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Timestamp getRepTimestamp() {
		return repTimestamp;
	}

	public void setRepTimestamp(Timestamp repTimestamp) {
		this.repTimestamp = repTimestamp;
	}

	
	public static void main(String[] args) throws Exception {
		Distributor d = new Distributor();
		d.setAccountId(1);
		d.setDistributorId(2);
		d.setContactEmails(Arrays.asList("saurabh.sharma@aeris.net".split(",")));
		d.setCreatedBy("saurabh.sharma@aeris.net");
		d.setCreatedDate(new Date(System.currentTimeMillis()));
		d.setLastModifiedBy("saurabh.sharma@aeris.net");
		d.setLastModifiedDate(new Date(System.currentTimeMillis()));
		d.setLogoUrls("www.dev.logo.url.net");
		d.setPrivacyPolicyUrls("www.dev.privacy.policy.url.net");
		d.setStatus(1);
		d.setDistributorName(null);
		d.setRepTimestamp(new Timestamp(System.currentTimeMillis()));
		System.out.println(new ObjectMapper().writeValueAsString(d));
	}

	public void setOperatorId(long operatorId) {
		this.operatorId = operatorId ;
	}
	
	public long getOperatorId() {
		return this.operatorId;
	}

    public String getAccessLink() {
        return accessLink;
    }

    public void setAccessLink(String accessLink) {
        this.accessLink = accessLink;
    }

    public long getLiferayCompanyId() {
        return liferayCompanyId;
    }

    public void setLiferayCompanyId(long liferayCompanyId) {
        this.liferayCompanyId = liferayCompanyId;
    }

    public String getStoreUrl() {
        return storeUrl;
    }

    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}
}
