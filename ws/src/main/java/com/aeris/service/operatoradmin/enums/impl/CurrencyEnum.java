package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.ICurrencyEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.Currency;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * Country Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class CurrencyEnum implements ICurrencyEnum, IEventListener  {
	private Logger LOG = LoggerFactory.getLogger(CurrencyEnum.class);
	
	static final String SQL_GET_CURRENCIES = "select currency_code, currency_name, country_id from ws_currency order by currency_code";
	
	private Cache<String, Currency> cache;
	private Cache<String, Currency> cacheById;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "CurrencyCache/name") Cache<String, Currency> cache, 
			@Hazelcast(cache = "CurrencyCache/id") Cache<String, Currency> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_CURRENCIES);
			
			while (rs.next()) {
				Currency currency = new Currency();

				currency.setCurrencyCode(rs.getString("currency_code"));
				currency.setCurrencyName(rs.getString("currency_name"));
				currency.setCountryId(rs.getInt("country_id"));
				
				cache.put(currency.getCurrencyName(), currency);
				cacheById.put(currency.getCurrencyCode(), currency);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}
	

	@Override
	public Currency getCurrencyByName(String name) {
		return cache.get(name);
	}

	@Override
	public Currency getCurrencyByCode(String code) {
		return cacheById.get(code);
	}

	@Override
	public List<Currency> getAllCurrencies() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
