package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class Product implements Serializable, Comparable<Product> {
	private static final long serialVersionUID = 4994637609549403231L;

	private int productId;

	private String productName;

	private int carrierId;
	
	@JsonIgnore
	private String msisdnRange;
	
	@JsonIgnore
	private String staticIpRange;

	private String technology;

	@JsonIgnore
	private Date createdDate;

	@JsonIgnore
	private String createdBy;
    
    private String[] technologies;
    
    private boolean subcriptionLevelOnly;

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setCarrierId(int carrierId) {
		this.carrierId = carrierId;
	}

	public int getCarrierId() {
		return carrierId;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getMsisdnRange() {
		return msisdnRange;
	}

	public void setMsisdnRange(String msisdnRange) {
		this.msisdnRange = msisdnRange;
	}

	public String getStaticIpRange() {
		return staticIpRange;
	}

	public void setStaticIpRange(String staticIpRange) {
		this.staticIpRange = staticIpRange;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
    
    public String[] getTechnologies() {
        return technologies;
    }

    public void setTechnologies(String[] technologies) {
        this.technologies = technologies;
    }

    public boolean isMultiModeProduct() {
        return (this.technologies != null && this.technologies.length > 1);
    }

    public boolean isSubcriptionLevelOnly() {
        return subcriptionLevelOnly;
    }

    public void setSubcriptionLevelOnly(boolean subcriptionLevelOnly) {
        this.subcriptionLevelOnly = subcriptionLevelOnly;
    }
    
    @Override
    public int compareTo(Product prd) {
    	return this.productName.compareTo(prd.productName);
    }
}
