package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum PermissionValue implements Serializable {
	// defined based on string permission definition rwud - read write update
	// delete
	NONE, READ, WRITE, UPDATE, DELETE, EXPIRE, ALL;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Permission: ");
		sb.append(name());

		return sb.toString();
	}
}
