package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class Permission implements Serializable {
	private static final long serialVersionUID = -5415299803214735896L;
	String resource;
	String permissions;

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getResource() {
		return resource;
	}

	public void setPermissions(int dbPermissions) {
		String padded = String.format("%04d", dbPermissions);

		List<String> permissionValues = new ArrayList<String>();

		if (padded.charAt(0) == '1') {
			permissionValues.add(PermissionValue.READ.name().toLowerCase());
		}

		if (padded.charAt(1) == '1') {
			permissionValues.add(PermissionValue.WRITE.name().toLowerCase());
		}

		if (padded.charAt(2) == '1') {
			permissionValues.add(PermissionValue.UPDATE.name().toLowerCase());
		}

		if (padded.charAt(3) == '1') {
			permissionValues.add(PermissionValue.DELETE.name().toLowerCase());
		}

		permissions = StringUtils.join(permissionValues, ',');
	}

	public String getPermissions() {
		return permissions;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Resource: " + resource + " ").append("Permissions: " + permissions);

		return sb.toString();
	}
}