package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.aeris.service.operatoradmin.model.constraints.NotPast;

public class SalesAgent implements Serializable {

	/**
	 * Sales Agent VO
	 */
	private static final long serialVersionUID = 2692586079268037806L;
	private long id;
	@NotNull(message = "{agentId.notnull}")
	@Size(min = 1, max = 50, message = "{agentId.size}")
	private String agentId;
	@NotNull(message = "{agent.firstName.notnull}")
	private String firstName;
	@NotNull(message = "{agent.lastName.notnull}")
	private String lastName;
	@NotNull(message = "{agent.company.notnull}")
	private String company;
	@NotNull(message = "{agentType.notnull}")
	private String agentType;
	@NotNull(message = "{agent.startDate.notnull}")	
	private Date startDate;
	@NotNull(message = "{agent.endDate.notnull}")
	private Date endDate;
	private int notUsed;
	private Date createdDate;
	@NotNull(message = "{createdBy.notnull}")
	private String createdBy;
	private Date lastModifiedDate;
	private String lastModifiedBy;
	private Timestamp repTimestamp;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getNotUsed() {
		return notUsed;
	}

	public void setNotUsed(int notUsed) {
		this.notUsed = notUsed;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Timestamp getRepTimestamp() {
		return repTimestamp;
	}

	public void setRepTimestamp(Timestamp repTimestamp) {
		this.repTimestamp = repTimestamp;
	}

	@Override
	public String toString() {
		return "SalesAgent [id=" + id + ", agentId=" + agentId + ", firstName="
				+ firstName + ", lastName=" + lastName + ", company=" + company
				+ ", agentType=" + agentType + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", notUsed=" + notUsed
				+ ", createdDate=" + createdDate + ", createdBy=" + createdBy
				+ ", lastModifiedDate=" + lastModifiedDate
				+ ", lastModifiedBy=" + lastModifiedBy + ", repTimestamp="
				+ repTimestamp + "]";
	}

}
