package com.aeris.service.operatoradmin.dao.impl;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.client.AS4gClient;
import com.aeris.service.operatoradmin.dao.IAS4gDAO;
import com.aeris.service.operatoradmin.model.HssApnConfig;
import com.aeris.service.operatoradmin.model.HssPSConfig;
import com.aeris.service.operatoradmin.model.HssProfile;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AS4gDAO implements IAS4gDAO {

    @Inject
    private AS4gClient aS4gClient;
    private Cache<String, HssProfile> hssProfilesCache;
    private Cache<String, HssPSConfig> hssPSConfigsCache;
    private Cache<String, HssApnConfig> hssApnConfigsCache;
    private static Logger LOG = LoggerFactory.getLogger(AS4gDAO.class);

    public void init(
            @Hazelcast(cache = "HssProfilesCache/name") Cache<String, HssProfile> hssProfilesCache,
            @Hazelcast(cache = "HssPSConfigsCache/name") Cache<String, HssPSConfig> hssPSConfigsCache,
            @Hazelcast(cache = "HssApnConfigsCache/name") Cache<String, HssApnConfig> hssApnConfigsCache) {
        this.hssProfilesCache = hssProfilesCache;
        this.hssPSConfigsCache = hssPSConfigsCache;
        this.hssApnConfigsCache = hssApnConfigsCache;
        populateHssProfilesCache();
        populateHssPSConfigsCache();
        populateHssApnConfigsInCache();
    }

    private void populateHssProfilesCache() {
        hssProfilesCache.clear();
        List<HssProfile> hssProfiles = aS4gClient.getHssProfiles();
        for (HssProfile hssProfile : hssProfiles) {
            hssProfilesCache.put(hssProfile.getId(), hssProfile);
        }
    }

    private void populateHssPSConfigsCache() {
        hssPSConfigsCache.clear();
        List<HssPSConfig> hssPSConfigs = aS4gClient.getHssPSConfigs();
        for (HssPSConfig hssPSConfig : hssPSConfigs) {
            hssPSConfigsCache.put(hssPSConfig.getId(), hssPSConfig);
        }
    }

    private void populateHssApnConfigsInCache() {
        hssApnConfigsCache.clear();
        List<HssApnConfig> hssApnConfigs = aS4gClient.getHssApnConfigs();
        for (HssApnConfig hssApnConfig : hssApnConfigs) {
            hssApnConfigsCache.put(hssApnConfig.getId(), hssApnConfig);
        }
    }

    @Override
    public List<HssProfile> getHssProfiles() {
        if (hssProfilesCache.getValues().isEmpty()) {
            populateHssProfilesCache();
        }
        return hssProfilesCache.getValues();
    }

    @Override
    public List<HssPSConfig> getHssPSConfigs() {
        if (hssPSConfigsCache.getValues().isEmpty()) {
            populateHssPSConfigsCache();
        }
        return hssPSConfigsCache.getValues();
    }

    @Override
    public List<HssApnConfig> getHssApnConfigs() {
        if (hssApnConfigsCache.getValues().isEmpty()) {
            populateHssApnConfigsInCache();
        }
        return hssApnConfigsCache.getValues();
    }

    @Override
    public String getHssApnValues(String hssProfileId) {
        LOG.debug("getHssApnValues() : " + hssProfileId);
        if (hssProfileId == null || hssProfileId.isEmpty()) {
            return null;
        }
        List<String> hssApnValues = new ArrayList<String>();
        HssProfile hssProfile = hssProfilesCache.get(hssProfileId);
        if (hssProfile != null) {
            LOG.debug("getHssApnValues() : hssProfile : " + hssProfile.getId());
            HssPSConfig hssPSConfig = hssPSConfigsCache.get(hssProfile.getPsConf());
            if (hssPSConfig != null) {
                LOG.debug("getHssApnValues() : psConf : " + hssPSConfig.getId());
                String apns = hssPSConfig.getApns();
                if (apns != null) {
                    LOG.debug("getHssApnValues() : psConf : apns : " + apns);
                    String[] apnIds = apns.split(",");
                    for (String apnId : apnIds) {
                        HssApnConfig hssApnConfig = hssApnConfigsCache.get(apnId);
                        if (hssApnConfig != null && hssApnConfig.getApn() != null) {
                            hssApnValues.add(hssApnConfig.getApn());
                        }
                    }
                } else if (hssPSConfig.getApnDefault() != null) {
                    LOG.debug("getHssApnValues() : psConf : apnDefault : " + hssPSConfig.getApnDefault());
                    HssApnConfig hssApnConfig = hssApnConfigsCache.get(hssPSConfig.getApnDefault());
                    if (hssApnConfig != null && hssApnConfig.getApn() != null) {
                        hssApnValues.add(hssApnConfig.getApn());
                    }
                }
            }
        } else {
            // Reload cache
            populateHssProfilesCache();
            populateHssPSConfigsCache();
            populateHssApnConfigsInCache();
        }
        String apnsLTE = StringUtils.join(hssApnValues, ",");
        return apnsLTE;
    }
}
