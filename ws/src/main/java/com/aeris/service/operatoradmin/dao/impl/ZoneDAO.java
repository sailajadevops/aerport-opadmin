package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneMappingException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZonePreference;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

public class ZoneDAO implements IZoneDAO{

	private static final String SQL_GET_ALL_ZONES_BY_OPERATOR_AND_ZONESET = "select zone_id, " +
																"zone_name, " +
																"product_id, " + 
																"operator_id, " + 
																"operators, " +
																"geographies, " +
																"mo_sms_allowed, "	+ 
																"mt_sms_allowed, "	+ 
																"mt_voice_allowed, " + 
																"mo_voice_allowed, " +
																"packet_allowed, " +
																"zone_preferences, " +
																"zone_set_id, "+
																"technology "+
																"from " +
																"roaming_zones where " +
																"operator_id = ? and "+
																"zone_set_id = ? ";
																;

	private static final String SQL_GET_ALL_ZONES_BY_OPERATOR = "select zone_id, " +
																		"zone_name, " +
																		"product_id, " + 
																		"operator_id, " + 
																		"operators, " +
																		"geographies, " +
																		"mo_sms_allowed, "	+ 
																		"mt_sms_allowed, "	+ 
																		"mt_voice_allowed, " + 
																		"mo_voice_allowed, " +
																		"packet_allowed, " +
																		"zone_preferences, " +
																		"zone_set_id, "+
																		"technology "+
																		"from " +
																		"roaming_zones where " +
																		"operator_id = ? " ;																

	private static final String SQL_GET_ALL_ZONES = "select zone_id, " +
														"zone_name, " +
														"product_id, " + 
														"operator_id, " + 
														"operators, " +
														"geographies, " +
														"mo_sms_allowed, "	+ 
														"mt_sms_allowed, "	+ 
														"mt_voice_allowed, " + 
														"mo_voice_allowed, " +
														"packet_allowed, " +
														"zone_preferences," +
														"zone_set_id, " +
														"technology "+
														"from " +
														"roaming_zones";
	
	
	private static final String SQL_GET_ZONE_COUNT_BY_ZONE_SET_ID = "select count(*) as zone_cnt " +
																	"from " +
																	"roaming_zones where " +
																	"operator_id = ? and " +
																	"zone_name = ? and " +
																	"zone_id != ? and "+
																	"zone_set_id = ?";
	
	private static final String SQL_GET_ZONE_COUNT_BY_PRODUCT_ID = "select count(*) as zone_cnt " +
																	"from " +
																	"roaming_zones where " +
																	"operator_id = ? and " +
																	"zone_name = ? and " +
																	"zone_id != ? and "+
																	"product_id = ?";
		
	private static final String SQL_GET_ZONES_BY_ZONE_ID = "select zone_id, " +
																"zone_name, " +
																"product_id, " + 
																"operator_id, " + 
																"operators, " +
																"geographies, " +
																"mo_sms_allowed, "	+ 
																"mt_sms_allowed, "	+ 
																"mt_voice_allowed, " + 
																"mo_voice_allowed, " +
																"packet_allowed, " +
																"mo_sms_cost, " +
																"mt_sms_cost, " +
																"mo_voice_cost, " +
																"mt_voice_cost, " +
																"packet_cost, " +
																"mo_sms_count_included, " +
																"mt_sms_count_included, " +
																"mo_voice_mins_included, " +
																"mt_voice_mins_included, " +
																"packet_kb_included, " +
																"zone_preferences, " +
																"zone_set_id, "+
																"technology "+
																"from " +
																"roaming_zones where " +
																"operator_id = ? and " +
																"zone_id = ? ";
																
		
	private static final String SQL_CREATE_NEW_ZONE =   "insert into roaming_zones " +
														"(zone_name, " +
														"product_id, " +
														"operators, " +
														"geographies, " +
														"operator_id, " +
														"mo_sms_allowed, "	+ 
														"mt_sms_allowed, "	+ 
														"mo_voice_allowed, " +
														"mt_voice_allowed, " + 
														"packet_allowed, " +
														"mo_sms_cost, " +
														"mt_sms_cost, " +
														"mo_voice_cost, " +
														"mt_voice_cost, " +
														"packet_cost, " +
														"mo_sms_count_included, " +
														"mt_sms_count_included, " +
														"mo_voice_mins_included, " +
														"mt_voice_mins_included, " +
														"packet_kb_included, " +
														"zone_preferences, " +
														"created_by, " +
														"date_created, " +
														"last_modified_by, " +
														"last_modified_date,  " +
														"zone_set_id ," +
														"TECHNOLOGY )"+
														"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
												
	private static final String SQL_UPDATE_ZONE = "update roaming_zones set " +
														"zone_name = ?, " +
														"product_id = ?, " +
														"operators = ?, " +
														"geographies = ?, " +
														"mo_sms_allowed = ?, "	+ 
														"mt_sms_allowed = ?, "	+ 
														"mt_voice_allowed = ?, " + 
														"mo_voice_allowed = ?, " +
														"packet_allowed = ?, " +
														"mo_sms_cost = ?, " +
														"mt_sms_cost = ?, " +
														"mo_voice_cost = ?, " +
														"mt_voice_cost = ?, " +
														"packet_cost = ?, " +
														"mo_sms_count_included = ?, " +
														"mt_sms_count_included = ?, " +
														"mo_voice_mins_included = ?, " +
														"mt_voice_mins_included = ?, " +
														"packet_kb_included = ?, " +
														"zone_preferences = ?, " +
														"last_modified_by = ?, " +
														"last_modified_date = ?, "+
														"TECHNOLOGY = ? "+
														"where " +
														"operator_id = ? and " +
														"zone_id = ? and "+
														"zone_set_id = ?";
	
	private static final String SQL_DELETE_ZONE = "delete from roaming_zones where " +
													"operator_id = ? and " +
													"zone_id = ? and "+
													"zone_set_id = ?";
	
	private static final String SQL_DELETE_ZONE_WITHOUT_ZONESET_ID = "delete from roaming_zones where " +
													"operator_id = ? and " +
													"zone_id = ?  ";
													
											
	
	private Cache<Integer, Zone> cache;

	private static Logger LOG = LoggerFactory.getLogger(ZoneDAO.class);
	
	public void init(@Hazelcast(cache = "ZoneCache/id") Cache<Integer, Zone> cache) {
		this.cache = cache;
		List<Zone> zonesList = getAllZones();
		Zone[] zones = new Zone[zonesList.size()];
		zonesList.toArray(zones);
		
		putInCache(zones);
	} 

	@Override
	public List<Zone> getAllZones(final int operatorId, final int zoneSetId) throws ZoneException {
		LOG.info("getAllZones(operatorId,zoneSetId): processing get all zones..operatorId="+operatorId+", zoneSetId="+zoneSetId);
		
		// If already loaded, fetch from cache
		if(!cache.getValues().isEmpty())
		{
			return new ArrayList<Zone>(Collections2.filter(cache.getValues(), new Predicate<Zone>() {
				@Override
				public boolean apply(Zone input) {
					if(zoneSetId > 0)
						return ((operatorId == input.getOperatorId()) && (zoneSetId == input.getZoneSetId()) );
					else
						return (operatorId == input.getOperatorId());
				}
				
			}));
		}
		
		// If not already loaded, fetch from db
		List<Zone> zones = new ArrayList<Zone>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("getAllZones(operatorId,zoneSetId): Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			LOG.debug("getAllZones(operatorId,zoneSetId): Calling buildGetZonesQuery(conn, "+operatorId+","+zoneSetId+")...");
			ps = buildGetZonesQuery(conn, operatorId, zoneSetId);

			rs = ps.executeQuery();

			while (rs.next()) {
				Zone zone = new Zone();
				zone.setZoneId(rs.getInt("zone_id"));
				zone.setZoneName(rs.getString("zone_name"));
				zone.setProductId(rs.getInt("product_id"));
				zone.setOperatorId(rs.getInt("operator_id"));
				
				// Set Operators 
				setOperators(rs, zone);
				
				// Set Geographies
				setGeographies(rs, zone);

				// Set Services
				setServices(rs, zone);
				
				// Set Zone Preferences
				setZonePreferences(rs, zone);
				
				zone.setZoneSetId(rs.getInt("zone_set_id"));
				zone.setTechnology(rs.getString("technology"));

				zones.add(zone);
				
				// Store in cache
				cache.put(zone.getZoneId(), zone);
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during getting zones in roaming_zones - db error" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Exception during getAllZones - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during getting zones in roaming_zones - fatal error" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("GetAllZones failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAllZones(operatorId,zoneSetId): Returned " + zones.size() + " zones to the caller");

		return zones;
	}

	private void setZonePreferences(ResultSet rs, Zone zone) throws SQLException {
		String zonePreferences = rs.getString("zone_preferences");
		
		if (StringUtils.isNotEmpty(zonePreferences)) {
			String[] zonePrefs = zonePreferences.split(",");
			List<ZonePreference> zonePrefList = new ArrayList<ZonePreference>();
			
			for (String zonePref : zonePrefs) {
				if(ZonePreference.isValid(zonePref))
				{
					zonePrefList.add(ZonePreference.fromName(zonePref));
				}
			}

			zone.setZonePreferences(zonePrefList);
		}
	}

	private PreparedStatement buildGetZonesQuery(Connection conn, int operatorId, int zoneSetId) throws SQLException {
		PreparedStatement ps;
		StringBuilder query = new StringBuilder();
				
		if(operatorId > 0 && zoneSetId > 0){
			//get Zones available for given Operator & ZoneSet
			query.append(SQL_GET_ALL_ZONES_BY_OPERATOR_AND_ZONESET);
		}else if(operatorId > 0){
			//get Zones available for given Operator 
			query.append(SQL_GET_ALL_ZONES_BY_OPERATOR);
		}else{
			// get all available zones
			query.append(SQL_GET_ALL_ZONES);
		}
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;

		if(operatorId > 0 && zoneSetId > 0){
			ps.setInt(++i, operatorId);
			ps.setInt(++i, zoneSetId);
		}else if(operatorId > 0){
			ps.setInt(++i, operatorId);
		}
		
		LOG.debug("buildGetZonesQuery(): Returning Query :"+query.toString());
		return ps;
	}

	@Override
	public List<Zone> getZonesByZoneSet(final int operatorId,final int zoneSetId) throws ZoneException {
		LOG.info("getZonesByZoneSet: processing get zones by operatorId="+operatorId+", zoneSetId="+zoneSetId);
		
		// If already loaded, fetch from cache
		if(!cache.getValues().isEmpty())
		{
			return new ArrayList<Zone>(Collections2.filter(cache.getValues(), new Predicate<Zone>() {
				@Override
				public boolean apply(Zone input) {
					boolean matchingZone = (operatorId == input.getOperatorId()) && (zoneSetId == input.getZoneSetId());
					if(matchingZone)
						LOG.info("Returning Zone(id :"+input.getZoneId()+") from Cache..");
					return matchingZone;
				}
				
			}));
		}
		
		List<Zone> zones = new ArrayList<Zone>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("Calling buildGetZonesQuery(conn, "+operatorId+", "+zoneSetId);
			ps = buildGetZonesQuery(conn, operatorId, zoneSetId);

			rs = ps.executeQuery();

			while (rs.next()) {
				Zone zone = new Zone();
				zone.setZoneId(rs.getInt("zone_id"));
				zone.setZoneName(rs.getString("zone_name"));
				zone.setProductId(rs.getInt("product_id"));
				zone.setOperatorId(rs.getInt("operator_id"));
				
				// Set Operators 
				setOperators(rs, zone);
				
				// Set Geographies
				setGeographies(rs, zone);

				// Set Services
				setServices(rs, zone);

				// Set Zone Preferences
				setZonePreferences(rs, zone);
				zone.setZoneSetId(rs.getInt("zone_set_id"));
				zone.setTechnology(rs.getString("technology"));
				zones.add(zone);
			}

		} catch (SQLException sqle) {
			LOG.error("Exception during getZonesByZoneSet in roaming_zones - db error" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Exception during getZonesByZoneSet - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during getZonesByZoneSet  in roaming_zones - fatal error" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("getZonesByZoneSet failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getZonesByZoneSet: Returned " + zones.size() + " zones to the caller");

		return zones;
	}

	public Zone[] getOverageZonesByZoneSet(int operatorId, int zoneSetId, int homeZoneId) throws ZoneNotFoundException, ZoneException {
 		LOG.info("getOverageZonesByProduct(): Processing getOverageZonesByProduct with operatorId="+operatorId+", zoneSetId="+zoneSetId+", homeZoneId="+homeZoneId);
		Zone[] zoneArr =  new Zone[6];
		int index = 0;
		
		Zone homeZone = getZone(operatorId, zoneSetId, homeZoneId);
		
		// Throw error if the homeZone is not a valid zone within the selected zoneSet
		if(homeZone.getZoneSetId() != zoneSetId){
			throw new ZoneNotFoundException("Specified Home Zone is not found for the selected zoneSet :"+zoneSetId);
		}
		
		// Search First 4 zones by ZoneSet and set them as home zone, canada zone, mexico zone, and international zone
		List<Zone> zones = getZonesByZoneSet(operatorId, zoneSetId);
		
		Collections.sort(zones, new Comparator<Zone>() {
			@Override
			public int compare(Zone o1, Zone o2) {
				return o1.getZoneId() - o2.getZoneId();
			}
		});		
		
		// Set Home Zone
		zoneArr[index++] = homeZone;
		
		// Set Canada Zone, Mexico Zone and International Zone
		for (Zone zone : zones) {
			if(zone.getZoneId() != homeZone.getZoneId() && index < zoneArr.length){	
				// Commenting below line because instaed of loading the zone again frm DB, we can pull it out of zones list
				//zoneArr[index++] = getZone(operatorId, zone.getZoneId());	
				zoneArr[index++] = zone;
			}
		}
		
		return zoneArr;
	}

	@Override
	public Zone getZone(int operatorId,int zoneSetId, int zoneId) throws ZoneNotFoundException, ZoneException {
		LOG.debug("getZone(): processing get zone for operatorId="+operatorId+", zoneSetId="+zoneSetId+", zoneId="+zoneId);
		
		Zone zone = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("getZone(): Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getZone():Creating Prepared Statement to fetch zone data from roaming_zones table");
			
			String sql = SQL_GET_ZONES_BY_ZONE_ID;
			if(zoneSetId > 0){
				sql = SQL_GET_ZONES_BY_ZONE_ID+" AND ZONE_SET_ID = ? ";
			}
			LOG.debug("SQL_GET_ZONES_BY_ZONE_ID :"+sql +"(operatorId="+operatorId+",zoneId="+zoneId+",zoneSetId="+zoneSetId+")");
			// Fetch a Single Zone by Zone Id from DB 
			ps = conn.prepareStatement(sql);

			LOG.debug("getZone():Setting Parameters");
			
			ps.setLong(1, operatorId);
			ps.setLong(2, zoneId);
			if(zoneSetId > 0){
				ps.setLong(3, zoneSetId);
			}

			LOG.debug("getZone(): executing query: " + SQL_GET_ZONES_BY_ZONE_ID+" : (operatorId="+operatorId+", zoneSetId="+zoneSetId+", zoneId="+zoneId+")");
			rs = ps.executeQuery();

			if (rs.next()) {
				zone = new Zone();
				zone.setZoneId(rs.getInt("zone_id"));
				zone.setZoneName(rs.getString("zone_name"));
				zone.setProductId(rs.getInt("product_id"));
				zone.setOperatorId(rs.getInt("operator_id"));
				
				// Set Operators 
				setOperators(rs, zone);
				
				// Set Geographies
				setGeographies(rs, zone);

				// Set Services
				setServices(rs, zone);

				// Set Included plan
				setIncludedPlan(rs, zone);
				
				// Set Zone Preferences
				setZonePreferences(rs, zone);
				zone.setZoneSetId(rs.getInt("zone_set_id"));
				zone.setTechnology(rs.getString("technology"));
			}
			
			if(zone == null)
			{
				LOG.error("Zone does not exist for zone id: " + zoneId);
				throw new ZoneNotFoundException("Zone does not exist for zone id: " + zoneId);
			}
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured while getting zone" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (SQLException sqle) {
			LOG.error("Exception during getting zone in roaming_zones - db error" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Exception during getting zone - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during getting zone in roaming_zones - fatal error" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Get Zone failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("Returned zone to the caller: "+zone);
		
		return zone;
	}
	@Override
	public List<Zone> createZones(Connection conn, List<Zone> zones, int zoneSetId, int operatorId, int productId, Date requestedDate, String requestedUser) throws ZoneException, DuplicateZoneException{
		List<Zone> returnList = new ArrayList<Zone>();
		if(zones != null){
			for(Zone zone : zones){
				Zone createdZone = null;
				try {
					createdZone = 	createNewZoneWithoutCommit(conn, operatorId, zoneSetId, productId, zone.getZoneName(), 
									zone.getOperators(), zone.getGeographies(), zone.getServices(), zone.getIncludedPlan(), zone.getZonePreferences(), 
									zone.getTechnology(), requestedDate, requestedUser);
					returnList.add(createdZone);
				} catch (ZoneException e) {
					LOG.error("Exception Occurred :"+e);
					throw e;
				} catch (DuplicateZoneException e) {
					LOG.error("Exception occurred :"+e);
					throw e;
				}
				
			}
		}
		
		return returnList;
	}
	
	private Zone createNewZoneWithoutCommit(Connection conn, int operatorId, int zoneSetId, int productId, String zoneName, List<String> operators, List<String> geographies,
			EnrolledServices services, IncludedPlanDetails includedPlan, List<ZonePreference> zonePreferences, String technology,Date requestedDate, String requestedUser) 
			throws ZoneException, DuplicateZoneException {
		Zone zone = null;
		int newZoneId = -1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			// Validate Zone Name Exists
			boolean zoneExists = validateZoneName(operatorId, productId, zoneSetId, zoneName, 0, conn);
			
			if(zoneExists)
			{
				throw new DuplicateZoneException("Zone Name already Exists: "+zoneName);
			}
			
			LOG.debug("Creating Prepared Statement to create zone data in roaming_zones table");
			LOG.debug("SQL_CREATE_NEW_ZONE :"+SQL_CREATE_NEW_ZONE);
			
			ps = conn.prepareStatement(SQL_CREATE_NEW_ZONE, new String[]{"zone_id"});

			LOG.debug("Setting Parameters");
			
			ps.setString(1, zoneName);// zone_name
			ps.setInt(2, productId);// product_id
			ps.setString(3, StringUtils.join(operators, ","));// operators
			ps.setString(4, StringUtils.join(geographies, ","));// geographies
			ps.setInt(5, operatorId);// operator_id
			ps.setInt(6, services.isAllowMOSms() ? 1 : 0);// mo_sms_allowed
			ps.setInt(7, services.isAllowMTSms() ? 1 : 0);// mt_sms_allowed
			ps.setInt(8, services.isAllowMOVoice() ? 1 : 0);// mo_voice_allowed
			ps.setInt(9, services.isAllowMTVoice() ? 1 : 0);// mt_voice_allowed
			ps.setInt(10, services.isAllowPacket() ? 1 : 0);// packet_allowed
			ps.setDouble(11, includedPlan.getPerMOSmsPrice());// mo_sms_cost
			ps.setDouble(12, includedPlan.getPerMTSmsPrice());// mt_sms_cost
			ps.setDouble(13, includedPlan.getPerMinMOVoicePrice());// mo_voice_cost
			ps.setDouble(14, includedPlan.getPerMinMTVoicePrice());// mt_voice_cost
			ps.setDouble(15, includedPlan.getPerKBPacketPrice());// packet_cost
			ps.setInt(16, includedPlan.getIncludedMOSms());// mo_sms_count_included
			ps.setInt(17, includedPlan.getIncludedMTSms());// mt_sms_count_included
			ps.setInt(18, includedPlan.getIncludedMOVoiceMins());// mo_voice_mins_included
			ps.setInt(19, includedPlan.getIncludedMTVoiceMins());// mt_voice_mins_included
			ps.setLong(20, includedPlan.getIncludedPacketKB());// packet_kb_included
			ps.setString(21, StringUtils.join(zonePreferences, ","));// zone_preferences
			ps.setString(22, requestedUser);// created_by
			ps.setDate(23, new java.sql.Date(requestedDate.getTime()));// date_created
			ps.setString(24, requestedUser);// last_modified_by
			ps.setDate(25, new java.sql.Date(requestedDate.getTime()));// last_modified_date
			if(zoneSetId > 0)
				ps.setInt(26, zoneSetId);
			else
				ps.setNull(26, Types.INTEGER);
			ps.setString(27, technology);

			ps.executeUpdate();
			
			// Read the newly generated zone id
			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				newZoneId = rs.getInt(1);
			}

			// Return created zone
			zone = new Zone();

			zone.setZoneId(newZoneId);
			zone.setZoneName(zoneName);
			zone.setProductId(productId);
			zone.setOperators(operators);
			zone.setGeographies(geographies);
			zone.setServices(services);			
			zone.setIncludedPlan(includedPlan);
			zone.setZonePreferences(zonePreferences);			
			zone.setOperatorId(operatorId);
			zone.setZoneSetId(zoneSetId);
			zone.setTechnology(technology);
			
			LOG.info("Created New Zone = " + zone);
		} catch (DuplicateZoneException e) {
			LOG.error("Exception in createNewZone :"+e);			
			throw e;
		} catch (SQLException sqle) {
			LOG.error("Exception in createNewZone - DB Error during creating zone", sqle);			
			throw new ZoneException("Exception in createNewZone - DB Error during creating zone", sqle);
		} catch (Exception e) {
			LOG.error("Create Zone failed due to fatal error:"+e);			
			throw new ZoneException("Create Zone failed due to fatal error:"+e);
		}finally{
			DBUtils.cleanup(null, ps, rs);
		}

		return zone;
	}
	@Override
	public Zone createNewZone(int operatorId, int zoneSetId, int productId, String zoneName, List<String> operators, List<String> geographies,
			EnrolledServices services, IncludedPlanDetails includedPlan, List<ZonePreference> zonePreferences, String technology,Date requestedDate, String requestedUser) 
			throws ZoneException, DuplicateZoneException {
		Connection conn = null;
		Zone zone = null;

		LOG.info("createNewZone: processing create zone...");
		
		try {
			LOG.debug("Fetched db connection");
			
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Auto Commit off
			conn.setAutoCommit(false);
			
			zone = createNewZoneWithoutCommit(conn, operatorId, zoneSetId, productId, zoneName, operators, geographies, services, includedPlan, zonePreferences, technology, requestedDate, requestedUser);
			
			// Commit the changes
			if(conn != null)
			{
				conn.commit();
			}
			
			// Put In Cache
			putInCache(zone);			
			
			LOG.info("Created New Zone = " + zone);
		} catch (DuplicateZoneException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (SQLException sqle) {
			LOG.error("Exception during creating zone in roaming_zones" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Exception in createNewZone - DB Error during creating zone", sqle);
		} catch (Exception e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Create Zone failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn);
		}

		return zone;
	}
	
	@Override
	public Zone updateZone(int operatorId, int zoneSetId, int zoneId, int productId, String zoneName, List<String> operators, List<String> geographies, EnrolledServices services,
			IncludedPlanDetails includedPlan, List<ZonePreference> zonePreferences, String technology, Date requestedDate, String requestedUser)
			throws ZoneNotFoundException, ZoneException, DuplicateZoneException, ZoneMappingException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		Zone zone = null;

		LOG.info("updateZone: processing update zone...");
		
		try {
			LOG.debug("Fetched db connection");
			
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Auto Commit off
			conn.setAutoCommit(false);	
			
            // Zone override validation 
			// validateZoneOverride(operatorId,zoneSetId, zoneId, conn);
			// validateZoneOverrideSProfile(operatorId,zoneSetId, zoneId, conn);

			// Validate Zone Name Exists
			boolean zoneExists = validateZoneName(operatorId,productId, zoneSetId, zoneName, zoneId, conn);
			
			if(zoneExists){
				throw new DuplicateZoneException("Zone Name already Exists: "+zoneName);
			}
			
			LOG.debug("Creating Prepared Statement to update the data in roaming_zones table");
			LOG.debug("SQL_UPDATE_ZONE :"+SQL_UPDATE_ZONE);
			ps = conn.prepareStatement(SQL_UPDATE_ZONE);

			LOG.debug("Setting Parameters");
			
			ps.setString(1, zoneName);// zone_name
			ps.setInt(2, productId);// product_id
			ps.setString(3, StringUtils.join(operators, ","));// operators
			ps.setString(4, StringUtils.join(geographies, ","));// geographies
			ps.setInt(5, services.isAllowMOSms() ? 1 : 0);// mo_sms_allowed
			ps.setInt(6, services.isAllowMTSms() ? 1 : 0);// mt_sms_allowed
			ps.setInt(7, services.isAllowMOVoice() ? 1 : 0);// mo_voice_allowed
			ps.setInt(8, services.isAllowMTVoice() ? 1 : 0);// mt_voice_allowed
			ps.setInt(9, services.isAllowPacket() ? 1 : 0);// packet_allowed
			ps.setDouble(10, includedPlan.getPerMOSmsPrice());// mo_sms_cost
			ps.setDouble(11, includedPlan.getPerMTSmsPrice());// mt_sms_cost
			ps.setDouble(12, includedPlan.getPerMinMOVoicePrice());// mo_voice_cost
			ps.setDouble(13, includedPlan.getPerMinMTVoicePrice());// mt_voice_cost
			ps.setDouble(14, includedPlan.getPerKBPacketPrice());// packet_cost
			ps.setInt(15, includedPlan.getIncludedMOSms());// mo_sms_count_included
			ps.setInt(16, includedPlan.getIncludedMTSms());// mt_sms_count_included
			ps.setInt(17, includedPlan.getIncludedMOVoiceMins());// mo_voice_mins_included
			ps.setInt(18, includedPlan.getIncludedMTVoiceMins());// mt_voice_mins_included
			ps.setLong(19, includedPlan.getIncludedPacketKB());// packet_kb_included
			ps.setString(20, StringUtils.join(zonePreferences, ","));// zone_preferences
			ps.setString(21, requestedUser);// last_modified_by
			ps.setDate(22, new java.sql.Date(requestedDate.getTime()));// last_modified_date
			ps.setString(23, technology);
			ps.setLong(24, operatorId);// zone_id
			ps.setLong(25, zoneId);// zone_id
			if(zoneSetId > 0)
				ps.setInt(26, zoneSetId); //zone_set_id
			else
				ps.setNull(26, Types.INTEGER);
			
			int updateCount = ps.executeUpdate();
			
			if (updateCount <= 0) {
				LOG.error("Zone does not exist for zone id: " + zoneId);
				throw new ZoneNotFoundException("Zone does not exist for zone id: " + zoneId);
			}

			// Commit the changes
			if(conn != null)
			{
				conn.commit();
			}		
			
			// Return updated zone
			zone = new Zone();

			zone.setZoneId(zoneId);
			zone.setZoneName(zoneName);
			zone.setProductId(productId);
			zone.setOperators(operators);
			zone.setGeographies(geographies);
			zone.setServices(services);
			zone.setIncludedPlan(includedPlan);
			zone.setZonePreferences(zonePreferences);
			zone.setOperatorId(operatorId);
			zone.setZoneSetId(zoneSetId);
			zone.setTechnology(technology);
			// Put In Cache
			putInCache(zone);	
			
			LOG.info("Updated Zone Successfully = " + zone);
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured while updating zone" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (DuplicateZoneException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} /*catch (ZoneMappingException e) {
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		}*/ catch (SQLException sqle) {
			LOG.error("Exception during updating zone in roaming_zones - db error" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Exception during updateZone - DB error", sqle);
		} catch (Exception e) {
			LOG.error("Exception during updating zone in roaming_zones - fatal error" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Updating Zone failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return zone;
	}

	@Override
	public boolean deleteZone(int operatorId, int zoneSetId, int zoneId, Date requestedDate, String requestedUser)
			throws ZoneNotFoundException, ZoneException, ZoneMappingException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		boolean success = false;

		LOG.info("deleteZone: processing delete zone...");
		
		try {
			LOG.debug("Fetched db connection");
			
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			// Auto commit off
			conn.setAutoCommit(false);
			//Zone override validation 
			validateZoneOverride(operatorId,zoneSetId, zoneId, conn);
			validateZoneOverrideSProfile(operatorId,zoneSetId, zoneId, conn);

			LOG.debug("Creating Prepared Statement to delete the data in roaming_zones table");
			
			if(zoneSetId > 0){
				ps = conn.prepareStatement(SQL_DELETE_ZONE);
				LOG.debug("Setting Parameters");
				ps.setLong(1, operatorId);// operator_id
				ps.setLong(2, zoneId);// zone_id
				ps.setLong(3, zoneSetId);
			}else{
				ps = conn.prepareStatement(SQL_DELETE_ZONE_WITHOUT_ZONESET_ID);
				LOG.debug("Setting Parameters");
				ps.setLong(1, operatorId);// operator_id
				ps.setLong(2, zoneId);// zone_id
			}
			
			int updateCount = ps.executeUpdate();
			
			if (updateCount <= 0) {
				LOG.error("Zone does not exist for zone id: " + zoneId);
				throw new ZoneNotFoundException("Zone does not exist for zone id: " + zoneId);
			}
			
			// Commit the changes
			if(conn != null)
			{
				conn.commit();
			}
			
			// Mark delete as successful
			success = true;

			// Put In Cache
			cache.evict(zoneId);	
						
			LOG.info("Deleted Zone Successfully");
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured while deleting zone" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting zone in roaming_zones - db error" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Exception during deleting zone - DB error", sqle);
		} catch (ZoneException e) {
			throw e ;
		}catch (ZoneMappingException e) {
			throw e ;
		} catch (Exception e) {
			LOG.error("Exception during deleting zone in roaming_zones - fatal error" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new ZoneException("Deleting Zone failed due to fatal error:"+e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		return success;
	}
	
	@Override
	public List<Zone> getAllZones() {
		LOG.info("getAllZones: processing get all zones");
		
		// If not already loaded, fetch from db
		List<Zone> zones = new ArrayList<Zone>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("getAllZones: Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			LOG.debug("Creating Prepared Statement to fetch all zones from roaming_zones table");
			
			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			LOG.debug("getAllZones(): Calling buildGetZonesQuery(conn,0, 0, 0)...");
			ps = buildGetZonesQuery(conn,0, 0);

			rs = ps.executeQuery();

			while (rs.next()) {
				Zone zone = new Zone();
				zone.setZoneId(rs.getInt("zone_id"));
				zone.setZoneName(rs.getString("zone_name"));
				zone.setProductId(rs.getInt("product_id"));
				zone.setOperatorId(rs.getInt("operator_id"));
				
				// Set Operators 
				setOperators(rs, zone);
				
				// Set Geographies
				setGeographies(rs, zone);

				// Set Services
				setServices(rs, zone);
				
				// Set Zone Preferences
				setZonePreferences(rs, zone);

				zone.setZoneSetId(rs.getInt("zone_set_id"));
                
                zone.setTechnology(rs.getString("technology"));
				
				zones.add(zone);
				
				// Store in cache
				cache.put(zone.getZoneId(), zone);
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during getting zones by product in roaming_zones - db error" + sqle);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + sqle);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
		} catch (Exception e) {
			LOG.error("Exception during getting zones by product  in roaming_zones - fatal error" + e);
			
			if (conn != null) {
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAllZones: Returned " + zones.size() + " zones to the caller");

		return zones;
	}

	private void setIncludedPlan(ResultSet rs, Zone zone) throws SQLException {
		IncludedPlanDetails includedPlan = new IncludedPlanDetails();
		
		includedPlan.setPerKBPacketPrice(rs.getDouble("packet_cost"));
		includedPlan.setPerMinMOVoicePrice(rs.getDouble("mo_voice_cost"));
		includedPlan.setPerMinMTVoicePrice(rs.getDouble("mt_voice_cost"));
		includedPlan.setPerMOSmsPrice(rs.getDouble("mo_sms_cost"));
		includedPlan.setPerMTSmsPrice(rs.getDouble("mt_sms_cost"));
		
		includedPlan.setIncludedMOSms(rs.getInt("mo_sms_count_included"));
		includedPlan.setIncludedMTSms(rs.getInt("mt_sms_count_included"));
		includedPlan.setIncludedMTVoiceMins(rs.getInt("mt_voice_mins_included"));
		includedPlan.setIncludedMOVoiceMins(rs.getInt("mo_voice_mins_included"));
		includedPlan.setIncludedPacketKB(rs.getInt("packet_kb_included"));

		zone.setIncludedPlan(includedPlan);
	}

	private void setServices(ResultSet rs, Zone zone) throws SQLException {
		EnrolledServices enrolledServices = new EnrolledServices();

		enrolledServices.setAllowMOSms(rs.getInt("mo_sms_allowed") == 1);
		enrolledServices.setAllowMTSms(rs.getInt("mt_sms_allowed") == 1);
		enrolledServices.setAllowMOVoice(rs.getInt("mo_voice_allowed") == 1);
		enrolledServices.setAllowMTVoice(rs.getInt("mt_voice_allowed") == 1);
		enrolledServices.setAllowPacket(rs.getInt("packet_allowed") == 1);

		zone.setServices(enrolledServices);
	}

	private void setGeographies(ResultSet rs, Zone zone) throws SQLException {
		String geographies = rs.getString("geographies");

		if (StringUtils.isNotEmpty(geographies)) {
			String[] geos = geographies.replaceAll("^[,\\s]+", "").split("[,\\s]+");
			List<String> geosList = new ArrayList<String>();
			Collections.addAll(geosList, geos);

			zone.setGeographies(geosList);
		}
	}

	private void setOperators(ResultSet rs, Zone zone) throws SQLException {
		String operators = rs.getString("operators");

		if (StringUtils.isNotEmpty(operators)) {
			String[] ops = operators.replaceAll("^[,\\s]+", "").split("[,\\s]+");
			List<String> operatorsList = new ArrayList<String>();
			Collections.addAll(operatorsList, ops);

			zone.setOperators(operatorsList);
		}
	}
	
	public void putInCache(Zone... zones) {
		for (Zone zone : zones) {
			cache.put(zone.getZoneId(), zone);
		}
	}
	
	private boolean validateZoneName(int operatorId,int productId, int zoneSetId, String zoneName, int filteredZoneId, Connection conn)
	{		
		LOG.info("Creating Prepared Statement to validate zone name in roaming_zones table");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		boolean zoneExists = false;
		
		try
		{	// If zoneSetId is not specified, then validate the Zone Name for the specified product. Otherwise validate it for the specified Zone Set
			if(zoneSetId > 0){
				ps = conn.prepareStatement(SQL_GET_ZONE_COUNT_BY_ZONE_SET_ID);
				ps.setInt(1, operatorId);
				ps.setString(2, zoneName);
				ps.setLong(3, filteredZoneId);
				ps.setInt(4, zoneSetId);
			}else{
				ps = conn.prepareStatement(SQL_GET_ZONE_COUNT_BY_PRODUCT_ID);
				ps.setInt(1, operatorId);
				ps.setString(2, zoneName);
				ps.setLong(3, filteredZoneId);
				ps.setInt(4, productId);
			}
			
			
	
			LOG.info("getZoneNameCount: validating if zone name exists: " + zoneName);
			rs = ps.executeQuery();

			if (rs.next()) {
				zoneExists = (rs.getInt("zone_cnt") > 0);// Set if zone
																// exists
			}
		} catch (SQLException e) {
			LOG.error("Exception during getZoneNameCount - validateZoneName", e);
			throw new GenericServiceException("Exception during getZoneNameCount", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		
		LOG.info("Zone Name already exists: "+zoneExists);		
		
		return zoneExists;
	}
	
	
	/**
	 * validates whether zone is already mapped to one or more rate plans or not
	 * 
	 * @param operatorId
	 * @param zoneId
	 * @param conn
	 * @return
	 * @throws ZoneException
	 * @throws ZoneMappingException
	 * @throws ZoneNotFoundException
	 */
	private boolean validateZoneOverride(int operatorId, int zoneSetId, int zoneId, Connection conn) throws ZoneException, ZoneMappingException, ZoneNotFoundException {		
		LOG.info("Creating Prepared Statement to validate RATE_PLAN_ZONE_RATING_POLICY in roaming_zones table");
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean zoneExists = false;
		try {	
			ps = conn.prepareStatement("select count(*) from RATE_PLAN_ZONE_RATING_POLICY where zone_id = ?");
			ps.setLong(1, zoneId);
			LOG.info("validating if zone is already associated with rate plans: " + zoneId);
			rs = ps.executeQuery();

			if (rs.next()) {
				zoneExists = (rs.getInt(1) > 0);
				if(zoneExists) {
					Zone zone = getZone(operatorId, zoneSetId, zoneId);
					LOG.info(String.format("Zone %s already associated with one or more rate plans", zone.getZoneName()));
					throw new ZoneMappingException(String.format("Zone %s already associated with one or more rate plans", zone.getZoneName()));
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception during validateZoneOverride", e);
			throw new GenericServiceException("Exception during validateZoneOverride", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		LOG.info("Zone already associated with one or more rate plans ?"+ zoneExists);		
		return zoneExists;
	}
	
	
	/**
	 * validates whether zone is already mapped to service profiles or not
	 * 
	 * @param operatorId
	 * @param zoneId
	 * @param conn
	 * @return
	 * @throws ZoneException
	 * @throws ZoneMappingException
	 * @throws ZoneNotFoundException
	 */
	private boolean validateZoneOverrideSProfile(int operatorId, int zoneSetid, int zoneId, Connection conn) throws ZoneException, ZoneMappingException, ZoneNotFoundException {		
		LOG.info("Creating Prepared Statement to validate RATE_PLAN_ZONE_RATING_POLICY in service_zone_config table");
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean zoneExists = false;
		try {	
			ps = conn.prepareStatement("select count(*) from service_zone_config where zone_id = ?");
			ps.setLong(1, zoneId);
			LOG.info("deleteZone: validating if zone is already associated with service profiles: " + zoneId);
			rs = ps.executeQuery();

			if (rs.next()) {
				zoneExists = (rs.getInt(1) > 0);
				if(zoneExists) {
					Zone zone = getZone(operatorId, zoneSetid, zoneId);
					LOG.info(String.format("Zone %s already associated with one or more service profiles", zone.getZoneName()));
					throw new ZoneMappingException(String.format("Zone %s already associated with one or more service profiles", zone.getZoneName()));
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception during validateZoneOverrideSProfile", e);
			throw new GenericServiceException("Exception during validateZoneOverrideSProfile", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		LOG.info("Zone already associated with one or more service profiles? "+ zoneExists);		
		return zoneExists;
	}
}
