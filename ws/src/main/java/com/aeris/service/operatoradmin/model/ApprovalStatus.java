package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum ApprovalStatus implements Serializable {
	APPROVED(1), PENDING(2);
	int value;

	ApprovalStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static ApprovalStatus fromValue(int value) {
		ApprovalStatus[] statuses = values();

		for (ApprovalStatus approvalStatus : statuses) {
			if (approvalStatus.getValue() == value) {
				return approvalStatus;
			}
		}

		return null;
	}

	public static ApprovalStatus fromName(String value) {
		ApprovalStatus[] statuses = values();

		for (ApprovalStatus approvalStatus : statuses) {
			if (approvalStatus.name().equalsIgnoreCase(value)) {
				return approvalStatus;
			}
		}

		return null;
	}
}
