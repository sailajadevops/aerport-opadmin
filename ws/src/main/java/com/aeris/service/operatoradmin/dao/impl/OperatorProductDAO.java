package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.IOperatorProductDAO;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.utils.DBUtils;

public class OperatorProductDAO implements IOperatorProductDAO{

	private static final String SQL_GET_PRODUCTS_BY_OPERATOR = "SELECT a.product_id,"
            + "  b.display_name,"
            + "  a.technology,"
            + "  a.carrier_id,"
            + "  a.msisdn_range,"
            + "  a.static_ip_range,"
            + "  a.IS_SUBSCRIPTION_LEVEL_ONLY,"
            + "  RTRIM (replace(replace (xmlagg (xmlelement (e, pt.technology || ',')),'<E>'),'</E>'), ',') technologies"
            + " FROM product a"
            + " INNER JOIN ws_operator_products b ON a.product_id = b.product_id"
            + " AND b.operator_id  = ?"
            + " LEFT JOIN product_technology pt ON pt.product_id = a.product_id"
            + " group by "
            + "   (a.product_id,"
            + "  b.display_name,"
            + "  a.technology,"
            + "  a.carrier_id,"
            + "  a.msisdn_range,"
            + "  a.static_ip_range,"
            + "  a.IS_SUBSCRIPTION_LEVEL_ONLY"
            + ")";
		
	private Cache<Integer, List<Product>> cache;

	private static Logger LOG = LoggerFactory.getLogger(OperatorProductDAO.class);
	
	public void init(@Hazelcast(cache = "OperatorProductCache/id") Cache<Integer, List<Product>> cache) {
		this.cache = cache;
	}

	@Override
	public List<Product> getProductsByOperator(final int operatorId) {
		LOG.debug("getProductsByOperator: processing get products by operator");
		
		// If already loaded, fetch from cache
		if(cache.get(operatorId) != null)
		{
			return cache.get(operatorId);
		}
		
		List<Product> products = new ArrayList<Product>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			LOG.debug("getProductsByOperator: Fetched db connection");

			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("Creating Prepared Statement to fetch products data by operator from ws_operator_products table");
			
			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			ps = buildGetProductsByOperatorQuery(conn, operatorId);

			LOG.debug("getProductsByOperator: executed query: " + ps);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Product product = new Product();
				
				product.setProductId(rs.getInt("product_id"));
				product.setProductName(rs.getString("display_name"));
				product.setCarrierId(rs.getInt("carrier_id"));
                product.setTechnology(rs.getString("technology"));
                String technologies = rs.getString("technologies");
                if (technologies != null) {
                    String[] techStrings = technologies.split(",");
                    product.setTechnologies(techStrings);
                } else {
                    String[] techStrings = new String[1];
                    techStrings[0] = rs.getString("technology");
                    product.setTechnologies(techStrings);
                }
                int subscritionLevelOnly = rs.getInt("IS_SUBSCRIPTION_LEVEL_ONLY");
                if (subscritionLevelOnly == 1) {
                    product.setSubcriptionLevelOnly(true);
                } else {
                    product.setSubcriptionLevelOnly(false);
                }
				products.add(product);
			}
			Collections.sort(products);
			cache.put(operatorId, products);

		} catch (SQLException e) {
			LOG.error("Exception during getProductsByOperator", e);
			throw new GenericServiceException("Unable to getProductsByOperator", e);
		} catch (GenericServiceException e) {
			LOG.error("ServiceException in getProductsByOperator" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getProductsByOperator: Returned " + products.size() + " products to the caller");

		return products;
	}

	private PreparedStatement buildGetProductsByOperatorQuery(Connection conn, int operatorId) throws SQLException {
		PreparedStatement ps;
		
		// Get Products by operator
		StringBuilder query = new StringBuilder(SQL_GET_PRODUCTS_BY_OPERATOR);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());
		ps.setInt(1, operatorId);

		return ps;
	}

    @Override
    public Product getProductByOperatorAndProductId(int operatorId, int productId) {
    	List<Product> products = getProductsByOperator(operatorId);
        
        for (Product product : products) {
            if (product.getProductId() == productId) {
                return product;
            }
        }
        return null;
    }
}
