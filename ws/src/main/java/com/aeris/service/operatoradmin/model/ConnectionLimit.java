package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class ConnectionLimit implements Serializable{
	private long serviceCode;
	private long connectionLimit;
	public long getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(long serviceCode) {
		this.serviceCode = serviceCode;
	}
	public long getConnectionLimit() {
		return connectionLimit;
	}
	public void setConnectionLimit(long connectionLimit) {
		this.connectionLimit = connectionLimit;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
