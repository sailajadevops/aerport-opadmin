package com.aeris.service.operatoradmin.resources;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.ZoneException;
import com.aeris.service.operatoradmin.exception.ZoneMappingException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.EnrolledServices;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZonePreference;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateZoneRequest;
import com.aeris.service.operatoradmin.payload.UpdateZoneRequest;

/**
 * Rest Resource for Zone management per operator. 
 * Includes methods for
 * 
 * <ul>
 * <li>
 * Fetch the zones per operator
 * </li>
 * <li>
 * Fetch the zones per product
 * </li>
 * <li>
 * Fetch a specific zone by zone id
 * </li>
 * <li>
 * Create a new zone
 * </li>
 * <li>
 * Edit an existing zone
 * </li>
 * <li>
 * Delete an existing zone
 * </li>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}/zones")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class ZoneResource {
	private static final Logger LOG = LoggerFactory.getLogger(ZoneResource.class);

	@Inject
	private IZoneDAO zoneDAO;

	/**
	 * Fetch available Zones for given operator & zone set 
	 * 
	 * @param operatorId The operator identifier
	 * @param zoneSetId The zoneSet identifier
	 * 
	 * @return A list of zones matching zoneSetId and operatorId 
	 */
	@GET 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"}) 
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllZones(@PathParam("operatorId") @ValidOperator final String operatorId, @QueryParam("zoneSetId") @Pattern(regexp = "[0-9]+", message = "{zoneSetId.number}") final String zoneSetId) {
		LOG.info("Received request to fetch all zones for operator id: " + operatorId+" , zoneSetId="+zoneSetId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		List<Zone> zones = null;
		
		try {
			LOG.info("Fetching zones..");
			if(NumberUtils.isNumber(zoneSetId)){
				zones = zoneDAO.getAllZones(Integer.parseInt(operatorId),Integer.parseInt(zoneSetId));
			}else{
				zones = zoneDAO.getAllZones(Integer.parseInt(operatorId),0);
			}
				
			if (zones != null) {
				LOG.info("fetched zones successfully, no of zones fetched: " + zones.size());

				ObjectMapper mapper = new ObjectMapper();
				String zonesJson = mapper.writeValueAsString(zones);

				builder.entity(zonesJson);
			} else {
				LOG.info(String.format("No zones found for the operator id(%s), zoneSetId(%s) ",operatorId,zoneSetId));
				return Response.status(Status.NOT_FOUND).entity(String.format("No zones found for the operator id(%s), zoneSetId(%s) ",operatorId,zoneSetId)).build();
			}
		} catch (ZoneException e) {
			LOG.error("ZoneException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getAllZones Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting zones").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllZones: " + response);
		return response;
	}
	
	/**
	 * Fetch a specific zone details matching the zone id.
	 * 
	 * @param operatorId The operator identifier
	 * @param zoneId The zone identifier against which the zone details are retreived
	 * 
	 * @return The zone details matching the zone id 
	 */
	@Path("/{zoneId}")
	@GET @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"}) 
	public Response getZone(@PathParam("operatorId") @ValidOperator final String operatorId,@PathParam("zoneId") @Pattern(regexp = "[0-9]+", message = "{zoneId.number}") final String zoneId,
			@QueryParam("zoneSetId") @Pattern(regexp = "[0-9]+", message = "{zoneSetId.number}") final String zoneSetId) {
		LOG.info("Received request to fetch zone for zone id: " + zoneId);
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		Zone zone = null;

		try {
			// Fetch from database
			if(NumberUtils.isNumber(zoneSetId)){
				zone = zoneDAO.getZone(Integer.parseInt(operatorId), Integer.parseInt(zoneSetId), Integer.parseInt(zoneId));
			}else{
				zone = zoneDAO.getZone(Integer.parseInt(operatorId), 0, Integer.parseInt(zoneId));
			}

			if (zone != null) {
				LOG.info("fetched zone successfully for zoneId: " + zoneId);

				ObjectMapper mapper = new ObjectMapper();
				String zoneJson = mapper.writeValueAsString(zone);

				builder.entity(zoneJson);
			} else {
				LOG.info("No zone found for the zoneId : " + zoneId);
				return Response.status(Status.NOT_FOUND).entity("No zone found for the zoneId").build();
			}
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (ZoneException e) {
			LOG.error("ZoneException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getZone Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting zone").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getZone: " + response);
		return response;
	}

	/**
	 * Create a new zone under the operator with the information
	 * specified in the {@link CreateZoneRequest}
	 *  
	 * @param operatorId The operator against which the zone is being created
	 * @param request The request payload holding the list of geographies or carriers/operators
	 * available in the zone
	 * 
	 * @return The newly create zone details
	 */
	@POST 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createNewZone(@PathParam("operatorId") @ValidOperator final String operatorId, @QueryParam("zoneSetId")  @Pattern(regexp = "[0-9]+", message = "{zoneSetId.number}") final String zoneSetId,
			@Valid CreateZoneRequest request) {

		LOG.info("Received request to create new zone under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String zoneName = request.getZoneName();
		int productId = Integer.parseInt(request.getProductId());
		List<String> geographies = request.getGeographies();
		List<String> operators = request.getOperators();
		EnrolledServices services = request.getServices();
		List<ZonePreference> zonePreferences = request.getZonePreferences();
		IncludedPlanDetails includedPlan = request.getIncludedPlan();
		String userId = request.getUserId();
		Date requestedDate = new Date();

		try {
			LOG.info("Calling ZoneDAO.createNewZone()");
			Zone zone = null;
			if(NumberUtils.isNumber(zoneSetId)){
				zone = zoneDAO.createNewZone(Integer.parseInt(operatorId),Integer.parseInt(zoneSetId), productId, zoneName, operators, geographies, services, includedPlan, zonePreferences, request.getTechnology(), requestedDate, userId);
			}else{
				zone = zoneDAO.createNewZone(Integer.parseInt(operatorId),0, productId, zoneName, operators, geographies, services, includedPlan, zonePreferences, request.getTechnology(), requestedDate, userId);
			}
			

			ObjectMapper mapper = new ObjectMapper();
			String zoneJson = mapper.writeValueAsString(zone);
			builder.entity(zoneJson);

			LOG.info("createNewZone() Response: " + zoneJson);
		} catch (DuplicateZoneException e) {
			LOG.error("DuplicateZoneException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ZoneException e) {
			LOG.error("ZoneException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("createNewZone: Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while creating zone").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewZone: " + response);
		return response;
	}

	/**
	 * Update an existing zone under the specified operator with the information
	 * specified in the {@link UpdateZoneRequest}
	 *  
	 * @param operatorId The operator against which the zone is being created
	 * @param zoneId The zone identifier
	 * @param request The update request payload holding the list of geographies or carriers/operators
	 * available in the zone
	 * 
	 * @return The zone details with the updated information
	 */
	@Path("/{zoneId}")
	@PUT 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateZone(@PathParam("operatorId") @ValidOperator final String operatorId, @QueryParam("zoneSetId")  @Pattern(regexp = "[0-9]+", message = "{zoneSetId.number}") final String zoneSetId,
			@PathParam("zoneId") @Pattern(regexp = "[0-9]+", message = "{zoneId.number}") final String zoneId,
			@Valid UpdateZoneRequest request) {

		LOG.info("Received request to update zone under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		String zoneName = request.getZoneName();
		int productId = Integer.parseInt(request.getProductId());
		List<String> geographies = request.getGeographies();
		List<String> operators = request.getOperators();
		EnrolledServices services = request.getServices();
		List<ZonePreference> zonePreferences = request.getZonePreferences();
		IncludedPlanDetails includedPlan = request.getIncludedPlan();
		String userId = request.getUserId();
		Date requestedDate = new Date();

		try {
			LOG.info("Calling ZoneDAO.updateZone()");
			Zone zone = null;
			if(NumberUtils.isNumber(zoneSetId)){
				zone = zoneDAO.updateZone(Integer.parseInt(operatorId), Integer.parseInt(zoneSetId), Integer.parseInt(zoneId), productId, zoneName, operators, geographies, services, includedPlan, zonePreferences, request.getTechnology(), requestedDate, userId);
			}else{
				zone = zoneDAO.updateZone(Integer.parseInt(operatorId), 0, Integer.parseInt(zoneId), productId, zoneName, operators, geographies, services, includedPlan, zonePreferences, request.getTechnology(), requestedDate, userId);
			}
			

			ObjectMapper mapper = new ObjectMapper();
			String zoneJson = mapper.writeValueAsString(zone);
			builder.entity(zoneJson);

			LOG.info("updateZone() Response: " + zoneJson);
		} catch (DuplicateZoneException e) {
			LOG.error("DuplicateZoneException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ZoneMappingException e) {
			LOG.error("ZoneMappingException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (ZoneException e) {
			LOG.error("ZoneException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateZone: Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while updating zone").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateZone: " + response);
		return response;
	}

	/**
	 * Delete an existing zone matching the zone id under the operator specified by operator id
	 * 
	 * @param operatorId The operator identifier
	 * @param zoneId The zone id for the zone which will be deleted
	 * @param userId The user who requested the operation
	 * 
	 * @return true if the operation is successful, otherwise false
	 */
	@DELETE 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	@Path("/{zoneId}")
	public Response deleteZone(@PathParam("operatorId") @ValidOperator final String operatorId, @QueryParam("zoneSetId")  @Pattern(regexp = "[0-9]+", message = "{zoneSetId.number}") final String zoneSetId,
			@PathParam("zoneId") @Pattern(regexp = "[0-9]+", message = "{zoneId.number}") final String zoneId,
			@QueryParam("userId") @Email(message = "{userId.email}") final String userId) {
		LOG.info("Received request to delete zone with zone id: " + zoneId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		Date requestedDate = new Date();

		try {
			// Fetch from database
			boolean success = false;
			if(NumberUtils.isNumber(zoneSetId)){
				success = zoneDAO.deleteZone(Integer.parseInt(operatorId), Integer.parseInt(zoneSetId), Integer.parseInt(zoneId), requestedDate, userId);
			}else{
				success = zoneDAO.deleteZone(Integer.parseInt(operatorId), 0, Integer.parseInt(zoneId), requestedDate, userId);
			}

			if (!success) {
				LOG.info("deleteZone failed for zoneId: " + zoneId);
				return Response.status(Status.NOT_FOUND).entity("deleteZone failed for zoneId: " + zoneId).build();
			} else {
				LOG.info("Zone with zone id: " + zoneId + " deleted successfully");

				ObjectMapper mapper = new ObjectMapper();
				String deleteJson = mapper.writeValueAsString(success);

				builder.entity(deleteJson);
			}
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (ZoneMappingException e) {
			LOG.error("ZoneMappingException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (ZoneException e) {
			LOG.error("ZoneException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("deleteZone Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while deleting zone").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from deleteZone: " + response);
		return response;
	}
}
