package com.aeris.service.operatoradmin.delegate;

import java.util.Map;

import javax.mail.Message;

public interface IEmailDelegate {
	boolean sendEmail(String fromEmailAddress, Map<Message.RecipientType, String[]> receipentEmailAddresses, String subject, String body);
}
