package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum ContactType implements Serializable {
	BILLING(1), OPERATIONS(4), TECHNICAL(5), ADMIN(3);
	int value;

	ContactType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static ContactType fromValue(int value) {
		ContactType[] types = values();

		for (ContactType contactType : types) {

			if (contactType.getValue() == value) {
				return contactType;
			}
		}

		return null;
	}

	public static ContactType fromName(String value) {
		ContactType[] types = values();

		for (ContactType contactType : types) {

			if (contactType.name().equalsIgnoreCase(value)) {
				return contactType;
			}
		}

		return null;
	}
}
