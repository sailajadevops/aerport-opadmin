package com.aeris.service.operatoradmin.delegate.impl;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.delegate.IEmailDelegate;
import com.aeris.service.operatoradmin.model.EmailProperties;
import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class EmailDelegate implements IEmailDelegate {
	private static final String SMTP = "smtp";

	private static Logger LOG = LoggerFactory.getLogger(EmailDelegate.class);

	private static Session emailSession;

	@Inject
	private EmailProperties emailProperties;

	private Session getEmailSession() {
		if (emailSession == null) {
			synchronized (EmailDelegate.class) {
				if (emailSession == null) {
					Properties properties = System.getProperties();

					// Setup aeris mail server
					properties.setProperty("mail.smtp.host", emailProperties.getMailSmtpHost());
					properties.setProperty("mail.smtp.port", emailProperties.getMailSmtpPort());
					properties.setProperty("mail.smtp.auth", emailProperties.getMailSmtpAuth());
					properties.setProperty("mail.smtp.starttls.enable", emailProperties.getMailSmtpStarttlsEnable());
					properties.setProperty("mail.user", emailProperties.getMailUser());
					properties.setProperty("mail.password", emailProperties.getMailPassword());
					properties.setProperty("mail.smtp.sendpartial", emailProperties.getMailSmtpSendpartial());
					properties.setProperty("mail.debug", emailProperties.getMailDebug());

					emailSession = Session.getDefaultInstance(properties);
				}
			}
		}

		return emailSession;
	}

	@Override
	public boolean sendEmail(String fromEmailAddress, Map<Message.RecipientType, String[]> receipentEmailAddresses, String subject, String body) {
		try {
			LOG.info("Getting Email Session...");

			MimeMessage msg = new MimeMessage(getEmailSession());

			msg.setFrom(new InternetAddress(fromEmailAddress));
			LOG.info("Sender Email Address: " + fromEmailAddress);

			if (receipentEmailAddresses != null) {
				Set<Message.RecipientType> receipentTypes = receipentEmailAddresses.keySet();

				for (Message.RecipientType receipentType : receipentTypes) {
					String[] receipents = receipentEmailAddresses.get(receipentType);

					if (receipents != null) {
						for (String receipent : receipents) {
							msg.addRecipient(receipentType, new InternetAddress(receipent));
							LOG.info("Added Recipient (type: " + receipentType + "): " + receipent + "");
						}
					}
				}
			}

			msg.setSubject(subject);
			LOG.info("Email Subject: " + subject);

			msg.setText(body, "UTF-8", "html");
			LOG.info("Added Content");

			Transport tr = getEmailSession().getTransport(SMTP);

			LOG.info("Connecting to the smtp host...");

			tr.connect(emailProperties.getMailSmtpHost(), emailProperties.getMailUser(), emailProperties.getMailPassword());

			msg.saveChanges();

			LOG.info("Sending email...");
			tr.sendMessage(msg, msg.getAllRecipients());

			tr.close();

			LOG.info("Email Sent successfully.");
            return true;
		} catch (Exception e) {
			LOG.error("Error Sending email: ", e);
            return false;
		}
	}
}
