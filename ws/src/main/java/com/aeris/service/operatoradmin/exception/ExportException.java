package com.aeris.service.operatoradmin.exception;

public class ExportException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public ExportException(String s) {
		super(s);
	}

	public ExportException(String ex, Throwable t) {
		super(ex, t);
	}
}
