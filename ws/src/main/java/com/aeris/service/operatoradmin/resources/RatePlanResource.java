package com.aeris.service.operatoradmin.resources;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.http.HttpStatus;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.IRatePlanDAO;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.exception.AccountRatePlanMappingException;
import com.aeris.service.operatoradmin.exception.BadStartDateException;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.ExportException;
import com.aeris.service.operatoradmin.exception.RatePlanAlreadyAssignedException;
import com.aeris.service.operatoradmin.exception.RatePlanException;
import com.aeris.service.operatoradmin.exception.RatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.SubRatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.ProvisionTriggerSettings;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanPaymentType;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.RatePlanTier;
import com.aeris.service.operatoradmin.model.RatePlanType;
import com.aeris.service.operatoradmin.model.SuspendTriggerSettings;
import com.aeris.service.operatoradmin.model.ZoneRatePlan;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateApiKeyRequest;
import com.aeris.service.operatoradmin.payload.CreateRatePlanRequest;
import com.aeris.service.operatoradmin.payload.UpdateAccountRequest;
import com.aeris.service.operatoradmin.payload.UpdateRatePlanRequest;
import com.aeris.service.operatoradmin.payload.UpdateRatePlanStatusRequest;
import com.aeris.service.operatoradmin.utils.NonTieredRatePlanExporter;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.aeris.service.operatoradmin.utils.TieredRatePlanExporter;

/**
 * Rest Resource for Rate Plan management. Includes methods for
 * 
 * <ul>
 * <li>
 * Create a new Rate Plan
 * </li>
 * <li>
 * Get Rate Plans assigned to an account
 * </li>
 * <li>
 * Assign Rate Plan to an account
 * </li>
 * <li>
 * Expire Rate Plan assigned to an account
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class RatePlanResource {
	private static final Logger LOG = LoggerFactory.getLogger(RatePlanResource.class);

	@Inject
	private IRatePlanDAO ratePlanDAO;
	
	@Inject
	private NonTieredRatePlanExporter nontieredRatePlanExporter;
	
	@Inject
	private TieredRatePlanExporter tieredRatePlanExporter;
	
	@Inject
	private OAUtils oaUtils;
    
    @Inject
    private IProductEnum productEnum;
		
	/**
	 * <p>
	 * Fetches List of Account details per operator.
	 * Allows to filter the Accounts based of status or list of specific account ids.
	 * </p>
	 * 
	 * @param operatorId The Operator Id
	 * @param status The Status
	 * @param start The start index for pagination
	 * @param count The record count for pagination
	 * @param accountIds The list of specific account ids to be fetched 
	 * @param extendAttributes true if the api sends extended account information to the client
	 * 
	 * @return The List of Accounts
	 */
	@GET
	@Path("/rateplans")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllRatePlans(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@QueryParam("status") @DefaultValue("all") final String status, @QueryParam("start") final String start,
			@QueryParam("count") final String count,
			@QueryParam("productIds") @Pattern(regexp = "(\\d+)(,\\s*\\d+)*", message = "{productIds.pattern}") final String productIds,
			@QueryParam("accessType") final String accessType,
			@QueryParam("extendAttributes") final boolean extendAttributes) {
		LOG.info("Received request to fetch all Accounts for operatorId: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		List<RatePlan> ratePlans = null;

		try {
			if (paginated) {
				LOG.info("Returning a max record size of " + count + " rate plans starting from " + start + " for operatorId: " + operatorId);
			} else {
				LOG.info("Query is not paginated, returning all available rate plans for operatorId: " + operatorId);
			}

			// Parse Product Ids
			int[] productIdArr = productIdsToIntArray(productIds);

			// Fetch orders with any status
			if (!RatePlanStatus.isValid(status)) {
				// Fetch from database
				ratePlans = ratePlanDAO.getAllRatePlans(Integer.parseInt(operatorId), start, count, productIdArr, RatePlanAccessType.fromName(accessType));
			}
			// Fetch orders with specific status like Open, In Progress,
			// Completed etc.
			else {
				// Fetch from database
				ratePlans = ratePlanDAO.getAllRatePlans(Integer.parseInt(operatorId), start, count, productIdArr, RatePlanAccessType.fromName(accessType), 
						RatePlanStatus.fromName(status));
			}

			if (ratePlans != null) {
				LOG.info("fetched rate plans successfully, no of rate plans: " + ratePlans.size());

				ObjectMapper mapper = new ObjectMapper();
				String ratePlansJson = mapper.writeValueAsString(ratePlans);

				builder.entity(ratePlansJson);
			} else {
				LOG.info("No rate plans for operator : " + operatorId);
				return Response.status(Status.NOT_FOUND).entity("No rate plans found for the operator").build();
			}
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getAllRatePlans Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting rate plans").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllRatePlans: " + response);
		return response;
	}
	
	/**
	 * <p>
	 * Fetches List of Account details per operator.
	 * Allows to filter the Accounts based of status or list of specific account ids.
	 * </p>
	 * 
	 * @param operatorId The Operator Id
	 * @param status The Status
	 * @param start The start index for pagination
	 * @param count The record count for pagination
	 * @param accountIds The list of specific account ids to be fetched 
	 * @param extendAttributes true if the api sends extended account information to the client
	 * 
	 * @return The List of Accounts
	 */
	@GET
	@Path("/rateplans/{ratePlanId}/subrateplans")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getSubRatePlans(@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("ratePlanId") @Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") final String tieredRatePlanId) {
		LOG.info("Received request to fetch sub rate plans for tieredRatePlanId: " + tieredRatePlanId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		List<RatePlan> ratePlans = null;
		List<RatePlan> unattachedRatePlansForAccount = null;
		try {
			ratePlans = ratePlanDAO.getSubRatePlans(Integer.parseInt(operatorId), Long.parseLong(tieredRatePlanId));
			unattachedRatePlansForAccount = ratePlanDAO.getUnattachedSubRatePlans(Integer.parseInt(operatorId), Long.parseLong(tieredRatePlanId));

			if (ratePlans != null) {
				LOG.info("fetched sub rate plans successfully, no of sub rate plans: " + ratePlans.size());

				if(unattachedRatePlansForAccount != null){
					ratePlans.addAll(unattachedRatePlansForAccount);
				}

				ObjectMapper mapper = new ObjectMapper();
				String ratePlansJson = mapper.writeValueAsString(ratePlans);

				builder.entity(ratePlansJson);
			} else if(unattachedRatePlansForAccount != null){
				ratePlans = unattachedRatePlansForAccount;
			} else {
				LOG.info("No sub rate plans for operator : " + operatorId);
				return Response.status(Status.NOT_FOUND).entity("No sub rate plans found for the operator").build();
			}
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getSubRatePlans Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting sub rate plans").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getSubRatePlans: " + response);
		return response;
	}
	
	/**
	 * Fetch a specific account identified by account id
	 * 
	 * @param operatorId the operator id
	 * @param accountId the account id
	 * 
	 * @return The Account details
	 */
	@GET
	@Path("/rateplans/{ratePlanId}") 
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	@Produces(MediaType.APPLICATION_JSON+";qs=1")
	public Response getRatePlan(
			@Context HttpServletRequest httpServletRequest, 
			@Context HttpServletResponse httpServletResponse,
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("ratePlanId") @Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") final String ratePlanId) {				
		LOG.info("Received request to fetch rate plan for ratePlanId: " + ratePlanId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			// Fetch from database
			RatePlan ratePlan = ratePlanDAO.getRatePlan(Integer.parseInt(operatorId), Integer.parseInt(ratePlanId));

			if (ratePlan != null) {
				LOG.info("fetched ratePlan successfully for ratePlanId: " + ratePlanId);

				ObjectMapper mapper = new ObjectMapper();
				String accountJson = mapper.writeValueAsString(ratePlan);

				builder.entity(accountJson);
			} else {
				LOG.info("No ratePlan found for ratePlanId: " + ratePlanId);
				return Response.status(Status.NOT_FOUND).entity("No ratePlan found for ratePlanId: " + ratePlanId).build();
			}
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getRatePlan Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting ratePlan").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getRatePlan: " + response);
		return response;
	}
	
	@GET
	@Path("/rateplans/{ratePlanId}")
	@Produces(MediaType.TEXT_PLAIN+";qs=.5")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response exportRatePlan(
			@Context HttpServletRequest httpServletRequest, 
			@Context HttpServletResponse httpServletResponse,
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("ratePlanId") @Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") final String ratePlanId) {
		LOG.info("Received request to export rate plan for ratePlanId: " + ratePlanId);

		ResponseBuilder builder = Response.ok().type(MediaType.TEXT_PLAIN);
		byte[] bytes = null; 
				
		try {
			String  userAgent  =   httpServletRequest.getHeader("User-Agent");

	        String newline = "\n";
	        LOG.info("User Agent for the request is {}", userAgent);
	        //=================OS=======================
	         if (userAgent.toLowerCase().indexOf("windows") >= 0 ) {
	        	newline = "\r\n"; 
	         }
	         
	         
			// Fetch from database
			RatePlan ratePlan = ratePlanDAO.getRatePlan(Integer.parseInt(operatorId), Integer.parseInt(ratePlanId));
			
			if(ratePlan != null)
			{
				if(ratePlan.getRatePlanType() == RatePlanType.TIERED)
				{
					// Export tiered rate plan to byte stream
					bytes = tieredRatePlanExporter.exportRatePlanToTextFile(ratePlan, newline);
				}
				else
				{
					// Export nontiered rate plan to byte stream
					bytes = nontieredRatePlanExporter.exportRatePlanToTextFile(ratePlan, newline);
				}			
			}
			
			if (bytes != null) {
				LOG.info("fetched rate plan successfully for ratePlanId: " + ratePlanId+", writing to response stream as attachment");
				
				String fileName = oaUtils.generateExportedFileName(ratePlan);
				
				// Pop up as an attachment to the user
				builder.header("content-disposition",
						"attachment; filename = "+fileName);
				
				// Write Content
				builder.entity(new ByteArrayInputStream(bytes));
			} else {
				LOG.info("No ratePlan found for ratePlanId: " + ratePlanId);
				return Response.status(Status.NOT_FOUND).entity("No ratePlan found for ratePlanId: " + ratePlanId).build();
			} 
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (ExportException e) {
			LOG.error("ExportException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getRatePlan Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting ratePlan").build();
		}

		Response response = builder.build();

		LOG.info("Sent response from exportRatePlan: " + response);
		return response;
	}

	/**
	 * Create a new account for an operator based on the information 
	 * specified in the {@link CreateApiKeyRequest} by the client
	 * 
	 * The request accepts the user credentials, account name, 
	 * contacts information, billing details and list of products enrolled for this account
	 * 
	 * @param operatorId the operator id
	 * @param request the create account request sent by the client
	 * 
	 * @return the newly created account information
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/rateplans")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response createNewRatePlan(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@Valid CreateRatePlanRequest request) {

		LOG.info("Received request to create rate plan under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		int productId = request.getProductId();	
		String ratePlanLabel = request.getRatePlanLabel();		

		RatePlanType ratePlanType= request.getRatePlanType();	
		RatePlanPaymentType paymentType = request.getPaymentType();		
		RatePlanAccessType accessType = request.getAccessType();		
		RatePlanPeriodType periodType = request.getPeriodType();	
		int accessFeeMonths = request.getAccessFeeMonths();
		boolean expireIncluded = request.isExpireIncluded();
		int includedPeriodMonths = request.getIncludedPeriodMonths();
		int devicePoolingPolicyId = request.getDevicePoolingPolicyId();
		int packetDataRoundingPolicyId = request.getPacketDataRoundingPolicyId();
		RatePlanStatus status = request.getStatus();
		boolean overrideRoamingIncluded = request.isOverrideRoamingIncluded();
		
		int tierCriteria = request.getTierCriteria();		
		int homeZoneId = request.getHomeZoneId();		
		String description = request.getDescription();				
		String specialNotes = request.getSpecialNotes();		
		int carrierRatePlanId = request.getCarrierRatePlanId();	
		long accountId = request.getAccountId();		
		
		Date startDate = request.getStartDate();		
		Date endDate = request.getEndDate();
		String currencyName = request.getCurrencyName();
		RatePlanFeeDetails ratePlanFeeDetails = request.getRatePlanFeeDetails();
		ProvisionTriggerSettings provisionTriggerSettings = request.getProvisionTriggerSettings();
		SuspendTriggerSettings suspendTriggerSettings = request.getSuspendTriggerSettings();
		List<ZoneRatePlan> zoneRatePlanSettings = request.getZoneRatePlanSettings();
		List<RatePlanTier> ratePlanTiers = request.getRatePlanTiers();
		String userId = request.getUserId();
		boolean waterfallEnabled = request.isWaterfallEnabled();
                boolean isWholesaleRatePlan = request.getIsWholesaleRatePlan();
        
        boolean roamingIncluded = request.isRoamingIncluded();

        String technology = request.getTechnology();
        if (technology == null) {
            technology = getProductTechnology(productId);
        }
        
		Date requestedDate = new Date();

		try {			
			LOG.info("Validating request...");
			
			Response vResponse = validateRequest(builder, ratePlanType, accessType, tierCriteria, accountId, ratePlanTiers);		
			
			if(vResponse.getStatus() == HttpStatus.SC_OK)
			{
				LOG.info("Calling Rate Plan DAO");
				
				RatePlan ratePlan = ratePlanDAO.createNewRatePlan(Integer.parseInt(operatorId), productId, ratePlanLabel, ratePlanType, paymentType,
						accessType, periodType, accessFeeMonths, expireIncluded, status, overrideRoamingIncluded, tierCriteria, homeZoneId, description, specialNotes,
						carrierRatePlanId, accountId, startDate, endDate, currencyName, includedPeriodMonths, devicePoolingPolicyId,
						packetDataRoundingPolicyId, ratePlanFeeDetails, provisionTriggerSettings, suspendTriggerSettings, zoneRatePlanSettings, ratePlanTiers,
						requestedDate, userId, request.getProRateCancellationFee(), request.getContractTerm(),
						request.getCancellationFeeReductionInterval(), request.getAutoCancelAtTermEnd(), request.getAccountForDaysInProvisionState(),
						 request.getAccountForDaysInSuspendState(), request.getRatePlanOverageBucket(), roamingIncluded, request.getZoneSetId(), technology, waterfallEnabled,request.getWholesaleRateplanId(),
                                                 request.getIsWholesaleRatePlan());
				
				ObjectMapper mapper = new ObjectMapper();
				String accountJson = mapper.writeValueAsString(ratePlan);
				builder.entity(accountJson);
	
				LOG.info("Create Rate Plan Response: " + accountJson);
			}
		} catch (SubRatePlanNotFoundException e) {
			LOG.error("SubRatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (DuplicateRatePlanException e) {
			LOG.error("DuplicateRatePlanException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (DuplicateZoneException e) {
			LOG.error("DuplicateZoneException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("createNewAccount Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while creating account").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from createNewAccount: " + response);
		return response;
	}

	private Response validateRequest(ResponseBuilder builder, RatePlanType ratePlanType, RatePlanAccessType accessType, int tierCriteria,
			long accountId, List<RatePlanTier> ratePlanTiers) {
		ResponseBuilder responseBuilder = builder;		
		
		// If rate plan is tiered, ensure tierCriteria is selected before setting the tiers information
		if(ratePlanType == RatePlanType.TIERED)
		{
			// Rate Plan Tier Information is set
			if(ratePlanTiers != null && !ratePlanTiers.isEmpty())
			{
				// Tier Criteria is not selected, throw bad request
				if(tierCriteria <= 0)
				{
					responseBuilder.status(Status.BAD_REQUEST).entity("The tierCriteria is required to set tiers in the rate plan"); 
				}
			}			
		}
		
		// If the rate plan is exclusive, accountId is mandatory
		if(accessType == RatePlanAccessType.EXCLUSIVE)
		{
			if(accountId <= 0)
			{
				responseBuilder.status(Status.BAD_REQUEST).entity("The accountId is required to create an exclusive rate plan"); 
			}
		}
		
		return responseBuilder.build();
	}

	/**
	 * Update an exisiting account for an operator based on the information 
	 * specified in the {@link UpdateAccountRequest} by the client
	 * 
	 * The request accepts the account name, contacts information, billing details 
	 * and list of products enrolled for this account
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param request The update account requested by the user client
	 * 
	 * @return The Updated Account serialized back to the client
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/rateplans/{ratePlanId}")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateRatePlan(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("ratePlanId") @Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") final String ratePlanId,
			@Valid UpdateRatePlanRequest request) {

		LOG.info("Received request to update rate plan with ratePlanId: " + ratePlanId + " under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		int productId = request.getProductId();	
		String ratePlanLabel = request.getRatePlanLabel();		

		RatePlanType ratePlanType= request.getRatePlanType();	
		RatePlanPaymentType paymentType = request.getPaymentType();		
		RatePlanAccessType accessType = request.getAccessType();		
		RatePlanPeriodType periodType = request.getPeriodType();
		int accessFeeMonths = request.getAccessFeeMonths();
		boolean expireIncluded = request.isExpireIncluded();
		int includedPeriodMonths = request.getIncludedPeriodMonths();
		int devicePoolingPolicyId = request.getDevicePoolingPolicyId();
		int packetDataRoundingPolicyId = request.getPacketDataRoundingPolicyId();
		RatePlanStatus status = request.getStatus();
		boolean overrideRoamingIncluded = request.isOverrideRoamingIncluded();
		
		int tierCriteria = request.getTierCriteria();		
		int homeZoneId = request.getHomeZoneId();		
		String description = request.getDescription();				
		String specialNotes = request.getSpecialNotes();		
		int carrierRatePlanId = request.getCarrierRatePlanId();	
		long accountId = request.getAccountId();		
		
		Date startDate = request.getStartDate();		
		Date endDate = request.getEndDate(); 
		String currencyName = request.getCurrencyName();
		RatePlanFeeDetails ratePlanFeeDetails = request.getRatePlanFeeDetails();
		ProvisionTriggerSettings provisionTriggerSettings = request.getProvisionTriggerSettings();
		SuspendTriggerSettings suspendTriggerSettings = request.getSuspendTriggerSettings();
		List<ZoneRatePlan> zoneRatePlanSettings = request.getZoneRatePlanSettings();
		List<RatePlanTier> ratePlanTiers = request.getRatePlanTiers();
		String userId = request.getUserId();
		boolean waterfallEnabled = request.isWaterfallEnabled();
		
        boolean roamingIncluded = request.isRoamingIncluded();
        
        String technology = request.getTechnology();
        if (technology == null) {
            technology = getProductTechnology(productId);
        }
        
		Date requestedDate = new Date();

		try {
			LOG.info("Validating request...");
			
			Response vResponse = validateRequest(builder, ratePlanType, accessType, tierCriteria, accountId, ratePlanTiers);		
			
			if(vResponse.getStatus() == HttpStatus.SC_OK)
			{
				LOG.info("Calling ratePlanDAO.updateRatePlan()");

				RatePlan ratePlan = ratePlanDAO.updateRatePlan(Integer.parseInt(operatorId), Integer.parseInt(ratePlanId), productId, ratePlanLabel,
						ratePlanType, paymentType, accessType, periodType, accessFeeMonths, expireIncluded, status, overrideRoamingIncluded, tierCriteria,
						homeZoneId, description, specialNotes, carrierRatePlanId, accountId, startDate, endDate, currencyName, includedPeriodMonths,
						devicePoolingPolicyId, packetDataRoundingPolicyId, ratePlanFeeDetails, provisionTriggerSettings, suspendTriggerSettings,
						zoneRatePlanSettings, ratePlanTiers, requestedDate, userId, request.getProRateCancellationFee(), 
						request.getContractTerm(), request.getCancellationFeeReductionInterval(),
						request.getAutoCancelAtTermEnd(), request.getAccountForDaysInProvisionState(),
						request.getAccountForDaysInSuspendState(), request.getRatePlanOverageBucket(), roamingIncluded, request.getZoneSetId(), technology, waterfallEnabled, request.getWholesaleRateplanId(), request.getIsWholesaleRatePlan());

				ObjectMapper mapper = new ObjectMapper();

				String ratePlanJson = mapper.writeValueAsString(ratePlan);
				builder.entity(ratePlanJson);

				LOG.info("Update Rate Plan Response: " + ratePlanJson);
			}
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (SubRatePlanNotFoundException e) {
			LOG.error("SubRatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (ZoneNotFoundException e) {
			LOG.error("ZoneNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (BadStartDateException e) {
			LOG.error("BadStartDateException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (RatePlanAlreadyAssignedException e) {
			LOG.error("RatePlanAlreadyAssignedException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (DuplicateRatePlanException e) {
			LOG.error("DuplicateRatePlanException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (DuplicateZoneException e) {
			LOG.error("DuplicateZoneException occured :", e);
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateRatePlan Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while updating rate plan").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateRatePlan: " + response);
		return response;
	}
	
	/**
	 * Update an exisiting account for an operator based on the information 
	 * specified in the {@link UpdateAccountRequest} by the client
	 * 
	 * The request accepts the account name, contacts information, billing details 
	 * and list of products enrolled for this account
	 * 
	 * @param operatorId The operator id
	 * @param accountId The account id
	 * @param request The update account requested by the user client
	 * 
	 * @return The Updated Account serialized back to the client
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/rateplans/{ratePlanId}/status")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateRatePlanStatus(
			@PathParam("operatorId") @ValidOperator final String operatorId,
			@PathParam("ratePlanId") @Pattern(regexp = "[0-9]+", message = "{ratePlanId.number}") final String ratePlanId,
			@Valid UpdateRatePlanStatusRequest request) {

		LOG.info("Received request to update rate plan status with ratePlanId: " + ratePlanId + " under operator: " + operatorId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		RatePlanStatus status = request.getStatus();
		String specialComments = request.getSpecialNotes();
		String userId = request.getUserId();
		Date requestedDate = new Date();
        Date newEndDate = request.getNewEndDate();

		try {
			LOG.info("Calling ratePlanDAO.updateRatePlanStatus()");
			boolean success = ratePlanDAO.updateRatePlanStatus(Integer.parseInt(operatorId), Long.parseLong(ratePlanId), status, specialComments, requestedDate, userId, newEndDate);
			ObjectMapper mapper = new ObjectMapper();

			String accountStatusJson = mapper.writeValueAsString(success);
			builder.entity(accountStatusJson);

			LOG.info("Update Rate Plan status Response: " + accountStatusJson);
		} catch (AccountRatePlanMappingException e) {
			LOG.error("AccountRatePlanMappingException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateRatePlan Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while updating rate plan").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from updateRatePlanStatus: " + response);
		return response;
	}

	/**
	 * Converts comma seperated account ids to list of account ids 
	 * 
	 * @param accountIds the comma seperated account ids
	 * 
	 * @return list of account ids
	 */
	private int[] productIdsToIntArray(final String productIds) {
		int[] productIdIntArr = null;
		
		if(productIds != null)
		{		
			String[] productIdArr = productIds.split(",\\s*");
			productIdIntArr = new int[productIdArr.length];
			int i = 0;
			
			for (String productId : productIdArr) {
				productIdIntArr[i++] = Integer.parseInt(productId);
			}
		}

		return productIdIntArr;
	}
    
    private String getProductTechnology(int productId) {
        Product product = productEnum.getProductById(productId);
        return product.getTechnology();
    }
    
        @GET
	@Path("/account/{accountId}/rateplans/wholesale")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getWholesaleRatePlans(
			@PathParam("operatorId") @ValidOperator final String operatorId,			
			@QueryParam("productIds") @Pattern(regexp = "(\\d+)(,\\s*\\d+)*", message = "{productIds.pattern}") final String productIds,
                        @PathParam("accountId") final String accountId) {
		LOG.info("Received request to fetch all wholesale accounts for accountId: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		List<RatePlan> ratePlans = null;

		try {
			// Parse Product Ids
			int[] productIdArr = productIdsToIntArray(productIds);
                        
			ratePlans = ratePlanDAO.getWholesaleRatePlans(Long.parseLong(accountId), productIdArr);

			if (ratePlans != null) {
				LOG.info("fetched rate plans successfully, no of rate plans: " + ratePlans.size());

				ObjectMapper mapper = new ObjectMapper();
				String ratePlansJson = mapper.writeValueAsString(ratePlans);

				builder.entity(ratePlansJson);
			} else {
				LOG.info("No rate plans for operator : " + operatorId);
				return Response.status(Status.NOT_FOUND).entity("No rate plans found for the operator").build();
			}
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getAllRatePlans Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting rate plans").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllRatePlans: " + response);
		return response;
	}
        
        @GET
	@Path("/account/{accountId}/rateplans/retailrateplans")
	@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getResellerRetailRatePlans(
			@PathParam("operatorId") @ValidOperator final String operatorId,
                        @PathParam("accountId") final String accountId,
                        @QueryParam("productIds") @Pattern(regexp = "(\\d+)(,\\s*\\d+)*", message = "{productIds.pattern}") final String productIds) {
		LOG.info("Received request to fetch all retail rate plans for wholesale account: " + accountId);

		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		List<RatePlan> ratePlans = null;

		try {   
                        // Parse Product Ids
			int[] productIdArr = productIdsToIntArray(productIds);
			ratePlans = ratePlanDAO.getResellerRetailRatePlans(Long.parseLong(accountId), productIdArr);

			if (ratePlans != null) {
				LOG.info("fetched rate plans successfully, no of rate plans: " + ratePlans.size());

				ObjectMapper mapper = new ObjectMapper();
				String ratePlansJson = mapper.writeValueAsString(ratePlans);

				builder.entity(ratePlansJson);
			} else {
				LOG.info("No rate plans for account : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No rate plans found for the account").build();
			}
		} catch (RatePlanException e) {
			LOG.error("RatePlanException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("getAllRatePlans Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Unable to service your request. Fatal error while getting rate plans").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllRatePlans: " + response);
		return response;
	}
}
