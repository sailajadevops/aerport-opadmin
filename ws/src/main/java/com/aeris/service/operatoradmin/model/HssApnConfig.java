package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HssApnConfig implements Serializable {

    private String id;
    private String apn;
    private String apnOiReplacement;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApn() {
        return apn;
    }

    public void setApn(String apn) {
        this.apn = apn;
    }

    public String getApnOiReplacement() {
        return apnOiReplacement;
    }

    public void setApnOiReplacement(String apnOiReplacement) {
        this.apnOiReplacement = apnOiReplacement;
    }
}