package com.aeris.service.operatoradmin.enums;

import com.aeris.service.operatoradmin.model.ApiKeyPermission;

public interface IApiKeyPermissionEnum  extends IBaseEnum{
	ApiKeyPermission getPermissionByValue(String permissionValue);
	ApiKeyPermission getPermissionById(int permissionId);
}
