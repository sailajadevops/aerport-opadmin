package com.aeris.service.operatoradmin.exception;

public class DistributorNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DistributorNotFoundException(String s) {
		super(s);
	}

	public DistributorNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
