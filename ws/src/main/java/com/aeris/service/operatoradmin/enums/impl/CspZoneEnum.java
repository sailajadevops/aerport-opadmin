package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.ICspZoneEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.CspZone;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * Country Cache used for lookup purposes
 * 
 * @author Srinivas Puranam
 */
@Singleton
public class CspZoneEnum implements ICspZoneEnum, IEventListener  {
	private Logger LOG = LoggerFactory.getLogger(CspZoneEnum.class);
	
	static final String SQL_GET_CSP_ZONES = "select zone_id, zone_name from csp_zone order by zone_id";
	
	private Cache<String, CspZone> cache;
	private Cache<String, CspZone> cacheById;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "CspZoneCache/name") Cache<String, CspZone> cache, 
			@Hazelcast(cache = "CspZoneCache/id") Cache<String, CspZone> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	 
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_CSP_ZONES);
			
			while (rs.next()) {
				CspZone cspZone = new CspZone();

				cspZone.setZoneId(rs.getInt("zone_id"));
				cspZone.setZoneName(rs.getString("zone_name"));
				
				cache.put(cspZone.getZoneName(), cspZone);
				cacheById.put(String.valueOf(cspZone.getZoneId()), cspZone);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}
	

	@Override
	public CspZone getCspZoneByName(String name) {
		return cache.get(name);
	}

	@Override
	public CspZone getCspZoneById(int id) {
		return cacheById.get(String.valueOf(id));
	}

	@Override
	public List<CspZone> getAllCspZones() {
		return cacheById.getValues();
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}

	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
