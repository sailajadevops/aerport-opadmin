package com.aeris.service.operatoradmin.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.impl.AccountDAO;

public class DBUtils {
	private static Logger LOG = LoggerFactory.getLogger(AccountDAO.class);
	
	public static void cleanup(Connection conn, Statement st, ResultSet rs) {
		if(conn != null)
		{
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				 LOG.error("DB Connection error", e);
			}
		}
		
		DBConnectionManager.getInstance().cleanup(conn, st, rs);
	}

	public static void cleanup(Connection conn, Statement st) {
		if(conn != null)
		{
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				 LOG.error("DB Connection error", e);
			}
		}

		DBConnectionManager.getInstance().cleanup(conn, st);
	}

	public static void cleanup(Connection conn) {
		if(conn != null)
		{
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				 LOG.error("DB Connection error", e);
			}
		}

		DBConnectionManager.getInstance().cleanup(conn);
	}
}
