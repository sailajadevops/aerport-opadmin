package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum OrderStatus implements Serializable {
	OPEN(1), IN_PROGRESS(2), COMPLETED(3), SHIPPED(4), CANCELLED(0);
	int value;

	OrderStatus(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(String value) {
		return fromName(value) != null;
	}

	public static OrderStatus fromValue(int value) {
		OrderStatus[] statuses = values();

		for (OrderStatus accountStatus : statuses) {
			if (accountStatus.getValue() == value) {
				return accountStatus;
			}
		}

		return null;
	}

	public static OrderStatus fromName(String value) {
		OrderStatus[] statuses = values();

		for (OrderStatus accountStatus : statuses) {
			if (accountStatus.name().equalsIgnoreCase(value)) {
				return accountStatus;
			}
		}

		return null;
	}
}
