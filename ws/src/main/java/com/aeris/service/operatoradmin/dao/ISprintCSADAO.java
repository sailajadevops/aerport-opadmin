package com.aeris.service.operatoradmin.dao;

import java.util.Date;

import com.aeris.service.operatoradmin.model.SprintCSA;

/**
 * DAO Interface Definition for the sprint csa management.
 *  
 * @author Srinivas Puranam
 */
public interface ISprintCSADAO {
	
	
	/**
	 * Fetches the Sprint CSA Info from the db for a carrier
	 * by firing a SQL query
	 * 
	 * @param carrierId the carrier identifier
	 * @return the csa information
	 */
	SprintCSA getSprintCSA(String carrierId);
	
	
	/**
	 * Switches the CSA values for a carrier in the db by 
	 * using SQL update
	 * 
	 * @param carrierId the carrier identifier
	 * @param lastModifiedDate the last modified date
	 * @param lastModifiedBy the user
	 * @return true if the op is success and sql update count is more than 0, otherwise false.
	 */
	boolean switchCSA(String carrierId, Date lastModifiedDate, String lastModifiedBy);
	
	
	/**
	 * Update the secondary CSA value for a carrier in the db by 
	 * using SQL update
	 * 
	 * @param carrierId the carrier identifier
	 * @param newCSA the new csa value
	 * @param lastModifiedDate the last modified date
	 * @param lastModifiedBy the user
	 * @return true if the op is success and sql update count is more than 0, otherwise false.
	 */
	boolean updateSecondaryCSA(String carrierId, String newCSA, Date lastModifiedDate, String lastModifiedBy);
}
