package com.aeris.service.operatoradmin.dao.storedproc;

import java.sql.Connection;

import com.aeris.service.operatoradmin.exception.StoredProcedureException;

public interface IStoredProcedure<T> {
	public abstract T execute(Object... input) throws StoredProcedureException;
	public abstract T execute(Connection conn, Object... input) throws StoredProcedureException;
}