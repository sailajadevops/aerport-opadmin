package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IResourceEnum;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.Resource;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.google.inject.Singleton;

/**
 * Resource Definition Cache used for lookup purposes 
 * 
 * @author SP00125222
 */
@Singleton
public class ResourceEnum implements IResourceEnum {
	private Logger LOG = LoggerFactory.getLogger(ResourceEnum.class);
	static final String SQL_GET_RESOURCES = "select resource_id, resource_type, resource_path from ws_resource";
	
	private Cache<String, Resource> cache;
	private Cache<String, Resource> cacheById;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "ResourceCache/name") Cache<String, Resource> cache, 
			@Hazelcast(cache = "ResourceCache/id") Cache<String, Resource> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;
		loadResources();
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			// Create Prepared Statement
			stmt = conn.createStatement();
			rs = stmt.executeQuery(SQL_GET_RESOURCES);
			
			while (rs.next()) {
				Resource resource = new Resource();
				
				resource.setResourceId(rs.getInt("resource_id"));
				resource.setResourcePath(rs.getString("resource_path"));
				resource.setResourceType(rs.getString("resource_type"));
				
				cache.put(resource.getResourcePath(), resource);
				cacheById.put(String.valueOf(resource.getResourceId()), resource);
			}
			
			LOG.info("resources loaded successfully");
		} catch (SQLException e) {
			LOG.error("Exception during loadResources", e);
			throw new GenericServiceException("Unable to loadResources", e);
		} finally {
			DBUtils.cleanup(conn, stmt, rs);
		}
	}
	
	/**
	 * Fetch the Resource Definition matching the path
	 * 
	 * @param resourcePath the path of the resource
	 * @return the Resource Definition
	 */
	@Override
	public Resource getResourceByPath(String resourcePath)
	{
		return cache.get(resourcePath);
	}
	
	/**
	 * Fetch the Resource Definition matching the id
	 * 
	 * @param resourceId the id of the resource
	 * @return the Resource Definition
	 */
	@Override
	public Resource getResourceById(int resourceId)
	{
		return cacheById.get(String.valueOf(resourceId));
	}

	/**
	 * Fetch all the Resource Definitions
	 * 
	 * @return the Resource Definitions
	 */
	@Override
	public List<Resource> getResources() {
		return new ArrayList<Resource>(cache.getValues());
	}
		
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}
}
