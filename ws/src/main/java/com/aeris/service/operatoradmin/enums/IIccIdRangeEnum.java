package com.aeris.service.operatoradmin.enums;

import java.util.List;

import com.aeris.service.operatoradmin.model.IccIdRange;

public interface IIccIdRangeEnum  extends IBaseEnum{
	IccIdRange getIccIdRange(String range);
	List<IccIdRange> getAllIccIdRanges();
}
