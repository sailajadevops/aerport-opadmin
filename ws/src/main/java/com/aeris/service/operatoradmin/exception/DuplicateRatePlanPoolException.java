package com.aeris.service.operatoradmin.exception;

public class DuplicateRatePlanPoolException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DuplicateRatePlanPoolException(String s) {
		super(s);
	}

	public DuplicateRatePlanPoolException(String ex, Throwable t) {
		super(ex, t);
	}
}
