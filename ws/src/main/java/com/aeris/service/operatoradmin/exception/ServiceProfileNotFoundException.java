package com.aeris.service.operatoradmin.exception;

public class ServiceProfileNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public ServiceProfileNotFoundException(String s) {
		super(s);
	}

	public ServiceProfileNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
