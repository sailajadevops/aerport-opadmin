package com.aeris.service.operatoradmin.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class SalesAgentException extends WebApplicationException {
	private static final long serialVersionUID = 7538422852362471686L;

	public SalesAgentException(String s) {
		super(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
	             .entity(s).type(MediaType.TEXT_PLAIN).build());
	}}
