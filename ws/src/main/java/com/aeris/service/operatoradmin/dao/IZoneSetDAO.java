package com.aeris.service.operatoradmin.dao;

import java.util.Date;
import java.util.List;

import com.aeris.service.operatoradmin.exception.DuplicateZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetException;
import com.aeris.service.operatoradmin.exception.ZoneSetNotFoundException;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZoneSet;

public interface IZoneSetDAO {
	List<ZoneSet> getAllZoneSets(int operatorId) throws ZoneSetException;
	List<ZoneSet> getZoneSets(int operatorId, int productId) throws ZoneSetException;
	List<ZoneSet> getZoneSets(int operatorId, int productId, int accountId) throws ZoneSetException;
	List<ZoneSet> getZoneSets(int operatorId, List<String> productIdList, List<String> accountIdList) throws ZoneSetException;
	ZoneSet getZoneSet(int operatorId, int zoneSetId) throws ZoneSetException,ZoneSetNotFoundException;
	ZoneSet createNewZoneSet(int operatorId, int productId, int accountId, String zoneSetName, Date requestedDate, String requestedUser, List<Zone> zones) throws DuplicateZoneSetException, ZoneSetException;
	ZoneSet createNewZoneSet(int operatorId, int productId, int accountId, String zoneSetName, Date requestedDate, String requestedUser, List<Zone> zones, int RESId) throws DuplicateZoneSetException, ZoneSetException; 
	ZoneSet updateZoneSet(int zoneSetId, int operatorId, int productId, int accountId, String zoneSetName, Date requestedDate, String requestedUser) throws DuplicateZoneSetException,ZoneSetNotFoundException,ZoneSetException;
	boolean deleteZoneSet(int operatorId, int zoneSetId, Date requestedDate, String requestedUser) throws ZoneSetException, ZoneSetNotFoundException;
}
