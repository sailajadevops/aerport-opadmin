package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.sql.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aeris.service.operatoradmin.model.constraints.NotPast;

@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class AccountRatePlanPoolOverageBucket implements Serializable {

	/**
	 */
	private static final long serialVersionUID = 1291879228945913750L;

	private long accountId ;
	private String bucketSizeType;
	private double bucketSize; 
	private double bucketPrice;
	private int autoAdd;
	private int noAccessFeeDeviceCount; 
	@NotNull(message = "{overage.startDate.notnull}")
	private Date startDate;
	@NotNull(message = "{overage.endDate.notnull}")
	private Date endDate;
	private String repTimeStamp ;
	private Date bucketCreatedDate, bucketUpdatedDate ;
	@NotNull(message="{createdBy.notnull}")
	private String bucketCreatedBy;
	private String bucketUpdatedBy ;


	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}


	public long getAccountId() {
		return accountId;
	}


	public void setAccountId(long newAccountId) {
		this.accountId = newAccountId;
	}


	public String getBucketSizeType() {
		return bucketSizeType;
	}


	public void setBucketSizeType(String bucketSizeType) {
		this.bucketSizeType = bucketSizeType;
	}


	public double getBucketSize() {
		return bucketSize;
	}


	public void setBucketSize(double bucketSize) {
		this.bucketSize = bucketSize;
	}


	public double getBucketPrice() {
		return bucketPrice;
	}


	public void setBucketPrice(double bucketPrice) {
		this.bucketPrice = bucketPrice;
	}


	public int getAutoAdd() {
		return autoAdd;
	}


	public void setAutoAdd(int autoAdd) {
		this.autoAdd = autoAdd;
	}


	public int getNoAccessFeeDeviceCount() {
		return noAccessFeeDeviceCount;
	}


	public void setNoAccessFeeDeviceCount(int noAccessFeeDeviceCount) {
		this.noAccessFeeDeviceCount = noAccessFeeDeviceCount;
	}


	public String getRepTimeStamp() {
		return repTimeStamp;
	}


	public void setRepTimeStamp(String repTimeStamp) {
		this.repTimeStamp = repTimeStamp;
	}


	public Date getCreatedDate() {
		return bucketCreatedDate;
	}


	public void setCreatedDate(Date bucketCreatedDate) {
		this.bucketCreatedDate = bucketCreatedDate;
	}


	public Date getUpdatedDate() {
		return bucketUpdatedDate;
	}


	public void setUpdatedDate(Date bucketUpdatedDate) {
		this.bucketUpdatedDate = bucketUpdatedDate;
	}


	public String getCreatedBy() {
		return bucketCreatedBy;
	}


	public void setCreatedBy(String bucketCreatedBy) {
		this.bucketCreatedBy = bucketCreatedBy;
	}


	public String getUpdatedBy() {
		return bucketUpdatedBy;
	}


	public void setUpdatedBy(String bucketUpdatedBy) {
		this.bucketUpdatedBy = bucketUpdatedBy;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
