package com.aeris.service.operatoradmin.guice;

import java.io.IOException;
import java.util.List;

import com.aeris.service.common.cache.guice.CacheListener;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.util.PropertyConfigLoader;
import com.aeris.service.jdbc.guice.QueryListener;
import com.aeris.service.operatoradmin.ApplicationVersion;
import com.aeris.service.operatoradmin.auth.Authenticator;
import com.aeris.service.operatoradmin.client.AS4gClient;
import com.aeris.service.operatoradmin.client.AerCloudClient;
import com.aeris.service.operatoradmin.dao.IAS4gDAO;
import com.aeris.service.operatoradmin.dao.IAccountApplicationDAO;
import com.aeris.service.operatoradmin.dao.IAccountDAO;
import com.aeris.service.operatoradmin.dao.ICSPRatePlanDAO;
import com.aeris.service.operatoradmin.dao.ICommunicationPlanDAO;
import com.aeris.service.operatoradmin.dao.ICustomerSIMOrderDAO;
import com.aeris.service.operatoradmin.dao.IDistributorDAO;
import com.aeris.service.operatoradmin.dao.IGlobalSIMInventoryDAO;
import com.aeris.service.operatoradmin.dao.IOperatorConfigDAO;
import com.aeris.service.operatoradmin.dao.IOperatorProductDAO;
import com.aeris.service.operatoradmin.dao.IProductCarrierDAO;
import com.aeris.service.operatoradmin.dao.IRESDAO;
import com.aeris.service.operatoradmin.dao.IRatePlanDAO;
import com.aeris.service.operatoradmin.dao.IRatePlanPoolDAO;
import com.aeris.service.operatoradmin.dao.ISIMMgmtDAO;
import com.aeris.service.operatoradmin.dao.ISIMPackDAO;
import com.aeris.service.operatoradmin.dao.ISMPPConnectionDAO;
import com.aeris.service.operatoradmin.dao.ISalesAgentDAO;
import com.aeris.service.operatoradmin.dao.IServiceProfileDAO;
import com.aeris.service.operatoradmin.dao.ISprintCSADAO;
import com.aeris.service.operatoradmin.dao.ISupplierSIMOrderDAO;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.dao.IZoneSetDAO;
import com.aeris.service.operatoradmin.dao.impl.AS4gDAO;
import com.aeris.service.operatoradmin.dao.impl.AccountApplicationDAO;
import com.aeris.service.operatoradmin.dao.impl.AccountDAO;
import com.aeris.service.operatoradmin.dao.impl.CSPRatePlanDAO;
import com.aeris.service.operatoradmin.dao.impl.CommunicationPlanDAO;
import com.aeris.service.operatoradmin.dao.impl.CustomerSIMOrderDAO;
import com.aeris.service.operatoradmin.dao.impl.DistributorDAO;
import com.aeris.service.operatoradmin.dao.impl.GlobalSIMInventoryDAO;
import com.aeris.service.operatoradmin.dao.impl.OperatorConfigDAO;
import com.aeris.service.operatoradmin.dao.impl.OperatorProductDAO;
import com.aeris.service.operatoradmin.dao.impl.ProductCarrierDAO;
import com.aeris.service.operatoradmin.dao.impl.RESDAO;
import com.aeris.service.operatoradmin.dao.impl.RatePlanDAO;
import com.aeris.service.operatoradmin.dao.impl.RatePlanPoolDAO;
import com.aeris.service.operatoradmin.dao.impl.SIMMgmtDAO;
import com.aeris.service.operatoradmin.dao.impl.SIMPackDAO;
import com.aeris.service.operatoradmin.dao.impl.SMPPConnectionDAO;
import com.aeris.service.operatoradmin.dao.impl.SalesAgentDAO;
import com.aeris.service.operatoradmin.dao.impl.ServiceProfileDAO;
import com.aeris.service.operatoradmin.dao.impl.SprintCSADAO;
import com.aeris.service.operatoradmin.dao.impl.SupplierSIMOrderDAO;
import com.aeris.service.operatoradmin.dao.impl.ZoneDAO;
import com.aeris.service.operatoradmin.dao.impl.ZoneSetDAO;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.ResultStatus;
import com.aeris.service.operatoradmin.dao.storedproc.impl.AssignRatePlanToAccountStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.CreateCSPRatePlanCDMAStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.CreateCSPRatePlanGSMStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.ExpireAccountRatePlanStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetAccessPointsStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetCarriersByProductStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetChildAccountsStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetOperatorsStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetProductsStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetSIMFormatsStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetSMPPConnectionsStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetSMSFormatsStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetSuppliersStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetVerticalsStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.GetWarehousesStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.UpdateCSPRatePlanCDMAStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.impl.UpdateCSPRatePlanGSMStoredProcedure;
import com.aeris.service.operatoradmin.delegate.IEmailDelegate;
import com.aeris.service.operatoradmin.delegate.impl.EmailDelegate;
import com.aeris.service.operatoradmin.enums.IAccessPointNameEnum;
import com.aeris.service.operatoradmin.enums.IApiKeyEnum;
import com.aeris.service.operatoradmin.enums.IApiKeyPermissionEnum;
import com.aeris.service.operatoradmin.enums.IContractEnum;
import com.aeris.service.operatoradmin.enums.ICountryEnum;
import com.aeris.service.operatoradmin.enums.ICspZoneEnum;
import com.aeris.service.operatoradmin.enums.ICurrencyEnum;
import com.aeris.service.operatoradmin.enums.IIccIdPrefixEnum;
import com.aeris.service.operatoradmin.enums.IIccIdRangeEnum;
import com.aeris.service.operatoradmin.enums.IImsiRangeEnum;
import com.aeris.service.operatoradmin.enums.IOperatorEnum;
import com.aeris.service.operatoradmin.enums.IOperatorKeyEnum;
import com.aeris.service.operatoradmin.enums.IPacketRoundingPolicyEnum;
import com.aeris.service.operatoradmin.enums.IPoolingPolicyEnum;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.enums.IRESEnum;
import com.aeris.service.operatoradmin.enums.IResourceEnum;
import com.aeris.service.operatoradmin.enums.ISIMFormatEnum;
import com.aeris.service.operatoradmin.enums.ISIMSupplierEnum;
import com.aeris.service.operatoradmin.enums.ISIMWarehouseEnum;
import com.aeris.service.operatoradmin.enums.ISMPPConnectionEnum;
import com.aeris.service.operatoradmin.enums.ISMSFormatEnum;
import com.aeris.service.operatoradmin.enums.ITierCriteriaEnum;
import com.aeris.service.operatoradmin.enums.IVerticalEnum;
import com.aeris.service.operatoradmin.enums.impl.AccessPointNameEnum;
import com.aeris.service.operatoradmin.enums.impl.ApiKeyEnum;
import com.aeris.service.operatoradmin.enums.impl.ApiKeyPermissionEnum;
import com.aeris.service.operatoradmin.enums.impl.ContractEnum;
import com.aeris.service.operatoradmin.enums.impl.CountryEnum;
import com.aeris.service.operatoradmin.enums.impl.CspZoneEnum;
import com.aeris.service.operatoradmin.enums.impl.CurrencyEnum;
import com.aeris.service.operatoradmin.enums.impl.IccIdPrefixEnum;
import com.aeris.service.operatoradmin.enums.impl.IccIdRangeEnum;
import com.aeris.service.operatoradmin.enums.impl.ImsiRangeEnum;
import com.aeris.service.operatoradmin.enums.impl.OperatorEnum;
import com.aeris.service.operatoradmin.enums.impl.OperatorKeyEnum;
import com.aeris.service.operatoradmin.enums.impl.PacketRoundingPolicyEnum;
import com.aeris.service.operatoradmin.enums.impl.PoolingPolicyEnum;
import com.aeris.service.operatoradmin.enums.impl.ProductEnum;
import com.aeris.service.operatoradmin.enums.impl.RESEnum;
import com.aeris.service.operatoradmin.enums.impl.ResourceEnum;
import com.aeris.service.operatoradmin.enums.impl.SIMFormatEnum;
import com.aeris.service.operatoradmin.enums.impl.SIMSupplierEnum;
import com.aeris.service.operatoradmin.enums.impl.SIMWarehouseEnum;
import com.aeris.service.operatoradmin.enums.impl.SMPPConnectionEnum;
import com.aeris.service.operatoradmin.enums.impl.SMSFormatEnum;
import com.aeris.service.operatoradmin.enums.impl.TierCriteriaEnum;
import com.aeris.service.operatoradmin.enums.impl.VerticalEnum;
import com.aeris.service.operatoradmin.events.EventDispatcher;
import com.aeris.service.operatoradmin.events.IEventDispatcher;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.guice.utils.Matchers;
import com.aeris.service.operatoradmin.model.AccessPointName;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.ApplicationProperties;
import com.aeris.service.operatoradmin.model.Carrier;
import com.aeris.service.operatoradmin.model.EmailProperties;
import com.aeris.service.operatoradmin.model.Operator;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.SIMFormat;
import com.aeris.service.operatoradmin.model.SIMSupplier;
import com.aeris.service.operatoradmin.model.SIMWarehouse;
import com.aeris.service.operatoradmin.model.SMPPConnection;
import com.aeris.service.operatoradmin.model.SMSFormat;
import com.aeris.service.operatoradmin.model.Vertical;
import com.aeris.service.operatoradmin.s3.S3AccessManager;
import com.aeris.service.operatoradmin.utils.NonTieredRatePlanExporter;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.aeris.service.operatoradmin.utils.OperatorAdminEmailer;
import com.aeris.service.operatoradmin.utils.ReflectionUtils;
import com.aeris.service.operatoradmin.utils.TieredRatePlanExporter;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;

public class ServiceModule extends AbstractModule {
	private static final String PACKAGES_TO_SCAN = "com.aeris.service";
	@Override
	protected void configure() {
		bindProperties();
		bindDAOModule();
		bindEnumModule();
		bindCacheModule();
		bindStoredProcedureModule();
		bindEventModule();
		bindUtilsModule();
		this.bindQueryListener();
	}

	private void bindDAOModule() {
		bind(ISprintCSADAO.class).to(SprintCSADAO.class);
		bind(IAccountApplicationDAO.class).to(AccountApplicationDAO.class);
		bind(ICustomerSIMOrderDAO.class).to(CustomerSIMOrderDAO.class);
		bind(IGlobalSIMInventoryDAO.class).to(GlobalSIMInventoryDAO.class);
		bind(ISupplierSIMOrderDAO.class).to(SupplierSIMOrderDAO.class);
		bind(IAccountDAO.class).to(AccountDAO.class);
		bind(IDistributorDAO.class).to(DistributorDAO.class);
		bind(IServiceProfileDAO.class).to(ServiceProfileDAO.class);
		bind(IRatePlanDAO.class).to(RatePlanDAO.class);
		bind(IRatePlanPoolDAO.class).to(RatePlanPoolDAO.class);
		bind(IZoneDAO.class).to(ZoneDAO.class);
		bind(IOperatorProductDAO.class).to(OperatorProductDAO.class); 
		bind(IProductCarrierDAO.class).to(ProductCarrierDAO.class); 
		bind(IOperatorConfigDAO.class).to(OperatorConfigDAO.class); 
		bind(AerCloudClient.class).asEagerSingleton();
		bind(ICSPRatePlanDAO.class).to(CSPRatePlanDAO.class);
		bind(ISalesAgentDAO.class).to(SalesAgentDAO.class);
		bind(IZoneSetDAO.class).to(ZoneSetDAO.class);
        bind(IAS4gDAO.class).to(AS4gDAO.class);
        bind(AS4gClient.class).asEagerSingleton();
        bind(ISIMPackDAO.class).to(SIMPackDAO.class);
        bind(ICommunicationPlanDAO.class).to(CommunicationPlanDAO.class);
        bind(IRESDAO.class).to(RESDAO.class);
        bind(ISMPPConnectionDAO.class).to(SMPPConnectionDAO.class);
        bind(ISIMMgmtDAO.class).to(SIMMgmtDAO.class);
	}

	private void bindEnumModule() {
		bind(IApiKeyPermissionEnum.class).to(ApiKeyPermissionEnum.class);
		bind(IResourceEnum.class).to(ResourceEnum.class);
		bind(ISIMFormatEnum.class).to(SIMFormatEnum.class);
		bind(IProductEnum.class).to(ProductEnum.class);
		bind(ISIMSupplierEnum.class).to(SIMSupplierEnum.class);
		bind(ISIMWarehouseEnum.class).to(SIMWarehouseEnum.class);
		bind(IIccIdPrefixEnum.class).to(IccIdPrefixEnum.class);
		bind(IIccIdRangeEnum.class).to(IccIdRangeEnum.class);
		bind(IImsiRangeEnum.class).to(ImsiRangeEnum.class);
		bind(IOperatorEnum.class).to(OperatorEnum.class);
		bind(IAccessPointNameEnum.class).to(AccessPointNameEnum.class);
		bind(IVerticalEnum.class).to(VerticalEnum.class);
		bind(ISMPPConnectionEnum.class).to(SMPPConnectionEnum.class);
		bind(ISMSFormatEnum.class).to(SMSFormatEnum.class);
		bind(ICountryEnum.class).to(CountryEnum.class);
		bind(ICurrencyEnum.class).to(CurrencyEnum.class);
		bind(ICspZoneEnum.class).to(CspZoneEnum.class);
		bind(IPoolingPolicyEnum.class).to(PoolingPolicyEnum.class);
		bind(IPacketRoundingPolicyEnum.class).to(PacketRoundingPolicyEnum.class);
		bind(ITierCriteriaEnum.class).to(TierCriteriaEnum.class);
		bind(IApiKeyEnum.class).to(ApiKeyEnum.class);
		bind(IOperatorKeyEnum.class).to(OperatorKeyEnum.class);
		bind(IRESEnum.class).to(RESEnum.class);
		bind(IContractEnum.class).to(ContractEnum.class);
	}
	
	private void bindUtilsModule() {
		bind(OAUtils.class).asEagerSingleton();
		bind(NonTieredRatePlanExporter.class).asEagerSingleton();
		bind(TieredRatePlanExporter.class).asEagerSingleton();
        bind(OperatorAdminEmailer.class).asEagerSingleton();
		bind(IEmailDelegate.class).to(EmailDelegate.class);
		bind(Authenticator.class).asEagerSingleton();
		bind(S3AccessManager.class).asEagerSingleton();
	}
	
	private void bindCacheModule() {
		bindListener(Matchers.packageMatcher(ReflectionUtils.DEFAULT_PACKAGE), new CacheListener(Hazelcast.class));
	}

	private void bindStoredProcedureModule() {
		bind(new TypeLiteral<IStoredProcedure<List<Account>>>() {
		}).to(GetChildAccountsStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<Product>>>() {
		}).to(GetProductsStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<SIMFormat>>>() {
		}).to(GetSIMFormatsStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<SIMWarehouse>>>() {
		}).to(GetWarehousesStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<SIMSupplier>>>() {
		}).to(GetSuppliersStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<Operator>>>() {
		}).to(GetOperatorsStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<AccessPointName>>>() {
		}).to(GetAccessPointsStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<Vertical>>>() {
		}).to(GetVerticalsStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<SMPPConnection>>>() {
		}).to(GetSMPPConnectionsStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<SMSFormat>>>() {
		}).to(GetSMSFormatsStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<List<Carrier>>>() {
		}).to(GetCarriersByProductStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<ResultStatus>>() {
		}).annotatedWith(Names.named("AssignRatePlanToAccountStoredProcedure")).to(AssignRatePlanToAccountStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<ResultStatus>>() {
		}).annotatedWith(Names.named("ExpireAccountRatePlanStoredProcedure")).to(ExpireAccountRatePlanStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<ResultStatus>>() {
		}).annotatedWith(Names.named("createCSPRatePlanCDMAStoredProcedure")).to(CreateCSPRatePlanCDMAStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<ResultStatus>>() {
		}).annotatedWith(Names.named("createCSPRatePlanGSMStoredProcedure")).to(CreateCSPRatePlanGSMStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<ResultStatus>>() {
		}).annotatedWith(Names.named("updateCSPRatePlanCDMAStoredProcedure")).to(UpdateCSPRatePlanCDMAStoredProcedure.class);
		bind(new TypeLiteral<IStoredProcedure<ResultStatus>>() {
		}).annotatedWith(Names.named("updateCSPRatePlanGSMStoredProcedure")).to(UpdateCSPRatePlanGSMStoredProcedure.class);
	}
	
	private void bindProperties()
	{
		Names.bindProperties(binder(), PropertyConfigLoader.getInstance().readProperties("operatoradminapp.properties"));
        bind(ApplicationProperties.class).asEagerSingleton();
        bind(EmailProperties.class).asEagerSingleton();
        bind(ApplicationVersion.class).asEagerSingleton();
	}

	private void bindEventModule() {
		Multibinder<IEventListener> multibinder = Multibinder.newSetBinder(binder(), IEventListener.class);
		
		for (Class<? extends IEventListener> cls : ReflectionUtils.getSubTypesOf(IEventListener.class)) {
			multibinder.addBinding().to(cls);
		}

		bind(IEventDispatcher.class).to(EventDispatcher.class);
	}
    //Bind QueryListener
    private void bindQueryListener() {
        try {
            this.bindListener(Matchers.packageMatcher(PACKAGES_TO_SCAN),
                    new QueryListener(new String[]{"/oracle-queries-xml.properties"}));
        } catch (IOException ex) {
            throw new GenericServiceException("Unable to read properties- ", ex);
        }
    }
}
