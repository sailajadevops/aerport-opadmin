package com.aeris.service.operatoradmin.model;

import java.util.Date;

public class SIMPack {

    private String uniqueCode;
    private boolean used;
    private long customerAccountId;

    private String iccidStart;
    private String iccidEnd;
    private String iccidAdditional;

    private int packetSize;
    private String simType;

    private Date createdDate;
    private Date lastModifiedDate;
    private String createdBy;
    private String lastModifiedBy;

    private long distributorAccountId;

    public String getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(String uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public long getCustomerAccountId() {
        return customerAccountId;
    }

    public void setCustomerAccountId(long customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    public String getIccidStart() {
        return iccidStart;
    }

    public void setIccidStart(String iccidStart) {
        this.iccidStart = iccidStart;
    }

    public String getIccidEnd() {
        return iccidEnd;
    }

    public void setIccidEnd(String iccidEnd) {
        this.iccidEnd = iccidEnd;
    }

    public String getIccidAdditional() {
        return iccidAdditional;
    }

    public void setIccidAdditional(String iccidAdditional) {
        this.iccidAdditional = iccidAdditional;
    }

    public int getPacketSize() {
        return packetSize;
    }

    public void setPacketSize(int packetSize) {
        this.packetSize = packetSize;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public long getDistributorAccountId() {
        return distributorAccountId;
    }

    public void setDistributorAccountId(long distributorAccountId) {
        this.distributorAccountId = distributorAccountId;
    }

    @Override
    public String toString() {
        return "SIMPack{" + "uniqueCode=" + uniqueCode + ", used=" + used + ", customerAccountId=" + customerAccountId + ", iccidStart=" + iccidStart + ", iccidEnd=" + iccidEnd + ", iccidAdditional=" + iccidAdditional + ", packetSize=" + packetSize + ", simType=" + simType + ", createdDate=" + createdDate + ", lastModifiedDate=" + lastModifiedDate + ", createdBy=" + createdBy + ", lastModifiedBy=" + lastModifiedBy + ", distributorAccountId=" + distributorAccountId + '}';
    }
}
