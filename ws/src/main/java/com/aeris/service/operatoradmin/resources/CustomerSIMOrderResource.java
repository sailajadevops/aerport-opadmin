package com.aeris.service.operatoradmin.resources;

import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.security.auth.login.AccountNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.operatoradmin.dao.ICustomerSIMOrderDAO;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.enums.ISIMFormatEnum;
import com.aeris.service.operatoradmin.exception.SIMConfigNotFoundException;
import com.aeris.service.operatoradmin.exception.SIMOrderException;
import com.aeris.service.operatoradmin.exception.SIMOrderNotFoundException;
import com.aeris.service.operatoradmin.model.OrderStatus;
import com.aeris.service.operatoradmin.model.Product;
import com.aeris.service.operatoradmin.model.SIMConfig;
import com.aeris.service.operatoradmin.model.SIMFormat;
import com.aeris.service.operatoradmin.model.SIMOrder;
import com.aeris.service.operatoradmin.model.constraints.ValidOperator;
import com.aeris.service.operatoradmin.payload.CreateCustSIMOrderRequest;
import com.aeris.service.operatoradmin.payload.UpdateCustSIMOrderRequest;

/**
 * Rest Resource for Customer SIM Order management. Includes methods for 
 * <ul>
 * <li>
 * Placing a new SIM order for an account
 * </li>
 * <li>
 * Fetching the SIM orders for an account
 * </li>
 * <li>
 * Fetch SIM orders for an account by status
 * </li>
 * <li>
 * Fetch SIM order options for the account
 * </li>
 * </ul>
 * 
 * @author Srinivas Puranam
 */
@Path("/operators/{operatorId}/accounts/{accountId}/simOrders")
@Singleton
@RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
public class CustomerSIMOrderResource{
	private static final Logger LOG = LoggerFactory.getLogger(CustomerSIMOrderResource.class);

    @Inject
	private ICustomerSIMOrderDAO simOrderDAO;

    @Inject
	private IProductEnum productCache;
    
    @Inject
	private ISIMFormatEnum simFormatCache;
    
    /**
     * Retrieve SIM Orders for a specified account. Also Filter the SIM Orders by status.
     * Pagination support to send paged SIM Order data.
     * 
     * @param operatorId the operator for which the account belongs
     * @param accountId the account identifier
     * @param status the status of the order by which the results are filtered
     * @param start the start index of the order for pagination support 
     * @param count the no of SIM order records to send back to client in a round trip
     * 
     * @return {@link Response} with List of {@link SIMOrder} entities
     */
    @GET
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
	public Response getAllSIMOrders(@PathParam("operatorId") @ValidOperator final String operatorId, 
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId, 
			@QueryParam("status") @DefaultValue("all") final String status, 
			@QueryParam("start") final String start, 
			@QueryParam("count") final String count) {
		LOG.info("Received request to fetch all SIM Orders for account id: " + accountId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);

		try {
			if (!NumberUtils.isNumber(operatorId)) {
				LOG.error("The format of operatorId in the request is not valid, value is: '" + operatorId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of operatorId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("The format of accountId in the request is not valid, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of accountId in the request is not valid").build();
			}

			if (paginated) {
				LOG.info("Returning a max record size of " + count + " records starting from " + start);
			} else {
				LOG.info("Query is not paginated, returning all available SIM Orders for account: " + accountId);
			}

			List<SIMOrder> simOrders = null;
			
			// Fetch orders with all status if the supplied status is not valid
			if(!OrderStatus.isValid(status))
			{
				// Fetch from database
				simOrders = simOrderDAO.getAllSIMOrders(operatorId, accountId, start, count);
			}
			// Fetch orders with specific status like Open, In Progress, Completed etc. 
			else
			{
				// Fetch from database
				simOrders = simOrderDAO.getAllSIMOrders(operatorId, accountId, start, count, OrderStatus.fromName(status));
			}
			
			if (simOrders != null) {
				LOG.info("fetched SIM Orders successfully, sim order count: "+simOrders.size());

				ObjectMapper mapper = new ObjectMapper();
				String apiKeysJson = mapper.writeValueAsString(simOrders);

				builder.entity(apiKeysJson);
			} else {
				LOG.info("No SIM Orders found for account id : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No sim orders found for the accountId").build();
			}
		} catch (Exception e) {
			LOG.error("getAllSIMOrders Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting SIM Orders").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getAllSIMOrders: " + response);
		return response;
	}
    
    /**
     * Retrieve a SIM Order for the given order id for the specified account.
     * 
     * @param operatorId The operator for which the account belongs
     * @param accountId the account identifier
     * @param orderId the Order Id for which the SIM Order is fetched.
     *   
     * @return {@link Response} with the {@link SIMOrder}
     */
    @GET
    @Path("/{orderId}")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
    public Response getSIMOrder(@PathParam("operatorId") @ValidOperator final String operatorId, 
    		@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId, 
    		@PathParam("orderId") @Pattern(regexp = "[0-9]+", message = "{orderId.number}") final String orderId) {
		LOG.info("Received request to fetch all SIM Orders for account id: " + accountId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			if (!NumberUtils.isNumber(operatorId)) {
				LOG.error("The format of operatorId in the request is not valid, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of operatorId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("The format of accountId in the request is not valid, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of accountId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(orderId)) {
				LOG.error("The format of orderId in the request is not valid, value is: '" + orderId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of orderId in the request is not valid").build();
			}

			// Fetch from database
			SIMOrder simOrder = simOrderDAO.getSIMOrderByOrderId(operatorId, accountId, orderId);
			
			if (simOrder != null) {
				LOG.info("fetched Customer SIM Order for orderId: "+orderId);

				ObjectMapper mapper = new ObjectMapper();
				String apiKeysJson = mapper.writeValueAsString(simOrder);

				builder.entity(apiKeysJson);
			} else {
				LOG.info("No SIM Order found for order id: "+orderId+" against account: "+accountId);
				return Response.status(Status.NOT_FOUND).entity("No SIM Order found for order id: "+orderId+" against account: "+accountId).build();
			}
		} catch (Exception e) {
			LOG.error("getSIMOrder Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting SIM Order").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getApiKeys: " + response);
		return response;
	}
    
    /**
     * Fetch SIM Configuration for the specified account. SIM Configuration holds the list of {@link SIMFormat}, {@link Product} and SIM Cost combinations allowed for
     * account. 
     * 
     * @param operatorId The operator for which the account belongs
     * @param accountId the account identifier
     * 
     * @return {@link Response} with the list of {@link SIMConfig} options available for the account.
     */
    @GET
    @Path("/options")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR", "OPERATOR"})
    public Response getSIMConfig(@PathParam("operatorId") @ValidOperator final String operatorId, 
    		@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId) {
		LOG.info("Received request to fetch SIM Config for account id: " + accountId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);

		try {
			if (!NumberUtils.isNumber(operatorId)) {
				LOG.error("The format of operatorId in the request is not valid, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of operatorId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("The format of accountId in the request is not valid, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of accountId in the request is not valid").build();
			}
			
			// Fetch from database
			List<SIMConfig> simConfig = simOrderDAO.getSIMConfig(operatorId, accountId);
			
			if (simConfig != null) {
				LOG.info("fetched Customer SIM Config: "+ReflectionToStringBuilder.toString(simConfig));

				ObjectMapper mapper = new ObjectMapper();
				String simConfigJson = mapper.writeValueAsString(simConfig);

				builder.entity(simConfigJson);
			} else {
				LOG.info("No SIM Config found for account id: " + accountId);
				return Response.status(Status.NOT_FOUND).entity("No SIM Config found").build();
			}
		} catch (Exception e) {
			LOG.error("getSIMConfig Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while getting SIM Config").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getApiKeys: " + response);
		return response;
	}
    
	/**
	 * Creates a New SIM Order for the specified account under the specified operator. A New SIM Order request {@link CreateCustSIMOrderRequest} 
	 * will take the following values as input:
	 * 
	 *  <ul>
	 *  <li>
	 *  The SIM Format
	 *  </li>
	 *  <li>
	 *  The Product
	 *  </li>
	 *  <li>
	 *  Quantity of the SIMs
	 *  </li>
	 *  <li>
	 *  Start ICCID
	 *  </li>
	 *  <li>
	 *  Shipping Address
	 *  </li>
	 *  <li>
	 *  Description
	 *  </li>
	 *  </ul>
	 *  
	 * @param operatorId The operator for which the account belongs
	 * @param accountId The Account Identifier
	 * @param request The payload {@link CreateCustSIMOrderRequest} 
	 * 
	 * @return {@link Response} with the new {@link SIMOrder} which is created. 
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response createNewOrder(@PathParam("operatorId") @ValidOperator final String operatorId, 
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId, 
			@Valid CreateCustSIMOrderRequest request) {

		LOG.info("Received request to create SIM Order for account id: " + accountId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		long quantity = request.getQuantity();
		int productId = request.getProductId();
		int simFormatId = request.getSimFormatId();
		String userId = request.getUserId();
		Date requestedDate = new Date(); 
		String description = request.getDescription();
		String shippingAddress = request.getShippingAddress();
		String comments = request.getComments();
		String trackingNumber = request.getTrackingNumber();
		
		try {
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("The format of accountId in the request is not valid, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of accountId in the request is not valid").build();
			}
			
			if (request.getQuantity() <= 0) {
				LOG.error("Missing quantity in request, value is: '" + quantity + "'");
				return Response.status(Status.BAD_REQUEST).entity("Missing quantity in request, quantity must be a number greater than 0").build();
			}
			
			if (StringUtils.isEmpty(userId)) {
				LOG.error("Missing userId in request, value is: '" + userId + "'");
				return Response.status(Status.BAD_REQUEST).entity("Missing userId in request").build();
			}
			
			if (StringUtils.isEmpty(shippingAddress)) {
				shippingAddress = "-NA-";
			}
			
			Product product = productCache.getProductById(productId); 
			SIMFormat simFormat = simFormatCache.getSIMFormatById(simFormatId);
			
			if (product == null) {
				LOG.error("The productId in request is not valid, value is: '" + productId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The productId in request is not valid").build();
			}
			
			if (simFormat == null) {
				LOG.error("The simFormat in request is not valid, value is: '" + simFormatId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The simFormat in request is not valid").build();
			}

			// Create Customer Order in Database
			SIMOrder simOrder = simOrderDAO.createSIMOrder(operatorId, accountId, product.getProductId(), simFormat.getId(), quantity, description, shippingAddress, 
					trackingNumber, comments, requestedDate, userId);
			
			if (simOrder != null && simOrder.getOrderId() > 0) {
				LOG.info("Created Customer SIM Order Successfully, order id is : "+simOrder.getOrderId());

				ObjectMapper mapper = new ObjectMapper();
				String apiKeysJson = mapper.writeValueAsString(simOrder);

				builder.entity(apiKeysJson);
			} else {
				LOG.info("Create new sim order failed, invalid accountId : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("Create new sim order failed, accountId is not valid").build();
			}
		} catch (SIMConfigNotFoundException e) {
			LOG.error("SIMConfigNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (SIMOrderException e) {
			LOG.error("SIMOrderException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("createNewOrder Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while creating SIM Order").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getApiKeys: " + response);
		return response;
	}
	
	/**
	 * Update a SIM Order identified by the orderId for the specified account under the specified operator. The update SIM Order request {@link UpdateCustSIMOrderRequest} 
	 * will take the following values as input:
	 * 
	 *  <ul>
	 *  <li>
	 *  Quantity of the SIMs
	 *  </li>
	 *  <li>
	 *  The New Order Status
	 *  </li>
	 *  <li>
	 *  Start ICCID
	 *  </li>
	 *  <li>
	 *  Shipping Address
	 *  </li>
	 *  <li>
	 *  Description
	 *  </li>
	 *  </ul>
	 *  
	 * @param operatorId The operator for which the account belongs
	 * @param accountId The Account Identifier
	 * @param orderId The Order Id for the SIM Order which is updated
	 * @param request The payload {@link UpdateCustSIMOrderRequest}
	 * 
	 * @return {@link Response} with success message.
	 */
	@PUT
	@Path("/{orderId}")
	@Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response updateOrder(@PathParam("operatorId") @ValidOperator final String operatorId, 
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId,
			@PathParam("orderId") @Pattern(regexp = "[0-9]+", message = "{orderId.number}") final String orderId, 
			@Valid UpdateCustSIMOrderRequest request) {

		LOG.info("Received request to create SIM Order for account id: " + accountId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		long quantity = request.getQuantity();
		String description = request.getDescription();
		String shippingAddress = request.getShippingAddress();
		OrderStatus status = request.getStatus();
		String comments = request.getComments();
		String trackingNumber = request.getTrackingNumber();
		String userId = request.getUserId();
		Date requestedDate = new Date(); 
		
		try {
			if (!NumberUtils.isNumber(operatorId)) {
				LOG.error("The format of operatorId in the request is not valid, value is: '" + operatorId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of operatorId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("The format of accountId in the request is not valid, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of accountId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(orderId)) {
				LOG.error("The format of orderId in the request is not valid, value is: '" + orderId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of orderId in the request is not valid").build();
			}
			
			if (StringUtils.isEmpty(userId)) {
				LOG.error("Missing userId in request, value is: '" + userId + "'");
				return Response.status(Status.BAD_REQUEST).entity("Missing userId in request").build();
			}
			
			if (StringUtils.isEmpty(shippingAddress)) {
				shippingAddress = "-NA-";
			}
			
			// Update Customer Order in Database
			boolean flag = simOrderDAO.updateSIMOrder(operatorId, accountId, orderId, quantity, description, shippingAddress, status, trackingNumber, comments, 
					requestedDate, userId);
			
			if (flag) {
				LOG.info("Update Customer SIM Order Successfully, order id is : "+orderId);

				ObjectMapper mapper = new ObjectMapper();
				String updateOrderJson = mapper.writeValueAsString("SIM Order Id: "+orderId+" updated successfully");

				builder.entity(updateOrderJson);
			} else {
				LOG.info("Update sim order failed, invalid accountId : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("Update sim order failed, accountId is not valid").build();
			}
		} catch (SIMOrderNotFoundException e) {
			LOG.error("SIMOrderNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (SIMOrderException e) {
			LOG.error("SIMOrderException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("updateOrder Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while updating SIM Order").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getApiKeys: " + response);
		return response;
	}
	
	/**
	 * Delete a SIM Order identified by the orderId for the specified account under the specified operator. 
	 *  
	 * @param operatorId The operator for which the account belongs
	 * @param accountId The Account Identifier
	 * @param orderId The Order Id for the SIM Order which has to be deleted
	 * 
	 * @return {@link Response} with success message.
	 */
	@DELETE
	@Path("/{orderId}")
    @RolesAllowed({"PLATFORM_ADMIN", "OPERATOR_WR"})
	public Response deleteOrder(@PathParam("operatorId") @ValidOperator final String operatorId, 
			@PathParam("accountId") @Pattern(regexp = "[0-9]+", message = "{accountId.number}") final String accountId, 
			@PathParam("orderId") @Pattern(regexp = "[0-9]+", message = "{orderId.number}") final String orderId,
			@QueryParam("userId") @Email(message="{userId.email.pattern}") final String requestedUser) {

		LOG.info("Received request to create SIM Order for account id: " + accountId);
		
		ResponseBuilder builder = Response.ok().type(MediaType.APPLICATION_JSON);
		
		// requestedDate and userId will be used for Auditing purposes
		Date requestedDate  = new Date();
		
		try {
			if (!NumberUtils.isNumber(operatorId)) {
				LOG.error("The format of operatorId in the request is not valid, value is: '" + operatorId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of operatorId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(accountId)) {
				LOG.error("The format of accountId in the request is not valid, value is: '" + accountId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of accountId in the request is not valid").build();
			}
			
			if (!NumberUtils.isNumber(orderId)) {
				LOG.error("The format of orderId in the request is not valid, value is: '" + orderId + "'");
				return Response.status(Status.BAD_REQUEST).entity("The format of orderId in the request is not valid").build();
			}
			
			// Delete Customer Order in Database
			boolean flag = simOrderDAO.deleteSIMOrder(operatorId, accountId, orderId, requestedDate, requestedUser);
			
			if (flag) {
				LOG.info("Deleted Customer SIM Order Successfully, order id is : "+orderId);

				ObjectMapper mapper = new ObjectMapper();
				String apiKeysJson = mapper.writeValueAsString("SIM Order Id : "+orderId+" deleted successfully");

				builder.entity(apiKeysJson);
			} else {
				LOG.info("Delete sim order failed, invalid accountId : " + accountId);
				return Response.status(Status.NOT_FOUND).entity("Delete sim order failed, accountId is not valid").build();
			}
		} catch (SIMOrderNotFoundException e) {
			LOG.error("SIMOrderNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (AccountNotFoundException e) {
			LOG.error("AccountNotFoundException occured :", e);
			return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
		} catch (SIMOrderException e) {
			LOG.error("SIMOrderException occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} catch (Exception e) {
			LOG.error("deleteOrder Exception occured :", e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Unable to service your request. Fatal error while deleting SIM Order").build();
		}

		Response response = builder.build();

		LOG.debug("Sent response from getApiKeys: " + response);
		return response;
	}

}
