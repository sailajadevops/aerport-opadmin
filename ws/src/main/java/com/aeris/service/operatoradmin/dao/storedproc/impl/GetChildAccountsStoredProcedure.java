package com.aeris.service.operatoradmin.dao.storedproc.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import oracle.jdbc.OracleTypes;

import com.aeris.service.common.db.DBConstant;
import com.aeris.service.operatoradmin.dao.storedproc.AbstractProcedureCall;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.Account;
import com.aeris.service.operatoradmin.model.AccountStatus;
import com.aeris.service.operatoradmin.model.AccountType;
import com.aeris.service.operatoradmin.model.BillableStatus;
import com.aeris.service.operatoradmin.model.InvoiceAddress;
import com.aeris.service.operatoradmin.model.Region;
import com.google.inject.Inject;

public class GetChildAccountsStoredProcedure extends AbstractProcedureCall<List<Account>> {

	private static final String PROC_SUB_ACCOUNTS_LIST_UI = "prov_sub_accounts.sub_accounts_list_ui";

	@Inject
	public GetChildAccountsStoredProcedure() {
		super(DBConstant.CONFIG_PROVISION, PROC_SUB_ACCOUNTS_LIST_UI);

		registerParameter(new InParameter("in_account_id", Types.BIGINT));
		registerParameter(new InParameter("in_operator_id", Types.INTEGER));
		registerParameter(new InParameter("in_group_type", Types.VARCHAR));
		registerParameter(new OutParameter("out_curs", OracleTypes.CURSOR));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Account> execute(Object... input) throws StoredProcedureException {
		
		// Execute the proc
		Map<String, Object> results = (Map<String, Object>) super.execute(input);
		List<Map<String, Object>> cursor = null;
		
		// Read the cursor
		if (results != null) {
			cursor = (List<Map<String, Object>>) results.get("out_curs");
		}

		List<Account> accounts = createAccountList(cursor);

		return accounts;
	}

	private List<Account> createAccountList(List<Map<String, Object>> cursor) {
		List<Account> accounts = new ArrayList<Account>();

		for (Map<String, Object> row : cursor) {
			Account account = new Account();

			String accountId = (String) row.get("account_id");

			if (accountId != null) {
				account.setAccountId(Long.valueOf(accountId));
			}

			String accountType = (String) row.get("account_type");

			if (accountType != null) {
				account.setType(AccountType.fromValue(Integer.parseInt(accountType)));
			}

			String name = (String) row.get("name");

			if (name != null) {
				account.setAccountName(name);
			}

			String wwwAddr = (String) row.get("www_addr");

			if (wwwAddr != null) {
				account.setWebAddress(wwwAddr);
			}

			String region = (String) row.get("region_code");

			if (region != null) {
				account.setRegion(Region.fromValue(Integer.parseInt(region)));
			}

			String accountStatus = (String) row.get("account_status");

			if (accountStatus != null) {
				account.setStatus(AccountStatus.fromValue(Integer.parseInt(accountStatus)));
			}

			String invoiceAddress = (String) row.get("invoice_address");

			if (invoiceAddress != null) {
				InvoiceAddress address = new InvoiceAddress();
				address.setFullAddress(invoiceAddress);

				account.setInvoiceAddress(address);
			}

			String specialInstruction = (String) row.get("special_instruction");

			if (specialInstruction != null) {
				account.setSpecialInstructions(specialInstruction);
			}

			String includedAlertProfiles = (String) row.get("included_alert_profiles");

			if (includedAlertProfiles != null) {
				account.setIncludedAlertProfiles(Integer.valueOf(includedAlertProfiles));
			}

			String tags = (String) row.get("tags");

			if (tags != null) {
				account.setSpecialInstructions(tags);
			}

			String billableStatus = (String) row.get("billable_status");

			if (billableStatus != null) {
				account.setBillableStatus(BillableStatus.fromValue(Integer.parseInt(billableStatus)));
			}

			String operatorId = (String) row.get("operator_id");

			if (operatorId != null) {
				account.setOperatorId(Integer.valueOf(operatorId));
			}

			String netSuiteId = (String) row.get("netsuite_id");

			if (netSuiteId != null) {
				account.setNetSuiteId(netSuiteId);
			}

			String parentAccountId = (String) row.get("parent_account_id");

			if (parentAccountId != null) {
				account.setParentAccountId(parentAccountId);
			}

			String carrierAccountId = (String) row.get("carrier_account_id");

			if (carrierAccountId != null) {
				account.setCarrierAccountId(carrierAccountId);
			}

			String apiKey = (String) row.get("api_key");

			if (apiKey != null) {
				account.setApiKey(apiKey);
			}

			String productIds = (String) row.get("product_ids");

			if (productIds != null) {
				account.setProductIds(getProductIdsForAccount(productIds));
			}

			accounts.add(account);
		}
		
		return accounts;
	}

	private List<String> getProductIdsForAccount(String productId) {
		Set<String> productIds = new HashSet<String>();

		if (productId != null) {
			String[] productIdArr = productId.split(",");
			Collections.addAll(productIds, productIdArr);
		}

		return new ArrayList<String>(productIds);
	}
}
