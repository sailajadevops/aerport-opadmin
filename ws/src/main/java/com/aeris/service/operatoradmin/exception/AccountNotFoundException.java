package com.aeris.service.operatoradmin.exception;

public class AccountNotFoundException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public AccountNotFoundException(String s) {
		super(s);
	}

	public AccountNotFoundException(String ex, Throwable t) {
		super(ex, t);
	}
}
