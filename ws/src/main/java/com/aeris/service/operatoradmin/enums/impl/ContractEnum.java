package com.aeris.service.operatoradmin.enums.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.enums.IContractEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.model.Contract;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.google.inject.Singleton;

@Singleton
public class ContractEnum implements IContractEnum,IEventListener{
private Logger LOG = LoggerFactory.getLogger(ContractEnum.class);
	
	private Cache<Long, Contract> contractCache;
	private static final String SQL_GET_ALL_CONTRACTS = "select ac.ACCOUNT_ID, c.id, c.NAME , c.PARTNER_ID, c.AUTHORIZATION, c.GATEWAY_URL, c.SMPP_CONNECTION_ID, c.LICENSE_KEY, c.Permanent_Dialable_Number  from ACCOUNT_CONTRACT ac, CONTRACT c where ac.CONTRACT_ID = c.id";
	
	
	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
	
	public void init(@Hazelcast(cache = "contractCache/name") Cache<Long, Contract> contractCache){
		this.contractCache = contractCache;
		loadResources();
	}
	
	public void loadResources(){
		LOG.info("loadResources : loading all contracts from database ..");

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			if(LOG.isDebugEnabled()){
				LOG.debug("SQL_GET_ALL_CONTRACTS :"+SQL_GET_ALL_CONTRACTS);
			}

			ps = conn.prepareStatement(SQL_GET_ALL_CONTRACTS);
			rs = ps.executeQuery();

			while(rs.next()){
				Contract contract = new Contract();
				long accId = rs.getLong("ACCOUNT_ID");
				contract.setAccountId(accId);
				contract.setContractId(rs.getInt("id"));
				contract.setContractName(rs.getString("NAME"));
				contract.setPartnerId(rs.getString("PARTNER_ID"));
				contract.setAuthorization(rs.getString("AUTHORIZATION"));
				contract.setGatewayUrl(rs.getString("GATEWAY_URL"));
				contract.setSmppConnectionId(rs.getLong("SMPP_CONNECTION_ID"));
				contract.setLicenseKey(rs.getString("LICENSE_KEY"));
				int permDialableNo = rs.getInt("Permanent_Dialable_Number");
				contract.setPermanentDialableNo(permDialableNo == 0 ? false : true);
				contractCache.put(accId, contract);
			}

		}catch (Exception e) {
			LOG.error("Exception during getAllContracts " + e);
			throw new GenericServiceException("Exception during getAllContracts - ", e);
		}  finally {
			DBUtils.cleanup(conn, ps, rs);
		}

	}
	
	@Override
	public Contract getContract(long accountId) {
		return contractCache.get(accountId);
	}
	
	@Override
	public void putContractInCache(long accountId, Contract contract) {
		contractCache.put(accountId, contract);
	}
	
	@Override
	public void removeContractFromCache(long accountId) {
		contractCache.evict(accountId);
	}

}
