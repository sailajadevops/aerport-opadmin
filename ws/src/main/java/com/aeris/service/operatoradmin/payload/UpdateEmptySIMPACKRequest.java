package com.aeris.service.operatoradmin.payload;

import java.util.List;

/**
 *
 * @author SB00122138
 */
public class UpdateEmptySIMPACKRequest {

    private String userId;
    private List<Simpack> simpacks;
    private List<String> emailRecipients ;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Simpack> getSimpacks() {
        return simpacks;
    }

    public void setSimpacks(List<Simpack> simpacks) {
        this.simpacks = simpacks;
    }

	public List<String> getEmailRecipients() {
		return emailRecipients;
	}

	public void setEmailRecipients(List<String> emailRecipients) {
		this.emailRecipients = emailRecipients;
	}

}
