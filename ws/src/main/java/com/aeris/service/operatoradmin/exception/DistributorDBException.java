package com.aeris.service.operatoradmin.exception;

public class DistributorDBException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public DistributorDBException(String s) {
		super(s);
	}

	public DistributorDBException(String ex, Throwable t) {
		super(ex, t);
	}
}
