package com.aeris.service.operatoradmin.exception;

public class StoredProcedureException extends Exception {
	private static final long serialVersionUID = 3856961344904116103L;

	public StoredProcedureException(String s) {
		super(s);
	}

	public StoredProcedureException(String ex, Throwable t) {
		super(ex, t);
	}
}
