package com.aeris.service.operatoradmin.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.ISprintCSADAO;
import com.aeris.service.operatoradmin.model.SprintCSA;
import com.aeris.service.operatoradmin.utils.DBUtils;


/**
 * DAO for the sprint csa management.
 * Allows the upper layers to do crud operations in the 
 * sprint_csa_v1 table.
 * 
 * @author Srinivas Puranam
 */
public class SprintCSADAO implements ISprintCSADAO{
    private static final String UPD_SEC_CSA_QUERY = "update aerbill_prov.sprint_csa_v1 set secondary_csa = ?, last_modified_date = ?, last_modified_by = ?, rep_timestamp = ? where carrier_id = ?";
    private static final String SWITCH_CSA_QUERY = "update aerbill_prov.sprint_csa_v1 set csa = ?, secondary_csa = ?, last_modified_date = ?, last_modified_by = ?, rep_timestamp = ? where carrier_id = ?";
	private static final String GET_CSA_SQL = "select csa, secondary_csa, last_modified_date, last_modified_by from aerbill_prov.sprint_csa_v1 where carrier_id = ?";
	private static final Logger LOG = LoggerFactory.getLogger(SprintCSADAO.class);
    
	/* (non-Javadoc)
	 * @see com.aeris.service.aeraccountmgmt.dao.csa.ISprintCSADAO#getSprintCSA(java.lang.String)
	 */
	@Override
	public SprintCSA getSprintCSA(String carrierId) {
		Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        SprintCSA csa = null;
        try {
        	LOG.info("fetching csa and secondary csa from db");
            conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
            
            if(conn != null)
            {
            	LOG.info("Connection established");
            	LOG.debug("Executing query: "+GET_CSA_SQL+", with params: ("+carrierId+")");
            	
                ps = conn.prepareStatement(GET_CSA_SQL);
                ps.setLong(1, Long.valueOf(carrierId));
                rs = ps.executeQuery();
            }
            
            if(rs != null && rs.next())
            {
                LOG.info("Processing result set");
                
            	csa = new SprintCSA();
            	csa.setPrimaryCSA(rs.getString(1));
            	csa.setSecondaryCSA(rs.getString(2));
            	csa.setLastModifiedDate(rs.getDate(3));
            	csa.setLastModifiedBy(rs.getString(4));
            	
            	LOG.debug("created SprintCSA model "+ReflectionToStringBuilder.toString(csa));
            }
        }
        catch(Exception e)
        {
        	e.printStackTrace();
            LOG.error("Exception during getSprintCSA()" + e);
        }
        finally
        {
        	LOG.info("closing db resources");
        	DBUtils.cleanup(conn, ps, rs);
        }
           
		return csa;
	}

	/* (non-Javadoc)
	 * @see com.aeris.service.aeraccountmgmt.dao.csa.ISprintCSADAO#switchCSA(java.lang.String, java.util.Date, java.lang.String)
	 */
	@Override
	public boolean switchCSA(String carrierId, Date lastModifiedDate, String lastModifiedBy) {
		Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean success = false;
        int updateCnt = 0;
        
        try {
        	SprintCSA oldCsa = getSprintCSA(carrierId);
        	
        	if(oldCsa != null)
        	{
        		String primaryCsa = oldCsa.getPrimaryCSA();
        		String secondaryCsa = oldCsa.getSecondaryCSA();
            	
            	LOG.info("switch csa: swapping the csa and secondary csa values in db for carrierId: "+carrierId);
                conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
                
                if(conn != null)
                {
                	LOG.info("Connection established");
                	LOG.debug("Executing query: "+SWITCH_CSA_QUERY+", with params: ("+primaryCsa+","+secondaryCsa+","+lastModifiedDate+","+lastModifiedBy+","+carrierId+")");
                	
                    ps = conn.prepareStatement(SWITCH_CSA_QUERY);
                    ps.setString(1, secondaryCsa);
                    ps.setString(2, primaryCsa);
                    ps.setDate(3, new java.sql.Date(lastModifiedDate.getTime()));
                    ps.setString(4, lastModifiedBy);
                    ps.setTimestamp(5, new java.sql.Timestamp(lastModifiedDate.getTime()));
                    ps.setLong(6, Long.valueOf(carrierId));
                    
                    updateCnt = ps.executeUpdate();
                    LOG.debug("Updated Rows: "+updateCnt+" in aerbill_prov.sprint_csa_v1");
                }
                
                if(updateCnt > 0)
                {
                    LOG.info("Switched Csa's. Updated Successfully.");
                    success = true;
                }
                
            }
        	else
	    	{
	    		LOG.error("No CSA details found for carriedId: "+carrierId+", switching csa failed.");
	    	}
        }
        catch(Exception e)
        {
        	e.printStackTrace();
            LOG.error("Exception during switchCSA()" + e);
        }
        finally
        {
        	LOG.info("closing db resources");
        	DBUtils.cleanup(conn, ps, rs);
        }
           
		return success;
	}

	/* (non-Javadoc)
	 * @see com.aeris.service.aeraccountmgmt.dao.csa.ISprintCSADAO#updateSecondaryCSA(java.lang.String, java.lang.String, java.util.Date, java.lang.String)
	 */
	@Override
	public boolean updateSecondaryCSA(String carrierId, String newCSA, Date lastModifiedDate, String lastModifiedBy) {
		Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean success = false;
        int updateCnt = 0;
        
        try {
        	if(newCSA != null)
        	{
        		String secondaryCsa = newCSA;
            	
            	LOG.info("updating the secondary csa values in db for carrierId: "+carrierId);
                conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
                
                if(conn != null)
                {
                	LOG.info("Connection established");
                	LOG.debug("Executing query: "+UPD_SEC_CSA_QUERY+", with params: ("+secondaryCsa+","+lastModifiedDate+","+lastModifiedBy+","+carrierId+")");
                	
                    ps = conn.prepareStatement(UPD_SEC_CSA_QUERY);
                    ps.setString(1, secondaryCsa);
                    ps.setDate(2, new java.sql.Date(lastModifiedDate.getTime()));
                    ps.setString(3, lastModifiedBy);
                    ps.setTimestamp(4, new java.sql.Timestamp(lastModifiedDate.getTime()));
                    ps.setLong(5, Long.valueOf(carrierId));
                    
                    updateCnt = ps.executeUpdate();
                    LOG.debug("Updated Rows: "+updateCnt+" in aerbill_prov.sprint_csa_v1");
                }
                
                if(updateCnt > 0)
                {
                    LOG.info("Updated Secondary CSA Successfully with value: "+secondaryCsa);
                    success = true;
                }
                
            }
        	else
	    	{
	    		LOG.error("New Secondary CSA Value cannot be empty");
	    	}
        }
        catch(Exception e)
        {
        	e.printStackTrace();
            LOG.error("Exception during updateSecondaryCSA()" + e);
        }
        finally
        {
        	LOG.info("closing db resources");
        	DBUtils.cleanup(conn, ps, rs);
        }
           
		return success;
	}

}
