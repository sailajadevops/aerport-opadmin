package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class SMPPConfiguration implements Serializable {
	private static final long serialVersionUID = 6841897394579261153L;
	
	@NotNull(message="{smppConnectionId.notnull}")
	@Min(value=1, message="{smppConnectionId.number}")
	private int smppConnectionId;	

	@NotNull(message="{smsFormatCode.notnull}")
	@Min(value=1, message="{smsFormatCode.number}")
	private int smsFormatCode;

	public int getSmppConnectionId() {
		return smppConnectionId;
	}

	public void setSmppConnectionId(int smppConnectionId) {
		this.smppConnectionId = smppConnectionId;
	}

	public int getSmsFormatCode() {
		return smsFormatCode;
	}

	public void setSmsFormatCode(int smsFormatCode) {
		this.smsFormatCode = smsFormatCode;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}	
}
