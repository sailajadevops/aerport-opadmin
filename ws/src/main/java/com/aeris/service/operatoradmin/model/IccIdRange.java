package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import java.util.Date;

public class IccIdRange implements Serializable {
	private static final long serialVersionUID = -2108681718789026951L;
	private String range;
	private String rangeName;
	private Date orderDate;
	private long tail;

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public String getRangeName() {
		return rangeName;
	}

	public void setRangeName(String rangeName) {
		this.rangeName = rangeName;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public long getTail() {
		return tail;
	}

	public void setTail(long tail) {
		this.tail = tail;
	}
}