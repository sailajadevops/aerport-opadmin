package com.aeris.service.operatoradmin.model;

import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HssProfile implements Serializable {

    private String id;
    private String psConf;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPsConf() {
        return psConf;
    }

    public void setPsConf(String psConf) {
        this.psConf = psConf;
    }
}