package com.aeris.service.operatoradmin.dao.impl;

import static com.aeris.service.operatoradmin.utils.ApplicationUtils.convertStringToDouble;
import static com.aeris.service.operatoradmin.utils.ApplicationUtils.convertStringToInt;
import static com.aeris.service.operatoradmin.utils.ApplicationUtils.convertStringToTS;
import static com.aeris.service.operatoradmin.utils.ApplicationUtils.getGMTDateStr;

import java.math.BigDecimal;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.common.db.DBConnectionManager;
import com.aeris.service.operatoradmin.dao.ICSPRatePlanDAO;
import com.aeris.service.operatoradmin.dao.IProductCarrierDAO;
import com.aeris.service.operatoradmin.dao.IRatePlanDAO;
import com.aeris.service.operatoradmin.dao.IZoneDAO;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.dao.storedproc.ResultStatus;
import com.aeris.service.operatoradmin.enums.ICspZoneEnum;
import com.aeris.service.operatoradmin.enums.IProductEnum;
import com.aeris.service.operatoradmin.exception.AccountRatePlanMappingException;
import com.aeris.service.operatoradmin.exception.BadStartDateException;
import com.aeris.service.operatoradmin.exception.DAOException;
import com.aeris.service.operatoradmin.exception.DuplicateRatePlanException;
import com.aeris.service.operatoradmin.exception.DuplicateZoneException;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.RatePlanAlreadyAssignedException;
import com.aeris.service.operatoradmin.exception.RatePlanException;
import com.aeris.service.operatoradmin.exception.RatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.exception.SubRatePlanNotFoundException;
import com.aeris.service.operatoradmin.exception.ZoneNotFoundException;
import com.aeris.service.operatoradmin.model.AccountRatePlan;
import com.aeris.service.operatoradmin.model.ApplicationProperties;
import com.aeris.service.operatoradmin.model.Carrier;
import com.aeris.service.operatoradmin.model.CarrierRatePlan;
import com.aeris.service.operatoradmin.model.CspZone;
import com.aeris.service.operatoradmin.model.DurationUnit;
import com.aeris.service.operatoradmin.model.IncludedPlanDetails;
import com.aeris.service.operatoradmin.model.PacketUnit;
import com.aeris.service.operatoradmin.model.ProvisionTriggerSettings;
import com.aeris.service.operatoradmin.model.RatePlan;
import com.aeris.service.operatoradmin.model.RatePlanAccessType;
import com.aeris.service.operatoradmin.model.RatePlanFeeDetails;
import com.aeris.service.operatoradmin.model.RatePlanOverageBucket;
import com.aeris.service.operatoradmin.model.RatePlanPaymentType;
import com.aeris.service.operatoradmin.model.RatePlanPeriodType;
import com.aeris.service.operatoradmin.model.RatePlanStatus;
import com.aeris.service.operatoradmin.model.RatePlanTier;
import com.aeris.service.operatoradmin.model.RatePlanType;
import com.aeris.service.operatoradmin.model.SuspendTriggerSettings;
import com.aeris.service.operatoradmin.model.Zone;
import com.aeris.service.operatoradmin.model.ZoneRatePlan;
import com.aeris.service.operatoradmin.utils.Constants;
import com.aeris.service.operatoradmin.utils.DBUtils;
import com.aeris.service.operatoradmin.utils.OAUtils;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.inject.Inject;
import com.google.inject.name.Named;

public class RatePlanDAO implements IRatePlanDAO {
	
 private static final int IN_USE = 1;

 private static Logger LOG = LoggerFactory.getLogger(RatePlanDAO.class);
 
 public static final String SQL_GET_ASSIGNED_RATE_PLANS =  "select * "+ 
		 													"from (" +
															"select distinct crp.technology_type, "+
															" arp.account_rate_plan_id, "+
															" arp.carrier_plan_id, "+
															" crp.rate_plan_id, "+
															" rp.rate_plan_label, "+
															" rp.contract_term, "+
															" crp.rate_plan_name, "+
															" to_char(arp.start_date, 'yyyy-mm-dd')    as arp_start_date, "+
															" to_char(arp.end_date, 'yyyy-mm-dd')      as arp_end_date, "+
															" to_char(crp.start_date, 'yyyy-mm-dd')     as start_date, "+
															" to_char(crp.end_date, 'yyyy-mm-dd')       as end_date, "+
															" crp.service_plan_cost as rate_plan_access_fee, "+
															" crp.access_fee_period_type, "+
															" crp.access_fee_period_length as access_fee_period_length, "+
															" crp.first_bill_activation_fee, "+
															" crp.suspend_fee, "+
															" crp.unsuspend_fee, "+
															" crp.deactivation_fee, "+
															" crp.reactivation_fee, "+
															" crp.product_id, "+
															" ROW_NUMBER() OVER (ORDER BY arp.account_rate_plan_id) AS rnum "+ 
															" from aerbill_prov.account_rate_plan_v1 arp "+
															" inner join aerbill_prov.csp_rate_plan crp "+
															" on arp.csp_rate_plan_id = crp.rate_plan_id "+
															" left join rate_plan rp "+
															" on crp.rate_plan_id = rp.rate_plan_id "+
															" where arp.account_id      = ? "+
												    		" and sysgmtdate between arp.start_date and arp.end_date "+
															" group by crp.rate_plan_id, "+
															"  rp.rate_plan_label, "+
															"  rp.contract_term," +
															"  crp.rate_plan_name, "+
															"  arp.account_rate_plan_id, "+
															"  arp.carrier_plan_id, "+
															"  crp.access_fee_period_type, "+
															"  crp.access_fee_period_length, "+
															"  arp.start_date, "+
															"  arp.end_date, "+
															"  crp.start_date, "+
															"  crp.end_date, "+
															"  crp.service_plan_cost, "+
															"  crp.technology_type, "+
															"  crp.first_bill_activation_fee, "+
															"  crp.suspend_fee, "+
															"  crp.unsuspend_fee, "+
															"  crp.deactivation_fee, "+
															"  crp.reactivation_fee, "+
															"  crp.product_id)";
 
 public static final String SQL_GET_ASSIGNED_RATE_PLAN_BY_ID =  "select * "+ 
		 													"from (" +
															"select distinct crp.technology_type, "+
															" arp.account_rate_plan_id, "+
															" arp.carrier_plan_id, "+
															" crp.rate_plan_id, "+
															" rp.rate_plan_label, "+
															" rp.contract_term, "+
															" crp.rate_plan_name, "+
															" to_char(arp.start_date, 'yyyy-mm-dd')    as arp_start_date, "+
															" to_char(arp.end_date, 'yyyy-mm-dd')      as arp_end_date, "+
															" to_char(crp.start_date, 'yyyy-mm-dd')     as start_date, "+
															" to_char(crp.end_date, 'yyyy-mm-dd')       as end_date, "+
															" crp.service_plan_cost as rate_plan_access_fee, "+
															" crp.access_fee_period_type, "+
															" crp.access_fee_period_length as access_fee_period_length, "+
															" crp.first_bill_activation_fee, "+ 
															" crp.suspend_fee, "+
															" crp.unsuspend_fee, "+
															" crp.deactivation_fee, "+
															" crp.reactivation_fee, "+
															" crp.product_id, "+
															" ROW_NUMBER() OVER (ORDER BY arp.account_rate_plan_id) AS rnum "+ 
															" from aerbill_prov.account_rate_plan_v1 arp "+
															" inner join aerbill_prov.csp_rate_plan crp "+
															" on arp.csp_rate_plan_id = crp.rate_plan_id "+
															" left join rate_plan rp "+
															" on crp.rate_plan_id = rp.rate_plan_id "+
															" where arp.account_id      = ? "+
															" and arp.csp_rate_plan_id = ? "+
															//AERPORT-5928 : rate plans with future date will be considered for approval 
												    		//" and sysgmtdate between arp.start_date and arp.end_date "+
															" group by crp.rate_plan_id, "+
															"  rp.rate_plan_label, "+
															"  rp.contract_term," +
															"  crp.rate_plan_name, "+
															"  arp.account_rate_plan_id, "+
															"  arp.carrier_plan_id, "+
															"  crp.access_fee_period_type, "+
															"  crp.access_fee_period_length, "+
															"  arp.start_date, "+
															"  arp.end_date, "+
															"  crp.start_date, "+
															"  crp.end_date, "+
															"  crp.service_plan_cost, "+
															"  crp.technology_type, "+
															"  crp.first_bill_activation_fee, "+
															"  crp.suspend_fee, "+
															"  crp.unsuspend_fee, "+
															"  crp.deactivation_fee, "+
															"  crp.reactivation_fee, "+
															"  crp.product_id)";

 public static final String SQL_GET_ASSIGNED_RATE_PLAN_ACROSS_ACCOUNTS_BY_ID =  "SELECT "+
																				  "arp.account_rate_plan_id, "+
																				  "arp.account_id "+
																				"FROM aerbill_prov.account_rate_plan_v1 arp "+
																				"WHERE arp.csp_rate_plan_id = ? "+
																				"AND sysgmtdate BETWEEN arp.start_date AND arp.end_date ";
 
 private static String SQL_GET_ALL_RATE_PLANS_EXCL_SUB_RATE_PLANS =  "select  "+
																	 "a.rate_plan_id               , "+
																	 "a.rate_plan_label            , "+
																	 "a.rate_plan_name             , "+
																	 "a.rate_plan_type             , "+
																	 "a.product_id                 , "+
																	 "a.is_shared                  , "+
																	 "a.home_zone_id               , "+
																	 "a.account_id                 , "+
																	 "a.status_id                  , "+
																	 "a.start_date                 , "+
																	 "a.end_date                   , "+
																	 "a.currency_code              , "+
																	 "a.zone_set_id 				 "+				
																	 "from rate_plan a               "+
																	 "where sysgmtdate between a.start_date and a.end_date " +
																	 "and a.rate_plan_type != 3";// Fetch only non sub rate plans
 
 private static String SQL_GET_ALL_RATE_PLANS_EXCL_SUB_RATE_PLANS_2 =  "select  "+
																	 "a.rate_plan_id               , "+
																	 "a.rate_plan_label            , "+
																	 "a.rate_plan_name             , "+
																	 "a.rate_plan_type             , "+
																	 "a.product_id                 , "+
																	 "a.is_shared                  , "+
																	 "a.home_zone_id               , "+
																	 "a.account_id                 , "+
																	 "a.status_id                  , "+
																	 "a.start_date                 , "+
																	 "a.end_date                   , "+
																	 "a.currency_code              , "+
                                                                     "a.operator_id              , "+
																	 "a.zone_set_id 				 "+				
																	 "from rate_plan a               "+
																	 "where sysgmtdate between a.start_date and a.end_date " +
																	 "and a.rate_plan_type != 3";// Fetch only non sub rate plans
 
 
 private static String SQL_GET_ALL_RATE_PLANS =  "select  "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.product_id                 , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
												 "a.zone_set_id 	       , "+
												 "a.carrier_rate_plan_id       , "+
                                                                                                 "a.wholesale_rate_plan_id     , "+
                                                                                                 "a.is_wholesale                 "+
												 "from rate_plan a               "+
												 "where sysgmtdate between a.start_date and a.end_date";
 
 private static String SQL_GET_ALL_RATE_PLANS_2 =  "select  "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.product_id                 , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
                                                 "a.operator_id              , "+
												 "a.zone_set_id 	       , "+
												 "a.carrier_rate_plan_id       , "+
                                                                                                 "a.wholesale_rate_plan_id     , "+
                                                                                                 "a.is_wholesale                 "+
												 "from rate_plan a               "+
												 "where sysgmtdate between a.start_date and a.end_date";
 
 
 private static String SQL_GET_ALL_RATE_PLANS_BY_ACCESS_TYPE =   "select  "+
																 "a.rate_plan_id               , "+
																 "a.rate_plan_label            , "+
																 "a.rate_plan_name             , "+
																 "a.rate_plan_type             , "+
																 "a.product_id                 , "+
																 "a.is_shared                  , "+
																 "a.home_zone_id               , "+
																 "a.account_id                 , "+
																 "a.status_id                  , "+
																 "a.start_date                 , "+
																 "a.end_date                   , "+
																 "a.currency_code              , "+
																 "a.zone_set_id 				 "+
																 "from rate_plan a               "+
																 "where sysgmtdate between a.start_date and a.end_date " +
																 "and a.rate_plan_type != 3 "+// Fetch only non sub rate plans
																 "and a.is_shared = ?";
 
 private static String SQL_GET_ALL_RATE_PLANS_BY_ACCESS_TYPE_2 =   "select  "+
																 "a.rate_plan_id               , "+
																 "a.rate_plan_label            , "+
																 "a.rate_plan_name             , "+
																 "a.rate_plan_type             , "+
																 "a.product_id                 , "+
																 "a.is_shared                  , "+
																 "a.home_zone_id               , "+
																 "a.account_id                 , "+
																 "a.status_id                  , "+
																 "a.start_date                 , "+
																 "a.end_date                   , "+
																 "a.currency_code              , "+
                                                                 "a.operator_id              , "+
																 "a.zone_set_id 				 "+
																 "from rate_plan a               "+
																 "where sysgmtdate between a.start_date and a.end_date " +
																 "and a.rate_plan_type != 3 "+// Fetch only non sub rate plans
																 "and a.is_shared = ?";
 
 
 private static String SQL_GET_ALL_RATE_PLANS_BY_PRODUCT =  "select  "+
																	 "a.rate_plan_id               , "+
																	 "a.rate_plan_label            , "+
																	 "a.rate_plan_name             , "+
																	 "a.rate_plan_type             , "+
																	 "a.product_id                 , "+
																	 "a.is_shared                  , "+
																	 "a.home_zone_id               , "+
																	 "a.account_id                 , "+
																	 "a.status_id                  , "+
																	 "a.start_date                 , "+
																	 "a.end_date                   , "+
																	 "a.currency_code              , "+
																	 "a.zone_set_id 	       , "+
																	 "a.wholesale_rate_plan_id     , "+
																	 "a.is_wholesale "                 +
																	 "from rate_plan a               "+
																	 "where sysgmtdate between a.start_date and a.end_date "+
																	 "and a.rate_plan_type != 3 "+// Fetch only non sub rate plans
																	 "and a.product_id in :product_id_list";
 
 private static String SQL_GET_ALL_RATE_PLANS_BY_PRODUCT_2 =  "select  "+
																	 "a.rate_plan_id               , "+
																	 "a.rate_plan_label            , "+
																	 "a.rate_plan_name             , "+
																	 "a.rate_plan_type             , "+
																	 "a.product_id                 , "+
																	 "a.is_shared                  , "+
																	 "a.home_zone_id               , "+
																	 "a.account_id                 , "+
																	 "a.status_id                  , "+
																	 "a.start_date                 , "+
																	 "a.end_date                   , "+
																	 "a.currency_code              , "+
																	 "a.zone_set_id 	       , "+
																	 "a.wholesale_rate_plan_id     , "+
                                                                     "a.operator_id     , "+
																	 "a.is_wholesale "                 +
																	 "from rate_plan a               "+
																	 "where sysgmtdate between a.start_date and a.end_date "+
																	 "and a.rate_plan_type != 3 "+// Fetch only non sub rate plans
																	 "and a.product_id in :product_id_list";
 
 
 private static String SQL_GET_ALL_RATE_PLANS_BY_PRODUCT_AND_ACCESS_TYPE =  "select  "+
																	 "a.rate_plan_id               , "+
																	 "a.rate_plan_label            , "+
																	 "a.rate_plan_name             , "+
																	 "a.rate_plan_type             , "+
																	 "a.product_id                 , "+
																	 "a.is_shared                  , "+
																	 "a.home_zone_id               , "+
																	 "a.account_id                 , "+
																	 "a.status_id                  , "+
																	 "a.start_date                 , "+
																	 "a.end_date                   , "+
																	 "a.currency_code                "+
																	 "from rate_plan a               "+
																	 "where sysgmtdate between a.start_date and a.end_date "+
																	 "and a.rate_plan_type != 3 "+// Fetch only non sub rate plans
																	 "and a.product_id in :product_id_list "+
																 	 "and a.is_shared = ?";
 
 private static String SQL_GET_ALL_RATE_PLANS_BY_PRODUCT_AND_ACCESS_TYPE_2 =  "select  "+
																	 "a.rate_plan_id               , "+
																	 "a.rate_plan_label            , "+
																	 "a.rate_plan_name             , "+
																	 "a.rate_plan_type             , "+
																	 "a.product_id                 , "+
																	 "a.is_shared                  , "+
																	 "a.home_zone_id               , "+
																	 "a.account_id                 , "+
																	 "a.status_id                  , "+
																	 "a.start_date                 , "+
																	 "a.end_date                   , "+
                                                                     "a.operator_id                   , "+
																	 "a.currency_code                "+
																	 "from rate_plan a               "+
																	 "where sysgmtdate between a.start_date and a.end_date "+
																	 "and a.rate_plan_type != 3 "+// Fetch only non sub rate plans
																	 "and a.product_id in :product_id_list "+
																 	 "and a.is_shared = ?";
 
 
 private static String SQL_GET_RATE_PLAN_BY_ID = "select  "+
		 										 "a.zone_set_id                , "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.payment_type               , "+
												 "a.product_id                 , "+
												 "a.tier_criteria              , "+
												 "a.expire_included            , "+
												 "a.access_fee_duration_months , "+
												 "a.override_roaming_included  , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.description                , "+
												 "a.special_notes              , "+
												 "a.carrier_rate_plan_id       , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
												 "a.included_period_months     , "+
												 "a.device_policy_id 		   , "+
												 "a.packet_rounding_policy_id  , "+
												 "a.created_by				   , "+
												 "a.last_modified_by		   , "+
												 //cancellation fee related changes here 
												 "a.PRO_RATE_CANCELLATION_FEE , "+
												 "a.CONTRACT_TERM			   ," +
												 "a.CANC_FEE_REDUCTION_INT, "    +
												 "a.AUTO_CANCEL_AT_TERM_END,"    +
												 "a.ACC_DAYS_PROV_STAT,"         +
												 "a.ACC_DAYS_SUSPEND_STATE,"      +
                                                                                                 "a.INCL_ROAM_FLG,"      +
                                                                                                 "a.TECHNOLOGY,"      +
                                                                                                 "a.WATERFALL_ENABLED,"      +
                                                                                                 "a.IS_WHOLESALE,"      +
                                                                                                 "a.WHOLESALE_RATE_PLAN_ID,"      +
												 //end
												 "b.access_fee                 , "+
												 "b.activation_fee             , "+
												 "b.suspend_fee                , "+
												 "b.unsuspend_fee              , "+
												 "b.deactivation_fee           , "+
												 "b.reactivation_fee           , "+
												 "c.prov_mt_sms_cost           , "+
												 "c.prov_mt_voice_cost         , "+
												 "c.prov_mo_sms_cost           , "+
												 "c.prov_mo_voice_cost         , "+
												 "c.prov_packet_cost	       , "+
												 "c.susp_mt_sms_cost	       , "+
												 "c.susp_mt_voice_cost         , "+
												 "c.susp_mo_sms_cost           , "+
												 "c.susp_mo_voice_cost         , "+
												 "c.susp_packet_cost           , "+
												 "c.prov_duration              , "+
												 "c.prov_duration_unit         , "+
												 "c.prov_packet_threshold      , "+
												 "c.prov_packet_threshold_unit , "+
												 "c.prov_mo_sms_threshold      , "+
												 "c.prov_mt_sms_threshold      , "+
												 "c.prov_mo_voice_threshold    , "+
												 "c.prov_mt_voice_threshold    , "+
												 "c.prov_traffic_days          , "+
												 "c.susp_duration              , "+
												 "c.susp_duration_unit         , "+
												 "c.susp_packet_threshold      , "+
												 "c.susp_packet_threshold_unit , "+
												 "c.susp_mo_sms_threshold      , "+
												 "c.susp_mt_sms_threshold      , "+
												 "c.susp_mo_voice_threshold    , "+
												 "c.susp_mt_voice_threshold    , "+
												 "c.susp_traffic_days          , "+
												 "RTRIM(replace(replace(xmlagg (xmlelement (e, d.tier_id || ';' || d.tier_rate_plan_id || ';' || d.start_count || ';' "+
												 "|| d.end_count || ';' || d.in_use || ',')),'<E>'),'</E>'), ',') as tiers, "+
												 "RTRIM(replace(replace(xmlagg (xmlelement (e, e.zone_id || ';'|| e.mo_sms_count_included || ';' || e.mt_sms_count_included "+
												 "|| ';' || e.mo_voice_mins_included || ';' || e.mt_voice_mins_included || ';' || e.packet_kb_included "+
												 "|| ';' || e.mt_sms_cost || ';' || e.mt_voice_cost || ';' || e.mo_sms_cost || ';' || e.mo_voice_cost "+
												 "|| ';' || e.packet_cost || ',')),'<E>'),'</E>'), ',') as zone_overrides "+
												 "from rate_plan a "+
												 "left join rate_plan_fee b "+
												 "     on a.rate_plan_id = b.rate_plan_id "+
												 "left join rate_plan_trigger c "+
												 "     on a.rate_plan_id = c.rate_plan_id "+
												 "left join rate_plan_tier d "+
												 "     on a.rate_plan_id = d.rate_plan_id "+
												 "left join rate_plan_zone_rating_policy e "+
												 "     on a.rate_plan_id = e.rate_plan_id "+
												 "where a.rate_plan_id = ? " + 
												 "group by "+
												 "( "+
												 "a.zone_set_id                , "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.payment_type               , "+
												 "a.product_id                 , "+
												 "a.tier_criteria              , "+
												 "a.expire_included            , "+
												 "a.access_fee_duration_months , "+
												 "a.override_roaming_included  , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.description                , "+
												 "a.special_notes              , "+
												 "a.carrier_rate_plan_id       , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
												 "a.included_period_months     , "+
												 "a.device_policy_id 		   , "+
												 "a.packet_rounding_policy_id  , "+
												 "a.created_by				   , "+
												 "a.last_modified_by		   , "+
												 "a.PRO_RATE_CANCELLATION_FEE , "+
												 "a.CONTRACT_TERM			   ," +
												 "a.CANC_FEE_REDUCTION_INT, "    +
												 "a.AUTO_CANCEL_AT_TERM_END,"    +
												 "a.ACC_DAYS_PROV_STAT,"         +
												 "a.ACC_DAYS_SUSPEND_STATE,"      +
                                                                                                 "a.INCL_ROAM_FLG,"              +
                                                                                                 "a.TECHNOLOGY,"              +
                                                                                                 "a.WATERFALL_ENABLED,"           +
                                                                                                 "a.IS_WHOLESALE,"      +
                                                                                                 "a.WHOLESALE_RATE_PLAN_ID," +
												 "b.access_fee                 , "+
												 "b.activation_fee             , "+
												 "b.suspend_fee                , "+
												 "b.unsuspend_fee              , "+
												 "b.deactivation_fee           , "+
												 "b.reactivation_fee           , "+
												 "c.prov_mt_sms_cost           , "+
												 "c.prov_mt_voice_cost         , "+
												 "c.prov_mo_sms_cost           , "+
												 "c.prov_mo_voice_cost         , "+
												 "c.prov_packet_cost	       , "+
												 "c.susp_mt_sms_cost	       , "+
												 "c.susp_mt_voice_cost         , "+
												 "c.susp_mo_sms_cost           , "+
												 "c.susp_mo_voice_cost         , "+
												 "c.susp_packet_cost           , "+
												 "c.prov_duration              , "+
												 "c.prov_duration_unit         , "+
												 "c.prov_packet_threshold      , "+
												 "c.prov_packet_threshold_unit , "+
												 "c.prov_mo_sms_threshold      , "+
												 "c.prov_mt_sms_threshold      , "+
												 "c.prov_mo_voice_threshold    , "+
												 "c.prov_mt_voice_threshold    , "+
												 "c.prov_traffic_days          , "+
												 "c.susp_duration              , "+
												 "c.susp_duration_unit         , "+
												 "c.susp_packet_threshold      , "+
												 "c.susp_packet_threshold_unit , "+
												 "c.susp_mo_sms_threshold      , "+
												 "c.susp_mt_sms_threshold      , "+
												 "c.susp_mo_voice_threshold    , "+
												 "c.susp_mt_voice_threshold    , "+
												 "c.susp_traffic_days            "+
												 ")"; 

 private static String SQL_GET_SUB_RATE_PLANS =  "select distinct "+
		 										 "a.zone_set_id                , "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.payment_type               , "+
												 "a.product_id                 , "+
												 "a.tier_criteria              , "+
												 "a.expire_included            , "+
												 "a.access_fee_duration_months , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.description                , "+
												 "a.special_notes              , "+
												 "a.carrier_rate_plan_id       , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
												 "a.included_period_months     , "+
												 "a.device_policy_id 		   , "+
												 "a.packet_rounding_policy_id  , "+
												 "a.PRO_RATE_CANCELLATION_FEE , "+
												 "a.CONTRACT_TERM			   ," +
												 "a.CANC_FEE_REDUCTION_INT, "    +
												 "a.AUTO_CANCEL_AT_TERM_END,"    +
												 "a.ACC_DAYS_PROV_STAT,"         +
												 "a.ACC_DAYS_SUSPEND_STATE,"      +
                                                 "a.INCL_ROAM_FLG,"      +
                                                 "a.TECHNOLOGY,"              +
                                                 "a.waterfall_enabled,"           +
												 "c.access_fee                 , "+
												 "c.activation_fee             , "+
												 "c.suspend_fee                , "+
												 "c.unsuspend_fee              , "+
												 "c.deactivation_fee           , "+
												 "c.reactivation_fee           , "+
												 "d.prov_mt_sms_cost           , "+
												 "d.prov_mt_voice_cost         , "+
												 "d.prov_mo_sms_cost           , "+
												 "d.prov_mo_voice_cost         , "+
												 "d.prov_packet_cost	       , "+
												 "d.susp_mt_sms_cost	       , "+
												 "d.susp_mt_voice_cost         , "+
												 "d.susp_mo_sms_cost           , "+
												 "d.susp_mo_voice_cost         , "+
												 "d.susp_packet_cost           , "+
												 "d.prov_duration              , "+
												 "d.prov_duration_unit         , "+
												 "d.prov_packet_threshold      , "+
												 "d.prov_packet_threshold_unit , "+
												 "d.prov_mo_sms_threshold      , "+
												 "d.prov_mt_sms_threshold      , "+
												 "d.prov_mo_voice_threshold    , "+
												 "d.prov_mt_voice_threshold    , "+
												 "d.prov_traffic_days          , "+
												 "d.susp_duration              , "+
												 "d.susp_duration_unit         , "+
												 "d.susp_packet_threshold      , "+
												 "d.susp_packet_threshold_unit , "+
												 "d.susp_mo_sms_threshold      , "+
												 "d.susp_mt_sms_threshold      , "+
												 "d.susp_mo_voice_threshold    , "+
												 "d.susp_mt_voice_threshold    , "+
												 "d.susp_traffic_days          , "+
												 "RTRIM(replace(replace (xmlagg (xmlelement (e, e.tier_id || ';' || e.tier_rate_plan_id || ';' || e.start_count || ';' "+
												 "|| e.end_count || ';' || e.in_use || ',')),'<E>'),'</E>'), ',') as tiers, "+
												 "RTRIM(replace(replace (xmlagg (xmlelement (e, f.zone_id || ';'|| f.mo_sms_count_included || ';' || f.mt_sms_count_included "+
												 "|| ';' || f.mo_voice_mins_included || ';' || f.mt_voice_mins_included || ';' || f.packet_kb_included "+
												 "|| ';' || f.mt_sms_cost || ';' || f.mt_voice_cost || ';' || f.mo_sms_cost || ';' || f.mo_voice_cost "+
												 "|| ';' || f.packet_cost || ',')),'<E>'),'</E>'), ',') as zone_overrides "+
												 "from rate_plan a "+
												 "inner join rate_plan_tier b "+
												 "     on a.rate_plan_id = b.tier_rate_plan_id "+
												 "left join rate_plan_fee c "+
												 "     on a.rate_plan_id = c.rate_plan_id "+
												 "left join rate_plan_trigger d "+
												 "     on a.rate_plan_id = d.rate_plan_id "+
												 "left join rate_plan_tier e "+
												 "     on a.rate_plan_id = e.rate_plan_id "+
												 "left join rate_plan_zone_rating_policy f "+
												 "     on a.rate_plan_id = f.rate_plan_id "+
												 "where b.rate_plan_id = ? and a.rate_plan_type = 3 "+
												 "group by "+
												 "( "+
												 "a.zone_set_id                , "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.payment_type               , "+
												 "a.product_id                 , "+
												 "a.tier_criteria              , "+
												 "a.expire_included            , "+
												 "a.access_fee_duration_months , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.description                , "+
												 "a.special_notes              , "+
												 "a.carrier_rate_plan_id       , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
												 "a.included_period_months     , "+
												 "a.device_policy_id 		   , "+
												 "a.packet_rounding_policy_id  , "+
												 "a.PRO_RATE_CANCELLATION_FEE , "+
												 "a.CONTRACT_TERM			   ," +
												 "a.CANC_FEE_REDUCTION_INT, "    +
												 "a.AUTO_CANCEL_AT_TERM_END,"    +
												 "a.ACC_DAYS_PROV_STAT,"         +
												 "a.ACC_DAYS_SUSPEND_STATE,"      +
                                                 "a.INCL_ROAM_FLG,"      +
                                                 "a.TECHNOLOGY,"              +
                                                 "a.waterfall_enabled,"              +
												 "c.access_fee                 , "+
												 "c.activation_fee             , "+
												 "c.suspend_fee                , "+
												 "c.unsuspend_fee              , "+
												 "c.deactivation_fee           , "+
												 "c.reactivation_fee           , "+
												 "d.prov_mt_sms_cost           , "+
												 "d.prov_mt_voice_cost         , "+
												 "d.prov_mo_sms_cost           , "+
												 "d.prov_mo_voice_cost         , "+
												 "d.prov_packet_cost	       , "+
												 "d.susp_mt_sms_cost	       , "+
												 "d.susp_mt_voice_cost         , "+
												 "d.susp_mo_sms_cost           , "+
												 "d.susp_mo_voice_cost         , "+
												 "d.susp_packet_cost           , "+
												 "d.prov_duration              , "+
												 "d.prov_duration_unit         , "+
												 "d.prov_packet_threshold      , "+
												 "d.prov_packet_threshold_unit , "+
												 "d.prov_mo_sms_threshold      , "+
												 "d.prov_mt_sms_threshold      , "+
												 "d.prov_mo_voice_threshold    , "+
												 "d.prov_mt_voice_threshold    , "+
												 "d.prov_traffic_days          , "+
												 "d.susp_duration              , "+
												 "d.susp_duration_unit         , "+
												 "d.susp_packet_threshold      , "+
												 "d.susp_packet_threshold_unit , "+
												 "d.susp_mo_sms_threshold      , "+
												 "d.susp_mt_sms_threshold      , "+
												 "d.susp_mo_voice_threshold    , "+
												 "d.susp_mt_voice_threshold    , "+
												 "d.susp_traffic_days            "+
												 ")";

	private static final String SQL_GET_UNATTACHED_SUB_RATE_PLANS = "select distinct a.zone_set_id, " +
																	"a.rate_plan_id, " +
																	"a.rate_plan_label, " +
																	"a.rate_plan_name , " +
																	"a.rate_plan_type , " +
																	"a.payment_type, " +
																	"a.product_id, " +
																	"a.tier_criteria, " +
																	"a.expire_included, " +
																	"a.access_fee_duration_months , " +
																	"a.is_shared , " +
																	"a.home_zone_id, " +
																	"a.description, " +
																	"a.special_notes, " +
																	"a.carrier_rate_plan_id, " +
																	"a.account_id , " +
																	"a.status_id, " +
																	"a.start_date , " +
																	"a.end_date , " +
																	"a.currency_code, " +
																	"a.included_period_months," +
																	"a.device_policy_id, " +
																	"a.packet_rounding_policy_id, " +
																	"a.PRO_RATE_CANCELLATION_FEE , " +
																	"a.CONTRACT_TERM," +
																	"a.CANC_FEE_REDUCTION_INT, " +
																	"a.AUTO_CANCEL_AT_TERM_END," +
																	"a.ACC_DAYS_PROV_STAT," +
																	"a.ACC_DAYS_SUSPEND_STATE," +
																	"a.INCL_ROAM_FLG," +
																	"a.TECHNOLOGY," +
																	"a.waterfall_enabled," +
																	"c.access_fee ," +
																	" c.activation_fee, " +
																	"c.suspend_fee, " +
																	"c.unsuspend_fee, " +
																	"c.deactivation_fee ," +
																	"c.reactivation_fee, " +
																	"d.prov_mt_sms_cost, " +
																	"d.prov_mt_voice_cost, " +
																	"d.prov_mo_sms_cost, " +
																	"d.prov_mo_voice_cost, " +
																	"d.prov_packet_cost\t, " +
																	"d.susp_mt_sms_cost, " +
																	"d.susp_mt_voice_cost, " +
																	"d.susp_mo_sms_cost, " +
																	"d.susp_mo_voice_cost, " +
																	"d.susp_packet_cost, " +
																	"d.prov_duration, " +
																	"d.prov_duration_unit, " +
																	"d.prov_packet_threshold , " +
																	"d.prov_packet_threshold_unit , " +
																	"d.prov_mo_sms_threshold , " +
																	"d.prov_mt_sms_threshold , " +
																	"d.prov_mo_voice_threshold, " +
																	"d.prov_mt_voice_threshold, " +
																	"d.prov_traffic_days , " +
																	"d.susp_duration, " +
																	"d.susp_duration_unit, " +
																	"d.susp_packet_threshold , " +
																	"d.susp_packet_threshold_unit , " +
																	"d.susp_mo_sms_threshold , " +
																	"d.susp_mt_sms_threshold , " +
																	"d.susp_mo_voice_threshold, " +
																	"d.susp_mt_voice_threshold, " +
																	"d.susp_traffic_days , " +
																	"RTRIM(replace(replace (xmlagg (xmlelement (e, e.tier_id || ';' || e.tier_rate_plan_id || ';'" +
																	" || e.start_count || ';' || e.end_count || ';' || e.in_use || ',')),'<E>'),'</E>'),',') as tiers, " +
																	"RTRIM(replace(replace (xmlagg (xmlelement (e, f.zone_id || ';'|| f.mo_sms_count_included || ';' ||" +
																	" f.mt_sms_count_included || ';' || f.mo_voice_mins_included || ';' || f.mt_voice_mins_included || ';' || " +
																	" f.packet_kb_included || ';' || f.mt_sms_cost || ';' || f.mt_voice_cost || ';' || f.mo_sms_cost || ';' || f.mo_voice_cost || ';' || f.packet_cost || ',')),'<E>'),'</E>'), ',') as zone_overrides " +
																	" FROM RATE_PLAN a " +
																	"LEFT join rate_plan_tier b on a.rate_plan_id = b.tier_rate_plan_id" +
																	" left join rate_plan_fee c on a.rate_plan_id = c.rate_plan_id left " +
																	"join rate_plan_trigger d on a.rate_plan_id = d.rate_plan_id " +
																	"left join rate_plan_tier e on a.rate_plan_id = e.rate_plan_id " +
																	"left join rate_plan_zone_rating_policy f on a.rate_plan_id = f.rate_plan_id " +
																	"WHERE a.account_id = (SELECT account_id FROM rate_plan rp WHERE rp.rate_plan_id = ?) " +
                                                                    " AND a.product_id = (SELECT rp1.product_id FROM rate_plan rp1 WHERE rp1.rate_plan_id = ?)"+
			                                                        "AND a.rate_plan_id NOT IN (SELECT tier_rate_plan_id FROM rate_plan_tier) " +
																	" AND a.rate_plan_type = 3 " +
																	" group by ( a.zone_set_id, a.rate_plan_id, a.rate_plan_label, a.rate_plan_name , a.rate_plan_type , a.payment_type, a.product_id , a.tier_criteria, a.expire_included, a.access_fee_duration_months , a.is_shared, a.home_zone_id, a.description, a.special_notes, a.carrier_rate_plan_id, a.account_id , a.status_id, a.start_date , a.end_date , a.currency_code, a.included_period_months, a.device_policy_id, a.packet_rounding_policy_id, a.PRO_RATE_CANCELLATION_FEE , a.CONTRACT_TERM\t\t\t ,a.CANC_FEE_REDUCTION_INT, a.AUTO_CANCEL_AT_TERM_END,a.ACC_DAYS_PROV_STAT,a.ACC_DAYS_SUSPEND_STATE,a.INCL_ROAM_FLG,a.TECHNOLOGY,a.waterfall_enabled,c.access_fee , c.activation_fee , c.suspend_fee, c.unsuspend_fee, c.deactivation_fee, c.reactivation_fee, d.prov_mt_sms_cost, d.prov_mt_voice_cost, d.prov_mo_sms_cost, d.prov_mo_voice_cost, d.prov_packet_cost\t, d.susp_mt_sms_cost\t, d.susp_mt_voice_cost, d.susp_mo_sms_cost, d.susp_mo_voice_cost, d.susp_packet_cost, d.prov_duration, d.prov_duration_unit, d.prov_packet_threshold , d.prov_packet_threshold_unit , d.prov_mo_sms_threshold , d.prov_mt_sms_threshold , d.prov_mo_voice_threshold, d.prov_mt_voice_threshold, d.prov_traffic_days , d.susp_duration, d.susp_duration_unit, d.susp_packet_threshold , d.susp_packet_threshold_unit , d.susp_mo_sms_threshold , d.susp_mt_sms_threshold , d.susp_mo_voice_threshold, d.susp_mt_voice_threshold, d.susp_traffic_days)";

private static final String SQL_GET_RATE_PLANS_PAGINATION_CLAUSE = " where rnum between ? and ?";

private static final String SQL_GET_CARRIER_RATE_PLANS = "select rate_plan_id," +
															"carrier_id, " +
															"rate_plan_name, " +
															"start_date, " +
															"end_date, " +
															"plan_description, " +
															"initial_charge, " +
															"max_size_in_kb, " +
															"monthly_fee, " +
															"last_modified_date, " +
															"last_modified_by " +
															"from carrier_rate_plan " +
															"where carrier_id in :carrier_ids";


private static final String SQL_GET_CARRIER_RATE_PLANS_BY_CONTRACT = "select crp.rate_plan_id," +
		"crp.carrier_id, " +
		"crp.rate_plan_name, " +
		"crp.start_date, " +
		"crp.end_date, " +
		"crp.plan_description, " +
		"crp.initial_charge, " +
		"crp.max_size_in_kb, " +
		"crp.monthly_fee, " +
		"crp.last_modified_date, " +
		"crp.last_modified_by " +
		"from carrier_rate_plan crp join contract_crp cc on crp.RATE_PLAN_ID = cc.CRP_ID join ACCOUNT_CONTRACT ac on cc.CONTRACT_ID = ac.CONTRACT_ID  " +
		"where crp.carrier_id in :carrier_ids and ac.ACCOUNT_ID = ?";

	private static String SQL_GET_UNPOOLED_RATE_PLANS = "select distinct b.rate_plan_name " +
														"from account_rate_plan_v1 a, "+
														"csp_rate_plan b "+
														"where a.csp_rate_plan_id = b.rate_plan_id "+
														"and b.rate_plan_name not in "+
														"(select distinct rate_plan "+
														"from t_account_rate_pool_map c, t_pool d  "+
														"where c.rate_plan is not null "+
														"and c.pool_name  = d.pool_name "+
														"and c.account_id = ? "+
														"and c.valid_flag != 2 "+// 2 = expired rate plan pools
														"and d.pool_name != (select pool_name from aerbill_prov.t_pool where pool_id = ?)) "+
														"and a.account_id = ? "+
														"and a.end_date > sysgmtdate " +
														"and b.device_pooling_policy not in ( 'Rate Plan, Report Group', 'Report Group')";
	
	private static String SQL_GET_UNPOOLED_RATE_PLANS_ALL = "select distinct b.rate_plan_name " +
			"from account_rate_plan_v1 a, "+
			"csp_rate_plan b "+
			"where a.csp_rate_plan_id = b.rate_plan_id "+
			"and b.rate_plan_name not in "+
			"(select distinct rate_plan "+
			"from t_account_rate_pool_map c, t_pool d  "+
			"where c.rate_plan is not null "+
			"and c.pool_name  = d.pool_name "+
			"and c.account_id = ? "+
			"and c.valid_flag != 2 "+// 2 = expired rate plan pools
			") "+
			"and a.account_id = ? "+
			"and a.end_date > sysgmtdate " +
			"and b.device_pooling_policy not in ( 'Rate Plan, Report Group', 'Report Group')";
	

	private static String SQL_GET_UNPOOLED_RATE_PLANS_BY_PRODUCT = "select distinct b.rate_plan_name " +
																	"from account_rate_plan_v1 a, "+
																	"csp_rate_plan b, "+
																	"aerisgen.Rate_Plan e "+
																	"where a.csp_rate_plan_id = b.rate_plan_id "+
																	"and a.csp_rate_plan_id  = e.rate_plan_id "+
																	"and e.IS_WHOLESALE = 0 "+
																	"and b.rate_plan_name not in "+
																	"(select distinct rate_plan "+
																	"from t_account_rate_pool_map c, t_pool d  "+
																	"where c.rate_plan is not null "+
																	"and c.pool_name  = d.pool_name "+
																	"and c.account_id = ? "+
																	"and c.valid_flag != 2 "+// 2 = expired rate plan pools
																	"and d.pool_name != ( select pool_name from aerbill_prov.t_pool where pool_id = ? ) ) "+
																	"and a.account_id = ? and b.product_id = ? "+
																	"and a.end_date > sysgmtdate " +
																	"and b.device_pooling_policy not in ( 'Rate Plan, Report Group', 'Report Group', 'No Pooling')";
	
	private static String SQL_GET_UNPOOLED_RATE_PLANS_BY_PRODUCT_ALL = "select distinct b.rate_plan_name " +
			"from account_rate_plan_v1 a, "+
			"csp_rate_plan b, "+
			"aerisgen.Rate_Plan e "+
			"where a.csp_rate_plan_id = b.rate_plan_id "+
			"and a.csp_rate_plan_id  = e.rate_plan_id "+
			"and e.IS_WHOLESALE = 0 "+
			"and b.rate_plan_name not in "+
			"(select distinct rate_plan "+
			"from t_account_rate_pool_map c, t_pool d  "+
			"where c.rate_plan is not null "+
			"and c.pool_name  = d.pool_name "+
			"and c.account_id = ? "+
			"and c.valid_flag != 2 "+// 2 = expired rate plan pools
			") "+
			"and a.account_id = ? and b.product_id = ? "+
			"and a.end_date > sysgmtdate " +
			"and b.device_pooling_policy not in ( 'Rate Plan, Report Group', 'Report Group','No Pooling')";
  
	private static final String SQL_GET_NEXT_RATE_PLAN_ID = "select rate_id.NEXTVAL from dual";

	private static final String SQL_GET_NEXT_RATE_PLAN_TIER_ID = "select rate_plan_tier_seq.NEXTVAL from dual";
	
	private static final String SQL_CREATE_RATE_PLAN_INFO = "insert "+
															"into rate_plan "+
															"( "+
															"rate_plan_id              , "+
															"rate_plan_label           , "+
															"rate_plan_name            , "+
															"rate_plan_type            , "+
															"payment_type              , "+
															"product_id                , "+
															"tier_criteria             , "+
															"access_fee_duration_months, "+
															"expire_included           , "+
															"is_shared                 , "+
															"home_zone_id              , "+
															"description               , "+
															"special_notes             , "+
															"carrier_rate_plan_id      , "+
															"account_id                , "+
															"status_id                 , "+
															"override_roaming_included , "+
															"start_date                , "+
															"end_date                  , "+
															"currency_code             , "+
															"included_period_months    , "+
															"device_policy_id          , "+
															"packet_rounding_policy_id , "+
															"created_date              , "+
															"created_by                , "+
															"last_modified_date        , "+
															"last_modified_by          , "+
															"rep_timestamp             , "+
															"PRO_RATE_CANCELLATION_FEE , "+
															"CONTRACT_TERM	           , "+
															"CANC_FEE_REDUCTION_INT    , "+
															"AUTO_CANCEL_AT_TERM_END   , "+
															"ACC_DAYS_PROV_STAT        , "+
															"ACC_DAYS_SUSPEND_STATE    , "+
                                                                                                                        "INCL_ROAM_FLG             , "+
															"zone_set_id               , "+
                                                                                                                        "technology                , "+
                                                                                                                        "waterfall_enabled         , "+
                                                                                                                        "is_wholesale              , "+
                                                                                                                        "wholesale_rate_plan_id      "+
															") "+
															"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)";
	
    private static final String SQL_CREATE_RATE_PLAN_INFO_2 = "insert "+
															"into rate_plan "+
															"( "+
															"rate_plan_id              , "+
															"rate_plan_label           , "+
															"rate_plan_name            , "+
															"rate_plan_type            , "+
															"payment_type              , "+
															"product_id                , "+
															"tier_criteria             , "+
															"access_fee_duration_months, "+
															"expire_included           , "+
															"is_shared                 , "+
															"home_zone_id              , "+
															"description               , "+
															"special_notes             , "+
															"carrier_rate_plan_id      , "+
															"account_id                , "+
															"status_id                 , "+
															"override_roaming_included , "+
															"start_date                , "+
															"end_date                  , "+
															"currency_code             , "+
															"included_period_months    , "+
															"device_policy_id          , "+
															"packet_rounding_policy_id , "+
															"created_date              , "+
															"created_by                , "+
															"last_modified_date        , "+
															"last_modified_by          , "+
															"rep_timestamp             , "+
															"PRO_RATE_CANCELLATION_FEE , "+
															"CONTRACT_TERM	           , "+
															"CANC_FEE_REDUCTION_INT    , "+
															"AUTO_CANCEL_AT_TERM_END   , "+
															"ACC_DAYS_PROV_STAT        , "+
															"ACC_DAYS_SUSPEND_STATE    , "+
                                                                                                                        "INCL_ROAM_FLG             , "+
															"zone_set_id               , "+
                                                                                                                        "technology                , "+
                                                                                                                        "waterfall_enabled         , "+
                                                                                                                        "is_wholesale              , "+
                                                                                                                        "operator_id              , "+
                                                                                                                        "wholesale_rate_plan_id      "+
															") "+
															"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)";
	
    
	private static final String SQL_CREATE_RATE_PLAN_TRIGGERS = "insert "+
																"into rate_plan_trigger "+
																"( "+
																"rate_plan_id              , "+
																"prov_duration             , "+
																"prov_duration_unit        , "+
																"prov_packet_threshold     , "+
																"prov_packet_threshold_unit, "+
																"prov_mo_sms_threshold     , "+
																"prov_mt_sms_threshold     , "+
																"prov_mo_voice_threshold   , "+
																"prov_mt_voice_threshold   , "+
																"prov_traffic_days         , "+
																"susp_duration             , "+
																"susp_duration_unit        , "+
																"susp_packet_threshold     , "+
																"susp_packet_threshold_unit, "+
																"susp_mo_sms_threshold	   , "+
																"susp_mt_sms_threshold	   , "+
																"susp_mo_voice_threshold   , "+
																"susp_mt_voice_threshold   , "+
																"susp_traffic_days         , "+
																"prov_mt_sms_cost          , "+
																"prov_mt_voice_cost        , "+
																"prov_mo_sms_cost          , "+
																"prov_mo_voice_cost        , "+
																"prov_packet_cost	       , "+
																"susp_mt_sms_cost	       , "+
																"susp_mt_voice_cost        , "+
																"susp_mo_sms_cost          , "+
																"susp_mo_voice_cost        , "+
																"susp_packet_cost          , "+
																"created_date              , "+
																"created_by                , "+
																"last_modified_date        , "+
																"last_modified_by          , "+
																"rep_timestamp               "+
																") "+
																"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_CREATE_RATE_PLAN_FEES =  "insert "+
															 "into rate_plan_fee"+
															 "( "+
															 "rate_plan_id              , "+
															 "access_fee                , "+
															 "activation_fee            , "+
															 "suspend_fee               , "+
															 "unsuspend_fee             , "+
															 "deactivation_fee          , "+
															 "reactivation_fee          , "+
															 "created_date              , "+
															 "created_by                , "+
															 "last_modified_date        , "+
															 "last_modified_by          , "+
															 "rep_timestamp               "+
															 ") "+
															 "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String SQL_CREATE_RATE_PLAN_TIERS = "insert "+
															 "into rate_plan_tier"+
															 "( "+
															 "tier_id            , "+
															 "rate_plan_id       , "+
															 "start_count        , "+
															 "end_count          , "+
															 "tier_rate_plan_id  , "+
															 "in_use             , "+
															 "created_date       , "+
															 "created_by         , "+
															 "last_modified_date , "+
															 "last_modified_by   , "+
															 "rep_timestamp        "+
															 ") "+
															 "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_UPDATE_RATE_PLAN_INFO = "update rate_plan set "+
															"rate_plan_label            = ?, "+
															"rate_plan_type             = ?, "+
															"payment_type               = ?, "+
															"product_id                 = ?, "+
															"tier_criteria              = ?, "+
															"access_fee_duration_months = ?, "+
															"expire_included            = ?, "+
															"is_shared                  = ?, "+
															"home_zone_id               = ?, "+
															"description                = ?, "+
															"special_notes              = ?, "+
															"carrier_rate_plan_id       = ?, "+
															"account_id                 = ?, "+
															"status_id                  = ?, "+
															"override_roaming_included  = ?, "+
															"start_date                 = ?, "+
															"end_date                   = ?, "+
															"currency_code              = ?, "+
															"included_period_months     = ?, "+
															"device_policy_id     		= ?, "+
															"packet_rounding_policy_id  = ?, "+
															"last_modified_date         = ?, "+
															"last_modified_by 	        = ?, "+
															"rep_timestamp   	        = ?, "+
															"PRO_RATE_CANCELLATION_FEE = ?, "+
															"CONTRACT_TERM			   = ?," +
															"CANC_FEE_REDUCTION_INT = ?, "    +
															"AUTO_CANCEL_AT_TERM_END = ?,"    +
															"ACC_DAYS_PROV_STAT = ?,"         +
															"ACC_DAYS_SUSPEND_STATE = ?,"      +
                                                            "INCL_ROAM_FLG = ? ,"      +
															"zone_set_id = ?,"			+
                                                            "TECHNOLOGY = ?, "      +
                                                            "waterfall_enabled = ?, "			+
                                                                                                                        "is_wholesale = ?, "			+
                                                                                                                        "wholesale_rate_plan_id = ?"			+
															"where rate_plan_id         = ?";

	private static final String SQL_UPDATE_RATE_PLAN_INFO_2 = "update rate_plan set "+
															"rate_plan_label            = ?, "+
															"rate_plan_type             = ?, "+
															"payment_type               = ?, "+
															"product_id                 = ?, "+
															"tier_criteria              = ?, "+
															"access_fee_duration_months = ?, "+
															"expire_included            = ?, "+
															"is_shared                  = ?, "+
															"home_zone_id               = ?, "+
															"description                = ?, "+
															"special_notes              = ?, "+
															"carrier_rate_plan_id       = ?, "+
															"account_id                 = ?, "+
															"status_id                  = ?, "+
															"override_roaming_included  = ?, "+
															"start_date                 = ?, "+
															"end_date                   = ?, "+
															"currency_code              = ?, "+
															"included_period_months     = ?, "+
															"device_policy_id     		= ?, "+
															"packet_rounding_policy_id  = ?, "+
															"last_modified_date         = ?, "+
															"last_modified_by 	        = ?, "+
															"rep_timestamp   	        = ?, "+
															"PRO_RATE_CANCELLATION_FEE = ?, "+
															"CONTRACT_TERM			   = ?," +
															"CANC_FEE_REDUCTION_INT = ?, "    +
															"AUTO_CANCEL_AT_TERM_END = ?,"    +
															"ACC_DAYS_PROV_STAT = ?,"         +
															"ACC_DAYS_SUSPEND_STATE = ?,"      +
                                                            "INCL_ROAM_FLG = ? ,"      +
															"zone_set_id = ?,"			+
                                                            "TECHNOLOGY = ?, "      +
                                                            "waterfall_enabled = ?, "			+
                                                                                                                        "is_wholesale = ?, "			+
                                                                                                                        "operator_id = ?, "			+
                                                                                                                        "wholesale_rate_plan_id = ?"			+
															"where rate_plan_id         = ?";

    
    private static final String SQL_UPDATE_RATE_PLAN_TRIGGERS = "update rate_plan_trigger set "+
																"prov_duration              = ?, "+
																"prov_duration_unit         = ?, "+
																"prov_packet_threshold      = ?, "+
																"prov_packet_threshold_unit = ?, "+
																"prov_mo_sms_threshold      = ?, "+
																"prov_mt_sms_threshold      = ?, "+
																"prov_mo_voice_threshold    = ?, "+
																"prov_mt_voice_threshold    = ?, "+
																"prov_traffic_days          = ?, "+
																"susp_duration              = ?, "+
																"susp_duration_unit         = ?, "+
																"susp_packet_threshold      = ?, "+
																"susp_packet_threshold_unit = ?, "+
																"susp_mo_sms_threshold	    = ?, "+
																"susp_mt_sms_threshold	    = ?, "+
																"susp_mo_voice_threshold    = ?, "+
																"susp_mt_voice_threshold    = ?, "+
																"susp_traffic_days          = ?, "+
																"prov_mt_sms_cost           = ?, "+
																"prov_mt_voice_cost         = ?, "+
																"prov_mo_sms_cost           = ?, "+
																"prov_mo_voice_cost         = ?, "+
																"prov_packet_cost	        = ?, "+
																"susp_mt_sms_cost	        = ?, "+
																"susp_mt_voice_cost         = ?, "+
																"susp_mo_sms_cost           = ?, "+
																"susp_mo_voice_cost         = ?, "+
																"susp_packet_cost           = ?, "+
																"last_modified_date         = ?, "+
																"last_modified_by 	        = ?, "+
																"rep_timestamp   	        = ? "+
																"where rate_plan_id         = ?";
	
	private static final String SQL_UPDATE_RATE_PLAN_FEES =  "update rate_plan_fee set "+
															 "access_fee                 = ?, "+
															 "activation_fee             = ?, "+
															 "suspend_fee                = ?, "+
															 "unsuspend_fee              = ?, "+
															 "deactivation_fee           = ?, "+
															 "reactivation_fee           = ?, "+
															 "last_modified_date         = ?, "+
															 "last_modified_by 	         = ?, "+
															 "rep_timestamp   	         = ? "+
															 "where rate_plan_id         = ?";
	
	private static final String SQL_UPDATE_RATE_PLAN_STATUS = 	"update rate_plan set "+
																"status_id                  = ?, "+
																"special_notes              = ?, "+
																"last_modified_date         = ?, "+
																"last_modified_by 	        = ?, "+
																"rep_timestamp   	        = ? "+
																"where rate_plan_id         = ?";
	
	private static final String SQL_UPDATE_RATE_PLAN_STATUS_WITH_END_DATE = "update rate_plan set "+
																			"status_id                  = ?, "+
																			"end_date                   = ?, "+
																			"last_modified_date         = ?, "+
																			"last_modified_by 	        = ?, "+
																			"rep_timestamp   	        = ? "+
																			"where rate_plan_id         = ?";

	private static final String SQL_CREATE_RATE_PLAN_ZONE_CONFIG = "INSERT INTO rate_plan_zone_rating_policy " 
																	+ "(rate_plan_id, " 
																	+ "zone_id, " 
																	+ "mo_sms_count_included, "
																	+ "mt_sms_count_included, " 
																	+ "mo_voice_mins_included, " 
																	+ "mt_voice_mins_included, " 
																	+ "packet_kb_included, " 
																	+ "mt_sms_cost, " 
																	+ "mt_voice_cost, " 
																	+ "mo_sms_cost, " 
																	+ "mo_voice_cost, " 
																	+ "packet_cost, " 
																	+ "created_date, " 
																	+ "created_by, " 
																	+ "last_modified_date, " 
																	+ "last_modified_by, " 
																	+ "rep_timestamp) " 
																	+ "values "
																	+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String SQL_INVALIDATE_RATE_PLAN = 	"update csp_rate_plan set " +
															"end_date = ?, "+
															"last_modified_by = ?, "+
															"last_modified_date = ? "+
															"where rate_plan_id = ?";
    private static final String SQL_REACTIVATE_CSP_RATE_PLAN = 	"update csp_rate_plan set " +
															"end_date = ?, "+
															"last_modified_by = ?, "+
															"last_modified_date = ? "+
															"where rate_plan_id = ?";

	private static final String SQL_CREATE_CSP_RATE_PLAN_TO_ZONE_MAPPING = 	"insert into csp_rate_plan_to_zone_map (" +
																			"rate_plan_id, " +
																			"zone_id, " +
																			"csp_zone, " +
																			"created_date, " +
																			"created_by, " +
																			"last_modified_date, " +
																			"last_modified_by, " +
																			"rep_timestamp)" +
																			"values (?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String SQL_DELETE_ACCOUNT_RATE_PLAN = "DELETE FROM aerbill_prov.account_rate_plan_v1 where account_id = ? and csp_rate_plan_id = ?";

	private static final String SQL_DELETE_CSP_RATE_PLAN_TO_ZONE_MAPPING = 	"delete from csp_rate_plan_to_zone_map where rate_plan_id = ?";
	
	private static final String SQL_DELETE_RATE_PLAN_FEES = "delete from rate_plan_fee where rate_plan_id = ?";
	
	private static final String SQL_DELETE_RATE_PLAN_TRIGGERS = "delete from rate_plan_trigger where rate_plan_id = ?";

	private static final String SQL_DELETE_RATE_PLAN_TIERS = "delete from rate_plan_tier where rate_plan_id = ?";

	private static final String SQL_DELETE_RATE_PLAN_ZONE_POLICY = "delete from rate_plan_zone_rating_policy where rate_plan_id = ?";

        private static String SQL_GET_WHOLESALE_RATE_PLANS =  "select  "+
		 										 "a.zone_set_id                , "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.payment_type               , "+
												 "a.product_id                 , "+
												 "a.tier_criteria              , "+
												 "a.expire_included            , "+
												 "a.access_fee_duration_months , "+
												 "a.override_roaming_included  , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.description                , "+
												 "a.special_notes              , "+
												 "a.carrier_rate_plan_id       , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
												 "a.included_period_months     , "+
												 "a.device_policy_id 		   , "+
												 "a.packet_rounding_policy_id  , "+
												 "a.created_by				   , "+
												 "a.last_modified_by		   , "+
												 //cancellation fee related changes here 
												 "a.PRO_RATE_CANCELLATION_FEE , "+
												 "a.CONTRACT_TERM			   ," +
												 "a.CANC_FEE_REDUCTION_INT, "    +
												 "a.AUTO_CANCEL_AT_TERM_END,"    +
												 "a.ACC_DAYS_PROV_STAT,"         +
												 "a.ACC_DAYS_SUSPEND_STATE,"      +
                                                                                                 "a.INCL_ROAM_FLG,"      +
                                                                                                 "a.TECHNOLOGY,"      +
                                                                                                 "a.WATERFALL_ENABLED,"      +
                                                                                                 "a.IS_WHOLESALE,"      +
                                                                                                 "a.WHOLESALE_RATE_PLAN_ID,"      +
												 //end
												 "b.access_fee                 , "+
												 "b.activation_fee             , "+
												 "b.suspend_fee                , "+
												 "b.unsuspend_fee              , "+
												 "b.deactivation_fee           , "+
												 "b.reactivation_fee           , "+
												 "c.prov_mt_sms_cost           , "+
												 "c.prov_mt_voice_cost         , "+
												 "c.prov_mo_sms_cost           , "+
												 "c.prov_mo_voice_cost         , "+
												 "c.prov_packet_cost	       , "+
												 "c.susp_mt_sms_cost	       , "+
												 "c.susp_mt_voice_cost         , "+
												 "c.susp_mo_sms_cost           , "+
												 "c.susp_mo_voice_cost         , "+
												 "c.susp_packet_cost           , "+
												 "c.prov_duration              , "+
												 "c.prov_duration_unit         , "+
												 "c.prov_packet_threshold      , "+
												 "c.prov_packet_threshold_unit , "+
												 "c.prov_mo_sms_threshold      , "+
												 "c.prov_mt_sms_threshold      , "+
												 "c.prov_mo_voice_threshold    , "+
												 "c.prov_mt_voice_threshold    , "+
												 "c.prov_traffic_days          , "+
												 "c.susp_duration              , "+
												 "c.susp_duration_unit         , "+
												 "c.susp_packet_threshold      , "+
												 "c.susp_packet_threshold_unit , "+
												 "c.susp_mo_sms_threshold      , "+
												 "c.susp_mt_sms_threshold      , "+
												 "c.susp_mo_voice_threshold    , "+
												 "c.susp_mt_voice_threshold    , "+
												 "c.susp_traffic_days          , "+
												 "rtrim (xmlagg (xmlelement (e, d.tier_id || ';' || d.tier_rate_plan_id || ';' || d.start_count || ';' "+
												 "|| d.end_count || ';' || d.in_use || ',')).extract ('//text()'), ',') as tiers, "+
												 "rtrim (xmlagg (xmlelement (e, e.zone_id || ';'|| e.mo_sms_count_included || ';' || e.mt_sms_count_included "+
												 "|| ';' || e.mo_voice_mins_included || ';' || e.mt_voice_mins_included || ';' || e.packet_kb_included "+
												 "|| ';' || e.mt_sms_cost || ';' || e.mt_voice_cost || ';' || e.mo_sms_cost || ';' || e.mo_voice_cost "+
												 "|| ';' || e.packet_cost || ',')).extract ('//text()'), ',') as zone_overrides "+
												 "from rate_plan a "+
												 "left join rate_plan_fee b "+
												 "     on a.rate_plan_id = b.rate_plan_id "+
												 "left join rate_plan_trigger c "+
												 "     on a.rate_plan_id = c.rate_plan_id "+
												 "left join rate_plan_tier d "+
												 "     on a.rate_plan_id = d.rate_plan_id "+
												 "left join rate_plan_zone_rating_policy e "+
												 "     on a.rate_plan_id = e.rate_plan_id "+
												 "where sysgmtdate between a.start_date and a.end_date "+
                                                                                                 "and a.is_wholesale = 1 "+
                                                                                                 "and a.account_id = ? "+
                                                                                                 "and a.product_id in :product_id_list"+
                                                                                                 "and a.status_id != 2"+ 
												 "group by "+
												 "( "+
												 "a.zone_set_id                , "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.payment_type               , "+
												 "a.product_id                 , "+
												 "a.tier_criteria              , "+
												 "a.expire_included            , "+
												 "a.access_fee_duration_months , "+
												 "a.override_roaming_included  , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.description                , "+
												 "a.special_notes              , "+
												 "a.carrier_rate_plan_id       , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
												 "a.included_period_months     , "+
												 "a.device_policy_id 		   , "+
												 "a.packet_rounding_policy_id  , "+
												 "a.created_by				   , "+
												 "a.last_modified_by		   , "+
												 "a.PRO_RATE_CANCELLATION_FEE , "+
												 "a.CONTRACT_TERM			   ," +
												 "a.CANC_FEE_REDUCTION_INT, "    +
												 "a.AUTO_CANCEL_AT_TERM_END,"    +
												 "a.ACC_DAYS_PROV_STAT,"         +
												 "a.ACC_DAYS_SUSPEND_STATE,"      +
                                                                                                 "a.INCL_ROAM_FLG,"              +
                                                                                                 "a.TECHNOLOGY,"              +
                                                                                                 "a.WATERFALL_ENABLED,"           +
                                                                                                 "a.IS_WHOLESALE,"      +
                                                                                                 "a.WHOLESALE_RATE_PLAN_ID," +
												 "b.access_fee                 , "+
												 "b.activation_fee             , "+
												 "b.suspend_fee                , "+
												 "b.unsuspend_fee              , "+
												 "b.deactivation_fee           , "+
												 "b.reactivation_fee           , "+
												 "c.prov_mt_sms_cost           , "+
												 "c.prov_mt_voice_cost         , "+
												 "c.prov_mo_sms_cost           , "+
												 "c.prov_mo_voice_cost         , "+
												 "c.prov_packet_cost	       , "+
												 "c.susp_mt_sms_cost	       , "+
												 "c.susp_mt_voice_cost         , "+
												 "c.susp_mo_sms_cost           , "+
												 "c.susp_mo_voice_cost         , "+
												 "c.susp_packet_cost           , "+
												 "c.prov_duration              , "+
												 "c.prov_duration_unit         , "+
												 "c.prov_packet_threshold      , "+
												 "c.prov_packet_threshold_unit , "+
												 "c.prov_mo_sms_threshold      , "+
												 "c.prov_mt_sms_threshold      , "+
												 "c.prov_mo_voice_threshold    , "+
												 "c.prov_mt_voice_threshold    , "+
												 "c.prov_traffic_days          , "+
												 "c.susp_duration              , "+
												 "c.susp_duration_unit         , "+
												 "c.susp_packet_threshold      , "+
												 "c.susp_packet_threshold_unit , "+
												 "c.susp_mo_sms_threshold      , "+
												 "c.susp_mt_sms_threshold      , "+
												 "c.susp_mo_voice_threshold    , "+
												 "c.susp_mt_voice_threshold    , "+
												 "c.susp_traffic_days            "+
												 ")";
        
        private static String SQL_GET_RESELLER_RETAIL_RATE_PLANS =  "select  "+
												 "a.rate_plan_id               , "+
												 "a.rate_plan_label            , "+
												 "a.rate_plan_name             , "+
												 "a.rate_plan_type             , "+
												 "a.product_id                 , "+
												 "a.is_shared                  , "+
												 "a.home_zone_id               , "+
												 "a.account_id                 , "+
												 "a.status_id                  , "+
												 "a.start_date                 , "+
												 "a.end_date                   , "+
												 "a.currency_code              , "+
												 "a.zone_set_id 	       , "+
												 "a.carrier_rate_plan_id       , "+
                                                                                                 "a.wholesale_rate_plan_id     , "+
                                                                                                 "a.is_wholesale               , "+
                                                                                                 "a.carrier_rate_plan_id       , "+
                                                                                                 "a.technology                   "+
												 "from rate_plan a               "+
												 "where sysgmtdate between a.start_date and a.end_date "+
                                                                                                 "and a.product_id in :product_id_list "+
                                                                                                 "and a.wholesale_rate_plan_id in "+
                                                                                                 "(select rate_plan_id "+
												 "from rate_plan "+
                                                                                                 "where is_wholesale=1 "+
												 "and account_id=?) ";
    
    private static String SQL_GET_SUB_RATE_PLANS_IN_USE = 	"select rpt.tier_rate_plan_id, rp.rate_plan_name from rate_plan_tier rpt join rate_plan rp "+
    														" on rpt.tier_rate_plan_id = rp.rate_plan_id where rpt.IN_USE = 1 and rpt.rate_plan_id in ";    
        
    
	@Inject
	private IProductCarrierDAO productCarrierDAO;
	
	@Inject
	private ICSPRatePlanDAO cspRatePlanDAO;
	
	@Inject
	@Named("AssignRatePlanToAccountStoredProcedure")
	private IStoredProcedure<ResultStatus> assignRatePlanToAccountStoredProcedure;
	
	@Inject
	@Named("ExpireAccountRatePlanStoredProcedure")
	private IStoredProcedure<ResultStatus> expireAccountRatePlanStoredProcedure;
	
	private Cache<Long, RatePlan> ratePlanCache;	
	private Cache<Long, Map<Long, AccountRatePlan>> accountRatePlanCache;
	
	@Inject
	private OAUtils oaUtils;

	@Inject
	private IZoneDAO zoneDAO;
	
	@Inject
	private ICspZoneEnum cspZoneCache;
    
    @javax.inject.Inject
	private IProductEnum productCache;
    
    @Inject
    private ApplicationProperties appProperties;
	
	public void init(@Hazelcast(cache = "RatePlanCache/id") Cache<Long, RatePlan> ratePlanCache, 
			@Hazelcast(cache = "AccountRatePlanCache/id") Cache<Long, Map<Long, AccountRatePlan>> accountRatePlanCache) {
		this.ratePlanCache = ratePlanCache;
		this.accountRatePlanCache = accountRatePlanCache;

		List<RatePlan> ratePlans = getAllRatePlans();
				
		// Put in Cache
		RatePlan[] ratePlanArr = new RatePlan[ratePlans.size()];
		ratePlans.toArray(ratePlanArr);
		
		putInCache(ratePlanArr);	
	}

	@Override
	public List<AccountRatePlan> getAccountRatePlans(int operatorId, long accountId, String start, String count,
			RatePlanStatus... rateplanStatusList) throws RatePlanException {
		List<AccountRatePlan> accountRatePlans = new ArrayList<AccountRatePlan>();
		
		// Fetch from cache if it is already stored is cache
		if(accountRatePlanCache.getKeys().contains(accountId))
		{
			return getCachedAccountRatePlans(accountId);
		}

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAllAccounts: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			ps = buildGetRatePlansByAccountIdQuery(conn, start, count, operatorId, accountId, rateplanStatusList);

			LOG.debug("getAllAccounts: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				AccountRatePlan accountRatePlan = new AccountRatePlan();
				accountRatePlan.setAccountRatePlanId(rs.getLong("account_rate_plan_id"));
				accountRatePlan.setAccountId(accountId);
				accountRatePlan.setStartDate(rs.getDate("arp_start_date"));
				accountRatePlan.setEndDate(rs.getDate("arp_end_date"));
				accountRatePlan.setCarrierRatePlanId(rs.getInt("carrier_plan_id"));
				
				RatePlan ratePlan = new RatePlan();
				
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name"));
				ratePlan.setStartDate(rs.getDate("start_date"));				
				ratePlan.setEndDate(rs.getDate("end_date"));				
				ratePlan.setCurrencyName("USD");			
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setContractTerm(rs.getInt("contract_term"));
				RatePlanFeeDetails feeDetails = createRatePlanFeeDetails(rs);				
				ratePlan.setRatePlanFeeDetails(feeDetails);

				// Uncomment after new schema is introduced
				//IncludedUsagePolicy usagePolicy = createIncludedUsagePolicy(rs);		
				//ratePlan.setIncludedUsagePolicy(usagePolicy);
				
				//ProvisionTriggerSettings provisionTriggerSettings = createProvisionTriggerSettings(rs);				
				//ratePlan.setProvisionTriggerSettings(provisionTriggerSettings);
				
				//SuspendTriggerSettings suspendTriggerSettings = createSuspendTriggerSettings(rs);				
				//ratePlan.setSuspendTriggerSettings(suspendTriggerSettings);
				
				// Uncomment after zone is introduced
				//ratePlan.setZoneRatePlanSettings(zoneRatePlanSettings);	
				
				// Set Rate Plan
				accountRatePlan.setRatePlan(ratePlan);
				
				accountRatePlans.add(accountRatePlan);
			}
			
			// Put in Cache
			putInCache(accountId, accountRatePlans);
		} catch (SQLException e) {
			LOG.error("SQLException during getAssignedRatePlans", e);
			throw new RatePlanException("DB Error while getting getAssignedRatePlans", e);
		} catch (Exception e) {
			LOG.error("ServiceException in getAssignedRatePlans" + e.getMessage());
			throw new RatePlanException("Unable to getAssignedRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAssignedRatePlans: Returned " + accountRatePlans.size() + " rate plans to the caller");

		return accountRatePlans;
	}

	private void putInCache(long accountId, List<AccountRatePlan> accountRatePlans) {
		Map<Long, AccountRatePlan> accountRatePlanMap = new HashMap<Long, AccountRatePlan>();
		
		for (AccountRatePlan accountRatePlan : accountRatePlans) {
			accountRatePlanMap.put(accountRatePlan.getAccountRatePlanId(), accountRatePlan);
		}
		
		accountRatePlanCache.put(accountId, accountRatePlanMap);
	}
	
	private void putInCache(long accountId, AccountRatePlan accountRatePlan) {
		Map<Long, AccountRatePlan> accountRatePlanMap = accountRatePlanCache.get(accountId);
		accountRatePlanMap.put(accountRatePlan.getAccountRatePlanId(), accountRatePlan);
				
		accountRatePlanCache.put(accountId, accountRatePlanMap);
	}
	
	private void putInCache(RatePlan... ratePlans) {
		for (RatePlan ratePlan : ratePlans) {
			ratePlanCache.put(ratePlan.getRatePlanId(), ratePlan);
		}
		List<Long> keys = ratePlanCache.getKeys();
	}
	
	private void evictFromCache(long accountId, long accountRatePlanId) {
		Map<Long, AccountRatePlan> accountRatePlanMap = accountRatePlanCache.get(accountId);
		accountRatePlanMap.remove(accountRatePlanId);
				
		accountRatePlanCache.put(accountId, accountRatePlanMap);
	}

	private List<AccountRatePlan> getCachedAccountRatePlans(long accountId) {
		List<AccountRatePlan> accountRatePlans = new ArrayList<AccountRatePlan>();
		Map<Long, AccountRatePlan> accountRatePlanMap = accountRatePlanCache.get(accountId);
		
		if(accountRatePlanMap != null)
		{
			accountRatePlans.addAll(accountRatePlanMap.values());
			
			// Sort by Account Rate Plan Id
			Collections.sort(accountRatePlans, new Comparator<AccountRatePlan>() {
				@Override
				public int compare(AccountRatePlan o1, AccountRatePlan o2) {
					return (int) (o1.getAccountRatePlanId() - o2.getAccountRatePlanId());
				}
			});
		}
		
		return accountRatePlans;
	}
	
	private RatePlanFeeDetails createRatePlanFeeDetails(ResultSet rs) throws SQLException
	{
		RatePlanFeeDetails feeDetails = new RatePlanFeeDetails();
		
		String accessFeePeriodType = rs.getString("access_fee_period_type");
		String accessFeeLengthStr = rs.getString("access_fee_period_length");
		int accessFeeMonths = 0;
		
		if(accessFeeLengthStr != null && NumberUtils.isNumber(accessFeeLengthStr.trim()))
		{
			int accessFeeLength = Integer.parseInt(accessFeeLengthStr.trim());
			
			if("Monthly".equalsIgnoreCase(accessFeePeriodType))
			{
				accessFeeMonths = accessFeeLength;
			}
			else if("Yearly".equalsIgnoreCase(accessFeePeriodType))
			{
				accessFeeMonths = accessFeeLength * 12;
			}
			else if("Daily".equalsIgnoreCase(accessFeePeriodType))
			{
				accessFeeMonths = accessFeeLength / 30;
			}
		}
			
		feeDetails.setAccessFee(rs.getDouble("rate_plan_access_fee"));
//		feeDetails.setAccessFeeMonths(accessFeeMonths);
		feeDetails.setActivationFee(rs.getDouble("first_bill_activation_fee"));
		feeDetails.setReactivationFee(rs.getDouble("reactivation_fee"));
		feeDetails.setSuspendFee(rs.getDouble("suspend_fee"));
		feeDetails.setUnsuspendFee(rs.getDouble("unsuspend_fee"));
		
		return feeDetails;
	}

	private PreparedStatement buildGetRatePlansByAccountIdQuery(Connection conn, String start, String count, int operatorId, long accountId, RatePlanStatus... ratePlanStatusList)
			throws SQLException {
		PreparedStatement ps;

		// Check if pagination parameters are valid
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);
		
		// Check if status is a search field
		boolean queryByStatus = ArrayUtils.isNotEmpty(ratePlanStatusList);
		
		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_ASSIGNED_RATE_PLANS);
				
		// Query if the status is used in the order search criteria
		// Uncomment when status column is introduced
//		if(queryByStatus)
//		{
//			// Build getOrders by Status query
//			StringBuffer inClause = new StringBuffer("(");
//			
//			for (int i = 0; i < ratePlanStatusList.length; i++) {
//				inClause.append("?");
//
//				if (i != ratePlanStatusList.length - 1) {
//					inClause.append(",");
//				}
//			}
//
//			inClause.append(")");
//			
//			String getOrdersWithStatusQuery = SQL_GET_CUST_ORDERS_BY_STATUS.replace(":status_list", inClause.toString());
//			
//			query = new StringBuilder(getOrdersWithStatusQuery);
//		}
		
		// Append Pagination parameters
		if (paginated) {
			query.append(SQL_GET_RATE_PLANS_PAGINATION_CLAUSE);
		}

		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		
		// Set Account Id
		ps.setLong(++i, accountId);
		
		// Uncomment when the operatorId is introduced
		// Set Operator Id
		//ps.setInt(++i, operatorId);
			
		// Set Status values
		// Uncomment when status column is introduced
//		if(queryByStatus)
//		{
//			int j = 0;
//			
//			// In clause for account ids
//			while (j < orderStatusList.length) {
//				String status = orderStatusList[j++].getValue();
//				ps.setString(++i, status);
//			}
//		}

		// paginate the results if required
		if (paginated) {
			long rownumStart = Long.parseLong(start);
			long rownumEnd = Long.parseLong(start) + Long.parseLong(count);

			ps.setLong(++i, rownumStart);
			ps.setLong(++i, rownumEnd);
		}

		return ps;
	}
	
	private PreparedStatement buildGetAccountRatePlanQuery(Connection conn, int operatorId, long accountId, long ratePlanId)
			throws SQLException {
		PreparedStatement ps;
		
		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_ASSIGNED_RATE_PLAN_BY_ID);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		
		// Set Account Id
		ps.setLong(++i, accountId);
		ps.setLong(++i, ratePlanId);

		return ps;
	}

	private PreparedStatement buildGetAllAssignedRatePlanQuery(Connection conn, long ratePlanId)
			throws SQLException {
		PreparedStatement ps;
		
		// Create Prepared Statement
		ps = conn.prepareStatement(SQL_GET_ASSIGNED_RATE_PLAN_ACROSS_ACCOUNTS_BY_ID);

		int i = 0;
		
		// Set ratePlanId
		ps.setLong(++i, ratePlanId);

		return ps;
	}
	
	private PreparedStatement buildGetRatePlansQuery(Connection conn, String start, String count, int operatorId, int[] productIds, RatePlanAccessType accessType, 
		RatePlanStatus... ratePlanStatusList) throws SQLException {
		PreparedStatement ps;

		// Check if pagination parameters are valid
		boolean paginated = NumberUtils.isNumber(start) && NumberUtils.isNumber(count);
		
		// Check if productIds is a search field
		boolean queryByProductIds = ArrayUtils.isNotEmpty(productIds);
		boolean queryByAccessType = accessType != null;
		
		// Query if the status is not in order search criteria
        StringBuilder query = new StringBuilder(SQL_GET_ALL_RATE_PLANS_EXCL_SUB_RATE_PLANS);
        if (appProperties.isRatePlanFilterByOperatorEnabled()) {
            query = new StringBuilder(SQL_GET_ALL_RATE_PLANS_EXCL_SUB_RATE_PLANS_2);
        } 
		
		// Query if the product ids and access type is used in the search criteria
		if(queryByProductIds && queryByAccessType)
		{
			// Build getOrders by Status query
			StringBuffer inClause = new StringBuffer("(");
			
			for (int i = 0; i < productIds.length; i++) {
				inClause.append("?");

				if (i != productIds.length - 1) {
					inClause.append(",");
				}
			}

			inClause.append(")");
            
            String getRatePlansByProductsQuery = SQL_GET_ALL_RATE_PLANS_BY_PRODUCT_AND_ACCESS_TYPE.replace(":product_id_list", inClause.toString());			
            if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                getRatePlansByProductsQuery = SQL_GET_ALL_RATE_PLANS_BY_PRODUCT_AND_ACCESS_TYPE_2.replace(":product_id_list", inClause.toString());
            } 
			
			query = new StringBuilder(getRatePlansByProductsQuery);
		}
		// Query if the product ids is used in the search criteria
		else if(queryByProductIds)
		{
			// Build getOrders by Status query
			StringBuffer inClause = new StringBuffer("(");
			
			for (int i = 0; i < productIds.length; i++) {
				inClause.append("?");

				if (i != productIds.length - 1) {
					inClause.append(",");
				}
			}

			inClause.append(")");
			
			String getRatePlansByProductsQuery = SQL_GET_ALL_RATE_PLANS_BY_PRODUCT.replace(":product_id_list", inClause.toString());
            
            if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                getRatePlansByProductsQuery = SQL_GET_ALL_RATE_PLANS_BY_PRODUCT_2.replace(":product_id_list", inClause.toString());
            }
			
			query = new StringBuilder(getRatePlansByProductsQuery);
		}
		// Query if the access type is used in the search criteria
		else if(queryByAccessType)
		{
			query = new StringBuilder(SQL_GET_ALL_RATE_PLANS_BY_ACCESS_TYPE);            
            if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                query = new StringBuilder(SQL_GET_ALL_RATE_PLANS_BY_ACCESS_TYPE_2);
            }
            
		}
		
		// Append Pagination parameters
		if (paginated) {
			query.append(SQL_GET_RATE_PLANS_PAGINATION_CLAUSE);
		}

		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;

		// Set Product Ids values
		if(queryByProductIds && queryByAccessType)
		{
			int j = 0;
			
			// In clause for account ids
			while (j < productIds.length) {
				int productId = productIds[j++];
				ps.setInt(++i, productId);
			}
			
			// Set shared or Exclusive rate plan
			ps.setInt(++i, accessType.getValue());
		}
		else if(queryByProductIds)
		{
			int j = 0;
			
			// In clause for account ids
			while (j < productIds.length) {
				int productId = productIds[j++];
				ps.setInt(++i, productId);
			}
		}
		else if(queryByAccessType)
		{
			// Set shared or Exclusive rate plan
			ps.setInt(++i, accessType.getValue());
		}

		// paginate the results if required
		if (paginated) {
			long rownumStart = Long.parseLong(start);
			long rownumEnd = Long.parseLong(start) + Long.parseLong(count);

			ps.setLong(++i, rownumStart);
			ps.setLong(++i, rownumEnd);
		}

		return ps;
	}
	
	private PreparedStatement buildGetAllRatePlansQuery(Connection conn) throws SQLException {
		PreparedStatement ps;
		
		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_ALL_RATE_PLANS);
        
        if (appProperties.isRatePlanFilterByOperatorEnabled()) {
            query = new StringBuilder(SQL_GET_ALL_RATE_PLANS_2);
        }
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		return ps;
	}
	
	private PreparedStatement buildGetRatePlanQuery(Connection conn, int operatorId, long ratePlanId)
			throws SQLException {
		PreparedStatement ps;
		
		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_RATE_PLAN_BY_ID);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		
		// Set Rate Plan Id
		ps.setLong(++i, ratePlanId);

		return ps;
	}
	
	private PreparedStatement buildGetSubRatePlansQuery(Connection conn, int operatorId, long tieredRatePlanId)
			throws SQLException {
		PreparedStatement ps;
		
		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_SUB_RATE_PLANS);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		
		// Set Rate Plan Id
		ps.setLong(++i, tieredRatePlanId);

		return ps;
	}

	private PreparedStatement buildGetUnattachedSubRatePlansQuery(Connection conn, int operatorId, long tieredRatePlanId)
			throws SQLException {
		PreparedStatement ps;

		// Query if the status is not in order search criteria
		StringBuilder query = new StringBuilder(SQL_GET_UNATTACHED_SUB_RATE_PLANS);

		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;

		// Set Rate Plan Id
		ps.setLong(++i, tieredRatePlanId);
		ps.setLong(++i, tieredRatePlanId);

		return ps;
	}
	
	@Override
	public List<RatePlan> getAllRatePlans(int operatorId, String start, String count, int[] productIds, RatePlanAccessType accessType, RatePlanStatus... rateplanStatusList) 
		throws RatePlanException {
		List<RatePlan> ratePlans = new ArrayList<RatePlan>();

		// Fetch from cache if it is already stored is cache
		if(!ratePlanCache.getValues().isEmpty())
		{
			return getCachedRatePlans(operatorId, start, count, productIds, accessType, rateplanStatusList);
		}
				
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAllRatePlans: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			ps = buildGetRatePlansQuery(conn, start, count, operatorId, productIds, accessType, rateplanStatusList);

			LOG.debug("getAllAccounts: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				RatePlan ratePlan = new RatePlan();
				
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name")); 
				ratePlan.setStartDate(rs.getDate("start_date"));				
				ratePlan.setEndDate(rs.getDate("end_date"));
				ratePlan.setCurrencyName(rs.getString("currency_code"));		
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setAccessType(RatePlanAccessType.fromValue(rs.getInt("is_shared")));
				ratePlan.setStatus(RatePlanStatus.fromValue(rs.getInt("status_id")));
				ratePlan.setRatePlanType(RatePlanType.fromValue(rs.getInt("rate_plan_type")));
				ratePlan.setZoneSetId(rs.getInt("zone_set_id"));
				if(ArrayUtils.isNotEmpty(productIds)){
                                    ratePlan.setWholesaleRateplanId(rs.getInt("wholesale_rate_plan_id"));
                                    ratePlan.setIsWholesaleRatePlan(BooleanUtils.toBoolean(rs.getInt("is_wholesale")));
                                }
                if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                    ratePlan.setOperatorId(rs.getInt("operator_id"));
                } 
				ratePlans.add(ratePlan);
			}
			
			// Put in Cache
			RatePlan[] ratePlanArr = new RatePlan[ratePlans.size()];
			ratePlans.toArray(ratePlanArr); 
			
			putInCache(ratePlanArr);			
		} catch (SQLException e) {
			LOG.error("Exception during getAllRatePlans", e);
			throw new RatePlanException("Unable to getAllRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAssignedRatePlans: Returned " + ratePlans.size() + " rate plans to the caller");

		return ratePlans;
	}

	private List<RatePlan> getCachedRatePlans(final int operatorId, String start, String count, int[] productIds, final RatePlanAccessType accessType, 
		RatePlanStatus... rateplanStatusList) {
		List<RatePlan> cachedRatePlans = new ArrayList<RatePlan>();
		long rownumStart = 0;
		long rownumEnd = 0;
		
		List<RatePlan> ratePlans = ratePlanCache.getValues();

		// Filter by product ids
		if (productIds != null && productIds.length > 0) {
			for (RatePlan ratePlan : ratePlans) {
				if(ratePlan != null)
				{
					int productId = ratePlan.getProductId();
                    
                    if(ArrayUtils.contains(productIds, productId))
					{
                        if (appProperties.isRatePlanFilterByOperatorEnabled() && ratePlan.getOperatorId() != operatorId) {
                            continue;
                        }
						cachedRatePlans.add(ratePlan);
					}
				}
			}
		} else {
			cachedRatePlans = new ArrayList<RatePlan>(ratePlans);
		}

		// Filter by access type		
		cachedRatePlans =  new ArrayList<RatePlan>(Collections2.filter(cachedRatePlans, new Predicate<RatePlan>() {
		@Override
			public boolean apply(RatePlan ratePlan) {
				boolean success = true;
			
				if(accessType != null) 
				{
					success = success && (ratePlan.getAccessType() == null || ratePlan.getAccessType().getValue() == accessType.getValue());
				}
				
				success = success && (ratePlan.getRatePlanType() != RatePlanType.SUB_RATE_PLAN);
				
				return success;
			}
		}));
		
		// Sort by Rate Plan Id
		Collections.sort(cachedRatePlans, new Comparator<RatePlan>() {
			@Override
			public int compare(RatePlan o1, RatePlan o2) {
				return (int) (o1.getRatePlanId() - o2.getRatePlanId());
			}
		});
		
		if (NumberUtils.isNumber(start) && NumberUtils.isNumber(count)) {
			rownumStart = Long.parseLong(start);
			rownumEnd = Long.parseLong(start) + Long.parseLong(count);
		}
		
		// Filter by status
		for (int i = 0; i < cachedRatePlans.size(); i++) {
			boolean success = true;
			RatePlan ratePlan = cachedRatePlans.get(i);

			for (RatePlanStatus ratePlanStatus : rateplanStatusList) {
				success = success && (ratePlanStatus == ratePlan.getStatus());

				if (success) {
					break;
				}
			}

			if (rownumStart > 0 && rownumEnd > rownumStart) {
				success = success && rownumStart <= i && i <= rownumEnd;
			}

			if (!success) {
				cachedRatePlans.remove(ratePlan);
			}
		}
		
		return cachedRatePlans;
	}

	@Override
	public RatePlan getRatePlan(int operatorId, long ratePlanId) throws RatePlanException, RatePlanNotFoundException {
		RatePlan ratePlan = null;				
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getRatePlan: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts provides pagination support
			ps = buildGetRatePlanQuery(conn, operatorId, ratePlanId);

			LOG.debug("getRatePlan: executed query: " + ps);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				ratePlan = new RatePlan();

				ratePlan.setZoneSetId(rs.getInt("zone_set_id"));
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name"));
				ratePlan.setRatePlanType(RatePlanType.fromValue(rs.getInt("rate_plan_type")));
				ratePlan.setPaymentType(RatePlanPaymentType.fromValue(rs.getInt("payment_type")));
				ratePlan.setAccessType(RatePlanAccessType.fromValue(rs.getInt("is_shared")));
				ratePlan.setPeriodType(rs.getInt("access_fee_duration_months") == 1 ? RatePlanPeriodType.MONTHLY : RatePlanPeriodType.VARIABLE);
				ratePlan.setStartDate(rs.getDate("start_date"));				
				ratePlan.setEndDate(rs.getDate("end_date"));
				ratePlan.setCurrencyName(rs.getString("currency_code"));		
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setStatus(RatePlanStatus.fromValue(rs.getInt("status_id")));
				ratePlan.setAccessFeeMonths(rs.getInt("access_fee_duration_months"));
				ratePlan.setExpireIncluded(rs.getInt("expire_included") == 1);
				ratePlan.setOverrideRoamingIncluded(rs.getInt("override_roaming_included") == 1);
				
				ratePlan.setTierCriteria(rs.getInt("tier_criteria"));
				ratePlan.setHomeZoneId(rs.getInt("home_zone_id"));
				ratePlan.setDescription(rs.getString("description"));
				ratePlan.setSpecialNotes(rs.getString("special_notes"));
				
				ratePlan.setCarrierRatePlanId(rs.getInt("carrier_rate_plan_id"));
				ratePlan.setAccountId(rs.getInt("account_id"));
				ratePlan.setIncludedPeriodMonths(rs.getInt("included_period_months"));
				ratePlan.setDevicePoolingPolicyId(rs.getInt("device_policy_id"));
				ratePlan.setPacketDataRoundingPolicyId(rs.getInt("packet_rounding_policy_id"));
				ratePlan.setCreatedBy(rs.getString("created_by"));
				ratePlan.setLastModifiedBy(rs.getString("last_modified_by"));
                ratePlan.setRoamingIncluded(rs.getInt("INCL_ROAM_FLG") == 1);
                ratePlan.setTechnology(rs.getString("technology"));
				ratePlan.setWaterfallEnabled(BooleanUtils.toBoolean(rs.getInt("WATERFALL_ENABLED")));
				// Set Tiers info for a tiered rate plan
				if(ratePlan.getRatePlanType() == RatePlanType.TIERED)
				{
					setRatePlanTiers(ratePlan, rs);
				}
				
				setRatePlanFeeDetails(ratePlan, rs);
				
				// Set Provision and Suspend Triggers
				setRatePlanTriggers(ratePlan, rs);
				
				// Set Zone Overrides
				setZoneRatePlanSettings(ratePlan, rs);
				
				ratePlan.setOperatorId(operatorId);
				//add the cancellation fee details as well
				ratePlan.setAutoCancelAtTermEnd(rs.getInt("AUTO_CANCEL_AT_TERM_END"));
				ratePlan.setProRateCancellationFee(rs.getInt("PRO_RATE_CANCELLATION_FEE"));
				ratePlan.setCancellationFeeReductionInterval(rs.getInt("CANC_FEE_REDUCTION_INT"));
				ratePlan.setContractTerm(rs.getInt("CONTRACT_TERM"));
				ratePlan.setAccountForDaysInProvisionState(rs.getInt("ACC_DAYS_PROV_STAT"));
				ratePlan.setAccountForDaysInSuspendState(rs.getInt("ACC_DAYS_SUSPEND_STATE"));
                                ratePlan.setIsWholesaleRatePlan(BooleanUtils.toBoolean(rs.getInt("IS_WHOLESALE")));
                                ratePlan.setWholesaleRateplanId(rs.getInt("WHOLESALE_RATE_PLAN_ID"));
				//set rate plan overage
				setRatePlanOverageBucket(ratePlan, conn);
			}
			
			if(ratePlan == null)
			{
				throw new RatePlanNotFoundException("Rate Plan: "+ratePlanId+" not found");
			}
			
		} catch (SQLException e) {
			LOG.error("SQLException during getRatePlan", e);
			throw new RatePlanException("Unable to getRatePlan", e);
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException in getRatePlan" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAssignedRatePlans: Returned rate plan: " + ReflectionToStringBuilder.toString(ratePlan) + " to the caller");

		return ratePlan;
	}
	
	
	/**
	 * gets the list of associated rate plan overage buckets 
	 * 
	 * @param ratePlan
	 * @param conn
	 * @throws SQLException
	 */
	private void setRatePlanOverageBucket(RatePlan ratePlan, Connection conn) throws SQLException {
		List<RatePlanOverageBucket> accOverageBktList = new ArrayList<RatePlanOverageBucket>();
		
		String getAccOverageBkt = "select * from aerisgen.RATE_PLAN_OVERAGE_BUCKET where account_id = " + 
				ratePlan.getAccountId()  + " and rate_plan_id = " + ratePlan.getRatePlanId();
		if(ratePlan.getRatePlanType() != RatePlanType.TIERED) {
			getAccOverageBkt =  "select * from aerisgen.RATE_PLAN_OVERAGE_BUCKET where rate_plan_id = " + ratePlan.getRatePlanId();
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// Create Prepared Statement
			ps = conn.prepareStatement(getAccOverageBkt);
			LOG.info("RATE_PLAN_OVERAGE_BUCKETS: for accountId = "  + ratePlan.getAccountId() + " and ratePlanId = " + ratePlan.getRatePlanId());
			rs = ps.executeQuery();
			while(rs.next()) {
				RatePlanOverageBucket rpOverageBucket = new RatePlanOverageBucket();
				rpOverageBucket.setAccountId(ratePlan.getAccountId());
				rpOverageBucket.setAutoAdd(rs.getInt("auto_add"));
				rpOverageBucket.setBucketPrice(rs.getDouble("bucket_price"));
				rpOverageBucket.setBucketSize(rs.getInt("bucket_size"));
				rpOverageBucket.setBucketSizeType(rs.getString("bucket_size_type"));
				double overageSize = rpOverageBucket.getBucketSize();
				if(rpOverageBucket.getBucketSizeType() != null && rpOverageBucket.getBucketSizeType().equalsIgnoreCase("GB")){
					overageSize = overageSize / 1024 ;
					BigDecimal bDec = new BigDecimal(String.valueOf(overageSize)).setScale(2, BigDecimal.ROUND_HALF_UP);
					overageSize = bDec.doubleValue();
				}
				rpOverageBucket.setBucketSize(overageSize);
				rpOverageBucket.setNoAccessFeeDeviceCount(rs.getInt("no_access_fee_device_count"));
				rpOverageBucket.setStartDate(rs.getDate("start_date"));
				rpOverageBucket.setEndDate(rs.getDate("end_date"));
				rpOverageBucket.setRepTimeStamp(getGMTDateStr(rs.getTimestamp("rep_timestamp")));
				rpOverageBucket.setCreatedDate(rs.getDate("created_date"));
				rpOverageBucket.setUpdatedDate(rs.getDate("last_modified_date"));
				rpOverageBucket.setCreatedBy(rs.getString("created_by"));
				rpOverageBucket.setUpdatedBy(rs.getString("last_modified_by"));
				accOverageBktList.add(rpOverageBucket);
			}
			ratePlan.setRatePlanOverageBucket(accOverageBktList);
		} catch (SQLException e) {
			LOG.error("Exception during getAccountOverageBucket for accountId= " + ratePlan.getAccountId(), e);
			throw new GenericServiceException("Exception during getAccountOverageBucket", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		
	}

	@Override
	public List<RatePlan> getSubRatePlans(int operatorId, long tieredRatePlanId) throws RatePlanException {
		List<RatePlan> ratePlans = new ArrayList<RatePlan>();				
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getSubRatePlans: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts provides pagination support
			ps = buildGetSubRatePlansQuery(conn, operatorId, tieredRatePlanId);

			LOG.debug("getSubRatePlans: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				RatePlan ratePlan = new RatePlan();

				ratePlan.setZoneSetId(rs.getInt("zone_set_id"));
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name"));
				ratePlan.setRatePlanType(RatePlanType.fromValue(rs.getInt("rate_plan_type")));
				ratePlan.setPaymentType(RatePlanPaymentType.fromValue(rs.getInt("payment_type")));
				ratePlan.setAccessType(RatePlanAccessType.fromValue(rs.getInt("is_shared")));
				ratePlan.setPeriodType(rs.getInt("access_fee_duration_months") == 1 ? RatePlanPeriodType.MONTHLY : RatePlanPeriodType.VARIABLE);
				ratePlan.setStartDate(rs.getDate("start_date"));				
				ratePlan.setEndDate(rs.getDate("end_date"));
				ratePlan.setCurrencyName(rs.getString("currency_code"));		
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setStatus(RatePlanStatus.fromValue(rs.getInt("status_id")));
				ratePlan.setAccessFeeMonths(rs.getInt("access_fee_duration_months"));
				ratePlan.setExpireIncluded(rs.getInt("expire_included") == 1);
				
				ratePlan.setTierCriteria(rs.getInt("tier_criteria"));
				ratePlan.setHomeZoneId(rs.getInt("home_zone_id"));
				ratePlan.setDescription(rs.getString("description"));
				ratePlan.setSpecialNotes(rs.getString("special_notes"));
				
				ratePlan.setCarrierRatePlanId(rs.getInt("carrier_rate_plan_id"));
				ratePlan.setAccountId(rs.getInt("account_id"));
				
				// Set Fee Details for a non-tiered rate plan
				if(ratePlan.getRatePlanType() != RatePlanType.TIERED)
				{
					setRatePlanFeeDetails(ratePlan, rs);
				}
				// Set Tiers info for a tiered rate plan
				else
				{
					setRatePlanTiers(ratePlan, rs);
				}
				
				// Set Provision and Suspend Triggers
				setRatePlanTriggers(ratePlan, rs);
				
				// Set Zone Overrides
				setZoneRatePlanSettings(ratePlan, rs);
                ratePlan.setRoamingIncluded(rs.getInt("INCL_ROAM_FLG") == 1);
                ratePlan.setTechnology(rs.getString("technology"));
				ratePlan.setWaterfallEnabled(BooleanUtils.toBoolean(rs.getInt("waterfall_enabled")));
				// Add Sub Rate Plan
				ratePlans.add(ratePlan);
			}			
		} catch (SQLException e) {
			LOG.error("SQLException during getSubRatePlans", e);
			throw new RatePlanException("Unable to getSubRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getSubRatePlans: Returned rate plan: " + ReflectionToStringBuilder.toString(ratePlans) + " to the caller");

		return ratePlans;
	}

	@Override
	public List<RatePlan> getUnattachedSubRatePlans(int operatorId, long tieredRatePlanId) throws RatePlanException {
		List<RatePlan> ratePlans = new ArrayList<RatePlan>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getSubRatePlans: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts provides pagination support
			ps = buildGetUnattachedSubRatePlansQuery(conn, operatorId, tieredRatePlanId);

			LOG.debug("getSubRatePlans: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				RatePlan ratePlan = new RatePlan();

				ratePlan.setZoneSetId(rs.getInt("zone_set_id"));
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name"));
				ratePlan.setRatePlanType(RatePlanType.fromValue(rs.getInt("rate_plan_type")));
				ratePlan.setPaymentType(RatePlanPaymentType.fromValue(rs.getInt("payment_type")));
				ratePlan.setAccessType(RatePlanAccessType.fromValue(rs.getInt("is_shared")));
				ratePlan.setPeriodType(rs.getInt("access_fee_duration_months") == 1 ? RatePlanPeriodType.MONTHLY : RatePlanPeriodType.VARIABLE);
				ratePlan.setStartDate(rs.getDate("start_date"));
				ratePlan.setEndDate(rs.getDate("end_date"));
				ratePlan.setCurrencyName(rs.getString("currency_code"));
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setStatus(RatePlanStatus.fromValue(rs.getInt("status_id")));
				ratePlan.setAccessFeeMonths(rs.getInt("access_fee_duration_months"));
				ratePlan.setExpireIncluded(rs.getInt("expire_included") == 1);

				ratePlan.setTierCriteria(rs.getInt("tier_criteria"));
				ratePlan.setHomeZoneId(rs.getInt("home_zone_id"));
				ratePlan.setDescription(rs.getString("description"));
				ratePlan.setSpecialNotes(rs.getString("special_notes"));

				ratePlan.setCarrierRatePlanId(rs.getInt("carrier_rate_plan_id"));
				ratePlan.setAccountId(rs.getInt("account_id"));

				// Set Fee Details for a non-tiered rate plan
				if(ratePlan.getRatePlanType() != RatePlanType.TIERED)
				{
					setRatePlanFeeDetails(ratePlan, rs);
				}
				// Set Tiers info for a tiered rate plan
				else
				{
					setRatePlanTiers(ratePlan, rs);
				}

				// Set Provision and Suspend Triggers
				setRatePlanTriggers(ratePlan, rs);

				// Set Zone Overrides
				setZoneRatePlanSettings(ratePlan, rs);
				ratePlan.setRoamingIncluded(rs.getInt("INCL_ROAM_FLG") == 1);
				ratePlan.setTechnology(rs.getString("technology"));
				ratePlan.setWaterfallEnabled(BooleanUtils.toBoolean(rs.getInt("waterfall_enabled")));
				// Add Sub Rate Plan
				ratePlans.add(ratePlan);
			}
		} catch (SQLException e) {
			LOG.error("SQLException during getSubRatePlans", e);
			throw new RatePlanException("Unable to getSubRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getSubRatePlans: Returned rate plan: " + ReflectionToStringBuilder.toString(ratePlans) + " to the caller");

		return ratePlans;
	}

	private void setZoneRatePlanSettings(RatePlan ratePlan, ResultSet rs) throws SQLException {
		List<ZoneRatePlan> zoneRatePlanSettings = new ArrayList<ZoneRatePlan>();
		List<Integer> zoneIds = new ArrayList<Integer>();
		
		String[] zoneProfilesStr = rs.getString("zone_overrides").split(",");
		
		for (String zoneProfile : zoneProfilesStr) {
			String[] zoneAttributes = zoneProfile.split(";");
			
			if(zoneAttributes.length > 0 && NumberUtils.isNumber(zoneAttributes[0]))
			{
				int zoneId = convertStringToInt(zoneAttributes[0]);
				
				if(!zoneIds.contains(zoneId))
				{
					ZoneRatePlan zoneRatePlan = new ZoneRatePlan();
					
					IncludedPlanDetails includedPlanDetails = new IncludedPlanDetails();
					includedPlanDetails.setIncludedMOSms(convertStringToInt(zoneAttributes[1]));
					includedPlanDetails.setIncludedMTSms(convertStringToInt(zoneAttributes[2]));
					includedPlanDetails.setIncludedMOVoiceMins(convertStringToInt(zoneAttributes[3]));
					includedPlanDetails.setIncludedMTVoiceMins(convertStringToInt(zoneAttributes[4]));
					includedPlanDetails.setIncludedPacketKB(convertStringToInt(zoneAttributes[5]));
					includedPlanDetails.setPerMTSmsPrice(convertStringToDouble(zoneAttributes[6]));
					includedPlanDetails.setPerMinMTVoicePrice(convertStringToDouble(zoneAttributes[7]));
					includedPlanDetails.setPerMOSmsPrice(convertStringToDouble(zoneAttributes[8]));
					includedPlanDetails.setPerMinMOVoicePrice(convertStringToDouble(zoneAttributes[9]));
					includedPlanDetails.setPerKBPacketPrice(convertStringToDouble(zoneAttributes[10]));
					
					zoneRatePlan.setZoneId(zoneId);
					zoneRatePlan.setIncludedPlanDetails(includedPlanDetails);
					
					LOG.info("Setting Zone Profile: "+ReflectionToStringBuilder.toString(zoneRatePlan));
					
					zoneIds.add(zoneId);
					zoneRatePlanSettings.add(zoneRatePlan);
				}
			}
		}
		
		LOG.info("Zone Configuration Set for rate plan: "+ratePlan.getRatePlanId());
		
		ratePlan.setZoneRatePlanSettings(zoneRatePlanSettings);
	}

	private void setRatePlanTiers(RatePlan ratePlan, ResultSet rs) throws SQLException {
		List<RatePlanTier> ratePlanTiers = new ArrayList<RatePlanTier>();
		
		String[] tiersStr = rs.getString("tiers").split(",");
		
		for (String tier : tiersStr) {
			String[] tierAttributes = tier.split(";");
			
			if(tierAttributes.length > 0 && NumberUtils.isNumber(tierAttributes[0]))
			{
				RatePlanTier ratePlanTier = new RatePlanTier();
				
				int tierId = Integer.parseInt(tierAttributes[0]);
				
				LOG.info("Tier Id: "+tierId);
				
				ratePlanTier.setTieredRatePlanId(Long.parseLong(tierAttributes[1]));
				ratePlanTier.setStartCount(convertStringToInt(tierAttributes[2]));
				ratePlanTier.setEndCount(convertStringToInt(tierAttributes[3]));
				
				LOG.info("Adding Rate Plan Tier: "+ReflectionToStringBuilder.toString(ratePlanTier));
				
				ratePlanTiers.add(ratePlanTier);
			}
		}
		
		LOG.info("Rate Plan Tiers Set for rate plan: "+ratePlan.getRatePlanId());
		
		ratePlan.setRatePlanTiers(ratePlanTiers);
	}


	private void setRatePlanTriggers(RatePlan ratePlan, ResultSet rs) throws SQLException {
		ProvisionTriggerSettings provisionTriggerSettings = new ProvisionTriggerSettings();
		provisionTriggerSettings.setDuration(rs.getInt("prov_duration"));
		provisionTriggerSettings.setDurationUnit(DurationUnit.fromValue(rs.getInt("prov_duration_unit")));
		provisionTriggerSettings.setMoSmsThreshold(rs.getInt("prov_mo_sms_threshold"));
		provisionTriggerSettings.setMoVoiceMinsThreshold(rs.getInt("prov_mo_voice_threshold"));
		provisionTriggerSettings.setMtSmsThreshold(rs.getInt("prov_mt_sms_threshold"));
		provisionTriggerSettings.setMtVoiceMinsThreshold(rs.getInt("prov_mt_voice_threshold"));
		provisionTriggerSettings.setPacketThreshold(rs.getLong("prov_packet_threshold"));
		provisionTriggerSettings.setPacketThresholdUnit(PacketUnit.fromValue(rs.getInt("prov_packet_threshold_unit")));
		provisionTriggerSettings.setTrafficDays(rs.getInt("prov_traffic_days"));
		provisionTriggerSettings.setPerKBPacketPrice(rs.getDouble("prov_packet_cost"));
		provisionTriggerSettings.setPerMinMOVoicePrice(rs.getDouble("prov_mo_voice_cost"));
		provisionTriggerSettings.setPerMinMTVoicePrice(rs.getDouble("prov_mt_voice_cost"));
		provisionTriggerSettings.setPerMOSmsPrice(rs.getDouble("prov_mo_sms_cost"));
		provisionTriggerSettings.setPerMTSmsPrice(rs.getDouble("prov_mt_sms_cost"));
		
		ratePlan.setProvisionTriggerSettings(provisionTriggerSettings);
						
		SuspendTriggerSettings suspendTriggerSettings = new SuspendTriggerSettings();
		suspendTriggerSettings.setDuration(rs.getInt("susp_duration"));
		suspendTriggerSettings.setDurationUnit(DurationUnit.fromValue(rs.getInt("susp_duration_unit")));
		suspendTriggerSettings.setMoSmsThreshold(rs.getInt("susp_mo_sms_threshold"));
		suspendTriggerSettings.setMoVoiceMinsThreshold(rs.getInt("susp_mo_voice_threshold"));
		suspendTriggerSettings.setMtSmsThreshold(rs.getInt("susp_mt_sms_threshold"));
		suspendTriggerSettings.setMtVoiceMinsThreshold(rs.getInt("susp_mt_voice_threshold"));
		suspendTriggerSettings.setPacketThreshold(rs.getLong("susp_packet_threshold"));
		suspendTriggerSettings.setPacketThresholdUnit(PacketUnit.fromValue(rs.getInt("susp_packet_threshold_unit")));
		suspendTriggerSettings.setTrafficDays(rs.getInt("susp_traffic_days"));
		suspendTriggerSettings.setPerKBPacketPrice(rs.getDouble("susp_packet_cost"));
		suspendTriggerSettings.setPerMinMOVoicePrice(rs.getDouble("susp_mo_voice_cost"));
		suspendTriggerSettings.setPerMinMTVoicePrice(rs.getDouble("susp_mt_voice_cost"));
		suspendTriggerSettings.setPerMOSmsPrice(rs.getDouble("susp_mo_sms_cost"));
		suspendTriggerSettings.setPerMTSmsPrice(rs.getDouble("susp_mt_sms_cost"));
		
		ratePlan.setSuspendTriggerSettings(suspendTriggerSettings);
	}

	private void setRatePlanFeeDetails(RatePlan ratePlan, ResultSet rs) throws SQLException {
		RatePlanFeeDetails ratePlanFeeDetails = new RatePlanFeeDetails();
		ratePlanFeeDetails.setAccessFee(rs.getDouble("access_fee"));
		ratePlanFeeDetails.setActivationFee(rs.getDouble("activation_fee"));
		ratePlanFeeDetails.setDeactivationFee(rs.getDouble("deactivation_fee"));
		ratePlanFeeDetails.setReactivationFee(rs.getDouble("reactivation_fee"));
		ratePlanFeeDetails.setSuspendFee(rs.getDouble("suspend_fee"));
		ratePlanFeeDetails.setUnsuspendFee(rs.getDouble("unsuspend_fee"));
		
		ratePlan.setRatePlanFeeDetails(ratePlanFeeDetails);
	}

	@Override
	public RatePlan createNewRatePlan(int operatorId, int productId, String ratePlanLabel, RatePlanType ratePlanType, RatePlanPaymentType paymentType,
			RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded, RatePlanStatus status,
			boolean overrideRoamingIncluded, int tierCriteria, int homeZoneId, String description, String specialNotes, int carrierRatePlanId, long accountId,
			Date startDate, Date endDate, String currencyName, int includedPeriodMonths, int devicePoolingPolicyId, int packetRoundingPolicyId,
			RatePlanFeeDetails ratePlanFeeDetails, ProvisionTriggerSettings provisionTriggerSettings, SuspendTriggerSettings suspendTriggerSettings,
			List<ZoneRatePlan> zoneRatePlanSettings, List<RatePlanTier> ratePlanTiers, Date requestedDate, String requestedUser, int proRateCancellationFee, int contractTerm,
			int cancellationFeeReductionInterval, int cancelFeeAtTermEnd, int accountForDaysInProvState,
			int accountForDaysInSuspendState, List<RatePlanOverageBucket> ratePlanOverageBucket, boolean roamingIncluded, int zoneSetId, String technology, boolean waterfallEnabled, Long wholesaleRateplanId, boolean isWholesaleRatePlan) throws RatePlanException,
			DuplicateRatePlanException, SubRatePlanNotFoundException, ZoneNotFoundException, DuplicateZoneException {
		RatePlan ratePlan = null;
		Connection conn = null;
		
		// Generate rate plan Id for the new rate plan
		long newRatePlanId = 0; 

		LOG.info("createNewRatePlan: processing new rate plan creation...");
		
		try {
			LOG.info("Creating a new rate plan (of type: "+ratePlanType.name()+") with rate plan label: "+ratePlanLabel+" and product id: " + productId);
			
			// Set Rate Plan Name to operator_product_* temporarily, the auto generated rate plan name will be set after the rate plan id is generated in csp rate plan table
			String ratePlanName = oaUtils.generateRatePlanName(operatorId, "*", productId);
			
			LOG.info("Auto generated rate plan name for the new rate plan: "+ratePlanName);
						
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			
			// Set Auto Commit as false
			conn.setAutoCommit(false);
			
			//validate rate plan label here
			validateRatePlanLabel(ratePlanLabel, conn);
						
			// Create non-tiered/tiered rate plan as csp rate plans for backward compatibility
			if(ratePlanType == RatePlanType.NON_TIERED || ratePlanType == RatePlanType.TIERED)
			{
				LOG.info("Calling csp rate plan dao for creating new csp rate plan...");
				//TODO: pending CSP rate plan related changes for cancellation ???
				// Create CSP Rate Plan and return the new rate plan id
				newRatePlanId = cspRatePlanDAO.createNewRatePlan(operatorId, productId, ratePlanName, ratePlanType, paymentType, accessType, periodType,
						accessFeeMonths, expireIncluded, status, tierCriteria, homeZoneId, description, specialNotes, carrierRatePlanId, accountId, startDate,
						endDate, currencyName, includedPeriodMonths, devicePoolingPolicyId, packetRoundingPolicyId, ratePlanFeeDetails,
						provisionTriggerSettings, suspendTriggerSettings, zoneRatePlanSettings, ratePlanTiers, roamingIncluded, requestedDate, requestedUser, zoneSetId, technology, conn);
			}
			// Generate a new rate plan id
			else
			{
				newRatePlanId = generateNewRatePlanId(conn);
			}
			
			// Set the actual rate plan name using rate plan id
			if(newRatePlanId > 0)
			{
				// Update Rate Plan Name with auto generated rate plan name using algo
				String autoGeneratedRatePlanName = cspRatePlanDAO.updateAndGetRatePlanNameUsingAlgo(operatorId, productId, newRatePlanId, conn);
				
				// Update rate plan name to auto generated rate plan name so as to save it in the new rate plan tables
				if(autoGeneratedRatePlanName != null)
				{
					ratePlanName = autoGeneratedRatePlanName;
				}
			}

			LOG.info("Creating a new rate plan (of type: "+ratePlanType.name()+") with new rate plan id: " + newRatePlanId);
			
			// Truncate to 30 chars
			requestedUser = StringUtils.abbreviate(requestedUser, 30);
			
			// Create new rate plan
			createNewRatePlanInfo(operatorId, newRatePlanId, ratePlanLabel, ratePlanName, ratePlanType, paymentType, accessType, periodType, accessFeeMonths,
					expireIncluded, status, overrideRoamingIncluded, productId, tierCriteria, homeZoneId, description, specialNotes, carrierRatePlanId,
					accountId, startDate, endDate, currencyName, includedPeriodMonths, devicePoolingPolicyId, packetRoundingPolicyId, requestedDate,
					requestedUser, proRateCancellationFee, contractTerm, cancellationFeeReductionInterval, cancelFeeAtTermEnd, accountForDaysInProvState,
					accountForDaysInSuspendState, roamingIncluded, zoneSetId, technology, waterfallEnabled, wholesaleRateplanId, isWholesaleRatePlan, conn);
			LOG.info("Created rate plan information for the new rate plan : " + newRatePlanId);

			// Save rate plan triggers
			if(provisionTriggerSettings != null || suspendTriggerSettings != null)
			{
				createRatePlanTriggers(operatorId, newRatePlanId, provisionTriggerSettings, suspendTriggerSettings, requestedDate, requestedUser, conn);
				LOG.info("Created rate plan triggers for the new rate plan: " + newRatePlanId);				
			}
			else
			{
				LOG.warn("Rate plan triggers information is empty, skipping creation of rate plan trigger details for rate plan id: "+newRatePlanId);		
			}

			if(ratePlanFeeDetails != null)
			{
				// Save rate plan fees
				createRatePlanFees(operatorId, newRatePlanId, ratePlanFeeDetails, requestedDate, requestedUser, conn);
				LOG.info("Created rate plan fees information for the new rate plan: " + newRatePlanId);					
			}
			else
			{
				LOG.warn("Rate plan fees information is empty, skipping creation of rate plan fees details for rate plan id: "+newRatePlanId);						
			}
			// Creates fees related information for a tiered rate plan
			// For a tiered rate plan, fees inferred from the rate plans that are 
			// configured as tiers 
			if(ratePlanType == RatePlanType.TIERED)
			{
				if(ratePlanTiers != null)
				{
					// Create rate plan tiers
					createRatePlanTiers(operatorId, newRatePlanId, ratePlanTiers, requestedDate, requestedUser, conn);
					LOG.info("Created rate plan tiers information for the new rate plan: " + newRatePlanId);
				}
				else
				{
					LOG.warn("Rate plan tiers information is empty, skipping creation of rate plan tiers for rate plan id: "+newRatePlanId);						
				}
			}
			
			// Set up zone level rate plan configuration
			if (zoneRatePlanSettings != null) {
				createRatePlanZoneConfiguration(newRatePlanId, zoneRatePlanSettings, requestedDate, requestedUser, conn);
				LOG.info("Added zones configuration for the Rate Plan with rate plan: " + newRatePlanId);
			}
			
			// For non-tiered rate plans, map rate plan overages to zones
			if((ratePlanType == RatePlanType.NON_TIERED || ratePlanType == RatePlanType.TIERED) && newRatePlanId > 0)
			{
				// Fetch overage zones
				Zone[] overageZones = zoneDAO.getOverageZonesByZoneSet(operatorId, zoneSetId, homeZoneId);
				
				// map rate plan to overage zones
				createRatePlanOverageZones(newRatePlanId, overageZones, requestedDate, requestedUser, conn);
			}
			
			//Create Rate plan Overage buckets
			if(ratePlanOverageBucket != null) {
				createRatePlnOverageBkt(accountId, newRatePlanId, ratePlanOverageBucket, conn);
			}
			
			// Commit the changes
			conn.commit();
			LOG.info("Committed createNewRatePlan changes to db");

			// Return the rate plan created to the client
			ratePlan = new RatePlan();
			
			ratePlan.setRatePlanId(newRatePlanId);
			ratePlan.setRatePlanLabel(ratePlanLabel);
			ratePlan.setRatePlanName(ratePlanName);
			ratePlan.setRatePlanType(ratePlanType);
			ratePlan.setPaymentType(paymentType);
			ratePlan.setAccessType(accessType);
			ratePlan.setPeriodType(periodType);
			ratePlan.setStatus(status);
			ratePlan.setAccessFeeMonths(accessFeeMonths);
			ratePlan.setExpireIncluded(expireIncluded);
			
			ratePlan.setProductId(productId);
			ratePlan.setRatePlanOverageBucket(ratePlanOverageBucket);
			
			ratePlan.setTierCriteria(tierCriteria);
			ratePlan.setHomeZoneId(homeZoneId);
			ratePlan.setDescription(description);
			ratePlan.setSpecialNotes(specialNotes);
			
			ratePlan.setCarrierRatePlanId(carrierRatePlanId);
			ratePlan.setAccountId(accountId);
			
			ratePlan.setStartDate(new java.sql.Date(startDate.getTime()));				
			ratePlan.setEndDate(new java.sql.Date(endDate.getTime()));
			ratePlan.setCurrencyName(currencyName);	
			ratePlan.setIncludedPeriodMonths(includedPeriodMonths);
			ratePlan.setDevicePoolingPolicyId(devicePoolingPolicyId);
			ratePlan.setPacketDataRoundingPolicyId(packetRoundingPolicyId);
			
			ratePlan.setProvisionTriggerSettings(provisionTriggerSettings);
			ratePlan.setSuspendTriggerSettings(suspendTriggerSettings);
			
			if(ratePlanType != RatePlanType.TIERED)
			{
				ratePlan.setRatePlanFeeDetails(ratePlanFeeDetails);
			}
			
			if(ratePlanType == RatePlanType.TIERED)
			{
				ratePlan.setRatePlanTiers(ratePlanTiers);
			}
			
			ratePlan.setZoneRatePlanSettings(zoneRatePlanSettings);
			ratePlan.setOperatorId(operatorId);
			ratePlan.setZoneSetId(zoneSetId);
            ratePlan.setTechnology(technology);
            ratePlan.setWaterfallEnabled(waterfallEnabled);
            ratePlan.setWholesaleRateplanId(wholesaleRateplanId);
			
			// Save in Cache
			putInCache(ratePlan);
			
			LOG.info("Returning rate plan created...");
		} catch (SQLException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new RatePlanException("Create new rate plan failed due to db error", e);
		} catch (SubRatePlanNotFoundException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (ZoneNotFoundException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (DuplicateRatePlanException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (DuplicateZoneException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (Exception e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw new RatePlanException("Create new rate plan failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn);
		}

		LOG.info("Returned rate plan to the caller: " + ratePlan);		
		
		return ratePlan;
	}
	
	
	private void validateRatePlanLabel(String ratePlanLabel, Connection conn) throws DuplicateRatePlanException {
		String query = "select count(*) from rate_plan where rate_plan_label = ?" ;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean ratePlanLabelExists = false;
		try {	
			ps = conn.prepareStatement(query);
			ps.setString(1, ratePlanLabel);
			LOG.info("validateRatePlanLabel: validating if rate plan label already exists: " + ratePlanLabel);
			rs = ps.executeQuery();

			if (rs.next()) {
				ratePlanLabelExists = (rs.getInt(1) > 0);
				if(ratePlanLabelExists) {
					LOG.error("Rate Plan name already exists="+ ratePlanLabel);		
					throw new DuplicateRatePlanException(String.format("Rate Plan label %s already exists", ratePlanLabel));
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception during ratePlanLabelCheck - validateRatePlanLabel", e);
			throw new GenericServiceException("Exception during validateRatePlanLabel", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
		
	}

	/**
	 * Creates New rate plan if insert= true or updates an existing rate plan
	 * 
	 * @param insert
	 * @param accountId
	 * @param ratePlanId
	 * @param rpOverageBucketList
	 * @param conn
	 * @throws SQLException
	 */
	private void createRatePlnOverageBkt(long accountId, long ratePlanId,  List<RatePlanOverageBucket> rpOverageBucketList, Connection conn) 
			throws SQLException {
		LOG.info("Creating New Rate Plan Overage Bucket");

		PreparedStatement ps = null;

		String insertUpdateOverageBucket = "INSERT INTO aerisgen.RATE_PLAN_OVERAGE_BUCKET "
				+ "(RATE_PLAN_ID, ACCOUNT_ID, START_DATE, END_DATE, BUCKET_SIZE, BUCKET_SIZE_TYPE, BUCKET_PRICE, NO_ACCESS_FEE_DEVICE_COUNT, "
				+ "AUTO_ADD, CREATED_DATE, LAST_MODIFIED_DATE, CREATED_BY, LAST_MODIFIED_BY, REP_TIMESTAMP) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;
		//delete previous associated overage buckets for rate plan
		deleteRatePlanOverageBuckets(ratePlanId, accountId, conn);

		try {
			// Create SQL Statement
			if(rpOverageBucketList != null) {
				LOG.debug("Creating Prepared Statement to insert the data in aerisgen.RATE_PLAN_OVERAGE_BUCKET table");
				ps = conn.prepareStatement(insertUpdateOverageBucket);
				for (RatePlanOverageBucket ratePlanOverageBucket : rpOverageBucketList) {
					LOG.debug("Setting aerisgen.RATE_PLAN_OVERAGE_BUCKET Parameters");
					int index = 1;
					// Set Parameters
					ps.setLong(index++, ratePlanId);
					ps.setLong(index++, accountId);
					ps.setDate(index++, ratePlanOverageBucket.getStartDate());
					ps.setDate(index++, ratePlanOverageBucket.getEndDate());
					//set the bucket in MB here if the unit is in GB
					double overageSize = ratePlanOverageBucket.getBucketSize();
					if(ratePlanOverageBucket.getBucketSizeType() != null && ratePlanOverageBucket.getBucketSizeType().equalsIgnoreCase("GB")){
						overageSize = overageSize * 1024 ;
						BigDecimal bDec = new BigDecimal(String.valueOf(overageSize)).setScale(2, BigDecimal.ROUND_HALF_UP);
						overageSize = bDec.doubleValue();
					}
					ps.setDouble(index++, overageSize);
					ps.setString(index++, ratePlanOverageBucket.getBucketSizeType());
					ps.setDouble(index++, ratePlanOverageBucket.getBucketPrice());
					ps.setInt(index++, ratePlanOverageBucket.getNoAccessFeeDeviceCount());
					ps.setInt(index++, ratePlanOverageBucket.getAutoAdd());
					if(ratePlanOverageBucket.getCreatedDate() == null) {
						ratePlanOverageBucket.setCreatedDate(new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					if(ratePlanOverageBucket.getUpdatedDate() == null) {
						ratePlanOverageBucket.setUpdatedDate(new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					ps.setDate(index++, ratePlanOverageBucket.getCreatedDate() != null ? ratePlanOverageBucket.getCreatedDate() : new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					ps.setDate(index++, ratePlanOverageBucket.getUpdatedDate() != null ? ratePlanOverageBucket.getUpdatedDate() : new java.sql.Date(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()) );
					//If createdBy is not present use value from updatedBy and vice-versa
					if(ratePlanOverageBucket.getCreatedBy() == null) {
						ratePlanOverageBucket.setCreatedBy(ratePlanOverageBucket.getUpdatedBy());
					}
					if(ratePlanOverageBucket.getUpdatedBy() == null) {
						ratePlanOverageBucket.setUpdatedBy(ratePlanOverageBucket.getCreatedBy());
					}
					ps.setString(index ++ , ratePlanOverageBucket.getCreatedBy());
					ps.setString(index ++ , ratePlanOverageBucket.getUpdatedBy());
                                        String repTimestamp = ratePlanOverageBucket.getRepTimeStamp();
					if(repTimestamp != null && !repTimestamp.trim().isEmpty()) {
						ps.setTimestamp(index++, convertStringToTS(ratePlanOverageBucket.getRepTimeStamp()));
					}else {
						ps.setTimestamp(index++, new Timestamp(Calendar.getInstance(TimeZone.getTimeZone(Constants.TIMEZONE_GMT)).getTimeInMillis()));
					}
					LOG.debug("Inserting aerisgen.RATE_PLAN_OVERAGE_BUCKET into the db");
					ps.addBatch();
				}
				ps.executeBatch();
			}

			LOG.info("Created aerisgen.RATE_PLAN_OVERAGE_BUCKET for account id = " + accountId + " and ratePlanId = " + ratePlanId +  " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating aerisgen.RATE_PLAN_OVERAGE_BUCKET", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	
	/**
	 * Deletes the associated rate plan overage buckets 
	 * 
	 * @param ratePlanId
	 * @param accountId
	 * @param conn
	 * @throws SQLException
	 */
	private void deleteRatePlanOverageBuckets(long ratePlanId, long accountId, Connection conn ) throws SQLException {
		String deleteAccountOverageBucket = "DELETE FROM aerisgen.RATE_PLAN_OVERAGE_BUCKET WHERE account_id = " + accountId + " and rate_plan_id = " + ratePlanId;
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(deleteAccountOverageBucket);
			ps.executeUpdate();
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting aerisgen.RATE_PLAN_OVERAGE_BUCKET for accountId=" + accountId + " and ratePlanId=" + ratePlanId, sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void createNewRatePlanInfo(int operatorId, long newRatePlanId, String ratePlanLabel, String ratePlanName, RatePlanType ratePlanType,
			RatePlanPaymentType paymentType, RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded,
			RatePlanStatus status, boolean overrideRoamingIncluded, int productId, int tierCriteria, int homeZoneId, String description, String specialNotes, 
			int carrierRatePlanId, long accountId, Date startDate, Date endDate, String currencyName, int includedPeriodMonths, int devicePoolingPolicyId, 
			int packetRoundingPolicyId,	Date requestedDate, String requestedUser, int proRateCancellationFee, int contractTerm, int cancellationFeeReductionInterval,
			int cancelFeeAtTermEnd, int accountForDaysInProvState,
			int accountForDaysInSuspendState, boolean roamingIncluded, int zoneSetId, String technology, boolean waterfallEnabled, long wholesaleRateplanId, boolean isWholesaleRatePlan, Connection conn) throws SQLException, DuplicateRatePlanException {
		LOG.info("Creating New Rate Plan Info");

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in rate_plan table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_RATE_PLAN_INFO);
            
            if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                ps = conn.prepareStatement(SQL_CREATE_RATE_PLAN_INFO_2);
            }

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(++index, newRatePlanId);// rate_plan_id
			ps.setString(++index, ratePlanLabel); // rate_plan_label
			ps.setString(++index, ratePlanName);// rate_plan_name
			
			// Set Tiered or Non-Tiered Plan
			ps.setInt(++index, ratePlanType.getValue());// rate_plan_type
			
			// Set Pre-Paid or Post-Paid Plan
			ps.setInt(++index, paymentType.getValue());// payment_type
			
			ps.setInt(++index, productId);// product_id
			
			// Set Tier Criteria if the rate plan is tiered
			if(ratePlanType == RatePlanType.TIERED && tierCriteria > 0)
			{
				ps.setInt(++index, tierCriteria);// tier_criteria				
			}
			else
			{
				ps.setNull(++index, Types.INTEGER);// tier_criteria		
			}
			
			// Set Monthly or Annual Rate Plan
			ps.setInt(++index, accessFeeMonths);// access_fee_duration_months
			
			//Expire Included
			ps.setInt(++index, expireIncluded ? 1 : 0);// expire_included
			
			// Set Shared or Exclusive Rate Plan
			ps.setInt(++index, accessType.getValue());// is_shared
			
			ps.setInt(++index, homeZoneId);// home_zone_id
			ps.setString(++index, description);// description
			ps.setString(++index, specialNotes);// special_notes
			
			// Set carrier rate plan id
			ps.setInt(++index, carrierRatePlanId);// carrier_rate_plan_id				
			
			// Set account id if the rate plan is exclusive
			if(accessType == RatePlanAccessType.EXCLUSIVE && accountId > 0)
			{
				ps.setLong(++index, accountId);// account_id				
			}
			else
			{
				ps.setNull(++index, Types.INTEGER);// account_id		
			}
			
			ps.setInt(++index, status.getValue());// status
			ps.setInt(++index, overrideRoamingIncluded ? 1 : 0);// override_roaming_included
			ps.setDate(++index, new java.sql.Date(startDate.getTime()));// start_date
			ps.setDate(++index, new java.sql.Date(endDate.getTime()));// end_date
			ps.setString(++index, currencyName);// currency_code
			ps.setInt(++index, includedPeriodMonths); // included_period_months
			
			// Set Device Pooling Policy
			if(devicePoolingPolicyId != 0)
			{
				ps.setInt(++index, devicePoolingPolicyId); // device_policy_id
			}
			else
			{
				ps.setNull(++index, Types.INTEGER);// device_policy_id
			}
			
			// Set Packet Data Rounding Policy
			if(packetRoundingPolicyId != 0)
			{
				ps.setInt(++index, packetRoundingPolicyId); // packet_rounding_policy_id
			}
			else
			{
				ps.setNull(++index, Types.INTEGER);// packet_rounding_policy_id
			}
			
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// created_date
			ps.setString(++index, requestedUser);// created_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
			ps.setString(++index, requestedUser);// last_modified_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp

			LOG.debug("Inserting rate plan basic info the db...");
			ps.setInt(++index, proRateCancellationFee);
			ps.setInt(++index, contractTerm);
			ps.setInt(++index, cancellationFeeReductionInterval);
			ps.setInt(++index, cancelFeeAtTermEnd);
			ps.setInt(++index, accountForDaysInProvState);
			ps.setInt(++index, accountForDaysInSuspendState);
            //Roaming Included
			ps.setInt(++index, roamingIncluded ? 1 : 0);// roamingIncluded
			ps.setInt(++index, zoneSetId);
            ps.setString(++index, technology);
            ps.setInt(++index, BooleanUtils.toInteger(waterfallEnabled));
            ps.setInt(++index, BooleanUtils.toInteger(isWholesaleRatePlan));
            
            if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                ps.setInt(++index, operatorId);
            }
            
            ps.setLong(++index, wholesaleRateplanId);
			// Insert rate plan info into the db
			ps.executeUpdate();

			LOG.info("Created Rate Plan Info for rate plan id = " + newRatePlanId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating rate plan info in table rate_plan", sqle);	
			
			if(StringUtils.contains(sqle.getMessage(), "RATE_PLAN_LABEL_UK"))
			{
				throw new DuplicateRatePlanException("The Rate Plan Label and Product combination already exists");
			}
			
			if(StringUtils.contains(sqle.getMessage(), "RATE_PLAN_NAME_UK"))
			{
				throw new DuplicateRatePlanException("The Rate Plan Name and Product combination already exists");
			}
			
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void createRatePlanFees(int operatorId, long newRatePlanId, RatePlanFeeDetails ratePlanFeeDetails, Date requestedDate, String requestedUser, Connection conn) 
			throws SQLException {
		LOG.info("Creating Rate Plan Fees Info");

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in rate_plan_fee table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_RATE_PLAN_FEES);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(++index, newRatePlanId);// rate_plan_id
			
			// Set Subscription Fees
			ps.setDouble(++index, ratePlanFeeDetails.getAccessFee()); // access_fee
			ps.setDouble(++index, ratePlanFeeDetails.getActivationFee());// activation_fee
			ps.setDouble(++index, ratePlanFeeDetails.getSuspendFee()); // suspend_fee
			ps.setDouble(++index, ratePlanFeeDetails.getUnsuspendFee());// unsuspend_fee
			ps.setDouble(++index, ratePlanFeeDetails.getDeactivationFee());// deactivation_fee
			ps.setDouble(++index, ratePlanFeeDetails.getReactivationFee());// reactivation_fee

			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// created_date
			ps.setString(++index, requestedUser);// created_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
			ps.setString(++index, requestedUser);// last_modified_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp

			LOG.debug("Inserting rate plan fees info in the db");

			// Insert rate plan fees info into the db
			ps.executeUpdate();

			LOG.info("Created Rate Plan Fees Info for rate plan id = " + newRatePlanId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating rate plan fees info in table rate_plan_fee", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void createRatePlanTriggers(int operatorId, long newRatePlanId, ProvisionTriggerSettings provisionTriggerSettings,
			SuspendTriggerSettings suspendTriggerSettings, Date requestedDate, String requestedUser, Connection conn) throws SQLException {
		LOG.info("Creating Rate Plan Trigger Info");

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in rate_plan_trigger table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_RATE_PLAN_TRIGGERS);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(++index, newRatePlanId);// rate_plan_id
			
			// Set Provision State Triggers
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getDuration() : 0); // prov_duration
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getDurationUnit().getValue() : DurationUnit.MONTHS.getValue());// prov_duration_unit
			ps.setLong(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getPacketThreshold() : 0);// prov_packet_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getPacketThresholdUnit().getValue() : PacketUnit.KB.getValue()); // prov_packet_threshold_unit
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getMoSmsThreshold() : 0);// prov_mo_sms_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getMtSmsThreshold() : 0);// prov_mt_sms_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getMoVoiceMinsThreshold() : 0);// prov_mo_voice_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getMtVoiceMinsThreshold() : 0);// prov_mt_voice_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getTrafficDays() : 0); // prov_traffic_days
			
			// Set Suspend State Triggers
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getDuration() : 0); // susp_duration
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getDurationUnit().getValue() : DurationUnit.MONTHS.getValue());// susp_duration_unit
			ps.setLong(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getPacketThreshold() : 0);// susp_packet_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getPacketThresholdUnit().getValue() : PacketUnit.KB.getValue()); // susp_packet_threshold_unit
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getMoSmsThreshold() : 0);// susp_mo_sms_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getMtSmsThreshold() : 0);// susp_mt_sms_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getMoVoiceMinsThreshold() : 0);// susp_mo_voice_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getMtVoiceMinsThreshold() : 0);// susp_mt_voice_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getTrafficDays() : 0); // susp_traffic_days			
			
			// Set Provision State Fees
			ps.setDouble(++index, provisionTriggerSettings.getPerMTSmsPrice());// prov_mt_sms_cost
			ps.setDouble(++index, provisionTriggerSettings.getPerMinMTVoicePrice());// prov_mt_voice_cost
			ps.setDouble(++index, provisionTriggerSettings.getPerMOSmsPrice());// prov_mo_sms_cost
			ps.setDouble(++index, provisionTriggerSettings.getPerMinMOVoicePrice());// prov_mo_voice_cost
			ps.setDouble(++index, provisionTriggerSettings.getPerKBPacketPrice());// prov_packet_cost
			
			// Set Suspend State Fees
			ps.setDouble(++index, suspendTriggerSettings.getPerMTSmsPrice());// susp_mt_sms_cost
			ps.setDouble(++index, suspendTriggerSettings.getPerMinMTVoicePrice());// susp_mt_voice_cost
			ps.setDouble(++index, suspendTriggerSettings.getPerMOSmsPrice());// susp_mo_sms_cost
			ps.setDouble(++index, suspendTriggerSettings.getPerMinMOVoicePrice());// susp_mo_voice_cost
			ps.setDouble(++index, suspendTriggerSettings.getPerKBPacketPrice());// susp_packet_cost

			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// created_date
			ps.setString(++index, requestedUser);// created_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
			ps.setString(++index, requestedUser);// last_modified_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp

			LOG.debug("Inserting rate plan triggers info in the db");

			// Insert additional info into the db
			ps.executeUpdate();

			LOG.info("Created Rate Plan Triggers Info for rate plan id = " + newRatePlanId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating rate plan trigger info in table rate_plan_trigger", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void createRatePlanTiers(int operatorId, long newRatePlanId, List<RatePlanTier> ratePlanTiers, Date requestedDate, String requestedUser, Connection conn) 
			throws RatePlanException, SQLException, SubRatePlanNotFoundException {
		LOG.info("Creating Rate Plan Tiers Info");

		PreparedStatement ps = null;

		try {
			LOG.debug("Creating Prepared Statement to insert the data in rate_plan_tier table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_RATE_PLAN_TIERS);

			LOG.debug("Setting Parameters");

			for (RatePlanTier ratePlanTier : ratePlanTiers) {
				ps.clearParameters();
				
				if(ratePlanTier != null)
				{
					LOG.info("processing rate plan tier information with range: "+ratePlanTier.getStartCount() +" - "+ratePlanTier.getEndCount()+" as rate plan id: "+ratePlanTier.getTieredRatePlanId());					
					
					int index = 0;
					
					// Generate next tier id
					long tierId = generateNewRatePlanTierId(conn);

					// Set Parameters
					ps.setLong(++index, tierId); // tier_id
					ps.setLong(++index, newRatePlanId); // rate_plan_id
					ps.setInt(++index, ratePlanTier.getStartCount()); // start_count
					ps.setInt(++index, ratePlanTier.getEndCount()); // end_count
					ps.setLong(++index, ratePlanTier.getTieredRatePlanId()); // tier_rate_plan_id
					ps.setInt(++index, IN_USE); // in_use

					ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// created_date
					ps.setString(++index, requestedUser);// created_by
					ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
					ps.setString(++index, requestedUser);// last_modified_by
					ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp

					// add to batch
					ps.addBatch();		
					
					LOG.info("Added as tier with tierId: "+tierId+" to the batch successfully");
				}
			}

			LOG.debug("Inserting rate plan tiers info in the db");

			// Execute batch of rate plan tiers
			ps.executeBatch();

			LOG.info("Created Rate Plan tiers Info for rate plan id = " + newRatePlanId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during creating rate plan tiers info in table rate_plan_tier", sqle);
			
			if(StringUtils.contains(sqle.getMessage(), "parent key not found"))
			{
				throw new SubRatePlanNotFoundException("One of the sub rate plans in the tiers is not valid in the tiered rate plan: "+newRatePlanId);
			}
			
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void createRatePlanZoneConfiguration(long newRatePlanId, List<ZoneRatePlan> zoneProfiles, Date requestedDate, String requestedUser, Connection conn) 
			throws ZoneNotFoundException, DuplicateZoneException, SQLException
	{
		LOG.info("Creating rate plan zone configuration for rate plan id: " + newRatePlanId);

		PreparedStatement ps = null;
		List<Integer> zoneIds = new ArrayList<Integer>();

		try {
			LOG.debug("Creating Prepared Statement to insert the data in rate_plan_zone_rating_policy table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_RATE_PLAN_ZONE_CONFIG);

			LOG.debug("Setting parameters");		
			
			for (ZoneRatePlan zoneRatePlan : zoneProfiles) {
				IncludedPlanDetails planDetails = zoneRatePlan.getIncludedPlanDetails();
				
				if(planDetails != null)
				{	
					// Add Zone Config to the Rate Plan
					addZoneConfigurationToRatePlan(newRatePlanId, zoneRatePlan.getZoneId(), planDetails, requestedDate, requestedUser, ps);
					
					if(!zoneIds.contains(zoneRatePlan.getZoneId()))
					{
						zoneIds.add(zoneRatePlan.getZoneId());
					}
					else
					{
						throw new DuplicateZoneException("Duplicate zone configuration for zone id: "+zoneRatePlan.getZoneId());
					}
				}
			}

			LOG.info("inserting rate plan zone configuration in the db");

			// Execute the sql batch now
			ps.executeBatch();

			LOG.info("Added rate plan zone configuration for rate plan id: " + newRatePlanId + " in database");
		} catch (BatchUpdateException sqle) {
			LOG.error("Exception during creating rate plan zone configuration in table rate_plan_zone_rating_policy - batch update", sqle);
			throw new ZoneNotFoundException("One of the zones specified is not valid :"+zoneIds);
		} catch (SQLException sqle) {
			LOG.error("Exception during creating rate plan zone configuration in table rate_plan_zone_rating_policy", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void addZoneConfigurationToRatePlan(long newRatePlanId, int zoneId, IncludedPlanDetails planDetails, Date requestedDate, String requestedUser,
			PreparedStatement ps) throws SQLException {
		ps.clearParameters();

		if (zoneId > 0) {
			ps.setLong(1, newRatePlanId);
			ps.setLong(2, zoneId);
			ps.setInt(3, planDetails.getIncludedMOSms());
			ps.setInt(4, planDetails.getIncludedMTSms());
			ps.setInt(5, planDetails.getIncludedMOVoiceMins());
			ps.setInt(6, planDetails.getIncludedMTVoiceMins());
			ps.setDouble(7, planDetails.getIncludedPacketKB());
			ps.setDouble(8, planDetails.getPerMTSmsPrice());
			ps.setDouble(9, planDetails.getPerMinMTVoicePrice());
			ps.setDouble(10, planDetails.getPerMOSmsPrice());
			ps.setDouble(11, planDetails.getPerMinMOVoicePrice());
			ps.setDouble(12, planDetails.getPerKBPacketPrice());
			ps.setTimestamp(13, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(14, requestedUser);
			ps.setTimestamp(15, new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(16, requestedUser);
			ps.setTimestamp(17, new java.sql.Timestamp(requestedDate.getTime()));

			ps.addBatch();

			LOG.debug("Creating: Zone Configuration for zone " + zoneId + " added to rate plan");
		} else {
			LOG.warn("Skipping: Zone Id is not valid, Zone Configuration for zone " + zoneId + " not added to rate plan");
		}
	}

	@Override
	public RatePlan updateRatePlan(int operatorId, long ratePlanId, int productId, String ratePlanLabel, RatePlanType ratePlanType,
			RatePlanPaymentType paymentType, RatePlanAccessType accessType, RatePlanPeriodType periodType, int accessFeeMonths, boolean expireIncluded,
			RatePlanStatus status, boolean overrideRoamingInclued, int tierCriteria, int homeZoneId, String description, String specialNotes,
			int carrierRatePlanId, long accountId, Date startDate, Date endDate, String currencyName, int includedPeriodMonths, int devicePoolingPolicyId,
			int packetRoundingPolicyId, RatePlanFeeDetails ratePlanFeeDetails, ProvisionTriggerSettings provisionTriggerSettings,
			SuspendTriggerSettings suspendTriggerSettings, List<ZoneRatePlan> zoneRatePlanSettings, List<RatePlanTier> ratePlanTiers, Date requestedDate,
			String requestedUser, int proRateCancellationFee, int contractTerm, int cancellationFeeReductionInterval,
			int cancelFeeAtTermEnd, int accountForDaysInProvState,
			int accountForDaysInSuspendState, List<RatePlanOverageBucket> ratePlanOverageBucket, boolean roamingIncluded, int zoneSetId, String technology, boolean waterfallEnabled, Long wholesaleRateplanId, boolean isWholesaleRatePlan) throws RatePlanNotFoundException, BadStartDateException, RatePlanException, SubRatePlanNotFoundException,
			DuplicateRatePlanException, ZoneNotFoundException, DuplicateZoneException, RatePlanAlreadyAssignedException {
		Connection conn = null;
		RatePlan ratePlan = null;
		boolean success = false;

		LOG.info("updateRatePlan: processing new rate plan creation...");
        
		try {
			LOG.info("Updating rate plan (of type: "+ratePlanType.name()+") with rate plan id: "+ratePlanId+", rate plan label: "+ratePlanLabel+" and product id: " + productId);
			
			// Validate Start Date
			validateRatePlanAndStartDate(ratePlanId, startDate);
			
			// Fetch the rate plan from cache
			ratePlan = ratePlanCache.get(ratePlanId);
						
			// Rate Plan Name cannot be modified
			String ratePlanName = ratePlan.getRatePlanName();
			
			LOG.info("Rate plan name: "+ratePlanName);
			
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			
			// Set Auto Commit as false
			conn.setAutoCommit(false);
			
			// Update non-tiered rate plan as csp rate plans for backward compatibilitty
			if(ratePlanType == RatePlanType.NON_TIERED)
			{
				LOG.info("Calling csp rate plan dao for updating csp rate plan...");
				
				// Only allow update of this rate plan is existing csp rate plan - non-tiered rate plans are created as csp rate plans 
				// for backward compatibility
				if(ratePlan.getRatePlanType() == RatePlanType.NON_TIERED)
				{
					// Update CSP Rate Plan and return success flag
					success = cspRatePlanDAO.updateRatePlan(operatorId, ratePlanId, productId, ratePlanName, ratePlanType, paymentType, accessType, periodType,
							accessFeeMonths, expireIncluded, status, tierCriteria, homeZoneId, description, specialNotes, carrierRatePlanId, accountId,
							startDate, endDate, currencyName, includedPeriodMonths, devicePoolingPolicyId, packetRoundingPolicyId, ratePlanFeeDetails,
							provisionTriggerSettings, suspendTriggerSettings, zoneRatePlanSettings, ratePlanTiers, roamingIncluded, requestedDate, requestedUser,zoneSetId, technology, conn);
				}
			}
			
			// Truncate to 30 chars
			requestedUser = StringUtils.abbreviate(requestedUser, 30);	

			// Update rate plan info
			updateRatePlanInfo(operatorId, ratePlanId, ratePlanLabel, ratePlanType, paymentType, accessType, accessFeeMonths, expireIncluded, status,
					overrideRoamingInclued, productId, tierCriteria, homeZoneId, description, specialNotes, carrierRatePlanId, accountId, startDate, endDate,
					currencyName, includedPeriodMonths, devicePoolingPolicyId, packetRoundingPolicyId, requestedDate, requestedUser, 
					proRateCancellationFee, contractTerm, cancellationFeeReductionInterval,
					cancelFeeAtTermEnd, accountForDaysInProvState, accountForDaysInSuspendState, roamingIncluded,zoneSetId, technology, waterfallEnabled, wholesaleRateplanId, isWholesaleRatePlan, conn);
			LOG.info("Updated rate plan basic information for the rate plan id : " + ratePlanId);
			
			//Create Rate plan Overage buckets
			createRatePlnOverageBkt(accountId, ratePlanId, ratePlanOverageBucket, conn);
			LOG.info("Updated rate plan overage bucket for the rate plan id: " + ratePlanId);	

			// Update rate plan triggers
			if(provisionTriggerSettings != null || suspendTriggerSettings != null)
			{
				updateRatePlanTriggers(operatorId, ratePlanId, provisionTriggerSettings, suspendTriggerSettings, requestedDate, requestedUser, conn);
				LOG.info("Updated rate plan triggers information for the rate plan id: " + ratePlanId);		
			}
			else
			{
				// Delete Old Rate Plan Fees Info
				deleteRatePlanTriggers(ratePlanId, conn);
				LOG.warn("Rate plan triggers information is empty, no rate plan triggers will be set for rate plan id: "+ratePlanId);		
			}

			// Update fees related information for a non-tiered rate plan
			if(ratePlanType != RatePlanType.TIERED)
			{
				// If this rate plan was a tiered rate plan previously, clear tiers information
				if(ratePlan.getRatePlanType() == RatePlanType.TIERED)
				{
					// Delete Previous Rate Plan Tiers Info
					deleteRatePlanTiers(ratePlanId, conn);
					LOG.info("Changed from tiered to non-tiered rate plan: Deleted old rate plan tiers for the rate plan: " + ratePlanId);
					
					if(ratePlanFeeDetails != null)
					{
						// Save rate plan fees
						createRatePlanFees(operatorId, ratePlanId, ratePlanFeeDetails, requestedDate, requestedUser, conn);
						LOG.info("Created rate plan fees information for the rate plan: " + ratePlanId);					
					}
					else
					{
						LOG.warn("Rate plan fees information is empty, skipping creation of rate plan fees details for rate plan id: "+ratePlanId);						
					}						
				}
				else
				{
					// If new rate plan fees is sent, update in rate_plan_fee table
					if(ratePlanFeeDetails != null)
					{						
						// Update rate plan fees
						updateRatePlanFees(operatorId, ratePlanId, ratePlanFeeDetails, requestedDate, requestedUser, conn);	
						LOG.info("Updated rate plan fees information for the rate plan: " + ratePlanId);
					}		
					// If no rate plan fees info is sent, only delete old rate fees (if any) saved in rate_plan_fee table
					else
					{
						// Delete Old Rate Plan Fees Info
						deleteRatePlanFees(ratePlanId, conn);
						LOG.info("Rate Plan fees is not given, so rate plan fees is not set for rate plan id: "+ratePlanId);
					}
				}			
				
				LOG.info("Update rate plan fees information completed for non-tiered rate plan: " + ratePlanId);
			}
			// Update tiers for a tiered rate plan
			// For a tiered rate plan, fees inferred from the rate plans that are 
			// configured as tiers 
			else
			{
				// If this rate plan was a non-tiered rate plan previously, clear fees information
				if(ratePlan.getRatePlanType() != RatePlanType.TIERED)
				{
					// Delete Rate Plan Fees Info
					deleteRatePlanFees(ratePlanId, conn);
					LOG.info("Changed from non-tiered to tiered rate plan: Deleted old rate plan fees for the rate plan: " + ratePlanId);
				}
				
				// Update Tiers Info
				updateRatePlanTiers(operatorId, ratePlanId, ratePlanTiers, requestedDate, requestedUser, conn);
				LOG.info("Update rate plan tiers information completed for tiered rate plan: " + ratePlanId);
			}
			
			// Set up zone level rate plan configuration
			updateRatePlanZoneConfiguration(ratePlanId, zoneRatePlanSettings, requestedDate, requestedUser, conn);
			LOG.info("Update rate plan zone rating policy information completed for tiered rate plan: " + ratePlanId);
						
			// For non-tiered rate plans, map rate plan overages to zones
			if(ratePlanType == RatePlanType.NON_TIERED && success)
			{
				// Fetch overage zones
				Zone[] overageZones = zoneDAO.getOverageZonesByZoneSet(operatorId, zoneSetId, homeZoneId);
				
				// map rate plan to overage zones
				updateRatePlanOverageZones(ratePlanId, overageZones, requestedDate, requestedUser, conn);
			}
			
			// Commit the changes
			conn.commit();
			LOG.info("Committed updateRatePlan changes to db");

			// Return the rate plan created to the client
			ratePlan = new RatePlan();
			
			ratePlan.setRatePlanId(ratePlanId);
			ratePlan.setRatePlanLabel(ratePlanLabel);
			ratePlan.setRatePlanName(ratePlanName);
			ratePlan.setRatePlanType(ratePlanType);
			ratePlan.setPaymentType(paymentType);
			ratePlan.setAccessType(accessType);
			ratePlan.setPeriodType(periodType);
			ratePlan.setStatus(status);
			ratePlan.setAccessFeeMonths(accessFeeMonths);
			ratePlan.setExpireIncluded(expireIncluded);
			
			ratePlan.setProductId(productId);
			
			ratePlan.setTierCriteria(tierCriteria);
			ratePlan.setHomeZoneId(homeZoneId);
			ratePlan.setDescription(description);
			ratePlan.setSpecialNotes(specialNotes);
			
			ratePlan.setCarrierRatePlanId(carrierRatePlanId);
			ratePlan.setAccountId(accountId);
			
			ratePlan.setStartDate(new java.sql.Date(startDate.getTime()));				
			ratePlan.setEndDate(new java.sql.Date(endDate.getTime()));
			ratePlan.setCurrencyName(currencyName);	
			ratePlan.setIncludedPeriodMonths(includedPeriodMonths);
			ratePlan.setDevicePoolingPolicyId(devicePoolingPolicyId);
			ratePlan.setPacketDataRoundingPolicyId(packetRoundingPolicyId);
			
			ratePlan.setRatePlanFeeDetails(ratePlanFeeDetails);
			ratePlan.setProvisionTriggerSettings(provisionTriggerSettings);
			ratePlan.setSuspendTriggerSettings(suspendTriggerSettings);
			ratePlan.setZoneRatePlanSettings(zoneRatePlanSettings);
			ratePlan.setRatePlanTiers(ratePlanTiers);
			//set the cancellation fee related changes 
			ratePlan.setAutoCancelAtTermEnd(cancelFeeAtTermEnd);
			ratePlan.setProRateCancellationFee(proRateCancellationFee);
			ratePlan.setCancellationFeeReductionInterval(cancellationFeeReductionInterval);
			ratePlan.setContractTerm(contractTerm);
			ratePlan.setAccountForDaysInProvisionState(accountForDaysInProvState);
			ratePlan.setAccountForDaysInSuspendState(accountForDaysInSuspendState);
			ratePlan.setRatePlanOverageBucket(ratePlanOverageBucket);
			ratePlan.setZoneSetId(zoneSetId);
            ratePlan.setTechnology(technology);
            ratePlan.setWaterfallEnabled(waterfallEnabled);
            ratePlan.setOperatorId(operatorId);
			// Save in Cache
			putInCache(ratePlan);
			
			LOG.info("Returning rate plan updated...");
		} catch (SQLException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new RatePlanException("Create new rate plan failed due to db error", e);
		} catch (ZoneNotFoundException e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw e;
		} catch (BadStartDateException e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw e;
		} catch (DuplicateZoneException e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw e;
		} catch (DuplicateRatePlanException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw e;
		} catch (SubRatePlanNotFoundException e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw e;
		}  catch (Exception e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw new RatePlanException("Update rate plan failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn);
		}

		LOG.info("Returned rate plan to the caller: " + ratePlan);		
		
		return ratePlan;
	}

	private void updateRatePlanZoneConfiguration(long ratePlanId, List<ZoneRatePlan> zoneRatePlanSettings, Date requestedDate, String requestedUser,
			Connection conn) throws SQLException, ZoneNotFoundException, DuplicateZoneException {
		// Delete previous zone configuration overrides
		deleteRatePlanZoneConfiguration(ratePlanId, conn);		
		LOG.info("Deleted old rate plan zones configuration for the Rate Plan: " + ratePlanId);
		
		// Set rate plan zone rating policy information
		if(zoneRatePlanSettings != null)
		{
			// Save new zone configuration overrides
			createRatePlanZoneConfiguration(ratePlanId, zoneRatePlanSettings, requestedDate, requestedUser, conn);
			LOG.info("Updated zones configuration for the Rate Plan: " + ratePlanId);
		}
		else
		{
			LOG.warn("Rate Plan Zone Rating Policy information is empty, so rate plan zone rating policy overrides are not set for rate plan id: "+ratePlanId);
		}
	}

	private void updateRatePlanTiers(int operatorId, long ratePlanId, List<RatePlanTier> ratePlanTiers, Date requestedDate, String requestedUser,
		Connection conn) throws SQLException, RatePlanException, SubRatePlanNotFoundException {
		// Delete old tier information
		deleteRatePlanTiers(ratePlanId, conn);
		
		// Set rate plan tier information
		if(ratePlanTiers != null)
		{
			createRatePlanTiers(operatorId, ratePlanId, ratePlanTiers, requestedDate, requestedUser, conn);				
			LOG.info("Updated rate plan tiers information for tiered rate plan: " + ratePlanId);	
		}
		else
		{
			LOG.warn("Rate Plan tiers is empty, so rate plan tiers are not set for rate plan id: "+ratePlanId);
		}
	}

	private void updateRatePlanTriggers(int operatorId, long ratePlanId, ProvisionTriggerSettings provisionTriggerSettings,
			SuspendTriggerSettings suspendTriggerSettings, Date requestedDate, String requestedUser, Connection conn) throws SQLException {
		LOG.info("Updating Rate Plan Trigger Info");

		PreparedStatement ps = null;
		int index = 0;
		int count = 0;

		try {
			LOG.debug("Creating Prepared Statement to update the rate plan triggers data in rate_plan_trigger table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_UPDATE_RATE_PLAN_TRIGGERS);

			LOG.debug("Setting Parameters");
			
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getDuration() : 0); // prov_duration
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getDurationUnit().getValue() : DurationUnit.MONTHS.getValue());// prov_duration_unit
			ps.setLong(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getPacketThreshold() : 0);// prov_packet_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getPacketThresholdUnit().getValue() : PacketUnit.KB.getValue()); // prov_packet_threshold_unit
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getMoSmsThreshold() : 0);// prov_mo_sms_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getMtSmsThreshold() : 0);// prov_mt_sms_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getMoVoiceMinsThreshold() : 0);// prov_mo_voice_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getMtVoiceMinsThreshold() : 0);// prov_mt_voice_threshold
			ps.setInt(++index, provisionTriggerSettings != null ? provisionTriggerSettings.getTrafficDays() : 0); // prov_traffic_days
			
			// Set Suspend State Triggers
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getDuration() : 0); // susp_duration
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getDurationUnit().getValue() : DurationUnit.MONTHS.getValue());// susp_duration_unit
			ps.setLong(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getPacketThreshold() : 0);// susp_packet_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getPacketThresholdUnit().getValue() : PacketUnit.KB.getValue()); // susp_packet_threshold_unit
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getMoSmsThreshold() : 0);// susp_mo_sms_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getMtSmsThreshold() : 0);// susp_mt_sms_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getMoVoiceMinsThreshold() : 0);// susp_mo_voice_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getMtVoiceMinsThreshold() : 0);// susp_mt_voice_threshold
			ps.setInt(++index, suspendTriggerSettings != null ? suspendTriggerSettings.getTrafficDays() : 0); // susp_traffic_days	
			
			// Set Provision State Fees
			ps.setDouble(++index, provisionTriggerSettings.getPerMTSmsPrice());// prov_mt_sms_cost
			ps.setDouble(++index, provisionTriggerSettings.getPerMinMTVoicePrice());// prov_mt_voice_cost
			ps.setDouble(++index, provisionTriggerSettings.getPerMOSmsPrice());// prov_mo_sms_cost
			ps.setDouble(++index, provisionTriggerSettings.getPerMinMOVoicePrice());// prov_mo_voice_cost
			ps.setDouble(++index, provisionTriggerSettings.getPerKBPacketPrice());// prov_packet_cost
			
			// Set Suspend State Fees
			ps.setDouble(++index, suspendTriggerSettings.getPerMTSmsPrice());// susp_mt_sms_cost
			ps.setDouble(++index, suspendTriggerSettings.getPerMinMTVoicePrice());// susp_mt_voice_cost
			ps.setDouble(++index, suspendTriggerSettings.getPerMOSmsPrice());// susp_mo_sms_cost
			ps.setDouble(++index, suspendTriggerSettings.getPerMinMOVoicePrice());// susp_mo_voice_cost
			ps.setDouble(++index, suspendTriggerSettings.getPerKBPacketPrice());// susp_packet_cost
						
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
			ps.setString(++index, requestedUser);// last_modified_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp
			
			// Set rate plan id to update
			ps.setLong(++index, ratePlanId);// rate_plan_id

			LOG.debug("Updating rate plan triggers info in the db");

			// Update rate plan triggers info into the db
			count = ps.executeUpdate();
			
			// If there is no existing rate plan trigger settings available, create them
			if(count <= 0)
			{
				LOG.info("No existing Rate Plan Triggers Info for rate plan id = " + ratePlanId);
				createRatePlanTriggers(operatorId, ratePlanId, provisionTriggerSettings, suspendTriggerSettings, requestedDate, requestedUser, conn);
			}

			LOG.info("Updated Rate Plan Triggers Info for rate plan id = " + ratePlanId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during updating rate plan trigger info in table rate_plan_trigger", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void updateRatePlanFees(int operatorId, long ratePlanId, RatePlanFeeDetails ratePlanFeeDetails, Date requestedDate, String requestedUser, Connection conn) 
		throws SQLException {
		LOG.info("Updating Rate Plan Fees Info");

		PreparedStatement ps = null;
		int index = 0;
		int count = 0;
		
		try {
			LOG.debug("Creating Prepared Statement to update the rate plan fees data in rate_plan_fee table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_UPDATE_RATE_PLAN_FEES);

			LOG.debug("Setting Parameters");
			
			// Set Subscription Fees
			ps.setDouble(++index, ratePlanFeeDetails.getAccessFee()); // access_fee
			ps.setDouble(++index, ratePlanFeeDetails.getActivationFee());// activation_fee
			ps.setDouble(++index, ratePlanFeeDetails.getSuspendFee()); // suspend_fee
			ps.setDouble(++index, ratePlanFeeDetails.getUnsuspendFee());// unsuspend_fee
			ps.setDouble(++index, ratePlanFeeDetails.getDeactivationFee());// deactivation_fee
			ps.setDouble(++index, ratePlanFeeDetails.getReactivationFee());// reactivation_fee
			
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
			ps.setString(++index, requestedUser);// last_modified_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp

			// Set Rate Plan Id to Update
			ps.setLong(++index, ratePlanId);// rate_plan_id

			LOG.debug("Updating rate plan fees info in the db...");

			// Update rate plan fees info into the db
			count = ps.executeUpdate();
			
			// If there is no existing rate plan fees entry available, create them
			if(count <= 0)
			{
				LOG.info("No existing Rate Plan fees Info for rate plan id = " + ratePlanId);
				createRatePlanFees(operatorId, ratePlanId, ratePlanFeeDetails, requestedDate, requestedUser, conn);
			}

			LOG.info("Updated Rate Plan Fees Info for rate plan id = " + ratePlanId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during updating rate plan fees info in table rate_plan_fee", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void updateRatePlanInfo(int operatorId, long ratePlanId, String ratePlanLabel, RatePlanType ratePlanType, RatePlanPaymentType paymentType,
			RatePlanAccessType accessType, int accessFeeMonths, boolean expireIncluded, RatePlanStatus status, boolean overrideRoamingIncluded, int productId, 
			int tierCriteria, int homeZoneId, String description, String specialNotes, int carrierRatePlanId, long accountId, Date startDate, Date endDate, 
			String currencyName, int includedPeriodMonths, int devicePoolingPolicyId, int packetRoundingPolicyId, Date requestedDate, String requestedUser,
			int proRateCancellationFee, int contractTerm, int cancellationFeeReductionInterval,
			int cancelFeeAtTermEnd, int accountForDaysInProvState,
			int accountForDaysInSuspendState, boolean roamingIncluded, int zoneSetId, String technology, boolean waterfallEnabled, long wholesaleRateplanId, boolean isWholesaleRatePlan, 
			Connection conn) throws SQLException, DuplicateRatePlanException {
		LOG.info("Updating basic Rate Plan Info");

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to update the rate plan data in rate_plan table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_UPDATE_RATE_PLAN_INFO);
            
            if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                ps = conn.prepareStatement(SQL_UPDATE_RATE_PLAN_INFO_2);
            }

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setString(++index, ratePlanLabel); // rate_plan_label
			
			// Set Tiered or Non-Tiered Plan
			ps.setInt(++index, ratePlanType.getValue());// rate_plan_type
			
			// Set Pre-Paid or Post-Paid Plan
			ps.setInt(++index, paymentType.getValue());// payment_type
			
			ps.setInt(++index, productId);// product_id
			
			// Set Tier Criteria if the rate plan is tiered
			if(ratePlanType == RatePlanType.TIERED && tierCriteria > 0)
			{
				ps.setInt(++index, tierCriteria);// tier_criteria				
			}
			else
			{
				ps.setNull(++index, Types.INTEGER);// tier_criteria		
			}
						
			// Set Monthly or Annual Rate Plan
			ps.setInt(++index, accessFeeMonths);// access_fee_duration_months
			
			//Expire Included
			ps.setInt(++index, expireIncluded ? 1 : 0);// expire_included
			
			// Set Shared or Exclusive Rate Plan
			ps.setInt(++index, accessType.getValue());// is_shared
			
			ps.setInt(++index, homeZoneId);// home_zone_id
			ps.setString(++index, description);// description
			ps.setString(++index, specialNotes);// special_notes
			
			// Set carrier rate plan id
			ps.setInt(++index, carrierRatePlanId);// carrier_rate_plan_id				
						
			// Set account id if the rate plan is exclusive
			if(accessType == RatePlanAccessType.EXCLUSIVE && accountId > 0)
			{
				ps.setLong(++index, accountId);// account_id				
			}
			else
			{
				ps.setLong(++index, Types.INTEGER);// account_id		
			}
			
			ps.setInt(++index, status.getValue());// status
			ps.setInt(++index, overrideRoamingIncluded ? 1 : 0);// override_roaming_included
			ps.setDate(++index, new java.sql.Date(startDate.getTime()));// start_date
			ps.setDate(++index, new java.sql.Date(endDate.getTime()));// end_date
			ps.setString(++index, currencyName);// currency_code
			ps.setInt(++index, includedPeriodMonths); // included_period_months
			
			// Set Device Pooling Policy
			if(devicePoolingPolicyId != 0)
			{
				ps.setInt(++index, devicePoolingPolicyId); // device_policy_id
			}
			else
			{
				ps.setNull(++index, Types.INTEGER);// device_policy_id
			}
			
			// Set Packet Data Rounding Policy
			if(packetRoundingPolicyId != 0)
			{
				ps.setInt(++index, packetRoundingPolicyId); // packet_rounding_policy_id
			}
			else
			{
				ps.setNull(++index, Types.INTEGER);// packet_rounding_policy_id
			}
			
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
			ps.setString(++index, requestedUser);// last_modified_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp
			
			//update the ratePlan with Cancellation fee details as well
			ps.setInt(++index, proRateCancellationFee);
			ps.setInt(++index, contractTerm);
			ps.setInt(++index, cancellationFeeReductionInterval);
			ps.setInt(++index, cancelFeeAtTermEnd);
			ps.setInt(++index, accountForDaysInProvState);
			ps.setInt(++index, accountForDaysInSuspendState);
            //Roaming Included
			ps.setInt(++index, roamingIncluded ? 1 : 0);// roamingIncluded
			ps.setInt(++index, zoneSetId);
            ps.setString(++index, technology);
            ps.setInt(++index, BooleanUtils.toInteger(waterfallEnabled));
            ps.setInt(++index, BooleanUtils.toInteger(isWholesaleRatePlan));
            
            if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                ps.setInt(++index, operatorId);
            }
            
            ps.setLong(++index, wholesaleRateplanId);
			// Set Rate Plan Id to Update
			ps.setLong(++index, ratePlanId);// rate_plan_id

			LOG.debug("Updating rate plan basic info in the db...");

			// Update rate plan info into the db
			ps.executeUpdate();

			LOG.info("Updated Rate Plan Info for rate plan id = " + ratePlanId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during updating rate plan info in table rate_plan", sqle);
						
			if(StringUtils.contains(sqle.getMessage(), "RATE_PLAN_LABEL_UK"))
			{
				throw new DuplicateRatePlanException("The Rate Plan Label and Product combination already exists");
			}
			
			if(StringUtils.contains(sqle.getMessage(), "RATE_PLAN_NAME_UK"))
			{
				throw new DuplicateRatePlanException("The Rate Plan Name and Product combination already exists");
			}
			
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	@Override
	public boolean updateRatePlanStatus(int operatorId, long ratePlanId, RatePlanStatus status, String specialComments, Date requestedDate, String requestedUser, Date newEndDate)	
		throws RatePlanNotFoundException, RatePlanException, AccountRatePlanMappingException{
		Connection conn = null;
		RatePlan ratePlan = null;
		boolean success = false;

		LOG.info("updateRatePlanStatus: processing update rate plan status...");
		
		try {
			LOG.info("Updating rate plan status for rate plan id: "+ratePlanId+" with status: " + status.name());
			
			// Verify that the rate plan does not have associated account rate plans
			checkAssociatedAccountRatePlans(ratePlanId);
			
			// Validate and Fetch the rate plan from cache
			ratePlan = validateAndReturnRatePlan(ratePlanId);
			
			// Get Prov11 Connection
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			
			// Set Auto Commit as false
			conn.setAutoCommit(false);
			
			// Update Rate Plan Status in DB
			success = setRatePlanStatus(operatorId, ratePlanId, status, specialComments, requestedDate, requestedUser, newEndDate, conn);
			
			// If success, do post success actions
			if(success)
			{
				if(status == RatePlanStatus.APPROVED)
				{
					LOG.info("Rate plan id: "+ratePlanId+" has been approved successfully.");
				}
				else if(status == RatePlanStatus.EXPIRED)
				{
					LOG.info("Rate plan id: "+ratePlanId+" has been invalidated successfully.");
				}
				else if(status == RatePlanStatus.PENDING)
				{
					LOG.info("Rate plan id: "+ratePlanId+" changed to status: "+status+" successfully.");
				}
				
				// For Non Tiered Plans and Tiered Rate Plans, update csp rate plan 
				if(ratePlan.getRatePlanType() == RatePlanType.NON_TIERED || ratePlan.getRatePlanType() == RatePlanType.TIERED)
				{
					if(status == RatePlanStatus.APPROVED)
					{
						// If this is an exclusive rate plan, assign to the account automatically in csp_rate_plan
						if(ratePlan.getAccessType() == RatePlanAccessType.EXCLUSIVE)
						{
							long accountId = ratePlan.getAccountId();
							int carrierRatePlanId = ratePlan.getCarrierRatePlanId();
							Date startDate = ratePlan.getStartDate();
							Date endDate = ratePlan.getEndDate();
							
							LOG.info("Rate plan id: "+ratePlanId+" is an exclusive rate plan. Performing automatic assignment of rate plan to account id: "+accountId);
							
							boolean assignSuccess = assignRatePlanToAccount(operatorId, accountId, ratePlanId, carrierRatePlanId, startDate, endDate, "Exclusive Rate Plan for account id: "+accountId, 
									requestedDate, requestedUser);
							
							if(assignSuccess)
							{
								LOG.info("Rate plan id: "+ratePlanId+" is successfully assigned to the account id: "+accountId);
							}
							else
							{
								LOG.warn("Failed to assign Rate plan id: "+ratePlanId+" to the account id: "+accountId);
							}
						}
                        if (newEndDate != null) {
                            if (!reactivateCSPRatePlan(operatorId, ratePlanId, requestedDate, requestedUser, newEndDate, conn)) {
                                LOG.warn("Reactivation of rate plan " + ratePlanId + " in table csp_rate_plan is failed.");
                            }
                        }
					} 
					else if(status == RatePlanStatus.EXPIRED)
					{
						// Invalidate CSP Rate plan
						boolean invalidateSuccess = invalidateRatePlan(operatorId, ratePlanId, requestedDate, requestedUser, conn);
						
						if(invalidateSuccess)
						{
							LOG.info("Rate plan id: "+ratePlanId+" is successfully invalidated");
						}
						else
						{
							LOG.warn("Failed to invalidate Rate plan id: "+ratePlanId);
						}
					}
				}
			}
						
			// Commit the changes
			conn.commit();
			LOG.info("Committed updateRatePlanStatus changes to db");

			// Set the rate plan status
			ratePlan.setStatus(status);

			//Update End Date in cache
			if(status == RatePlanStatus.EXPIRED)
			{
				ratePlan.setEndDate(new java.sql.Date(requestedDate.getTime()));
			} else if (newEndDate != null) {
                ratePlan.setEndDate(new java.sql.Date(newEndDate.getTime()));
            }
			
			// Save in Cache
			putInCache(ratePlan);
			
			success = true;
			
			LOG.info("Returning rate plan updated...");
		} catch (AccountRatePlanMappingException e) {
			throw e;
		} catch (SQLException e) {
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new RatePlanException("Create new rate plan failed due to db error", e);
		} catch (Exception e) {
			if(conn != null)
			{
				try 
				{
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}	
			
			throw new RatePlanException("Create new rate plan failed due to fatal error", e);
		} finally {
			DBUtils.cleanup(conn);
		}

		LOG.info("Returned rate plan to the caller: " + ratePlan);		
		
		return success;
	}

	private void checkAssociatedAccountRatePlans(long ratePlanId) throws AccountRatePlanMappingException, RatePlanException {
		List<AccountRatePlan> ratePlans;
		
		try {
			// Fetch assigned rate plans by ratePlanId
			 ratePlans = getAssigneddRatePlansAcrossAccounts(ratePlanId);
		} catch (Exception e) {
			LOG.error("getAssigneddRatePlansAcrossAccounts Exception occurred :", e);
			throw new RatePlanException("Rate plan expiration failed due to db error", e);
		}
		
		if (ratePlans.size() != 0) {
			LOG.info("Cannot delete rate plan with associated accounts, number of associated accounts: " + ratePlans.size()); 
			String resposeMessage = buildResponseMessage(ratePlans);
			throw new AccountRatePlanMappingException(resposeMessage);
		} else {
			LOG.info("No assigned account rate plans found for ratePlanId: " + ratePlanId + ", proceeding with rate plan experation");
			return;
		}
	}

	/**
	 * Builds response message for when user attempts to expire a rate plan before expiring associated account rate plans 
	 */
	String buildResponseMessage(List<AccountRatePlan> ratePlans){
		int i = 0;
		String response = "You have accounts assigned to this rate plan. You must first expire rate plan ID(s): ";
		for (AccountRatePlan associatedAccountRatePlan : ratePlans) {
			if (i > 0){
				response += ", " + associatedAccountRatePlan.getAccountRatePlanId() + " on account: " + associatedAccountRatePlan.getAccountId();
			}else{
				response += associatedAccountRatePlan.getAccountRatePlanId() + " on account: " + associatedAccountRatePlan.getAccountId();
			}
			i++;
		}
		return response += ".";
	}
	
	private boolean setRatePlanStatus(int operatorId, long ratePlanId, RatePlanStatus status, String specialComments, Date requestedDate, String requestedUser, Date newEndDate, Connection conn) 
		throws SQLException {
		LOG.info("Updating Rate Plan Status for rate plan id: "+ratePlanId+" to status: "+status.name());

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to update the rate plan status in rate_plan table");

			// Create SQL Statement
			ps = conn.prepareStatement(status != RatePlanStatus.EXPIRED && newEndDate == null ? SQL_UPDATE_RATE_PLAN_STATUS : SQL_UPDATE_RATE_PLAN_STATUS_WITH_END_DATE);

			LOG.debug("Setting Parameters");
			
			if(status != RatePlanStatus.EXPIRED && newEndDate == null)
			{
				ps.setInt(++index, status.getValue());// status
				ps.setString(++index, specialComments);// special_notes
				ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
				ps.setString(++index, requestedUser);// last_modified_by
				ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp
			}
			else
			{
				ps.setInt(++index, status.getValue());// status
                if (newEndDate != null) {
                    ps.setDate(++index, new java.sql.Date(newEndDate.getTime()));// end_date
                } else {
                    ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));// end_date
                }
				ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
				ps.setString(++index, requestedUser);// last_modified_by
				ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp
			}
			
			// Set Rate Plan Id to Update Status
			ps.setLong(++index, ratePlanId);// rate_plan_id

			LOG.debug("Updating rate plan status in the db...");

			// Update rate plan info into the db
			int count = ps.executeUpdate();
			
			// Success
			if(count > 0)
			{
				return true;
			}
			
			LOG.info("Updated Rate Plan Status for rate plan id = " + ratePlanId + " Successfully");
		} catch (SQLException sqle) {
			LOG.error("Exception during updating rate plan status in table rate_plan", sqle);			
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
		
		return false;
	}

	
	private void deleteRatePlanFees(long ratePlanId, Connection conn) throws SQLException {
		LOG.info("Deleting Rate Plan Fees Info for rate plan id: "+ ratePlanId);

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to delete the data in rate_plan_fee table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_RATE_PLAN_FEES);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(++index, ratePlanId);// rate_plan_id

			LOG.debug("Deleting rate plan fees info in the db");

			// Delete rate plan fees info in the db
			int count = ps.executeUpdate();

			LOG.info("Deleted Rate Plan Fees Info for rate plan id = " + ratePlanId + " Successfully, no of records deleted: "+count);
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting rate plan fees info in table rate_plan_fee", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void deleteRatePlanTiers(long ratePlanId, Connection conn) throws SQLException {
		LOG.info("Deleting Rate Plan Tiers Info for rate plan id: "+ ratePlanId);

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to delete the data in rate_plan_tier table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_RATE_PLAN_TIERS);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(++index, ratePlanId);// rate_plan_id

			LOG.debug("Deleting rate plan tiers info in the db");

			// Delete rate plan fees info in the db
			int count = ps.executeUpdate();

			LOG.info("Deleted Rate Plan Tiers Info for rate plan id = " + ratePlanId + " Successfully, no of records deleted: "+count);
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting rate plan tiers info in table rate_plan_tier", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void deleteRatePlanZoneConfiguration(long ratePlanId, Connection conn) throws SQLException {
		LOG.info("Deleting Rate Plan Zone Policy information for rate plan id: "+ ratePlanId);

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to delete the data in rate_plan_zone_rating_policy table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_RATE_PLAN_ZONE_POLICY);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(++index, ratePlanId);// rate_plan_id

			LOG.debug("Deleting rate plan zone configuration info in the db");

			// Delete rate plan zone configuration info in the db
			int count = ps.executeUpdate();

			LOG.info("Deleted Rate Plan Zone Policy information for rate plan id = " + ratePlanId + " Successfully, no of records deleted: "+count);
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting rate plan zone configuration info in table rate_plan_zone_rating_policy", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void deleteRatePlanTriggers(long ratePlanId, Connection conn) throws SQLException {
		LOG.info("Deleting Rate Plan Triggers Info for rate plan id: "+ ratePlanId);

		PreparedStatement ps = null;
		int index = 0;

		try {
			LOG.debug("Creating Prepared Statement to delete the data in rate_plan_trigger table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_RATE_PLAN_TRIGGERS);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setLong(++index, ratePlanId);// rate_plan_id

			LOG.debug("Deleting rate plan triggers info in the db");

			// Delete rate plan fees info in the db
			int count = ps.executeUpdate();

			LOG.info("Deleted Rate Plan Triggers Info for rate plan id = " + ratePlanId + " Successfully, no of records deleted: "+count);
		} catch (SQLException sqle) {
			LOG.error("Exception during deleting rate plan triggers info in table rate_plan_trigger", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
	}

	private void validateRatePlanAndStartDate(long ratePlanId, Date startDate) throws RatePlanNotFoundException, BadStartDateException {
		RatePlan ratePlan = validateAndReturnRatePlan(ratePlanId);
		
		// Check the start date is unaltered
		if(!startDate.equals(ratePlan.getStartDate()))
		{	
			// If startDate is altered, if it in the past, then throw bad request
			if(!oaUtils.isFutureDate(startDate))
			{
				LOG.error("The startDate cannot be in the past, startDate: "+startDate);
				throw new BadStartDateException("The startDate cannot be in the past");
			}
			else
			{
				LOG.info("The startDate is validated as a future date");					
			}
		}
		else
		{
			LOG.info("The startDate has not been changed, current start date value: "+ratePlan.getStartDate()+", selected start date value: "+new java.sql.Date(startDate.getTime()));					
		}
	}
	
	
	private void validateAssignedRatePlan(long ratePlanId, String ratePlanName, Connection conn) throws RatePlanAlreadyAssignedException {
		String query = "select count(*) from aerbill_prov.account_rate_plan_v1 where csp_rate_plan_id = ?  and end_date > SYSGMTDATE " ;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean ratePlanAssigned = false;
		try {	
			ps = conn.prepareStatement(query);
			ps.setLong(1, ratePlanId);
			LOG.info("validateAssignedRatePlan: validating if rate plan is already associated with accounts: " + ratePlanId);
			rs = ps.executeQuery();

			if (rs.next()) {
				ratePlanAssigned = (rs.getInt(1) > 0);
				if(ratePlanAssigned) {
					LOG.error("Rate plan already assigned to one or more accounts, ratePlanId="+ ratePlanId);		
					throw new RatePlanAlreadyAssignedException(String.format("Rate plan %s already assigned to one or more accounts", ratePlanName));
				}
			}
		} catch (SQLException e) {
			LOG.error("Exception during getZoneNameCount - validateZoneOverride", e);
			throw new GenericServiceException("Exception during validateZoneOverride", e);
		} finally {
			DBUtils.cleanup(null, ps, rs);
		}
	}

	private RatePlan validateAndReturnRatePlan(long ratePlanId) throws RatePlanNotFoundException {
		RatePlan ratePlan = ratePlanCache.get(ratePlanId);
		
		if(ratePlan == null)
		{
			throw new RatePlanNotFoundException("Rate Plan: "+ratePlanId+" not found");
		}
		
		return ratePlan;
	}
	
	//TODO
	
	private static final String QUERY_ACCOUNT_RATE_PLAN_MAP = "SELECT count(*) cnt FROM aerisgen.account_rate_plan_map WHERE rate_plan_id = ? and account_id = ?";
	
	private static final String INSERT_ACCOUNT_RATE_PLAN_MAP = "INSERT INTO aerisgen.account_rate_plan_map " +
			"(RATE_PLAN_ID, ACCOUNT_ID, RATE_PLAN_LABEL, STATUS_ID, START_DATE, END_DATE, CREATED_DATE, CREATED_BY, LAST_MODIFIED_DATE, LAST_MODIFIED_BY, REP_TIMESTAMP) " +
			"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String UPDATE_ACCOUNT_RATE_PLAN_MAP = "update aerisgen.account_rate_plan_map SET RATE_PLAN_LABEL = ?, STATUS_ID = ?, START_DATE = ?, END_DATE = ?, LAST_MODIFIED_DATE = ?, LAST_MODIFIED_BY = ?, REP_TIMESTAMP = ?" 
			+ " WHERE RATE_PLAN_ID = ? AND ACCOUNT_ID = ?";

	
	private int insertInAccountRatePlanMap(long accountId, long ratePlanId, String ratePlanLabel, Date startDate, Date endDate, Date requestedDate, String requestedBy) throws DAOException {
		Connection conn= null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement(QUERY_ACCOUNT_RATE_PLAN_MAP);
			ps.setLong(1, ratePlanId);
			ps.setLong(2, accountId);
			rs = ps.executeQuery();
			boolean found = false;
			if (rs.next()) {
				int count = rs.getInt("cnt");
				found = (count > 0);
			}
			DBUtils.cleanup(null, ps, rs);
			if (!found) {
				ps = conn.prepareStatement(INSERT_ACCOUNT_RATE_PLAN_MAP);
				ps.setLong(1, ratePlanId);
				ps.setLong(2, accountId);
				ps.setString(3, ratePlanLabel);
				ps.setInt(4, RatePlanStatus.APPROVED.getValue());
				ps.setDate(5,  new java.sql.Date(startDate.getTime()));
				ps.setDate(6,  new java.sql.Date(endDate.getTime()));
				ps.setTimestamp(7,  new java.sql.Timestamp(requestedDate.getTime()));
				ps.setString(8,  requestedBy.substring(0, Math.min(100, requestedBy.length())));
				ps.setTimestamp(9,  new java.sql.Timestamp(requestedDate.getTime()));
				ps.setString(10,  requestedBy.substring(0, Math.min(100, requestedBy.length())));
				ps.setTimestamp(11,  new java.sql.Timestamp(requestedDate.getTime()));
			} else {
				ps = conn.prepareStatement(UPDATE_ACCOUNT_RATE_PLAN_MAP);
				ps.setString(1, ratePlanLabel);
				ps.setInt(2, RatePlanStatus.APPROVED.getValue());
				ps.setDate(3,  new java.sql.Date(startDate.getTime()));
				ps.setDate(4,  new java.sql.Date(endDate.getTime()));
				ps.setTimestamp(5,  new java.sql.Timestamp(requestedDate.getTime()));
				ps.setString(6,  requestedBy.substring(0, Math.min(100, requestedBy.length())));
				ps.setTimestamp(7,  new java.sql.Timestamp(requestedDate.getTime()));
				ps.setLong(8, ratePlanId);
				ps.setLong(9, accountId);
			}
			
			int updated = ps.executeUpdate();
			LOG.debug("Inserted {} records in account_rate_plan_map. AccountID {}, RatePlanId {}", updated, accountId, ratePlanId);
			return updated;
		} catch (SQLException ex) {
			LOG.error("Failed to insert in account_rate_plan_map. AccountID {}, RatePlanId {}", ex);
			throw new DAOException("Failed to insert in account_rate_plan_map.");
		} finally {
			DBUtils.cleanup(conn, ps, null);
		}
	}
	
	private static final String EXPIRE_ACCOUNT_RATE_PLAN_MAP = "UPDATE aerisgen.account_rate_plan_map SET END_DATE = ?, REP_TIMESTAMP = ?, " +
			" LAST_MODIFIED_DATE = ?, LAST_MODIFIED_BY = ?, STATUS_ID = ? WHERE ACCOUNT_ID = ? AND RATE_PLAN_ID = ?";

	private int expireInAccountRatePlanMap(long accountId, long ratePlanId, Date requestedDate, String requestedBy) throws DAOException {
		Connection conn= null;
		PreparedStatement ps = null;
		
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();
			
			ps = conn.prepareStatement(EXPIRE_ACCOUNT_RATE_PLAN_MAP);
			ps.setDate(1,  new java.sql.Date(requestedDate.getTime()));
			ps.setTimestamp(2,  new java.sql.Timestamp(requestedDate.getTime()));
			ps.setTimestamp(3,  new java.sql.Timestamp(requestedDate.getTime()));
			ps.setString(4,  requestedBy.substring(0, Math.min(100, requestedBy.length())));
			ps.setInt(5, RatePlanStatus.EXPIRED.getValue());
			ps.setLong(6, accountId);
			ps.setLong(7, ratePlanId);
			
			int updated = ps.executeUpdate();
			LOG.debug("Updated {} records in account_rate_plan_map. AccountID {}, RatePlanId {}", updated, accountId, ratePlanId);
			return updated;
		} catch (SQLException ex) {
			LOG.error("Failed to update account_rate_plan_map. AccountID {}, RatePlanId {}", ex);
			throw new DAOException("Failed to update account_rate_plan_map.");
		} finally {
			DBUtils.cleanup(conn, ps, null);
		}
	}
	
	private void deleteAccountRatePlan(long accountId, long ratePlanId) throws RatePlanException{
		Connection conn = null;
		PreparedStatement ps = null;
		try{
			conn = DBConnectionManager.getInstance().getProvisionDatabaseConnection();
			ps = conn.prepareStatement(SQL_DELETE_ACCOUNT_RATE_PLAN);
			ps.setLong(1, accountId);
			ps.setLong(2, ratePlanId);
			
			ps.executeUpdate();
		}catch(Exception e){
			if(conn != null)
			{
				try {
					conn.rollback();
					LOG.info("Rolled back changes due to previous exception " + e);
				} catch (SQLException e1) {
					LOG.error("Exception during roll back " + e1);
				}
			}
			
			throw new RatePlanException("Error while deleting account rate plan ", e);
		}finally{
			DBUtils.cleanup(conn, ps);
		}
	}

	@Override
	public boolean assignRatePlanToAccount(int operatorId, long accountId, long ratePlanId, int carrierRatePlanId, Date startDate, Date endDate, String description,
			Date requestedDate, String requestedUser) throws RatePlanNotFoundException, RatePlanException, DuplicateRatePlanException {
		LOG.info("assignRatePlan: calling assignRatePlanToAccountStoredProcedure");
		ResultStatus status = null;
		
		try {
			// Assign with start date
			status = assignRatePlanToAccountStoredProcedure.execute(ratePlanId, accountId, carrierRatePlanId, new java.sql.Date(startDate.getTime()), requestedUser);
			
			if(status.getResultCode() == 0)
			{
				LOG.info("assignRatePlan: assignRatePlanToAccountStoredProcedure: success");
				
				AccountRatePlan accountRatePlan = getAccountRatePlan(operatorId, accountId, ratePlanId);
				
				// Update with end date
				//TODO change this to perform update only if end date is different than 2099-12-31
				LOG.info("assignRatePlan: calling endRatePlanToAccountStoredProcedure");
				status = expireAccountRatePlanStoredProcedure.execute(accountRatePlan.getAccountRatePlanId(), new java.sql.Date(endDate.getTime()), requestedUser);
				
				if(status.getResultCode() == 0)
				{
					LOG.info("assignRatePlan: endRatePlanToAccountStoredProcedure: success");	
					accountRatePlan.setEndDate(new java.sql.Date(endDate.getTime()));
					
				}
				//Insert in new account_rate_plan_map table.
				insertInAccountRatePlanMap(accountId, ratePlanId, accountRatePlan.getRatePlan().getRatePlanLabel(), startDate, endDate, requestedDate, requestedUser);
				LOG.info("assignRatePlan: insertInAccountRatePlanMap: success");
				
				// Store is cache
				if(accountRatePlanCache.getKeys().contains(accountId))
				{
					putInCache(accountId, accountRatePlan);
				}
			}
			else
			{	
				// rollback changes done by assignRatePlanToAccountStoredProcedure
				deleteAccountRatePlan(accountId, ratePlanId);
				throw new DuplicateRatePlanException("Rate Plan: "+ratePlanId+" is already assigned to account: "+accountId);
			}
		} catch (DAOException e) {
			throw new RatePlanException("Error assigning rate plan: "+ratePlanId+" to account: "+accountId, e);
		} catch (StoredProcedureException e) {
			LOG.error("StoredProcedureException while assigning rate plan: "+ratePlanId+" to account: "+accountId);
			throw new RatePlanException("Error assigning rate plan: "+ratePlanId+" to account: "+accountId, e);
		}
		
		LOG.info("assignRatePlan: complete");
		return true;
	}

	@Override
	public boolean invalidateAccountRatePlan(int operatorId, long accountId, long ratePlanId, Date requestedDate, String requestedUser)
			throws RatePlanNotFoundException, RatePlanException {
		LOG.info("invalidateAccountRatePlan: calling invalidateAccountRatePlan");
		ResultStatus status = null;
		
		try {
			LOG.info("invalidateAccountRatePlan: Validating account and rate plan exists or not");
			AccountRatePlan accountRatePlan = getAccountRatePlan(operatorId, accountId, ratePlanId);
			
			if(accountRatePlan == null)
			{
				LOG.error("invalidateAccountRatePlan failed: The rate plan does not exist for the account");
				throw new RatePlanNotFoundException("Rate Plan: "+ratePlanId+" not found for account: "+accountId);
			}
			
			status = expireAccountRatePlanStoredProcedure.execute(accountRatePlan.getAccountRatePlanId(), new java.sql.Timestamp(requestedDate.getTime() - (24 * 60 * 60 * 1000)), requestedUser);
			
			if(status.getResultCode() == 0)
			{
				LOG.info("invalidateAccountRatePlan: success");		
				
				//Expire in account_rate_plan_map
				expireInAccountRatePlanMap(accountId, ratePlanId, requestedDate, requestedUser);
				LOG.info("invalidateAccountRatePlan: expireInAccountRatePlanMap: success");
				// Remove from cache
				if(accountRatePlanCache.getKeys().contains(accountId))
				{
					evictFromCache(accountId, accountRatePlan.getAccountRatePlanId());
				}		
			}
			else
			{
				throw new RatePlanException("invalidateAccountRatePlan failed with status code: "+status.getResultCode());
			}
		} catch (DAOException e) {
			throw new RatePlanException("Error expiring rate plan: "+ratePlanId+" to account: "+accountId, e);
		} catch (StoredProcedureException e) {
			LOG.error("StoredProcedureException while expiring rate plan: "+ratePlanId+" to account: "+accountId);
			throw new RatePlanException("Error expiring rate plan: "+ratePlanId+" to account: "+accountId, e);
		}
		
		LOG.info("invalidateAccountRatePlan: complete");
		return true;
	}

	private boolean invalidateRatePlan(int operatorId, long ratePlanId, Date requestedDate, String requestedUser, Connection conn)
			throws RatePlanNotFoundException, RatePlanException, SQLException {
		LOG.info("invalidateRatePlan: calling invalidateRatePlan");
		PreparedStatement ps = null;
		int index = 0;
		boolean success = false;

		try {
			LOG.info("invalidateRatePlan: Validating rate plan exists or not");
			RatePlan ratePlan = getRatePlan(operatorId, ratePlanId);
			
			if(ratePlan == null)
			{
				LOG.error("invalidateRatePlan failed: The rate plan does not exist");
				throw new RatePlanNotFoundException("Rate Plan: "+ratePlanId+" not found for operator: "+operatorId);
			}
			
			LOG.debug("Creating Prepared Statement to invalidate the rate plan in csp_rate_plan table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_INVALIDATE_RATE_PLAN);

			LOG.debug("Setting Parameters");

			// Set Parameters
			ps.setDate(++index, new java.sql.Date(requestedDate.getTime()));// end_date
			ps.setString(++index, requestedUser);// last_modified_by
			ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date

			// Set the rate plan id to invalidate
			ps.setLong(++index, ratePlanId);// rate_plan_id
			
			LOG.debug("Invalidating rate plan in the db");

			// Invalidate rate plan in the db
			int count = ps.executeUpdate();

			if(count > 0)
			{
				LOG.info("Invalidated rate plan id = " + ratePlanId + " in csp_rate_plan table Successfully");				
				success = true;
			}
			else
			{
				LOG.warn("Invalidating rate plan id = " + ratePlanId + " in csp_rate_plan table failed");
			}
		} catch (SQLException sqle) {
			LOG.error("Exception during invalidating rate plan in table csp_rate_plan", sqle);
			throw sqle;
		} finally {
			DBUtils.cleanup(null, ps);
		}
		
		return success;
	}	
    
    private boolean reactivateCSPRatePlan(int operatorId, long ratePlanId, Date requestedDate, String requestedUser, Date newEndDate, Connection conn)
            throws RatePlanNotFoundException, RatePlanException, SQLException {
        LOG.info("reactivateCSPRatePlan: calling reactivateCSPRatePlan");
        PreparedStatement ps = null;
        int index = 0;
        boolean success = false;
        try {
            LOG.info("reactivateCSPRatePlan: Validating rate plan exists or not");
            RatePlan ratePlan = getRatePlan(operatorId, ratePlanId);
            if (ratePlan == null) {
                LOG.error("reactivateCSPRatePlan failed: The rate plan does not exist");
                throw new RatePlanNotFoundException("Rate Plan: " + ratePlanId + " not found for operator: " + operatorId);
            }
            LOG.debug("Creating Prepared Statement to reactivate the rate plan in csp_rate_plan table");
            // Create SQL Statement
            ps = conn.prepareStatement(SQL_REACTIVATE_CSP_RATE_PLAN);
            LOG.debug("Setting Parameters");
            // Set Parameters
            ps.setDate(++index, new java.sql.Date(newEndDate.getTime()));// end_date
            ps.setString(++index, requestedUser);// last_modified_by
            ps.setTimestamp(++index, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
            // Set the rate plan id to reactivate
            ps.setLong(++index, ratePlanId);// rate_plan_id
            LOG.debug("Reactivating rate plan in the db");
            // Invalidate rate plan in the db
            int count = ps.executeUpdate();
            if (count > 0) {
                LOG.info("Reactivated rate plan id = " + ratePlanId + " in csp_rate_plan table Successfully");
                success = true;
            } else {
                LOG.warn("Reactivating rate plan id = " + ratePlanId + " in csp_rate_plan table failed");
            }
        } catch (SQLException sqle) {
            LOG.error("Exception during reactivating rate plan in table csp_rate_plan", sqle);
            throw sqle;
        } finally {
            DBUtils.cleanup(null, ps);
        }
        return success;
    }
	
	@Override
	public List<CarrierRatePlan> getCarrierRatePlans(int productId) throws RatePlanException {
		List<Carrier> carriers = productCarrierDAO.getCarriersByProduct(productId);
		List<CarrierRatePlan> carrierRatePlans = new ArrayList<CarrierRatePlan>();
								
		if(carriers != null && !carriers.isEmpty())
		{
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				DBConnectionManager connectionManager = DBConnectionManager.getInstance();
				conn = connectionManager.getProvisionDatabaseConnection();

				// Build query
				ps = buildGetCarrierRatePlansQuery(carriers, conn);
				
				// Execute Query
				rs = ps.executeQuery();
				
				while (rs.next()) {
					CarrierRatePlan carrierRatePlan = new CarrierRatePlan();
					
					carrierRatePlan.setRatePlanId(rs.getLong("rate_plan_id"));
					carrierRatePlan.setProductId(productId);
					carrierRatePlan.setCarrierId(rs.getInt("carrier_id"));
					carrierRatePlan.setRatePlanName(rs.getString("rate_plan_name"));
					carrierRatePlan.setStartDate(rs.getDate("start_date"));
					carrierRatePlan.setEndDate(rs.getDate("end_date"));
					carrierRatePlan.setInitialCharges(rs.getDouble("initial_charge"));
					carrierRatePlan.setMaxKb(rs.getDouble("max_size_in_kb"));
					carrierRatePlan.setMonthlyFee(rs.getDouble("monthly_fee"));				
					carrierRatePlan.setDescription(rs.getString("plan_description"));
					
					carrierRatePlans.add(carrierRatePlan);
				}	
			} catch (SQLException e) {
				LOG.error("SQLException during getCarrierRatePlans", e);
				throw new RatePlanException("Unable to getCarrierRatePlans", e);
			} finally {
				DBUtils.cleanup(conn, ps, rs);
			}
			
		}
		
		LOG.info("getCarrierRatePlans: Returned " + carrierRatePlans.size() + " carrier rate plans to the caller");
	
		return carrierRatePlans;
	}
	
	@Override
	public List<CarrierRatePlan> getCarrierRatePlans(int productId,long accountId) throws RatePlanException {

		List<Carrier> carriers = productCarrierDAO.getCarriersByProduct(productId);
		List<CarrierRatePlan> carrierRatePlans = new ArrayList<CarrierRatePlan>();
								
		if(carriers != null && !carriers.isEmpty())
		{
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				DBConnectionManager connectionManager = DBConnectionManager.getInstance();
				conn = connectionManager.getProvisionDatabaseConnection();

				// Build query
				ps = buildGetCarrierRatePlansByContractQuery(carriers, conn, accountId);
				
				// Execute Query
				rs = ps.executeQuery();
				
				while (rs.next()) {
					CarrierRatePlan carrierRatePlan = new CarrierRatePlan();
					
					carrierRatePlan.setRatePlanId(rs.getLong("rate_plan_id"));
					carrierRatePlan.setProductId(productId);
					carrierRatePlan.setCarrierId(rs.getInt("carrier_id"));
					carrierRatePlan.setRatePlanName(rs.getString("rate_plan_name"));
					carrierRatePlan.setStartDate(rs.getDate("start_date"));
					carrierRatePlan.setEndDate(rs.getDate("end_date"));
					carrierRatePlan.setInitialCharges(rs.getDouble("initial_charge"));
					carrierRatePlan.setMaxKb(rs.getDouble("max_size_in_kb"));
					carrierRatePlan.setMonthlyFee(rs.getDouble("monthly_fee"));				
					carrierRatePlan.setDescription(rs.getString("plan_description"));
					
					carrierRatePlans.add(carrierRatePlan);
				}	
			} catch (SQLException e) {
				LOG.error("SQLException during getCarrierRatePlans", e);
				throw new RatePlanException("Unable to getCarrierRatePlans", e);
			} finally {
				DBUtils.cleanup(conn, ps, rs);
			}
			
		}
		
		LOG.info("getCarrierRatePlans: Returned " + carrierRatePlans.size() + " carrier rate plans to the caller");
	
		return carrierRatePlans;
	
	}
	
	private long generateNewRatePlanId(Connection conn) throws SQLException, RatePlanException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		long ratePlanId = -1;
		
		try
		{
			ps = conn.prepareStatement(SQL_GET_NEXT_RATE_PLAN_ID);	
			
			LOG.info("Creating sql statement: " + ps +" to get next rate plan id");
	
			rs = ps.executeQuery();
			
			if(rs.next())
			{
				ratePlanId = rs.getLong(1);
			}
			
			LOG.info("Next Rate Plan Id fetched: "+ratePlanId);
		}
		finally
		{
			DBUtils.cleanup(null, ps, rs);
		}
		
		if(ratePlanId == -1)
		{
			throw new RatePlanException("Could not fetch the next rate plan id while creating the new rate plan");
		}
		
		return ratePlanId;
	}
	
	private long generateNewRatePlanTierId(Connection conn) throws SQLException, RatePlanException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		long ratePlanId = -1;
		
		try
		{
			ps = conn.prepareStatement(SQL_GET_NEXT_RATE_PLAN_TIER_ID);	
			
			LOG.info("Creating sql statement: " + ps +" to get next rate plan tier id");
	
			rs = ps.executeQuery();
			
			if(rs.next())
			{
				ratePlanId = rs.getLong(1);
			}
			
			LOG.info("Next Rate Plan Tier Id fetched: "+ratePlanId);
		}
		finally
		{
			DBUtils.cleanup(null, ps, rs);
		}
		
		if(ratePlanId == -1)
		{
			throw new RatePlanException("Could not fetch the next rate plan tier id while creating the rate plan tier");
		}
		
		return ratePlanId;
	}

	private PreparedStatement buildGetCarrierRatePlansQuery(List<Carrier> carriers, Connection conn) throws SQLException {
		PreparedStatement ps; 
		
		// Build in clause
		StringBuffer inClause = new StringBuffer("(");
		
		for (int i = 0; i < carriers.size(); i++) {
			inClause.append("?");

			if (i != carriers.size() - 1) {
				inClause.append(",");
			}
		}

		inClause.append(")");
		
		String getCarrierRatePlansQuery = SQL_GET_CARRIER_RATE_PLANS.replace(":carrier_ids", inClause.toString());
		
		StringBuilder query = new StringBuilder(getCarrierRatePlansQuery);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		int j = 0;
		
		// In clause for carrier ids
		while (j < carriers.size()) {
			int carrierId = carriers.get(j++).getCarrierId();
			ps.setInt(++i, carrierId);
		}

		return ps;
	}

	
	private PreparedStatement buildGetCarrierRatePlansByContractQuery(List<Carrier> carriers, Connection conn, long accountId) throws SQLException {
		PreparedStatement ps; 
		
		// Build in clause
		StringBuffer inClause = new StringBuffer("(");
		
		for (int i = 0; i < carriers.size(); i++) {
			inClause.append("?");

			if (i != carriers.size() - 1) {
				inClause.append(",");
			}
		}

		inClause.append(")");
		
		String getCarrierRatePlansQuery = SQL_GET_CARRIER_RATE_PLANS_BY_CONTRACT.replace(":carrier_ids", inClause.toString());
		
		StringBuilder query = new StringBuilder(getCarrierRatePlansQuery);
		
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());

		int i = 0;
		int j = 0;
		
		// In clause for carrier ids
		while (j < carriers.size()) {
			int carrierId = carriers.get(j++).getCarrierId();
			ps.setInt(++i, carrierId);
		}

		ps.setLong(++i, accountId);
		return ps;
	}
	
	@Override
	public AccountRatePlan getAccountRatePlan(int operatorId, long accountId, long ratePlanId) throws RatePlanException, RatePlanNotFoundException {
		AccountRatePlan accountRatePlan = null;

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAccountRatePlan: Fetched db connection");

			// Fetch the account rate plan for the specified rate plan id
			ps = buildGetAccountRatePlanQuery(conn, operatorId, accountId, ratePlanId);

			LOG.debug("getAccountRatePlan: executed query: " + ps);
			rs = ps.executeQuery();

			if (rs.next()) {
				accountRatePlan = new AccountRatePlan();
				accountRatePlan.setAccountRatePlanId(rs.getLong("account_rate_plan_id"));
				accountRatePlan.setAccountId(accountId);
				accountRatePlan.setStartDate(rs.getDate("arp_start_date"));
				accountRatePlan.setEndDate(rs.getDate("arp_end_date"));
				accountRatePlan.setCarrierRatePlanId(rs.getInt("carrier_plan_id"));				
				RatePlan ratePlan = new RatePlan();
				
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name"));
				ratePlan.setStartDate(rs.getDate("start_date"));				
				ratePlan.setEndDate(rs.getDate("end_date"));				
				ratePlan.setCurrencyName("USD");			
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setContractTerm(rs.getInt("contract_term"));

				RatePlanFeeDetails feeDetails = createRatePlanFeeDetails(rs);				
				ratePlan.setRatePlanFeeDetails(feeDetails);

				// Uncomment after new schema is introduced
				//IncludedUsagePolicy usagePolicy = createIncludedUsagePolicy(rs);		
				//ratePlan.setIncludedUsagePolicy(usagePolicy);
				
				//ProvisionTriggerSettings provisionTriggerSettings = createProvisionTriggerSettings(rs);				
				//ratePlan.setProvisionTriggerSettings(provisionTriggerSettings);
				
				//SuspendTriggerSettings suspendTriggerSettings = createSuspendTriggerSettings(rs);				
				//ratePlan.setSuspendTriggerSettings(suspendTriggerSettings);
				
				// Uncomment after zone is introduced
				//ratePlan.setZoneRatePlanSettings(zoneRatePlanSettings);	
				
				// Set Rate Plan
				accountRatePlan.setRatePlan(ratePlan);
			}
			
			if(accountRatePlan == null)
			{
				throw new RatePlanNotFoundException("Rate Plan: "+ratePlanId+" not found for account: "+accountId);
			}
			
		} catch (SQLException e) {
			LOG.error("SQLException during getAccountRatePlan", e);
			throw new RatePlanException("Unable to getAccountRatePlan", e);
		} catch (RatePlanNotFoundException e) {
			LOG.error("RatePlanNotFoundException in getAccountRatePlan" + e.getMessage());
			throw e;
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAccountRatePlan: Returned account rate plan " + ReflectionToStringBuilder.toString(accountRatePlan) + " to the caller");

		return accountRatePlan;
	}

	public List<AccountRatePlan> getAssigneddRatePlansAcrossAccounts(long ratePlanId) throws RatePlanException {
		List<AccountRatePlan> accountRatePlans = new ArrayList<AccountRatePlan>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("buildGetAllAssignedRatePlanQuery: Fetched db connection");

			// Fetch the assigned rate plans for the given ratePlanId
			ps = buildGetAllAssignedRatePlanQuery(conn, ratePlanId);

			LOG.debug("buildGetAllAssignedRatePlanQuery: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				AccountRatePlan accountRatePlan = new AccountRatePlan();
				accountRatePlan.setAccountRatePlanId(rs.getLong("account_rate_plan_id"));
				accountRatePlan.setAccountId(rs.getLong("account_id"));

				accountRatePlans.add(accountRatePlan);
			}
			
		} catch (SQLException e) {
			LOG.error("SQLException during getAssignedRatePlans", e);
			throw new RatePlanException("DB Error while getting getAssignedRatePlans", e);
		} catch (Exception e) {
			LOG.error("ServiceException in getAssignedRatePlans" + e.getMessage());
			throw new RatePlanException("Unable to getAssignedRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAssignedRatePlans: Returned " + accountRatePlans.size() + " rate plans to the caller");
		return accountRatePlans;
	}

	@Override
	public List<String> getUnpooledAccountRatePlans(int operatorId, long accountId, int filteredRatePlanPoolId, int productId) throws RatePlanException {
		List<String> accountRatePlanNames = new ArrayList<String>();

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getUnpooledRatePlans: Fetched db connection");			
			ps = createGetUnpooledRatePlansQuery(accountId, filteredRatePlanPoolId, productId, conn);

			LOG.debug("getUnpooledRatePlans: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				accountRatePlanNames.add(rs.getString("rate_plan_name"));
			}
			
		} catch (SQLException e) {
			LOG.error("SQLException during getUnpooledRatePlans", e);
			throw new RatePlanException("DB Error while getting unpooled rate plans", e);
		} catch (Exception e) {
			LOG.error("Exception during getUnpooledRatePlans", e);
			throw new RatePlanException("Unable to getUnpooledRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getUnpooledRatePlans: Returned " + accountRatePlanNames.size() + " rate plans to the caller");

		return accountRatePlanNames;
	}

	private PreparedStatement createGetUnpooledRatePlansQuery(long accountId, int filteredRatePlanPoolId, int productId, Connection conn) throws SQLException {
		PreparedStatement ps = null;
		if(productId != 0) {
			// Create Get Unpooled rate plans query by product
			if(filteredRatePlanPoolId != 0) {
				ps = conn.prepareStatement(SQL_GET_UNPOOLED_RATE_PLANS_BY_PRODUCT);
				ps.setInt(2, filteredRatePlanPoolId);			
				ps.setLong(3, accountId);		
				ps.setInt(4, productId);	
				LOG.info("GetUnpooledRatePlansQuery=" + SQL_GET_UNPOOLED_RATE_PLANS_BY_PRODUCT);
			}
			else {
				ps = conn.prepareStatement(SQL_GET_UNPOOLED_RATE_PLANS_BY_PRODUCT_ALL);	 
				ps.setLong(2, accountId);		
				ps.setInt(3, productId);	
				LOG.info("GetUnpooledRatePlansQuery=" + SQL_GET_UNPOOLED_RATE_PLANS_BY_PRODUCT_ALL);
			}
			// Set Parameters
			ps.setLong(1, accountId);		
		}
		else  
		{
			// Create Get Unpooled rate plans query
			if(filteredRatePlanPoolId != 0) {
				ps = conn.prepareStatement(SQL_GET_UNPOOLED_RATE_PLANS);	
				ps.setInt(2, filteredRatePlanPoolId);			
				ps.setLong(3, accountId);
				LOG.info("GetUnpooledRatePlansQuery=" + SQL_GET_UNPOOLED_RATE_PLANS);
			} else {
				ps = conn.prepareStatement(SQL_GET_UNPOOLED_RATE_PLANS_ALL);
				ps.setLong(2, accountId);
				LOG.info("GetUnpooledRatePlansQuery=" + SQL_GET_UNPOOLED_RATE_PLANS_ALL);
			}
			// Set Parameters
			ps.setLong(1, accountId);		
		}
		
		return ps;
	}
	
	private List<RatePlan> getAllRatePlans() {
		List<RatePlan> ratePlans = new ArrayList<RatePlan>();
				
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
 
		try { 
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAllRatePlans: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			ps = buildGetAllRatePlansQuery(conn);

			LOG.debug("getAllAccounts: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				RatePlan ratePlan = new RatePlan();
				
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name"));
				ratePlan.setStartDate(rs.getDate("start_date"));				
				ratePlan.setEndDate(rs.getDate("end_date"));
				ratePlan.setCurrencyName(rs.getString("currency_code"));		
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setAccessType(RatePlanAccessType.fromValue(rs.getInt("is_shared")));
				ratePlan.setStatus(RatePlanStatus.fromValue(rs.getInt("status_id")));
				ratePlan.setRatePlanType(RatePlanType.fromValue(rs.getInt("rate_plan_type")));
				ratePlan.setZoneSetId(rs.getInt("zone_set_id"));
				//AERPORT-5928 : account_id & carrier_rate_plan_id was being set as '0' in rate plan
				ratePlan.setAccountId(rs.getLong("account_id"));
				ratePlan.setCarrierRatePlanId(rs.getInt("carrier_rate_plan_id"));
				ratePlan.setWholesaleRateplanId(rs.getInt("wholesale_rate_plan_id"));
                                ratePlan.setIsWholesaleRatePlan(BooleanUtils.toBoolean(rs.getInt("is_wholesale")));
                
                if (appProperties.isRatePlanFilterByOperatorEnabled()) {
                    ratePlan.setOperatorId(rs.getInt("operator_id"));
                } 
                                
				ratePlans.add(ratePlan);
			}	
		} catch (SQLException e) {
			LOG.error("Exception during getAllRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAssignedRatePlans: Returned " + ratePlans.size() + " rate plans to the caller");

		return ratePlans;
	}
	
	private void createRatePlanOverageZones(long newRatePlanId, Zone[] overageZones, Date requestedDate, String requestedUser, Connection conn) throws RatePlanException {
		LOG.info("Creating CSPRatePlan to Zone Mapping");
		
		PreparedStatement ps = null;
		
		try
		{
			LOG.debug("Creating Prepared Statement to insert the data in csp_rate_plan_to_zone_map table");
	
			// Create SQL Statement
			ps = conn.prepareStatement(SQL_CREATE_CSP_RATE_PLAN_TO_ZONE_MAPPING);	
	
			LOG.debug("Setting Parameters");
			
			Zone homeZone = overageZones[0];
			Zone canadaZone = overageZones[1];
			Zone mexicoZone = overageZones[2];
			Zone internationalZone = overageZones[3];
			Zone specialCarrierZone = overageZones[4];
			Zone usRoamingZone = overageZones[5];
			
			if(homeZone != null)
			{
				setCspRatePlanToZoneMappingParameters(newRatePlanId, homeZone, "USA", requestedDate, requestedUser, ps);
			}		
			
			if(canadaZone != null)
			{
				setCspRatePlanToZoneMappingParameters(newRatePlanId, canadaZone, "CANADA", requestedDate, requestedUser, ps);
			}		
			
			if(mexicoZone != null)
			{
				setCspRatePlanToZoneMappingParameters(newRatePlanId, mexicoZone, "MEXICO", requestedDate, requestedUser, ps);
			}		
			
			if(internationalZone != null)
			{
				setCspRatePlanToZoneMappingParameters(newRatePlanId, internationalZone, "INTERNATIONAL", requestedDate, requestedUser, ps);
			}		
			
			if(specialCarrierZone != null)
			{
				setCspRatePlanToZoneMappingParameters(newRatePlanId, specialCarrierZone, "SPECIAL CARRIER", requestedDate, requestedUser, ps);
			}		
			
			if(usRoamingZone != null)
			{
				setCspRatePlanToZoneMappingParameters(newRatePlanId, usRoamingZone, "US ROAMING", requestedDate, requestedUser, ps);
			}					
	
			LOG.debug("Inserting csp rate plan to zone mapping info in the db");
	
			// Execute batch of csp rate plan to zone mappings
			ps.executeBatch();
	
			LOG.info("Created csp rate plan to zone mappings for rate plan id = " + newRatePlanId + " Successfully");
		}
		catch (SQLException e) {
			LOG.error("createCSPRatePlanZoneMapping failed due to db error: "+e.getMessage());
			throw new RatePlanException("Could not create CSPRatePlan to Zone Mapping due to db error");
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
	}

	private void setCspRatePlanToZoneMappingParameters(long ratePlanId, Zone zone, String cspZoneName, Date requestedDate, String requestedUser, PreparedStatement ps) throws SQLException {
		
		// Get CSP Zone
		CspZone cspZone = cspZoneCache.getCspZoneByName(cspZoneName);
		
		LOG.info("adding csp rate plan to zone mapping for zone id: "+zone.getZoneId()+" and rate plan id: "+ratePlanId+", csp zone: "+cspZoneName);
		
		// Set Parameters
		ps.clearParameters();
		
		ps.setLong(1, ratePlanId);
		ps.setInt(2, zone.getZoneId());
		ps.setInt(3, cspZone.getZoneId());
		ps.setTimestamp(4, new java.sql.Timestamp(requestedDate.getTime()));// created_date
		ps.setString(5, requestedUser);// created_by
		ps.setTimestamp(6, new java.sql.Timestamp(requestedDate.getTime()));// last_modified_date
		ps.setString(7, requestedUser);// last_modified_by
		ps.setTimestamp(8, new java.sql.Timestamp(requestedDate.getTime()));// rep_timestamp
		
		// Add to batch
		ps.addBatch();
	}

	private void deleteRatePlanOverageZones(long ratePlanId, Connection conn) throws RatePlanException {
		LOG.info("Deleting CSPRatePlan to Zone Mapping for ratePlanId: "+ratePlanId);

		PreparedStatement ps = null;
		
		try
		{
			LOG.debug("Creating Prepared Statement to delete the data from csp_rate_plan_to_zone_map table");

			// Create SQL Statement
			ps = conn.prepareStatement(SQL_DELETE_CSP_RATE_PLAN_TO_ZONE_MAPPING);	

			LOG.debug("Setting Parameters");
			
			ps.setLong(1, ratePlanId);
			
			LOG.debug("deleting csp rate plan to zone mapping info in the db");

			// Execute batch of csp rate plan to zone mappings
			ps.executeUpdate();

			LOG.info("Deleted csp rate plan to zone mappings for rate plan id = " + ratePlanId + " Successfully");
		}
		catch (SQLException e) {
			LOG.error("deleteCSPRatePlanZoneMapping failed due to db error: "+e.getMessage());
			throw new RatePlanException("Could not delete CSPRatePlan to Zone Mapping due to db error");
		}
		finally
		{
			DBUtils.cleanup(null, ps);
		}
	}
	
	private void updateRatePlanOverageZones(long ratePlanId, Zone[] overageZones, Date requestedDate, String requestedUser, Connection conn) throws RatePlanException {
		LOG.info("Mapping rate plan overages to CSP Zones...");
		
		// Delete old csp rate plan to zone mappings
		deleteRatePlanOverageZones(ratePlanId, conn);
		
		// Create new csp rate plan to zone mappings 
		createRatePlanOverageZones(ratePlanId, overageZones, requestedDate, requestedUser, conn);
		
		LOG.info("Mapped rate plan overages to CSP Zones successfully");
		
	}
        
        @Override
	public List<RatePlan> getWholesaleRatePlans(long accountId, int[] productIds)
		throws RatePlanException {
		List<RatePlan> ratePlans = new ArrayList<RatePlan>();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getAllRatePlans: Fetched db connection");

			// Fetch the Accounts for the specified account ids and status or
			// get all accounts
			// provides pagination support
			ps = buildGetWholesaleRatePlans(conn, accountId, productIds);

			LOG.debug("getAllAccounts: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				RatePlan ratePlan = new RatePlan();

				ratePlan.setZoneSetId(rs.getInt("zone_set_id"));
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name"));
				ratePlan.setRatePlanType(RatePlanType.fromValue(rs.getInt("rate_plan_type")));
				ratePlan.setPaymentType(RatePlanPaymentType.fromValue(rs.getInt("payment_type")));
				ratePlan.setAccessType(RatePlanAccessType.fromValue(rs.getInt("is_shared")));
				ratePlan.setPeriodType(rs.getInt("access_fee_duration_months") == 1 ? RatePlanPeriodType.MONTHLY : RatePlanPeriodType.VARIABLE);
				ratePlan.setStartDate(rs.getDate("start_date"));				
				ratePlan.setEndDate(rs.getDate("end_date"));
				ratePlan.setCurrencyName(rs.getString("currency_code"));		
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setStatus(RatePlanStatus.fromValue(rs.getInt("status_id")));
				ratePlan.setAccessFeeMonths(rs.getInt("access_fee_duration_months"));
				ratePlan.setExpireIncluded(rs.getInt("expire_included") == 1);
				ratePlan.setOverrideRoamingIncluded(rs.getInt("override_roaming_included") == 1);
				
				ratePlan.setTierCriteria(rs.getInt("tier_criteria"));
				ratePlan.setHomeZoneId(rs.getInt("home_zone_id"));
				ratePlan.setDescription(rs.getString("description"));
				ratePlan.setSpecialNotes(rs.getString("special_notes"));
				
				ratePlan.setCarrierRatePlanId(rs.getInt("carrier_rate_plan_id"));
				ratePlan.setAccountId(rs.getInt("account_id"));
				ratePlan.setIncludedPeriodMonths(rs.getInt("included_period_months"));
				ratePlan.setDevicePoolingPolicyId(rs.getInt("device_policy_id"));
				ratePlan.setPacketDataRoundingPolicyId(rs.getInt("packet_rounding_policy_id"));
				ratePlan.setCreatedBy(rs.getString("created_by"));
				ratePlan.setLastModifiedBy(rs.getString("last_modified_by"));
                                ratePlan.setRoamingIncluded(rs.getInt("INCL_ROAM_FLG") == 1);
                                ratePlan.setTechnology(rs.getString("technology"));
				ratePlan.setWaterfallEnabled(BooleanUtils.toBoolean(rs.getInt("WATERFALL_ENABLED")));
				// Set Tiers info for a tiered rate plan
				if(ratePlan.getRatePlanType() == RatePlanType.TIERED)
				{
					setRatePlanTiers(ratePlan, rs);
				}
				
				setRatePlanFeeDetails(ratePlan, rs);
				
				// Set Provision and Suspend Triggers
				setRatePlanTriggers(ratePlan, rs);
				
				// Set Zone Overrides
				setZoneRatePlanSettings(ratePlan, rs);
				
				//add the cancellation fee details as well
				ratePlan.setAutoCancelAtTermEnd(rs.getInt("AUTO_CANCEL_AT_TERM_END"));
				ratePlan.setProRateCancellationFee(rs.getInt("PRO_RATE_CANCELLATION_FEE"));
				ratePlan.setCancellationFeeReductionInterval(rs.getInt("CANC_FEE_REDUCTION_INT"));
				ratePlan.setContractTerm(rs.getInt("CONTRACT_TERM"));
				ratePlan.setAccountForDaysInProvisionState(rs.getInt("ACC_DAYS_PROV_STAT"));
				ratePlan.setAccountForDaysInSuspendState(rs.getInt("ACC_DAYS_SUSPEND_STATE"));
                                ratePlan.setIsWholesaleRatePlan(BooleanUtils.toBoolean(rs.getInt("IS_WHOLESALE")));
                                ratePlan.setWholesaleRateplanId(rs.getInt("WHOLESALE_RATE_PLAN_ID"));
				//set rate plan overage
				setRatePlanOverageBucket(ratePlan, conn);
				ratePlans.add(ratePlan);
			}
						
		} catch (SQLException e) {
			LOG.error("Exception during getAllRatePlans", e);
			throw new RatePlanException("Unable to getAllRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getAssignedRatePlans: Returned " + ratePlans.size() + " rate plans to the caller");

		return ratePlans;
	}
        
        private PreparedStatement buildGetWholesaleRatePlans(Connection conn, long accountId, int[] productIds)
			throws SQLException {
		PreparedStatement ps;
		
                StringBuffer inClause = new StringBuffer("(");
			
		for (int i = 0; i < productIds.length; i++) {
                    inClause.append("?");
			if (i != productIds.length - 1) {
                            inClause.append(",");
			}
		}
		inClause.append(")");	
		String getRatePlansByProductsQuery = SQL_GET_WHOLESALE_RATE_PLANS.replace(":product_id_list", inClause.toString());			
		StringBuilder query = new StringBuilder(getRatePlansByProductsQuery);
                
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());
		int i = 0;
		ps.setLong(++i, accountId);
                int j = 0;
                while (j < productIds.length) {
                    int productId = productIds[j++];
                    ps.setInt(++i, productId);
		}                        
		return ps;
	}
        
        @Override
	public List<RatePlan> getResellerRetailRatePlans(long accountId, int[] productIds)
		throws RatePlanException {
		List<RatePlan> ratePlans = new ArrayList<RatePlan>();
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getResellerRetailRatePlans: Fetched db connection");

			// Fetch the retail rate plans for the specified account id 
			ps = buildGetResellerRetailRatePlans(conn, accountId, productIds);

			LOG.debug("getResellerRetailRatePlans: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				RatePlan ratePlan = new RatePlan();
				
				ratePlan.setRatePlanId(rs.getInt("rate_plan_id"));
				ratePlan.setRatePlanLabel(rs.getString("rate_plan_label"));
				ratePlan.setRatePlanName(rs.getString("rate_plan_name"));
				ratePlan.setStartDate(rs.getDate("start_date"));				
				ratePlan.setEndDate(rs.getDate("end_date"));
				ratePlan.setCurrencyName(rs.getString("currency_code"));		
				ratePlan.setProductId(rs.getInt("product_id"));
				ratePlan.setAccessType(RatePlanAccessType.fromValue(rs.getInt("is_shared")));
				ratePlan.setStatus(RatePlanStatus.fromValue(rs.getInt("status_id")));
				ratePlan.setRatePlanType(RatePlanType.fromValue(rs.getInt("rate_plan_type")));
				ratePlan.setZoneSetId(rs.getInt("zone_set_id"));
				ratePlan.setAccountId(rs.getLong("account_id"));
				ratePlan.setCarrierRatePlanId(rs.getInt("carrier_rate_plan_id"));
				ratePlan.setWholesaleRateplanId(rs.getInt("wholesale_rate_plan_id"));
                                ratePlan.setIsWholesaleRatePlan(BooleanUtils.toBoolean(rs.getInt("is_wholesale")));
                                ratePlan.setTechnology(rs.getString("technology"));
				ratePlans.add(ratePlan);
			}
						
		} catch (SQLException e) {
			LOG.error("Exception during getResellerRetailRatePlans", e);
			throw new RatePlanException("Unable to getResellerRetailRatePlans", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getResellerRetailRatePlans: Returned " + ratePlans.size() + " rate plans to the caller");

		return ratePlans;
	}
        
        private PreparedStatement buildGetResellerRetailRatePlans(Connection conn, long accountId, int[] productIds)
			throws SQLException {
		PreparedStatement ps;
		StringBuffer inClause = new StringBuffer("(");
			
		for (int i = 0; i < productIds.length; i++) {
                    inClause.append("?");
			if (i != productIds.length - 1) {
                            inClause.append(",");
			}
		}
		inClause.append(")");
                String getRetailRatePlansByProductsQuery = SQL_GET_RESELLER_RETAIL_RATE_PLANS.replace(":product_id_list", inClause.toString());			
		StringBuilder query = new StringBuilder(getRetailRatePlansByProductsQuery);
                
		// Create Prepared Statement
		ps = conn.prepareStatement(query.toString());
		int i = 0;		
                int j = 0;
                while (j < productIds.length) {
                    int productId = productIds[j++];
                    ps.setInt(++i, productId);
		}     
                ps.setLong(++i, accountId);
		return ps;
	}
    
    @Override    
    public List<RatePlan> getSubRatePlansInUse(List<Long> parenRatePlanIds) throws RatePlanException{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RatePlan> subRatePlans = new ArrayList<RatePlan>();
		try {
			DBConnectionManager connectionManager = DBConnectionManager.getInstance();
			conn = connectionManager.getProvisionDatabaseConnection();

			LOG.debug("getSubRatePlansInUse: Fetched db connection");

			StringBuilder INClause = new StringBuilder();
			for(Long rpId : parenRatePlanIds){
				INClause.append("?,");
			}
			
			
			ps = conn.prepareStatement(SQL_GET_SUB_RATE_PLANS_IN_USE +"(" + INClause.deleteCharAt( INClause.length() -1 ).toString() + ")");
			int index = 1;
			for(Long rpId : parenRatePlanIds){
				ps.setLong(index++, rpId);
			}

			LOG.debug("getSubRatePlansInUse: executed query: " + ps);
			rs = ps.executeQuery();

			while (rs.next()) {
				RatePlan rp = new RatePlan();
				rp.setRatePlanId(rs.getLong("tier_rate_plan_id"));
				rp.setRatePlanName(rs.getString("rate_plan_name"));
				subRatePlans.add(rp);
			}
						
		} catch (SQLException e) {
			LOG.error("Exception during getSubRatePlansInUse", e);
			throw new RatePlanException("Unable to getSubRatePlansInUse", e);
		} finally {
			DBUtils.cleanup(conn, ps, rs);
		}

		LOG.info("getSubRatePlansInUse: Returned " + subRatePlans.size() + " sub rate plans to the caller");

		return subRatePlans;
	
    }
}
