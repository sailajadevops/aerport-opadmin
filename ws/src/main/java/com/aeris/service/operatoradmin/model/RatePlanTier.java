package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
public class RatePlanTier implements Serializable {
	private static final long serialVersionUID = 6330386572224820088L;

	@NotNull(message = "{startCount.notnull}")
	private int startCount;

	@NotNull(message = "{endCount.notnull}")
	private int endCount;

	@NotNull(message = "{tieredRatePlanId.notnull}")
	@Min(value = 1, message = "{tieredRatePlanId.number}")
	private long tieredRatePlanId;

	public int getStartCount() {
		return startCount;
	}

	public void setStartCount(int startCount) {
		this.startCount = startCount;
	}

	public int getEndCount() {
		return endCount;
	}

	public void setEndCount(int endCount) {
		this.endCount = endCount;
	}

	public long getTieredRatePlanId() {
		return tieredRatePlanId;
	}

	public void setTieredRatePlanId(long tieredRatePlanId) {
		this.tieredRatePlanId = tieredRatePlanId;
	}	
}
