package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum PacketUnit implements Serializable {
	KB(1), MB(2), GB(3);
	int value;

	PacketUnit(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int value) {
		PacketUnit[] units = values();

		for (PacketUnit unit : units) {
			if (unit.getValue() == value) {
				return true;
			}
		}

		return false;
	}

	public static PacketUnit fromValue(int value) {
		PacketUnit[] units = values();

		for (PacketUnit unit : units) {
			if (unit.getValue() == value) {
				return unit;
			}
		}

		return null;
	}

	public static PacketUnit fromName(String value) {
		PacketUnit[] units = values();

		for (PacketUnit unit : units) {
			if (unit.name().equalsIgnoreCase(value)) {
				return unit;
			}
		}

		return null;
	}
}
