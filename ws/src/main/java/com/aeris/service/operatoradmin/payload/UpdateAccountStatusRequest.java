package com.aeris.service.operatoradmin.payload;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;

import com.aeris.service.operatoradmin.model.AccountStatus;
import com.aeris.service.operatoradmin.model.ApprovalStatus;
import com.aeris.service.operatoradmin.model.constraints.Enumeration;

@XmlRootElement
public class UpdateAccountStatusRequest implements Serializable {
	private static final long serialVersionUID = -4659589854090199702L;

	@Email(message = "{userId.email}")
	private String userId;

	@Enumeration(value = AccountStatus.class, message = "{status.enum}")
	private AccountStatus status;
	
	@Enumeration(value = ApprovalStatus.class, message = "{status.enum}")
	private ApprovalStatus approvalStatus;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public ApprovalStatus getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(ApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
}
