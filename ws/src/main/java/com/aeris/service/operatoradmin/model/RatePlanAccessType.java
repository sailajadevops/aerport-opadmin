package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public enum RatePlanAccessType implements Serializable {
	SHARED(1), EXCLUSIVE(0);
	int value;

	RatePlanAccessType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static boolean isValid(int value) {
		RatePlanAccessType[] statuses = values();

		for (RatePlanAccessType ratePlanAccessType : statuses) {
			if (ratePlanAccessType.getValue() == value) {
				return true;
			}
		}

		return false;
	}

	public static RatePlanAccessType fromValue(int value) {
		RatePlanAccessType[] statuses = values();

		for (RatePlanAccessType ratePlanAccessType : statuses) {
			if (ratePlanAccessType.getValue() == value) {
				return ratePlanAccessType;
			}
		}

		return null;
	}

	public static RatePlanAccessType fromName(String value) {
		RatePlanAccessType[] statuses = values();

		for (RatePlanAccessType ratePlanAccessType : statuses) {
			if (ratePlanAccessType.name().equalsIgnoreCase(value)) {
				return ratePlanAccessType;
			}
		}

		return null;
	}
}
