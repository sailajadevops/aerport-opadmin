package com.aeris.service.operatoradmin.payload;

import java.util.List;

import javax.validation.constraints.Pattern;

/**
 * The DTO class for distributorCode
 * 
 * @author saurabh.sharma@aeris.net
 *
 */
public class UpdateDistributorCode {

	private long distributorId ;
	private long code ;
	private List<String> emailRecipients ;
	private long accountId ;
	@Pattern(regexp = "[0-9]+", message = "{iccidStart.number}")
	private String iccidStart;
	@Pattern(regexp = "[0-9]+", message = "{iccidEnd.number}")
	private String iccidEnd ;
	private String assignedIccid ;
	
	
	public long getDistributorId() {
		return distributorId;
	}
	
	public void setDistributorId(long distributorId) {
		this.distributorId = distributorId;
	}
	
	
	public List<String> getEmailRecipients() {
		return emailRecipients;
	}
	
	public void setEmailRecipients(List<String> emailRecipients) {
		this.emailRecipients = emailRecipients;
	}

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public String getIccidStart() {
		return iccidStart;
	}

	public void setIccidStart(String iccidStart) {
		this.iccidStart = iccidStart;
	}

	public String getIccidEnd() {
		return iccidEnd;
	}

	public void setIccidEnd(String iccidEnd) {
		this.iccidEnd = iccidEnd;
	}

	public String getAssignedIccid() {
		return assignedIccid;
	}

	public void setAssignedIccid(String assignedIccid) {
		this.assignedIccid = assignedIccid;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}


}
