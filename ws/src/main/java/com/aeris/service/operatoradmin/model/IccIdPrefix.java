package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public class IccIdPrefix implements Serializable {
	private static final long serialVersionUID = -2108681718789026951L;
	private int range;
	private long tail;

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public long getTail() {
		return tail;
	}

	public void setTail(long tail) {
		this.tail = tail;
	}
}
