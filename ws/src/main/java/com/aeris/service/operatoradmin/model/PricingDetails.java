package com.aeris.service.operatoradmin.model;

import java.io.Serializable;

public class PricingDetails implements Serializable {
	private static final long serialVersionUID = -5386659106925832038L;
	private int noOfdays;
	private long packetKB;
	private int perKBPacketPrice;
	private int mtSmsCount;
	private int perMTSmsPrice;
	private int moSmsCount;
	private int perMOSmsPrice;
	private int mtVoiceMins;
	private int perMinMTVoicePrice;
	private int moVoiceMins;
	private int perMinMOVoicePrice;

	public int getNoOfdays() {
		return noOfdays;
	}

	public void setNoOfdays(int noOfdays) {
		this.noOfdays = noOfdays;
	}

	public long getPacketKB() {
		return packetKB;
	}

	public void setPacketKB(long packetKB) {
		this.packetKB = packetKB;
	}

	public int getPerKBPacketPrice() {
		return perKBPacketPrice;
	}

	public void setPerKBPacketPrice(int perKBPacketPrice) {
		this.perKBPacketPrice = perKBPacketPrice;
	}

	public int getMtSmsCount() {
		return mtSmsCount;
	}

	public void setMtSmsCount(int mtSmsCount) {
		this.mtSmsCount = mtSmsCount;
	}

	public int getPerMTSmsPrice() {
		return perMTSmsPrice;
	}

	public void setPerMTSmsPrice(int perMTSmsPrice) {
		this.perMTSmsPrice = perMTSmsPrice;
	}

	public int getMoSmsCount() {
		return moSmsCount;
	}

	public void setMoSmsCount(int moSmsCount) {
		this.moSmsCount = moSmsCount;
	}

	public int getPerMOSmsPrice() {
		return perMOSmsPrice;
	}

	public void setPerMOSmsPrice(int perMOSmsPrice) {
		this.perMOSmsPrice = perMOSmsPrice;
	}

	public int getMtVoiceMins() {
		return mtVoiceMins;
	}

	public void setMtVoiceMins(int mtVoiceMins) {
		this.mtVoiceMins = mtVoiceMins;
	}

	public int getPerMinMTVoicePrice() {
		return perMinMTVoicePrice;
	}

	public void setPerMinMTVoicePrice(int perMinMTVoicePrice) {
		this.perMinMTVoicePrice = perMinMTVoicePrice;
	}

	public int getMoVoiceMins() {
		return moVoiceMins;
	}

	public void setMoVoiceMins(int moVoiceMins) {
		this.moVoiceMins = moVoiceMins;
	}

	public int getPerMinMOVoicePrice() {
		return perMinMOVoicePrice;
	}

	public void setPerMinMOVoicePrice(int perMinMOVoicePrice) {
		this.perMinMOVoicePrice = perMinMOVoicePrice;
	}
}
