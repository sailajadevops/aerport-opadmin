package com.aeris.service.operatoradmin.enums.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aeris.service.common.cache.Cache;
import com.aeris.service.common.cache.impl.hazelcast.Hazelcast;
import com.aeris.service.operatoradmin.dao.storedproc.IStoredProcedure;
import com.aeris.service.operatoradmin.enums.ISIMFormatEnum;
import com.aeris.service.operatoradmin.events.Event;
import com.aeris.service.operatoradmin.events.IEventListener;
import com.aeris.service.operatoradmin.exception.GenericServiceException;
import com.aeris.service.operatoradmin.exception.StoredProcedureException;
import com.aeris.service.operatoradmin.model.SIMFormat;
import com.google.inject.Singleton;

/**
 * Resource Definition Cache used for lookup purposes 
 * 
 * @author SP00125222
 */
@Singleton
public class SIMFormatEnum implements ISIMFormatEnum, IEventListener {
	private Logger LOG = LoggerFactory.getLogger(SIMFormatEnum.class);
	private Cache<String, SIMFormat> cache;
	private Cache<String, SIMFormat> cacheById;
	
	@Inject
	private IStoredProcedure<List<SIMFormat>> getSIMFormatsStoredProcedure;
	
	/* (non-Javadoc)
	 * @see com.aeris.service.aeradmin.guice.AfterInject#init()
	 */
	public void init(@Hazelcast(cache = "SIMFormatCache/name") Cache<String, SIMFormat> cache, 
			@Hazelcast(cache = "SIMFormatCache/id") Cache<String, SIMFormat> cacheById) {
		this.cache = cache;
		this.cacheById = cacheById;	
		loadResources();
	}
	
	/**
	 * Loads all resource definitions into the memory for future lookup.
	 */
	void loadResources() {
		LOG.info("loading resources from db");
		
		try {
			// Call Get_Product Stored Procedure
			List<SIMFormat> simFormats = getSIMFormatsStoredProcedure.execute();	
			
			for(SIMFormat format: simFormats) 
			{
				cache.put(format.getName(), format);
				cacheById.put(String.valueOf(format.getId()), format);
			}
			
			LOG.info("resources loaded successfully");
		} catch (StoredProcedureException e) {
			LOG.error("Exception during getSIMFormatsStoredProcedure call", e);
			throw new GenericServiceException("Unable to loadResources", e);
		}
	}
	
	@Override
	public SIMFormat getSIMFormatByName(String name) {
		return cache.get(name);
	}

	@Override
	public SIMFormat getSIMFormatById(int id) {
		return cacheById.get(String.valueOf(id));
	}
	
	@Override
	public List<SIMFormat> getAllSIMFormats() {
		return new ArrayList<SIMFormat>(cacheById.getValues());
	}
	
	@Override
	public boolean isValid(String id) {
		return cacheById.get(id) != null; 
	}
	
	@Override
	public void onEvent(Event eventName, Object... params) {
		if(Event.REFRESH_CACHE == eventName)
		{
			loadResources();
		}
	}
}
