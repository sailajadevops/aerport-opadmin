
FILE: %FILE_NAME%                                          

--------------------------------------------------------------------------------------------------------------------------
BASIC INFORMATION
--------------------------------------------------------------------------------------------------------------------------

RATE PLAN NAME:    %RATE_PLAN_NAME%                     START DATE: 	        %START_DATE%                                          
                   %RATE_PLAN_NAME_2%                     
RATE PLAN LABEL:   %RATE_PLAN_LABEL%                    END DATE:               %END_DATE%                                          
                   %RATE_PLAN_LABEL_2%                     
PRODUCT:           %PRODUCT%                            CARRIER RATE PLAN:      %CARRIER_RATE_PLAN%                                          

HOME ZONE:         %HOME_ZONE%                          CURRENCY:               %CURRENCY_CODE%

ZONE SET:          %ZSET_NAME%                                          
 
DEVICE POOLING 	   %DEVICE_POOLING_POLICY%              PACKET DATA ROUNDING    %PACKET_ROUNDING_POLICY%                                          
POLICY:                                                 POLICY:

STATUS:            %STATUS%

--------------------------------------------------------------------------------------------------------------------------
CATEGORY
--------------------------------------------------------------------------------------------------------------------------

RATE PLAN PERIOD:  %RATE_PLAN_PERIOD%                   RATE PLAN TYPE:         %RATE_PLAN_ACCESS_TYPE%                                          
 
PAYMENT METHOD:    %RATE_PLAN_PAYMENT_TYPE%             RATE PLAN MODEL:        %RATE_PLAN_MODEL_TYPE%                                          


-----------------------------------------------------   ------------------------------------------------------------------
SUBSCRIPTION FEE                                        INCLUDED POLICY
-----------------------------------------------------   ------------------------------------------------------------------

ACCESS FEE:        %ACCESS_FEE%                         ROLLOVER INCLUDED       %ROLL_OVER_INCLUDED_USAGE%                                          
                                                        USAGE:	   
DURATION (MONTHS): %ACCESS_FEE_DUR_MONTHS%              
                                                        INCLUDED PERIOD         %INCLUDED_PERIOD_MONTHS%                                          
ACTIVATION FEE:    %ACTIVATION_FEE%                     (MONTHS):

SUSPEND FEE:       %SUSPEND_FEE%                                          

UNSUSPEND FEE:     %UNSUSPEND_FEE%                                          

DEACTIVATION FEE:  %DEACTIVATION_FEE%                                          

REACTIVATION FEE:  %REACTIVATION_FEE%                                          


-----------------------------------------------------   ------------------------------------------------------------------
PROVISION STATE TRIGGERS                                SUSPEND STATE TRIGGERS
-----------------------------------------------------   ------------------------------------------------------------------

DURATION:          %PROV_DUR_WITH_UNIT%                 DURATION:          %SUSP_DUR_WITH_UNIT%                                          

PACKET THRESHOLD:  %PROV_PKT_WITH_UNIT%                 PACKET THRESHOLD:  %SUSP_PKT_WITH_UNIT%                                          

MT SMS             %PROV_MT_SMS_THRESHOLD%              MT SMS             %SUSP_MT_SMS_THRESHOLD%                                          
THRESHOLD (COUNT):                                      THRESHOLD (COUNT): 	

MO SMS             %PROV_MO_SMS_THRESHOLD%              MO SMS             %SUSP_MO_SMS_THRESHOLD%                                          
THRESHOLD (COUNT):                                      THRESHOLD (COUNT): 

MT VOICE           %PROV_MT_VOICE_THRESHOLD%            MT VOICE           %SUSP_MT_VOICE_THRESHOLD%                                          
THRESHOLD  (MINS):                                      THRESHOLD  (MINS): 

MO VOICE           %PROV_MO_VOICE_THRESHOLD%            MO VOICE           %SUSP_MO_VOICE_THRESHOLD%                                          
THRESHOLD  (MINS):                                      THRESHOLD  (MINS): 

TRAFFIC DAYS:	   %PROV_TRAFFIC_DAYS%                  TRAFFIC DAYS:	   %SUSP_TRAFFIC_DAYS%                                          


-----------------------------------------------------   ------------------------------------------------------------------
PROVISION STATE FEES                                    SUSPEND STATE FEES
-----------------------------------------------------   ------------------------------------------------------------------

PACKET PRICE	   %PROV_PACKET_COST%                   PACKET PRICE	   %SUSP_PACKET_COST%                                          	
(PER KB):                                               (PER KB):	

MT SMS PRICE	   %PROV_MT_SMS_COST%                   MT SMS PRICE	   %SUSP_MT_SMS_COST%                                          
(PER COUNT):                                            (PER COUNT): 

MO SMS PRICE	   %PROV_MO_SMS_COST%                   MO SMS PRICE	   %SUSP_MO_SMS_COST%                                          
(PER COUNT):                                            (PER COUNT): 

MT VOICE PRICE	   %PROV_MT_VOICE_COST%                 MT VOICE PRICE	   %SUSP_MT_VOICE_COST%                                          
(PER MIN):                                              (PER MIN): 	

MO VOICE PRICE	   %PROV_MO_VOICE_COST%                 MO VOICE PRICE	   %SUSP_MO_VOICE_COST%                                          	
(PER MIN):                                              (PER MIN): 

--------------------------------------------------------------------------------------------------------------------------
OVERAGE BUCKET
--------------------------------------------------------------------------------------------------------------------------
#FOR [I < %BKT_LEN%]

START DATE FOR BUCKET									 %BKT_STRT_DT#I% 
ASSOCIATION                        

END DATE FOR BUCKET   						 			 %BKT_END_DT#I%
ASSOCIATION

BUCKET SIZE        	   						 	         %BKT_SIZE#I%

BUCKET PRICE ($)        	   						 	 %BKT_PRICE#I%

NO ACCESS FEE DEVICE COUNT								 %BKT_ACCFEE_DVCE_CNT#I%

AUTO ADD BUCKET	        	   						 	 %BKT_AUTO_ADD#I%

#END
#IF [%BKT_LEN% = 0]
                                                      -  NO OVERAGE BUCKETS DEFINED -
#END

--------------------------------------------------------------------------------------------------------------------------
CANCELLATION FEE
--------------------------------------------------------------------------------------------------------------------------

#IF [%CANFEE_CNTRT_TRM% > 0]
PRO-RATE CANCELLATION FEE								 %CANFEE_PRO_RT_CANFEE%

CONTRACT TERM (DAYS)  						 			 %CANFEE_CNTRT_TRM%

AUTO-CANCEL AT END OF TERM								 %CANFEE_AUTO_CNCL%

ACCOUNT FOR DAYS IN 						 			 %CANFEE_ACC_PROV_STAT%
PROVISION STATE

ACCOUNT FOR DAYS IN 						 			 %CANFEE_ACC_PROV_STAT%
SUSPEND STATE
 
REDUCTION INTERVAL (DAYS)								 %CANFEE_RDN_INTL%

#END
#IF [%CANFEE_CNTRT_TRM% = 0]
                                                      -  NO CANCELLATION SETTINGS DEFINED -
#END

--------------------------------------------------------------------------------------------------------------------------
ZONE RATING POLICY CUSTOMIZATION
--------------------------------------------------------------------------------------------------------------------------

------------------------------	--------------------------  ------------------------------------------------------  ------------------------------------------------------
CUSTOMIZED ZONE                 PACKET(KB)    $/KB          MT SMS        $/            MO SMS        $/            MT VOICE      $/MIN         MO VOICE      $/MIN
NAME                            INCLUDED      PACKET        INCLUDED      MT SMS        INCLUDED      MO SMS        INCLUDED      MT VOICE      INCLUDED      MO VOICE							
------------------------------  --------------------------  ------------------------------------------------------  ------------------------------------------------------
#FOR [I < %ZONE_LEN%]

%ZONE#I%                        %INCL_PKT#I%  %PKT_COST#I%  %INCL_MTS#I%  %MTS_COST#I%  %INCL_MOS#I%  %MOS_COST#I%  %INCL_MTV#I%  %MTV_COST#I%  %INCL_MOV#I%  %MOV_COST#I%  
#END
#IF [%ZONE_LEN% = 0]
                                                                      -  NO CUSTOMIZED ZONES -
#END


--------------------------------------------------------------------------------------------------------------------------
DEFAULT ZONE RATING POLICY
--------------------------------------------------------------------------------------------------------------------------

------------------------------	--------------------------  ------------------------------------------------------  ------------------------------------------------------
ZONE NAME                       PACKET(KB)    $/KB          MT SMS        $/            MO SMS        $/            MT VOICE      $/MIN         MO VOICE      $/MIN
                                INCLUDED      PACKET        INCLUDED      MT SMS        INCLUDED      MO SMS        INCLUDED      MT VOICE      INCLUDED      MO VOICE							
------------------------------  --------------------------  ------------------------------------------------------  ------------------------------------------------------
#FOR [I < %ZONE_LEN%]

%DZONE#I%                       %DINCL_PKT#I% %DPKT_COST#I% %DINCL_MTS#I% %DMTS_COST#I% %DINCL_MOS#I% %DMOS_COST#I% %DINCL_MTV#I% %DMTV_COST#I% %DINCL_MOV#I% %DMOV_COST#I%  
#END
#IF [%ZONE_LEN% = 0]
                                                                      -  NO ZONES AVAILABLE -
#END
 