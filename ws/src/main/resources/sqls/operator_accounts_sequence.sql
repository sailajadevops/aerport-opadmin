declare
    new_account_seq INTEGER;
begin
   select max(account_id) + 1
   into   new_account_seq
   from   aersys_accounts where account_id  between 40000 and 49998;

    execute immediate 'CREATE SEQUENCE sprint_acc_seq 
          MAXVALUE 49999
          START WITH ' || new_account_seq ||
          'INCREMENT BY 1
          CACHE 20
          NOCYCLE';
end;

declare
    new_account_seq INTEGER;
begin
   select max(account_id) + 1
   into   new_account_seq
   from   aersys_accounts where account_id  between 1 and 39998;

    execute immediate 'CREATE SEQUENCE aeris_acc_seq 
          MAXVALUE 39999
          START WITH ' || new_account_seq ||
          'INCREMENT BY 1
          CACHE 20
          NOCYCLE';
end;

declare
    new_account_seq INTEGER;
begin
   select max(account_id) + 1
   into   new_account_seq
   from   aersys_accounts where account_id  between 50000 and 59998;

    execute immediate 'CREATE SEQUENCE cisco_acc_seq 
          MAXVALUE 59999
          START WITH ' || new_account_seq ||
          'INCREMENT BY 1
          CACHE 20
          NOCYCLE';
end;