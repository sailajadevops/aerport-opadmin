declare
    max_tpool_id INTEGER;
    max_account_pool_id INTEGER;
begin
   select nvl(max(pool_id), 0) + 5
   into   max_tpool_id
   from   aerbill_prov.t_pool;
   
   select greatest(nvl(max(pool_id), 0) + 5, max_tpool_id)
   into max_account_pool_id
   from aerisgen.account_pool;

  execute immediate 'drop sequence aerisgen.account_pool_seq';
  execute immediate 'create sequence aerisgen.account_pool_seq start with ' || max_account_pool_id || ' increment by 1 cache 20 nocycle';
  
end;


declare
    max_taccountratepoolmap_id INTEGER;
begin
   select nvl(max(ACCOUNT_RATE_POOL_MAP_ID), 0) + 1
   into   max_taccountratepoolmap_id
   from   aerbill_prov.t_account_rate_pool_map;
   
  execute immediate 'create sequence aerisgen.account_rate_pool_map_seq start with ' || max_taccountratepoolmap_id || ' increment by 1 cache 20 nocycle';
  
end;
