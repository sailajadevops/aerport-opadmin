create or replace
PACKAGE BODY AERBILL_PROV.ACCOUNT_MANAGEMENT_2_0_V2 AS

  PROCEDURE approve_account
						(L_acct_id		IN	NUMBER,
						 status			OUT	NUMBER,
						 ExtErrMsg		OUT	VARCHAR2
						) IS
	BEGIN
  status 		:= 0;
	ExtErrMsg	:= 'OK';

	 UPDATE
		aersys_accounts
		SET NOT_USED=0
		where
		account_id=L_acct_id;

   UPDATE ws_accts_additional_info SET approval_status = 1
   where
		account_id=L_acct_id;

  COMMIT;

  EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			status := SQLCODE;
			ExtErrMsg := 'approve_account: ' || SUBSTR(SQLERRM, 1, 100);
			INSERT INTO SQL_ERRORS (code, description, TIME)
								VALUES (status, ExtErrMsg, Sysgmtdate);
			COMMIT;
			RETURN;

	END approve_account;

  	 	PROCEDURE update_account
						(L_acct_id		IN	NUMBER,
						L_account_status IN 	Number,
            L_region_code IN NUMBER,
            L_web_address IN VARCHAR2,
						L_invoice_address		IN		VARCHAR2,
						L_special_instruction	IN		VARCHAR2,
						L_tags		IN		VARCHAR2,
						L_contact_type		IN	array_num_type,
            L_contact_name IN		array_char_type,
						L_role		IN		array_char_type,
						L_job_title		IN      array_char_type,
						L_email		IN      array_char_type,
						L_phone_no		IN      array_char_type,
            L_Included_Alert_Profiles IN NUMBER,
            L_features_enabled IN array_num_type,
            L_billable_status IN NUMBER,
            L_account_name IN VARCHAR2,            
            L_last_modified_by IN VARCHAR2,            
            status			OUT	NUMBER,
						ExtErrMsg		OUT VARCHAR2
						) IS
      	L_row_count	number := null;
        L_aercloud_enabled boolean := false;
        L_connectivity_enabled boolean := false;
  BEGIN
  status 		:= 0;
	ExtErrMsg	:= 'OK';


  UPDATE AERSYS_ACCOUNTS SET region_code=L_region_code, www_addr = L_web_address, name = L_account_name
  WHERE account_id=L_acct_id;

  UPDATE ws_Accts_Additional_info SET account_status = l_account_status,
  invoice_address = L_invoice_address, special_instruction = L_special_instruction, tags = L_tags, included_alert_profiles=L_Included_Alert_Profiles,
   billable_status=L_billable_status, last_modified_by=L_last_modified_by, last_modified_date = sysdate
  where account_id=L_acct_id;

  If(L_contact_type.count>=1) THEN

    FOR i IN 1..L_contact_type.count
    LOOP
      BEGIN
        select contact_type into L_row_count FROM ws_account_contacts where account_id = L_acct_id and contact_type=L_contact_type(i);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
            IF(L_contact_name(i) IS NOT NULL) THEN
              insert into ws_account_contacts(Account_Id, contact_type, contact_name, contact_role, job_title, email,
              phone, last_modified_by, created_by, last_modified_date, date_created)
              values ( L_acct_id, L_contact_type(i), L_contact_name(i), L_role(i),
              L_job_title(i), L_email(i), L_phone_no(i), L_last_modified_by, L_last_modified_by, sysdate, sysdate);
            END IF;
         COMMIT;
         END;
         IF(L_contact_name(i) IS NOT NULL) THEN
           UPDATE ws_account_contacts SET contact_type = L_contact_type(i), contact_name= L_contact_name(i), contact_role = L_role(i),
           job_title = L_job_title(i), email = L_email(i), phone = L_phone_no(i), last_modified_by=L_last_modified_by, last_modified_date = sysdate
           WHERE account_id = L_acct_id and contact_type=L_contact_type(i);
         ELSE
           delete ws_account_contacts WHERE account_id = L_acct_id and contact_type=L_contact_type(i);
         END IF;
    END LOOP;
 END IF;
 COMMIT;

 FOR i IN 1..L_features_enabled.count
 LOOP
    IF (L_features_enabled(i) = 1) THEN
      L_connectivity_enabled := true;
    END IF;
    IF (L_features_enabled(i) = 2) THEN
      L_aercloud_enabled := true;
    END IF;
 END LOOP;


IF(L_connectivity_enabled = false) THEN
      BEGIN
        select feature_code into L_row_count FROM aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=1;
        delete aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=1;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              ExtErrMsg :='update account : connectivity is not enabled';
      END;
END IF;

IF(L_aercloud_enabled = false) THEN
      BEGIN
        select feature_code into L_row_count FROM aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=2;
        delete aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=2;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              ExtErrMsg :='update account : aercloud is not enabled';
      END;
END IF;


IF(L_connectivity_enabled = true) THEN
      BEGIN
        select feature_code into L_row_count FROM aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=1;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              insert into aerisgen.ws_acct_features (account_id, feature_code) values(L_acct_id, 1);
      END;
END IF;

IF(L_aercloud_enabled = true) THEN
      BEGIN
        select feature_code into L_row_count FROM aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=2;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              insert into aerisgen.ws_acct_features (account_id, feature_code) values(L_acct_id, 2);
      END;
END IF;

COMMIT;


  EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			status := SQLCODE;
			ExtErrMsg := 'update_account: ' || SUBSTR(SQLERRM, 1, 100);
			INSERT INTO SQL_ERRORS (code, description, TIME)
								VALUES (status, ExtErrMsg, Sysgmtdate);
			COMMIT;
			RETURN;

  END update_account;


	PROCEDURE create_account_additional_info
						(L_acct_id		IN	NUMBER,
						L_technology	IN		VARCHAR2,
						L_account_status IN 	Number,
						L_child_of		IN 		Number,
            L_parent_account_id IN Number,
            L_parent_label IN VARCHAR2,
						L_invoice_address		IN		VARCHAR2,
						L_special_instruction	IN		VARCHAR2,
						L_tags		IN		VARCHAR2,
						L_contact_type		IN	array_num_type,
            L_contact_name IN array_char_type,
						L_role		IN		array_char_type,
						L_job_title		IN      array_char_type,
						L_email		IN      array_char_type,
						L_phone_no		IN      array_char_type,
            L_Included_Alert_Profiles IN NUMBER,
            L_features_enabled IN array_num_type,
            L_billable_status IN NUMBER,            
            L_operator IN NUMBER,
            L_netsuite_id IN VARCHAR2,  
            L_carrier_account_id IN Number,            
            L_created_by IN VARCHAR2,
            L_last_modified_by IN VARCHAR2,
						status			OUT	NUMBER,
						ExtErrMsg		OUT VARCHAR2
						) IS

   L_top_account NUMBER :=0;
   L_aercloud_enabled boolean := false;
   L_connectivity_enabled boolean := false;
   L_row_count	number := null;

	BEGIN
		status 		:= 0;
		ExtErrMsg	:= 'OK';
    insert into ws_accts_additional_info(Account_Id, Technology, Account_Status,
          Invoice_Address, Special_Instruction, Tags, included_alert_profiles, approval_status, billable_status, operator_id, netsuite_id, carrier_account_id, created_by, last_modified_by, date_created, last_modified_date)
    values ( L_acct_id, L_technology, L_account_status, L_invoice_address, L_special_instruction,
          L_tags,  L_Included_Alert_Profiles, 0, L_billable_status, L_operator, L_netsuite_id, L_carrier_account_id, L_created_by, L_last_modified_by, sysdate, sysdate);

		IF(L_contact_type.count>=1) THEN
      FOR i IN 1..L_contact_type.count
      LOOP
         IF(L_contact_name(i) IS NOT NULL) THEN
            insert into ws_account_contacts(account_id, contact_type, contact_name, contact_role, job_title, email,
            phone, created_by, last_modified_by, date_created, last_modified_date)
            values ( l_acct_id, l_contact_type(i), l_contact_name(i), L_role(i),L_job_title(i), L_email(i), L_phone_no(i),
            L_created_by, L_last_modified_by, sysdate, sysdate);
          END IF;
      END LOOP;
    END IF;

    IF(L_child_of=1 AND L_parent_account_id!=0) THEN

      BEGIN

        Select top_level_parent INTO L_top_account from account_hierarchy
        where parent_account_id=L_parent_account_id
        AND top_level_parent=1;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
          insert into account_hierarchy (parent_account_id, child_account_id, rep_timestamp,
          use_parent_billing, top_level_parent, parent_label) values(L_parent_account_id, L_acct_id, systimestamp,
          0, 1, L_parent_label);

          insert into account_hierarchy (parent_account_id, child_account_id, rep_timestamp,
          use_parent_billing, top_level_parent) values(L_parent_account_id, L_parent_account_id, systimestamp,
          0, 0);

          commit;
      END;

        IF(L_top_account=1) THEN
          insert into account_hierarchy (parent_account_id, child_account_id, rep_timestamp,
          use_parent_billing, top_level_parent, parent_label) values (L_parent_account_id, L_acct_id, systimestamp,
          0, 0, L_parent_label);
        END IF;

    END IF;

		COMMIT;

   FOR i IN 1..L_features_enabled.count
   LOOP
      IF (L_features_enabled(i) = 1) THEN
        L_connectivity_enabled := true;
      END IF;
      IF (L_features_enabled(i) = 2) THEN
        L_aercloud_enabled := true;
      END IF;
   END LOOP;


  IF(L_connectivity_enabled = false) THEN
        BEGIN
          select feature_code into L_row_count FROM aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=1;
          delete aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=1;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                ExtErrMsg :='update account : connectivity is not enabled';
        END;
  END IF;

  IF(L_aercloud_enabled = false) THEN
        BEGIN
          select feature_code into L_row_count FROM aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=2;
          delete aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=2;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                ExtErrMsg :='update account : aercloud is not enabled';
        END;
  END IF;


  IF(L_connectivity_enabled = true) THEN
        BEGIN
          select feature_code into L_row_count FROM aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=1;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                insert into aerisgen.ws_acct_features (account_id, feature_code) values(L_acct_id, 1);
        END;
  END IF;

  IF(L_aercloud_enabled = true) THEN
        BEGIN
          select feature_code into L_row_count FROM aerisgen.ws_acct_features where account_id = L_acct_id and feature_code=2;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                insert into aerisgen.ws_acct_features (account_id, feature_code) values(L_acct_id, 2);
        END;
  END IF;

  COMMIT;

		EXCEPTION
		WHEN OTHERS THEN
			ROLLBACK;
			status := SQLCODE;
			ExtErrMsg := 'create_account_additional_info: ' || SUBSTR(SQLERRM, 1, 100);
			INSERT INTO SQL_ERRORS (code, description, TIME)
								VALUES (status, ExtErrMsg, Sysgmtdate);
			COMMIT;
			RETURN;

	END create_account_additional_info;

	PROCEDURE create_account_new
						(L_acct_name	IN		VARCHAR2,
						L_service_name	IN 	VARCHAR2,
						L_login			IN 	VARCHAR2,
						L_password		IN		VARCHAR2,
						L_technology	IN		VARCHAR2,
						L_MB_only		IN		NUMBER,
						L_MT_VOICE			IN		NUMBER,
						L_staticIP		IN	NUMBER,
						L_acct_type		IN  NUMBER,
            L_region_code IN NUMBER,
            L_carrier_account_id IN Number,
            L_web_address IN VARCHAR2,
            L_SMS IN NUMBER,
            L_MO_VOICE IN NUMBER,
            L_MO_VOICE_REGEX IN VARCHAR2,
            L_roaming IN NUMBER,
            L_operator IN NUMBER,
						L_acct_id		OUT	NUMBER,
            status			OUT	NUMBER,
						ExtErrMsg		OUT	VARCHAR2
						) IS

		local_code				NUMBER 			:= 0;
		LOCAL_REGION			NUMBER 			:= 1;
		CSP_ACCOUNT				NUMBER 			:= 2;
		local_technology		VARCHAR2(12)	:= '';
		dft_cdma_carrier_id	number			:= null;
		dft_cdma_csp_id		number			:= null;
		dft_gsm_carrier_id	number			:= null;
		dft_gsm_csp_id			number			:= null;

	BEGIN

		L_acct_id 	:= 0;
		status 		:= 0;
		ExtErrMsg	:= 'OK';

		IF (trim(L_service_name) IS NULL OR
			 trim(L_login) IS NULL 			OR
			 trim(L_password) IS NULL) THEN
			status := 1;
			ExtErrMsg := 'Invalid Serice_name/login/password = ' || trim(L_service_name) ||
							 '/' || trim(L_login) || '/' || trim(L_password);
			RETURN;
		END IF;

		--
		--	Create an account
		--
		IF(L_acct_type = 2 AND L_operator = OPERATOR_SPRINT) THEN
			SELECT	account_id + 1
			INTO 		L_acct_id
			FROM		aersys_accounts
			WHERE		account_id =
						(SELECT 	MAX(account_id)
						FROM		aersys_accounts
						WHERE		account_id between 40000 and 49998)
			FOR UPDATE;
		END IF;
		IF(L_acct_type = 2 AND L_operator = OPERATOR_AERIS) THEN
			SELECT	account_id + 1
			INTO 		L_acct_id
			FROM		aersys_accounts
			WHERE		account_id =
						(SELECT 	MAX(account_id)
						FROM		aersys_accounts
						WHERE		account_id <20000)
			FOR UPDATE;
		END IF;

		INSERT  INTO		aersys_accounts
					(account_type, account_id, name, region_code, rep_time,not_used,www_addr)
		VALUES	(CSP_ACCOUNT, L_acct_id, L_acct_name, L_region_code,
					sys_extract_utc(systimestamp),1,L_web_address);
		IF((L_acct_id > 40000 AND L_acct_id<49999) AND (l_operator=1))		THEN
			insert into AERBILL_TRAFFIC.t_ban_acct_lkp@BILLING.BDB022.AERIS.NET values
				  (L_carrier_account_id, L_acct_id, 'portal' , sysdate, 'portal', sysdate, 'N');
		END IF;

		--
		--	Add service Name to account
		--
		add_service_name_new (L_acct_id, L_service_name, L_login, L_password,
								L_MB_only, L_MT_VOICE, L_staticIP, L_acct_type, L_technology, L_SMS, L_MO_VOICE, l_mo_voice_regex, L_Roaming, status, ExtErrMsg);
		IF (status != 0) THEN
    			ExtErrMsg := 'Add service_name failed: service_name = ' || L_service_name || '.' ;
				ROLLBACK;
      RETURN;
		END IF;
		
		EXCEPTION
			WHEN OTHERS THEN
				ROLLBACK;
				status := SQLCODE;
				ExtErrMsg := 'create_account: ' || SUBSTR(SQLERRM, 1, 100);
				INSERT INTO SQL_ERRORS (code, description, TIME)
									VALUES (status, ExtErrMsg, Sysgmtdate);
				COMMIT;
				RETURN;
COMMIT;

	END create_account_new;


--
--------------------------------------------------------------
--	Add service_name to an account
--------------------------------------------------------------
--
	PROCEDURE add_service_name_new
						(L_acct_id IN NUMBER,
						L_service_name IN VARCHAR2,
						L_login	IN VARCHAR2,
						L_password IN VARCHAR2,
						L_MB_only	IN NUMBER,
						L_MT_VOICE	IN NUMBER,
						L_staticIP IN NUMBER,
						L_acct_type	IN NUMBER,
						L_Technology	IN	VARCHAR2,
            L_SMS IN NUMBER,
            L_MO_VOICE In NUMBER,
            L_MO_VOICE_REGEX IN VARCHAR2,
            L_roaming IN NUMBER,
						status OUT	NUMBER,
						ExtErrMsg OUT	VARCHAR2
						) IS
		local_code				NUMBER := 0;
		Aeris5YY_ID				NUMBER := 8;
		CommandCenter_ID		NUMBER := 7;
		GSM_ID			NUMBER := 6;
		CommandCenter_ACCT	NUMBER := 40000;
		Default_Carrier_ID	NUMBER;
    local_technology VARCHAR2(30) :='';


	BEGIN

		status := 0;
		ExtErrMsg := 'OK';
    -- Set to 0 as MicrBurst is not available as a paremeter in the new account management UI.
    -- Kept the provision to accept it in procedure just in case we are required to support it

		IF (L_acct_id >= CommandCenter_ACCT) THEN
			Default_Carrier_ID := CommandCenter_ID;
		ELSE
			local_technology := UPPER(trim(L_technology));
			IF (local_technology LIKE '%GSM%') THEN
			Default_Carrier_ID := Aeris5YY_ID;
			ELSE
        IF (local_technology LIKE 'CDMA') THEN
        Default_Carrier_ID := Aeris5YY_ID;
        END IF;
      END IF;
		END IF;

		SELECT	service_code + 1
		INTO 		local_code
		FROM		owner_account_tcdb_link
		WHERE		service_code =
					(SELECT MAX(service_code)
					FROM		owner_account_tcdb_link
					WHERE		service_code < 10000)
		FOR UPDATE;

		INSERT
		INTO		owner_account_tcdb_link
					(account_id, service_code, service_name)
		VALUES	(L_acct_id, local_code, L_service_name);

		INSERT
		INTO		account_list_tcdb_link
					(login_name, PASSWORD, service_code)
		VALUES	(L_login, L_password, local_code);

		INSERT
		INTO		CDMA_CARRIER_ACCOUNT
					(service_code, carrier_id)
		VALUES	(local_code, Default_Carrier_ID);

    INSERT
    INTO    aersys_service_profile
            (Account_ID, Service_Code, Service_Name, SMS, MT_Voice, MO_Voice, MO_Voice_Regex, roaming, technology)
    VALUES  (l_acct_id,local_code, l_service_name, L_SMS, l_mt_voice, l_mo_voice, l_mo_voice_regex, l_roaming, l_technology);


	--	COMMIT;

		--
		--	if MB_only then there is no voice or staticIP services
		--
		IF (L_MB_only = 1 AND L_acct_type = 2 AND l_acct_id<20000) THEN
			INSERT
			INTO		amps_account
						(rep_timestamp, account_id, service_name, service_code, voice_allowed, last_change)
			VALUES 	(sys_extract_utc(systimestamp), L_acct_id, L_service_name, local_code, L_MT_VOICE, Sysgmtdate);
		--	COMMIT;
    END IF;

    IF (L_MB_only = 1 AND L_acct_type = 2 AND (l_acct_id>40000 AND l_acct_id<49999)) THEN
      INSERT
			INTO		mb_account
						(rep_timestamp, service_code)
			VALUES 	(sys_extract_utc(systimestamp), local_code);
		--	COMMIT;
		END IF;


		--
		--	check for voice option
		--
		IF (L_MT_VOICE = 1 AND L_acct_type = 2 AND l_acct_id<20000) THEN
			INSERT
			INTO		voice_account
						(rep_timestamp, account_id, service_name, service_code, voice_allowed, last_change)
			VALUES 	(sys_extract_utc(systimestamp), L_acct_id, L_service_name, local_code, 0, Sysgmtdate);
		--	COMMIT;
		END IF;

		IF (L_MT_VOICE = 1 AND L_acct_type = 2 AND (l_acct_id>40000 AND l_acct_id<49999)) THEN
			INSERT
			INTO		AERBILL_PROV.voice_account_v1
						(rep_timestamp, service_code)
			VALUES 	(sys_extract_utc(systimestamp), local_code);
		--	COMMIT;
			RETURN;
		END IF;

		--
		--	check for staicIP option
		--
		IF (L_staticIP = 1 AND L_acct_type = 2 AND l_acct_id<20000) THEN
			INSERT
			INTO		ip_account
						(rep_timestamp, account_id, service_name, service_code, last_change)
			VALUES 	(sys_extract_utc(systimestamp), L_acct_id, L_service_name, local_code, Sysgmtdate);
		--	COMMIT;
		END IF;

		IF (L_staticIP = 1 AND L_acct_type = 2 AND (l_acct_id>40000 AND l_acct_id<49999)) THEN
			INSERT
			INTO		AERBILL_PROV.ip_account_v1
						(rep_timestamp, service_code)
			VALUES 	(sys_extract_utc(systimestamp), local_code);
		--	COMMIT;
			RETURN;
		END IF;

		EXCEPTION
			WHEN OTHERS THEN
				ROLLBACK;
        delete from aersys_accounts where account_id = l_acct_id;
				status := SQLCODE;
				ExtErrMsg := 'add_service_name : ' || SUBSTR(SQLERRM, 1, 100);
				INSERT INTO SQL_ERRORS (code, description, TIME)
									VALUES (status, ExtErrMsg, Sysgmtdate);
				COMMIT;
				RETURN;

 COMMIT;
	END add_service_name_new;


  PROCEDURE get_acct_details
						(L_acct_id		IN			NUMBER,
						account_cv	OUT	account_cursor,
						status			OUT		NUMBER,
						ExtErrMsg		OUT		VARCHAR2
            ) IS

  BEGIN

    status := 0;
		ExtErrMsg := 'OK';

		OPEN account_cv FOR
    SELECT
            account.account_id,
            account.name,
            account.www_addr,
            account.region_code,
            hierarchy.parent_account_id,
            additional_info.invoice_address,
            additional_info.account_status,
            additional_info.special_instruction,
            additional_info.tags,
            account_contacts.contact_type,
            account_contacts.contact_name,
            account_contacts.contact_role,
            account_contacts.job_title,
            account_contacts.email,
            account_contacts.phone,
            additional_info.carrier_account_id,
            additional_info.included_alert_profiles,
            additional_info.operator_id,
            acct_features.feature_code,
            additional_info.billable_status,
            additional_info.netsuite_id

    FROM
            Aersys_Accounts account
            LEFT JOIN ws_accts_additional_Info additional_info on account.account_id = additional_info.account_id
            LEFT JOIN ws_account_contacts account_contacts on account.account_id = account_contacts.account_id
            LEFT JOIN ws_acct_features acct_features on account.account_id = acct_features.account_id            
            LEFT JOIN account_hierarchy hierarchy on account.account_id = hierarchy.child_account_id AND hierarchy.top_level_parent!=1
    WHERE
            account.account_id=L_acct_id;

				COMMIT;
				RETURN;
  END get_acct_details;

  procedure get_approval_list
            (approval_cv OUT approval_cursor,
            status			OUT		NUMBER,
						ExtErrMsg		OUT		VARCHAR2
            ) IS

  BEGIN
    status := 0;
    ExtErrMsg := 'OK';

    Open approval_cv FOR
      Select distinct account.Account_ID, account.Name
      From aersys_accounts account, ws_accts_additional_info additional_info
      where account.account_id = additional_info.account_id AND additional_info.approval_status = 0
      AND account.not_used = 1;

    EXCEPTION
			WHEN NO_DATA_FOUND THEN
				ROLLBACK;
				status := 0;
				ExtErrMsg := 'No account  found for approval.';
				RETURN;

			WHEN OTHERS THEN
				ROLLBACK;
				status := SQLCODE;
				ExtErrMsg := 'get_acct_details failed: ' || SUBSTR(SQLERRM, 1, 100);
				INSERT INTO SQL_ERRORS (code, description, TIME)
									VALUES (status, ExtErrMsg, Sysgmtdate);
				COMMIT;
				RETURN;

  END get_approval_list;

	PROCEDURE get_Acct_servicename
						(L_acct_id		IN			NUMBER,
						service_cv		OUT	service_cursor,
						status			OUT		NUMBER,
						ExtErrMsg		OUT		VARCHAR2) IS
	BEGIN

		status := 0;
		ExtErrMsg := 'OK';

    IF (l_acct_id<20005) THEN

      OPEN service_cv FOR
        SELECT	a.account_id, a.service_name, b.login_name,
              b.PASSWORD, p.technology, a.service_code,
              DECODE(NVL(c.service_code, 0), 0, 0, 1) MB_only,
              DECODE(NVL(d.service_code, 0), 0, 0, 1) StaticIP,
              DECODE(NVL(e.service_code, 0), 0, 0, 1) Voice,
              p.SMS,
              p.MO_VOICE,
              p.MO_VOICE_REGEX,
              p.roaming
        FROM
              owner_account_tcdb_link a,
              account_list_tcdb_link b,
              amps_account c,
              ip_account d,
              voice_account e,
              aersys_service_profile p
        WHERE		a.account_id 	= L_acct_id					AND
              a.service_code = b.service_code			AND
              a.account_id	= c.account_id		(+)	AND
              a.service_code = c.service_code	(+)	AND
              a.account_id	= d.account_id		(+)	AND
              a.service_code = d.service_code	(+)	AND
              a.account_id	= e.account_id		(+)	AND
              a.service_code = e.service_code	(+) AND
              a.service_code = p.service_code (+) AND
              a.account_id = p.account_id (+);
    END IF;

    IF(l_acct_id >40000 AND l_acct_id<49999) THEN

        OPEN service_cv FOR
        SELECT	a.account_id, a.service_name, b.login_name,
						b.PASSWORD, p.technology, a.service_code,
						DECODE(NVL(c.service_code, 0), 0, 0, 1) MB_only,
						DECODE(NVL(d.service_code, 0), 0, 0, 1) StaticIP,
						DECODE(NVL(e.service_code, 0), 0, 0, 1) Voice,
            p.SMS,
            p.MO_VOICE,
            p.MO_VOICE_REGEX,
            p.roaming
        FROM
            owner_account_tcdb_link a,
						account_list_tcdb_link b,
						MB_account c,
						ip_account_v1 d,
						voice_account_v1 e,
            aersys_service_profile p
        WHERE		a.account_id 	= l_acct_id				AND
						a.service_code = b.service_code			AND
						a.service_code = c.service_code	(+)	AND
						a.service_code = d.service_code	(+)	AND
						a.service_code = e.service_code	(+) AND
            a.service_code = p.service_code (+) AND
            a.account_id = p.account_id (+);

    END IF;

    COMMIT;

		EXCEPTION
			WHEN NO_DATA_FOUND THEN
				ROLLBACK;
				status := 0;
				ExtErrMsg := 'No service name found for account_id = ' ||
								TO_CHAR(L_acct_id) || '.';
				RETURN;

			WHEN OTHERS THEN
				ROLLBACK;
				status := SQLCODE;
				ExtErrMsg := 'get_Acct_servicename failed: ' || SUBSTR(SQLERRM, 1, 100);
				INSERT INTO SQL_ERRORS (code, description, TIME)
									VALUES (status, ExtErrMsg, Sysgmtdate);
				COMMIT;
				RETURN;

	END get_Acct_servicename;

  procedure update_service (L_account_id IN NUMBER,
                            L_service_code IN NUMBER,
                            L_service_name IN VARCHAR2,
                            L_login_name IN VARCHAR2,
                            L_password IN VARCHAR2,
                            L_MB_only	IN NUMBER,
                            L_MT_VOICE	IN NUMBER,
                            L_staticIP IN NUMBER,
                            L_SMS IN NUMBER,
                            L_MO_VOICE In NUMBER,
                            L_MO_VOICE_REGEX IN VARCHAR2,
                            L_roaming IN NUMBER,
                            status OUT	NUMBER,
                            ExtErrMsg OUT	VARCHAR2
                            ) IS

  L_row_count NUMBER :=0;

  BEGIN

    update account_list_tcdb_link  set password=L_password where service_code = L_service_code
    AND login_name=L_login_name;

    update aersys_service_profile set SMS = L_SMS, mo_voice= l_mo_voice, mt_voice = l_mt_voice,
    mo_voice_regex = l_mo_voice_regex, roaming = L_roaming
    where account_id = L_account_ID AND service_code = L_service_code;

    IF(L_MB_ONLY=1) THEN

      BEGIN

        select service_code INTO L_row_count from amps_account where service_code=L_service_code and account_id=L_account_id;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
          insert into amps_account (account_id, service_code, service_name, voice_allowed, last_change)
          values (l_account_id, l_service_code, l_service_name, l_mt_voice, sysdate);

        COMMIT;

      END;

      update amps_account set voice_allowed=L_MT_VOICE, last_change=sysdate where service_code=L_service_code and account_id=L_account_id;

    ELSE

        delete amps_account where service_code=L_service_code and account_id=L_account_id;

    END IF;

    IF(L_MT_VOICE=1) THEN

      BEGIN
        select service_code INTO L_row_count from voice_account where service_code=L_service_code and account_id=L_account_id;

          EXCEPTION
            WHEN NO_DATA_FOUND THEN
            insert into voice_account (account_id, service_code, voice_allowed, last_change) values(L_account_id, L_service_code, L_MT_voice, sysdate);
        commit;

      END;

        update voice_account set voice_allowed = l_mt_voice, last_change=sysdate where service_code=L_service_code and account_id=L_account_id;
        L_row_count := 0;

    ELSE

        update voice_account set voice_allowed=0, last_change= sysdate where service_code=L_service_code and account_id=L_account_id;

    END IF;

    IF(l_staticip = 1) THEN

      BEGIN

        select service_code INTO L_row_count from ip_account where service_code=L_service_code and account_id=L_account_id;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
          insert into ip_account (account_id, service_code, service_name, last_change)
          values (l_account_id, l_service_code, l_service_name, sysdate);
          commit;
      END;

    ELSE

      delete ip_account where service_code=L_service_code and account_id=L_account_id;

    END IF;

    COMMIT;

        EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          status := SQLCODE;
          ExtErrMsg := 'Update service_name failed: service_name/account_id = ' ||
                 L_service_name || '/' || TO_CHAR(L_account_id) || '.'|| SUBSTR(SQLERRM, 1, 100) ;
          INSERT INTO SQL_ERRORS (code, description, TIME)
                    VALUES (status, ExtErrMsg, Sysgmtdate);
          COMMIT;
          RETURN;

  END update_service;

END ACCOUNT_MANAGEMENT_2_0_V2;
