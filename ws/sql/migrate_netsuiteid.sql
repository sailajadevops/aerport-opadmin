DECLARE
---------------------------------------------------------------------------------------------
  -- Name       : Account_BAN_Migration.sql
  -- Type       : PL/SQL Script
  -- Owner      : AERISGEN
  -- Purpose    : Migrate the netsuiteid from mysql DB to WS_ACCTS_ADDITIONAL_INFO.NETSUITE_ID.
  ---------------------------------------------------------------------------------------------
--Curson to fetch account and ban information

CURSOR netsuiteid_cur
IS
SELECT additional_info.account_id, netsuite.netsuite_id FROM aerisgen.netsuiteid_migration_temp netsuite, aerisgen.ws_accts_additional_info additional_info
where additional_info.account_id = netsuite.account_id;

v_err_msg VARCHAR2(100);

BEGIN
  DBMS_OUTPUT.ENABLE(1000000);
  v_err_msg := 'Updating BAN data into AERISGEN.WS_ACCTS_ADDITIONAL_INFO';
  DBMS_OUTPUT.put_line ( 'Inseting data into AERISGEN.WS_ACCTS_ADDITIONAL_INFO..' ) ;
	
  FOR netsuiteid_rec IN netsuiteid_cur
  LOOP
		update AERISGEN.WS_ACCTS_ADDITIONAL_INFO set netsuite_id = netsuiteid_rec.netsuite_id where account_id = netsuiteid_rec.account_id;
  END LOOP;  
  COMMIT;
  
  EXCEPTION
    -- Show Exception to the user in the log --
  WHEN OTHERS THEN
    dbms_output.put_line('Error: ' || v_err_msg || ', SQL ERROR: ' || SQLCODE || SQLERRM); 
    
    v_err_msg := 'Completed the netsuite_id data copying';
    DBMS_OUTPUT.put_line ( 'Completed the netsuite_id acopying..' ) ;
END;
/
drop table aerisgen.netsuiteid_migration_temp;