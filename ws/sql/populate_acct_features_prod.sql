DECLARE
---------------------------------------------------------------------------------------------
  -- Name       : populate_acct_features_preprod.sql
  -- Type       : PL/SQL Script
  -- Owner      : AERISGEN
  -- Purpose    : populate connectivity services feature code for all account except Aercloud only accounts.
  ---------------------------------------------------------------------------------------------
--Curson to fetch account ids



CURSOR feature_cur
IS
SELECT acct.account_id FROM aerisgen.aersys_accounts acct
WHERE (acct.account_type = 2 OR acct.account_type = 0) AND acct.not_used = 0 AND account_id NOT IN (10745,10946)
ORDER by acct.account_id;

v_err_msg VARCHAR2(100);
local_account_id NUMBER;

BEGIN
v_err_msg := 'Inserting feature codes into AERISGEN.WS_FEATURE_CODES';
INSERT INTO aerisgen.ws_feature_codes (FEATURE_CODE,FEATURE_DESC) VALUES (3, 'TrafficPolicy');	
INSERT INTO aerisgen.ws_feature_codes (FEATURE_CODE,FEATURE_DESC) VALUES (4, 'BlockSMS');
INSERT INTO aerisgen.ws_feature_codes (FEATURE_CODE,FEATURE_DESC) VALUES (5, 'TrafficPolicyAlert');	
INSERT INTO aerisgen.ws_feature_codes (FEATURE_CODE,FEATURE_DESC) VALUES (6, 'TrafficPolicyFilter');	
INSERT INTO aerisgen.ws_feature_codes (FEATURE_CODE,FEATURE_DESC) VALUES (7, 'Traffic Detal Report 2.0');	
INSERT INTO aerisgen.ws_feature_codes (FEATURE_CODE,FEATURE_DESC) VALUES (8, 'Mid Cycle Rate Plan');

v_err_msg := 'Inserting account specific feature codes into AERISGEN.WS_FEATURE_CODES';
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (1,3);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (1,4);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (1,5);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (1,6);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (1,7);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (40006,3);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (40006,4);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (40006,5);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (40006,6);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (10115,2);
insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (10953,2);

COMMIT;

  EXCEPTION
    -- Show Exception to the user in the log --
  WHEN OTHERS THEN
    dbms_output.put_line('Error: ' || v_err_msg || ', SQL ERROR: ' || SQLCODE || SQLERRM); 
    
    v_err_msg := 'Completed feature code and account specific feature code insertion';
    DBMS_OUTPUT.put_line ( 'Completed feature code and account specific feature code insertion..' ) ;


END;
/
BEGIN
  DBMS_OUTPUT.ENABLE(1000000);
  v_err_msg := 'Inserting connectivity feature code into AERISGEN.WS_ACCT_FEATURES';
  DBMS_OUTPUT.put_line ( 'Inserting connectivity feature code into AERISGEN.WS_ACCT_FEATURES..' ) ;
	
  FOR feature_rec IN feature_cur
  LOOP
    BEGIN
      select account_id 
      into local_account_id
      from ws_acct_features
      where account_id = feature_rec.account_id and feature_code = 1;
    
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          insert into AERISGEN.WS_ACCT_FEATURES (ACCOUNT_ID,FEATURE_CODE) values (feature_rec.account_id,1);
    END;
  END LOOP;  
  
  COMMIT;
  
  EXCEPTION
    -- Show Exception to the user in the log --
  WHEN OTHERS THEN
    dbms_output.put_line('Error: ' || v_err_msg || ', SQL ERROR: ' || SQLCODE || SQLERRM); 
    
    v_err_msg := 'Completed connectivity feature code insertion';
    DBMS_OUTPUT.put_line ( 'Completed connectivity feature code insertion..' ) ;
  
END;