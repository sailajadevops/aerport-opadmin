/* make the operator entry */

INSERT INTO
WS_OPERATOR
(
  OPERATOR_ID,
  DESCRIPTION,
  REP_TIMESTAMP
)
VALUES
(
  3,
  'CISCO',
  SYSTIMESTAMP
);


/* make new product */

INSERT INTO
PRODUCT
(
  PRODUCT_ID,
  PRODUCT_NAME,
  TECHNOLOGY,
  DATE_CREATED,
  CREATED_BY,
  REP_TIMESTAMP,
  CARRIER_ID,
  MSISDN_RANGE,
  STATIC_IP_RANGE
)
VALUES
(
  8,
  'Global SIM',
  'GSM',
  SYSTIMESTAMP,
  'Harshal.Vaidya@Aeris.net',
  SYSTIMESTAMP,
  15,
  'AERIS',
  'AerisVFN'
);


/* make product operator mapping */

INSERT INTO 
WS_OPERATOR_PRODUCTS
(
  OPERATOR_ID,
  PRODUCT_ID,
  LAST_MODIFIED_DATE,
  LAST_MODIFIED_BY,
  REP_TIMESTAMP
)
VALUES
(
  3,
  8, /*id for GLOBAL SIM product for CISCO*/
  SYSTIMESTAMP,
  'harshal.vaidya@aeris.net',
  SYSTIMESTAMP
);

/*make an entry in the product carrier table*/

INSERT INTO
WS_PRODUCT_CARRIER
(
  REP_TIMESTAMP,
  PRODUCT_ID,
  CARRIER_ID,
  CREATED_DATE,
  CREATED_BY,
  LAST_MODIFIED_BY,
  LAST_MODIFIED_DATE
)
VALUES
(
  SYSTIMESTAMP,
  8, /*the newly added product global sim*/
  15,
  SYSTIMESTAMP,
  'harshal.vaidya@aeris.net',
  'harshal.vaidya@aeris.net',
  SYSTIMESTAMP
);