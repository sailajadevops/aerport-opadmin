create or replace PACKAGE AERBILL_PROV.ACCOUNT_MANAGEMENT_2_0_V2 AS

   err_num   NUMBER;
    err_msg   VARCHAR2(256);
    DEFAULT_CDMA_RATE                                 VARCHAR2(24)  := 'CDMA2INTRO';
    DEFAULT_GSM_RATE                                    VARCHAR2(24)  := 'GSPINTRO';
    TECHNOLOGY_CDMA                                    VARCHAR2(4)                    := 'CDMA';
    TECHNOLOGY_GSM                                 VARCHAR2(3)                    := 'GSM';
    DFT_CDMA_CARRIER_RATE        varchar2(24)       := 'AERPLAN1';
    DFT_GSM_CARRIER_RATE                           varchar2(24)       := '16033';
    AerisEnhanced_ID                                           NUMBER                                             := 6;
    Aeris5YY_ID       number                                                := 8;
    OPERATOR_AERIS      number                                                := 1;
    OPERATOR_SPRINT      number                                                := 2;



	PROCEDURE approve_account
						(
						L_acct_id		IN	NUMBER,
						status			OUT	NUMBER,
						ExtErrMsg		OUT	VARCHAR2
						);

    PROCEDURE update_account
						(L_acct_id		IN	NUMBER,
						L_account_status IN 	Number,
						L_region_code IN NUMBER,
						L_web_address IN VARCHAR2,
            L_invoice_address		IN		VARCHAR2,
						L_special_instruction	IN		VARCHAR2,
						L_tags		IN		VARCHAR2,
						L_contact_type		IN	array_num_type,
            L_contact_name IN		array_char_type,
						L_role		IN		array_char_type,
						L_job_title		IN      array_char_type,
						L_email		IN      array_char_type,
						L_phone_no		IN      array_char_type,
            L_Included_Alert_Profiles IN NUMBER,
            L_features_enabled IN array_num_type,
            L_billable_status IN NUMBER,
            L_account_name IN VARCHAR2,
            L_last_modified_by IN VARCHAR2,
						status			OUT	NUMBER,
						ExtErrMsg		OUT VARCHAR2
						);


  PROCEDURE create_account_additional_info
						(L_acct_id		IN	NUMBER,
						L_technology	IN		VARCHAR2,
						L_account_status IN 	Number,
						L_child_of		IN 		Number,
            L_parent_account_id IN Number,
            L_parent_label IN VARCHAR2,
						L_invoice_address		IN		VARCHAR2,
						L_special_instruction	IN		VARCHAR2,
						L_tags		IN		VARCHAR2,
						L_contact_type		IN	array_num_type,
            L_contact_name IN		array_char_type,
						L_role		IN		array_char_type,
						L_job_title		IN      array_char_type,
						L_email		IN      array_char_type,
						L_phone_no		IN      array_char_type,
            L_Included_Alert_Profiles IN NUMBER,
            L_features_enabled IN array_num_type,
            L_billable_status IN NUMBER,
            L_operator IN NUMBER,
            L_netsuite_id VARCHAR2,
            L_carrier_account_id IN Number,            
            L_created_by IN VARCHAR2,
            L_last_modified_by IN VARCHAR2,
						status			OUT	NUMBER,
						ExtErrMsg		OUT VARCHAR2
						);

  	PROCEDURE create_account_new
						(L_acct_name	IN		VARCHAR2,
						L_service_name	IN 	VARCHAR2,
						L_login			IN 	VARCHAR2,
						L_password		IN		VARCHAR2,
						L_technology	IN		VARCHAR2,
						L_MB_only		IN		NUMBER,
						L_MT_VOICE			IN		NUMBER,
						L_staticIP		IN	NUMBER,
						L_acct_type		IN  NUMBER,
            L_region_code IN NUMBER,
            L_carrier_account_id IN Number,
            L_web_address IN VARCHAR2,
            L_SMS IN NUMBER,
            L_MO_VOICE In NUMBER,
            L_MO_VOICE_REGEX IN VARCHAR2,
            L_roaming IN NUMBER,
            L_operator IN NUMBER,
						L_acct_id		OUT	NUMBER,
						status			OUT	NUMBER,
						ExtErrMsg		OUT	VARCHAR2
						);

    PROCEDURE add_service_name_new
						(L_acct_id IN NUMBER,
						L_service_name IN VARCHAR2,
						L_login	IN VARCHAR2,
						L_password IN VARCHAR2,
						L_MB_only	IN NUMBER DEFAULT 0,
						L_MT_VOICE	IN NUMBER,
						L_staticIP IN NUMBER,
						L_acct_type	IN NUMBER,
						L_Technology	IN	VARCHAR2,
            L_SMS IN NUMBER,
            L_MO_VOICE In NUMBER,
            L_MO_VOICE_REGEX IN VARCHAR2,
            L_roaming IN NUMBER,
						status OUT	NUMBER,
						ExtErrMsg OUT	VARCHAR2
						);

	TYPE account_rec IS RECORD
						(L_acct_id		NUMBER,
						L_acct_name		VARCHAR2(60),
            L_web_address VARCHAR2(40),
            L_region_code NUMBER,
            L_parent_acct_id NUMBER,
            L_invoice_address	VARCHAR2(2048),
            L_account_status Number,
            L_special_instruction	VARCHAR2(2048),
            L_tags VARCHAR2(2048),
						L_contact_type NUMBER,
            L_contact_name VARCHAR2(100),
						L_role VARCHAR2(100),
						L_job_title VARCHAR2(100),
						L_email VARCHAR2(100),
						L_phone_no VARCHAR2(100),
            L_ban NUMBER,
            L_Included_Alert_Profiles NUMBER,
            L_operator NUMBER,
            L_features_enabled NUMBER,
            L_billable_status NUMBER, 
            L_netsuite_id NUMBER);

	TYPE account_cursor IS REF CURSOR RETURN account_rec;

  TYPE approval_rec IS RECORD
              (L_account_id NUMBER,
              L_account_name varchar2(100));

  TYPE approval_cursor IS REF CURSOR RETURN approval_rec;

  TYPE service_rec IS RECORD
						(L_acct_id		NUMBER,
						L_service_name	CHAR(24),
						L_login			CHAR(24),
						L_password		CHAR(24),
            L_Technology CHAR(24),
						L_MB_only		NUMBER,
						L_MT_VOICE			NUMBER,
						L_staticIP		NUMBER,
						L_service_code	NUMBER,
            L_SMS   NUMBER,
            L_MO_VOICE    NUMBER,
            L_MO_VOICE_REGEX VARCHAR2(30),
            L_ROAMING NUMBER
            );

	TYPE service_cursor IS REF CURSOR RETURN service_rec;

  PROCEDURE get_approval_list
            (approval_cv OUT approval_cursor,
            status			OUT		NUMBER,
						ExtErrMsg		OUT		VARCHAR2
            ) ;

 PROCEDURE get_acct_details
						(L_acct_id		IN			NUMBER,
						account_cv		OUT	account_cursor,
						status			OUT		NUMBER,
						ExtErrMsg		OUT		VARCHAR2);

 PROCEDURE get_Acct_servicename
						(L_acct_id		IN			NUMBER,
						service_cv		OUT	service_cursor,
						status			OUT		NUMBER,
						ExtErrMsg		OUT		VARCHAR2);

 PROCEDURE update_service (L_account_id IN NUMBER,
                            L_service_code IN NUMBER,
                            L_service_name IN VARCHAR2,
                            L_login_name IN VARCHAR2,
                            L_password IN VARCHAR2,
                            L_MB_only	IN NUMBER,
                            L_MT_VOICE	IN NUMBER,
                            L_staticIP IN NUMBER,
                            L_SMS IN NUMBER,
                            L_MO_VOICE In NUMBER,
                            L_MO_VOICE_REGEX VARCHAR2,
                            L_roaming IN NUMBER,
                            status OUT	NUMBER,
                            ExtErrMsg OUT	VARCHAR2
                            );


END ACCOUNT_MANAGEMENT_2_0_V2;
