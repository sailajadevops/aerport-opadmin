DECLARE
---------------------------------------------------------------------------------------------
  -- Name       : Account_BAN_Migration.sql
  -- Type       : PL/SQL Script
  -- Owner      : AERBILL_PROV
  -- Purpose    : Migrate the BAN from Billing DB to WS_ACCTS_ADDITIONAL_INFO.CARRIER_ACCOUNT_ID.
  ---------------------------------------------------------------------------------------------
--Curson to fetch account and ban information

CURSOR ban_cur
IS
SELECT acct.account_id, acct.name, carrier_link.ban FROM aerisgen.aersys_accounts acct
LEFT JOIN AERBILL_TRAFFIC.t_ban_acct_lkp@BILLING.BDB022.AERIS.NET carrier_link 
on acct.account_id = carrier_link.account_id 
WHERE (acct.account_type = 2 OR acct.account_type = 0) AND acct.not_used = 0 
AND (lower(acct.NAME) NOT IN ('aeris','aeris.net','aeris communications inc.') OR acct.account_id = 1) 
ORDER by acct.account_id;

v_err_msg VARCHAR2(100);

BEGIN
  DBMS_OUTPUT.ENABLE(1000000);
  v_err_msg := 'Updating BAN data into AERISGEN.WS_ACCTS_ADDITIONAL_INFO';
  DBMS_OUTPUT.put_line ( 'Inseting data into AERISGEN.WS_ACCTS_ADDITIONAL_INFO..' ) ;
	
  FOR ban_rec IN ban_cur
  LOOP
    update AERISGEN.WS_ACCTS_ADDITIONAL_INFO set carrier_account_id = ban_rec.ban where account_id = ban_rec.account_id;
  END LOOP;  
  COMMIT;
  
  EXCEPTION
    -- Show Exception to the user in the log --
  WHEN OTHERS THEN
    dbms_output.put_line('Error: ' || v_err_msg || ', SQL ERROR: ' || SQLCODE || SQLERRM); 
    
    v_err_msg := 'Completed the BAN data copying';
    DBMS_OUTPUT.put_line ( 'Completed the BAN acopying..' ) ;
  
END;