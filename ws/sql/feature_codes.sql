INSERT INTO aerisgen.ws_feature_codes VALUES (1, 'Connectivity', systimestamp);
INSERT INTO aerisgen.ws_feature_codes VALUES (2, 'Aercloud', systimestamp);
INSERT INTO aerisgen.ws_feature_codes VALUES (3, 'TrafficPolicy', systimestamp);	
INSERT INTO aerisgen.ws_feature_codes VALUES (4, 'BlockSMS', systimestamp);
INSERT INTO aerisgen.ws_feature_codes VALUES (5, 'TrafficPolicyAlert', systimestamp);	
INSERT INTO aerisgen.ws_feature_codes VALUES (6, 'TrafficPolicyFilter', systimestamp);	
INSERT INTO aerisgen.ws_feature_codes VALUES (7, 'Traffic Detal Report 2.0', systimestamp);	
INSERT INTO aerisgen.ws_feature_codes VALUES (8, 'Mid Cycle Rate Plan', systimestamp);	
